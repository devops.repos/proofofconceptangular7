Routing typically we will import our routing and then export that.
Core is only going to be imported into our root module, so this module, normally, don’t have really any exports, is normally a standalone and it will be imported just into the root.
Shared is the one that almost always will have some exports, because otherwise it wouldn’t be shareable, for sure.
Services is the one with all the root level and shared services.