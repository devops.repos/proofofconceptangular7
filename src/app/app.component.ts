import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { Subscription } from 'rxjs';
import { NavService } from './core/sidenav-list/nav.service';
import { CommonService } from './services/helper-services/common.service';
import { IdleDialogService } from './core/session-out/idle-dialog.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
  isLoading: boolean = true;
  isOverlay: boolean = false;
  navStatusSub: Subscription;

  constructor(
    private authService: AuthService,
    private navService: NavService,
    private commonService: CommonService,
    private idleDialogService: IdleDialogService
  ) {
  }

  ngOnInit() {
    /** Progress Spinner */
    if (this.authService.isUserTokenValid()) {
      setTimeout(() => {
        this.isLoading = false;
      }, 10);
    } else {
      this.navStatusSub = this.navService.getCurrentUrl().subscribe((url: string) => {
        const activeUrl = url.split('/');
        if (activeUrl.length > 1) {
          setTimeout(() => {
            this.isLoading = false;
          }, 10);
        }
      });
    }

    /** Setting isOverlay Locally **/
    this.commonService.getIsOverlay().subscribe(res => { this.isOverlay = res });


  }

  ngOnDestroy() {
    this.navStatusSub.unsubscribe();
  }
}
