import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PactLandingPageComponent } from './pact-landing-page.component';

describe('PactLandingPageComponent', () => {
  let component: PactLandingPageComponent;
  let fixture: ComponentFixture<PactLandingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PactLandingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PactLandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
