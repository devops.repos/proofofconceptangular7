import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { NotificationService } from 'src/app/services/helper-services/notification.service';
import { ThemeService } from 'src/app/services/helper-services/theme.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSnackBar } from '@angular/material';
import {MatDialog} from '@angular/material/dialog';
import { AuthData } from 'src/app/models/auth-data.model';
import { IdleDialogService } from '../session-out/idle-dialog.service';
import { UserAgencyType, UserSiteType } from '../../models/pact-enums.enum';
import { UserService } from 'src/app/services/helper-services/user.service';
import { environment } from '../../../environments/environment';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ApplicationNotifications } from '../pact-dashboard/pact-dashboard.model';
import { PactDashboardService } from '../pact-dashboard/pact-dashboard.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SupportDialogComponent } from './support-dialog/support-dialog.component';

@Component({
  selector: 'app-pact-landing-page',
  templateUrl: './pact-landing-page.component.html',
  styleUrls: ['./pact-landing-page.component.scss']
})
export class PactLandingPageComponent implements OnInit, OnDestroy {
  selectedTheme: string;
  themeSub: Subscription;

  userAgencyType = UserAgencyType;
  userData: AuthData;     /* Current user information */
  is_SH_RA = false;
  is_SH_PE = false;
  is_SH_HP = false;
  is_CAS = false;
  is_ML_DHS_Liasian = false;
  is_ML_Team = false;
  is_ML_HP = false;
  is_ML_IREA = false;
  isImpersonation = false;

  currentUser = '';
  currentUserAgencyType = '';

  navCollapsed = false;
  widthSideBar: number;
  widthSideBarExpanded = 260;
  widthSideBarCollapsed = 50;

  mobileSize: MediaQueryList;
  normalSize: MediaQueryList;
  private mobileQueryListener: () => void;

  notification: string;

  userDataSub: Subscription;
  currentUserSub: Subscription;
  currentUserAgencyTypeSub: Subscription;
  navStatusSub: Subscription;
  widthSideBarSub: Subscription;

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private theme: ThemeService,
    private navService: NavService,
    private userService: UserService,
    private authService: AuthService,
    private commonService: CommonService,
    private notificationService: NotificationService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private snackBar: MatSnackBar,
    private idleDialogService: IdleDialogService,
    private dashboardService: PactDashboardService,
    private dialog: MatDialog
  ) {
    this.mobileSize = media.matchMedia('(max-width: 959px)');
    this.normalSize = media.matchMedia('(min-width: 960px)');

    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileSize.addListener(this.mobileQueryListener);
    this.normalSize.addListener(this.mobileQueryListener);

    /* Getting all the App Notifications (Alerts, PopUps, Announcements) on every page redirection */
    // this.route.data.subscribe((res) => {
    //   var notificationList: ApplicationNotifications = res.notifications;
    //   // console.log('NotificationResolver : ', notificationList);
    //   if (notificationList) {
    //     this.dashboardService.setAppNotification(notificationList);
    //   }

    // });

  }

  ngOnInit() {
     /* Check the browser compatibility  */
     const browserName = this.getBrowserName();
    //  console.log(browserName);
     if (browserName !== 'chrome') {
       this.router.navigate(['/un-supported-browser']);
     }
    /* Getting the selected theme from the theme service */
    this.themeSub = this.theme.getSelectedTheme().subscribe((res: string) => {
      this.selectedTheme = res;
      // console.log(res);
    });
    /* Getting the CurrentUser Details from UserService */
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        // console.log('userData: ', this.userData);
        this.is_SH_RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_RA);
        this.is_SH_PE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_PE);
        this.is_SH_HP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_HP);
        this.is_CAS = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.CAS);
        this.is_ML_DHS_Liasian = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_DHS_Liasian);
        this.is_ML_Team = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_Team);
        this.is_ML_HP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_HP);
        this.is_ML_IREA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_IREA);
        this.isImpersonation = this.commonService._doesValueExistInJson(this.userData.userFunctions, 'IMPERSONATION');
        // console.log('isImpersonate: ', this.isImpersonate);
      }
    });

    /* Getting the currentUser from the UserService */
    this.currentUserSub = this.userService.getCurrentUser().subscribe((usr: string) => {
      this.currentUser = usr;
    });

    /* Getting the currentUserAgencyType from UserService */
    this.currentUserAgencyTypeSub = this.userService.getCurrentUserAgencyType().subscribe((usrAgncyType: UserAgencyType) => {
      this.currentUserAgencyType = usrAgncyType;
      // console.log('sidenav-list current userAgencyType: ' + this.currentUserAgencyType);
    });

    /* Checking for user Idle status */
    if (environment.idleTimeOut > 0) {
      this.idleDialogService.CheckUserIdleStatus();
    }

    /* Testing server Error and displaying as notification */
    this.notificationService.notification$.subscribe(message => {
      this.notification = message;
      if (this.notification) {
        this.snackBar.open(this.notification, '', {
          duration: 5000
        });
      }
    });

    /* Getting the Sidenav collapsed status from the nav Service */
    this.navService.getNavCollapsedStatus().subscribe(stat => (this.navCollapsed = stat));

    /* Getting the width of the sidenav to reflect the changes to nav toggle */
    this.widthSideBarSub = this.navService.getWidthSideBar().subscribe(res => {
      this.widthSideBar = res;
    });

    /* below code is to minimize the sidnav when the listed pages are triggered */
    this.navStatusSub = this.navService.getCurrentUrl().subscribe((url: string) => {
      // console.log('currentURL: ', url);
      if ((url === '/vcs/client-awaiting-placement' ||
        url === '/vcs/referral-roster/interview-outcome' ||
        url === '/vcs/vacancy-listing' ||
        url === '/vcs/tad-verification') &&
        !this.mobileSize.matches) {
        if (this.widthSideBar === this.widthSideBarExpanded) {
          this.toggleSideBar();
          // console.log('navCollapsed : ' + this.navCollapsed);
          // console.log('widthSideBar : ' + this.widthSideBar);
        }
      }
    });
  }

  getBrowserName() {
    // let browserName: string;
    // if ((navigator.userAgent.indexOf('Opera') || navigator.userAgent.indexOf('OPR')) != -1) {
    //   browserName = 'Opera';
    // } else if (navigator.userAgent.indexOf('Chrome') != -1) {
    //   browserName = 'Chrome';
    // } else if (navigator.userAgent.indexOf('Safari') != -1) {
    //   browserName = 'Safari';
    // } else if (navigator.userAgent.indexOf('Firefox') != -1) {
    //   browserName = 'Firefox';
    // } else if ((navigator.userAgent.indexOf('MSIE') != -1) || (!!window.document.documentMode == true)) {
    //   browserName = 'IE';
    // } else {
    //   browserName = 'unknown';
    // }
    // return browserName;
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        return 'edge';
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        return 'opera';
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        return 'chrome';
      case agent.indexOf('trident') > -1 || agent.indexOf('msie') > -1 || (!!window.document.documentMode == true):
        return 'ie';
      case agent.indexOf('firefox') > -1:
        return 'firefox';
      case agent.indexOf('safari') > -1:
        return 'safari';
      default:
        return 'other';
    }
  }

  /* Toggle the sidnav to collapse/expand */
  toggleSideBar() {
    if (this.widthSideBar === this.widthSideBarExpanded) {
      this.widthSideBar = this.widthSideBarCollapsed;
    } else {
      this.widthSideBar = this.widthSideBarExpanded;
    }
    this.navCollapsed = !this.navCollapsed;
    this.navService.navCollapsedToggle(this.navCollapsed);
    this.navService.setWidthSideBar(this.widthSideBar);
  }

  /* Making sidenav expanded explicitly */
  setNavCollapsed() {
    this.navService.navCollapsedToggle(false);
    this.navService.setWidthSideBar(260);
  }

  navCollapsedRecover() {
    this.navService.navCollapsedToggle(true);
  }

  /* setting the selected theme name */
  onThemeSelected(selectedTheme: string) {
    this.theme.setSelectedTheme(selectedTheme);
  }

  /* Testing for the Client Side and server side errors (only for testing purpose) should be removed later */
  onErrorSelected(error: string) {
    if (error === 'clientError') {
      throw new Error('Client Error !!!');
    } else if (error === 'serverError') {
      this.httpClient.get(environment.pactApiUrl + 'identity').subscribe();
    }
  }

  /* Sidenav User Profile tab (static workout) should be removed later */
  setCurrentUserAgencyType(userAgencyType: UserAgencyType) {
    this.userService.setCurrentUserAgencyType(userAgencyType);
    if (userAgencyType === UserAgencyType.PlacementEntity) {
      this.setCurrentUser('Prasad Mittapalli');
    } else if (userAgencyType === UserAgencyType.HousingProvider) {
      this.setCurrentUser('Apson Shrestha');
    } else if (userAgencyType === UserAgencyType.Reviewer) {
      this.setCurrentUser('Mir Ather');
    } else if (userAgencyType === UserAgencyType.ReferringAgency) {
      this.setCurrentUser('Hayagreeva Acharya Kalathur');
    }
  }
  /* (static workout) should be removed later */
  setCurrentUser(user: string) {
    this.userService.setCurrentUser(user);
  }

  onLogoutClick() {
    this.router.navigate(['/session-out', 'logout']);
  }

  openSupportDialog() {
    const dialogRef = this.dialog.open(SupportDialogComponent, {
      width: '90%',
      panelClass: 'transparent',
      // disableClose: true
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }

  ngOnDestroy(): void {
    this.mobileSize.removeListener(this.mobileQueryListener);
    this.normalSize.removeListener(this.mobileQueryListener);
    if (this.themeSub) {
      this.themeSub.unsubscribe();
    }
    if (this.navStatusSub) {
      this.navStatusSub.unsubscribe();
    }
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.currentUserAgencyTypeSub) {
      this.currentUserAgencyTypeSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }

  }
}

