import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-support-dialog',
  templateUrl: './support-dialog.component.html',
  styleUrls: ['./support-dialog.component.scss']
})
export class SupportDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SupportDialogComponent>,
  ) {}

  ngOnInit() {}

  onOKClick(){
    this.dialogRef.close();
  }

}
