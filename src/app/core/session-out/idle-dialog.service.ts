import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { UserIdleService } from 'angular-user-idle';
import { IdleDialogBoxComponent } from './idle-dialog-box/idle-dialog-box.component';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IdleDialogService {
  waitingTime = environment.idleDialogWaitTime ; /* Default 300 seconds = 5 minutes */
  idleDialogRef: MatDialogRef<IdleDialogBoxComponent>;

  onTimerStartSub: Subscription;
  onTimeoutSub: Subscription;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private userIdle: UserIdleService
  ) { }

  CheckUserIdleStatus() {
    /* Start watching for user inactivity. */
    // console.log('userIdle.startWatching() ', new Date());
    this.userIdle.startWatching();

    /* Start watching when user idle is starting. */
    this.onTimerStartSub = this.userIdle.onTimerStart().subscribe(count => {
      // console.log('userIdle.onTimerStart() count : ', count);
    });

    /* Start watch when time is up. */
    this.onTimeoutSub = this.userIdle.onTimeout().subscribe(() => {
      // console.log('userIdle.onTimeout() ', new Date());
      /*
       * Opening a Dialog Popup to display session timeout warning
       * with 5 minutes of count down timer.
       */
      this.idleDialogRef = this.dialog.open(IdleDialogBoxComponent, {
        // data: { waitingTime: this.waitingTime },
        panelClass: 'transparent',
        disableClose: true
      });
      this.idleDialogRef.afterClosed().subscribe(result => {
        // console.log('idleDialogRef.afterClosed() ', new Date());
        if (result === 'userClickedContinueButton') {
          // console.log('userIdle.resetTimer()');
          this.userIdle.resetTimer();
        } else if (result === 'userClickedLogoutButton') {
          /* Redirect to session-out page. */
          // console.log('userIdle.stopTimer()');
          // this.userIdle.stopTimer();
          // console.log('userIdle.stopWatching()');
          this.userIdle.stopWatching();
          if (this.onTimerStartSub) {
            // console.log('onTimerStartSub.unsubscribe()');
            this.onTimerStartSub.unsubscribe();
          }
          if (this.onTimeoutSub) {
            // console.log('onTimeoutSub.unsubscribe()');
            this.onTimeoutSub.unsubscribe();
          }
          setTimeout(() => {
            // console.log('router.navigate([\'/session-out\', \'logout\'])');
            this.router.navigate(['/session-out', 'logout']);
          }, 500);
        } else {
          /* Redirect to session-out page. */
          // console.log('userIdle.stopTimer()');
          // this.userIdle.stopTimer();
          // console.log('userIdle.stopWatching()');
          this.userIdle.stopWatching();
          if (this.onTimerStartSub) {
            // console.log('onTimerStartSub.unsubscribe()');
            this.onTimerStartSub.unsubscribe();
          }
          if (this.onTimeoutSub) {
            // console.log('onTimeoutSub.unsubscribe()');
            this.onTimeoutSub.unsubscribe();
          }
          setTimeout(() => {
            // console.log('router.navigate([\'/session-out\', \'expired\'])');
            this.router.navigate(['/session-out', 'expired']);
          }, 500);
        }
      });

      // /* adding 1 extra second to dialog box to see progress bar 100 completion in that dialog box. */
      // const timeout = (this.waitingTime + 1) * 1000;
      // /* the idleDialogBox will close after the waitingTime's up */
      // setTimeout(() => {
      //   console.log('dialog.closeAll()');
      //   this.dialog.closeAll(); /* closes all the opened MatDialog */
      //   console.log('idleDialogRef.close()');
      //   this.idleDialogRef.close();
      // }, timeout);
    });
  }
}
