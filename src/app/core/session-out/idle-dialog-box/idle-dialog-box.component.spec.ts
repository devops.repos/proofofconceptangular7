import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdleDialogBoxComponent } from './idle-dialog-box.component';

describe('IdleDialogBoxComponent', () => {
  let component: IdleDialogBoxComponent;
  let fixture: ComponentFixture<IdleDialogBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdleDialogBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdleDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
