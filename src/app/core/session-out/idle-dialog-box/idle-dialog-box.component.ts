import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { interval, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-idle-dialog-box',
  templateUrl: './idle-dialog-box.component.html',
  styleUrls: ['./idle-dialog-box.component.scss']
})
export class IdleDialogBoxComponent implements OnInit {

  sub: Subscription;
  progressbarValue = 0;
  curSec = 0;
  waitingTime: number; // seconds


  constructor(
    public dialogRef: MatDialogRef<IdleDialogBoxComponent>,
    // @Inject(MAT_DIALOG_DATA) public data: any,
    // private router: Router
  ) {
    // this.waitingTime = data.waitingTime;
    this.waitingTime = environment.idleDialogWaitTime;
    // console.log('idle-dialog-box.component.ts : ');
    // console.log('-> waitingTime: ', this.waitingTime);
  }

  ngOnInit() {
    setTimeout(() => {
      this.dialogRef.close('idle');
    }, (environment.idleDialogWaitTime * 1000));

    /* startTimer is to sync the waitingTimer with the progressbar. */
    // this.startTimer(this.waitingTime);
  }

  startTimer(seconds: number) {
    const time = seconds;
    /* 1sec = 1000 ms
     * for 5 minutes (300s) = 300,000ms
     * dividing 300000ms into equal 100 parts (i.e. 3000) gives the interval to set for the progressbar
     */
    const timer$ = interval(seconds * 10); /* milliseconds */

    this.sub = timer$.subscribe(sec => {
      // console.log('-> sec: ', sec);
      this.progressbarValue = sec;
      // console.log('-> progressbarValue: ', this.progressbarValue);
      this.curSec = sec;

      if (this.curSec === 100) {
        // console.log('sub.unsubscribe()');
        this.sub.unsubscribe();
        this.dialogRef.close('idle');
      }
    });
  }

  onContinueClick(): void {
    /* Destroying the progressbar counter on user click. */
    //this.sub.unsubscribe();
    this.dialogRef.close('userClickedContinueButton');
  }

  onLogoutClick() {
    /* Destroying the progressbar counter on user click. */
    //this.sub.unsubscribe();
    this.dialogRef.close('userClickedLogoutButton');
  }

}
