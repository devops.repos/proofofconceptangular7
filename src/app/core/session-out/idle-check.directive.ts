import { Directive, HostListener, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { IdleDialogBoxComponent } from './idle-dialog-box/idle-dialog-box.component';
import { finalize } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Directive({
  selector: '[appIdleCheck]'
})
export class IdleCheckDirective {
  idleTime: number = 900;  // In seconds (900sec = 15 Minutes)
  userActivity;
  userInactive: Subject<any> = new Subject();

  waitingTime = environment.idleDialogWaitTime; /* 300 seconds = 5 minutes */
  idleDialogRef: MatDialogRef<IdleDialogBoxComponent>;

  constructor(
    private router: Router,
    private dialog: MatDialog,
  ) { }

  // @HostListener('mouseenter')
  // mouseenter() {
  //   // console.log("OMG It's a Mouse!!!");
  //   this.setTimeout();
  // }

  @HostListener('mouseover')
  mouseover() {
    // console.log("OMG It's still here!!!");
    if (!this.idleDialogRef) {
      this.setTimeout();
    }
  }

  @HostListener('mouseout')
  mouseout() {
    // console.log('Phew thank god it left!');
    if (!this.idleDialogRef) {
      this.setTimeout();
    }
  }

  // @HostListener('mouseout')
  // isItSafe() {
  //   console.log("It really left rigth");
  // }
  // @HostListener('mouseout')
  // messageInput() {
  //   console.log(`The message input is ${this.message}`);
  // }

  @HostListener('keydown', ['$event'])
  onKeyDown(e: KeyboardEvent) {
    // console.log('Key Pressed');
    if (!this.idleDialogRef) {
      this.setTimeout();
    }
  }

  setTimeout() {
    this.resetTimer();
    this.userActivity = setTimeout(() => {
      /*
        * Opening a Dialog Popup to display session timeout warning
        * with 10 minutes of count down timer.
        */
      this.idleDialogRef = this.dialog.open(IdleDialogBoxComponent, {
        data: { waitingTime: this.waitingTime },
        panelClass: 'transparent',
        disableClose: true
      });
      this.idleDialogRef.afterClosed().pipe(
        finalize(() => this.idleDialogRef = undefined)
      ).subscribe(result => {
        if (result === 'userClickedContinueButton') {
          // console.log('Idle Timer reset.');
          this.resetTimer();
        } else if (result === 'userClickedLogoutButton') {
          // console.log('Idle Logout clicked');
          /* Redirect to session-out page. */
          this.resetTimer();
          this.router.navigate(['/session-out', 'logout']);
        } else {
          // console.log('Idle session expired.');
          /* Redirect to session-out page. */
          this.resetTimer();
          this.router.navigate(['/session-out', 'expired']);
        }
      });

      /* adding 1 extra second to dialog box to see progress bar 100 completion in that dialog box. */
      const timeout = (this.waitingTime + 1) * 1000;
      /* the idleDialogBox will close after the waitingTime's up */
      setTimeout(() => {
        this.dialog.closeAll(); /* closes all the opened MatDialog */
        this.idleDialogRef.close();
      }, timeout);
    }, (this.idleTime * 1000));
  }

  resetTimer() {
    clearTimeout(this.userActivity);
  }
}
