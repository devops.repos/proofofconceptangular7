import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ThemeService } from 'src/app/services/helper-services/theme.service';

@Component({
  selector: 'app-session-out',
  templateUrl: './session-out.component.html',
  styleUrls: ['./session-out.component.scss']
})
export class SessionOutComponent implements OnInit {
  urlParameter: string;

  selectedTheme: string;
  themeSub: Subscription;

  constructor(
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private theme: ThemeService
  ) {}

  ngOnInit() {
    /* Setting the session-out as the requesting url to re-authenticate the user
     * and redirect to dashboard
     */
    if (this.router.url) {
      this.authService.setRequestingSource(this.router.url);
    }

    this.activatedRoute.paramMap.subscribe(params => {
      this.urlParameter = params.get('value');
      // console.log('urlParameter: ', this.urlParameter);
      this.authService.logout();
    });

    /* Getting the selected theme from the theme service */
    this.themeSub = this.theme.getSelectedTheme().subscribe((res: string) => {
      this.selectedTheme = res;
    });
  }

  onLoginClick() {
    // console.log('-> authenticating the user');
    this.authService.authUser();
  }
}
