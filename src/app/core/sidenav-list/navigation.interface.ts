export interface INavigationItems {
  navigationItemsOnly: INavigationItemsOnly[];
  navigationItemsWithFunctionality: INavigationItemsWithFunctionality[];
}

export interface INavigationItemsOnly {
  systemModuleType: number;
  systemModuleTypeDescription: string;
  navigationItemID: number;
  navigationItemName: string;
  iconName: string;
  url: string;
  isDashboardItem: boolean;
  isSidenavItem: boolean;
  isAdministrationItem: boolean;
  itemOrder: number;
}

export interface INavigationItemsWithFunctionality {
  systemModuleType: number;
  systemModuleTypeDescription: string;
  navigationItemID: number;
  navigationItemName: string;
  functionID: number;
  functionality: string;
  iconName: string;
  url: string;
  isDashboardItem: boolean;
  isSidenavItem: boolean;
  isAdministrationItem: boolean;
  itemOrder: number;
}
