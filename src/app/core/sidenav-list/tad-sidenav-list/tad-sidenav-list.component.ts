import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NavService } from '../nav.service';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { IVCSTadSidenavStatus } from 'src/app/pact-modules/vacancyControlSystem/tad-submission/tad-interface.model';
import { TadService } from 'src/app/pact-modules/vacancyControlSystem/tad-submission/tad.service';

@Component({
  selector: 'app-tad-sidenav-list',
  templateUrl: './tad-sidenav-list.component.html',
  styleUrls: ['./tad-sidenav-list.component.scss']
})
export class TadSidenavListComponent implements OnInit, OnDestroy {
  @Input() navCollapsed = false;
  @Input() mobileSize: MediaQueryList;

  currentUrl;

  // tadSidenavStatus: IVCSTadSidenavStatus = {
  //   isUnitRosterComplete: 0,
  //   isReferralRosterComplete: 0,
  //   isTenantRosterComplete: 0,
  //   isTadHpComplete: 0,
  // }
  isUnitRosterComplete = false;
  isReferralRosterComplete = false;
  isTenantRosterComplete = false;
  isTadHpComplete = false;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  currentUrlSub: Subscription;
  // tadSidenavCompleteStatusSub: Subscription;
  isUnitRosterCompleteSub: Subscription;
  isReferralRosterCompleteSub: Subscription;
  isTenantRosterCompleteSub: Subscription;
  isTadHpCompleteSub: Subscription;

  constructor(
    private navService: NavService,
    private tadService: TadService
  ) { }

  ngOnInit() {
    this.currentUrlSub = this.navService.getCurrentUrl().subscribe((url: string) => {
      this.currentUrl = url;
      const activeSys = url.split('/');
      if (activeSys[1] === 'dashboard') {

        // if (this.tadSidenavCompleteStatusSub) {
        //   this.tadSidenavCompleteStatusSub.unsubscribe();
        // }
        if (this.isUnitRosterCompleteSub) {
          this.isUnitRosterCompleteSub.unsubscribe();
        }
        if (this.isReferralRosterCompleteSub) {
          this.isReferralRosterCompleteSub.unsubscribe();
        }
        if (this.isTenantRosterCompleteSub) {
          this.isTenantRosterCompleteSub.unsubscribe();
        }
        if (this.isTadHpCompleteSub) {
          this.isTadHpCompleteSub.unsubscribe();
        }
      } else if (activeSys[1] === 'vcs') {
        if (activeSys.length > 3) {
          if (activeSys[2] === 'tad') {
            /* fetch the selected tad and check the completed status */
            // this.tadSidenavCompleteStatusSub = this.tadService.getTadSidenavStatus().subscribe((rslt) => {
            //   if (rslt) {
            //     this.tadSidenavStatus = rslt;
            //   }
            // });
            this.isUnitRosterCompleteSub = this.tadService.getIsUnitRosterComplete().subscribe((rslt) => {
                this.isUnitRosterComplete = rslt;
            });
            this.isReferralRosterCompleteSub = this.tadService.getIsReferralRosterComplete().subscribe((rslt) => {
                this.isReferralRosterComplete = rslt;
            });
            this.isTenantRosterCompleteSub = this.tadService.getIsTenantRosterComplete().subscribe((rslt) => {
                this.isTenantRosterComplete = rslt;
            });
            this.isTadHpCompleteSub = this.tadService.getIsTadHpComplete().subscribe((rslt) => {
                this.isTadHpComplete = rslt;
            });
          }
        }
      }
    });
  }

  ngOnDestroy() {
    if (this.currentUrlSub) {
      this.currentUrlSub.unsubscribe();
    }
    // if (this.tadSidenavCompleteStatusSub) {
    //   this.tadSidenavCompleteStatusSub.unsubscribe();
    // }

    if (this.isUnitRosterCompleteSub) {
      this.isUnitRosterCompleteSub.unsubscribe();
    }
    if (this.isReferralRosterCompleteSub) {
      this.isReferralRosterCompleteSub.unsubscribe();
    }
    if (this.isTenantRosterCompleteSub) {
      this.isTenantRosterCompleteSub.unsubscribe();
    }
    if (this.isTadHpCompleteSub) {
      this.isTadHpCompleteSub.unsubscribe();
    }
  }
}
