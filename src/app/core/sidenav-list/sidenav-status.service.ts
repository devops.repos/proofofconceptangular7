import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { SidenavStatus } from './sidenav-status.model';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ClientApplicationService,
  ClientApplication
} from 'src/app/services/helper-services/client-application.service';

const pages = [
  'consent-search',
  'demographics',
  'housing-homeless',
  'clinical-assessment',
  'adls',
  'medications-providers-and-hospitalization',
  'trauma-child-welfare',
  'symptoms-substance-use',
  'housing-preferences',
  'psychiatric-psychosocial-mhr',
  'documents',
  'summary-transmit'
];

const tadPages = [
  'unit-roster',
  'referral-roster',
  'tenant-roster',
  'tad-hp'
]

@Injectable({
  providedIn: 'root'
})
export class SidenavStatusService implements OnDestroy {
  private pactApiURL = environment.pactApiUrl + 'SidenavStatus';

  defaultValue: SidenavStatus = {
    pactApplicationID: 0,
    isConsentComplete: 2,
    isDemographicComplete: 2,
    isHousingHomelessComplete: 2,
    isClinicalAssessmentComplete: 2,
    isADLComplete: 2,
    isMedicationsProvidersHospitalizationComplete: 2,
    isTraumaAndChildWelfareComplete: 2,
    isSymptomsSubstanceComplete: 2,
    isHousingPreferenceComplete: 2,
    isPsychiatricPsychoMHRComplete: 2,
    isComplete: 0
  };

  // private sidenavStatus = new BehaviorSubject<SidenavStatus>(this.defaultValue);
  // private sidenavStatus1: SidenavStatus;

  clientApplicationData: ClientApplication;
  selectedApplicationId = new BehaviorSubject<number>(0);

  previousApplicationId: number;

  private isNewApplication = false;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // sidenavStatusSub: Subscription;
  // clientApplicationDataSub: Subscription;
  // routeSub: Subscription;

  constructor(
    private httpClient: HttpClient,
    private clientApplicationService: ClientApplicationService
  ) { }

  setApplicationIDForSidenavStatus(applicationId: number) {
    this.selectedApplicationId.next(applicationId);
  }

  getApplicationIDForSidenavStatus() {
    return this.selectedApplicationId.asObservable();
  }

  getSidenavCompleteStatus(applicationID: number) {
    /* set the client info */
    this.httpClient.post(this.pactApiURL + '/GetClientApplication/', JSON.stringify(applicationID), this.httpOptions).subscribe(result => {
      const data = result as ClientApplication;
      this.clientApplicationData = data;
      this.clientApplicationService.setClientApplicationData(data);
    });
    return this.httpClient.post<SidenavStatus>(this.pactApiURL + '/GetSidenavStatus/', JSON.stringify(applicationID), this.httpOptions);
  }

  // setSidenavCompleteStatus(val: SidenavStatus) {
  //   this.sidenavStatus.next(val);
  //   // this.sidenavStatus1 = val;
  // }
  // readSidnavStatus() {
  //   // var promise = new Promise((resolve, reject) => {
  //   //   setTimeout(() => {
  //   //     if (this.sidenavStatus1) {
  //   //       resolve(this.sidenavStatus1);
  //   //     } else {
  //   //       reject();
  //   //     }
  //   //   }, 1000);
  //   //   // if (this.sidenavStatus) {
  //   //   //   resolve(this.sidenavStatus.asObservable());
  //   //   // } else {
  //   //   //   reject();
  //   //   // }
  //   // });
  //   // return promise;
  //   // console.log(this.sidenavStatus.getValue());
  //   return this.sidenavStatus.asObservable();
  // }




  /* Getting the Complete/ InComplete status of sidenav List */
  // getSidenavStatus() {
  //   return this.sidenavStatus.asObservable();
  // }

  // setSidenavStatus(applicationID: number) {
  //   /* tslint:disable-next-line: max-line-length */
  //   this.httpClient.post<SidenavStatus>(this.pactApiURL + '/GetSidenavStatus/', JSON.stringify(applicationID), this.httpOptions).subscribe(res => {
  //     this.sidenavStatus.next(res);
  //   });
  //   // const promise = new Promise((resolve, reject) => {
  //   //   resolve(
  //   //   this.httpClient.get<SidenavStatus>(this.pactApiURL + '/GetSidenavStatus/' + applicationID).subscribe(res => {
  //   //     this.sidenavStatus.next(res);
  //   //   })
  //   //   );
  //   // });
  //   // return promise;
  // }

  /* Setting the sidenav Status for the application with the given ID from URL router parameter
   * This is the case for page refresh or reload
   */
  // _sidenavStatusReload(route: ActivatedRoute) {
  //   /* Setting the Requesting Url in authService for auto re-navigation to same page after re-authentication */
  //   this.authService.setRequestingSource(route.snapshot['_routerState'].url);
  //   // console.log(this.authService.getRequestingSource());

  //   /* Getting the ApplicationID from the URL of currently loaded application page */
  //   this.routeSub = route.paramMap.subscribe(params => {
  //     const selectedApplicationID = params.get('applicationID');
  //     if (selectedApplicationID == null) {
  //       return;
  //     } else {
  //       const appid = parseInt(selectedApplicationID);
  //       if (isNaN(appid)) {
  //         throw new Error('Invalid ApplicationID!!');
  //       } else if (appid > 0 && appid < 2147483647) {
  //         this.selectedApplicationId.next(appid);
  //         /* Because of HashLocationStrategy when we change the applicationID in URL the sidenav get's updated
  //          * But the content under <router-outlet> doesn't update,
  //          * so below condition checks for the applicationID change and sync the sidenav statuses with the content
  //          */
  //         if (
  //           this.previousApplicationId &&
  //           this.previousApplicationId !== appid
  //         ) {
  //           // alert('ApplicationID change triggered');
  //           //window.location.reload();
  //         } else {
  //           /* Set the Sidenav Status for the provided applicationID in URL */
  //           this.setSidenavStatus(appid);
  //         }
  //         this.previousApplicationId = appid;

  //         this.clientApplicationService.getClientApplicationData().subscribe(res => {
  //           if (res == null) {
  //             /* The client Info is not set */
  //             /* tslint:disable-next-line: max-line-length */
  //             this.httpClient
  //               .post(
  //                 this.pactApiURL + '/GetClientApplication/',
  //                 JSON.stringify(appid),
  //                 this.httpOptions
  //               )
  //               .subscribe(result => {
  //                 const data = result as ClientApplication;
  //                 this.clientApplicationData = data;
  //                 if (data) {
  //                   this.clientApplicationService.setClientApplicationData(data);
  //                 }
  //               });
  //           }
  //         });
  //       } else {
  //         throw new Error('Invalid ApplicationID!!');
  //       }
  //     }
  //   });
  // }

  /** Setting the isNewApplication Flag to true
   * which is going to be used to set the sidenav Tree structure
   * either for NewApplication or PendingApplication
   * Currently this flag value is being used below on the functions routeToNextPage() and routeToPerviousPage()
   */
  setIsNewApplicationFlag() {
    this.isNewApplication = true;
    return true;
  }

  routeToNextPage(router: Router, route: ActivatedRoute) {
    route.paramMap.subscribe(params => {
      const appID = +params.get('applicationID');
      const activeSys = router.url.split('/');
      if (this.isNewApplication) {
        /* New Application Tree Structure in Sidenav */
        for (const val of pages) {
          if (activeSys.includes(val)) {
            let newAppNextPageIndex = pages.indexOf(val) + 1;
            if (newAppNextPageIndex >= pages.length) {
              newAppNextPageIndex = 0;
            }
            router.navigate(['/shs/newApp/' + pages[newAppNextPageIndex] + '/' + appID]);
          }
        }
      } else {
        /* Pending Application Tree Structure in Sidenav */
        for (const val of pages) {
          if (activeSys.includes(val)) {
            let pendingAppnextPageIndex = pages.indexOf(val) + 1;
            if (pendingAppnextPageIndex >= pages.length) {
              pendingAppnextPageIndex = 0;
            }
            router.navigate(['/shs/pendingApp/' + pages[pendingAppnextPageIndex] + '/' + appID]);
          }
        }
      }
    });
  }

  routeToPreviousPage(router: Router, route: ActivatedRoute) {
    route.paramMap.subscribe(params => {
      const appID = +params.get('applicationID');
      const activeSys = router.url.split('/');
      if (this.isNewApplication) {
        /* New Application Tree Structure in Sidenav */
        for (const val of pages) {
          if (activeSys.includes(val)) {
            let newAppPreviousPageIndex = pages.indexOf(val) - 1;
            if (newAppPreviousPageIndex < 0) {
              newAppPreviousPageIndex = pages.length - 1;
            }
            router.navigate(['/shs/newApp/' + pages[newAppPreviousPageIndex] + '/' + appID]);
          }
        }
      } else {
        /* Pending Application Tree Structure in Sidenav */
        for (const val of pages) {
          if (activeSys.includes(val)) {
            let pendingAppPreviousPageIndex = pages.indexOf(val) - 1;
            if (pendingAppPreviousPageIndex < 0) {
              pendingAppPreviousPageIndex = pages.length - 1;
            }
            router.navigate(['/shs/pendingApp/' + pages[pendingAppPreviousPageIndex] + '/' + appID]);
          }
        }
      }
    });
  }

  routeToTadNextPage(router: Router, route: ActivatedRoute) {
    route.paramMap.subscribe(params => {
      const activeSys = router.url.split('/');
      for (const val of tadPages) {
        if (activeSys.includes(val)) {
          let nextTadPageIndex = tadPages.indexOf(val) + 1;
          if (nextTadPageIndex >= tadPages.length) {
            nextTadPageIndex = 0;
          }
          router.navigate(['/vcs/tad/' + tadPages[nextTadPageIndex]]);
        }
      }
    });
  }

  routeToTadPreviousPage(router: Router, route: ActivatedRoute) {
    route.paramMap.subscribe(params => {
      const activeSys = router.url.split('/');
      for (const val of tadPages) {
        if (activeSys.includes(val)) {
          let previousTadPageIndex = tadPages.indexOf(val) - 1;
          if (previousTadPageIndex < 0) {
            previousTadPageIndex = tadPages.length - 1;
          }
          router.navigate(['/vcs/tad/' + tadPages[previousTadPageIndex]]);
        }
      }
    });
  }

  ngOnDestroy() {
    // if (this.clientApplicationDataSub) {
    //   this.clientApplicationDataSub.unsubscribe();
    // }
    // if (this.routeSub) {
    //   this.routeSub.unsubscribe();
    // }
    // if (this.sidenavStatusSub) {
    //   this.sidenavStatusSub.unsubscribe();
    // }
  }
}
