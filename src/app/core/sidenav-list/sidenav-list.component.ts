import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';
import {
  UserAgencyType,
  UserSiteType,
  UserRole,
  appStatusColor
} from '../../models/pact-enums.enum';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { Router, NavigationStart } from '@angular/router';
import { ReferralPackageNavService } from './referral-package-nav.service';
import { ConsentService } from 'src/app/pact-modules/supportiveHousingSystem/consent-search/consent-search.service';
import { environment } from 'src/environments/environment';
import { TadService } from 'src/app/pact-modules/vacancyControlSystem/tad-submission/tad.service';
import { TadSite } from 'src/app/pact-modules/vacancyControlSystem/tad/tad.model';
import { PlacementsVerificationService } from 'src/app/pact-modules/vacancyControlSystem/placements-awaiting-verification/placements-verification.service';
import { ReferralRosterService } from 'src/app/pact-modules/vacancyControlSystem/referral-roster/referral-roster.service';
import { TenantRosterService } from 'src/app/pact-modules/vacancyControlSystem/tenant-roster/tenant-roster.service';
import { INavigationItems, INavigationItemsOnly, INavigationItemsWithFunctionality } from './navigation.interface';
import { NavigationService } from './navigation.service';
// import { PlacementsVerificationS./navigation.servicevacancyControlSystem/placements-awaiting-verification/placements-verification.service';
import { CoCService } from 'src/app/pact-modules/vacancyControlSystem/coc-referral-queue/coc-referral-queue.service';
import { VCSCoCSiteList } from 'src/app/pact-modules/vacancyControlSystem/coc-referral-queue/coc-referral-queue.model';
import { IVCSPlacementClientSelected } from 'src/app/pact-modules/vacancyControlSystem/placements-awaiting-verification/placements-verification-interface.model';
import { C2vService } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v.service';
import { V2cService } from 'src/app/pact-modules/vacancyControlSystem/vacancy-listing/v2c.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Tutorial } from 'src/app/shared/tutorial/tutorial.model';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit, OnDestroy {
  userAgencyType = UserAgencyType;
  currentUserAgencyType = UserAgencyType.HousingProvider;
  currentUserAgencyTypeSub: Subscription;

  navigatonItems: INavigationItems;
  navigationItemsOnly: INavigationItemsOnly[] = [];  // only Navigation Items, no functionalities
  navigationItemsWithFunctionality: INavigationItemsWithFunctionality[] = []; // Normalize list, will have duplication Navigation items with differrent functionalities
  capsAndApplicationItems: INavigationItemsOnly[] = [];
  vcsItems: INavigationItemsOnly[] = [];
  detItems: INavigationItemsOnly[] = [];
  reportItems: INavigationItemsOnly[] = [];
  adminItems: INavigationItemsOnly[] = [];

  userData: AuthData; // Current user information
  userDataSub: Subscription;
  getUserSiteSub: Subscription;
  // UserSiteType flags
  is_SH_RA = false;
  is_SH_PE = false;
  is_SH_HP = false;
  is_CAS = false;
  is_ML_DHS_Liasian = false;
  is_ML_Team = false;
  is_ML_HP = false;
  is_ML_IREA = false;
  is_SH_HPD = false; // for CoC side menu
  is_SH_PE_oldValue = false;
  // UserRoles flag
  is_STAFF = false;
  is_SUPERVISOR = false;
  is_MANAGER = false;
  is_SYS_ADMIN = false;

  currentUrl = '/';
  currentUrlSub: Subscription;
  shsExpanded = false;
  newAppExpanded = false;
  pendingAppExpanded = false;
  vcsExpanded = false;
  capsExpanded = false;
  reportsExpanded = false;
  adminExpanded = false;
  informationExpanded = false;
  clientCaseFolderExpanded = false;
  adminVisible: boolean = true;

  //Make Referral Tabs
  homelessTab: number = 0;
  trackingTab: number = null;
  preferenceTab: number = null;
  summaryTab: number = null;
  makeReferralTabStatusSub: Subscription;

  currentSys: string;
  newApp = false;
  pendingApp = false;
  referralPackage = false;
  cocDetails = false;
  clientPlacementsHistory = false;
  clientSourceType = 0;
  // isClientPlacementFormOpen = false;
  // clientPlacementNumber = 0;
  tadSubmission = false;
  selectedHpSite: TadSite;

  navCollapsed = false;

  @Output() sidenavClose = new EventEmitter();

  mobileSize: MediaQueryList;
  private mobileQueryListener: () => void;

  videoLink: Tutorial;
  pageName = 'nav-tutorial';

  navCollapsedSub: Subscription;
  routeSub: Subscription;
  // isClientPlacementFormOpenSub: Subscription;
  // clientPlacementNumberSub: Subscription;
  tadSelectedHpSiteIDSub: Subscription;
  navigationItemsSub: Subscription;
  clientSourceTypeSub: Subscription;

  constructor(
    private router: Router,
    private navService: NavService,
    private userService: UserService,
    private commonService: CommonService,
    private referralPackageNavService: ReferralPackageNavService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private consentService: ConsentService,
    private tadService: TadService,
    private placementVerificationService: PlacementsVerificationService,
    private referralRosterService: ReferralRosterService,
    private tenantRosterService: TenantRosterService,
    private navigationService: NavigationService,
    private cocService: CoCService,
    private c2vService: C2vService,
    private v2cService: V2cService,
    private authService: AuthService
  ) {
    this.mobileSize = media.matchMedia('(max-width: 959px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileSize.addListener(this.mobileQueryListener);
  }

  ngOnInit() {
    /* Getting the CurrentUser Details from UserService */
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        // console.log('userData: ', this.userData);
        /* Setting UserSiteType Flags */
        this.is_SH_RA = this.commonService._doesValueExistInJson(
          this.userData.siteCategoryType,
          UserSiteType.SH_RA
        );
        this.is_SH_PE = this.commonService._doesValueExistInJson(
          this.userData.siteCategoryType,
          UserSiteType.SH_PE
        );
        this.is_SH_HP = this.commonService._doesValueExistInJson(
          this.userData.siteCategoryType,
          UserSiteType.SH_HP
        );
        this.is_CAS = this.commonService._doesValueExistInJson(
          this.userData.siteCategoryType,
          UserSiteType.CAS
        );
        this.is_ML_DHS_Liasian = this.commonService._doesValueExistInJson(
          this.userData.siteCategoryType,
          UserSiteType.ML_DHS_Liasian
        );
        this.is_ML_Team = this.commonService._doesValueExistInJson(
          this.userData.siteCategoryType,
          UserSiteType.ML_Team
        );
        this.is_ML_HP = this.commonService._doesValueExistInJson(
          this.userData.siteCategoryType,
          UserSiteType.ML_HP
        );
        this.is_ML_IREA = this.commonService._doesValueExistInJson(
          this.userData.siteCategoryType,
          UserSiteType.ML_IREA
        );
        this.is_SH_PE_oldValue = this.is_SH_PE;
        /* Setting UserRole Flags */
        if (this.userData.roleId === UserRole.STAFF) {
          this.is_STAFF = true;
          this.is_SUPERVISOR = false;
          this.is_MANAGER = false;
          this.is_SYS_ADMIN = false;
        } else if (this.userData.roleId === UserRole.SUPERVISOR) {
          this.is_STAFF = false;
          this.is_SUPERVISOR = true;
          this.is_MANAGER = false;
          this.is_SYS_ADMIN = false;
        } else if (this.userData.roleId === UserRole.MANAGER) {
          this.is_STAFF = false;
          this.is_SUPERVISOR = false;
          this.is_MANAGER = true;
          this.is_SYS_ADMIN = false;
        } else if (this.userData.roleId === UserRole.SYS_ADMIN) {
          this.is_STAFF = false;
          this.is_SUPERVISOR = false;
          this.is_MANAGER = false;
          this.is_SYS_ADMIN = true;
        }
      }
      /* Nav Tutorial */
      if (this.pageName) {
        this.commonService.getVideoURL(this.pageName).subscribe(res => {
          if (res) {
            this.videoLink = res.body as Tutorial;
          }
        },
          error => console.error('Error!', error)
        );
      }

    });

    this.getNavigationItems();
    this.getCurrentUrl();

    /* Getting the sidenav collapsed status */
    this.navCollapsedSub = this.navService.getNavCollapsedStatus().subscribe(stat => (this.navCollapsed = stat));

    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event.navigationTrigger == 'popstate') {
          this.newApp = false;
          this.pendingApp = false;
          this.newAppExpanded = false;
          this.pendingAppExpanded = false;
          this.referralPackage = false;
          this.cocDetails = false;
        }
      }
    });

    this.makeReferralTabStatusSub = this.referralPackageNavService.getCurrentHomelessInformationTabIndex().subscribe(res => {
      this.homelessTab = res;
    });
    this.makeReferralTabStatusSub = this.referralPackageNavService.getCurrentReferralReadyTrackingTabIndex().subscribe(res => {
      this.trackingTab = res;
    });
    this.makeReferralTabStatusSub = this.referralPackageNavService.getCurrentHousingPreferenceTabIndex().subscribe(res => {
      this.preferenceTab = res;
    });
    this.makeReferralTabStatusSub = this.referralPackageNavService.getCurrentReferralPlacementSummaryTabIndex().subscribe(res => {
      this.summaryTab = res;
    });
  }

  /* Tutorial Video Open */
  openVideo() {
    if (this.videoLink && this.videoLink.url) {
      this.commonService.OpenWindow(this.videoLink.url);
    }
    this.onSidenavClose();
  }

  getNavigationItems() {
    /* Get NavigationItems for Sidenav */
    this.navigationItemsSub = this.navigationService.getNavigationItems().subscribe((res: INavigationItems) => {
      if (res) {
        this.navigatonItems = res;
        this.navigationItemsOnly = res.navigationItemsOnly;
        this.navigationItemsWithFunctionality = res.navigationItemsWithFunctionality;
        this.capsAndApplicationItems = this.navigationItemsOnly.filter(d => (d.systemModuleType === 766 || d.systemModuleType === 807) && d.isSidenavItem == true && d.isAdministrationItem == false);
        this.vcsItems = this.navigationItemsOnly.filter(d => d.systemModuleType === 765 && d.isSidenavItem == true && d.isAdministrationItem == false);
        this.detItems = this.navigationItemsOnly.filter(d => d.systemModuleType === 767 && d.isSidenavItem == true && d.isAdministrationItem == false);
        this.reportItems = this.navigationItemsOnly.filter(d => d.systemModuleType === 936 && d.isSidenavItem == true);
        this.adminItems = this.navigationItemsOnly.filter(d => d.isSidenavItem == true && d.isAdministrationItem == true);
        // console.log('capsAndApplicationItems: ', this.capsAndApplicationItems);
        // console.log('vcsItems: ', this.vcsItems);
        // console.log('detItems: ', this.detItems);
        // console.log('adminItems: ', this.adminItems);
      }
    });
  }

  getCurrentUrl() {
    /* Getting the currently active url and setting the required flags for sidenav */
    this.currentUrlSub = this.navService.getCurrentUrl().subscribe((url: string) => {
      this.currentUrl = url;
      const activeSys = url.split('/');
      // console.log('activeSys: ', activeSys);
      if (activeSys[1] === 'dashboard') {
        /* Reset all the flags when landing back to Dashboard */
        this.referralRosterService.setCurrentRRTabIndex(0);
        this.referralRosterService.setRRAgencySelected(0);
        this.referralRosterService.setRRSiteSelected(0);

        this.tenantRosterService.setCurrentTRTabIndex(0);
        this.tenantRosterService.setTRAgencySelected(0);
        this.tenantRosterService.setTRSiteSelected(0);

        this.referralRosterService.setisRoutedFromRR(false);
        this.referralRosterService.setCurrentRRTabNo(0);

        this.placementVerificationService.setIsClientPlacements(false);
        this.placementVerificationService.setIsDeletePlacement(false);

        this.tadService.setIsTadWorkflow(false);
        this.tadService.setIsTadVerificationWorkflow(false);

        /* Reset CLAP Filters, pageNumber, rowsCounter */
        this.c2vService.setCLAPFilterModel(null);
        this.c2vService.setCLAPPageNumber(0);
        this.c2vService.setCLAPNoOfRowCount(10);

        /* Reset the User Selected Filters, PageNumber and RowsCounter of C2V Handshake screen */
        this.c2vService.setC2VHandShakeFilterModel(null);
        this.c2vService.setC2VHandShakePageNumber(0);
        this.c2vService.setC2VHandShakeNoOfRowCount(10);

        /* Reset VacancyListing Filters, pageNumber, rowsCounter */
        this.v2cService.setVacancyListingFilterModel(null);
        this.v2cService.setVacancyListingPageNumber(0);
        this.v2cService.setVacancyListingNoOfRowCount(10);

        /* Reset the User Selected Filters, PageNumber and RowsCounter of V2C Handshake screen */
        this.v2cService.setV2CHandShakeFilterModel(null);
        this.v2cService.setV2CHandShakePageNumber(0);
        this.v2cService.setV2CHandShakeNoOfRowCount(10);

        /* Set the dropdown Type as RA+HP (Application Transmitted By) */
        this.referralRosterService.setRRAgencyDropdownTypeSelected(1);

      } else if (activeSys[1] === 'caps') {
        this.currentSys = 'caps';
        this.capsExpanded = true;
      } else if (activeSys[1] === 'shs') {
        this.currentSys = 'shs';
        this.shsExpanded = true;
        this.newApp = false;
        this.pendingApp = false;
        if (activeSys[2] === 'transmittedApps') {
          this.currentSys = 'shs';
          this.shsExpanded = true;
          this.newApp = false;
          this.pendingApp = false;
          this.newAppExpanded = false;
          this.pendingAppExpanded = false;
        }
        if (activeSys.length > 4) {
          if (activeSys[2] === 'newApp') {
            this.consentService.getIsTransmittedApplication().subscribe((flag: boolean) => {
              if (!flag) {
                this.newApp = true;
                this.pendingApp = false;
                this.newAppExpanded = true;
                this.pendingAppExpanded = false;
              } else {
                this.newApp = false;
                this.pendingApp = false;
                this.newAppExpanded = false;
                this.pendingAppExpanded = false;
              }
            });
          } else if (activeSys[2] === 'pendingApp') {
            this.newApp = false;
            this.pendingApp = true;
            this.newAppExpanded = false;
            this.pendingAppExpanded = true;
          }
        }
      } else if (activeSys[1] === 'ds') {
        this.currentSys = 'ds';
        if (activeSys.length > 2) {
          if (
            activeSys[2] === "match-sources" ||
            activeSys[2] === "application" ||
            activeSys[2] === "client-case-folder" ||
            activeSys[2] === "outcome" ||
            activeSys[2] === "clinical-review" ||
            activeSys[2] === "homeless-review" ||
            activeSys[2] === "medicaid-prioritization" ||
            activeSys[2] === "vulnerability-assessment" ||
            activeSys[2] === "determination-summary" ||
            activeSys[2] === "sign-off-follow-up") {
            this.adminVisible = false;
            this.adminExpanded = false;
          }
          else {
            this.adminVisible = true;
          }
        }
      } else if (activeSys[1] === 'vcs') {
        this.currentSys = 'vcs';
        this.vcsExpanded = true;
        if (activeSys.length > 2) {
          if (activeSys[2] === 'referral-package') {
            this.referralPackage = true;
            this.vcsExpanded = false;
          } else {
            this.referralPackage = false;
            this.vcsExpanded = true;
          }
          if (activeSys[2] === 'c2v-scheduler' || activeSys[2] === 'c2v-referral-handshake-success') {
          } else {
            this.vcsExpanded = true;
            /* To reset the workflow of editing interview Date/time from Referral Roster, if user clicks any sidenav */
            this.referralRosterService.setisRoutedFromRR(false);
            this.referralRosterService.setCurrentRRTabNo(0);
          }

          if (activeSys[2] === 'agency-site-maintenance' || activeSys[2] === 'site-profile' || activeSys[2] === 'createagencysite'
            || activeSys[2] === 'createsite' || activeSys[2] === 'transfersite') {
            this.vcsExpanded = false;
            this.adminExpanded = true;
          } else {
            this.vcsExpanded = true;
            this.adminExpanded = false;
          }

          if (activeSys[2] === 'coc-referral-queue' && activeSys[3]) {
            this.vcsExpanded = false;
            this.cocDetails = true;

            /*Check if user is HPD */
            if (this.is_SH_PE && this.userData.agencyNo == '2092') {
              this.getUserSiteSub = this.cocService.getUserSite(this.userData.optionUserId).subscribe(res => {
                var peSiteList = res as VCSCoCSiteList[]
                peSiteList.forEach(item => {
                  if (item.siteNo == '999') {
                    this.is_SH_HPD = true;
                    this.is_SH_PE = false;
                  }
                })
              });
            }

          } else {
            this.vcsExpanded = true;
            this.cocDetails = false;
            this.is_SH_PE = this.is_SH_PE_oldValue;
            this.is_SH_HPD = false;
          }

          if (activeSys[2] === 'client-placements-history') {
            this.vcsExpanded = false;
            this.clientPlacementsHistory = true;
            this.clientSourceTypeSub = this.placementVerificationService.getPlacementClientSelected().subscribe((res: IVCSPlacementClientSelected) => {
              if (res) {
                this.clientSourceType = res.clientSourceType;
                // console.log(this.clientSourceType);
              }
            })
            // this.isClientPlacementFormOpenSub = this.placementVerificationService.getIsClientPlacementFormTabOpen().subscribe(res => {
            //   this.isClientPlacementFormOpen = res;
            //   this.clientPlacementNumberSub = this.placementVerificationService.getClientPlacementNumber().subscribe(num => {
            //     if (num > 0) {
            //       this.clientPlacementNumber = num;
            //     }
            //   });
            // });
          } else {
            this.vcsExpanded = true;
            this.clientPlacementsHistory = false;
            this.clientSourceType = 0;
            if (this.clientSourceTypeSub) {
              this.clientSourceTypeSub.unsubscribe();
            }
            // if (this.clientPlacementNumberSub) {
            //   this.clientPlacementNumberSub.unsubscribe();
            // }
            // if (this.isClientPlacementFormOpenSub) {
            //   this.isClientPlacementFormOpenSub.unsubscribe();
            // }
          }

          if (activeSys[2] === 'tad' && activeSys[3]) {
            this.vcsExpanded = false;
            this.tadSubmission = true;
            /* Get the HpSiteID selected in TAD page */
            this.tadSelectedHpSiteIDSub = this.tadService.getTadSelectedHpSiteID().subscribe((site: TadSite) => {
              if (site.siteID > 0) {
                this.selectedHpSite = site;
              }
            });
          } else {
            this.tadService.getIsTadWorkflow().subscribe((flag: boolean) => {
              if (flag) {
                // console.log('tadworkflow: ', flag);
                this.vcsExpanded = false;
                this.tadSubmission = flag;
                /* Get the HpSiteID selected in TAD page */
                this.tadSelectedHpSiteIDSub = this.tadService.getTadSelectedHpSiteID().subscribe((site: TadSite) => {
                  if (site.siteID > 0) {
                    this.selectedHpSite = site;
                  }
                });
              } else {
                this.vcsExpanded = true;
                this.tadSubmission = false;
                if (this.tadSelectedHpSiteIDSub) {
                  this.tadSelectedHpSiteIDSub.unsubscribe();
                }
              }
            });
          }
        }
      } else if (activeSys[1] === 'reports') {
        this.currentSys = 'reports';
        this.reportsExpanded = true;
      } else if (activeSys[1] === 'admin') {
        this.currentSys = 'admin';
        this.adminExpanded = true;
        /* To reset the workflow of editing interview Date/time from Referral Roster, if user clicks any sidenav */
        this.referralRosterService.setisRoutedFromRR(false);
        this.referralRosterService.setCurrentRRTabNo(0);
      } else if (activeSys[1] === 'information') {
        this.currentSys = 'information';
        this.informationExpanded = true;
      }
    });
  }

  /* Dashboard flags */
  onDashboardSelected() {
    this.shsExpanded = false;
    this.vcsExpanded = false;
    this.capsExpanded = false;
    this.reportsExpanded = false;
    this.newAppExpanded = false;
    this.pendingAppExpanded = false;
    this.adminExpanded = false;
    this.informationExpanded = false;
    this.clientCaseFolderExpanded = false;
    if (this.mobileSize.matches) {
      this.onSidenavClose();
    }
  }

  //#region Coordinated assessment flags
  onCapsSelected() {
    if (!this.navCollapsed) {
      this.setExpandedValue('caps', !this.capsExpanded);
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
      this.setExpandedValue('caps', true);
    }
  }

  onCapsSubMenuSelected(id: number) {
    if (!this.navCollapsed) {
      this.currentSys = 'caps';
    }
    if (this.mobileSize.matches) {
      this.onSidenavClose();
    }
    var isJuniperUser = false;
    this.authService.getIsJuniperUserFlag().subscribe(flag => {
      isJuniperUser = flag;
    });
    // window.location.href = environment.capsURL;
    window.location.href = environment.capsURL + "?isJuniperUser=" + isJuniperUser + "&Target=" + id;
  }
  //#endregion Coordinated assessment flags

  //#region Supportive housing flags
  onShsSelected() {
    if (!this.navCollapsed) {
      this.setExpandedValue('shs', !this.shsExpanded);
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
      this.setExpandedValue('shs', true);
    }
  }

  onShsSubMenuSelected(item: INavigationItemsOnly) {
    if (!this.navCollapsed) {
      this.currentSys = 'shs';
    }
    if (this.mobileSize.matches) {
      this.onSidenavClose();
    }
    // this.router.navigate([item.url]);
  }

  onNewAppSelected() {
    if (!this.navCollapsed) {
      this.setExpandedValue('newApp', !this.newAppExpanded);
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
      this.setExpandedValue('newApp', true);
    }
  }

  onPendingAppSelected() {
    if (!this.navCollapsed) {
      this.setExpandedValue('pendingApp', !this.pendingAppExpanded);
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
      this.setExpandedValue('pendingApp', true);
    }
  }

  //#endregion Supportive housing flags
  //#region Determination flags
  // onDsSelected() {
  //   if (!this.navCollapsed) {
  //     this.setExpandedValue('ds', !this.dsExpanded);
  //   } else if (this.navCollapsed) {
  //     this.navService.navCollapsedToggle(false);
  //     this.navService.setWidthSideBar(260);
  //     this.setExpandedValue('ds', true);
  //   }
  // }
  // onDsSubMenuSelected() {
  //   if (!this.navCollapsed) {
  //     this.currentSys = 'ds';
  //   }
  //   if (this.mobileSize.matches) {
  //     this.onSidenavClose();
  //   }
  // }
  //#endregion Determination flags

  //#region  Vacancy Control flags
  onVcsSelected() {
    if (!this.navCollapsed) {
      this.setExpandedValue('vcs', !this.vcsExpanded);
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
      this.setExpandedValue('vcs', true);
    }
  }

  onVcsSubMenuSelected(item: INavigationItemsOnly) {
    if (!this.navCollapsed) {
      this.currentSys = 'vcs';
    }
    if (this.mobileSize.matches) {
      this.onSidenavClose();
    }
    // this.router.navigate([item.url]);
  }

  onReportSubMenuSelected(item: INavigationItemsOnly) {
    if (!this.navCollapsed) {
      this.currentSys = 'reports';
    }
    if (this.mobileSize.matches) {
      this.onSidenavClose();
    }
    // this.router.navigate([item.url]);
  }

  onInformationSubMenuSelected() {
    if (!this.navCollapsed) {
      this.currentSys = 'information';
    }
    if (this.mobileSize.matches) {
      this.onSidenavClose();
    }
  }

  onAdministrationSubMenuSelected(item: INavigationItemsOnly) {
    if (this.mobileSize.matches) {
      this.onSidenavClose();
    }
    // this.router.navigate([item.url]);
  }

  onHomelessInformationTabSelected(index: number) {
    this.referralPackageNavService.setCurrentHomelessInformationTabIndex(index);
    this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null);
    this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
    this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null);
  }
  onReferralReadyTrackingTabSelected(index: number) {
    this.referralPackageNavService.setCurrentHomelessInformationTabIndex(null);
    this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(index);
    this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
    this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null);
  }
  onHousingPreferenceTabSelected(index: number) {
    this.referralPackageNavService.setCurrentHomelessInformationTabIndex(null);
    this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null);
    this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(index);
    this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null);
  }
  onReferralAndPlacementSummaryTabSelected(index: number) {
    this.referralPackageNavService.setCurrentHomelessInformationTabIndex(null);
    this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null);
    this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
    this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(index);
  }

  /*CoC Referral */
  onCoCReferralTabSelected(index: number) {
    this.referralPackageNavService.setCurrentCoCReferralTabIndex(index);
  }

  // /*Client Placements History tabs */
  // onClientPlacementTabSelected(index: number) {
  //   this.placementVerificationService.setCurrentClientPlacementsHistoryTabIndex(index);
  // }

  //#endregion Vacancy Control flags

  //#region Reports flags
  onReportsSelected() {
    // if (!this.navCollapsed) {
    //   this.setExpandedValue('reports', !this.reportsExpanded);
    // } else if (this.navCollapsed) {
    //   this.navService.navCollapsedToggle(false);
    //   this.navService.setWidthSideBar(260);
    //   this.setExpandedValue('reports', true);
    // }
    this.currentSys = 'reports';
    if (this.mobileSize.matches) {
      this.onSidenavClose();
    }
  }
  //#endregion Reports flags

  //#region Admin Tools flags
  onAdminToolsSelected() {
    if (!this.navCollapsed) {
      this.setExpandedValue('admin', !this.adminExpanded);
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
      this.setExpandedValue('admin', true);
    }
  }
  //#endregion Admin Tools flags

  onInformationSelected() {
    if (!this.navCollapsed) {
      this.setExpandedValue('information', !this.informationExpanded);
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
      this.setExpandedValue('information', true);
    }
  }
  onClientCaseFolderSelected() {
    if (!this.navCollapsed) {
      this.setExpandedValue('clientCaseFolder', !this.clientCaseFolderExpanded);
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
      this.setExpandedValue('clientCaseFolder', true);
    }
  }

  setExpandedValue(module: string, value: boolean) {
    this.capsExpanded = false;
    this.shsExpanded = false;
    this.newAppExpanded = false;
    this.pendingAppExpanded = false;
    this.vcsExpanded = false;
    this.reportsExpanded = false;
    this.adminExpanded = false;
    this.informationExpanded = false;
    this.clientCaseFolderExpanded = false;
    if (module === 'caps') {
      this.capsExpanded = value;
    } else if (module === 'shs') {
      this.shsExpanded = value;
    } else if (module === 'newApp') {
      this.newAppExpanded = value;
    } else if (module === 'pendingApp') {
      this.pendingAppExpanded = value;
    } else if (module === 'ds') {
    } else if (module === 'vcs') {
      this.vcsExpanded = value;
    } else if (module === 'reports') {
      this.reportsExpanded = value;
    } else if (module === 'admin') {
      this.adminExpanded = value;
    } else if (module === 'information') {
      this.informationExpanded = value;
    } else if (module === 'clientCaseFolder') {
      this.clientCaseFolderExpanded = value;
    }
  }

  public onSidenavClose() {
    this.sidenavClose.emit();
    //this.determinationSideNavService.setAllDeterminationDisabled(true);
  }

  onNewSurvey() {
    //window.location.href = this.newSurveyURL;
    window.location.href = environment.capsURL;
  }

  onPendingSurvey() {
    //window.location.href = this.pendingSurveyURL;
    window.location.href = environment.capsURL;
  }

  onSubmittedSurvey() {
    //window.location.href = this.submittedSurveyURL;
    window.location.href = environment.capsURL;
  }


  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.currentUrlSub) {
      this.currentUrlSub.unsubscribe();
    }
    if (this.navCollapsedSub) {
      this.navCollapsedSub.unsubscribe();
    }
    if (this.makeReferralTabStatusSub) {
      this.makeReferralTabStatusSub.unsubscribe();
    }
    if (this.tadSelectedHpSiteIDSub) {
      this.tadSelectedHpSiteIDSub.unsubscribe();
    }
    if (this.navigationItemsSub) {
      this.navigationItemsSub.unsubscribe();
    }
    if (this.getUserSiteSub) {
      this.getUserSiteSub.unsubscribe();
    }
    if (this.clientSourceTypeSub) {
      this.clientSourceTypeSub.unsubscribe();
    }
  }
}
