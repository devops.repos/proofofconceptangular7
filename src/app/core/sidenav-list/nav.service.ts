
import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Event, NavigationEnd, Router} from '@angular/router';
import {BehaviorSubject, Subject} from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { environment } from 'src/environments/environment';
import { ApplicationNotifications } from '../pact-dashboard/pact-dashboard.model';
import { PactDashboardService } from '../pact-dashboard/pact-dashboard.service';

@Injectable({
  providedIn: 'root'
})
export class NavService {
  private getAppNotificationURL = environment.pactApiUrl + 'AppNotification/GetAppNotification';

  private widthSideBar = new BehaviorSubject<number>(260);

  private navCollapsed = new BehaviorSubject<boolean>(null);

  private currentSys = new Subject<string>();
  private currentUrl = new BehaviorSubject<string>('/');

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private dashboardService: PactDashboardService,
    private userService: UserService,
    private authService: AuthService
  ) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        // console.log('UrlAfterRedirects: ', event.urlAfterRedirects);
        this.currentUrl.next(event.urlAfterRedirects);
        // Getting AppNotifications on every Navigation Change
        this.userService.getUserDataAsPromise().then(res => {
          if (res) {
            if (this.authService.isUserTokenValid()) {
              // We have a Valid User
              this.httpClient.get(this.getAppNotificationURL).subscribe((res : ApplicationNotifications) => {
                if (res) {
                  // console.log('AppNotifications: ', res);
                  this.dashboardService.setAppNotification(res);
                }
              });
            }
          }
        },
        (err) => {})

      }
    });
  }

  getWidthSideBar() {
    return this.widthSideBar.asObservable();
  }

  setWidthSideBar(value: number) {
    this.widthSideBar.next(value);
  }

  navCollapsedToggle(status: boolean) {
    this.navCollapsed.next(status);
  }
  getNavCollapsedStatus() {
    return this.navCollapsed.asObservable();
  }

  getCurrentSys() {
    return this.currentSys.asObservable();
  }
  setCurrentSys(sys: string) {
    this.currentSys.next(sys);
  }

  getCurrentUrl() {
    return this.currentUrl.asObservable();
  }

}
