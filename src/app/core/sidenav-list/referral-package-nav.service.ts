import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReferralPackageNavService {
  private currentHomelessInformationTabIndex = new BehaviorSubject<number>(0);
  private currentReferralReadyTrackingTabIndex = new BehaviorSubject<number>(null);
  private currentHousingPreferenceTabIndex = new BehaviorSubject<number>(null);
  private currentReferralPlacementSummaryTabIndex = new BehaviorSubject<number>(null);

  /* CoC Referral */
  private currentCoCReferralTabIndex = new BehaviorSubject<number>(0);

  constructor() {}

  getCurrentHomelessInformationTabIndex() {
    return this.currentHomelessInformationTabIndex.asObservable();
  }

  setCurrentHomelessInformationTabIndex(index: number) {
    this.currentHomelessInformationTabIndex.next(index);
  }

  getCurrentReferralReadyTrackingTabIndex() {
    return this.currentReferralReadyTrackingTabIndex.asObservable();
  }

  setCurrentReferralReadyTrackingTabIndex(index: number) {
    this.currentReferralReadyTrackingTabIndex.next(index);
  }

  getCurrentHousingPreferenceTabIndex() {
    return this.currentHousingPreferenceTabIndex.asObservable();
  }

  setCurrentHousingPreferenceTabIndex(index: number) {
    this.currentHousingPreferenceTabIndex.next(index);
  }

  getCurrentReferralPlacementSummaryTabIndex() {
    return this.currentReferralPlacementSummaryTabIndex.asObservable();
  }

  setCurrentReferralPlacementSummaryTabIndex(index: number) {
    this.currentReferralPlacementSummaryTabIndex.next(index);
  }


  /*CoC Referral Section */

  getCurrentCoCReferralTabIndex() {
    return this.currentCoCReferralTabIndex.asObservable();
  }

  setCurrentCoCReferralTabIndex(index: number) {
    this.currentCoCReferralTabIndex.next(index);
  }
}
