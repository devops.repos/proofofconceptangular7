import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model';

@Injectable({
    providedIn: 'root'
})
export class DeterminationSideNavService {

    /* Enabling or Disabling the left navigation */
    //Housing Determination 
    IsHousingDeterminationDisabled = new BehaviorSubject<boolean>(true);
    IsClinicalReviewDisabled = new BehaviorSubject<boolean>(true);
    IsHomelessReviewDisabled = new BehaviorSubject<boolean>(true);
    IsMedicaidPrioritizationDisabled = new BehaviorSubject<boolean>(true);
    IsVulnerabilityAssessmentDisabled = new BehaviorSubject<boolean>(true);
    IsDeterminationSummaryDisabled = new BehaviorSubject<boolean>(true);
    //Sign Off
    IsSignOffFollowUpDisabled = new BehaviorSubject<boolean>(true);
    /* Enabling or Disabling the left navigation */

    constructor() {
    }

    //Set Housing Determination
    setIsHousingDeterminationDisabled(value: boolean) {
        this.IsHousingDeterminationDisabled.next(value);
    }
    //Get Housing Determination
    getIsHousingDeterminationDisabled() {
        return this.IsHousingDeterminationDisabled.asObservable();
    }
    //Set Clinical Review
    setIsClinicalReviewDisabled(value: boolean) {
        this.IsClinicalReviewDisabled.next(value);
    }
    //Get Clinical Review
    getIsClinicalReviewDisabled() {
        return this.IsClinicalReviewDisabled.asObservable();
    }
    //Set Homeless Review
    setIsHomelessReviewDisabled(value: boolean) {
        this.IsHomelessReviewDisabled.next(value);
    }
    //Get Homeless Review
    getIsHomelessReviewDisabled() {
        return this.IsHomelessReviewDisabled.asObservable();
    }
    //Set Mediacaid Prioritization
    setIsMedicaidPrioritizationDisabled(value: boolean) {
        this.IsMedicaidPrioritizationDisabled.next(value);
    }
    //Get Medicaid Prioritization
    getIsMedicaidPrioritizationDisabled() {
        return this.IsMedicaidPrioritizationDisabled.asObservable();
    }
    //Set Vulnerability Assessment
    setIsVulnerabilityAssessmentDisabled(value: boolean) {
        this.IsVulnerabilityAssessmentDisabled.next(value);
    }
    //Get Vulnerability Assessment
    getIsVulnerabilityAssessmentDisabled() {
        return this.IsVulnerabilityAssessmentDisabled.asObservable();
    }
    //Set Determination Summary
    setIsDeterminationSummaryDisabled(value: boolean) {
        this.IsDeterminationSummaryDisabled.next(value);
    }
    //Get Determination Summary
    getIsDeterminationSummaryDisabled() {
        return this.IsDeterminationSummaryDisabled.asObservable();
    }
    //Set Sign Off Follow Up
    setIsSignOffFollowUpDisabled(value: boolean) {
        this.IsSignOffFollowUpDisabled.next(value);
    }
    //Get Sign Off Follow Up
    getIsSignOffFollowUpDisabled() {
        return this.IsSignOffFollowUpDisabled.asObservable();
    }
    //Set All Determination Enabled
    setAllDeterminationDisabled(value: boolean) {
        //this.IsOutcomeDisabled.next(value);
        this.IsHousingDeterminationDisabled.next(value);
        this.IsClinicalReviewDisabled.next(value);
        this.IsHomelessReviewDisabled.next(value);
        this.IsMedicaidPrioritizationDisabled.next(value);
        this.IsVulnerabilityAssessmentDisabled.next(value);
        this.IsDeterminationSummaryDisabled.next(value);
        this.IsSignOffFollowUpDisabled.next(value);
    }

    //Enable Pending Review
    enablePendingReview(applicationDeterminationData: iApplicationDeterminationData) {
        //Clinical Review
        if (applicationDeterminationData.isMentalHealthConditionTabCompleted ||
            applicationDeterminationData.isSubstanceUseTabCompleted ||
            applicationDeterminationData.isMedicalConditionsTabCompleted ||
            applicationDeterminationData.isClinicalAtRiskTabCompleted ||
            applicationDeterminationData.isClinicalSummaryTabCompleted) {
            this.IsHousingDeterminationDisabled.next(false);
            this.IsClinicalReviewDisabled.next(false);
        }
        else {
            this.IsClinicalReviewDisabled.next(true);
        }
        //Homeless Review
        if (applicationDeterminationData.isHousingHomelessTabCompleted ||
            applicationDeterminationData.isHomeless1To4YearsTabCompleted ||
            applicationDeterminationData.isResidentialTreatmentAndInstitutionalHistoryTabCompleted ||
            applicationDeterminationData.isHomelessAtRiskTabCompleted ||
            applicationDeterminationData.isHomelessSummaryTabCompleted ||
            applicationDeterminationData.isHousingHomelessReportTabCompleted) {
            this.IsHousingDeterminationDisabled.next(false);
            this.IsHomelessReviewDisabled.next(false);
        }
        else {
            this.IsHomelessReviewDisabled.next(true);
        }
        //Medicaid Prioritization
        if (applicationDeterminationData.isMedicaidClaimsTabCompleted ||
            applicationDeterminationData.isMedicaidPrioritizationTabCompleted) {
            this.IsHousingDeterminationDisabled.next(false);
            this.IsMedicaidPrioritizationDisabled.next(false);
        }
        else {
            this.IsMedicaidPrioritizationDisabled.next(true);
        }
        //Vulnerability Assessment
        if (applicationDeterminationData.isActivitiesOfDailyLivingTabCompleted ||
            applicationDeterminationData.isSVAReviewTabCompleted ||
            applicationDeterminationData.isVulnerabilityAssessmentTabCompleted ||
            applicationDeterminationData.isSVASummaryReportTabCompleted) {
            this.IsHousingDeterminationDisabled.next(false);
            this.IsVulnerabilityAssessmentDisabled.next(false);
        }
        else {
            this.IsVulnerabilityAssessmentDisabled.next(true);
        }
        //Determination Summary
        if (applicationDeterminationData.isDeterminationSummaryTabCompleted ||
            applicationDeterminationData.isConditionsRecommendationsTabCompleted ||
            applicationDeterminationData.isDeterminationLetterTabCompleted) {
            this.IsHousingDeterminationDisabled.next(false);
            this.IsDeterminationSummaryDisabled.next(false);
        }
        else {
            this.IsDeterminationSummaryDisabled.next(true);
        }
        //Sign Off
        if (applicationDeterminationData.isSignOffTabCompleted ||
            applicationDeterminationData.isFollowUpTabCompleted) {
            this.IsSignOffFollowUpDisabled.next(false);
        }
        else {
            this.IsSignOffFollowUpDisabled.next(true);
        }
    }
}
