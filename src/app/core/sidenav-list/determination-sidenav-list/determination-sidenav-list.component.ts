import { Component, OnInit, Input, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { MediaMatcher } from '@angular/cdk/layout';

//Service
import { MyWorklistService } from 'src/app/pact-modules/determination/my-worklist/my-worklist.service';
import { NavService } from '../nav.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { DeterminationSideNavService } from './determination-sidenav-list.service';
import { INavigationItemsOnly } from '../navigation.interface';

@Component({
  selector: 'app-determination-sidenav-list',
  templateUrl: './determination-sidenav-list.component.html',
  styleUrls: ['./determination-sidenav-list.component.scss']
})

export class DeterminationSidenavListComponent implements OnInit, OnDestroy {
  @Input() navCollapsed: boolean;
  @Input() detItems: INavigationItemsOnly[];

  //Global Variables
  mobileSize: MediaQueryList;
  private mobileQueryListener: () => void;

  dsExpanded: boolean = false;
  dsVisible: boolean = true;
  pendingReviewExpanded: boolean = false;
  isPendingReview: boolean = false;

  currentUrl = '/';
  currentUrlSub: Subscription;
  determinationApplicationIdSub: Subscription;

  currentSys: string;
  myWorklistExpanded: boolean = false;
  applicationReviewExpanded: boolean = false;
  housingDeterminationExpanded: boolean = false;
  myWorklistPendingSelected: boolean = false;
  myWorklistFollowUpSelected: boolean = false;
  myWorklistCompletedSelected: boolean = false;
  determinationApplicationId: number = null;

  /* Enabling or Disabling the left navigation */
  //Housing Determination
  IsHousingDeterminationDisabled: boolean = true;
  IsClinicalReviewDisabled: boolean = true;
  IsHomelessReviewDisabled: boolean = true;
  IsMedicaidPrioritizationDisabled: boolean = true;
  IsVulnerabilityAssessmentDisabled: boolean = true;
  IsDeterminationSummaryDisabled: boolean = true;
  //Sign Off
  IsSignOffFollowUpDisabled: boolean = true;
  /* Enabling or Disabling the left navigation */

  constructor(private myWorklistService: MyWorklistService,
    private navService: NavService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private determinationSideNavService: DeterminationSideNavService,
    media: MediaMatcher,
    changeDetectorRef: ChangeDetectorRef) {
    this.mobileSize = media.matchMedia('(max-width: 959px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileSize.addListener(this.mobileQueryListener);
  }

  ngOnInit() {
    this.determinationSideNavService.getIsHousingDeterminationDisabled().subscribe(res => { this.IsHousingDeterminationDisabled = res });
    this.determinationSideNavService.getIsClinicalReviewDisabled().subscribe(res => { this.IsClinicalReviewDisabled = res });
    this.determinationSideNavService.getIsHomelessReviewDisabled().subscribe(res => { this.IsHomelessReviewDisabled = res });
    this.determinationSideNavService.getIsMedicaidPrioritizationDisabled().subscribe(res => { this.IsMedicaidPrioritizationDisabled = res });
    this.determinationSideNavService.getIsVulnerabilityAssessmentDisabled().subscribe(res => { this.IsVulnerabilityAssessmentDisabled = res });
    this.determinationSideNavService.getIsDeterminationSummaryDisabled().subscribe(res => { this.IsDeterminationSummaryDisabled = res });
    this.determinationSideNavService.getIsSignOffFollowUpDisabled().subscribe(res => { this.IsSignOffFollowUpDisabled = res });

    /* Getting the currently active url and setting the required flags for determination sidenav */
    this.currentUrlSub = this.navService.getCurrentUrl().subscribe((url: string) => {
      this.currentUrl = url;
      const activeSys = url.split('/');
      if (activeSys[1] === 'ds') {
        this.currentSys = 'ds';
        if (activeSys.length > 2) {
          if (activeSys[2] === 'case-assignment' || activeSys[2] === 'auto-assignment') {
            this.dsVisible = true;
            this.dsExpanded = true;
            this.isPendingReview = false;
            this.pendingReviewExpanded = false;
            this.myWorklistExpanded = false;
            this.applicationReviewExpanded = false;
            this.housingDeterminationExpanded = false;
          }
          else if (activeSys[2] === 'my-worklist') {
            this.dsVisible = true;
            this.dsExpanded = true;
            this.myWorklistExpanded = true;
            this.isPendingReview = false;
            this.pendingReviewExpanded = false;
            this.applicationReviewExpanded = false;
            this.housingDeterminationExpanded = false;
            this.myWorklistService.getTabIndex().subscribe(res => {
              if (res === 0) {
                this.myWorklistPendingSelected = true;
                this.myWorklistFollowUpSelected = false;
                this.myWorklistCompletedSelected = false;
              }
              else if (res === 1) {
                this.myWorklistPendingSelected = false;
                this.myWorklistFollowUpSelected = true;
                this.myWorklistCompletedSelected = false;
              }
              else if (res === 2) {
                this.myWorklistPendingSelected = false;
                this.myWorklistFollowUpSelected = false;
                this.myWorklistCompletedSelected = true;
              }
            });
          }
          else if (activeSys[2] === 'match-sources' || activeSys[2] === 'application' || activeSys[2] === 'client-case-folder' || activeSys[2] === 'outcome') {
            this.dsVisible = false;
            this.dsExpanded = false;
            this.isPendingReview = true;
            this.pendingReviewExpanded = true;
            this.myWorklistExpanded = false;
            this.applicationReviewExpanded = true;
            this.housingDeterminationExpanded = false;
            this.getDeterminationApplicationId();
          }
          else if (activeSys[2] === 'clinical-review' || activeSys[2] === 'homeless-review' || activeSys[2] === 'medicaid-prioritization' || activeSys[2] === 'vulnerability-assessment' || activeSys[2] === 'determination-summary') {
            this.dsVisible = false;
            this.dsExpanded = false;
            this.isPendingReview = true;
            this.pendingReviewExpanded = true;
            this.myWorklistExpanded = false;
            this.applicationReviewExpanded = false;
            this.housingDeterminationExpanded = true;
            this.getDeterminationApplicationId();
          }
          else if (activeSys[2] === 'sign-off-follow-up') {
            this.dsVisible = false;
            this.dsExpanded = false;
            this.isPendingReview = true;
            this.pendingReviewExpanded = true;
            this.myWorklistExpanded = false;
            this.applicationReviewExpanded = false;
            this.housingDeterminationExpanded = false;
            this.getDeterminationApplicationId();
          }
          else {
            this.dsVisible = true;
            this.isPendingReview = false;
            this.pendingReviewExpanded = false;
            this.myWorklistExpanded = false;
            this.applicationReviewExpanded = false;
            this.housingDeterminationExpanded = false;
          }
        }
      } else if (activeSys[1] === 'reports') {
        this.currentSys = 'reports';
        this.dsVisible = true;
        this.dsExpanded = false;
      }

    });
  }

  //Get Application Id from Application Determination Service
  getDeterminationApplicationId() {
    this.determinationApplicationIdSub = this.applicationDeterminationService.getApplicationIdForDetermination().subscribe(res => {
      if (res) {
        this.determinationApplicationId = res;
      }
    });
  }

  //On Worklist Selected
  onMyWorklistSelected() {
    this.myWorklistExpanded = !this.myWorklistExpanded;
  }

  //On Application Review Selected
  onApplicationReviewSelected() {
    this.applicationReviewExpanded = !this.applicationReviewExpanded;
  }

  //On Application Review Selected
  onHousingDeterminationSelected() {
    this.housingDeterminationExpanded = !this.housingDeterminationExpanded;
  }

  //On Worklist Pending Selected
  onMyWorklistPendingSelected() {
    this.myWorklistPendingSelected = true;
    this.myWorklistFollowUpSelected = false;
    this.myWorklistCompletedSelected = false;
    this.myWorklistService.setTabIndex(0);
  }

  //On Worklist FollowUp Selected
  onMyWorklistFollowUpSelected() {
    this.myWorklistPendingSelected = false;
    this.myWorklistFollowUpSelected = true;
    this.myWorklistCompletedSelected = false;
    this.myWorklistService.setTabIndex(1);
  }

  //On Worklist Completed Selected
  onMyWorklistCompletedSelected() {
    this.myWorklistPendingSelected = false;
    this.myWorklistFollowUpSelected = false;
    this.myWorklistCompletedSelected = true;
    this.myWorklistService.setTabIndex(2);
  }

  //On Pending Review Selected
  onPendingReviewSelected() {
    if (!this.navCollapsed) {
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
    }
    this.pendingReviewExpanded = !this.pendingReviewExpanded;
  }

  //On Determination System Selected
  onDsSelected() {
    if (!this.navCollapsed) {
    } else if (this.navCollapsed) {
      this.navService.navCollapsedToggle(false);
      this.navService.setWidthSideBar(260);
    }
    this.dsExpanded = !this.dsExpanded;
  }

  //On Destroy
  ngOnDestroy() {
    if (this.currentUrlSub) {
      this.currentUrlSub.unsubscribe();
    }
    if (this.determinationApplicationIdSub) {
      this.determinationApplicationIdSub.unsubscribe();
    }
  }
}
