import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { INavigationItems } from './navigation.interface';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  private navigationURL = environment.pactApiUrl + 'Navigation';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private navigationItems = new BehaviorSubject<INavigationItems>(null);

  constructor(private http: HttpClient) { }

  // fetchNavigationItems() {
  //   return this.http.get(this.navigationURL + '/GetNavigationItems');
  // }
  fetchNavigationItems(optionUserId: number) {
    return this.http.post(this.navigationURL + '/GetNavigationItems', JSON.stringify(optionUserId), this.httpOptions);
  }

  setNavigationItems(navItems: INavigationItems) {
    this.navigationItems.next(navItems);
  }

  getNavigationItems() {
    return this.navigationItems.asObservable();
  }
}
