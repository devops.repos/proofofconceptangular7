export interface SidenavStatus {
    pactApplicationID: number;
    isConsentComplete: number;
    isDemographicComplete: number;
    isHousingHomelessComplete: number;
    isClinicalAssessmentComplete: number;
    isADLComplete: number;
    isMedicationsProvidersHospitalizationComplete: number;
    isTraumaAndChildWelfareComplete: number;
    isSymptomsSubstanceComplete: number;
    isHousingPreferenceComplete: number;
    isPsychiatricPsychoMHRComplete: number;
    isComplete: number;
}