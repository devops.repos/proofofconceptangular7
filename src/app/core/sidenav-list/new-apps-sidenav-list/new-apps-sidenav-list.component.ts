import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { SidenavStatus } from '../sidenav-status.model';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { Subscription } from 'rxjs';
import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
import { SidenavStatusService } from '../sidenav-status.service';
import { takeLast, last, takeWhile } from 'rxjs/operators';
import { NavService } from '../nav.service';

@Component({
  selector: 'app-new-apps-sidenav-list',
  templateUrl: './new-apps-sidenav-list.component.html',
  styleUrls: ['./new-apps-sidenav-list.component.scss']
})
export class NewAppsSidenavListComponent implements OnInit, OnDestroy {

  @Input() newAppExpanded: false;
  @Input() navCollapsed: false;
  selectedApplicationID: number;
  sidenavStatus: SidenavStatus = {
    pactApplicationID: 0,
    isConsentComplete: 2,
    isDemographicComplete: 2,
    isHousingHomelessComplete: 2,
    isClinicalAssessmentComplete: 2,
    isADLComplete: 2,
    isMedicationsProvidersHospitalizationComplete: 2,
    isTraumaAndChildWelfareComplete: 2,
    isSymptomsSubstanceComplete: 2,
    isHousingPreferenceComplete: 2,
    isPsychiatricPsychoMHRComplete: 2,
    isComplete: 0
  };

  @Output() sidenavClose = new EventEmitter();

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;
  currentUrl;

  selectedApplicationIDSub: Subscription;
  sidenavStatusSub: Subscription;
  sidenavCompleteStatusSub: Subscription;
  currentUrlSub: Subscription;

  constructor(
    private clientApplicationService: ClientApplicationService,
    private sidenavStatusService: SidenavStatusService,
    private navService: NavService
  ) { }

  ngOnInit() {
      this.currentUrlSub = this.navService
      .getCurrentUrl()
      .subscribe((url: string) => {
        this.currentUrl = url;
        const activeSys = url.split('/');
        if (activeSys[1] === 'dashboard') {
          if (this.selectedApplicationIDSub) {
            this.selectedApplicationIDSub.unsubscribe();
          }
          if (this.sidenavCompleteStatusSub) {
            this.sidenavCompleteStatusSub.unsubscribe();
          }
        } else if (activeSys[1] === 'shs') {
          if (activeSys.length > 4) {
            if (activeSys[2] === 'newApp' || activeSys[2] === 'pendingApp') {
              this.selectedApplicationIDSub = this.sidenavStatusService.getApplicationIDForSidenavStatus().subscribe(res => {
                if (res) {
                  this.selectedApplicationID = res;
                  // console.log('selectedApplicationID: ',this.selectedApplicationID);
                  this.sidenavCompleteStatusSub = this.sidenavStatusService.getSidenavCompleteStatus(this.selectedApplicationID).subscribe((rslt) => {
                    if (rslt) {
                      this.sidenavStatus = rslt;
                    }
                  });
                }
              });
            }
          }
        }
      });

      /* Getting the Complete/InComplete Status of sidenav List for a particular ApplicationID  from SidenavStatusService */
    // this.sidenavStatusSub = this.sidenavStatusService
    // .readSidnavStatus()
    // .subscribe(res => {
    //   if (res) {
    //       this.sidenavStatus = res;
    //     // console.log(this.sidenavStatus);
    //   }
    // });

    // if (this.sidenavStatusSub) {
    //   setTimeout(() => {
    //     this.sidenavStatusSub.unsubscribe();
    //   }, 3000);
    // }
  }

  public onSidenavClose() {
    this.sidenavClose.emit();
  }

  ngOnDestroy() {
    if (this.selectedApplicationIDSub) {
      this.selectedApplicationIDSub.unsubscribe();
    }
    if (this.sidenavStatusSub) {
      this.sidenavStatusSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
    if (this.currentUrlSub) {
      this.currentUrlSub.unsubscribe();
    }
  }

}
