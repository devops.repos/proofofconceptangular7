import { Component, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'sign-in',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, AfterViewInit ,OnDestroy {
    isLoading: boolean = true;
    isOverlay: boolean = false;
    authServiceSub1: Subscription;
    authServiceSub2: Subscription;
    loginFormGroup: FormGroup;
    lanID : string;

  constructor(
        private authService: AuthService, private formBuilder: FormBuilder,
          private router: Router, private toastr: ToastrService,
        ) {
  }

  readonly validationMssg = {

    userIdCtrl: {
      'required': 'Please enter UserID.',
      'maxlength' : 'Max length of text is 10 characters.'
    },
    passwordCtrl: {
      'required': 'Please enter password (case insensitive)',
      'maxlength' : 'Max length of text is 10 characters.'
    },


  };

  ngOnInit() {

    this.loginFormGroup = this.formBuilder.group({

      userIdCtrl : ['', [Validators.required, Validators.maxLength(10)]],
      passwordCtrl : ['', [Validators.required, Validators.maxLength(10)]],
    });

    this.authServiceSub1 = this.authService.subscribeAuthenticationEventListner().subscribe(res => {
      if(res != undefined && res != null)
        this.lanID = res;
        console.log('Login Component, User lanID = ' + this.lanID);
    });

  }

  ngAfterViewInit(){
    if(this.lanID == null || this.lanID == undefined || this.lanID.trim().length == 0){
      this.router.navigate(['/dashboard']); // to invoke the AuthService for reauthentication
    }
  }

  get f() { return this.loginFormGroup.controls; }

  onLoginClick(){
      let that = this;
      let uid = this.loginFormGroup.get('userIdCtrl').value;
      let password = this.loginFormGroup.get('passwordCtrl').value;

      if(this.lanID == null || this.lanID == undefined || this.lanID.trim().length == 0){
        that.toastr.error('LanID is unavailable to proceed.', '');
        return;
      }

      this.authServiceSub2 = this.authService.login(uid, password, this.lanID).subscribe(
        data => {
            if(data === true) {
              that.toastr.success("LanID is updated for User record.", '');
              that.router.navigate(['/dashboard']);
            } else {
              that.toastr.warning('User is either inActive or matching record not found. Please contact CAS.', '');
            }
        },
        error => {
          that.toastr.warning('Error while Saving !!', '');
        }
      );
  }

  ngOnDestroy() {
    if(this.authServiceSub1)  this.authServiceSub1.unsubscribe();
    if(this.authServiceSub2)  this.authServiceSub2.unsubscribe();
  }

}
