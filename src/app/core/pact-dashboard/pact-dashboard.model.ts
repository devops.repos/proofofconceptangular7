// export class UserStats {
//     stats: StatsList[];
// }

export class StatsList {
    header: string;
    stats: StatsValue[];
}

export class StatsValue {
    id: number;
    statTitle: string;
    statValue: number;
}

export class AppNotification {
    appNotificationID?: number;
    notificationTitle?: string;
    notificationMessage?: string;
    notificationLongMessage?: string;
    isAlert?: boolean;
    isAlertDisplayOnce?: boolean;
    isPopup?: boolean;
    isPopupDisplayOnce?: boolean;
    isAnnouncement?: boolean;
    sortOrder?: number;
    startDate?: string;
    endDate?: string;
    userID?: number;
}

export class ApplicationNotifications {
    appAnnouncement: AppNotification[];
    appAlert: AppNotification[];
    appPopup: AppNotification[];
}

declare global {
    interface Document {
        documentMode?: any;
    }
}
