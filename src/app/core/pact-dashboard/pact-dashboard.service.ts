import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StatsList, AppNotification, ApplicationNotifications } from './pact-dashboard.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PactDashboardService {

  private apiURL = environment.pactApiUrl + 'Dashboard/';
  private getHPsitesForIncompleteUnitsURL = environment.pactApiUrl + 'Dashboard/GetHPsitesForIncompleteUnits';
  private getAppNotificationURL = environment.pactApiUrl + 'AppNotification/GetAppNotification';
  private saveAppNotificationUserURL = environment.pactApiUrl + 'AppNotification/SaveAppNotificationUser';


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private appNotification = new BehaviorSubject<ApplicationNotifications>(null);

  constructor(private httpClient: HttpClient) { }

  getUserStats(optionUserID: number, siteID: number) {
    const getUrl = this.apiURL + 'GetUserStats';
    return this.httpClient.get(getUrl, { params: { userID: optionUserID.toString(), siteID: siteID.toString() }, observe: 'response' });
  }

  getHPsitesForIncompleteUnits(agencyId: number) {
    return this.httpClient.get(this.getHPsitesForIncompleteUnitsURL, { params: { AgencyId: agencyId.toString() }, observe: 'response' });
  }

  getApplicationNotifications() {
    return this.httpClient.get(this.getAppNotificationURL, this.httpOptions);
  }

  setAppNotification(note: ApplicationNotifications) {
    this.appNotification.next(note);
  }

  getAppNotification() {
    return this.appNotification.asObservable();
  }

  saveAppNotificationUser(appNotification: AppNotification) {
    return this.httpClient.post<ApplicationNotifications>(this.saveAppNotificationUserURL, appNotification, this.httpOptions);
  }

}
