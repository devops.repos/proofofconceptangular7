import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImpersonateService {

  impersonateURL = environment.pactApiUrl + 'Impersonate/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  getImpersonateUserList(searchString: string) {
    return this.httpClient.post(this.impersonateURL + 'GetUserList', JSON.stringify(searchString), this.httpOptions);
  }
}
