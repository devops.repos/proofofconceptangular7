export interface ImpersonateUserList {
  optionUserID: number;
  lanID: string;
  nonHraLanID: string;
  firstName: string;
  lastName: string;
  email: string;
  officePhone: string;
  officeExt: string;
  faxNumber: string;
  cellPhone: string;
  initials: string;
  salutation: string;
  lastAccessDate: string;
  roleID: number;
  roleName: string;
  isSH_RA: boolean;
  isSH_PE: boolean;
  isSH_HP: boolean;
  isCAS: boolean;
  isML_DHS_Liasian: boolean;
  isML_Team: boolean;
  isML_HP: boolean;
  isML_IREA: boolean;
}
