import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ImpersonateUserList } from './impersonateUserList.model';
import { ImpersonateService } from './impersonate.service';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-impersonate',
  templateUrl: './impersonate.component.html',
  styleUrls: ['./impersonate.component.scss']
})
export class ImpersonateComponent implements OnInit {
  userList: ImpersonateUserList[] = [];
  filteredUserList: Observable<ImpersonateUserList[]>;

  impersonateGroup: FormGroup;

  selectedUser: ImpersonateUserList;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private impersonateService: ImpersonateService
  ) {
    this.impersonateGroup = this.formBuilder.group({
      searchStringCtrl: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])]
    });
  }

  ngOnInit() {
    /* Getting the UserList from the Api */
    this.impersonateGroup.get('searchStringCtrl').valueChanges.subscribe(value => {
      this.userList = [];
      if (value.length >= 3) {
        /* Getting the user List */
        this.impersonateService.getImpersonateUserList(value).subscribe(res => {
          this.userList = res as ImpersonateUserList[];
          // console.log(this.userList);
          if (this.userList.length > 0) {
            this.filteredUserList = this.impersonateGroup.get('searchStringCtrl').valueChanges.pipe(
                startWith(''),
                map(user => user ? this._filterUserList(user) : this.userList.slice())
              );
          }
        });
      }
    });
  }
  private _filterUserList(user : string): ImpersonateUserList[] {
    if (typeof(user) == 'string') {
      // console.log('_filterUserList datatype: ', typeof(user));
      const filterValue = user.toLowerCase();
      return this.userList.filter(user => user.lanID.toLowerCase().includes(filterValue) || user.firstName.toLowerCase().includes(filterValue) || user.lastName.toLowerCase().includes(filterValue));
    }
  }

  onSelectedUser(event) {
    // console.log(event.option.value);
    if (event.option.value) {
      this.selectedUser = event.option.value;
    }
  }

  displaySelectedUser(selectedUser: ImpersonateUserList) {
    if (selectedUser) {
      // console.log('displaySelectedUser: ', selectedUser);
      var text = selectedUser.firstName + ' ' + selectedUser.lastName + ' | Role: ' + selectedUser.roleName + ' | SiteCategoryType: ';
      text += selectedUser.isSH_RA ? '(SH_RA) ' : selectedUser.isSH_PE ? '(SH_PE) ' : selectedUser.isSH_HP ? '(SH_HP) ' : selectedUser.isCAS ? '(CAS) ' :
      selectedUser.isML_DHS_Liasian ? '(ML_DHS_Liasian) ' : selectedUser.isML_HP ? '(ML_HP) ' : selectedUser.isML_IREA ? '(ML_IREA) ' : selectedUser.isML_Team ? '(ML_Team) ' : '';
      // console.log('text: ', text);
      return text;
    } else {
      return null;
    }
  }

  onImpersonate() {
    if (this.impersonateGroup.invalid && !this.selectedUser) {
      return;
    }
    // console.log(this.impersonateGroup.get('searchStringCtrl').value);
    // this.authService.authUser(this.impersonateGroup.get('searchStringCtrl').value);
    this.authService.authUser(this.selectedUser.lanID);
  }
}
