import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ThemeService } from 'src/app/services/helper-services/theme.service';
import { NotificationService } from 'src/app/services/helper-services/notification.service';
import { MatSnackBar } from '@angular/material';
import { ErrorsService, ErrorLog } from '../errors.service';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss']
})
export class ErrorsComponent implements OnInit, OnDestroy {
  routeParams;
  data;

  errorLog: ErrorLog;
  errorServiceSub: Subscription;

  themeSub: Subscription;
  selectedTheme: string;
  showButton: boolean = true;
  notification: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private theme: ThemeService,
    private notificationService: NotificationService,
    private snackBar: MatSnackBar,
    private errorService: ErrorsService
  ) {}

  ngOnInit() {
    // this.routeParams = this.activatedRoute.snapshot.queryParams;
    // console.log('routeParams: ', this.routeParams);
    // this.data = this.activatedRoute.snapshot.data;

    /** Getting the selected theme from the theme service */
    this.themeSub = this.theme.getSelectedTheme().subscribe((res: string) => {
      this.selectedTheme = res;
      // console.log(res);
    });

    /* Testing server Error and displaying as notification */
    // this.notificationService.notification$.subscribe(message => {
    //   this.notification = message;
    //   if (this.notification) {
    //     this.snackBar.open(this.notification, '', {
    //       duration: 5000
    //     });
    //   }
    // });

    /* Getting the ErrorLog from error Service */
    this.errorServiceSub = this.errorService.getErrorLog().subscribe(res => {
        // console.log(res);
        this.errorLog = res;
        if(this.errorLog && this.errorLog.shortMessage && this.errorLog.shortMessage.includes("403"))
            this.showButton = false;
    });

  }

  ngOnDestroy(): void {
    if(this.themeSub) this.themeSub.unsubscribe();
    if(this.errorServiceSub) this.errorServiceSub.unsubscribe();
  }

}
