import {
  ErrorHandler,
  Injectable,
  Injector,
  OnInit,
  NgZone,
  Type
} from '@angular/core';
import {
  Location,
  HashLocationStrategy,
  LocationStrategy,
  PathLocationStrategy
} from '@angular/common';
import {
  HttpErrorResponse,
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import { Router, Event, NavigationError } from '@angular/router';

import { Observable, Subscription, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';

import { AuthService } from '../../services/auth/auth.service';
import { AuthData } from '../../models/auth-data.model';

import * as jwt_decode from 'jwt-decode';
import { UserService } from 'src/app/services/helper-services/user.service';

export interface ErrorLog {
  source: string;
  requestUri: string;
  requestBody: string;
  shortMessage: string;
  stackTrace: string;
  serverName: string;
  severityLevel: string;
  userName: string;
}

@Injectable()
export class ErrorsService implements OnInit {
  private pactApiURL = environment.pactApiUrl + 'Logger';
  private userData: AuthData;
  userDataSub: Subscription;

  private errorLog = new BehaviorSubject<ErrorLog>(null);

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private injector: Injector,
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private httpClient: HttpClient,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    // Subscribe to the NavigationError
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationError) {
        // Redirect to the ErrorComponent
        // this.log(event.error).subscribe(errorWithContext => this.ngZone.run(() => {
        //   this.router.navigate(['/error'], { queryParams: errorWithContext });
        // }));
        this.log(event.error);
      }
    });
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });
  }

  log(error: any) {
    const errorToSend = this.addContextInfo(error);
    if (errorToSend && errorToSend.requestUri !== '/error' && error.status !== 0 && error.status !== 200) {
      this.errorLog.next(errorToSend);
      // return FakeHttpService.post(errorToSend);
      if (!errorToSend.requestUri.includes('/api/identity')) {
        this.httpClient
          .post(this.pactApiURL + '/Error/', errorToSend, this.httpOptions)
          .subscribe(res => this.ngZone.run(() => {
            // console.log(res);
            this.router.navigate(['/error']);
          }));
      } else {
        this.ngZone.run(() => {
          this.router.navigate(['/error']);
        });
      }
    }
  }

  addContextInfo(error: any) {
    // You can include context details here (usually coming from other services: UserService...)

    /** HttpErrorResponse returned manually from API */
    // headers: HttpHeaders {normalizedNames: Map(0), lazyUpdate: null, lazyInit: ƒ}
    // status: 403
    // statusText: "Forbidden"
    // url: "http://localhost:37959/api/SidenavStatus/GetSidenavStatus/226209"
    // ok: false
    // name: "HttpErrorResponse"
    // message: "Http failure response for http://localhost:37959/api/SidenavStatus/GetSidenavStatus/226209: 403 Forbidden"
    // error: "Unauthorized Operation"

    // headers: HttpHeaders {normalizedNames: Map(0), lazyUpdate: null, lazyInit: ƒ}
    // status: 401
    // statusText: "Unauthorized"
    // url: "http://localhost:37959/api/identity"
    // ok: false
    // name: "HttpErrorResponse"
    // message: "Http failure response for http://localhost:37959/api/identity: 401 Unauthorized"
    // error:
    //    message: "Windows Authentication has failed."

    let source = '';
    let requestUri = '';
    const requestBody = '';
    let shortMessage = '';
    let stackTrace = '';
    let serverName = '';
    let severityLevel = error.name || 'Error';
    let userName = '';

    // if (error.error instanceof ErrorEvent) {
    //   // client-side error
    //   shortMessage = `Error: ${error.error.message}`;
    // } else {
    //   // server-side error
    //   shortMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    // }

    if (this.authService.getToken()) {
      const token = jwt_decode(this.authService.getToken());
      userName = token.unique_name;
    }

    if (error.name == 'HttpErrorResponse') {
      source = 'FrontEnd PACTWeb httpError';
      requestUri = error.url;
      if (error.status == 500) {
        shortMessage = error.status + ' : ' + error.statusText;
        stackTrace = error.error ? error.error : error.url;
      }
      else if (error.status == 403) {
        shortMessage = '(' + error.status + ' : ' + error.statusText + ')   ' + error.error;
        stackTrace = error.message ? error.message : error.url;
      }
      else {
        shortMessage = error.status + ' : ' + error.statusText;
        stackTrace = error.stackTrace ? error.stackTrace : error.url;
      }
      serverName = window.location.hostname ? window.location.hostname : '' ;
      severityLevel = error.name || 'Error';
    }
    else {
      source = 'FrontEnd PACTWeb';
      // const location = this.injector.get(LocationStrategy);
      const location = this.injector.get<LocationStrategy>(LocationStrategy as Type<LocationStrategy>)
      requestUri = location ? (location instanceof HashLocationStrategy ? location.path() : '') : '';
      shortMessage = error.message;
      stackTrace = error.stackTrace ? error.stackTrace : requestUri;
      serverName = window.location.hostname ? window.location.hostname : '' ;
      severityLevel = error.name || 'Error';
    }

    const errorWithContext = {
      source,
      requestUri,
      requestBody,
      shortMessage,
      stackTrace,
      serverName,
      severityLevel,
      userName
    };
    // console.log(errorWithContext);
    return errorWithContext;
  }

  getErrorLog() {
    return this.errorLog.asObservable();
  }
}

class FakeHttpService {
  static post(error: any): Observable<any> {
    // console.log('Error sent to the server: ', error);
    // return error;
    const errorObservable = new Observable(observer => {
      observer.next(error);
    });

    return errorObservable;
  }
}
