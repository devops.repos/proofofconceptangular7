import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotificationService } from '../../services/helper-services/notification.service';
import { ErrorsService } from './errors.service';

// import * as StackTraceParser from 'error-stack-parser';

@Injectable()
export class ErrorsHandler implements ErrorHandler {
  constructor(private injector: Injector, private ngZone: NgZone) {}

  handleError(error: Error | HttpErrorResponse) {
    const notificationService = this.injector.get(NotificationService);
    const errorsService = this.injector.get(ErrorsService);
    const router = this.injector.get(Router);

    if (error instanceof HttpErrorResponse) {
      // Server error happened
      if (!navigator.onLine) {
        // No Internet connection
        // console.log('error-handler.ts -> no Internet Connection');
        return notificationService.notify('No Internet Connection');
      }
      // Http Error
      // Send the error to the server
      // console.log('error-handler.ts -> Server Error.');
      // errorsService.log(error).subscribe();
      // errorsService.log(error).subscribe(errorWithContextInfo => this.ngZone.run(() => {
      //   router.navigate(['/error'], { queryParams: errorWithContextInfo });
      // }));
      errorsService.log(error);
      // Show notification to the user
      // return notificationService.notify(`${error.status} - ${error.message}`);
    } else {
      // Client Error Happend
      // Send the error to the server and then
      // redirect the user to the page with all the info
      // console.log('error-handler.ts -> Client Error.');
      // errorsService.log(error).subscribe(errorWithContextInfo => this.ngZone.run(() => {
      //   router.navigate(['/error'], { queryParams: errorWithContextInfo });
      // }));
      errorsService.log(error);
    }
  }
}
