import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ErrorsHandler } from './errors-handler';
import { ErrorsService } from './errors.service';

import { ErrorsComponent } from './errors-component/errors.component';
import { ErrorRoutingModule } from './errors-routing.module';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ErrorRoutingModule,
    MaterialModule,
    FlexLayoutModule
  ],
  declarations: [
    ErrorsComponent,
  ],
  providers: [
    ErrorsService,
    {
      provide: ErrorHandler,
      useClass: ErrorsHandler,
    },
  ]
})
export class ErrorsModule { }
