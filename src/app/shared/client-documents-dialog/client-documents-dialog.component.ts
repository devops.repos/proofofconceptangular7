import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface iClientDocumentsData {
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    pactClientSearchAuditId: number;
    optionUserId: number;
}

@Component({
    selector: 'app-client-documents-dialog',
    templateUrl: './client-documents-dialog.component.html'
})
export class ClientDocumentsDialogComponent implements OnInit {
    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) public clientDocumentsData: iClientDocumentsData,
        private dialogRef: MatDialogRef<ClientDocumentsDialogComponent>) {
    }
    //Close the dialog on close button
    CloseDialog() {
        this.dialogRef.close(true);
    }
    //On Init
    ngOnInit() {
    }
}