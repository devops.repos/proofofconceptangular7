import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { ClientDocumentsDialogComponent } from './client-documents-dialog.component';
import { ClientDocumentsModule } from '../client-documents/client-documents.module';

@NgModule({
  declarations: [ClientDocumentsDialogComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    ClientDocumentsModule
  ],
  exports: [ClientDocumentsDialogComponent]
})

export class ClientDocumentsDialogModule { }