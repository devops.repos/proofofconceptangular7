import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetDiagnosisComponent } from './det-diagnosis.component';

describe('DetDiagnosisComponent', () => {
  let component: DetDiagnosisComponent;
  let fixture: ComponentFixture<DetDiagnosisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetDiagnosisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetDiagnosisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
