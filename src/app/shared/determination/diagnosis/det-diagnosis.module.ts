import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import "ag-grid-enterprise";

import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { DetDiagnosisComponent } from './det-diagnosis.component';


@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([
        DetDiagnosisComponent
    ])
  ],
  declarations: [
    DetDiagnosisComponent
  ],
  exports: [
    DetDiagnosisComponent
  ]
})
export class DetDiagnosisModule { }
