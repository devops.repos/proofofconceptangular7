import { Component, OnInit, Input,Output, EventEmitter, ViewChild } from '@angular/core';
import { PACTClinicalMHRDiagnosisDetails } from '../../diagnosis/diagnosis.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';

@Component({
  selector: 'det-diagnosis',
  templateUrl: './det-diagnosis.component.html',
  styleUrls: ['./det-diagnosis.component.scss']
})
export class DetDiagnosisComponent implements OnInit {

  @Input('name')  titleName : string;
  @Input()  hasDiagnosis : boolean;
  @Input() options : PACTClinicalMHRDiagnosisDetails;
  @Input('diagnosis') diagnosisLst : PACTClinicalMHRDiagnosisDetails[];
  @Input() allDiagnosisList : PACTClinicalMHRDiagnosisDetails[];
  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  public gridOptions: GridOptions;
  rowData: PACTClinicalMHRDiagnosisDetails[];
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;
  context: any;
  //diagnosisGroup: FormGroup;

  diagnosisList : PACTClinicalMHRDiagnosisDetails[];
  tableHeader : string;
  
  constructor (private formBuilder: FormBuilder) 
  {
      this.gridOptions = {
        rowHeight: 30
      } as GridOptions;
      this.columnDefs = [
        {
          headerName: 'Diagnosis Id',
          field: 'diagnosisID',
          hide : true
        },
        {
          headerName: ' Diagnoses',
          field: 'description',
          width: 800,
          filter: 'agTextColumnFilter',
          sortable: false,
          resizable: false,
          suppressMenu: true,
          suppressSizeToFit: false,
          autoHeight:true,
          cellStyle: {
            'white-space': 'normal'
          },
          
        },
        {
          headerName: 'R/O',
          field: 'riskOf',
          width: 75,
          filter: false,
          sortable: false,
          resizable: false,
          suppressMenu: true,
          suppressSizeToFit: false,
          hide : false,
          cellRenderer: (params: { value: boolean }) => {
            return params.value === true ? 'Yes' : 'No';    
          },
          
        },
        {
          headerName: 'H/O',
          field: 'historyOf',
          width: 80,
          filter: false,
          sortable: false,
          resizable: false,
          suppressMenu: true,
          suppressSizeToFit: false,
          hide : false,
          cellRenderer: (params: { value: boolean }) => {
            return params.value === true ? 'Yes' : 'No';    
          },
          
        },
        {
          headerName: 'P/V',
          field: 'provisional',
          filter: false,
          sortable: false,
          resizable: false,
          suppressMenu: true,
          suppressSizeToFit: false,
          width: 75,
          hide : false,
          cellRenderer: (params: { value: boolean }) => {
            return params.value === true ? 'Yes' : 'No';    
          },
          
        }
      ];
      
      this.defaultColDef = {
        filter: true,
      };
      this.pagination = true;
      this.context = { componentParent: this };
    }
    
    ngOnInit() {
      this.refreshDiagnosisList();
      // if (this.hasDiagnosis) {
      //   this.diagnosisGroup.disable();
      // } else {
      //     this.diagnosisGroup.enable();
      // }

      if(this.options.diagnosisTypeID === 48) {
        this.tableHeader ='Medical';
      }
      else if (this.options.diagnosisTypeID === 49){      
        this.tableHeader ='Principal';
      }
      else{      
        this.tableHeader ='Other';
      }

  }


  onFirstDataRendered(params) {
    params.api.setDomLayout('autoHeight');
    params.api.sizeColumnsToFit();
  }

  onGridReady = (params: { api: any; columnApi: any }) => {
    this.refreshDiagnosisList();
    
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the diagnosis loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Diagnosis Available</span>';
    
    params.columnApi.getColumn('description').colDef.headerName= this.tableHeader + ' '+ this.gridOptions.columnDefs[1].headerName;
    
    this.gridOptions.api.refreshHeader();
    this.gridOptions.columnApi.setColumnVisible('riskOf', this.options.riskOf) //In that case we hide it
    this.gridOptions.columnApi.setColumnVisible('historyOf', this.options.historyOf) //In that case we hide it
    this.gridOptions.columnApi.setColumnVisible('provisional', this.options.provisional) //In that case we hide it
    params.api.setDomLayout('autoHeight');
    params.api.sizeColumnsToFit();
    
  }

  refreshDiagnosisList () {    
    if (this.diagnosisLst != undefined) {
      this.rowData = this.diagnosisLst.filter(x => x.isActive === true);
    }
  }

}
