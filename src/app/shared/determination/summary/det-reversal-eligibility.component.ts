import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DETClinicalHomelessMedicaidSummary } from './det-summary.model';
import { FormGroup, FormBuilder } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ClinicalHomelessMedicaidSummaryService } from './det-summary.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';
import { HomelesReviewService } from 'src/app/pact-modules/determination/homeless-review/homeless-review.service';

@Component({
    selector: 'det-reversal-eligibility',
    templateUrl: './det-reversal-eligibility.component.html',
    styleUrls: ['./det-reversal-eligibility.component.scss']
})

export class DetReversalEligibilityComponent {
    detClinicalHomelessMedicaidSummary: DETClinicalHomelessMedicaidSummary;
    detClinicalHomelessMedicaidSummaryID: number;
    overallEligibility: number;
    reverseEligibilityGroup: FormGroup;
    userData: AuthData;

    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any
        , private dialogRef: MatDialogRef<DetReversalEligibilityComponent>
        , private formBuilder: FormBuilder
        , private toastrService: ToastrService
        , private userService: UserService
        , private clinicalHomelessMedicaidSummaryService: ClinicalHomelessMedicaidSummaryService
        , private determinationSideNavService: DeterminationSideNavService
        , private homelessReviewService: HomelesReviewService) {
        if (data) {
            //console.log('Reverse Eligibility -', data);  
            this.detClinicalHomelessMedicaidSummary = data;
            this.detClinicalHomelessMedicaidSummaryID = this.detClinicalHomelessMedicaidSummary.detClinicalHomelessMedicaidSummaryID;
            this.overallEligibility = this.detClinicalHomelessMedicaidSummary.overallEligibility;
        }
    }

    ngOnInit() {

        //User Data
        this.userService.getUserData().subscribe(res => {
            if (res) {
                this.userData = res;
            }
        });

        this.reverseEligibilityGroup = this.formBuilder.group({
            explainReversal: ['']
        });

        if (this.detClinicalHomelessMedicaidSummary !== null) {
            if (this.detClinicalHomelessMedicaidSummary.explainReversal !== null && this.detClinicalHomelessMedicaidSummary.overallEligibility === 678)
                this.reverseEligibilityGroup.controls['explainReversal'].setValue(this.detClinicalHomelessMedicaidSummary.explainReversal);
            else
                this.reverseEligibilityGroup.controls['explainReversal'].setValue(null);
        }
    }

    //Reverse 
    reverse() {
        if (this.overallEligibility === 678) {
            if (this.reverseEligibilityGroup.get('explainReversal').value == null || this.reverseEligibilityGroup.get('explainReversal').value.trim() == "") {
                if (!this.toastrService.currentlyActive) {
                    this.toastrService.error("Reversal Reason is required field.");
                }
                return;
            }
            if (this.reverseEligibilityGroup.get('explainReversal').value.length == 1) {
                if (!this.toastrService.currentlyActive) {
                    this.toastrService.error("Reversal Reason cannot be less than 2 characters.");
                }
                return;
            }
        }
        this.detClinicalHomelessMedicaidSummary.explainReversal = this.reverseEligibilityGroup.get('explainReversal').value == null ? null : this.reverseEligibilityGroup.get('explainReversal').value.trim();
        this.detClinicalHomelessMedicaidSummary.detClinicalHomelessMedicaidSummaryID = this.detClinicalHomelessMedicaidSummaryID;
        this.detClinicalHomelessMedicaidSummary.overallEligibility = this.overallEligibility === 677 ? 678 : 677;
        this.detClinicalHomelessMedicaidSummary.userID = this.userData.optionUserId;
        this.clinicalHomelessMedicaidSummaryService.saveDETClinicalHomelessMedicaidSummary(this.detClinicalHomelessMedicaidSummary)
            .subscribe(data => {
                if (this.detClinicalHomelessMedicaidSummary.summaryType === 611) {
                    this.determinationSideNavService.setIsHomelessReviewDisabled(true);
                    this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(true);
                    this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(true);
                    this.determinationSideNavService.setIsDeterminationSummaryDisabled(true);
                    this.determinationSideNavService.setIsSignOffFollowUpDisabled(true);
                }
                else if (this.detClinicalHomelessMedicaidSummary.summaryType === 612) {
                    this.homelessReviewService.setHomelessReportTabEnabled(false);
                    this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(true);
                    this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(true);
                    this.determinationSideNavService.setIsDeterminationSummaryDisabled(true);
                    this.determinationSideNavService.setIsSignOffFollowUpDisabled(true);
                }
                this.dialogRef.close(true);
            },
                error => {
                    throw new Error(error.message);
                });
    }

    //Close the dialog on close button
    closeDialog() {
        this.dialogRef.close(true);
    }
}