import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { DETClinicalHomelessMedicaidSummary } from './det-summary.model';

@Component({
  selector: "det-summary-action",
  template: `
  <div *ngIf="!hideActionForGenPop && (params.data.overallEligibility == 678 || params.data.reasonInEligible > 0)">
    <mat-icon
      class="menu-icon"
      color="warn"
      [matMenuTriggerFor]="detSummaryAction">
      more_vert
    </mat-icon>
    <mat-menu #detSummaryAction="matMenu">
      <button mat-menu-item (click)="showReverseDialog()" class="menu-button">Reverse</button>
    </mat-menu>
  </div>
  `,
  styles: [
    `
      .menu-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 30px;
        width: 100%;
        height: 30px;
      }
    `
  ]
})

export class DetSummaryActionComponent implements ICellRendererAngularComp {
  public params: any;
  public cell: any;
  public hideActionForGenPop : boolean;
  detClinicalHomelessMedicaidSummary : DETClinicalHomelessMedicaidSummary = {
    detClinicalHomelessMedicaidSummaryID : null,
    pactApplicationID : null,
    summaryType : null,
    agreementPopulationID : null,
    agreementTypeName : null,
    overallEligibilityOrig : null,
    eligibility : null,
    demographicEligibility : null,
    overallEligibility : null,
    overallEligibilityDescription: null,
    reasonInEligible : null,
    reasonInEligibleDescription : null,
    explainReversal : null,
    userID: null
  };
  
  
  
  constructor(
    //public dialog: MatDialog
  ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    //Special Condition to hide Action for Gen Pop for Path 1 and 2
    if( (this.params.context.componentParent.clientCategoryType === 671
          || this.params.context.componentParent.clientCategoryType === 672)
        && this.params.data.agreementPopulationID === 45 
      )
      this.hideActionForGenPop = true;
    else
      this.hideActionForGenPop = false;  
  }
  
  showReverseDialog() {
    this.detClinicalHomelessMedicaidSummary.detClinicalHomelessMedicaidSummaryID = this.params.data.detClinicalHomelessMedicaidSummaryID;
    this.detClinicalHomelessMedicaidSummary.summaryType = this.params.data.summaryType;
    this.detClinicalHomelessMedicaidSummary.agreementPopulationID = this.params.data.agreementPopulationID;
    this.detClinicalHomelessMedicaidSummary.agreementTypeName = this.params.data.agreementTypeName;
    this.detClinicalHomelessMedicaidSummary.overallEligibility = this.params.data.overallEligibility;
    this.detClinicalHomelessMedicaidSummary.overallEligibilityDescription = this.params.data.overallEligibilityDescription;
    this.detClinicalHomelessMedicaidSummary.reasonInEligibleDescription = this.params.data.reasonInEligibleDescription;
    this.detClinicalHomelessMedicaidSummary.explainReversal = this.params.data.explainReversal;
    
    this.params.context.componentParent.openReverseDialog(this.detClinicalHomelessMedicaidSummary);
  }

  refresh(): boolean {
    return false;
  }
}
