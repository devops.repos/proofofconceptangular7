import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DETClinicalHomelessMedicaidSummary } from './det-summary.model';
import { DETClinicalHomelessMedicaidReviewInput } from 'src/app/pact-modules/determination/clinical-review/clinical-review.model';
import { DetSummaryActionComponent } from './det-summary-action.component';
import { ClinicalReviewService } from 'src/app/pact-modules/determination/clinical-review/clinical-review.service';
import { ClinicalHomelessMedicaidSummaryService } from './det-summary.service';
import { DetReversalEligibilityComponent } from './det-reversal-eligibility.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'det-summary',
  templateUrl: './det-summary.component.html',
  styleUrls: ['./det-summary.component.scss']
})
export class DetSummaryComponent implements OnInit {
  @Input() detClinicalHomelessMedicaidReviewInput: DETClinicalHomelessMedicaidReviewInput;
  @Output() calculateClinicalInEligible = new EventEmitter<boolean>();
  @Output() calculateHomelessInEligible = new EventEmitter<number>();

  //Variables
  clientCategoryType: number;
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  rowData: DETClinicalHomelessMedicaidSummary[];
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;
  context: any;
  homelessPath: number;

  constructor(private clinicalReviewService: ClinicalReviewService,
    private clinicalHomelessMedicaidSummaryService: ClinicalHomelessMedicaidSummaryService,
    public dialog: MatDialog) {
    this.columnDefs = [
      {
        headerName: 'DETClinicalHomelessMedicaidSummaryID',
        field: 'detClinicalHomelessMedicaidSummaryID',
        hide: true
      },
      {
        headerName: 'AgreementPopulationID',
        field: 'agreementPopulationID',
        hide: true
      },
      {
        headerName: ' Agreement Type',
        field: 'agreementTypeName',
        width: 250,
        //autoHeight: true,
        tooltipField: 'agreementTypeName'
      },
      {
        headerName: 'Eligibility',
        field: 'overallEligibilityDescription',
        width: 130,
      },
      {
        headerName: 'Reason Ineligible',
        field: 'reasonInEligibleDescription',
        width: 730,
        tooltipField: 'reasonInEligibleDescription'
      },
      {
        headerName: 'Explain Reversal',
        field: 'explainReversal',
        width: 500,
        tooltipField: 'explainReversal'
      },
      {
        headerName: 'Actions',
        field: 'action',
        width: 65,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.defaultColDef = {
      filter: false,
      sortable: false,
      resizable: true,
      suppressMenu: true,
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: DetSummaryActionComponent
    };
  }

  ngOnInit() {
  }

  onFirstDataRendered(params: any) {
    params.api.setDomLayout('autoHeight');
  }

  onGridReady = (params: { api: any; columnApi: any }) => {
    params.api.setDomLayout('autoHeight');
    this.overlayNoRowsTemplate = '<span style="color: #337ab7">No Summary To Show</span>';
    /** API call to get the grid data */
    this.getRowData();
  }

  getRowData() {
    if (this.detClinicalHomelessMedicaidReviewInput) {
      this.clientCategoryType = this.detClinicalHomelessMedicaidReviewInput.clientCategoryType;
      var summaryType = this.detClinicalHomelessMedicaidReviewInput.summaryType;
      this.clinicalHomelessMedicaidSummaryService.getDETClinicalHomelessMedicaidSummary(this.detClinicalHomelessMedicaidReviewInput)
        .subscribe(
          res => {
            const data = res as DETClinicalHomelessMedicaidSummary[];
            if (data) {
              this.rowData = data.filter(r => r.summaryType === summaryType);
              if (this.rowData) {
                //Clinical Review
                if (summaryType && summaryType === 611) {
                  //If in-eligible then set variable For next button routing
                  let clinicalResult = this.rowData.filter(x => x.overallEligibility === 677 && x.summaryType === summaryType);
                  let isClinicalInEligible: boolean;
                  if (clinicalResult !== null) {
                    isClinicalInEligible = clinicalResult.length > 0 ? false : true;
                  }
                  else {
                    isClinicalInEligible = true;
                  }
                  this.calculateClinicalInEligible.emit(isClinicalInEligible);
                }
                //Homeless Review
                else if (summaryType && summaryType === 612) {
                  let isHomelessEligible: boolean = false;
                  let isMedicaidPath: boolean = false;
                  let homelessResult = this.rowData.filter(x => x.overallEligibility === 677 && x.summaryType === summaryType);
                  if (homelessResult && homelessResult.length > 0) {
                    homelessResult.forEach(element => {
                      let clinicalResult = data.find((x: { agreementPopulationID: number; summaryType: number }) => x.agreementPopulationID === element.agreementPopulationID && x.summaryType === 611);
                      if (clinicalResult) {
                        if (element.overallEligibility === clinicalResult.overallEligibility) {
                          console.log("agree -", element.agreementPopulationID);
                          isHomelessEligible = true;
                        }
                      }
                    });
                  }
                  if (isHomelessEligible) {
                    let medicaidResult = this.rowData.filter(x => x.overallEligibility === 677 && x.summaryType === summaryType && (x.agreementPopulationID === 1 || x.agreementPopulationID === 30 || x.agreementPopulationID === 31 || x.agreementPopulationID === 32 || x.agreementPopulationID === 33 || x.agreementPopulationID === 34 || x.agreementPopulationID === 35));
                    if (medicaidResult && medicaidResult.length > 0) {
                      medicaidResult.forEach(element => {
                        let clinicalResult = data.find((x: { agreementPopulationID: number; summaryType: number }) => x.agreementPopulationID === element.agreementPopulationID && x.summaryType === 611);
                        if (clinicalResult) {
                          if (element.overallEligibility === 677 && clinicalResult.overallEligibility === 677) {
                            isMedicaidPath = true;
                          }
                        }
                      });
                    }
                    if (isMedicaidPath) {
                      this.homelessPath = 2;
                    }
                    else {
                      this.homelessPath = 3;
                    }
                  }
                  else {
                    this.homelessPath = 1;
                  }
                  this.calculateHomelessInEligible.emit(this.homelessPath);
                }
              }
              else {
                this.rowData = null;
              }
            }
            else {
              this.rowData = null;
            }
          },
          error => {
            throw new Error(error.message);
          }
        );
    }
  }

  //Open Dialog
  openReverseDialog(detClinicalHomelessMedicaidSummary: DETClinicalHomelessMedicaidSummary): void {
    this.dialog.open(DetReversalEligibilityComponent, {
      width: '800px',
      maxHeight: '450px',
      disableClose: true,
      autoFocus: false,
      data: detClinicalHomelessMedicaidSummary
    }).afterClosed().subscribe(() => {
      this.getRowData();
    }
    );
  }
}
