export class DETClinicalHomelessMedicaidSummary {
    detClinicalHomelessMedicaidSummaryID? : number;
    pactApplicationID? : number;
    summaryType : number;
    agreementPopulationID : number;
    agreementTypeName : string;
    overallEligibilityOrig : number;
    eligibility : number;
    demographicEligibility : number;
    overallEligibility : number;
    overallEligibilityDescription: string;
    reasonInEligible : number;
    reasonInEligibleDescription : string;
    explainReversal : string;
    userID?: number;
}



        
        
        