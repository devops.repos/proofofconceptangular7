import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetSummaryComponent } from './det-summary.component';

describe('SummaryComponent', () => {
  let component: DetSummaryComponent;
  let fixture: ComponentFixture<DetSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
