import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

//Model References
import { DETClinicalHomelessMedicaidReviewInput } from 'src/app/pact-modules/determination/clinical-review/clinical-review.model';
import { DETClinicalHomelessMedicaidSummary } from './det-summary.model';


@Injectable({
  providedIn: 'root'
})
export class ClinicalHomelessMedicaidSummaryService {
  //Clinical Summary
  getDETClinicalHomelessMedicaidSummaryUrl = environment.pactApiUrl + 'DETClinicalReview/GetDETClinicalHomelessMedicaidSummary';
  saveDETClinicalHomelessMedicaidSummaryUrl = environment.pactApiUrl + 'DETClinicalReview/SaveDETClinicalHomelessMedicaidSummary';
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),
    response : "json",
  };
  
  constructor(private httpClient: HttpClient) { }
  //Clinical Summary
  getDETClinicalHomelessMedicaidSummary(detClinicalReviewInput : DETClinicalHomelessMedicaidReviewInput) : Observable<any>
  {
     return this.httpClient.post(this.getDETClinicalHomelessMedicaidSummaryUrl, JSON.stringify(detClinicalReviewInput), this.httpOptions);
  }

  //Clinical Summary
  saveDETClinicalHomelessMedicaidSummary(detClinicalHomelessMedicaidSummary : DETClinicalHomelessMedicaidSummary) : Observable<any>
  {
     return this.httpClient.post(this.saveDETClinicalHomelessMedicaidSummaryUrl, JSON.stringify(detClinicalHomelessMedicaidSummary), this.httpOptions);
  }


}

