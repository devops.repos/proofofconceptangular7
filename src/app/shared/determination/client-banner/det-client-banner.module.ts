import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';

import { DetClientBannerComponent } from './det-client-banner.component';

@NgModule({
  declarations: [
    DetClientBannerComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    RouterModule
  ],
  exports: [
    DetClientBannerComponent
  ]
})
export class DetClientBannerModule { }
