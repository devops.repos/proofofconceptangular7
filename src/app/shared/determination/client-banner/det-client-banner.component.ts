import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { Subscription } from 'rxjs';
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'

@Component({
  selector: 'det-client-banner',
  templateUrl: './det-client-banner.component.html',
  styleUrls: ['./det-client-banner.component.scss']
})

export class DetClientBannerComponent implements OnInit, OnDestroy {

  @Input() isHousingProgramType: boolean;
  @Input() canShowApplicationLink: boolean = true;
  @Input() canShowClientCaseFolderLink: boolean = true;

  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  constructor(private applicationDeterminationService: ApplicationDeterminationService) { }

  ngOnInit() {
    /** Getting the application details for determination ApplicationDeterminationService */
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
      if (res) {
        const data = res as iApplicationDeterminationData;
        if (data) {
          this.applicationDeterminationData = data;
          if (this.applicationDeterminationData.reasonToRejectType === 33 || this.applicationDeterminationData.reasonToWithdrawType === 33) {
            this.isHousingProgramType = false;
          }
        }
      }
    });
  }

  ngOnDestroy() {
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }
}
