import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetClientBannerComponent } from './det-client-banner.component';

describe('DetClientBannerComponent', () => {
  let component: DetClientBannerComponent;
  let fixture: ComponentFixture<DetClientBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetClientBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetClientBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
