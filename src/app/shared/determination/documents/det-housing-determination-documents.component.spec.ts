import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetHousingDeterminationDocumentsComponent } from './det-housing-determination-documents.component';

describe('DetHousingDeterminationDocumentsComponent', () => {
  let component: DetHousingDeterminationDocumentsComponent;
  let fixture: ComponentFixture<DetHousingDeterminationDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetHousingDeterminationDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetHousingDeterminationDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
