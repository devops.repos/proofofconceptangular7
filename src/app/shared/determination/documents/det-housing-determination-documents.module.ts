import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { DetHousingDeterminationDocumentsComponent } from './det-housing-determination-documents.component';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    DetHousingDeterminationDocumentsComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    AgGridModule.withComponents()
  ],
  exports: [
    DetHousingDeterminationDocumentsComponent
  ]
})
export class DetHousingDeterminationDocumentsModule { }
