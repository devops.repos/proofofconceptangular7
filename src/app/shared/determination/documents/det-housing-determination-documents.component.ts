import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { HttpEventType } from '@angular/common/http'
import { Subscription } from 'rxjs';
import { GridOptions } from 'ag-grid-community';
import { ToastrService } from 'ngx-toastr';

//Service
import { DocumentService } from 'src/app/shared/document/document.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { CommonService } from 'src/app/services/helper-services/common.service';

//Models
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { AuthData } from 'src/app/models/auth-data.model';
import { iHousingDeterminationDocumentsData } from 'src/app/models/determination-documents.model';
import { iDocumentPackage, iDocumentLink } from 'src/app/models/document-package.model'

@Component({
  selector: 'app-det-housing-determination-documents',
  templateUrl: './det-housing-determination-documents.component.html',
  styleUrls: ['./det-housing-determination-documents.component.scss']
})

export class DetHousingDeterminationDocumentsComponent implements OnInit, OnDestroy {
  //Input
  @Input() housingDeterminationDocumentsData: iHousingDeterminationDocumentsData;
  @Input() isAgencySiteVisible: boolean = true;

  //Global Variables
  columnDefs = [];
  rowData = [];
  defaultColDef = {};
  gridColumnApi: any;
  gridOptions: GridOptions;
  overlayNoRowsTemplate: string;

  reportParams: PACTReportUrlParams;
  reportParameters: PACTReportParameters[] = [];

  userData: AuthData;
  userDataSub: Subscription;

  determinationLetterGuid: string;
  applicationSummaryGuid: string;
  mentalHealthReportGuid: string;
  psychiarticEvalReportGuid: string;
  psychosocialEvalReportGuid: string;
  vulnerabilityAssessmentReportGuid: string;
  housingHomelessGuid: string;
  determinationSummaryReportGuid: string;

  //Constructor
  constructor(private commonService: CommonService,
    private message: ToastrService,
    private userService: UserService,
    private documentService: DocumentService) {
    //Grid Column Definitions
    this.columnDefs = [
      { headerName: 'Document Type', field: 'documentTypeDescription', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Description', field: 'documentDescription', filter: 'agTextColumnFilter', width: 300 },
      {
        headerName: 'Document Name',
        field: 'documentName',
        cellRenderer: (params: { value: string; data: { pactDocumentID: number; documentType: number; documentExtension: string; fileNetDocID: number; capsReportID: string }; }) => {
          var link = document.createElement('a');
          link.href = '#';
          link.innerText = params.value;
          link.addEventListener('click', (e) => {
            e.preventDefault();
            this.commonService.setIsOverlay(true);
            this.openDocument(params.data.pactDocumentID, params.data.documentType, params.data.documentExtension, params.data.fileNetDocID, params.data.capsReportID);
          });
          return link;
        },
        filter: 'agTextColumnFilter',
        width: 300
      },
      { headerName: 'Attached Date-Time', field: 'createdDateTime', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Attached By', field: 'createdByName', filter: 'agTextColumnFilter', width: 200 },
      {
        headerName: 'Agency/Site',
        field: 'agencySiteName',
        filter: 'agTextColumnFilter',
        width: 350
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  ngOnInit() {
    //Get User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => this.userData = res);
  }

  //Destroy 
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

  //On Grid Ready
  onGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.gridColumnApi = params.columnApi;
    this.overlayNoRowsTemplate = '<span style="color: #337ab7">No Documents To Show</span>';
    this.populateGrid();
    if (!this.isAgencySiteVisible) {
      this.gridColumnApi.setColumnVisible('agencySiteName', false);
    }
  }

  //Populate Grid
  populateGrid() {
    if (this.housingDeterminationDocumentsData && this.housingDeterminationDocumentsData.pactApplicationId) {
      this.commonService.getDocumentPackage(this.housingDeterminationDocumentsData.pactApplicationId)
        .subscribe(res => {
          if (res) {
            const data = res as iDocumentPackage[];
            if (data) {
              if (this.housingDeterminationDocumentsData.documentType && this.housingDeterminationDocumentsData.documentType.length > 0) {
                this.rowData = data.filter(d => this.housingDeterminationDocumentsData.documentType.includes(d.documentType));
              }
              else {
                this.rowData = data
              }
            }
            else {
              this.rowData = null;
            }
          }
          else {
            this.rowData = null;
          }
        });
    }
  }

  //Open Document
  openDocument(pactDocumentId: number, documentType: number, documentExtension: string, fileNetDocID: number, capsReportID: string) {
    //File Net
    if (fileNetDocID) {
      this.commonService.getFileNetDocumentLink(fileNetDocID.toString()).subscribe(res => {
        const data = res as iDocumentLink;
        if (data) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    }
    else {
      //SSRS
      if (documentType === 331) {
        if (capsReportID) {
          this.commonService.displaySurveySummaryReport(capsReportID, this.userData.optionUserId);
        }
      }
      else if (documentType === 326 || documentType === 327 || documentType === 328 || documentType === 329 || documentType === 330 || documentType === 332 || documentType === 334 || documentType === 874) {
        this.viewSSRSDocument(documentType);
      }
      else {
        //Storage MTC
        if (pactDocumentId && documentExtension) {
          var docName = pactDocumentId.toString() + "." + documentExtension;
          this.documentService.viewDocument(docName).subscribe(
            data => {
              switch (data.type) {
                case HttpEventType.Response:
                  const downloadedFile = new Blob([data.body], {
                    type: data.body.type
                  });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(downloadedFile, docName);
                  } else {
                    this.commonService.OpenWindow(URL.createObjectURL(downloadedFile));
                    this.commonService.setIsOverlay(false);
                  }
                  break;
              }
            }, error => {
              this.message.error('There was an error in opening the document.', 'Viewing Document Failed!');
            });
        }
      }
    }
  }

  //View SSRS Document
  viewSSRSDocument(documentType: number) {
    switch (documentType) {
      case 326: {
        if (!this.determinationLetterGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingDeterminationDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "DeterminationLetter", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.determinationLetterGuid = res;
                  this.generateReport(res, "DeterminationLetter");
                }
              }
            );
        }
        else {
          this.generateReport(this.determinationLetterGuid, "DeterminationLetter");
        }
        break;
      }
      case 327: {
        if (!this.applicationSummaryGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingDeterminationDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PACTReportApplicationSummary", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.applicationSummaryGuid = res;
                  this.generateReport(res, "PACTReportApplicationSummary");
                }
              }
            );
        }
        else {
          this.generateReport(this.applicationSummaryGuid, "PACTReportApplicationSummary");
        }
        break;
      }
      case 328: {
        if (!this.mentalHealthReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingDeterminationDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "MentalHealthReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.mentalHealthReportGuid = res;
                  this.generateReport(res, "MentalHealthReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.mentalHealthReportGuid, "MentalHealthReport");
        }
        break;
      }
      case 329: {
        if (!this.psychiarticEvalReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingDeterminationDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PsychiarticEvalReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.psychiarticEvalReportGuid = res;
                  this.generateReport(res, "PsychiarticEvalReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.psychiarticEvalReportGuid, "PsychiarticEvalReport");
        }
        break;
      }
      case 330: {
        if (!this.psychosocialEvalReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingDeterminationDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PsychosocialEvalReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.psychosocialEvalReportGuid = res;
                  this.generateReport(res, "PsychosocialEvalReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.psychosocialEvalReportGuid, "PsychosocialEvalReport");
        }
        break;
      }
      case 332: {
        if (!this.vulnerabilityAssessmentReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingDeterminationDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "SVAReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.vulnerabilityAssessmentReportGuid = res;
                  this.generateReport(res, "SVAReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.vulnerabilityAssessmentReportGuid, "SVAReport");
        }
        break;
      }
      case 334: {
        if (!this.housingHomelessGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingDeterminationDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "HHHReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.housingHomelessGuid = res;
                  this.generateReport(res, "HHHReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.housingHomelessGuid, "HHHReport");
        }
        break;
      }
      case 874: {
        if (!this.determinationSummaryReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingDeterminationDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "DeterminationSummaryReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.determinationSummaryReportGuid = res;
                  this.generateReport(res, "DeterminationSummaryReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.determinationSummaryReportGuid, "DeterminationSummaryReport");
        }
        break;
      }
      default: {
        break;
      }
    }
  }

  //Generate Report
  generateReport(reportGuid: string, reportName: string) {
    this.reportParams = { reportParameterID: reportGuid, reportName: reportName, "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.commonService.OpenWindow(URL.createObjectURL(data));
            this.commonService.setIsOverlay(false);
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }
}
