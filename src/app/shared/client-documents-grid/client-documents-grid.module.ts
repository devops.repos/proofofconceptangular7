import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { ClientDocumentsGridComponent } from './client-documents-grid.component';
import { AgGridModule } from 'ag-grid-angular';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [ClientDocumentsGridComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    AgGridModule.withComponents(),
    NgxMaskModule
  ],
  exports: [ClientDocumentsGridComponent]
})

export class ClientDocumentsGridModule { }