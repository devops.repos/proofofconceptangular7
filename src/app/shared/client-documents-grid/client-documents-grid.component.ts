import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { GridOptions } from 'ag-grid-community';
import { CommonService } from 'src/app/services/helper-services/common.service';

export interface iDocumentLinkSearch {
  fileNetDocID: string;
  fileNetLibrary: string;
}

export interface iDocumentLink {
  linkURL: string;
}

export interface iDocumentSearchResults {
  fileNetDocID: number;
  fileNetDocTypeDescription: string;
  fileNetDocDate: string;
  searchCIN: string;
  searchCaseNumber: string;
  fileNetDocTypeID: number;
  fileNetLibrary: string;
  validationMessage: string;
}

@Component({
  selector: 'app-client-documents-grid',
  templateUrl: './client-documents-grid.component.html',
  styleUrls: ['./client-documents-grid.component.scss']
})

export class ClientDocumentsGridComponent implements OnInit {
  @Input() clientDocumentsData: {
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    pactClientSearchAuditId: number;
    optionUserId: number;
  };

  //Variables
  fullName: string;
  documentsLinkSearch: iDocumentLinkSearch = { fileNetDocID: null, fileNetLibrary: null };
  columnDefs = [];
  rowData: iDocumentSearchResults[];
  defaultColDef = {};
  gridOptions: GridOptions;
  overlayNoRowsTemplate: string;
  errorMessage: string;

  constructor(
    private httpClient: HttpClient,
    private message: ToastrService,
    private commonService: CommonService) {
    //Grid Column Definitions
    this.columnDefs = [
      {
        headerName: 'Document Description',
        field: 'fileNetDocTypeDescription',
        cellRenderer: (params: { value: string; data: { fileNetDocID: string; fileNetLibrary: string; }; }) => {
          var link = document.createElement('a');
          link.href = '#';
          link.innerText = params.value;
          link.addEventListener('click', (e) => {
            e.preventDefault();
            this.commonService.setIsOverlay(true);
            this.documentsLinkSearch.fileNetDocID = params.data.fileNetDocID;
            this.documentsLinkSearch.fileNetLibrary = params.data.fileNetLibrary;
            if (this.documentsLinkSearch) {
              this.getClientDocumentLink(this.documentsLinkSearch).subscribe(res => {
                const data = res as iDocumentLink;
                if (data) {
                  this.commonService.OpenWindow(data.linkURL);
                  this.commonService.setIsOverlay(false);
                }
              });
            }
          });
          return link;
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      { headerName: 'CIN', field: 'searchCIN', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'CASE #', field: 'searchCaseNumber', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Entry Date', field: 'fileNetDocDate', filter: 'agTextColumnFilter', width: 100 }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  ngOnInit() {
    if (this.clientDocumentsData
      && this.clientDocumentsData.lastName != null
      && this.clientDocumentsData.firstName != null) {
      this.fullName = this.clientDocumentsData.lastName + ", " + this.clientDocumentsData.firstName;
      this.populateGrid();
    }
  }

  clientDocumentsURL = environment.pactApiUrl + 'Document/GetClientDocuments';
  clientDocumentLinkURL = environment.pactApiUrl + 'Document/GetClientDocumentLink';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  //Search and get client documents list - Api call
  searchClientDocuments(): Observable<any> {
    return this.httpClient.post(this.clientDocumentsURL, JSON.stringify(this.clientDocumentsData), this.httpOptions);
  }

  //Get client document link - Api call
  getClientDocumentLink(documentLinkSearch: iDocumentLinkSearch): Observable<any> {
    return this.httpClient.post(this.clientDocumentLinkURL, JSON.stringify(documentLinkSearch), this.httpOptions);
  }

  //OnGrid Ready
  onGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.overlayNoRowsTemplate = '<span style="color: #337ab7">No Documents To Show</span>';
    params.api.sizeColumnsToFit();
  }

  //Populate Grid
  populateGrid() {
    if (this.clientDocumentsData
      && this.clientDocumentsData.firstName && this.clientDocumentsData.lastName && this.clientDocumentsData.ssn && this.clientDocumentsData.dob != null) {
      this.searchClientDocuments()
        .subscribe(res => {
          if (res) {
            const data = res as iDocumentSearchResults[];
            if (data && data.length > 0) {
              if (data[0].validationMessage != null) {
                if (!this.message.currentlyActive) {
                  this.message.error(data[0].validationMessage)
                }
                this.rowData = null;
              }
              else {
                this.rowData = data;
              }
            }
            else {
              this.rowData = null;
            }
          }
          else {
            this.rowData = null;
          }
        },
          error => {
            this.overlayNoRowsTemplate = '<span style="color: #337ab7">No Documents To Show</span>';
            this.rowData = null;
            if (!this.message.currentlyActive) {
              this.message.error("File Net search service is currently unavailable. Please try again later!");
            }
            this.errorMessage = error;
          }
        );
    }
  }
}
