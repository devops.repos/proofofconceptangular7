import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDocumentsGridComponent } from './client-documents-grid.component';

describe('ClientDocumentsGridComponent', () => {
  let component: ClientDocumentsGridComponent;
  let fixture: ComponentFixture<ClientDocumentsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDocumentsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDocumentsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
