import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { IReferralRosterList, IPlacementOutcome, IVCSOnlineUnits, IVCSIncomeSourceType } from 'src/app/pact-modules/vacancyControlSystem/referral-roster/referral-roster-interface.model';
import { AuthData } from 'src/app/models/auth-data.model';
import { VCSAvailableInterviewTimeSlot, VCSAvailableInterviewTimeSlotInput, VCSPlacementMatchCriteria } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v-interfaces.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { Subscription, BehaviorSubject } from 'rxjs';
import { ReferralRosterService } from 'src/app/pact-modules/vacancyControlSystem/referral-roster/referral-roster.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { C2vService } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { UserService } from 'src/app/services/helper-services/user.service';
import { Router } from '@angular/router';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { IVCSTenantRosterList, ISideDoorClientSearchResult, IVCSHpAgenciesWithTrackedSites, IVCSMoveInValidator, IVCSAgencies } from 'src/app/pact-modules/vacancyControlSystem/tenant-roster/tenant-roster-interface.model';
import { TenantRosterService } from 'src/app/pact-modules/vacancyControlSystem/tenant-roster/tenant-roster.service';
import { ConfirmDialogService } from '../confirm-dialog/confirm-dialog.service';
import { MatTabChangeEvent } from '@angular/material';
import { TadService } from 'src/app/pact-modules/vacancyControlSystem/tad-submission/tad.service';

@Component({
  selector: 'app-placement-outcome',
  templateUrl: './placement-outcome.component.html',
  styleUrls: ['./placement-outcome.component.scss'],
  providers: [DatePipe]
})
export class PlacementOutcomeComponent implements OnInit, OnDestroy {

  @Input() referralType: number = -1;  //525 = Regular Referral, 526 = Supportive Housing SideDoor Referral, 610 = Non-Supportive Housing SideDoor Referral
  panelExpanded = true;
  selectedTab = 0;
  isMainFormTabDisabled = false;
  isDocTabDisabled = true;
  rrSelected: IReferralRosterList = {
    vcsReferralID: 0,
    pactApplicationID: 0,
    clientID: 0,
    referralDate: '',
    firstName: '',
    lastName: '',
    dateOfBirth: '',
    approvalExpiryDate: '',
    referringAgencyID: 0,
    referringAgencyNo: '',
    referringSiteID: 0,
    referringSiteNo: '',
    referringAgencyName: '',
    referringSiteName: '',
    hpAgencyID: 0,
    hpAgencyNo: '',
    hpSiteID: 0,
    hpSiteNo: '',
    hpAgencyName: '',
    hpSiteName: '',
    ssn: '',
    clientEligibleFor: '',
    svaPrioritization: '',
    serviceNeeds: '',
    placementCriteria: '',
    primaryServiceContractID: 0,
    primaryServiceContractName: '',
    agreementPopulationID: 0,
    populationName: '',
    siteAgreementPopulationID: 0,
    agreementTypeDescription: '',
    siteLocationType: 0,
    siteLocationTypeDescription: '',
    siteAddress: '',
    unitsOccupied: 0,
    unitsAvailable: 0,
    totalUnits: 0,
    pendingReferrals: 0,
    referralStatusType: 0,
    referralStatusTypeDescription: '',
    referralType: 0,
    unitType: 0,
    unitTypeDescription: '',
    matchPercentage: 0,
    withdrawnReasonType: 0,
    referralClosedComment: '',
    interviewDate: '',
    interviewTime: '',
    interviewAddress: '',
    interviewCity: '',
    interviewState: '',
    interviewZip: '',
    interviewPhoneNo: '',
    interviewLocation: '',
    canHPScheduleInterview: false,
    referredBy: '',
    referredDate: '',
    updatedBy: '',
    updatedDate: '',
    vcsPlacementMoveInID: 0
  };
  trSelected: IVCSTenantRosterList = {
    hpAgencyID: 0,
    hpAgencyNo: '',
    hpSiteID: 0,
    hpSiteNo: '',
    hpAgencyName: '',
    hpSiteName: '',
    primaryServiceContractID: 0,
    primaryServiceContractName: '',
    agreementPopulationID: 0,
    populationName: '',
    siteAgreementPopulationID: 0,
    agreementTypeDescription: '',
    unitID: 0,
    unitName: '',
    unitType: 0,
    unitTypeDescription: '',
    unitStatusType: 0,
    unitStatusTypeDescription: '',
    rentalSubsidies: '',
    isSideDoorAllowed: false,
    isMonitorVacancy: false,
    referralType: 0,
    tenantFirstName: '',
    tenantLastName: '',
    raAgencyID: 0,
    raAgencyNo: '',
    raAgencyName: '',
    raSiteID: 0,
    raSiteNo: '',
    raSiteName: '',
    tenantEligibility: '',
    svaPrioritization: '',
    moveInDate: '',
    moveOutDate: '',
    moveInVerificationStatusType: 0,
    moveInVerificationStatusDescription: '',
    moveInVerificationDate: '',
    moveOutVerificationStatusType: 0,
    moveOutVerificationStatusDescription: '',
    moveOutVerificationDate: '',
    applicableTab: 0,
    isApplicationMatchRequired: false,
    clientID: 0,
    clientSourceType: 0,
    vcsPlacementMoveInID: 0,
    vcsPlacementMoveInVerificationID: 0,
    vcsPlacementMoveOutID: 0,
    vcsPlacementMoveOutVerificationID: 0,
    siteLocationType: 0,
    siteLocationTypeDescription: '',
    siteAddress: '',
    isVerified: false,
    offlineReasonType: 0,
    offlineReasonTypeDescription: '',
    offlineOtherSpecify: '',
    expectedAvailableDate: '',
    unitAddlComment: '',
    unitFeatures: '',
    siteTrackedType: 0,
    contractingAgencyType: 0,
    referralDate: '',
    moveInSubmittedBy: '',
    moveInSubmittedDate: '',
    moveOutSubmittedBy: '',
    moveOutSubmittedDate: '',
  }
  sideDoorClientSearchResult: ISideDoorClientSearchResult = {
    sideDoorClient: {
      unitID: 0,
      clientSourceType: 0,
      clientID: 0,
      vcsid: '',
      firstName: '',
      lastName: '',
      referralDate: '',
      ssn: '',
      dob: '',
      genderType: 0,
      genderTypeDescription: '',
      userID: 0,
    },
    sideDoorClientReferral: {
      pactApplicationID: 0,
      raAgencyID: 0,
      raAgencyNo: '',
      raAgencyName: '',
      raSiteID: 0,
      raSiteNo: '',
      raSiteName: '',
      eligibility: '',
      serviceNeeds: '',
      placementCriteria: '',
      svaPrioritization: '',
      isEligibilityCriteriaMatched: false,
      isReferralPending: false,
      isApprovalActive: false,
      matchPercentage: 0,
    },
    placementMatchCriteria: []
  }

  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  referralDate: any;
  ssReferralDate: any;

  currentUser: AuthData;
  is_SH_HP = false;
  is_SH_PE = false;
  isIntrvwEditable = false;
  placementOutcomeFormEditable = false;
  eligibilityCriteriaNotMatched = false;
  approvalNotActive = false;

  isSideDoorMoveInDone = false;
  vcsReferralIDforSidedoor: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  existingPlacementOutcome: IPlacementOutcome = {
    vcsReferralID: 0,
    interviewDate: '',
    interviewTime: '',
    wasInterviewConducted: 0,
    interviewOutcomeType: 0,
    placementOutcomeType: 0,
    expectedMoveInDate: '',
    expectedUnitID: 0,
    moveInDate: '',
    unitID: 0,
    unitName: '',
    initialClientRentalContribution: 0,
    initialIncomeSourceList: [],
    initialIncomeSourceOtherSpecify: '',
    clientRentalContribution: 0,
    incomeSourceList: [],
    incomeSourceOtherSpecify: '',
    reasonType: 0,
    comments: '',
    referralReceivedType: 0,
    referralReceivedDate: '',
    referralReceivedOtherSpecify: '',
    pactApplicationID: 0,
    vcsID: '',
    clientID: 0,
    clientSourceType: 0,
    referringAgencyID: 0,
    referringAgencyNo: '',
    referringAgencyName: '',
    referringSiteID: 0,
    hpAgencyID: 0,
    hpSiteID: 0,
    siteAgreementPopulationID: 0,
    referralGroupGUID: '',
    referralType: 0,
    isScheduleMandatory: 0,
    unitType: 0,
    matchPercent: 0,
    referralStatusType: 0,
    cocType: 0,
    withdrawnReasonType: 0,
    referralClosedComment: '',
    isEligibilityCriteriaMatched: false,
    isApprovalActive: false,
    placementMatchCriteriaList: [],
    firstName: '',
    lastName: '',
    ssn: '',
    dob: '',
    gender: '',
    referralDate: '',
    eligibility: '',
    placementCriteria: '',
    svaPrioritization: '',
    serviceNeeds: '',
    createdByName: '',
    createdDate: '',
    updatedByName: '',
    updatedDate: '',
  };
  availableInterviewTimeSlot: VCSAvailableInterviewTimeSlot[];
  vcsOnlineUnits: IVCSOnlineUnits[];
  vcsMatchCriteria: VCSPlacementMatchCriteria[];

  interviewOutcomeGroup: FormGroup;

  selectedIncomeSourceForForm: number[] = [];
  selectedInitialIncomeSourceForForm: number[] = [];
  selectedIncomeSource: IVCSIncomeSourceType[] = [];
  isThereOtherIncomeSource = false;
  wasThereInitialOtherIncomeSource = false;
  isThereOtherIncomeSourceForUpdate = false;

  incomeSourceUpdated = false;

  interviewOutcome: RefGroupDetails[];
  placementOutcome: RefGroupDetails[];
  wasInterviewConducted: RefGroupDetails[];
  interviewNotConductedReasons: RefGroupDetails[];
  hpDidNotAcceptClientReasons: RefGroupDetails[];
  clientDidNotAcceptHousingReasons: RefGroupDetails[];
  interviewOutcomePendingReasons: RefGroupDetails[];
  incomeSource: RefGroupDetails[];

  allActiveAgencies: IVCSHpAgenciesWithTrackedSites[];
  moveInValidator: IVCSMoveInValidator;

  isTadWorkflow = false;

  commonServiceSub: Subscription;
  rrSelectedSub: Subscription;
  trSelectedSub: Subscription;
  currentUserSub: Subscription;
  placementOutcomeFormEditableSub: Subscription;
  existingPlacementOutcomeSub: Subscription;
  availableInterviewTimeSlotSub: Subscription;
  vcsOnlineUnitsSub: Subscription;
  vcsMatchCriteriaSub: Subscription;
  wasInterviewConductedCtrlSub: Subscription;
  interviewOutcomeCtrlSub: Subscription;
  placementOutcomeCtrlSub: Subscription;
  sideDoorClientSearchResultSub: Subscription;
  raHpAgenciesSub: Subscription;
  moveInValidatorSub: Subscription;
  saveVCSPlacementOutcomeSub: Subscription;
  istadWorkflowSub: Subscription;

  constructor(
    private referralRosterService: ReferralRosterService,
    private tenantRosterService: TenantRosterService,
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private c2vService: C2vService,
    private toastrService: ToastrService,
    private datePipe: DatePipe,
    private userService: UserService,
    private router: Router,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService
  ) {
    this.interviewOutcomeGroup = this.formBuilder.group({
      interviewDateCtrl: ['', Validators.required],
      interviewTimeCtrl: [-1, Validators.required],
      wasInterviewConductedCtrl: [-1, Validators.required],
      referralReceivedFromCtrl: [-1, Validators.required],
      specifyReferralReceivedCtrl: ['', Validators.required],
      referralReceivedDateCtrl: ['', Validators.required],
      reasonCtrl: [-1, Validators.required],
      commentCtrl: [''],
      interviewOutcomeCtrl: [-1, Validators.required],
      placementOutcomeCtrl: [-1, Validators.required],
      expectedMoveInDateCtrl: ['', Validators.required],
      moveInDateCtrl: ['', Validators.required],
      unitNumberCtrl: [-1, Validators.required],
      clientRentalContributionCtrl: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(1)]],
      incomeSourceCtrl: [this.selectedIncomeSource, Validators.required],
      specifyIncomeSourceCtrl: ['', Validators.required]
    });
  }

  ngOnInit() {
    // console.log(`referralType: ` + this.referralType);
    // Get Refgroup Details
    const refGroupList = '7,63,64,65,66,67,68,69';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.interviewOutcome = data.filter(d => d.refGroupID === 63);
        this.placementOutcome = data.filter(d => d.refGroupID === 64);
        this.wasInterviewConducted = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
        this.interviewNotConductedReasons = data.filter(d => d.refGroupID === 65);
        this.hpDidNotAcceptClientReasons = data.filter(d => d.refGroupID === 66);
        this.clientDidNotAcceptHousingReasons = data.filter(d => d.refGroupID === 67);
        this.interviewOutcomePendingReasons = data.filter(d => d.refGroupID === 68);
        this.incomeSource = data.filter(d => d.refGroupID === 69);
      },
      error => {
        throw new Error(error.message);
      }
    );

    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
        /** Checking the User siteCategoryType for HP */
        this.currentUser.siteCategoryType.forEach(ct => {
          if (ct.siteCategory === UserSiteType.SH_HP) {
            this.is_SH_HP = true;
          } else if (ct.siteCategory === UserSiteType.SH_PE) {
            this.is_SH_PE = true;
          }
        });
      }
    });

    /* Required for sideDoor MoveIn and MoveOut */
    /** Get all the active HP Agencies List with only tracked Sites for the ReferralReceivedFrom dropdown */
    // this.raHpAgenciesSub = this.tenantRosterService.getVCSHpAgenciesWithTrackedSites().subscribe((agencies: IVCSHpAgenciesWithTrackedSites[]) => {
    //   if (agencies) {
    //     this.allActiveAgencies = agencies;
    //   }
    // });
    this.raHpAgenciesSub = this.tenantRosterService.getVCSAllActiveAgencies().subscribe((agencies: IVCSAgencies[]) => {
      if (agencies) {
        this.allActiveAgencies = agencies;
      }
    });

    if (this.referralType === 525) {
      /** REGULAR REFERRAL MOVE-IN */
      this.isDocTabDisabled = false;
      /** Get the Editable Flag to make PlacementOutcome form editable or readonly */
      this.placementOutcomeFormEditableSub = this.referralRosterService.getPlacementOutcomeEditableFlag().subscribe(flag => {
        if (flag) {
          this.placementOutcomeFormEditable = flag;
        } else {
          this.interviewOutcomeGroup.disable();
        }
      });
      /** Get the selected ReferralRoster Details from the ReferralRosterService */
      this.rrSelectedSub = this.referralRosterService.getReferralRosterSelected().subscribe(rr => {
        if (rr) {
          this.rrSelected = rr;
          // console.log('this.rrSelected: ', this.rrSelected);
          /* Get the List of Match Criteria from the Match Logic for the selected VCSReferralID */
          if (this.rrSelected.vcsReferralID) {
            this.vcsMatchCriteriaSub = this.referralRosterService.getVCSMatchCriteriaByVCSReferralID(this.rrSelected.vcsReferralID).subscribe((match: VCSPlacementMatchCriteria[]) => {
              this.vcsMatchCriteria = match;
            });
          }
          /** Get the PlacementOutcome if available for the selected referralRoster */
          if (this.rrSelected.referralType == 525) {
            this.getRegularPlacementOutcome(this.rrSelected.vcsReferralID);
          } else if (this.rrSelected.referralType == 526) {
            this.getSideDoorPlacementOutcome(this.rrSelected.vcsPlacementMoveInID);
          }

          // Get the online units for the selected referralRoster (siteAgreementPopulationID)
          // this.vcsOnlineUnitsSub = this.referralRosterService.getVCSUnitOnlineBySiteAgreementPopulationID(this.rrSelected.siteAgreementPopulationID).subscribe((units: IVCSOnlineUnits[]) => {
          //   if (units) {
          //     this.vcsOnlineUnits = units;
          //   }
          // });
          this.vcsOnlineUnitsSub = this.tenantRosterService.getVCSOnlineUnitsBySiteID(this.rrSelected.hpSiteID).subscribe((units: IVCSOnlineUnits[]) => {
            if (units) {
              this.vcsOnlineUnits = units;
            }
          });

          if (this.rrSelected.canHPScheduleInterview || (!this.rrSelected.interviewDate && !this.rrSelected.interviewTime)) {
            this.isIntrvwEditable = true;
          }
          if (this.rrSelected.interviewDate && this.rrSelected.interviewTime) {
            // console.log('referralType=525: ', new Date(this.rrSelected.interviewDate));
            this.interviewOutcomeGroup.get('interviewDateCtrl').setValue(new Date(this.rrSelected.interviewDate));
            this.interviewOutcomeGroup.get('interviewTimeCtrl').setValue(this.rrSelected.interviewTime);
          }
          /** Convert string type referralDate to Date type */
          if (this.rrSelected.referralDate) {
            this.referralDate = new Date(this.rrSelected.referralDate);
          }
        } else {
          this.router.navigate(['/vcs/referral-roster']);
        }
      });

    } else if ((this.referralType === 526 || this.referralType === 610)) {
      /** SIDE-DOOR MOVE-IN */
      this.isDocTabDisabled = true;
      /* Making Document tab initially hidden for SideDoor-MoveIn until the moveIn is done,
      because we don't have either vcsReferralID or PactApplicationID */
      this.isSideDoorMoveInDone = false;
      /** Get the selected TenantRoster Details from the TenantRosterService */
      this.trSelectedSub = this.tenantRosterService.getTenantRosterSelected().subscribe(tr => {
        if (tr) {
          this.trSelected = tr;
          // console.log('526 || 610 trSelected: ', this.trSelected);
          /** Get the sideDoor Client Search Result */
          this.sideDoorClientSearchResultSub = this.tenantRosterService.getSideDoorClientSearchResult().subscribe(rslt => {
            if (rslt.sideDoorClient) {
              this.sideDoorClientSearchResult = rslt;
              /** Convert string type referralDate to Date type */
              if (this.referralType == 526 && this.sideDoorClientSearchResult.sideDoorClientReferral.pactApplicationID > 0) {
                this.ssReferralDate = new Date(this.sideDoorClientSearchResult.sideDoorClient.referralDate);
              }
              if (rslt.sideDoorClientReferral && !rslt.sideDoorClientReferral.isEligibilityCriteriaMatched) {
                this.eligibilityCriteriaNotMatched = true;
              }
              if (rslt.sideDoorClientReferral && !rslt.sideDoorClientReferral.isApprovalActive) {
                this.approvalNotActive = true;
              }
            } else {
              this.router.navigate(['/vcs/tenant-roster']);
            }
          });
        } else {
          this.router.navigate(['/vcs/tenant-roster']);
        }
      });
      /** Get the Editable Flag to make PlacementOutcome form editable or readonly */
      this.placementOutcomeFormEditableSub = this.referralRosterService.getPlacementOutcomeEditableFlag().subscribe(flag => {
        if (flag) {
          this.placementOutcomeFormEditable = flag;
        } else {
          this.interviewOutcomeGroup.disable();
        }
      });
    }

    /** Resetting the InterviewOutcome form value based on runtime value selection */
    this.wasInterviewConductedCtrlSub = this.interviewOutcomeGroup.get('wasInterviewConductedCtrl').valueChanges.subscribe(res => {
      if (res === 34) {
        // this.interviewOutcomeGroup.get('referralReceivedFromCtrl').setValue(-1);
        // this.interviewOutcomeGroup.get('specifyReferralReceivedCtrl').setValue('');
        // this.interviewOutcomeGroup.get('referralReceivedDateCtrl').setValue('');
        this.interviewOutcomeGroup.get('interviewOutcomeCtrl').setValue(-1);
        this.interviewOutcomeGroup.get('placementOutcomeCtrl').setValue(-1);
        this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').setValue('');
        this.interviewOutcomeGroup.get('moveInDateCtrl').setValue('');
        this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(-1);
        this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue('');
        this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue([-1]);
        this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue('');
        this.interviewOutcomeGroup.get('reasonCtrl').setValue(-1);
        this.interviewOutcomeGroup.get('commentCtrl').setValue('');
      }
    });
    this.interviewOutcomeCtrlSub = this.interviewOutcomeGroup.get('interviewOutcomeCtrl').valueChanges.subscribe(res => {
      // alert(res);
      if (res === 541) {
        this.interviewOutcomeGroup.get('reasonCtrl').setValue(-1);
        this.interviewOutcomeGroup.get('commentCtrl').setValue('');
      } else {
        this.interviewOutcomeGroup.get('placementOutcomeCtrl').setValue(-1);
        this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').setValue('');
        this.interviewOutcomeGroup.get('moveInDateCtrl').setValue('');
        this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(-1);
        this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue('');
        this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue([-1]);
        this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue('');
        this.interviewOutcomeGroup.get('reasonCtrl').setValue(-1);
        this.interviewOutcomeGroup.get('commentCtrl').setValue('');
      }
    });
    this.placementOutcomeCtrlSub = this.interviewOutcomeGroup.get('placementOutcomeCtrl').valueChanges.subscribe(res => {
      // alert(res);
      this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').setValue('');
      this.interviewOutcomeGroup.get('moveInDateCtrl').setValue('');
      this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(-1);
      this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue('');
      this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue([-1]);
      this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue('');
      this.interviewOutcomeGroup.get('reasonCtrl').setValue(-1);
      this.interviewOutcomeGroup.get('commentCtrl').setValue('');
    });

    /* Check if the workFlow is coming from TAD, if so, on submit or exit, redirect back to TAD workflow */
    this.istadWorkflowSub = this.tadService.getIsTadWorkflow().subscribe(flag => {
      if (flag) {
        this.isTadWorkflow = flag;
      }
    });
  }

  getRegularPlacementOutcome(vcsReferralID: number) {
    if (vcsReferralID > 0) {
      /** Get the PlacementOutcome if available for the selected referralRoster */
      this.existingPlacementOutcomeSub = this.referralRosterService.getVCSPlacementOutcomeByVCSReferralID(vcsReferralID).subscribe((po: IPlacementOutcome) => {
        if (po) {
          // console.log('Regular ExistingPlacementOutcome: ', po);
          this.existingPlacementOutcome = po;
          po.incomeSourceList.forEach(is => {
            this.selectedIncomeSourceForForm.push(is.incomeSourceType);
            if (is.incomeSourceType == 566) {
              this.isThereOtherIncomeSource = true;
            }
          });
          po.initialIncomeSourceList.forEach(iis => {
            this.selectedInitialIncomeSourceForForm.push(iis.incomeSourceType);
            if (iis.incomeSourceType == 566) {
              this.wasThereInitialOtherIncomeSource = true;
            }
          });
          let exptMoveInDate;
          let moveInDate;
          if (po.expectedMoveInDate) {
            exptMoveInDate = new Date(po.expectedMoveInDate);
          }
          if (po.moveInDate) {
            moveInDate = new Date(po.moveInDate);
          }
          this.interviewOutcomeGroup.get('wasInterviewConductedCtrl').setValue(po.wasInterviewConducted);
          this.interviewOutcomeGroup.get('interviewOutcomeCtrl').setValue(po.interviewOutcomeType);
          this.interviewOutcomeGroup.get('placementOutcomeCtrl').setValue(po.placementOutcomeType);
          this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').setValue(exptMoveInDate);
          this.interviewOutcomeGroup.get('moveInDateCtrl').setValue(moveInDate);
          if (po.placementOutcomeType === 545) {
            // 545 = Pending Approval
            this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(po.expectedUnitID);
          } else if (po.placementOutcomeType === 546) {
            this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(po.unitID);
          } else {
            this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(-1);
          }
          if (this.placementOutcomeFormEditable) {
            this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue(po.clientRentalContribution);
            this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue(this.selectedIncomeSourceForForm);
            this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(po.incomeSourceOtherSpecify);
          } else {
            this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue(po.initialClientRentalContribution);
            this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue(this.selectedInitialIncomeSourceForForm);
            this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(po.initialIncomeSourceOtherSpecify);
          }

          this.interviewOutcomeGroup.get('reasonCtrl').setValue(po.reasonType);
          this.interviewOutcomeGroup.get('commentCtrl').setValue(po.comments);
        }
      });
    }
  }

  getSideDoorPlacementOutcome(vcsPlacementMoveInID: number) {
    if (vcsPlacementMoveInID > 0) {
      /** Get the PlacementOutcome if available for the selected TenantRoster */
      this.existingPlacementOutcomeSub = this.tenantRosterService.getVCSPlacementOutcomeByVCSPlacementMoveInID(vcsPlacementMoveInID).subscribe((po: IPlacementOutcome) => {
        if (po) {
          // console.log('Sidedoor ExistingPlacementOutcome: ', po);
          this.existingPlacementOutcome = po;
          po.incomeSourceList.forEach((is, index) => {
            this.selectedIncomeSourceForForm.push(is.incomeSourceType);
            if (is.incomeSourceType == 566) {
              this.isThereOtherIncomeSource = true;
            }
            // if (index == po.incomeSourceList.length - 1) {
            //   this.updateTenantProfileGroup.get('updateIncomeSourceCtrl').setValue(this.selectedIncomeSourceForForm);
            // }
          });
          po.initialIncomeSourceList.forEach(iis => {
            this.selectedInitialIncomeSourceForForm.push(iis.incomeSourceType);
            if (iis.incomeSourceType == 566) {
              this.wasThereInitialOtherIncomeSource = true;
            }
          });
          let refReceivedDate;
          let exptMoveInDate;
          let moveInDate;
          if (po.expectedMoveInDate) {
            exptMoveInDate = new Date(po.expectedMoveInDate);
            this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').setValue(exptMoveInDate);
          }
          if (po.interviewDate) {
            this.interviewOutcomeGroup.get('interviewDateCtrl').setValue(new Date(po.interviewDate));
          }
          if (po.referralReceivedDate) {
            refReceivedDate = new Date(po.referralReceivedDate);
            this.interviewOutcomeGroup.get('referralReceivedDateCtrl').setValue(refReceivedDate);
          }
          if (po.moveInDate) {
            moveInDate = new Date(po.moveInDate);
          }
          this.interviewOutcomeGroup.get('wasInterviewConductedCtrl').setValue(po.wasInterviewConducted);
          this.interviewOutcomeGroup.get('interviewOutcomeCtrl').setValue(po.interviewOutcomeType);
          this.interviewOutcomeGroup.get('placementOutcomeCtrl').setValue(po.placementOutcomeType);
          this.interviewOutcomeGroup.get('referralReceivedFromCtrl').setValue(po.referralReceivedType);
          this.interviewOutcomeGroup.get('specifyReferralReceivedCtrl').setValue(po.referralReceivedOtherSpecify);
          this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue(po.initialClientRentalContribution);
          this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue(this.selectedInitialIncomeSourceForForm);
          this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(po.initialIncomeSourceOtherSpecify);
          this.interviewOutcomeGroup.get('moveInDateCtrl').setValue(moveInDate);
          this.interviewOutcomeGroup.get('reasonCtrl').setValue(po.reasonType);
          this.interviewOutcomeGroup.get('commentCtrl').setValue(po.comments);

          if (po.placementOutcomeType === 545) {
            // 545 = Pending Approval
            this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(po.expectedUnitID);
          } else if (po.placementOutcomeType === 546) {
            this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(po.unitID);
          } else {
            this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(-1);
          }
          if (this.placementOutcomeFormEditable) {
            this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue(po.clientRentalContribution);
            this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue(this.selectedIncomeSourceForForm);
            this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(po.incomeSourceOtherSpecify);
          } else {
            this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue(po.initialClientRentalContribution);
            this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue(this.selectedInitialIncomeSourceForForm);
            this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(po.initialIncomeSourceOtherSpecify);
          }
        }
      });
    }
  }

  onExpansionPanelToggle() {
    this.panelExpanded = !this.panelExpanded;
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;
  }

  onReferralReceivedDateSelected() {
    let interviewDate;
    const sysDate = new Date();
    if (this.datePipe.transform(this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value, 'MM/dd/yyyy')) {
      const referralReceivedDatePicked = new Date(this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value);
      if (this.interviewOutcomeGroup.get('interviewDateCtrl').value) {
        interviewDate = new Date(this.interviewOutcomeGroup.get('interviewDateCtrl').value);
        if (referralReceivedDatePicked > interviewDate) {
          this.toastrService.error('Housing Referral Date must be on or before Interview Date ' + (interviewDate.getMonth() + 1) + '/' + interviewDate.getDate() + '/' + interviewDate.getFullYear());
        }
      }
      if (referralReceivedDatePicked > sysDate) {
        this.toastrService.error('Housing Referral Date must be on or before  ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
      }
      if (this.referralType == 526 && referralReceivedDatePicked < this.ssReferralDate) {
        this.toastrService.error('Housing Referral Received Date must be on or after Referral Date ' + (this.ssReferralDate.getMonth() + 1) + '/' + this.ssReferralDate.getDate() + '/' + this.ssReferralDate.getFullYear());
      }
    }
  }

  onInterviewDateSelected() {
    if (this.datePipe.transform(this.interviewOutcomeGroup.get('interviewDateCtrl').value, 'MM/dd/yyyy')) {
      if (this.referralType == 525 && this.rrSelected) {
        /* Regular Referral Validation */
        const sysDate = new Date();
        let expectedMoveInDate;
        let moveInDate;
        const datePicked = new Date(this.interviewOutcomeGroup.get('interviewDateCtrl').value);
        if (datePicked) {
          if (this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').value) {
            expectedMoveInDate = new Date(this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').value);
            if (datePicked > expectedMoveInDate) {
              this.toastrService.error('The expected move-In date must be after the interview date ' + (datePicked.getMonth() + 1) + '/' + datePicked.getDate() + '/' + datePicked.getFullYear());
            }
          }
          if (this.interviewOutcomeGroup.get('moveInDateCtrl').value) {
            moveInDate = new Date(this.interviewOutcomeGroup.get('moveInDateCtrl').value);
            if (datePicked > moveInDate) {
              this.toastrService.error('The move-In date must be after the interview date ' + (datePicked.getMonth() + 1) + '/' + datePicked.getDate() + '/' + datePicked.getFullYear());
            }
          }
          /** Call the db to get the availableInterviewTimeSlot only if datePicked is < sysDate and > referralDate */
          if (datePicked < sysDate && datePicked >= this.referralDate) {
            const datePickedYear = datePicked.getFullYear();
            const datePickedMonth = datePicked.getMonth() + 1;
            const datePickedDay = datePicked.getDate();
            const sDate = datePickedYear + '-' + datePickedMonth + '-' + datePickedDay;
            const inputValue: VCSAvailableInterviewTimeSlotInput = {
              HPSiteID: this.rrSelected.hpSiteID,
              InterviewDate: sDate,
            };
            /** Getting the available Interview Time slot from database for the select HPSite */
            this.availableInterviewTimeSlotSub = this.c2vService.GetAvailableInterviewTimeSlot(inputValue).subscribe((slt: VCSAvailableInterviewTimeSlot[]) => {
              if (slt) {
                this.availableInterviewTimeSlot = slt;
              }
            });
          } else {
            this.availableInterviewTimeSlot = null;
            if (datePicked >= sysDate) {
              this.toastrService.error('The interview Date must be on or before ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
            } else if (datePicked < this.referralDate) {
              this.toastrService.error('Interview date must be after the housing referral date ' + (this.referralDate.getMonth() + 1) + '/' + this.referralDate.getDate() + '/' + this.referralDate.getFullYear());
            }
          }
        }
      } else if (this.referralType == 526 || this.referralType == 610) {
        /* SideDoor Validation */
        const sysDate = new Date();
        const interviewDate = new Date(this.interviewOutcomeGroup.get('interviewDateCtrl').value);
        if (interviewDate > sysDate) {
          this.toastrService.error('Interview Date must be on or before ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
        }
        if (this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value) {
          const referralReceivedDate = new Date(this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value);
          if (interviewDate < referralReceivedDate) {
            this.toastrService.error('Interview date must be on or after the housing referral date ' + (referralReceivedDate.getMonth() + 1) + '/' + referralReceivedDate.getDate() + '/' + referralReceivedDate.getFullYear());
          }
        }
        if (this.interviewOutcomeGroup.get('moveInDateCtrl').value) {
          const moveInDate = new Date(this.interviewOutcomeGroup.get('moveInDateCtrl').value);
          if (interviewDate > moveInDate) {
            this.toastrService.error(': Interview Date must be on or before Move-In Date ' + (moveInDate.getMonth() + 1) + '/' + moveInDate.getDate() + '/' + moveInDate.getFullYear());
          }
        }
      }
    }
  }

  onMoveInDateSelected() {
    if (this.referralType == 525) {
      /* Regular Referral Validation */
      let interviewDate;
      const sysDate = new Date();
      if (this.datePipe.transform(this.interviewOutcomeGroup.get('moveInDateCtrl').value, 'MM/dd/yyyy')) {
        const moveInDatePicked = new Date(this.interviewOutcomeGroup.get('moveInDateCtrl').value);
        if (this.interviewOutcomeGroup.get('interviewDateCtrl').value) {
          interviewDate = new Date(this.interviewOutcomeGroup.get('interviewDateCtrl').value);
          if (moveInDatePicked < interviewDate) {
            this.toastrService.error('The move-in date must be on or after the interview date ' + (interviewDate.getMonth() + 1) + '/' + interviewDate.getDate() + '/' + interviewDate.getFullYear());
          }
        }
        if (this.referralDate && moveInDatePicked < this.referralDate) {
          this.toastrService.error('The move-in date must be on or after the housing referral date ' + (this.referralDate.getMonth() + 1) + '/' + this.referralDate.getDate() + '/' + this.referralDate.getFullYear());
        }
        if (moveInDatePicked > sysDate) {
          this.toastrService.error('The Move-In Date must be on or before ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
        }
      }
    } else if (this.referralType == 526 || this.referralType == 610) {
      /* SideDoor Validation */
      const sysDate = new Date();
      if (this.datePipe.transform(this.interviewOutcomeGroup.get('moveInDateCtrl').value, 'MM/dd/yyyy')) {
        const moveInDatePicked = new Date(this.interviewOutcomeGroup.get('moveInDateCtrl').value);
        if (moveInDatePicked > sysDate) {
          this.toastrService.error('The Move-In Date must be on or before ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
        }
        if (this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value) {
          const referralReceivedDate = new Date(this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value);
          if (moveInDatePicked < referralReceivedDate) {
            this.toastrService.error('Move-In Date must be on or after Housing Referral Date ' + (referralReceivedDate.getMonth() + 1) + '/' + referralReceivedDate.getDate() + '/' + referralReceivedDate.getFullYear());
          }
        }
        if (this.interviewOutcomeGroup.get('interviewDateCtrl').value) {
          const interviewDate = new Date(this.interviewOutcomeGroup.get('interviewDateCtrl').value);
          if (moveInDatePicked < interviewDate) {
            this.toastrService.error('The move-in date must be on or after the interview date ' + (interviewDate.getMonth() + 1) + '/' + interviewDate.getDate() + '/' + interviewDate.getFullYear());
          }
        }
      }
    }

  }
  onExpectedMoveInDateSelected() {
    let interviewDate;
    const sysDate = new Date();
    if (this.datePipe.transform(this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').value, 'MM/dd/yyyy')) {
      const expectedMoveInDatePicked = new Date(this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').value);
      if (this.interviewOutcomeGroup.get('interviewDateCtrl').value) {
        interviewDate = new Date(this.interviewOutcomeGroup.get('interviewDateCtrl').value);
        if (expectedMoveInDatePicked < interviewDate) {
          this.toastrService.error('The Expected Move-In date must be on or after Interview Date ' + (interviewDate.getMonth() + 1) + '/' + interviewDate.getDate() + '/' + interviewDate.getFullYear());
        }
      }
      if (expectedMoveInDatePicked < sysDate) {
        this.toastrService.error('The Expected Move-In date must be after ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
      }
    }
  }

  onMultiIncomeSourceSelected(event) {
    if (!event) {
      const valueSelected = this.interviewOutcomeGroup.get('incomeSourceCtrl').value;
      this.selectedIncomeSource = [];
      this.incomeSourceUpdated = false;
      let v1: IVCSIncomeSourceType;
      let c1: number[] = [];
      let c2: number[] = [];
      let missing: number[] = [];
      if (this.existingPlacementOutcome.incomeSourceList.length > 0) {
        this.existingPlacementOutcome.incomeSourceList.forEach(is => {
          c1.push(is.incomeSourceType);
        });
      }
      if (valueSelected.length > 0) {
        valueSelected.forEach(vs => {
          c2.push(vs);
        });
      }

      if (c1.length > 0) {
        if (c2.length > 0 && c2.length > c1.length) {
          /* checking if user selected any new Income source */
          missing = c2.filter(item => c1.indexOf(item) < 0);
          if (missing.length > 0) {
            /* new income source selected */
            this.incomeSourceUpdated = true;
            valueSelected.forEach(vs => {
              v1 = { vcsIncomeSourceID: 0, incomeSourceType: vs }
              this.selectedIncomeSource.push(v1);
            });
          } else {
            this.incomeSourceUpdated = false;
          }
        }
        if (c2.length > 0 && c2.length <= c1.length) {
          /* checking if user unSelected any existing income source */
          missing = c1.filter(item => c2.indexOf(item) < 0);
          if (missing.length > 0) {
            /* existing income source unselected */
            /* Updated the income source */
            this.incomeSourceUpdated = true;
            valueSelected.forEach(vs => {
              v1 = { vcsIncomeSourceID: 0, incomeSourceType: vs }
              this.selectedIncomeSource.push(v1);
            });
            /* If existing Other income source type is unselected reset the specify comment value */
            missing.forEach(mis => {
              if (mis == 566) {
                this.isThereOtherIncomeSource = false;
                this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue('');
              }
            })
          } else {
            /* Nothing Updated */
            this.incomeSourceUpdated = false;
            valueSelected.forEach(vs => {
              if (vs == 566) {
                /* Resetting the value for other specify if exist */
                this.isThereOtherIncomeSource = true;
                this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(this.existingPlacementOutcome.incomeSourceOtherSpecify);
              }
            });
          }
        }
      } else {
        // all new
        valueSelected.forEach(vs => {
          v1 = { vcsIncomeSourceID: 0, incomeSourceType: vs }
          this.selectedIncomeSource.push(v1);
        });
      }
      if (this.selectedIncomeSource.length > 0) {
        this.selectedIncomeSource.forEach(el => {
          if (el.incomeSourceType == 566) {
            this.isThereOtherIncomeSource = true;
          } else {
            this.isThereOtherIncomeSource = false;
            this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue('');
          }
        });
      }

    }
  }

  onTransmit() {
    if (this.referralType == 525) {
      const sysDate = new Date();
      const intrvwDate = this.interviewOutcomeGroup.get('interviewDateCtrl').value ? new Date(this.interviewOutcomeGroup.get('interviewDateCtrl').value) : this.interviewOutcomeGroup.get('interviewDateCtrl').value;
      const intrvwTime = this.interviewOutcomeGroup.get('interviewTimeCtrl').value;
      const wasIntrvwConducted = this.interviewOutcomeGroup.get('wasInterviewConductedCtrl').value;
      const intrvwOutcome = this.interviewOutcomeGroup.get('interviewOutcomeCtrl').value;
      const plcmtOutcome = this.interviewOutcomeGroup.get('placementOutcomeCtrl').value;
      const exptMoveInDate = this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').value ? new Date(this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').value) : this.interviewOutcomeGroup.get('expectedMoveInDateCtrl').value;
      const actualMoveInDate = this.interviewOutcomeGroup.get('moveInDateCtrl').value ? new Date(this.interviewOutcomeGroup.get('moveInDateCtrl').value) : this.interviewOutcomeGroup.get('moveInDateCtrl').value;
      const unitNumber = this.interviewOutcomeGroup.get('unitNumberCtrl').value;
      const clientRentalContribution = this.interviewOutcomeGroup.get('clientRentalContributionCtrl').value;
      // const incomeSrc = this.interviewOutcomeGroup.get('incomeSourceCtrl').value;
      const incomeSrc = this.selectedIncomeSource;
      const specifyComment = this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').value;
      const reason = this.interviewOutcomeGroup.get('reasonCtrl').value;
      const comments = this.interviewOutcomeGroup.get('commentCtrl').value;
      // let canTransmit = false;

      const savePlcmtOutcome: IPlacementOutcome = {
        vcsReferralID: this.rrSelected.vcsReferralID,
        interviewDate: intrvwDate,
        interviewTime: intrvwTime,
        wasInterviewConducted: wasIntrvwConducted,
        interviewOutcomeType: (intrvwOutcome === 0 || intrvwOutcome === -1) ? null : intrvwOutcome,
        placementOutcomeType: (plcmtOutcome === 0 || plcmtOutcome === -1) ? null : plcmtOutcome,
        expectedMoveInDate: exptMoveInDate,
        expectedUnitID: (unitNumber === 0 || unitNumber === -1) ? null : unitNumber,
        moveInDate: actualMoveInDate,
        unitID: (unitNumber === 0 || unitNumber === -1) ? null : unitNumber,
        unitName: null,
        initialClientRentalContribution: null,
        initialIncomeSourceList: null,
        initialIncomeSourceOtherSpecify: null,
        clientRentalContribution: clientRentalContribution,
        incomeSourceList: this.selectedIncomeSource,
        incomeSourceOtherSpecify: specifyComment,
        reasonType: (reason === 0 || reason === -1) ? null : reason,
        comments: comments,
        referralReceivedType: null,
        referralReceivedDate: null,
        referralReceivedOtherSpecify: null,
        pactApplicationID: this.rrSelected.pactApplicationID,
        vcsID: null,
        clientID: this.rrSelected.clientID,
        clientSourceType: 573,
        referringAgencyID: null,
        referringAgencyNo: null,
        referringAgencyName: null,
        referringSiteID: null,
        hpAgencyID: null,
        hpSiteID: this.rrSelected.hpSiteID,
        siteAgreementPopulationID: null,
        referralGroupGUID: null,
        referralType: this.referralType,
        isScheduleMandatory: 0,
        unitType: null,
        matchPercent: null,
        referralStatusType: null,
        cocType: null,
        withdrawnReasonType: null,
        referralClosedComment: null,
        isEligibilityCriteriaMatched: null,
        isApprovalActive: null,
        placementMatchCriteriaList: null,
        firstName: null,
        lastName: null,
        ssn: null,
        dob: null,
        gender: null,
        referralDate: null,
        eligibility: null,
        placementCriteria: null,
        svaPrioritization: null,
        serviceNeeds: null,
        createdByName: null,
        createdDate: null,
        updatedByName: null,
        updatedDate: null
      };

      if (!intrvwDate) {
        this.toastrService.error('Please enter the InterviewDate.');
      } else if (intrvwDate >= sysDate) {
        this.toastrService.error('The interview Date must be on or  before ' + (sysDate.getMonth() + 1) + '-' + sysDate.getDate() + '-' + sysDate.getFullYear());
      } else if (intrvwDate < this.referralDate) {
        this.toastrService.error('Interview date must be after the housing referral date ' + (this.referralDate.getMonth() + 1) + '-' + this.referralDate.getDate() + '-' + this.referralDate.getFullYear());
      } else if (intrvwTime == 0 || intrvwTime == -1) {
        this.toastrService.error('Please select the interviewTime if available.');
      } else if (!wasIntrvwConducted || wasIntrvwConducted == 0 || wasIntrvwConducted == -1) {
        this.toastrService.error('Please select WasInterviewConducted.');
      } else if (wasIntrvwConducted === 34) {
        // Interview Not Conducted
        if (reason === 0 || reason === -1) {
          this.toastrService.error('Please select the reason.');
        } else if (comments === '') {
          this.toastrService.error('Please write some comments.');
        } else {
          // Transmit the Outcome
          // canTransmit = true;
          this.transmitPlacement(savePlcmtOutcome, '/vcs/referral-roster');
        }
      } else if (wasIntrvwConducted === 33) {
        // Interview was Conducted
        if (intrvwOutcome === 0 || intrvwOutcome === -1) {
          this.toastrService.error('Please select Interview Outcome.');
        } else if (intrvwOutcome === 542 || intrvwOutcome === 543 || intrvwOutcome === 544) {
          // Negative InterviewOutcome
          if (reason === 0 || reason === -1) {
            this.toastrService.error('Please select the reason.');
          } else if (comments === '') {
            this.toastrService.error('Please write some comments.');
          } else {
            // Transmit the Outcome
            // canTransmit = true;
            this.transmitPlacement(savePlcmtOutcome, '/vcs/referral-roster');
          }
        } else if (intrvwOutcome === 541) {
          // InterviewOutcome Accepted
          if (plcmtOutcome === 0 || plcmtOutcome === -1) {
            this.toastrService.error('Please select the Placement Outcome.');
          } else if (plcmtOutcome === 545) {
            // Pending Approval
            if (!exptMoveInDate) {
              this.toastrService.error('Please select the Expected Move-In Date');
            } else if (exptMoveInDate < sysDate) {
              this.toastrService.error('The Expected Move-In date must be after ' + (sysDate.getMonth() + 1) + '-' + sysDate.getDate() + '-' + sysDate.getFullYear());
            } else if (exptMoveInDate < intrvwDate) {
              this.toastrService.error('The Expected Move-In date must be on or after Interview Date ' + (intrvwDate.getMonth() + 1) + '-' + intrvwDate.getDate() + '-' + intrvwDate.getFullYear());
            } else if (comments === '') {
              this.toastrService.error('Please write some comments');
            } else {
              // Transmit the Outcome
              // canTransmit = true;
              this.transmitPlacement(savePlcmtOutcome, '/vcs/referral-roster');
            }
          } else if (plcmtOutcome === 546) {
            // Move-In
            if (!actualMoveInDate) {
              this.toastrService.error('Please select the Move-In Date');
            } else if (actualMoveInDate > sysDate) {
              this.toastrService.error('The Move-In Date must be on or before ' + (sysDate.getMonth() + 1) + '-' + sysDate.getDate() + '-' + sysDate.getFullYear());
            } else if (this.referralDate && actualMoveInDate < this.referralDate) {
              this.toastrService.error('The move-in date must be on or after the housing referral date ' + (this.referralDate.getMonth() + 1) + '-' + this.referralDate.getDate() + '-' + this.referralDate.getFullYear());
            } else if (actualMoveInDate < intrvwDate) {
              this.toastrService.error('The move-in date must be on or after the interview date ' + (intrvwDate.getMonth() + 1) + '-' + intrvwDate.getDate() + '-' + intrvwDate.getFullYear());
            } else if (unitNumber === 0 || unitNumber === -1) {
              this.toastrService.error('Please select the Unit Number');
            } else if (clientRentalContribution === '') {
              this.toastrService.error('Please provide the client rental contribution');
            } else if (incomeSrc.length == 0) {
              this.toastrService.error('Please select the Income Source');
            } else if (this.isThereOtherIncomeSource && specifyComment === '') {
              this.toastrService.error('Please write some Income Source comments');
            } else {
              /* Unit#, MoveIn-Date validation (Occupancy Validation, Offline Period Validation, Existing Tenant Verification) */
              const validatorInput: IVCSMoveInValidator = {
                unitID: unitNumber,
                moveInDate: actualMoveInDate,
                clientID: this.rrSelected.clientID,
                clientSourceType: 573, // 573 = Pact Client, 574 = Community Unit Client, 575 = Master Leasing Client
                pactApplicationID: this.rrSelected.pactApplicationID,
                vcsReferralID: this.rrSelected.vcsReferralID,
                referralType: this.referralType,
              }
              // console.log('VAlidatorInput: ', validatorInput);
              this.moveInValidatorSub = this.tenantRosterService.getVCSMoveInValidators(validatorInput).subscribe((val: IVCSMoveInValidator) => {
                if (val) {
                  savePlcmtOutcome.isEligibilityCriteriaMatched = val.isEligibilityCriteriaMatched;
                  savePlcmtOutcome.isApprovalActive = val.isApprovalActive;
                  // console.log('moveInValidator: ', val);
                  if (!val.wasUnitOffline && (!val.isUnitAvailable || val.wasUnitOccupied)) {
                    /* Occupancy Validation
                     Unit was not Offline but another Client was residing in the selected Unit during this Move-In Date selected */
                    const title = 'Verify';
                    const primaryMessage = `Our records show that another tenant was living in this unit on this move-In date, please review your records and select a different move-in date or revise the prior tenant housing record.`;
                    const secondaryMessage = ``;
                    const confirmButtonName = 'OK';
                    const dismissButtonName = '';
                    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                      positiveResponse => { },
                      negativeResponse => { }
                    );
                  } else if (val.wasUnitOffline) {
                    /* Offline Period Validation
                      Unit was Offline during the selected Move-In Date */
                    const title = 'Verify';
                    const primaryMessage = `Our records show that this unit was offline during period.`;
                    const secondaryMessage = `Do you still want to continue?`;
                    const confirmButtonName = 'Yes';
                    const dismissButtonName = 'No';
                    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                      positiveResponse => {
                        if (comments === '') {
                          this.toastrService.error('Please write some comments.');
                        } else {
                          this.transmitPlacement(savePlcmtOutcome, '/vcs/referral-roster');
                        }
                      },
                      negativeResponse => {
                        const title = 'Verify';
                        const primaryMessage = `Please select a different Move-In Date.`;
                        const secondaryMessage = ``;
                        const confirmButtonName = 'OK';
                        const dismissButtonName = '';
                        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                          positiveResponse => { },
                          negativeResponse => { }
                        );
                      }
                    );
                  } else if (val.wasClientResiding) {
                    /* Existing Tenant Verification
                    Client is currently residing in a unit or was residing in a unit during the selected Move-In Date */
                    const title = 'Verify';
                    const primaryMessage = `This tenant is listed as residing in ` + val.wasResidingUnit + ` during this period. Please review your records and select a different move-in date or revise the prior housing record`;
                    const secondaryMessage = ``;
                    const confirmButtonName = 'OK';
                    const dismissButtonName = '';
                    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                      positiveResponse => { },
                      negativeResponse => { }
                    );
                  } else if (val.isClientResiding) {
                    /* Existing Tenant Verification
                    Client is currently residing in a unit or was residing in a unit during the selected Move-In Date */
                    const title = 'Verify';
                    const primaryMessage = `This tenant is currently residing in ` + val.currentlyResidingUnit;
                    const secondaryMessage = ``;
                    const confirmButtonName = 'OK';
                    const dismissButtonName = '';
                    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                      positiveResponse => { },
                      negativeResponse => { }
                    );
                  } else if (!val.isEligibilityCriteriaMatched) {
                    /* Eligibility Criteria validation for Regular referral Placement Outcome */
                    const title = 'Verify';
                    const primaryMessage = `This Client's Eligibility Criteria does not match with the Unit Profile you selected.`;
                    const secondaryMessage = `Do you still want to continue?`;
                    const confirmButtonName = 'Yes';
                    const dismissButtonName = 'No';
                    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                      positiveResponse => { this.transmitPlacement(savePlcmtOutcome, '/vcs/referral-roster'); },
                      negativeResponse => { }
                    );

                  } else {
                    this.transmitPlacement(savePlcmtOutcome, '/vcs/referral-roster');
                  }
                }
              });
            }
          }
        }
      }
    } else if (this.referralType == 526 || this.referralType == 610) {
      const sysDate = new Date();
      const wasIntrvwConducted = this.interviewOutcomeGroup.get('wasInterviewConductedCtrl').value;
      const referralReceivedFrom = this.interviewOutcomeGroup.get('referralReceivedFromCtrl').value;
      const specifyReferralReceivedFrom = this.interviewOutcomeGroup.get('specifyReferralReceivedCtrl').value;
      const referralReceivedDate = this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value ? new Date(this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value) : this.interviewOutcomeGroup.get('referralReceivedDateCtrl').value;
      const intrvwDate = this.interviewOutcomeGroup.get('interviewDateCtrl').value ? new Date(this.interviewOutcomeGroup.get('interviewDateCtrl').value) : this.interviewOutcomeGroup.get('interviewDateCtrl').value;
      const moveInDate = this.interviewOutcomeGroup.get('moveInDateCtrl').value ? new Date(this.interviewOutcomeGroup.get('moveInDateCtrl').value) : this.interviewOutcomeGroup.get('moveInDateCtrl').value;
      const clientRentalContribution = this.interviewOutcomeGroup.get('clientRentalContributionCtrl').value;
      // const incomeSrc = this.interviewOutcomeGroup.get('incomeSourceCtrl').value;
      const incomeSrc = this.selectedIncomeSource;
      const specifyIncomeComment = this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').value;
      const comments = this.interviewOutcomeGroup.get('commentCtrl').value;

      const savePlcmtOutcome: IPlacementOutcome = {
        vcsReferralID: null,
        interviewDate: intrvwDate,
        interviewTime: '00:00:00.0000000',
        wasInterviewConducted: wasIntrvwConducted,
        interviewOutcomeType: null,
        placementOutcomeType: null,
        expectedMoveInDate: null,
        expectedUnitID: null,
        moveInDate: moveInDate,
        unitID: this.trSelected.unitID,
        unitName: this.trSelected.unitName,
        initialClientRentalContribution: null,
        initialIncomeSourceList: null,
        initialIncomeSourceOtherSpecify: null,
        clientRentalContribution: clientRentalContribution,
        incomeSourceList: this.selectedIncomeSource,
        incomeSourceOtherSpecify: specifyIncomeComment,
        reasonType: null,
        comments: comments,
        referralReceivedType: (referralReceivedFrom === -1) ? null : referralReceivedFrom,
        referralReceivedDate: referralReceivedDate,
        referralReceivedOtherSpecify: specifyReferralReceivedFrom,
        pactApplicationID: this.referralType == 526 ? this.sideDoorClientSearchResult.sideDoorClientReferral.pactApplicationID : null,
        vcsID: this.sideDoorClientSearchResult.sideDoorClient.vcsid,
        clientID: this.sideDoorClientSearchResult.sideDoorClient.clientID,
        clientSourceType: this.sideDoorClientSearchResult.sideDoorClient.clientSourceType,
        referringAgencyID: this.referralType == 526 ? this.sideDoorClientSearchResult.sideDoorClientReferral.raAgencyID : null,
        referringAgencyNo: this.referralType == 526 ? this.sideDoorClientSearchResult.sideDoorClientReferral.raAgencyNo : null,
        referringAgencyName: this.referralType == 526 ? this.sideDoorClientSearchResult.sideDoorClientReferral.raAgencyName : null,
        referringSiteID: this.referralType == 526 ? this.sideDoorClientSearchResult.sideDoorClientReferral.raSiteID : null,
        hpAgencyID: this.trSelected.hpAgencyID,
        hpSiteID: this.trSelected.hpSiteID,
        siteAgreementPopulationID: this.trSelected.siteAgreementPopulationID,
        referralGroupGUID: null,
        referralType: this.referralType,
        isScheduleMandatory: 0,
        unitType: this.trSelected.unitType,
        matchPercent: this.referralType == 526 ? this.sideDoorClientSearchResult.sideDoorClientReferral.matchPercentage : null,
        referralStatusType: 519,
        cocType: null,
        withdrawnReasonType: null,
        referralClosedComment: null,
        isEligibilityCriteriaMatched: null,
        isApprovalActive: null,
        placementMatchCriteriaList: this.referralType == 526 ? this.sideDoorClientSearchResult.placementMatchCriteria : null,
        firstName: null,
        lastName: null,
        ssn: null,
        dob: null,
        gender: null,
        referralDate: null,
        eligibility: null,
        placementCriteria: null,
        svaPrioritization: null,
        serviceNeeds: null,
        createdByName: null,
        createdDate: null,
        updatedByName: null,
        updatedDate: null
      };

      if (!wasIntrvwConducted || wasIntrvwConducted == 0 || wasIntrvwConducted == -1) {
        this.toastrService.error('Please select WasInterviewConducted.');
      } else if (referralReceivedFrom == 0 || referralReceivedFrom == -1) {
        this.toastrService.error('Please select Referral Received From.');
      } else if (referralReceivedFrom == -2 && specifyReferralReceivedFrom == '') {
        /* -1 = Other */
        this.toastrService.error('Please write some comment for Referral Received From');
      } else if (!referralReceivedDate) {
        this.toastrService.error('Please select the Referral Received Date');
      } else if (this.referralType == 526 && referralReceivedDate < this.ssReferralDate) {
        this.toastrService.error('Housing Referral Received Date must be on or after Referral Date ' + (this.ssReferralDate.getMonth() + 1) + '/' + this.ssReferralDate.getDate() + '/' + this.ssReferralDate.getFullYear());
      } else if (!intrvwDate) {
        this.toastrService.error('Please select the Interview Date');
      } else if (!moveInDate) {
        this.toastrService.error('Please select the Move-In Date');
      } else if (clientRentalContribution === '') {
        this.toastrService.error('Please provide the client rental contribution');
      } else if (incomeSrc.length == 0) {
        this.toastrService.error('Please select the Income Source');
      } else if (this.isThereOtherIncomeSource && specifyIncomeComment === '') {
        this.toastrService.error('Please write some Income Source comments');
      } else if ((this.eligibilityCriteriaNotMatched || this.approvalNotActive) && comments === '') {
        this.toastrService.error('Please write some comments.');
      } else if (intrvwDate && referralReceivedDate) {
        if (referralReceivedDate > intrvwDate) {
          this.toastrService.error('Housing Referral Date must be on or before Interview Date ' + (intrvwDate.getMonth() + 1) + '/' + intrvwDate.getDate() + '/' + intrvwDate.getFullYear());
        } else if (referralReceivedDate > sysDate) {
          this.toastrService.error('Housing Referral Date must be on or before  ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
        } else if (intrvwDate > sysDate) {
          this.toastrService.error('Interview Date must be on or before ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
        } else if (intrvwDate && moveInDate) {
          if (intrvwDate > moveInDate) {
            this.toastrService.error(': Interview Date must be on or before Move-In Date ' + (moveInDate.getMonth() + 1) + '/' + moveInDate.getDate() + '/' + moveInDate.getFullYear());
          } else if (moveInDate > sysDate) {
            this.toastrService.error('The Move-In Date must be on or before ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
          } else if (moveInDate < referralReceivedDate) {
            this.toastrService.error('Move-In Date must be on or after Housing Referral Date ' + (referralReceivedDate.getMonth() + 1) + '/' + referralReceivedDate.getDate() + '/' + referralReceivedDate.getFullYear());
          } else if (moveInDate < intrvwDate) {
            this.toastrService.error('The move-in date must be on or after the interview date ' + (intrvwDate.getMonth() + 1) + '/' + intrvwDate.getDate() + '/' + intrvwDate.getFullYear());
          } else {
            /* Unit#, MoveIn-Date validation (Occupancy Validation, Offline Period Validation, Existing Tenant Verification) */
            const validatorInput: IVCSMoveInValidator = {
              unitID: this.sideDoorClientSearchResult.sideDoorClient.unitID,
              moveInDate: moveInDate,
              clientID: this.sideDoorClientSearchResult.sideDoorClient.clientID,
              clientSourceType: this.sideDoorClientSearchResult.sideDoorClient.clientSourceType,
              pactApplicationID: this.referralType == 526 ? this.sideDoorClientSearchResult.sideDoorClientReferral.pactApplicationID : null,
              referralType: this.referralType,
            }
            this.moveInValidatorSub = this.tenantRosterService.getVCSMoveInValidators(validatorInput).subscribe((val: IVCSMoveInValidator) => {
              if (val) {
                // console.log(val);
                if (this.referralType == 526) {
                  savePlcmtOutcome.isEligibilityCriteriaMatched = val.isEligibilityCriteriaMatched;
                  savePlcmtOutcome.isApprovalActive = val.isApprovalActive;
                }
                // console.log('moveInValidator: ', val);
                if (!val.wasUnitOffline && (!val.isUnitAvailable || val.wasUnitOccupied)) {
                  /* Occupancy Validation
                   Unit was not Offline but another Client was residing in the selected Unit during this Move-In Date selected */
                  const title = 'Verify';
                  const primaryMessage = `Our records show that another tenant was living in this unit on this move-In date, please review your records and select a different move-in date or revise the prior tenant housing record.`;
                  const secondaryMessage = ``;
                  const confirmButtonName = 'OK';
                  const dismissButtonName = '';
                  this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                    positiveResponse => { },
                    negativeResponse => { }
                  );
                } else if (val.wasUnitOffline) {
                  /* Offline Period Validation
                    Unit was Offline during the selected Move-In Date */
                  const title = 'Verify';
                  const primaryMessage = `Our records show that this unit was offline during period.`;
                  const secondaryMessage = `Do you still want to continue?`;
                  const confirmButtonName = 'Yes';
                  const dismissButtonName = 'No';
                  this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                    positiveResponse => {
                      if (comments === '') {
                        this.toastrService.error('Please write some comments.');
                      } else {
                        this.transmitPlacement(savePlcmtOutcome, '/vcs/tenant-roster');
                      }
                    },
                    negativeResponse => {
                      const title = 'Verify';
                      const primaryMessage = `Please select a different Move-In Date.`;
                      const secondaryMessage = ``;
                      const confirmButtonName = 'OK';
                      const dismissButtonName = '';
                      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                        positiveResponse => { },
                        negativeResponse => { }
                      );
                    }
                  );
                } else if (val.wasClientResiding) {
                  /* Existing Tenant Verification
                  Client is currently residing in a unit or was residing in a unit during the selected Move-In Date */
                  const title = 'Verify';
                  const primaryMessage = `This tenant is listed as residing in ` + val.wasResidingUnit + ` during this period. Please review your records and select a different move-in date or revise the prior housing record`;
                  const secondaryMessage = ``;
                  const confirmButtonName = 'OK';
                  const dismissButtonName = '';
                  this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                    positiveResponse => { },
                    negativeResponse => { }
                  );
                } else if (val.isClientResiding) {
                  /* Existing Tenant Verification
                  Client is currently residing in a unit or was residing in a unit during the selected Move-In Date */
                  const title = 'Verify';
                  const primaryMessage = `This tenant is currently residing in ` + val.currentlyResidingUnit;
                  const secondaryMessage = ``;
                  const confirmButtonName = 'OK';
                  const dismissButtonName = '';
                  this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                    positiveResponse => { },
                    negativeResponse => { }
                  );
                } else {
                  this.transmitPlacement(savePlcmtOutcome, '/vcs/tenant-roster');
                }
              }
            });
          }
        }
      }
    }

  }

  transmitPlacement(savePlcmtOutcome: IPlacementOutcome, redirectTo: string) {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to transmit? `;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.saveVCSPlacementOutcomeSub = this.referralRosterService.saveVCSPlacementOutcome(savePlcmtOutcome).subscribe((rslt: number) => {
          if (rslt > 0) {
            this.toastrService.success('Placement Outcome saved successfully.');
            // this.referralRosterService.setCurrentReferralRosterTabIndex(0);

            if (this.referralType == 526 || this.referralType == 610) {
              this.vcsReferralIDforSidedoor.next(rslt);
              this.isSideDoorMoveInDone = true;
              // this.interviewOutcomeGroup.disable();
              this.isMainFormTabDisabled = true;
              this.isDocTabDisabled = false;
              if (this.referralType == 526) {
                this.selectedTab = 2;
              } else if (this.referralType == 610) {
                this.selectedTab = 1;
              }
            } else {
              this.isSideDoorMoveInDone = false;
              this.isMainFormTabDisabled = false;
              if (this.isTadWorkflow) {
                this.router.navigate(['/vcs/tad/tad-submission']);
              } else {
                this.router.navigate([redirectTo]);
              }
            }
          }
        });
      },
      negativeResponse => { }
    );

  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        if (this.isTadWorkflow) {
          this.router.navigate(['/vcs/tad/tad-submission']);
        } else {
          if (this.referralType == 525) {
            this.router.navigate(['/vcs/referral-roster']);
          } else if (this.referralType == 526 || this.referralType == 610) {
            this.router.navigate(['/vcs/tenant-roster']);
          }
        }
      },
      negativeResponse => { }
    );

  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.rrSelectedSub) {
      this.rrSelectedSub.unsubscribe();
    }
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.placementOutcomeFormEditableSub) {
      this.placementOutcomeFormEditableSub.unsubscribe();
    }
    if (this.existingPlacementOutcomeSub) {
      this.existingPlacementOutcomeSub.unsubscribe();
    }
    if (this.availableInterviewTimeSlotSub) {
      this.availableInterviewTimeSlotSub.unsubscribe();
    }
    if (this.vcsOnlineUnitsSub) {
      this.vcsOnlineUnitsSub.unsubscribe();
    }
    if (this.vcsMatchCriteriaSub) {
      this.vcsMatchCriteriaSub.unsubscribe();
    }
    if (this.wasInterviewConductedCtrlSub) {
      this.wasInterviewConductedCtrlSub.unsubscribe();
    }
    if (this.interviewOutcomeCtrlSub) {
      this.interviewOutcomeCtrlSub.unsubscribe();
    }
    if (this.placementOutcomeCtrlSub) {
      this.placementOutcomeCtrlSub.unsubscribe();
    }
    if (this.trSelectedSub) {
      this.trSelectedSub.unsubscribe();
    }
    if (this.sideDoorClientSearchResultSub) {
      this.sideDoorClientSearchResultSub.unsubscribe();
    }
    if (this.raHpAgenciesSub) {
      this.raHpAgenciesSub.unsubscribe();
    }
    if (this.moveInValidatorSub) {
      this.moveInValidatorSub.unsubscribe();
    }
    if (this.saveVCSPlacementOutcomeSub) {
      this.saveVCSPlacementOutcomeSub.unsubscribe();
    }
    if (this.istadWorkflowSub) {
      this.istadWorkflowSub.unsubscribe();
    }
  }
}
