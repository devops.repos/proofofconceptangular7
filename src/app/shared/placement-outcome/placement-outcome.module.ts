import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { PlacementOutcomeComponent } from './placement-outcome.component';
import { DocumentModule } from '../document/document.module';
import { ContentBannerModule } from '../content-banner/content-banner.module';
import { ConfirmDialogModule } from '../confirm-dialog/confirm-dialog.module';
import { DirectivesModule } from '../directives/directives.module';

@NgModule({
  declarations: [PlacementOutcomeComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ErrorsModule,
    HttpClientModule,
    ContentBannerModule,
    DocumentModule,
    ConfirmDialogModule,
    DirectivesModule
  ],
  exports: [PlacementOutcomeComponent]
})
export class PlacementOutcomeModule { }
