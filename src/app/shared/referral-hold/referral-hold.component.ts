import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { RefGroupDetails } from "src/app/models/refGroupDetailsDropDown.model";
import { CommonService } from "src/app/services/helper-services/common.service";
import { Subscription } from 'rxjs';
import { VCSReferralReady, VCSReferralReadyHistory } from 'src/app/pact-modules/vacancyControlSystem/referral-package/referral-package.model';
import { ReferralService } from "src/app/pact-modules/vacancyControlSystem/referral-package/referral-package.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatRadioChange, MatSelectChange } from '@angular/material';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';

@Component({
  selector: 'app-referral-hold',
  templateUrl: './referral-hold.component.html',
  styleUrls: ['./referral-hold.component.scss']
})
export class ReferralHoldComponent implements OnInit {

  getRefHistorySub: Subscription;
  commonServiceSub: Subscription; 

  responseItems: RefGroupDetails[];
  holdReasons: RefGroupDetails[];
  history: VCSReferralReadyHistory[] = [];  
  clientDocGroup: FormGroup;
  oldClientDocData: VCSReferralReady;
  //isDisabled = true;

  gridColumnApi: any;
   defaultColDef: any;
   //context: any;
   overlayLoadingTemplate: string;
   overlayNoRowsTemplate: string;
  columnDefs: any;
  

  @Input() clientDocData: VCSReferralReady;
  @Input() isDisabled: boolean = false;
  @Output() onValueChange : EventEmitter<VCSReferralReady> = new EventEmitter<VCSReferralReady>();
  public gridOptions: GridOptions;
  constructor(private commonService: CommonService,
              private referralService: ReferralService, 
              private formBuilder: FormBuilder) { 
    
    this.gridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.defaultColDef = {
      filter: false,
      resizable: true
    };

    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the history is loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No History Available</span>';

    this.columnDefs = [       
      {
        headerName: 'Date',
        field: 'createdDate',
        width: 90
      },
      {
        headerName: 'Client Status',
        field: 'clientStatus',
        width: 90
      },
      {
        headerName: 'Hold Reason',
        field: 'holdReason',
        //width: 170
      },
      {
        headerName: 'Comments',
        field: 'clientComments',
        //width: 170
      }
    ]; 
  
    this.clientDocGroup = this.formBuilder.group({
      vcsClientPackageID: [0],      
      isClientRefReady: [{value: 0, disabled: true}],
      clientRefReadyComment: [""],
      clientNotReadyReason: [""],
      clientNotReadyOtherSpecify: [""],
      clientNotReadyAddlComments: [""]
    });
  
  }
  ngOnChanges() {
    if(this.clientDocData != undefined){
      this.clientDocGroup.patchValue(this.clientDocData);
      if(this.clientDocData.vcsClientPackageID != null){
        this.getRefReadyHistory(this.clientDocData.vcsClientPackageID);        
      }
    }
    if(this.isDisabled) {
      this.clientDocGroup.controls.isClientRefReady.disable();
    }
    else {
      this.clientDocGroup.controls.isClientRefReady.enable();
    }
   
  }

  ngOnInit() {
    this.commonServiceSub = this.commonService.getRefGroupDetails("7,58").subscribe(
      res => {
        const data = res.body as RefGroupDetails[];  
          
        this.responseItems = data.filter(d => {
          return d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34);
        }); 

        this.holdReasons = data.filter(d => { 
          return d.refGroupID === 58;
        });
                   
      },
      error => console.error("Error!", error)
    );  
   
}

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  } 

  onGridReady(params: { api: any; columnApi: any })  {
    //this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    params.api.setDomLayout('autoHeight'); 

    this.gridColumnApi.autoSizeColumns(); 

  }

  isClientRefReadyChange(event: MatSelectChange) {
    if (event.value == 33) {
    this.clientDocGroup.controls["clientNotReadyReason"].setValue("");
    this.clientDocGroup.controls["clientNotReadyAddlComments"].setValue("");
    this.clientDocGroup.controls["clientNotReadyOtherSpecify"].setValue("");
   }
   else if (event.value == 34) {
    this.clientDocGroup.controls["clientRefReadyComment"].setValue("");
   }
   else if  (event.value == 0) {
    this.clientDocGroup.controls["clientNotReadyReason"].setValue("");
    this.clientDocGroup.controls["clientNotReadyAddlComments"].setValue("");
    this.clientDocGroup.controls["clientRefReadyComment"].setValue("");
    this.clientDocGroup.controls["clientNotReadyOtherSpecify"].setValue("");    
   }
   this.onChange();
  }

  clientNotReadyReasonChange(event: MatRadioChange) {
    //this.showBtn = true;
    if (event.value != 510) {
      this.clientDocGroup.controls["clientNotReadyOtherSpecify"].setValue("");  
      this.onChange();  
    }
  }

  getRefReadyHistory(packageID: number){
    this.getRefHistorySub = this.referralService.getClientHistory(packageID).subscribe(res => {
      this.history = res as VCSReferralReadyHistory[];
    });

  }

  onChange(){
    this.clientDocData = this.clientDocGroup.value;
    this.onValueChange.emit(this.clientDocData);
  }

  onClear(OldClientDocData: VCSReferralReady){
    this.clientDocGroup.patchValue(OldClientDocData);
    this.onChange();
  }
}
