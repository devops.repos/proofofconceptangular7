import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { AgGridModule } from 'ag-grid-angular';
import { NgxMaskModule } from 'ngx-mask';
import { ReferralHoldComponent } from './referral-hold.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ReferralHoldComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    AgGridModule.withComponents(),
    NgxMaskModule,
    ReactiveFormsModule,
  ],
  exports: [ReferralHoldComponent]
})

export class ReferralHoldModule { }