import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralHoldComponent } from './referral-hold.component';

describe('ReferralHoldComponent', () => {
  let component: ReferralHoldComponent;
  let fixture: ComponentFixture<ReferralHoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralHoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralHoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
