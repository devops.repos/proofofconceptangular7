import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedUnitHistoryComponent } from './shared-unit-history.component';

describe('UnitHistoryComponent', () => {
  let component: SharedUnitHistoryComponent;
  let fixture: ComponentFixture<SharedUnitHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SharedUnitHistoryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedUnitHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
