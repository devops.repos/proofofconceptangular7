import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import "ag-grid-enterprise";
import { ToastrModule } from 'ngx-toastr';

import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { SharedUnitDetailsComponent } from './shared-unit-details.component';
import { SharedUnitDetailsActionComponent } from './shared-unit-details-action.component';
import { SharedUnitHistoryDialogComponent } from './shared-unit-history-dialog.component';
import { SharedUnitHistoryComponent } from './shared-unit-history.component';
import { SharedCalenderDialogComponent } from './shared-calender-dialog.component';
import { PactCalendarModule } from 'src/app/shared/calendar/pact-calendar.module';
import { TutorialModule } from 'src/app/shared/tutorial/tutorial.module';


//import { PriorDocActionComponent } from './prior-doc-action.component';



@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    ReactiveFormsModule,
    PactCalendarModule,
    TutorialModule,
    ToastrModule.forRoot({
      progressBar: true,
      progressAnimation: 'increasing'
    }),
    AgGridModule.withComponents([
      SharedUnitDetailsComponent,
      SharedUnitDetailsActionComponent,
      SharedUnitHistoryComponent,
      SharedCalenderDialogComponent
    ])
  ],
  declarations: [
    SharedUnitDetailsComponent,
    SharedUnitDetailsActionComponent,
    SharedUnitHistoryDialogComponent,
    SharedUnitHistoryComponent,
    SharedCalenderDialogComponent

  ],
  entryComponents: [SharedUnitHistoryDialogComponent, SharedCalenderDialogComponent],
  exports: [
    SharedUnitDetailsComponent
  ]
})
export class SharedUnitDetailsModule { }
