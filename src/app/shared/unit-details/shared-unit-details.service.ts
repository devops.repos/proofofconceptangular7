import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Unit, SiteAgreementPopulation } from "./shared-unit-details-model"

@Injectable({
  providedIn: 'root'
})
export class SharedUnitDetailsService {
  siteAdminUrl = environment.pactApiUrl + 'SiteAdmin';
  getUnitsRequestURL = environment.pactApiUrl + 'Unit/GetUnits';
  saveUnitRequestURL = environment.pactApiUrl + 'Unit/SaveUnitDetails';
  deleteUnitRequestURL = environment.pactApiUrl + 'Unit/DeleteUnit';
  monitorVacanciesUrl = environment.pactApiUrl + 'VacancyMonitor';
  vCSTADUrl = environment.pactApiUrl + 'Unit/GetTADRules';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }


  getSiteDemogramics(value: string) {
    return this.httpClient.get(this.siteAdminUrl + "/GetSiteDemographic", { params: { siteId: value }, observe: 'response' });
  }



  getSiteAgreementPopulation(value: string) {
    return this.httpClient.get(this.siteAdminUrl + "/GetSiteAgreementPopulation", { params: { siteId: value }, observe: 'response' });
  }


  getUnitDetails(SiteID: number): Observable<any> {
    return this.httpClient.get(this.getUnitsRequestURL + '?SiteId=' + SiteID, this.httpOptions);
  }

  saveUnitDetails(unit: Unit): Observable<any> {
    return this.httpClient.post(this.saveUnitRequestURL, JSON.stringify(unit), this.httpOptions);
  }

  deleteUnit(UnitID: number, UserName: string): Observable<any> {
    let reqData = { "UnitID": UnitID, "UserName": UserName }
    return this.httpClient.post(this.deleteUnitRequestURL, reqData, this.httpOptions);
  }

  savePrimaryServiceContract(siteAgreementPopulation: SiteAgreementPopulation) {
    return this.httpClient.post(this.siteAdminUrl + "/SavePrimaryServiceContract", JSON.stringify(siteAgreementPopulation), this.httpOptions);
  }

  deletePrimaryServiceContract(SiteConsiteAgreementPopulationtact: SiteAgreementPopulation) {
    return this.httpClient.post(this.siteAdminUrl + "/DeletePrimaryServiceContract", JSON.stringify(SiteConsiteAgreementPopulationtact), this.httpOptions);
  }


  getUnitHistory(value: string) {
    return this.httpClient.get(this.siteAdminUrl + "/GetUnitHistory", { params: { UnitId: value }, observe: 'response' });
  }


  getAgreementPopulationUnitTypes(value: string, value1: string) {
    return this.httpClient.get(this.siteAdminUrl + "/GetAgreementPopulationUnitTypes", { params: { agreementPopulationID: value, ContarctAgencyType: value1 }, observe: 'response' });
  }

  getAgreementRentalSubsidies(value: string) {
    return this.httpClient.get(this.siteAdminUrl + "/GetAgreementRentalSubsidies", { params: { primaryserviceContratcID: value }, observe: 'response' });
  }

  getUnitOfflineReason() {
    return this.httpClient.get(this.monitorVacanciesUrl + "/GetUnitOfflineReason", { observe: 'response' });
  }

  getVCSTADValidator(roseterid: number) {
    return this.httpClient.get(this.vCSTADUrl + '?rosterID=' + roseterid, { observe: 'response' });
  }


}