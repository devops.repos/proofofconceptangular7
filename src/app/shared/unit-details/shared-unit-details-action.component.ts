import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { Unit } from '../unit-details/shared-unit-details-model';
import { ToastrService } from 'ngx-toastr';

// import { PACTDemographicsContacts } from './demographics.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "doc-action",
  template: `
   <mat-icon
  class=".doc-action-icon"
  color="warn"
  [matMenuTriggerFor]="UnitAction"
  >
  more_vert
  </mat-icon>

  <mat-menu #UnitAction="matMenu">
  <button mat-menu-item  (click)="onView()">Edit Unit</button>
  <button *ngIf="is_CAS === true || is_HP_staff === true" mat-menu-item  (click)="onUnitHistroy()">Unit History</button>
  <button  mat-menu-item  (click)="onDelete()">Delete Unit</button>
    </mat-menu>
    <mat-icon *ngIf="isTAD === true" class="tr-mat-info-icon" matTooltip="{{tooltipMessage}}"  color="primary" class="doc-action-icon">info</mat-icon>
    `
    ,
  
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})

export class SharedUnitDetailsActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private unitSelected: Unit;
  public isTAD: boolean = false;
  public is_CAS: boolean = false;
  public is_HP_staff: boolean= false;
  private tooltipMessage: string = '';


  constructor(
    private toastr: ToastrService,
  ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    if (this.params.context.componentParent.is_CAS === true) {
      this.is_CAS = true;
    }
    if (this.params.context.componentParent.is_HP_Staff === true) {
      this.is_HP_staff = true;
    }
    // console.log(params.data);
    if (this.params.context.componentParent.isTAD === true) {
      this.tooltipMessage = "Missing fields are :"
      if (this.params.context.componentParent.isUnitNameRule === true) {
        if (params.data.name === null || params.data.name.trim() === '') {
          this.isTAD = true;
          this.tooltipMessage = this.tooltipMessage + `\n Unit name, `
        }
      }
      if (this.params.context.componentParent.isUnitTypeRule === true) {
        if (params.data.unitTypeDesc === null || params.data.unitTypeDesc.trim() === '') {
          this.isTAD = true;
          this.tooltipMessage = this.tooltipMessage + `\n Unit type , `
        }
      }

      if (this.params.context.componentParent.isUnitFeatresRule === true) {
        if (params.data.unitFeaturesString === null || params.data.unitFeaturesString.trim() === '') {
          this.isTAD = true;

          this.tooltipMessage = this.tooltipMessage + `\n Unit features,`
        }
      }
      if (this.params.context.componentParent.isUnitRentalSubsidyRule === true) {
        if (params.data.rentalSubsidyString === null || params.data.rentalSubsidyString.trim() === '') {
          this.isTAD = true;
          this.tooltipMessage = this.tooltipMessage + `\n rental subsidies, `
        }
      }
    }

  }

  onView() {
    this.prepareUnitObject();
    // alert(this.unitSelected.isVacancyMonitorRequired);
    // alert(this.unitSelected.isVerified);

    /*  if  (this.unitSelected.isVacancyMonitorRequired === true && this.unitSelected.isVerified === false)
      {
        this.toastr.info("Vacancy verification is not done,Can't edit Unit", 'Info');
      }
      else {*/
    this.params.context.componentParent.unitViewInParent(this.unitSelected);
    /*}*/
  }

  onDelete() {
    this.prepareUnitObject();
    /*  if  (this.unitSelected.isVacancyMonitorRequired === true && this.unitSelected.isVerified === false)
      {
        this.toastr.info("Vacancy verification is not done,Can't delete Unit", 'Info');
      }
      else {*/
    this.params.context.componentParent.unitDeleteInParent(this.unitSelected);
    /* }*/
  }

  prepareUnitObject() {
    this.unitSelected = {
      unitID: this.params.data.unitID,
      name: this.params.data.name,

      address: this.params.data.address,
      city: this.params.data.city,
      state: this.params.data.state,
      zip: this.params.data.zip,

      isSideDoorAllowed: this.params.data.isSideDoorAllowedString === 'Yes' ? true : false,
      isVacancyMonitorRequired: this.params.data.isVacancyMonitorRequiredString === 'Yes' ? true : false,
      isVacancyMonitorRequiredString: this.params.data.isVacancyMonitorRequiredString,
      isSideDoorAllowedString: this.params.data.isVacancyMonitorRequiredString,

      siteAgreementPopulationID: this.params.data.siteAgreementPopulationID,
      primaryServiceAgreementPopName: this.params.data.primaryServiceAgreementPopName,

      unitTypeID: this.params.data.unitTypeID,
      unitTypeDesc: this.params.data.unitTypeDesc,

      unitStatusID: this.params.data.unitStatusID,
      unitStatusDesc: this.params.data.unitStatusDesc,

      contractAgencyID: this.params.data.contractAgencyID,
      contractAgencyDesc: this.params.data.contractAgencyDesc,

      unitFeatures: this.params.data.unitFeatures,
      rentalSubsidies: this.params.data.rentalSubsidies,

      unitFeaturesJson: this.params.data.unitFeaturesJson,
      unitFeaturesString: this.params.data.unitFeaturesString,

      rentalSubsidyJson: this.params.data.rentalSubsidyJson,
      rentalSubsidyString: this.params.data.rentalSubsidyString,

      userName: this.params.data.userName,
      isActive: this.params.data.isActive,

      siteAgreementPopulationSelected: this.params.data.siteAgreementPopulationSelected,
      contractAgencySelected: this.params.data.contractAgencySelected,
      unitTypeSelected: this.params.data.unitTypeSelected,
      unitStatusSelected: this.params.data.unitStatusSelected,
      isVerified: this.params.data.isVerified,
      offlineReasonType: this.params.data.offlineReasonType,
      offlineOtherSpecify: this.params.data.offlineOtherSpecify,
      expectedAvailableDate: this.params.data.expectedAvailableDate

    };
  }

  refresh(): boolean {
    return false;
  }

  onUnitHistroy() {
    this.prepareUnitObject();
    this.params.context.componentParent.unitHistoryInParent(this.unitSelected);
  }


}