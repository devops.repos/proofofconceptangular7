import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { StatusBarHeader } from "./shared-unit-details-model"
@Component({
  selector: 'app-shared-unit-history-dialog',
  templateUrl: './shared-unit-history-dialog.component.html',
  styleUrls: ['./shared-unit-history-dialog.component.scss'],
})
export class SharedUnitHistoryDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public UnithistoryData: StatusBarHeader,
    private dialogRef: MatDialogRef<SharedUnitHistoryDialogComponent>
  ) { }

  //Close the dialog on close button
  CloseDialog() {
    this.dialogRef.close(true);
  }

  //On Init
  ngOnInit() { }
}
