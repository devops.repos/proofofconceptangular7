import {
  Component, OnInit, Input, OnDestroy, Output,
  EventEmitter
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmDialogService } from '../../shared/confirm-dialog/confirm-dialog.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GridOptions } from 'ag-grid-community';
import { SharedUnitDetailsActionComponent } from './shared-unit-details-action.component';
import { SharedUnitDetailsService } from '../unit-details/shared-unit-details.service';
import { Unit, RentalSubsidy, UnitFeature, PrimaryServiceContractAgreementPopulation, StatusBarHeader, siteDemographic, SiteAgreementPopulation, UnitOfflineReason, VCSTADRule, SharedCalenderDialogData } from '../unit-details/shared-unit-details-model';
import { MatDialog } from '@angular/material';
import { SharedUnitHistoryDialogComponent } from './shared-unit-history-dialog.component';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { SharedCalenderDialogComponent } from './shared-calender-dialog.component';
import { IVCSAgencySiteInfoForCalendar } from 'src/app/shared/calendar/pact-calendar-interface.model';

@Component({
  selector: 'shared-unit-details',
  templateUrl: './shared-unit-details.component.html',
  styleUrls: ['./shared-unit-details.component.scss']
})

export class SharedUnitDetailsComponent implements OnInit, OnDestroy {


  unitDetailsFormGroup: FormGroup;
  totalUnits: number = 0; // fetch no. of. units from API service
  agunits:number=0;
  agreementpopulationID = 0;
  @Input() siteID: number;
  @Input() siteagreementPopulationId: number;
  @Input() isUnitRoaster = false;
  @Output() unitBackbtnclick = new EventEmitter();
  @Input() readonlymode: boolean = false;
  @Input() isTAD: boolean = false;
  @Input() tadValidationMsg = '';
  @Output() UnitDataChanged = new EventEmitter();

  isUnitTypeRule: boolean = true;
  isUnitNameRule: boolean = true;
  isUnitFeatresRule: boolean = false;
  isUnitRentalSubsidyRule: boolean = true;
  actionLabel: string = 'Add'


  public gridOptions: GridOptions;

  userDataSub: Subscription;
  commonServiceSub: Subscription;
  commonServiceSub2: Subscription;
  UnitServiceSub1: Subscription;
  UnitServiceSub2: Subscription;
  UnitServiceSub3: Subscription;

  contractAgencyTypes: RefGroupDetails[] = [];
  unitFeatures: RefGroupDetails[] = [];
  rentalSubsidies: RefGroupDetails[] = [];
  unitTypes: RefGroupDetails[] = [];
  unitStatuses: RefGroupDetails[] = [];
  primarySvcContractTypes: PrimaryServiceContractAgreementPopulation[] = [];
  siteDetail: siteDemographic;
  unitAddressVisible: boolean = false;
  siteAgrrementPopulationData: SiteAgreementPopulation[];
  pagination: any;
  rowSelection: any;
  context: any;
  columnDefs: any;
  defaultColDef: any;
  frameworkComponents: any;
  gridApi: any;

  unitsDataObj: Unit[];
  unitSelected: Unit = new Unit();
  siteStatusHeader: StatusBarHeader;
  unitOfflineReasons: UnitOfflineReason[] = [];
  unitOfflineReasonsWithoutFilter: UnitOfflineReason[] = [];
  calenderdata: SharedCalenderDialogData;
  agencySiteInfo: IVCSAgencySiteInfoForCalendar;

  userData: AuthData;
  is_SH_PE = false;
  is_SH_HP = false;
  is_CAS = false;
  is_HP_Staff= false;

  formErrors = {
    primarySvcContractCtrl: '',
    contractAgencyCtrl: '',
    unitNameCtrl: '',
    unitTypeCtrl: '',
    unitStatusCtrl: '',
    unitExpectedAvailableDateCtrl: '',
    unitOfflineReasonCtrl: ''
  };
  validationMessages = {

    primarySvcContractCtrl: {
      'required': 'Please select primary service contract.',


    },
    contractAgencyCtrl: {
      'required': 'Please select contracting Agency .',

    },
    unitNameCtrl: {
      'required': 'Unit Name is required .',

    },

    unitTypeCtrl: {
      'required': 'Please select unit type .',

    },
    unitStatusCtrl: {
      'required': 'Please select unit status',
      'dateValidator': 'TCO Contarct date is invalid'
    },
    unitOfflineReasonCtrl: {
      'required': 'Please select unit offline reason ',
    },
    unitExpectedAvailableDateCtrl: {
      'required': 'Please enter the expected available date .',
      'dateValidator': 'Contract date is invalid'
    }

  };


  constructor(
    private toastr: ToastrService,
    private router: Router,
    private userService: UserService,
    private commonService: CommonService,
    private confirmDialogService: ConfirmDialogService,
    private sharedUnitDetailsService: SharedUnitDetailsService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,

  ) {

    this.rowSelection = 'single';
    this.pagination = true;
    this.context = { componentParent: this };

    this.gridOptions = {
      rowHeight: 35,


      getRowStyle: (params) => {
        if (this.isTAD === true) {

          if (this.isUnitNameRule === true) {
            if (params.node.data.name === null || params.node.data.name.trim() === '') {
              return { background: '#FFB74D' };
            }
          }
          if (this.isUnitTypeRule === true) {
            if (params.node.data.unitTypeDesc === null || params.node.data.unitTypeDesc.trim() === '') {
              return { background: '#FFB74D' };

            }
          }

          if (this.isUnitFeatresRule === true) {
            if (params.node.data.unitFeaturesString === null || params.node.data.unitFeaturesString.trim() === '') {
              return { background: '#FFB74D' };
            }
          }
          if (this.isUnitRentalSubsidyRule === true) {
            if (params.node.data.rentalSubsidyString === null || params.node.data.rentalSubsidyString.trim() === '') {
              return { background: '#FFB74D' };
            }
          }
        }

      },
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;

    this.frameworkComponents = {
      actionRenderer: SharedUnitDetailsActionComponent
    };

    this.columnDefs = [
      {
        headerName: 'Actions',
        field: 'action',
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      },
      {
        headerName: 'Primary Service Agreement Pop',
        field: 'primaryServiceAgreementPopName',
        width: 130,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Contracting Agency',
        field: 'contractAgencyDesc',
        width: 130,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit#',
        field: 'unitID',
        width: 70,
        hide: true,
        suppressFilter: true,
        suppressToolPanel: true
      },
      {
        headerName: 'Unit Name',
        field: 'name',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDesc',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Status',
        field: 'unitStatusDesc',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Address',
        field: 'address',
        width: 100,
        filter: 'agTextColumnFilter',
        hide: true
      },
      {
        headerName: 'City',
        field: 'city',
        width: 100,
        filter: 'agTextColumnFilter',
        hide: true
      },
      {
        headerName: 'State',
        field: 'state',
        width: 70,
        filter: 'agTextColumnFilter',
        hide: true
      },
      {
        headerName: 'Zip',
        field: 'zip',
        width: 70,
        filter: 'agTextColumnFilter',
        hide: true
      },
      {
        headerName: 'Unit Features',
        field: 'unitFeaturesString',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Rental Subsidies',
        field: 'rentalSubsidyString',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Direct Referral',
        field: 'isSideDoorAllowedString',
        width: 80,
        filter: 'agTextColumnFilter',
        suppressFilter: true,
        suppressToolPanel: true
      },
      {
        headerName: 'Monitor Vacancy',
        field: 'isVacancyMonitorRequiredString',
        width: 80,
        filter: 'agTextColumnFilter',
        suppressFilter: true,
        suppressToolPanel: true
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true

    };
  }

  ngOnInit() {

    this.unitDetailsFormGroup = this.formBuilder.group({
      primarySvcContractCtrl: ['', [Validators.required]],
      contractAgencyCtrl: ['', [Validators.required]],
      unitIdCtrl: ['', []],
      unitNameCtrl: ['', [Validators.required]],
      unitFeaturesCtrl: ['', []],
      unitFeaturesListCtrl: ['', []],
      rentalSubsidyCtrl: ['', []],
      rentalSubsidyListCtrl: ['', []],
      unitTypeCtrl: ['', [Validators.required]],
      unitStatusCtrl: ['', [Validators.required]],
      unitAddressCtrl: ['', []],
      unitCityCtrl: ['', []],
      unitStateCtrl: ['', []],
      unitZipCtrl: ['', []],
      sideDoorCtrl: ['', []],
      vacancyMonitorCtrl: ['', []],
      unitOfflineReasonCtrl: ['', [Validators.required]],
      unitExpectedAvailableDateCtrl: ['', [Validators.required]],
      unitaddlComment: ['', []]
    });
    this.siteStatusHeader = {} as StatusBarHeader;
    this.loadRefGroupDetails();
    this.GetAllUnitsBySite(this.siteID);
    this.getSiteDetails();
    this.getPrimartSerivceContractList();
    this.loadUnitOfflineReason();
    this.loadTADVaildationRules()

    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        this.userData.siteCategoryType.forEach(ct => {
          if (ct.siteCategory === UserSiteType.SH_PE) {
            this.is_SH_PE = true;
          }
          else if (ct.siteCategory === UserSiteType.SH_HP) {
            this.is_SH_HP = true;
            //alert(this.userData.roleId);
            if (this.userData.roleId === 1) 
            {
              this.is_HP_Staff = true;
            }
          }
          else if (ct.siteCategory === UserSiteType.CAS) {
            this.is_CAS = true;
          }
        });
       /* if  (this.is_SH_HP  === true)
        {
          this.unitDetailsFormGroup.controls.contractAgencyCtrl.disable();
        }*/
      }
    });

    if (this.isTAD === true || this.isUnitRoaster === true) {
      // this.unitDetailsFormGroup.controls.primarySvcContractCtrl.disable();
    }

  }

  loadRefGroupDetails() {
    const rentalSubsidiesRefGroupId = 15; const unitTypeRefGroupId = 29; const unitFeaturesRefGroupId = 14;
    const unitStatusRefGroupId = 52;
    const contractAgencyRefGroupId = 55;
    this.commonServiceSub = this.commonService.getRefGroupDetails(rentalSubsidiesRefGroupId + ',' + unitTypeRefGroupId + ',' + unitFeaturesRefGroupId + ','
      + contractAgencyRefGroupId + ',' + unitStatusRefGroupId).subscribe(
        res => {
          const data = res.body as RefGroupDetails[];

          this.contractAgencyTypes = data.filter(d => (d.refGroupID === contractAgencyRefGroupId));
          this.unitTypes = data.filter(d => (d.refGroupID === unitTypeRefGroupId));
          this.unitStatuses = data.filter(d => (d.refGroupID === unitStatusRefGroupId && (d.refGroupDetailID === 472 || d.refGroupDetailID === 473)));
          this.rentalSubsidies = data.filter(d => (d.refGroupID === rentalSubsidiesRefGroupId));
          this.unitFeatures = data.filter(d => (d.refGroupID === unitFeaturesRefGroupId));
        },
        error => console.error('Error!', error)
      );

    this.commonServiceSub2 = this.commonService.getPrimaryServiceAgreementPopulation(this.siteID).subscribe(
      res => {
        this.primarySvcContractTypes = res as PrimaryServiceContractAgreementPopulation[];

        /* if ((this.siteagreementPopulationId != null ) ||  (this.siteagreementPopulationId != 0))
         {
         
         }*/
      },
      error => console.error('Error!', error)
    );
  }

  unitFeatureSelected(data) {

    let featuresSelected: UnitFeature[] = [];

    data.value.forEach(element => {
      let item = this.unitFeatures.find(it => it.refGroupDetailID === element);
      featuresSelected.push({
        "f_Id": item.refGroupDetailID,
        "f_desc": item.refGroupDetailDescription
      });
    });
    this.unitSelected.unitFeatures = featuresSelected;
  }

  rentalSubsidySelected(data) {

    let subsidySelected: RentalSubsidy[] = [];

    data.value.forEach(element => {
      let item = this.rentalSubsidies.find(it => it.refGroupDetailID === element);
      subsidySelected.push({
        "sub_Id": item.refGroupDetailID,
        "sub_desc": item.refGroupDetailDescription
      });
    });
    this.unitSelected.rentalSubsidies = subsidySelected;
  }

  onFirstDataRendered(params) {
    //params.api.sizeColumnsToFit();
  }

  onGridReady = (params: { api: any; columnApi: any }) => {
    // const allColumnIds = [];
    params.api.setDomLayout('autoHeight');
    if (this.is_SH_HP === true) {
      params.columnApi.setColumnVisible('isSideDoorAllowedString', false);
      params.columnApi.setColumnVisible('isVacancyMonitorRequiredString', false);
    }
    params.api.sizeColumnsToFit();
    this.gridApi = params.api;


  }



  GetAllUnitsBySite(siteID) {

    this.UnitServiceSub1 = this.sharedUnitDetailsService.getUnitDetails(this.siteID).subscribe(
      res => {
        this.FormatUnitsDataObject(res);
      },
      error => console.error('Error!', error)
    );
  }

  FormatUnitsDataObject(res) {
    let unitList = res as Unit[];
    let newUnitList: Unit[] = [];
    newUnitList = unitList.map(unit => {

      unit.unitFeatures = JSON.parse(unit.unitFeaturesJson);
      unit.unitFeaturesString = this.getFeaturesString(unit.unitFeatures);

      unit.rentalSubsidies = JSON.parse(unit.rentalSubsidyJson);
      unit.rentalSubsidyString = this.getSubsidiesString(unit.rentalSubsidies);

      return unit;
    });

    if (this.siteagreementPopulationId != 0) {
      this.unitsDataObj = newUnitList.filter(d => (d.siteAgreementPopulationID === this.siteagreementPopulationId))
      this.loadUnitTypes(this.siteagreementPopulationId, "0");
      this.loadRentalsubsities(this.siteagreementPopulationId);
    }
    else {
      this.unitsDataObj = newUnitList; // massaged object for GRID
    }

    

    this.totalUnits = this.unitsDataObj.length;
  }

  getFeaturesString(UnitFeaturesArray: UnitFeature[]) {
    let featuresString: string = null;

    if (UnitFeaturesArray) {
      UnitFeaturesArray.forEach(feature => {
        featuresString = featuresString ? (featuresString + ' ,' + feature.f_desc) : feature.f_desc;
      });
      return featuresString.substring(0, featuresString.length);
    }
    return '';
  }

  getSubsidiesString(rentSubsidyArray: RentalSubsidy[]) {
    let subsidiesString: string = null;

    if (rentSubsidyArray) {
      rentSubsidyArray.forEach(subsidy => {
        subsidiesString = subsidiesString ? (subsidiesString + ' ,' + subsidy.sub_desc) : subsidy.sub_desc;
      });
      return subsidiesString.substring(0, subsidiesString.length);
    }
    return '';
  }

  unitViewInParent(unitSelect: Unit) {

    /* if (cell.email != null) {
      this.siteContactModel.email = cell.email;
    }
    if (cell.extension != null) {
      this.siteContactModel.extension = cell.extension;
    }
    else{
      this.siteContactModel.extension = null;
    } */

   // alert(unitSelect.unitStatusID);
    if (!(unitSelect.unitStatusID === 472 || unitSelect.unitStatusID === 473  || unitSelect.unitStatusID === null || unitSelect.unitStatusID === 0 )) {
      this.clearUnit();
      this.unitDetailsFormGroup.markAsUntouched();
      this.toastr.info("Can't modify when unit is occupied", 'Info');
      return;

    }

    if (unitSelect.isVacancyMonitorRequired === true && unitSelect.isVerified === false && unitSelect.contractAgencyID === 488) {
      this.toastr.info("Vacancy verification is not done,Can't modify Unit", 'Info');
      this.clearUnit();
      console.log(this.unitSelected);
      this.unitDetailsFormGroup.markAsUntouched();
      return;
    }
    this.actionLabel = 'Save';

    //alert(unitSelect.siteAgreementPopulationID);
    //alert(unitSelect.unitStatusID);
    this.refershdropdowns();

    unitSelect.siteAgreementPopulationSelected = unitSelect.siteAgreementPopulationID;
    unitSelect.unitStatusSelected = unitSelect.unitStatusID;
    unitSelect.unitTypeSelected = unitSelect.unitTypeID;
    unitSelect.contractAgencySelected = unitSelect.contractAgencyID;

    this.unitSelected = unitSelect;
    console.log(this.unitSelected);
    let newfeaturesSelected: number[] = [];
    if (this.unitSelected.unitFeatures) {
      this.unitSelected.unitFeatures.forEach(it => {
        newfeaturesSelected.push(it.f_Id);
      });
    }
    this.unitDetailsFormGroup.get('unitFeaturesCtrl').setValue(newfeaturesSelected);


    let newSubsidySelected: number[] = [];
    if (this.unitSelected.rentalSubsidies) {
      this.unitSelected.rentalSubsidies.forEach(it => {
        newSubsidySelected.push(it.sub_Id);
      });
    }
    this.unitDetailsFormGroup.get('rentalSubsidyCtrl').setValue(newSubsidySelected);

  }

  unitDeleteInParent(unitDelete: Unit) {
    if (!(unitDelete.unitStatusID === 472 || unitDelete.unitStatusID === 473  || unitDelete.unitStatusID === null || unitDelete.unitStatusID === 0)) {
      this.clearUnit();
      this.unitDetailsFormGroup.markAsUntouched();
      this.toastr.info("Can't delete when unit is occupied", 'Info');
      return;

    }

    if (unitDelete.isVacancyMonitorRequired === true && unitDelete.isVerified === false && unitDelete.contractAgencyID === 488) {
      this.toastr.info("Vacancy verification is not done,Can't delete Unit", 'Info');
      this.clearUnit();
      console.log(this.unitSelected);
      this.unitDetailsFormGroup.markAsUntouched();
      return;
    }
    const title = 'Confirm Delete';
    const primaryMessage = '';
    const secondaryMessage = "Are you sure to delete " + unitDelete.name + " ?";
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => {
          this.UnitServiceSub3 = this.sharedUnitDetailsService.deleteUnit(unitDelete.unitID, this.userData.lanId).subscribe(
            res => {
              this.GetAllUnitsBySite(this.siteID);
              this.clearUnit();
              this.unitDetailsFormGroup.markAsUntouched();
              this.UnitDataChanged.emit();
            },
            error => console.error('Error!', error)
          );
        },
        (negativeResponse) => console.log(),
      );

  }



  saveUnit() {
    this.unitSelected.userName = this.userData.lanId;
    if (this.siteagreementPopulationId != 0) {
      this.unitSelected.siteAgreementPopulationSelected = this.siteagreementPopulationId;
    }
    this.unitSelected.isActive = true;

    if (this.unitSelected && this.unitSelected.unitID > 0) {

    }
    else {
      this.unitSelected.unitID = 0;
    }
    this.GetFormFieldsIntoUnitDetails();

    if (this.siteagreementPopulationId != 0) {
      this.unitSelected.siteAgreementPopulationSelected = this.siteagreementPopulationId;
      this.unitSelected.siteAgreementPopulationID = this.siteagreementPopulationId;
    }

    this.UnitServiceSub2 = this.sharedUnitDetailsService.saveUnitDetails(this.unitSelected).subscribe(
      res => {
        this.toastr.success("Sucess", "Unit detail saved sucessfully");
        this.GetAllUnitsBySite(this.siteID);
        this.clearUnit();
        this.unitDetailsFormGroup.markAsUntouched();
        this.UnitDataChanged.emit();


      },
      error => { this.toastr.error("Save Failed", 'Save Failed'); }
    );


  }

  setValidatorsOnFields() {

    /* this.contactForm.controls.faxCtrl.updateValueAndValidity(); */

  }

  clearValidatorsOnFields() {

    /*      this.contactForm.controls.firstNameCtrl.clearValidators();
            this.contactForm.controls.faxCtrl.clearValidators(); */

  }

  clearUnit() {
    this.unitSelected = new Unit();
    this.unitDetailsFormGroup.get('unitFeaturesCtrl').setValue('');
    this.unitDetailsFormGroup.get('rentalSubsidyCtrl').setValue('');
    this.actionLabel = "Add";
    if (this.siteagreementPopulationId > 0) {
      // this.unitDetailsFormGroup.controls.primarySvcContractCtrl.disable();
      this.unitSelected.siteAgreementPopulationSelected = this.siteagreementPopulationId;


    }
  }

  GetFormFieldsIntoUnitDetails() {
    this.unitSelected.contractAgencyID = this.unitSelected.contractAgencySelected; //this.unitDetailsFormGroup.get('rentalSubsidyCtrl').value;
    this.unitSelected.siteAgreementPopulationID = this.unitSelected.siteAgreementPopulationSelected; //this.unitDetailsFormGroup.get('rentalSubsidyCtrl').value;
    this.unitSelected.unitStatusID = this.unitSelected.unitStatusSelected; //this.unitDetailsFormGroup.get('rentalSubsidyCtrl').value;
    this.unitSelected.unitTypeID = this.unitSelected.unitTypeSelected; //this.unitDetailsFormGroup.get('rentalSubsidyCtrl').value;
  }

  ngOnDestroy = () => {
    this.commonServiceSub.unsubscribe();
    this.commonServiceSub2.unsubscribe();
    this.userDataSub.unsubscribe();
    this.UnitServiceSub1.unsubscribe();
    this.UnitServiceSub2.unsubscribe();
    this.UnitServiceSub3.unsubscribe();
  }

  onUnitBackbtnclick = (): boolean => {
    if (!this.isUnitRoaster) {
      this.unitBackbtnclick.emit();
      return true;
    }
    else {
      this.router.navigate(['/dashboard']);
      return true;

    }
  }





  unitHistoryInParent(unitHistory: Unit) {
    var siteinfo;
    this.sharedUnitDetailsService.getSiteDemogramics(this.siteID.toString())
      .subscribe(
        res1 => {
          siteinfo = res1.body as siteDemographic;
          this.siteStatusHeader = {
            AgencyID: siteinfo.agencyID,
            AgencyName: siteinfo.agencyNo + '-' + siteinfo.agencyName,
            AgencyAddress: '',
            SiteID: this.siteID,
            SiteName: siteinfo.siteNo + '-' + siteinfo.name,
            SiteAddress: siteinfo.address.trim() + "," + siteinfo.city.trim() + "," + siteinfo.state.trim() + "," + siteinfo.zip.trim(),
            SiteAgreementPopulationID: unitHistory.siteAgreementPopulationID,
            AgreementpopulationName: unitHistory.primaryServiceAgreementPopName,
            UnitID: unitHistory.unitID,
            UnitName: unitHistory.name
          }

          this.dialog.open(SharedUnitHistoryDialogComponent, {
            width: '1200px',
            maxHeight: '550px',
            disableClose: true,
            autoFocus: false,
            data: this.siteStatusHeader
          });
        },
        error => console.error('Error!', error)
      );


  }


  getSiteDetails() {
    //alert(this.siteID );
    if (this.siteID > 0) {
      this.sharedUnitDetailsService.getSiteDemogramics(this.siteID.toString())
        .subscribe(
          res1 => {
            this.siteDetail = res1.body as siteDemographic;
            if (this.siteDetail) {
              if (this.siteDetail.locationType) {
                if (this.siteDetail.locationType === 233)
                  this.unitAddressVisible = false;
                else
                  this.unitAddressVisible = true;
              }
              this.siteStatusHeader = {
                AgencyID: this.siteDetail.agencyID,
                AgencyName: this.siteDetail.agencyNo + '-' + this.siteDetail.agencyName,
                AgencyAddress: '',
                SiteID: this.siteID,
                SiteName: this.siteDetail.siteNo + '-' + this.siteDetail.name,
                SiteAddress: this.siteDetail.address.trim() + "," + this.siteDetail.city.trim() + "," + this.siteDetail.state.trim() + "," + this.siteDetail.zip.trim(),
                SiteAgreementPopulationID: null,
                AgreementpopulationName: '',
                UnitID: null,
                UnitName: ''
              }
            }
          },
          error => console.error('Error!', error)
        );
    }
  }


  ngOnChanges() {

    //this.loadRefGroupDetails();
    this.GetAllUnitsBySite(this.siteID);
    this.getSiteDetails();
    this.getPrimartSerivceContractList();
    // this.loadTADVaildationRules();
    //  alert(this.siteagreementPopulationId);


  }

  loadUnitTypes(siteagrid: number, contracttype: string) {

    this.sharedUnitDetailsService.getAgreementPopulationUnitTypes(siteagrid.toString(), contracttype)
      .subscribe(
        res => {
          this.unitTypes = res.body as RefGroupDetails[];
        },
        error => console.error('Error!', error)
      );
  }

  loadRentalsubsities(siteagrid: number) {

    this.sharedUnitDetailsService.getAgreementRentalSubsidies(siteagrid.toString())
      .subscribe(
        res => {
          this.rentalSubsidies = res.body as RefGroupDetails[];
        },
        error => console.error('Error!', error)
      );
  }

  refershdropdowns() {
    console.log(this.unitSelected);
    //alert(this.siteagreementPopulationId);

    // alert(this.unitSelected.siteAgreementPopulationSelected);
    // alert(this.unitSelected.siteAgreementPopulationSelected );
    // alert("refreshdropdown");
    // alert(this.unitSelected.siteAgreementPopulationID);
    // alert(this.unitSelected.siteAgreementPopulationSelected);
    if (this.unitSelected !== null && this.unitSelected.siteAgreementPopulationSelected !== null) {
      // alert(this.unitSelected.contractAgencySelected );
      if (this.unitSelected.siteAgreementPopulationSelected) {
        if (this.unitSelected.siteAgreementPopulationSelected !== 0) {
          if (this.unitSelected.contractAgencySelected === 488)
          {
            this.loadUnitTypes(this.unitSelected.siteAgreementPopulationSelected, "488");
            this.filterunitofflinereaseon(488);
          }
          else
          {
            this.loadUnitTypes(this.unitSelected.siteAgreementPopulationSelected, "0");
            this.filterunitofflinereaseon(0);
          }
          this.loadRentalsubsities(this.unitSelected.siteAgreementPopulationSelected);
          this.getagUnits(this.unitSelected.siteAgreementPopulationSelected);
          this.totalUnits = this.unitsDataObj.filter(d => (d.siteAgreementPopulationID === this.unitSelected.siteAgreementPopulationSelected)).length;
        }
      
        
      }
    }
    else {
      if (this.siteagreementPopulationId > 0) {

        if (this.unitSelected.contractAgencySelected === 488)
        {
          this.loadUnitTypes(this.siteagreementPopulationId, "488");
         this.filterunitofflinereaseon(488);
        }
        else
        {
          this.loadUnitTypes(this.siteagreementPopulationId, "0");
         this.filterunitofflinereaseon(0);
        }
        this.loadRentalsubsities(this.siteagreementPopulationId);
        this.getagUnits(this.siteagreementPopulationId );
        this.totalUnits = this.unitsDataObj.filter(d => (d.siteAgreementPopulationID === this.siteagreementPopulationId)).length;
      }
    }
    //alert(this.unitSelected.siteAgreementPopulationSelected);
  }





  ValidateUnitDetails() {
    //this.clearValidators();
    var messages: any;
    var key: any;
    this.resetformerror();
    //alert(this.unitDetailsFormGroup.controls.unitTypeCtrl.valid);



    if (this.unitSelected.siteAgreementPopulationSelected === 0) {
      key = "primarySvcContractCtrl";
      this.formErrors[key] += "please select Primary service contract" + '\n';
    }

    if ((!this.unitDetailsFormGroup.controls.primarySvcContractCtrl.valid) && this.siteagreementPopulationId === 0) {
      key = "primarySvcContractCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.unitDetailsFormGroup.controls.primarySvcContractCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.unitDetailsFormGroup.controls.contractAgencyCtrl.valid) {
      key = "contractAgencyCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.unitDetailsFormGroup.controls.contractAgencyCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }

    if (!this.unitDetailsFormGroup.controls.unitNameCtrl.valid) {
      key = "unitNameCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.unitDetailsFormGroup.controls.unitNameCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.unitDetailsFormGroup.controls.unitTypeCtrl.valid) {
      key = "unitTypeCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.unitDetailsFormGroup.controls.unitTypeCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.unitDetailsFormGroup.controls.unitStatusCtrl.valid) {
      key = "unitStatusCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.unitDetailsFormGroup.controls.unitStatusCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    //alert(this.unitSelected.unitStatusSelected)
    if (this.unitSelected.unitStatusSelected === 473) {
      if (!this.unitDetailsFormGroup.controls.unitOfflineReasonCtrl.valid) {
        key = "unitOfflineReasonCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.unitDetailsFormGroup.controls.unitOfflineReasonCtrl.errors) {
          if (errorKey) {
            //   alert(this.unitSelected.unitStatusSelected);
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      if (!this.unitDetailsFormGroup.controls.unitExpectedAvailableDateCtrl.valid) {
        key = "unitExpectedAvailableDateCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.unitDetailsFormGroup.controls.unitExpectedAvailableDateCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      else {
        var exerror;
        exerror = this.ValidateExpectedAvailabledate();
        if (exerror !== '') {
          key = "unitExpectedAvailableDateCtrl";
          this.formErrors[key] += exerror + '\n';
        }
      }
    }

  }

  resetformerror() {
    this.formErrors = {
      primarySvcContractCtrl: '',
      contractAgencyCtrl: '',
      unitNameCtrl: '',
      unitTypeCtrl: '',
      unitStatusCtrl: '',
      unitExpectedAvailableDateCtrl: '',
      unitOfflineReasonCtrl: ''
    };
  }

  ValidateAndSaveUnit() {
    // alert(this.siteAgrrementPopulationData.length);
    console.log(this.siteAgrrementPopulationData);
    if (this.siteAgrrementPopulationData) {
      this.ValidateAndSaveUnitCheck();
    }
    else {
      this.sharedUnitDetailsService.getSiteAgreementPopulation(this.siteID.toString())
        .subscribe(
          res => {
            if (res.body) {
              this.siteAgrrementPopulationData = res.body as SiteAgreementPopulation[];
              this.ValidateAndSaveUnitCheck();
            }
          },
          error => console.error('Error in retrieving the primary service contract Data By  ID...!', error)
        );


    }

  }

  ValidateAndSaveUnitCheck() {
    //alert(this.unitSelected.unitID )

    if (!this.IsEditMode()) {
      if (!this.IsUnitCountValidation()) {
        this.toastr.error(errormessage, "Allready added all unit for this service contract");
        return false;
      }
    }
    else {
      if (this.unitSelected.siteAgreementPopulationID != this.unitSelected.siteAgreementPopulationSelected) {
        if (!this.IsUnitCountValidation()) {
          this.toastr.error(errormessage, "Allready added all unit for this service contract");
          return false;
        }
      }
    }

    this.ValidateUnitDetails();
    var errormessage = '';

    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '') {
      this.toastr.error(errormessage, "Validation Error");
      return false;

    }
    else {
      this.saveUnit();
    }
  }


  getPrimartSerivceContractList = () => {

    this.sharedUnitDetailsService.getSiteAgreementPopulation(this.siteID.toString())
      .subscribe(
        res => {
          if (res.body) {
            this.siteAgrrementPopulationData = res.body as SiteAgreementPopulation[];
          }
        },
        error => console.error('Error in retrieving the primary service contract Data By  ID...!', error)
      );
    this.commonServiceSub2 = this.commonService.getPrimaryServiceAgreementPopulation(this.siteID).subscribe(
      res => {
        this.primarySvcContractTypes = res as PrimaryServiceContractAgreementPopulation[];

        if (this.siteagreementPopulationId > 0) {
          this.unitSelected.siteAgreementPopulationID = this.siteagreementPopulationId;
          this.unitDetailsFormGroup.controls.primarySvcContractCtrl.setValue(this.unitSelected.siteAgreementPopulationID);
          //   if (this.isTAD === true || this.isUnitRoaster=== true) {
          this.unitDetailsFormGroup.controls.primarySvcContractCtrl.disable();
          this.getagUnits(this.siteagreementPopulationId);
          //  }

        }
        else {
          this.unitSelected.siteAgreementPopulationID = 0;
          this.unitDetailsFormGroup.controls.primarySvcContractCtrl.setValue(0);

          this.unitDetailsFormGroup.controls.primarySvcContractCtrl.enable();
          this.getagUnits(0);
        }
        // alert(this.unitSelected.siteAgreementPopulationID);
        /* if ((this.siteagreementPopulationId != null ) ||  (this.siteagreementPopulationId != 0))
         {
         
         }*/
      },
      error => console.error('Error!', error)
    );
  }





  IsUnitCountValidation = (): boolean => {
    let newUnitList: Unit[] = [];
    let isunitvalidation: boolean = false;

    // alert(this.siteagreementPopulationId);
    //alert(this.unitSelected.siteAgreementPopulationSelected);
    if (this.siteagreementPopulationId != 0) {

      newUnitList = this.unitsDataObj.filter(d => (d.siteAgreementPopulationID === this.siteagreementPopulationId))
      var itm = this.siteAgrrementPopulationData.find(x => x.siteAgreementPopulationID === this.siteagreementPopulationId) as SiteAgreementPopulation

      if (itm !== null) {
        console.log(itm);
        // alert(itm.totalUnits);
        //alert(newUnitList.length);
        if (itm.totalUnits > newUnitList.length) {
          isunitvalidation = true;
        }
      }
    }
    else {
      if (this.unitSelected.siteAgreementPopulationSelected !== 0) {
        newUnitList = this.unitsDataObj.filter(d => (d.siteAgreementPopulationID === this.unitSelected.siteAgreementPopulationSelected))
        var itm = this.siteAgrrementPopulationData.find(x => x.siteAgreementPopulationID === this.unitSelected.siteAgreementPopulationSelected) as SiteAgreementPopulation

        if (itm !== null) {
          console.log(itm);
          //alert(itm.totalUnits);
          //alert(newUnitList.length);
          if (itm.totalUnits > newUnitList.length) {
            isunitvalidation = true;
          }
        }
      }
    }

    return isunitvalidation;
  }

  IsEditMode = (): boolean => {
    if (this.unitSelected && this.unitSelected.unitID > 0) {
      return true;
    }
    else
      return false;
  }

  ValidateExpectedAvailabledate = (): string => {
    var ermessage = '';
    var itm = this.unitOfflineReasons.find(x => x.unitOfflineReasonID === this.unitSelected.offlineReasonType) as UnitOfflineReason;
    if (itm != null) {
      let timeframe: number;
      timeframe = itm.timeFrame;
      var expecteddate = new Date();
      //alert(timeframe);
      if (timeframe > 0) {
        /* alert(timeframe);
        alert(expecteddate);*/
        expecteddate.setDate(expecteddate.getDate() + timeframe);
        /* alert(expecteddate);
         alert(this.verifyVacanyUnitdata.expectedAvailableDate);*/

        if (expecteddate < this.unitSelected.expectedAvailableDate) {
          ermessage = 'Expected available days should be within: ' + timeframe.toString() + "days";
        }
        /*alert(ermessage);*/
      }
    }
    return ermessage;
  }

  loadUnitOfflineReason() {
    this.sharedUnitDetailsService.getUnitOfflineReason()
      .subscribe(
        res => {
          const data = res.body as UnitOfflineReason[];
          this.unitOfflineReasons = data.filter(d => { return (d.unitOfflineReasonID >= 1) });
          this.unitOfflineReasonsWithoutFilter = data.filter(d => { return (d.unitOfflineReasonID >= 1) });
        },
        error => console.error('Error!', error)
      );

  }
  filterunitofflinereaseon(ctype:number)
  {
    //alert(ctype);
    if (ctype === 488)
    {
      this.unitOfflineReasons = this.unitOfflineReasonsWithoutFilter.filter(d => {return (d.unitOfflineReasonID >= 1 && d.unitOfflineReasonID <= 6 )}) ;
      //this.unitOfflineReasons =  this.unitOfflineReasons.filter(d => { return (d.timeFrame > 0) });
      //this.unitOfflineReasons = data;
    //  console.log(data);
      //console.log( this.unitOfflineReasons );
    }
    else
    this.unitOfflineReasons = this.unitOfflineReasonsWithoutFilter.filter(d => { return (d.unitOfflineReasonID >= 1) });
  
  }
  loadTADVaildationRules() {
    if (this.isTAD === true) {
      this.sharedUnitDetailsService.getVCSTADValidator(1)
        .subscribe(
          res => {
            const data = res.body as VCSTADRule[];

            if (data.filter(d => { return (d.ruleID === 2) }).length > 0)
              this.isUnitNameRule = true;
            else
              this.isUnitNameRule = false;

            // alert(d.length);
            if (data.filter(d => { return (d.ruleID === 3) }).length > 0)
              this.isUnitFeatresRule = true;
            else
              this.isUnitFeatresRule = false;

            if (data.filter(d => { return (d.ruleID === 4) }).length > 0)
              this.isUnitRentalSubsidyRule = true;
            else
              this.isUnitRentalSubsidyRule = false;

            if (data.filter(d => { return (d.ruleID === 5) }).length > 0)
              this.isUnitTypeRule = true;
            else
              this.isUnitTypeRule = false;
            //  alert(this.isUnitTypeRule) ;
            //alert(this.isUnitRentalSubsidyRule);
            //alert(this.isUnitNameRule);
            //alert(this.isUnitFeatresRule);
          },
          error => console.error('Error!', error)
        );
    }
  }
  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'UnitReport-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

    console.log('UnitReport-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }
  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  openCalender() {
    this.agencySiteInfo = {
      agencyID: this.siteDetail.agencyID,
      agencyNo: this.siteDetail.agencyNo,
      agencyName: this.siteDetail.agencyName,
      siteID: this.siteDetail.siteID,
      siteNo: this.siteDetail.siteNo,
      siteName: this.siteDetail.name,
    }

    this.calenderdata = {
      calendarFor: "['HPSite']",
      hpSiteID: this.siteID,
      preferredSlotEdit: true,
      agencySiteInfo: this.agencySiteInfo
    };
    this.dialog.open(SharedCalenderDialogComponent, {
      width: '1200px',
      maxHeight: '700px',
      disableClose: true,
      autoFocus: false,
      data: this.calenderdata
    });
  }

  getagUnits(id: number) {

    if (id !== null && id !==0) {
    var itm = this.siteAgrrementPopulationData.find(x => x.siteAgreementPopulationID === id) as SiteAgreementPopulation
    if (itm) {
      if (itm !== null) {
       this.agunits= itm.totalUnits;
      }
    }
  }
  else
  {
    var total = 0;

    if (this.siteAgrrementPopulationData != null && this.siteAgrrementPopulationData.length > 0) {      
      this.siteAgrrementPopulationData.forEach(x => total += x.totalUnits);
    }
    //alert(this.siteAgrrementPopulationData.length);
    //alert(total);
    this.agunits= total;
  }
}

}