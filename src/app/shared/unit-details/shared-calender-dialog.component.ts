import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SharedCalenderDialogData } from './shared-unit-details-model';
@Component({
  selector: 'app-shared-calender-dialog',
  templateUrl: './shared-calender-dialog.component.html',
  styleUrls: ['./shared-calender-dialog.component.scss'],
})
export class SharedCalenderDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SharedCalenderDialogData,

    private dialogRef: MatDialogRef<SharedCalenderDialogComponent>
  ) { }

  //Close the dialog on close button
  CloseDialog() {
    this.dialogRef.close(true);
  }

  //On Init
  ngOnInit() { }
}
