

import { IVCSAgencySiteInfoForCalendar } from 'src/app/shared/calendar/pact-calendar-interface.model';
export interface siteDemographic {
    siteID: number;
    siteNo: string;
    agencyID: number;
    housingProgram: string;
    name: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    locationType: number;
    referralAvailableType: number;
    tcoReadyType: number;
    tcoContractStartDate: string;
    taxCreditType: number;
    maxIncomeForStudio: number;
    maxIncomeForOneBR: number;
    levelOfCareType: number;
    contractType: number;
    contractStartDate: string;
    siteTrackedType: number;
    tadLiasion: number;
    sameAsSiteInterviewAddressType: number;
    interviewAddress: string;
    interviewCity: string;
    interviewState: string;
    interviewZip: string;
    interviewPhone: string;
    siteFeatures: string;
    siteTypeDesc: string;
    agencyName: string;
    agencyNo: string;
    userId: number;
    siteCategoryType?: number;
    boroughType?: number;
    isActive?: boolean;
    isSiteDemographicTabComplete?: boolean;
    isSiteContactTabComplete?: boolean;
    isPrimaryServiceContractTabComplete?: boolean;
    isSiteApprovalTabComplete?: boolean;
    isUnitDetailTabComplete?: boolean;
    isDraft?: boolean;
    isCapsMandate?: boolean;
}


export interface UserOptions {
    RA?: boolean;
    isPE?: boolean;
    isHP?: boolean;
    isIH?: boolean;
    isDisableForHP: boolean;
    isDisableForInactiveSite: boolean;
    isdisableActiveRadiobutton: boolean;
    isActive?: boolean;
    optionUserId: number;
}

export interface SiteAgreementPopulation {
    siteID: number;
    primaryServiceContractType: string;
    rentalSubsidies: string;
    totalUnits: number;
    siteAgreementPopulationID?: number;
    agreementPopulationID?: number;
    isActive?: boolean;
    userId?: number;
}


export interface SiteAgreementProfile {
    siteID: number;
    primaryServiceContractType: string;
    rentalSubsidies: string;
    totalUnits: number;
}




export class Unit {
    unitID: number;
    name: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    isSideDoorAllowed: boolean;
    isVacancyMonitorRequired: boolean;
    isVerified?: boolean;
    siteAgreementPopulationSelected: number;
    siteAgreementPopulationID: number;
    primaryServiceAgreementPopName: string;
    unitTypeID: number;
    unitTypeDesc: string;
    unitTypeSelected: number
    unitStatusID: number;
    unitStatusDesc: string;
    unitStatusSelected: number;
    contractAgencyID: number;
    contractAgencyDesc: string;
    contractAgencySelected: number;
    unitFeatures: UnitFeature[];
    rentalSubsidies: RentalSubsidy[];
    unitFeaturesJson: string;
    unitFeaturesString: string;
    rentalSubsidyJson: string;
    rentalSubsidyString: string;
    offlineReasonType?: number;
    offlineOtherSpecify?: string;
    expectedAvailableDate?: Date;
    AddlComment?: string;
    isActive: boolean;
    userName: string;
    isVacancyMonitorRequiredString: string;
    isSideDoorAllowedString: string;
}

export interface RentalSubsidy {

    sub_Id: number;
    sub_desc: string;
}

export interface UnitFeature {

    f_Id: number;
    f_desc: string;

}

export interface PrimaryServiceContractAgreementPopulation {
    agreementPopulationID: number;
    primaryServiceAgreementPopName: string;
    siteAgreementPopulationID: string;
}





export interface UnitHistory {
    unitHistoryID: number;
    unitID: number;
    statusID?: number;
    statusDescription?: string;
    startDate: string;
    endDate: string;
    comments?: string;
    verifiedByUserId?: number;
    verifiedByUserName: string;
    verifiedByDate: string;

}


export interface StatusBarHeader {
    AgencyID: number;
    AgencyName: string;
    AgencyAddress: string;
    SiteID: number;
    SiteName: string;
    SiteAddress: string;
    SiteAgreementPopulationID: number;
    AgreementpopulationName: string;
    UnitID: number;
    UnitName: string;
}



export interface UnitOfflineReason {
    unitOfflineReasonID: number;
    unitOfflineResonDescription: string;
    timeFrame?: number;

}

export interface VCSTADRule {
    ruleID: number;
    rosterNo?: number;
    rosterName: string;
    ruleName: string;
    isRuleHard?: boolean;

}




export interface SharedCalenderDialogData {
    calendarFor: string;
    hpSiteID: number;
    preferredSlotEdit: boolean;
    agencySiteInfo: IVCSAgencySiteInfoForCalendar;
}