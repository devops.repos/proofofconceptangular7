import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';

import { UnitHistory, StatusBarHeader } from "./shared-unit-details-model"
import { SharedUnitDetailsService } from './shared-unit-details.service';


@Component({
  selector: 'app-shared-unit-history',
  templateUrl: './shared-unit-history.component.html',
  styleUrls: ['./shared-unit-history.component.scss']
})
export class SharedUnitHistoryComponent implements OnInit, OnDestroy {


  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  pagination;
  rowSelection;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;
  public gridOptions: GridOptions;
  UnitHistoryData: UnitHistory[] = [];
  overlayLoadingTemplate: string = '';
  overlayNoRowsTemplate: string = '';

  @Input() unitHistroyData: StatusBarHeader;
  constructor(
    private sharedUnitDetailsService: SharedUnitDetailsService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;
    //this.gridOptions.api.hideOverlay();

    this.columnDefs = [

      {
        headerName: 'Unit Status',
        field: 'statusDescription',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Effective Start Date',
        field: 'startDate',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Effective End Date',
        field: 'endDate',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Comments',
        field: 'comments',
        width: 300,
        filter: 'agTextColumnFilter'
      }
      ,
      {
        headerName: 'Verified By/Date',
        valueGetter(params) {
          var vuser;
          if (params.data.verifiedByUserName != null)
               vuser = params.data.verifiedByUserName + '/ ' + params.data.verifiedByDate;
            else
             vuser ='';
          return vuser;
        },

        width: 200,
        filter: 'agTextColumnFilter'
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: false
    };
    this.rowSelection = 'single';

    this.pagination = true;
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the primary service contracts are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No  primary service contarcts are available</span>';
  }

  ngOnInit() {

    if (this.unitHistroyData.UnitID != null) {
      this.getUnitHistory();
    }

  }

  getUnitHistory = () => {
    this.sharedUnitDetailsService.getUnitHistory(this.unitHistroyData.UnitID.toString())
      .subscribe(
        res => {
          if (res.body) {
            this.UnitHistoryData = res.body as UnitHistory[];
          }
        },
        error => console.error('Error in retrieving the Unit History Unit ID...!', error)
      );

  }



  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function (column) {
      params.api.setDomLayout('autoHeight');


      //  params.api.sizeColumnsToFit();
      //params.api.setDomLayout('autoHeight');
      //  this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the documents are loading.</span>';
      //this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Documents Available</span>';

    });
  }




  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onRowSelected(event) {
    if (event.node.selected) {
      //alert('row ' + event.node.data.siteID + ' selected = ' + event.node.selected);
      // alert(event.node);        
    }
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'UnitHistory-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

  }
  ngOnDestroy = () => {

  }


}



