import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HousingApplicationSupportingDocumentsComponent } from './housing-application-supporting-documents.component';

describe('HousingApplicationSupportingDocumentsComponent', () => {
  let component: HousingApplicationSupportingDocumentsComponent;
  let fixture: ComponentFixture<HousingApplicationSupportingDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HousingApplicationSupportingDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousingApplicationSupportingDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
