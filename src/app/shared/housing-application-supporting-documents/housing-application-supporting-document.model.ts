export interface iHousingApplicationSupportingDocumentsData {
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    approvalExpiryDate: string;
    pactApplicationId: number;
    expandSection: number;
};