import { Component, OnInit, Input, OnDestroy, ViewChild, Output, EventEmitter, } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http'
import { Observable, Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { GridOptions } from 'ag-grid-community';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { DocumentService } from 'src/app/shared/document/document.service';
import { iHousingApplicationSupportingDocumentsData } from './housing-application-supporting-document.model';
import { DocumentComponent } from 'src/app/shared/document/document.component';

export interface iClientDocumentsData {
  firstName: string;
  lastName: string;
  dob: string;
  cin: string;
  ssn: string;
  pactClientId: number;
  pactClientSearchAuditId: number;
  optionUserId: number;
}

export interface iApplicationPackage {
  pactDocumentID: number;
  documentType: number;
  capsReportID: string;
  documentTypeDescription: string;
  documentName: string;
  documentExtension: string;
  documentDescription: string;
  fileNetDocID: number;
  createdBy: number;
  createdByName: string;
  createdDateTime: string;
}

export interface iDocumentLink {
  linkURL: string;
}

@Component({
  selector: 'app-housing-application-supporting-documents',
  templateUrl: './housing-application-supporting-documents.component.html',
  styleUrls: ['./housing-application-supporting-documents.component.scss']
})

export class HousingApplicationSupportingDocumentsComponent implements OnInit, OnDestroy {
  @Input() housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null,
    expandSection: null
  };
  @Input() canShowPostApprovalUpload: boolean = true;
  @Input() deleteDisabled: boolean = false;
  @Output() onDocValueChange = new EventEmitter();
  @ViewChild(DocumentComponent) docChild: DocumentComponent;

  //Global Variables
  columnDefs = [];
  applicationPackageRowData = [];
  determinationDocumentsRowData = [];
  defaultColDef = {};
  gridOptions: GridOptions;
  overlayNoRowsTemplate: string;
  applicationPackage: iApplicationPackage[];
  determinationDocuments: iApplicationPackage[];
  reportParams: PACTReportUrlParams;
  reportParameters: PACTReportParameters[] = [];
  userData: AuthData;
  userDataSub: Subscription;
  determinationLetterGuid: string;
  applicationSummaryGuid: string;
  mentalHealthReportGuid: string;
  psychiarticEvalReportGuid: string;
  psychosocialEvalReportGuid: string;
  vulnerabilityAssessmentReportGuid: string;
  determinationSummaryReportGuid: string;

  housingHomelessGuid: string;
  clientDocumentsData: iClientDocumentsData = { firstName: null, lastName: null, dob: null, cin: null, ssn: null, pactClientId: 0, pactClientSearchAuditId: 0, optionUserId: null };

  //Constructor
  constructor(
    private httpClient: HttpClient,
    private commonService: CommonService,
    private message: ToastrService,
    private userService: UserService,
    private documentService: DocumentService) {
    //Grid Column Definitions
    this.columnDefs = [
      { headerName: 'Document Type', field: 'documentTypeDescription', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Description', field: 'documentDescription', filter: 'agTextColumnFilter', width: 100 },
      {
        headerName: 'Document Name',
        field: 'documentName',
        cellRenderer: (params: { value: string; data: { pactDocumentID: number; documentType: number; documentExtension: string; fileNetDocID: number; capsReportID: string }; }) => {
          var link = document.createElement('a');
          link.href = '#';
          link.innerText = params.value;
          link.addEventListener('click', (e) => {
            e.preventDefault();
            this.commonService.setIsOverlay(true);
            this.openDocument(params.data.pactDocumentID, params.data.documentType, params.data.documentExtension, params.data.fileNetDocID, params.data.capsReportID);
          });
          return link;
        },
        filter: 'agTextColumnFilter',
        width: 100
      },
      { headerName: 'Attached Date-Time', field: 'createdDateTime', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Attached By', field: 'createdByName', filter: 'agTextColumnFilter', width: 100 }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  //On Init
  ngOnInit() {
    //Get User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => this.userData = res);

    //Get Application Package
    if (this.housingApplicationSupportingDocumentsData && this.housingApplicationSupportingDocumentsData.pactApplicationId) {
      this.getApplicationPackage()
        .subscribe(res => {
          if (res) {
            const data = res as iApplicationPackage[];
            if (data) {
              this.applicationPackage = data.filter(d => { return d.documentType !== 326 && d.documentType !== 332 && d.documentType !== 334 && d.documentType !== 874 && d.documentType !== 943 });
              if (this.applicationPackage) {
                this.applicationPackageRowData = this.applicationPackage;
              }
              this.determinationDocuments = data.filter(d => { return d.documentType === 326 || d.documentType === 332 || d.documentType === 334 || d.documentType === 874 || d.documentType === 943 });
              if (this.determinationDocuments) {
                this.determinationDocumentsRowData = this.determinationDocuments;
              }
            }
          }
        });
    }

    //Populate Clent Documents Data
    if (this.housingApplicationSupportingDocumentsData) {
      this.clientDocumentsData.firstName = this.housingApplicationSupportingDocumentsData.firstName;
      this.clientDocumentsData.lastName = this.housingApplicationSupportingDocumentsData.lastName;
      this.clientDocumentsData.dob = this.housingApplicationSupportingDocumentsData.dob;
      this.clientDocumentsData.cin = this.housingApplicationSupportingDocumentsData.cin === null ? "" : this.housingApplicationSupportingDocumentsData.cin;
      this.clientDocumentsData.ssn = this.housingApplicationSupportingDocumentsData.ssn;
      this.clientDocumentsData.pactClientId = this.housingApplicationSupportingDocumentsData.pactClientId === null ? 0 : this.housingApplicationSupportingDocumentsData.pactClientId;
      this.clientDocumentsData.optionUserId = this.userData.optionUserId;
    }
  }

  //Destroy 
  ngOnDestroy() {
    this.userDataSub.unsubscribe();
  }

  applicationPackageURL = environment.pactApiUrl + 'Document/GetApplicationPackage';
  documentLinkURL = environment.pactApiUrl + 'Document/GetDocumentLink';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  //Get Application Package - Api Call 
  getApplicationPackage(): Observable<any> {
    return this.httpClient.post(this.applicationPackageURL, JSON.stringify(this.housingApplicationSupportingDocumentsData.pactApplicationId), this.httpOptions);
  }

  //Get Document Link - Api call
  getDocumentLink(fileNetDocID: string): Observable<any> {
    if (fileNetDocID) {
      return this.httpClient.post(this.documentLinkURL, JSON.stringify(fileNetDocID), this.httpOptions);
    }
  }

  //On Application Package Grid Ready
  onApplicationPackageGridReady(params: { api: { setDomLayout: (arg0: string) => void; sizeColumnsToFit: () => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.overlayNoRowsTemplate = '<span style="color: #337ab7">No Documents To Show</span>';
    if (this.applicationPackage) {
      this.applicationPackageRowData = this.applicationPackage;
    }
    params.api.sizeColumnsToFit();
  }

  //On Determination Documents Grid Ready
  onDeterminationDocumentsGridReady(params: { api: { setDomLayout: (arg0: string) => void; sizeColumnsToFit: () => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.overlayNoRowsTemplate = '<span style="color: #337ab7">No Documents To Show</span>';
    if (this.determinationDocuments) {
      this.determinationDocumentsRowData = this.determinationDocuments;
    }
    params.api.sizeColumnsToFit();
  }

  //Open Document
  openDocument(pactDocumentId: number, documentType: number, documentExtension: string, fileNetDocID: number, capsReportID: string) {
    //File Net
    if (fileNetDocID) {
      this.getDocumentLink(fileNetDocID.toString()).subscribe(res => {
        const data = res as iDocumentLink;
        if (data) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    }
    else {
      //SSRS
      if (documentType === 331) {
        if (capsReportID) {
          this.commonService.displaySurveySummaryReport(capsReportID, this.userData.optionUserId);
        }
      }
      else if (documentType === 326 || documentType === 327 || documentType === 328 || documentType === 329 || documentType === 330 || documentType === 332 || documentType === 334 || documentType === 874) {
        this.viewSSRSDocument(documentType);
      }
      else {
        //Storage MTC
        if (pactDocumentId && documentExtension) {
          var docName = pactDocumentId.toString() + "." + documentExtension;
          this.documentService.viewDocument(docName).subscribe(
            data => {
              switch (data.type) {
                case HttpEventType.Response:
                  const downloadedFile = new Blob([data.body], {
                    type: data.body.type
                  });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(downloadedFile, docName);
                  } else {
                    this.commonService.OpenWindow(URL.createObjectURL(downloadedFile));
                    this.commonService.setIsOverlay(false);
                  }
                  break;
              }
            }, error => {
              this.message.error('There was an error in downloading the document.', 'Document download Failed!');
            });
        }
      }
    }
  }

  //View SSRS Document
  viewSSRSDocument(documentType: number) {
    switch (documentType) {
      case 326: {
        if (!this.determinationLetterGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingApplicationSupportingDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "DeterminationLetter", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.determinationLetterGuid = res;
                  this.generateReport(res, "DeterminationLetter");
                }
              }
            );
        }
        else {
          this.generateReport(this.determinationLetterGuid, "DeterminationLetter");
        }
        break;
      }
      case 327: {
        if (!this.applicationSummaryGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingApplicationSupportingDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PACTReportApplicationSummary", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.applicationSummaryGuid = res;
                  this.generateReport(res, "PACTReportApplicationSummary");
                }
              }
            );
        }
        else {
          this.generateReport(this.applicationSummaryGuid, "PACTReportApplicationSummary");
        }
        break;
      }
      case 328: {
        if (!this.mentalHealthReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingApplicationSupportingDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "MentalHealthReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.mentalHealthReportGuid = res;
                  this.generateReport(res, "MentalHealthReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.mentalHealthReportGuid, "MentalHealthReport");
        }
        break;
      }
      case 329: {
        if (!this.psychiarticEvalReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingApplicationSupportingDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PsychiarticEvalReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.psychiarticEvalReportGuid = res;
                  this.generateReport(res, "PsychiarticEvalReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.psychiarticEvalReportGuid, "PsychiarticEvalReport");
        }
        break;
      }
      case 330: {
        if (!this.psychosocialEvalReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingApplicationSupportingDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PsychosocialEvalReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.psychosocialEvalReportGuid = res;
                  this.generateReport(res, "PsychosocialEvalReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.psychosocialEvalReportGuid, "PsychosocialEvalReport");
        }
        break;
      }
      case 332: {
        if (!this.vulnerabilityAssessmentReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingApplicationSupportingDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "SVAReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.vulnerabilityAssessmentReportGuid = res;
                  this.generateReport(res, "SVAReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.vulnerabilityAssessmentReportGuid, "SVAReport");
        }
        break;
      }
      case 334: {
        if (!this.housingHomelessGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingApplicationSupportingDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "HHHReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.housingHomelessGuid = res;
                  this.generateReport(res, "HHHReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.housingHomelessGuid, "HHHReport");
        }
        break;
      }
      case 874: {
        if (!this.determinationSummaryReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.housingApplicationSupportingDocumentsData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "DeterminationSummaryReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.determinationSummaryReportGuid = res;
                  this.generateReport(res, "DeterminationSummaryReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.determinationSummaryReportGuid, "DeterminationSummaryReport");
        }
        break;
      }
      default: {
        break;
      }
    }
  }

  //Generate Report
  generateReport(reportGuid: string, reportName: string) {
    this.reportParams = { reportParameterID: reportGuid, reportName: reportName, "reportFormat": "PDF"  };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.commonService.OpenWindow(URL.createObjectURL(data));
            this.commonService.setIsOverlay(false);
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }

  //Reset the inputs on Document shared component 
  resetDocForm() {
    this.docChild.resetValues();
  }
  //Value change in the Doc component
  onDocumentValueChange(event: string) {
    this.onDocValueChange.emit(event);
  }
}
