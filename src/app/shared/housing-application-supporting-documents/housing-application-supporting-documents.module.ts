import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { AgGridModule } from 'ag-grid-angular';
import { NgxMaskModule } from 'ngx-mask';
import { HousingApplicationSupportingDocumentsComponent } from './housing-application-supporting-documents.component';
import { DocumentModule } from '../document/document.module';
import { ClientDocumentsGridModule } from '../client-documents-grid/client-documents-grid.module'

@NgModule({
  declarations: [HousingApplicationSupportingDocumentsComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    AgGridModule.withComponents(),
    NgxMaskModule,
    DocumentModule,
    ClientDocumentsGridModule
  ],
  exports: [HousingApplicationSupportingDocumentsComponent]
})

export class HousingApplicationSupportingDocumentsModule { }