import { AbstractControl } from '@angular/forms';

export class CustomValidators {

  static fileNameExtn = () => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const controlValue: string = control.value;
      const acceptableFileExtensions = 'PDF DOC DOCX XLS XLSX PPT TXT TIFF TIF Media MSG HTML MHT XPS';
      // console.log(controlValue);
      let fileName = '';
      let fileExtension = '';
      if (controlValue.length > 0) {
        fileName = controlValue.split('\\').pop();
        fileExtension = fileName.split('.').pop();

        const pattern = new RegExp(/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/); // unacceptable chars
        //  const pattern = new RegExp(/^[a-zA-Z0-9- ]*$/); // unacceptable chars

        if (pattern.test(fileName)) {
          return { fileName: true };
        } else if (!acceptableFileExtensions.toLowerCase().includes(fileExtension.toLowerCase())) {
          return { fileExtn: true };
        } else {
          return null; // good user input
        }
      }
    };
  }

  static dropdownRequired = () => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const controlValue: any = control.value;
      if (controlValue == null || controlValue == undefined || String(controlValue).trim() == '' || Number(controlValue) < 1) {
        return { dropdownRequired: true };
      } else {
        return null;
      }
    };
  }

  static radioGroupRequired = () => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const controlValue: any = control.value;
      if(controlValue == undefined || controlValue == null || String(controlValue).trim() == '' || Number(controlValue) < 1){
        return { radioGroupRequired: true };//return null;
      }
      else {
        return null;
      }
    };
  }

  static datePattern = () => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const controlValue: number = control.value;
      // tslint:disable-next-line: max-line-length
      const usDatePattern = /^02\/(?:[01]\d|2\d)\/(?:19|20)(?:0[048]|[13579][26]|[2468][048])|(?:0[13578]|10|12)\/(?:[0-2]\d|3[01])\/(?:19|20)\d{2}|(?:0[469]|11)\/(?:[0-2]\d|30)\/(?:19|20)\d{2}|02\/(?:[0-1]\d|2[0-8])\/(?:19|20)\d{2}$/;
      if (!control.value.match(usDatePattern)) {
        return { datePatternRequired: true };
      } else {
        return null;
      }
    };
  }

  static requiredCharLength = (minimumLength: number) => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const controlValue: string = control.value != null ? control.value.trim() : '';
      if (controlValue.length < minimumLength) {
        return { charLength: true };
      } else {
        return null; // good user input
      }
    };
  }

  static blankspaceValidator = () => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const controlValue: string = control.value != null ? control.value.trim() : '';
      const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
    };
  }

  static minimunCharLength = (minimumLength: number) => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const controlValue: string = control.value != null ? control.value.trim() : '';
      if (controlValue.length >= minimumLength || controlValue.length == 0) {
        return null;
      } else {
        return { charLength: true }; // good user input
      }
    };
  }
}


