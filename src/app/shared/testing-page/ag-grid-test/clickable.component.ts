import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: "ag-clickable",
  template: `
    <mat-icon
      (click)="click()"
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="referralRosterAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #referralRosterAction="matMenu">
      <button mat-menu-item routerLink="/vcs/referral-roster/interview-outcome">
        Interview Outcome
      </button>
      <button mat-menu-item>Delete</button>
      <button mat-menu-item>Transmit</button>
      <button mat-menu-item>Summary Report</button>
    </mat-menu>
  `,
  styles: [
    `
    .pendingMenu-icon {
      cursor: pointer;
      
    }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class ClickableComponent {
  @Input() cell: any;
  @Output() onClicked = new EventEmitter<boolean>();

  click(): void {
    this.onClicked.emit(this.cell);
  }
}
