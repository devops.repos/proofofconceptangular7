
import { Component, OnInit, ViewChild } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { HttpClient } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { MasterDropdown } from 'src/app/models/masterDropdown.module';
import { ClickableParentComponent } from './clickable.parent.component';
import { Dropdown1Component } from './dropdown1.component';
import { CustomDateComponent } from 'src/app/pact-modules/vacancyControlSystem/custom-date-component.component';

export interface GData {
  athlete: string;
  age: number;
  country: string;
  year: number;
  date: string;
  sport: string;
  gold: number;
  silver: number;
  bronze: number;
  total: number;
}

@Component({
  selector: 'app-ag-grid-test',
  templateUrl: './ag-grid-test.component.html',
  styleUrls: ['./ag-grid-test.component.scss']
})
export class AgGridTestComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('agGrid1') agGrid1: AgGridAngular;

  sites: MasterDropdown[] = [
    // {value: '', viewValue: ' -- Choose one -- '},
    {value: '001', viewValue: '001 -- JBFCS-GENERIC CR - ABRAHAM RESIDENCE'},
    {value: '002', viewValue: '002 -- BROOKLYN COMMUNITY RESIDENCE'},
    {value: '003', viewValue: '003 -- ROCKAWAY APT. TREATMENT PROGRAM'}
  ];

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  rowSelection;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;


  public gridOptions: GridOptions;
  rowData: GData[];

  constructor(private http: HttpClient) {
    this.gridOptions = {
       rowHeight: 35,
       sideBar: {
        toolPanels: [
                {
                    id: 'columns',
                    labelDefault: 'Columns',
                    labelKey: 'columns',
                    iconKey: 'columns',
                    toolPanel: 'agColumnsToolPanel',
                    toolPanelParams: {
                        suppressValues: true,
                        suppressPivots: true,
                        suppressPivotMode: true,
                        suppressRowGroups: false
                    }
                },
                {
                    id: 'filters',
                    labelDefault: 'Filters',
                    labelKey: 'filters',
                    iconKey: 'filter',
                    toolPanel: 'agFiltersToolPanel',
                }
            ],
            defaultToolPanel: ''
        }
    } as GridOptions;

    this.columnDefs = [
      {
        headerName: 'Athlete Details',
        // field: 'athlete',
        // width: 180,
        // set the column to use text filter
        filter: 'agTextColumnFilter',
        // pass in additional parameters to the text filter
        filterParams: {
          clearButton: true,
          applyButton: true,
          // debounceMs: 200
        },
        checkboxSelection: true,
        valueGetter: function(params) {
          return params.data.athlete + ' - (gold: ' + params.data.gold + ', silver: ' + params.data.silver + ', bronze: ' + params.data.bronze + ')';
        }
      },
      {
        headerName: 'Athlete',
        field: 'athlete',
        // width: 180,
        // set the column to use text filter
        filter: 'agTextColumnFilter',
        // pass in additional parameters to the text filter
        filterParams: {
          clearButton: true,
          applyButton: true,
          // debounceMs: 200
        }
      },
      {
        headerName: 'Age',
        field: 'age',
        // width: 80,
        filter: 'agNumberColumnFilter',
        cellStyle: function(params) {
          if (params.value == 24) {
              //mark police cells as red
              return {color: 'red'};
          } else {
              return null;
          }
        }
      },
      {
        headerName: 'Country',
        field: 'country',
        // width: 110,
        headerTooltip: 'The country the athlete represented',
        // editable: true,
        cellRenderer: 'dropdownRenderer',
      },
      {
        headerName: 'Year',
        field: 'year',
        // width: 80,
        filter: 'agNumberColumnFilter',
        // filterParams: { filterOptions: ['inRange'] },
        headerTooltip: 'The year of the olympics'
      },
      {
        headerName: 'Date',
        field: 'date',
        // width: 140,
        filter: 'agDateColumnFilter',
        filterParams: {
          filterOptions: ['inRange'],
          comparator: function(filterLocalDateAtMidnight, cellValue) {
            var dateAsString = cellValue;
            if (dateAsString == null) return -1;
            var dateParts = dateAsString.split('/');
            var cellDate = new Date(
              Number(dateParts[2]),
              Number(dateParts[1]) - 1,
              Number(dateParts[0])
            );
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
        headerTooltip: 'The date of the olympics'
      },
      {
        headerName: 'Sport',
        field: 'sport',
        // width: 100,
        // filter: 'agTextColumnFilter',
        headerTooltip: 'The sport the medal was for',
        editable: true,
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: function(params) {
          var selectedCountry = params.data.country;
          if (selectedCountry === 'United States') {
              return {
                  values: ['United States sports', 'swimming', 'football', 'basketball']
              };
          } else {
              return {
                  values: ['other sports', 'X', 'Y', 'Z']
              };
          }
        }
      },
      {
        headerName: 'Gold',
        field: 'gold',
        // width: 80,
        filter: 'agNumberColumnFilter',
        headerTooltip: 'How many gold medals'
      },
      {
        headerName: 'Silver',
        field: 'silver',
        // width: 80,
        filter: 'agNumberColumnFilter',
        headerTooltip: 'How many silver medals'
      },
      {
        headerName: 'Bronze',
        field: 'bronze',
        // width: 80,
        filter: 'agNumberColumnFilter',
        headerTooltip: 'How many bronze medals'
      },
      {
        headerName: 'Total',
        field: 'total',
        // width: 80,
        filter: false,
        headerTooltip: 'The total number of medals'
      },
      {
          headerName: 'Action',
          field: 'athlete',
          width: 80,
          filter: false,
          pinned: 'right',
          cellRenderer: 'actionRenderer',

      }
    ];
    this.defaultColDef = {
      sortable: false,
      resizable: true,
      filter: true
    };
    this.rowSelection = 'multiple';
    this.frameworkComponents = {
      agDateInput: CustomDateComponent,
      actionRenderer: ClickableParentComponent,
      dropdownRenderer: Dropdown1Component
    };
  }

  ngOnInit() {}

  // autoSizeAll() {
  //   var allColumnIds = [];
  //   this.gridColumnApi.getAllColumns().forEach(function(column) {
  //     allColumnIds.push(column.colId);
  //   });
  //   this.gridColumnApi.autoSizeColumns(allColumnIds);
  // }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // this.gridApi.sizeColumnsToFit();
    /** API call to get the grid data */
    // this.http
    //   .get(
    //     'https://raw.githubusercontent.com/ag-grid/ag-grid/master/packages/ag-grid-docs/src/olympicWinnersSmall.json'
    //   )
    //   .subscribe(data => {
    //     this.rowData = data as GData[];
    //   });

    /** Getting data statically from the flat file */
    this.http.get('assets/data/ag-grid-data.json').subscribe(res =>{
      // console.log(res);
      this.rowData = res as GData[];
    });

  }

  onRowSelected(event) {
    console.log(
      'row ' + event.node.data.athlete + ' selected = ' + event.node.selected
    );
    if (this.agGrid.api.getSelectedNodes().length > 3) {
      console.log('more than 3 selected');
      this.agGrid.isRowSelectable = false;
    }
  }

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    const selectedDataStringPresentation = selectedData
      .map(
        node =>
          '(' +
          node.athlete +
          ', age: ' +
          node.age +
          ', country: ' +
          node.country  +
          ', year: ' +
          node.year +
          ', date: ' +
          node.date +
          ', sport: ' +
          node.sport +
          ', gold: ' +
          node.gold +
          ', silver: ' +
          node.silver +
          ', bronze: ' +
          node.bronze +
          ', total: ' +
          node.total +
          ')'
      )
      .join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

}
