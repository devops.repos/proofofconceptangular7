import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import { MasterDropdown } from 'src/app/models/masterDropdown.module';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: 'dropdown-cell',
    template: `
    <mat-form-field>
    <mat-label>Select</mat-label>
    <mat-select [(ngModel)]="selectedValue">
      <mat-option (click)="click()" *ngFor="let country of countries" [value]="country.viewValue">
        {{country.viewValue}}
      </mat-option>
    </mat-select>
  </mat-form-field>
    `
})
export class DropdownComponent implements OnInit {
    selectedValue: string;
    value;

    @Input() cell: any;
    @Output() onClicked = new EventEmitter<boolean>();

    countries: MasterDropdown[] = [
      {value: '0', viewValue: 'select'},
      {value: '1', viewValue: 'United States'},
      {value: '2', viewValue: 'Russia'},
      {value: '10', viewValue: 'Australia'},
      {value: '3', viewValue: 'Canada'},
      {value: '4', viewValue: 'India'},
      {value: '5', viewValue: 'China'},
      {value: '6', viewValue: 'Nepal'},
      {value: '7', viewValue: 'Pakistan'},
      {value: '8', viewValue: 'Zimbabwe'},
      {value: '9', viewValue: 'Netherlands'},
      {value: '10', viewValue: 'South Korea'},
      {value: '11', viewValue: 'Japan'},
      {value: '12', viewValue: 'Norway'},
    ];
    ngOnInit(){
      this.selectedValue = this.cell.row.country;
    }



  click(): void {
    // alert('selectedvalue: ' + this.selectedValue);
    // this.value = this.cell;
    // alert('this.value: ' + this.value);
    // console.log('dropdown.this.value: ', this.value);
    // console.log('dropdown.this.value.row: ', this.value.row.country);
    this.cell.row.country = this.selectedValue;
    this.onClicked.emit(this.cell);
  }
}
