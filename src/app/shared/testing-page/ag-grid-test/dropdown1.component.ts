import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: 'clickable-cell',
    template: `
        <dropdown-cell (onClicked)="clicked($event)" [cell]="cell"></dropdown-cell>
    `
})
export class Dropdown1Component implements ICellRendererAngularComp {
    private params: any;
    public cell: any;

    agInit(params: any): void {
        this.params = params;
        // this.cell = {row: params.value, col: params.colDef.headerName};
        this.cell = {row: params.node.data, col: params.colDef.headerName};
        // console.log('dropdown1.cell value: ', this.cell);
    }

    public clicked(cell: any): void {
        // console.log('Child Cell Clicked: ' + JSON.stringify(cell));
        alert('Child Cell Clicked: ' + JSON.stringify(cell));
    }

    refresh(): boolean {
        return true;
    }
}
