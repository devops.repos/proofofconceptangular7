import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/services/helper-services/theme.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

export interface IDashboardItm {
  name: string;
  url: string;
  icon: string;
  functionID: number;
  number?: number;
  isOverdue?: boolean;
}

@Component({
  selector: 'app-testing-page',
  templateUrl: './testing-page.component.html',
  styleUrls: ['./testing-page.component.scss']
})
export class TestingPageComponent implements OnInit {

  dashboardItems: IDashboardItm[] = [
    {
      name: "New Survey",
      url: "/testing-page",
      icon: "assessment",
      functionID: 1
    },
    {
      name: "Vacancy Listing",
      url: "/vcs/vacancy-listing",
      icon: "list_alt",
      functionID: 2
    },
    {
      name: "Referral Roster",
      url: "/vcs/referral-roster",
      icon: "description",
      functionID: 3
    },
    {
      name: "Tenant Roster",
      url: "/vcs/tenant-roster",
      icon: "people",
      functionID: 4
    },
    {
      name: "Unit Roster",
      url: "/vcs/unit-roster",
      icon: "home",
      functionID: 5
    },
    {
      name: "Schedular",
      url: "/vcs/schedular",
      icon: "date_range",
      functionID: 6
    },
    {
      name: "Tad",
      url: "/vcs/tad",
      icon: "library_books",
      functionID: 7
    },
    {
      name: "Schedular",
      url: "/vcs/schedular",
      icon: "date_range",
      functionID: 6
    },
    {
      name: "Client Awaiting Placement List",
      url: "/vcs/client-awaiting-placement",
      icon: "date_range",
      functionID: 6
    },
    {
      name: "Placement Awaiting Verification",
      url: "/vcs/placements-awaiting-verification",
      icon: "date_range",
      functionID: 6
    },
    {
      name: "TADs Awaiting Verification",
      url: "/vcs/tad-awaiting-verification",
      icon: "date_range",
      functionID: 6
    }
  ]

  date1 = '12/25/2020';
  date2 = '12-25-2020';
  date3 = '2020/12/25';
  date4 = '2020-12-25';
  date5 = '2020/25/12';
  date6 = '2020-25-12';

  d1: Date; d2: Date;d3: Date;d4: Date;d5: Date;d6: Date;
  yyyy1; mm1; dd1;
  yyyy2; mm2; dd2;
  yyyy3; mm3; dd3;
  yyyy4; mm4; dd4;
  yyyy5; mm5; dd5;
  yyyy6; mm6; dd6;

  constructor() { }

  ngOnInit() {



    this.d1 = new Date(this.date1);
    this.d2 = new Date(this.date2);
    this.d3 = new Date(this.date3);
    this.d4 = new Date(this.date4);
    this.d5 = new Date(this.date5);
    this.d6 = new Date(this.date6);

    // this.yyyy1 = this.date1.substring(6, 10);
    // this.mm1 = this.date1.substring(3, 5);
    // this.dd1 = this.date1.substring(0, 2);


    // this.yyyy2 = this.date2.substring(6, 10);
    // this.mm2 = this.date2.substring(3, 5);
    // this.dd2 = this.date2.substring(0, 2);
    this.yyyy1 = this.d1.getFullYear();
    this.mm1 = this.d1.getMonth() + 1;
    this.dd1 = this.d1.getDate();
    this.yyyy2 = this.d2.getFullYear();
    this.mm2 = this.d2.getMonth() + 1;
    this.dd2 = this.d2.getDate();
    this.yyyy3 = this.d3.getFullYear();
    this.mm3 = this.d3.getMonth() + 1;
    this.dd3 = this.d3.getDate();
    this.yyyy4 = this.d4.getFullYear();
    this.mm4 = this.d4.getMonth() + 1;
    this.dd4 = this.d4.getDate();
    this.yyyy5 = this.d5.getFullYear();
    this.mm5 = this.d5.getMonth() + 1;
    this.dd5 = this.d5.getDate();
    this.yyyy6 = this.d6.getFullYear();
    this.mm6 = this.d6.getMonth() + 1;
    this.dd6 = this.d6.getDate();
  }
}
