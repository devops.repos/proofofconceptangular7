import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

export interface ITopping {
  Id: number;
  Name: string;
}

@Component({
  selector: 'app-testing-page1',
  templateUrl: './testing-page1.component.html',
  styleUrls: ['./testing-page1.component.scss']
})
export class TestingPage1Component implements OnInit {
  multiSelectGroup: FormGroup;

  valueSelected: string;
  toppingList: ITopping[] = [
    {Id: 1, Name: 'Extra cheese'},
    {Id: 2, Name: 'Mushroom'},
    {Id: 3, Name: 'Onion'},
    {Id: 4, Name: 'Pepperoni'},
    {Id: 5, Name: 'Sausage'},
    {Id: 6, Name: 'Tomato'},
  ]

  selectedElement = [-1];

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.multiSelectGroup = this.formBuilder.group({
      toppingCtrl: [this.selectedElement, Validators.required]
    });
   }



   onMultiIncomeSourceSelected(event) {
    if(!event) {
      console.log('dropdown is closed');
      // this.valueSelected = this.toppings.value && this.toppings.value.toString();
      this.valueSelected = this.multiSelectGroup.get('toppingCtrl').value;
    }
  }

  ngOnInit() {
    // this.multiSelectGroup.get('toppingCtrl').setValue(this.selectedElement);
  }
}


