import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
// import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { TestCalendarComponent } from './test-calendar.component';
import { DateAdapter, CalendarModule } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';


@NgModule({
  declarations: [
    TestCalendarComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }), 
  ],
  exports: [
    TestCalendarComponent,
  ]
})
export class TestPactCalendarModule { }
