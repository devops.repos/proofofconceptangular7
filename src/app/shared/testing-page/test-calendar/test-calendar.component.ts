import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
// import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';

const colors: any = {
  red: {
    primary: '#B71C1C',
    secondary: '#FFCDD2'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  green: {
    primary: '#22A213',
    secondary: '#B7F1BE'
  }
};

@Component({
  selector: 'app-test-calendar',
  templateUrl: './test-calendar.component.html',
  styleUrls: ['./test-calendar.component.scss']
})
export class TestCalendarComponent implements OnInit {

  // @ViewChild("modalContent") modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<span class="calendar-event-edit-icon"></span>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<span class="calendar-event-delete-icon"></span>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  createdBy;

  events: CalendarEvent[];

  activeDayIsOpen: boolean = true;

  constructor(
    // private modal: NgbModal
  ) { }

  ngOnInit() {
    this.createdBy = 'creatorID';
    this.events = [
      // {
      //   start: subDays(startOfDay(new Date()), 1),
      //   end: addDays(new Date(), 1),
      //   title: "A 3 day event",
      //   color: colors.red,
      //   actions: this.actions,
      //   allDay: true,
      //   resizable: {
      //     beforeStart: true,
      //     afterEnd: true
      //   },
      //   // draggable: true
      // },
      {
        start: addHours(new Date('2020-03-21'), 9),
        title: 'Camba/Putnam house state @9:00 AM',
        color: colors.red,
        actions: this.createdBy == 'creatorID' ? this.actions : null
      },
      {
        start: addHours(new Date('2020-03-25'), 12),
        title: 'Unique People Services, inc/grand concourse @12:00 PM',
        color: colors.red,
        actions: this.createdBy == 'notCreatorID' ? this.actions : null
      },
      {
        start: addHours(new Date('2020-03-25'), 16),
        title: 'Odyssey house @4:00 PM',
        color: colors.red,
        actions: this.createdBy == 'notCreatorID' ? this.actions : null
      },
      // {
      //   start: addHours(startOfDay(new Date()), 10),
      //   title: "Available slots : 10:00 AM, 1:00 PM, 3:00 PM",
      //   color: colors.green,
      // },

      // {
      //   start: subDays(endOfMonth(new Date()), 3),
      //   end: addDays(endOfMonth(new Date()), 3),
      //   title: "A long event that spans 2 months",
      //   color: colors.blue,
      //   allDay: true
      // },
      // {
      //   start: addHours(startOfDay(new Date()), 2),
      //   end: addHours(new Date(), 2),
      //   title: "A draggable and resizable event",
      //   color: colors.yellow,
      //   actions: this.actions,
      //   resizable: {
      //     beforeStart: true,
      //     afterEnd: true
      //   },
      //   // draggable: true
      // }
    ];
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    // this.modal.open(this.modalContent, { size: "lg" });
    console.log(this.modalData);
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

}

