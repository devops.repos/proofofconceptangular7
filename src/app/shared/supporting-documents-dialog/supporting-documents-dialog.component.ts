import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { iHousingApplicationSupportingDocumentsData } from 'src/app/shared/housing-application-supporting-documents/housing-application-supporting-document.model';

@Component({
    selector: 'app-supporting-documents-dialog',
    templateUrl: './supporting-documents-dialog.html',
    styleUrls: ['./supporting-documents-dialog.component.scss']
})

export class SupportingDocumentsDialogComponent implements OnInit {
    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) public housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData,
        private dialogRef: MatDialogRef<SupportingDocumentsDialogComponent>) {
    }

    //Close the dialog on close button
    CloseDialog() {
        this.dialogRef.close(true);
    }

    //On Init
    ngOnInit() {
    }
}