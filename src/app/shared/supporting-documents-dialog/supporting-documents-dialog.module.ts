import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { SupportingDocumentsDialogComponent } from './supporting-documents-dialog.component';
import { HousingApplicationSupportingDocumentsModule } from '../housing-application-supporting-documents/housing-application-supporting-documents.module';

@NgModule({
  declarations: [SupportingDocumentsDialogComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,    
    HousingApplicationSupportingDocumentsModule,
  ],
  exports: [SupportingDocumentsDialogComponent]
})

export class SupportingDocumentsDialogModule { }