import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PACTDocument } from './document.model';
import { CommonService } from 'src/app/services/helper-services/common.service';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "doc-action",
  template: `<mat-icon  matTooltip='View Document' (click)="onView()" class="doc-action-icon" color="primary">find_in_page</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete Document' class="doc-action-icon" color="warn" *ngIf="!params.data.deleteDisabled">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class DocActionComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;
  private docSelected: PACTDocument;

  constructor(
    private commonService: CommonService
  ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    this.docSelected = {
      pactDocumentID: this.params.data.pactDocumentID,
      docExtension: this.params.data.docExtension,
      docType: this.params.data.docType,
      fileNetDocID: this.params.data.fileNetDocID,
      capsReportID: this.params.data.capsReportID
    };
    this.commonService.setIsOverlay(true);
    // alert(JSON.stringify(this.docSelected));
    // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
    this.params.context.componentParent.docViewParent(this.docSelected);
    // this.c2vService.setClientAwaitingPlacementSelected(this.docSelected);
  }

  onDelete() {
    this.docSelected = {
      pactDocumentID: this.params.data.pactDocumentID,
      docExtension: this.params.data.docExtension,
      createdBy: this.params.data.createdBy,
      docType: this.params.data.docType
    };
    // alert(JSON.stringify(this.docSelected));
    // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
    this.params.context.componentParent.docDeleteParent(this.docSelected);
    // this.c2vService.setClientAwaitingPlacementSelected(this.docSelected);
    // todo
  }

  refresh(): boolean {
    return false;
  }
}
