import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import "ag-grid-enterprise";
import { ToastrModule } from 'ngx-toastr';

import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { DocumentComponent } from './document.component';
import { DocActionComponent } from './doc-action.component';
import { PriorDocActionComponent } from './prior-doc-action.component';
import { DirectivesModule } from '../directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    ReactiveFormsModule,
    DirectivesModule,
    ToastrModule.forRoot({
      progressBar: true,
      progressAnimation: 'increasing'
    }),
    AgGridModule.withComponents([
      DocActionComponent,
      PriorDocActionComponent
    ])
  ],
  declarations: [
    DocumentComponent,
    DocActionComponent,
    PriorDocActionComponent
  ],
  exports: [
    DocumentComponent
  ]
})
export class DocumentModule { }
