import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  OnDestroy
} from '@angular/core';
import { HttpEventType } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { Subscription, BehaviorSubject } from 'rxjs';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { GridOptions } from 'ag-grid-community';
import { ToastrService } from 'ngx-toastr';

import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { DocumentService } from './document.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { PACTDocument } from 'src/app/shared/document/document.model';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { DocActionComponent } from './doc-action.component';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { ConfirmDialogService } from '../confirm-dialog/confirm-dialog.service';
import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
import { PriorDocActionComponent } from './prior-doc-action.component';
import { iDocumentLink } from '../client-documents-grid/client-documents-grid.component';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
  providers: [DatePipe]
})
export class DocumentComponent implements OnInit, OnDestroy {

  userData: AuthData;
  documentForm: FormGroup;
  docTypes: RefGroupDetails[];
  fileToUpload: File = null;
  progress: number;
  message: string;
  originalPreSelectedDocType = 0;
  @Input() pactApplicationID: number;
  @Input() preSelectedDocType: number;
  @Input() docTitle: string;
  @Input() docSubTitle;
  @Input() vcsReferralID: number;
  @Input() deleteDisabled = false;
  @Input() vcsReferralIDObservable: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  @Output() uploadFinished = new EventEmitter();
  @Output() docDataChanged = new EventEmitter();
  @Output() onValueChange = new EventEmitter();

  @ViewChild('agGrid') agGrid: AgGridAngular;

  documentData: PACTDocument = {
    docType: null,
    pactFile: null,
    docName: null,
    docExtension: null,
    docDescription: null,
    pactApplicationID: this.pactApplicationID || 0,
    vcsReferralID: 0,
    pactDocumentID: null,
    createdBy: null,
    capsReportID: null
  };

  validationMessages = {
    docTypeCtrl: { dropdownRequired: 'Document type is required.' },
    docFileCtrl: {
      required: 'File is required.',
      fileExtn:
        'The file format you are attempting to attach is restricted. Please attach an acceptable format.',
      fileName: 'The file name should not contains special characters.'
    },
    docDescriptionCtrl: {
      required: 'Document description is required.', charLength: 'Invalid Document description'
    }
  };

  formErrors = {
    docTypeCtrl: '',
    docFileCtrl: '',
    docDescriptionCtrl: ''
  };

  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  public gridOptions: GridOptions;
  rowData: PACTDocument[];
  context: any;
  overlayNoRowsTemplate: string;

  isControlTouched = false;
  isVerificationTouched = false;
  isPsychiatricAttachment: boolean;
  verifiedDate: string;
  verifiedBy: number;
  verifiedByName: string;
  clinicianTitleTypes: RefGroupDetails[];
  clinicianTypeData: RefGroupDetails[];
  minDate = new Date(new Date().getTime() - 180 * 24 * 60 * 60 * 1000);
  maxDate = new Date(Date.now());
  isValid = false;
  verifyMessage: string;
  isCollMD = false;
  lblClinicianName = 'Name of Clinician';
  lblCollMD = 'Collaborating M.D.';

  collaboratingClinicianTitleTypes: RefGroupDetails[];
  isCollaboratingFieldsVisible: boolean = false;

  priorGridApi: any;
  priorGridColumnApi: any;
  priorFrameworkComponents: any;
  priorRowData: PACTDocument[];
  priorOverlayNoRowsTemplate: string;

  userDataSub: Subscription;
  docServiceSub: Subscription;
  docFormSub: Subscription;
  commonServiceSub: Subscription;
  ClientApplicationServiceSub: Subscription;
  vcsReferralIDObservableSub: Subscription;

  constructor(
    private fb: FormBuilder,
    private documentService: DocumentService,
    private commonService: CommonService,
    private userService: UserService,
    private confirmDialogService: ConfirmDialogService,
    private toastr: ToastrService,
    private clientApplicationService: ClientApplicationService,
    private datePipe: DatePipe
  ) {
    this.gridOptions = {
    } as GridOptions;

    this.columnDefs = [
      {
        headerName: 'Actions',
        field: 'action',
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      },
      { headerName: 'Type', field: 'docTypeDescription', width: 150, filter: 'agTextColumnFilter' },
      { headerName: 'Name', field: 'docName', width: 200, filter: 'agTextColumnFilter' },
      { headerName: 'Description', field: 'docDescription', width: 300, filter: 'agTextColumnFilter' },
      { headerName: 'Attached Date', field: 'attachedDate', filter: 'agTextColumnFilter' },
      { headerName: 'Attached Time', field: 'attachedTime', filter: 'agTextColumnFilter' },
      { headerName: 'Attached By', field: 'attachedBy', width: 250, filter: 'agTextColumnFilter' },
      {
        headerName: 'Agency/ Site', filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.agencyNo + '/' + params.data.siteNo;
        }
      },
      { headerName: 'Clinician Title', field: 'clinicianTitle', filter: 'agTextColumnFilter', hide: true },
      { headerName: 'Clinician Name', field: 'clinicianName', filter: 'agTextColumnFilter', hide: true },
      { headerName: 'License No', field: 'licenseNo', filter: 'agTextColumnFilter', hide: true },
      { headerName: 'Evaluation Date', field: 'evaluationDate', filter: 'agTextColumnFilter', hide: true },
      { headerName: 'Collaborating Clinician Title', field: 'collaboratingClinicianTitle', filter: 'agTextColumnFilter', hide: true },
      { headerName: 'Collaborating Clinician Name', field: 'collaboratingMD', filter: 'agTextColumnFilter', hide: true },
      { headerName: 'Collaborating Clinician License No', field: 'collaboratingClinicianLicenseNo', filter: 'agTextColumnFilter', hide: true },

    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.rowSelection = 'multiple';
    this.pagination = true;
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: DocActionComponent
    };

    this.priorFrameworkComponents = {
      actionRenderer: PriorDocActionComponent
    };
  }

  ngOnInit() {

    if (this.preSelectedDocType) {
      this.originalPreSelectedDocType = this.preSelectedDocType;
    }

    /** Getting the CurrentUser Details from UserService */
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    /* Get the vcsReferralID for Non-supportive Sidedoor moveIn document Upload */
    this.vcsReferralIDObservableSub = this.vcsReferralIDObservable.subscribe(vcsRefId => {
      if (vcsRefId > 0) {
        this.vcsReferralID = vcsRefId;
      }
    });

    this.documentForm = this.fb.group({
      docTypeCtrl: [
        this.originalPreSelectedDocType,
        [CustomValidators.dropdownRequired()]
      ],
      fileCtrl: [''],
      docFileCtrl: ['', [Validators.required, CustomValidators.fileNameExtn()]],
      docDescriptionCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
      isConsentVerifiedCtrl: [''],
      clinicianTitleTypeCtrl: [''],
      evaluationDateCtrl: [''],
      clinicianNameCtrl: [''],
      licenseNoCtrl: [''],
      collaboratingMDCtrl: [''],
      collaboratingClinicianTitleType: [''],
      collaboratingClinicianName: [''],
      collaboratingClinicianLicenseNo: [''],
    });

    if (!this.docTitle) {
      this.docTitle = 'Attach Documents';
    }

    if (!this.docSubTitle && this.preSelectedDocType === 55) {
      this.docSubTitle = 'Please attach documentation of homelessness that is drafted on agency letter head, dated, includes dates of homeless outreach engagement and location(s) of where the applicant was observed to be homeless, and name of staff attesting to agency\'s homeless assistance. For more information see <a href="./assets/documents/HUD_Chronic_Reference_Sheet.pdf" target="_blank">reference sheet </a> for definition of HUD chronically homeless and examples of appropriate supporting documentation for this application.';
    } else if (!this.docSubTitle && this.preSelectedDocType === 56) {
      this.docSubTitle = 'Please attach documentation from a NYS OASAS licensed substance use treatment provider that is drafted on agency letter head, dated within the last 30 days, and includes the name of the agency staff attesting to the applicant\’s treatment progress. The documentation must demonstrate 90 days of sobriety and/or treatment completion (including the results along with dates of the last three toxicology test). For an example of a substance use treatment letter, please see the <a href="./assets/documents/SubUse_Doc_Reference_Sheet.pdf" target="_blank">reference sheet. </a>';
    }

    this.loadRefGroupDetails();

    this.docFormSub = this.documentForm.valueChanges.subscribe(data => {
      this.logValidationErrors(this.documentForm);
    });

  }

  onSubmit = (): void => {
    this.setValidators();

    this.markFormGroupTouched(this.documentForm);
    this.logValidationErrors(this.documentForm);

    if (this.documentForm.valid && !this.isDocNameExists() && !this.isDocSizeExceeds20MB()) {
      if (this.preSelectedDocType === 52 || this.preSelectedDocType === 53 || this.preSelectedDocType === 54) {
        if (this.preSelectedDocType === 52 && this.isPsychiatricFormValid()) {
          this.saveDocument();
        }
        if (this.preSelectedDocType === 53 && this.isPsychosocialFormValid()) {
          this.saveDocument();
        }
        if (this.preSelectedDocType === 54 && this.isMedicalFormValid()) {
          this.saveDocument();
        }
      } else {
        this.saveDocument();
      }
    } else {
      this.showValidationErrors();
    }
  }

  onDocTypeChange() {
    if (this.documentForm.get('docTypeCtrl').value) {
      this.preSelectedDocType = this.documentForm.get('docTypeCtrl').value;
    }
    this.updateVerifyBlock();
  }

  /* Below code is removed for OPRE-2313 */
  onMedClinicanTypeChange() {
    //   // Licensed Psychiatric Nurse Practitioner (NPP)
    //   if (this.documentForm.get('clinicianTitleTypeCtrl').value === 433) {
    //     this.lblClinicianName = 'Name of Nurse Practitioner';
    //     this.lblCollMD = 'Collaborating M.D.';
    //     this.isCollMD = true;
    //     // Licensed Medical Doctor (MD)
    //   } else if (this.documentForm.get('clinicianTitleTypeCtrl').value === 929) {
    //     this.lblClinicianName = 'Name of Clinician';
    //     this.lblCollMD = null;
    //     this.isCollMD = false;
    //     // Physician Assistant (PA)
    //   } else if (this.documentForm.get('clinicianTitleTypeCtrl').value === 930) {
    //     this.lblClinicianName = 'Name of Physician Assistant';
    //     this.lblCollMD = 'Supervising M.D.';
    //     this.isCollMD = true;
    //   }
  }

  onClinicianTitleTypeChange() {
    if (this.documentForm.get('clinicianTitleTypeCtrl').value === 932)
      this.isCollaboratingFieldsVisible = true;
    else {
      this.isCollaboratingFieldsVisible = false;

      this.documentForm.controls['collaboratingClinicianTitleType'].setValue('');
      this.documentForm.controls['collaboratingClinicianName'].setValue('');
      this.documentForm.controls['collaboratingClinicianLicenseNo'].setValue('');

      this.documentForm.controls.collaboratingClinicianName.clearValidators();
      this.documentForm.controls.collaboratingClinicianName.updateValueAndValidity();
      this.documentForm.controls.collaboratingClinicianLicenseNo.clearValidators();
      this.documentForm.controls.collaboratingClinicianLicenseNo.updateValueAndValidity();
    }
  }

  isDocNameExists = () => {
    const uploadFileName = this.fileToUpload.name;
    const formDocName = uploadFileName.substr(0, uploadFileName.lastIndexOf('.'));
    if (this.rowData.filter(x => x.docName.toLowerCase() === formDocName.toLowerCase()).length > 0) {
      this.toastr.error('There is already a document with the same name. Please use a different name.');
      return true;
    } else {
      return false;
    }
  }

  isDocSizeExceeds20MB = () => {
    var fileSize = (this.fileToUpload.size / (1024 * 1024)).toFixed(2);
    //alert(parseFloat(fileSize));
    if (parseFloat(fileSize) > 20.00) {
      this.toastr.error('The document to be uploaded exceeds the size limit of 20 MB.');
      return true;
    }
    else if (parseFloat(fileSize) == 0.00) {
      this.toastr.error('The document to be uploaded cannot be the size of 0 Bytes.');
      return true;
    }
    else {
      return false;
    }
  }

  isPsychiatricFormValid() {
    let isPsychValid = false;

    if (this.verifiedBy) {
      if (this.documentForm.get('clinicianTitleTypeCtrl').value) {
        if (this.documentForm.get('clinicianNameCtrl').value) {
          if (this.documentForm.get('licenseNoCtrl').value) {
            if (this.datePipe.transform(this.documentForm.get('evaluationDateCtrl').value, 'MM/dd/yyyy')) {
              isPsychValid = true;
            } else {
              this.toastr.error('Please provide Evaluation Date.');
            }

          } else {
            this.toastr.error('Please provide License Number.');
          }

        } else {
          this.toastr.error('Please provide Clinician Name.');
        }
      } else {
        this.toastr.error('Please provide Clinician Title.');
      }
    } else {
      this.toastr.error('Please Verify the consent.');
    }

    if (isPsychValid && this.documentForm.get('clinicianTitleTypeCtrl').value === 932) {
      if (this.documentForm.get('collaboratingClinicianTitleType').value) {
        if (this.documentForm.get('collaboratingClinicianName').value) {
          if (this.documentForm.get('collaboratingClinicianLicenseNo').value) {
            isPsychValid = true;
          }
          else {
            isPsychValid = false;
            this.toastr.error('Please provide Collaborating Clinician License No.');
          }
        }
        else {
          isPsychValid = false;
          this.toastr.error('Please provide Collaborating Clinician Name.');
        }
      }
      else {
        isPsychValid = false;
        this.toastr.error('Please provide Collaborating Clinician Title Type.');
      }
    }

    return isPsychValid;
  }


  isPsychosocialFormValid() {
    let isPsychoValid = false;

    if (this.verifiedBy) {
      if (this.documentForm.get('clinicianNameCtrl').value) {
        if (this.datePipe.transform(this.documentForm.get('evaluationDateCtrl').value, 'MM/dd/yyyy')) {
          isPsychoValid = true;
        } else {
          this.toastr.error('Please provide Date of Psychosocial Summary.');
        }
      } else {
        this.toastr.error('Please provide Qualified Professional Name.');
      }
    } else {
      this.toastr.error('Please Verify the consent.');
    }
    return isPsychoValid;
  }

  isMedicalFormValid() {
    let isMedicalValid = false;

    if (this.verifiedBy) {
      if (this.documentForm.get('clinicianTitleTypeCtrl').value) {
        if (this.documentForm.get('clinicianNameCtrl').value) {
          if (this.documentForm.get('licenseNoCtrl').value) {
            if (this.datePipe.transform(this.documentForm.get('evaluationDateCtrl').value, 'MM/dd/yyyy')) {
              isMedicalValid = true;

              /* Below code is removed for OPRE-2313 */
              // if (!this.isCollMD || (this.isCollMD && this.documentForm.get('collaboratingMDCtrl').value)) {
              //   isMedicalValid = true;
              // } else {
              //   this.toastr.error('Please provide ' + this.lblCollMD);
              // }

            } else {
              this.toastr.error('Please provide Date of Medical Evaluation.');
            }

          } else {
            this.toastr.error('Please provide License Number.');
          }

        } else {
          this.toastr.error('Please provide ' + this.lblClinicianName);
        }
      } else {
        this.toastr.error('Please provide Clinician who performed the Medical Evaluation.');
      }
    } else {
      this.toastr.error('Please Verify the consent.');
    }
    return isMedicalValid;
  }

  updateVerifyBlock() {
    if (this.preSelectedDocType === 52) {
      this.verifyMessage = 'I verify that my agency has on record the “Comprehensive Psychiatric Evaluation” completed and signed, within the last 180 days, by a New York State licensed clinician. The NYS licensed clinician must be included on the HRA list of professionals able to complete a comprehensive psychiatric evaluation. I understand that HRA reserves the right to request this document and that I and/or my organization may be held liable for incorrect or fraudulent information.';
    } else if (this.preSelectedDocType === 53) {
      this.verifyMessage = 'I verify that my agency has on record the "Comprehensive Psychosocial Summary" completed within the last 180 days and signed by a qualified professional (e.g. social worker, case manager, or discharge planner, etc.).';
    } else if (this.preSelectedDocType === 54) {
      this.verifyMessage = 'I verify that my agency has on record a completed "Medical Evaluation" performed within the last 180 days and signed by a health care professional.';
    }

    if (this.preSelectedDocType === 54) {
      this.clinicianTitleTypes = this.clinicianTypeData.filter(d => (d.refGroupDetailID === 433 || d.refGroupDetailID === 929 || d.refGroupDetailID === 930));
    } else {
      this.clinicianTitleTypes = this.clinicianTypeData;
    }
  }

  onHandleFileInput = (files: FileList) => {
    this.fileToUpload = files[0] as File;
    if (this.fileToUpload) {
      this.documentForm.get('docFileCtrl').setValue(this.fileToUpload.name);
    }
  }

  loadRefGroupDetails = () => {
    const value = '16, 44'; // docType, Clinician Title type
    this.commonServiceSub = this.commonService.getRefGroupDetails(value).subscribe(res => {
      const data = res.body as RefGroupDetails[];
      this.clinicianTypeData = data.filter(d => d.refGroupID === 44);
      this.collaboratingClinicianTitleTypes = data.filter(d => (d.refGroupDetailID === 432 || d.refGroupDetailID === 434 || d.refGroupDetailID === 435));
      if (this.preSelectedDocType === 54) {
        this.clinicianTitleTypes = this.clinicianTypeData.filter(d => (d.refGroupDetailID === 433 || d.refGroupDetailID === 929 || d.refGroupDetailID === 930));
      } else {
        this.clinicianTitleTypes = this.clinicianTypeData;
      }

      if (this.preSelectedDocType > 0) {
        this.docTypes = data.filter(
          d => d.refGroupDetailID === this.preSelectedDocType
        );
      } else { // removed Psychiatric(52), Psychosocial(53), Medical(54), Substance Use(56) options from ddl
        this.docTypes = data.filter(d => (d.refGroupID === 16 && d.refGroupDetailID < 100 && d.refGroupDetailID !== 52 && d.refGroupDetailID !== 53 && d.refGroupDetailID !== 54 && d.refGroupDetailID !== 56));
      }

      this.updateVerifyBlock();
    },
      error => console.error('Error!', error)
    );
  }

  saveDocument = () => {
    const uploadFileName = this.fileToUpload.name;
    this.documentData.pactFile = this.fileToUpload;
    this.documentData.docName = uploadFileName.substr(
      0,
      uploadFileName.lastIndexOf('.')
    );
    this.documentData.docExtension = uploadFileName.split('.').pop();
    this.documentData.docType = this.documentForm.get('docTypeCtrl').value;
    this.documentData.docDescription = this.documentForm.get(
      'docDescriptionCtrl'
    ).value;
    this.documentData.pactApplicationID = this.pactApplicationID || 0;
    this.documentData.vcsReferralID = this.vcsReferralID || 0;

    if (this.preSelectedDocType === 52 || this.preSelectedDocType === 54) {
      this.documentData.clinicianTitleType = this.documentForm.get('clinicianTitleTypeCtrl').value;
      this.documentData.licenseNo = this.documentForm.get('licenseNoCtrl').value;
    }
    if (this.preSelectedDocType === 52 || this.preSelectedDocType === 53 || this.preSelectedDocType === 54) {
      this.documentData.clinicianName = this.documentForm.get('clinicianNameCtrl').value;
      this.documentData.evaluationDate = this.datePipe.transform(this.documentForm.get('evaluationDateCtrl').value, 'MM/dd/yyyy');
    }
    if (this.isCollMD && this.preSelectedDocType === 54) {
      this.documentData.collaboratingMD = this.documentForm.get('collaboratingMDCtrl').value;
    }

    if (this.preSelectedDocType === 52) {
      this.documentData.collaboratingClinicianTitleType = this.documentForm.get('collaboratingClinicianTitleType').value !== null ? this.documentForm.get('collaboratingClinicianTitleType').value : 0;
      this.documentData.collaboratingMD = this.documentForm.get('collaboratingClinicianName').value;
      this.documentData.collaboratingClinicianLicenseNo = this.documentForm.get('collaboratingClinicianLicenseNo').value;
    }

    const formData = new FormData();
    formData.append(
      'PACTApplicationId',
      this.documentData.pactApplicationID.toString()
    );
    formData.append('VCSReferralID', this.documentData.vcsReferralID.toString());
    formData.append('DocType', this.documentData.docType.toString());
    formData.append('PACTFile', this.documentData.pactFile);
    formData.append('DocName', this.documentData.docName);
    formData.append('DocExtension', this.documentData.docExtension);
    formData.append('DocDescription', this.documentData.docDescription);
    if (this.preSelectedDocType === 52 || this.preSelectedDocType === 54) {
      formData.append('ClinicianTitleType', this.documentData.clinicianTitleType.toString());
      formData.append('LicenseNo', this.documentData.licenseNo);
    }
    if (this.preSelectedDocType === 52 || this.preSelectedDocType === 53 || this.preSelectedDocType === 54) {
      formData.append('ClinicianName', this.documentData.clinicianName);
      formData.append('EvaluationDate', this.documentData.evaluationDate);
    }
    if (this.isCollMD && this.preSelectedDocType === 54) {
      formData.append('CollaboratingMD', this.documentData.collaboratingMD);
    }

    if (this.preSelectedDocType === 52) {
      formData.append('CollaboratingClinicianTitleType', this.documentData.collaboratingClinicianTitleType.toString());
      formData.append('CollaboratingMD', this.documentData.collaboratingMD);
      formData.append('CollaboratingClinicianLicenseNo', this.documentData.collaboratingClinicianLicenseNo);
    }

    // console.log(this.documentData);
    this.docServiceSub = this.documentService.saveDocument(formData).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round((100 * event.loaded) / event.total);
      } else if (event.type === HttpEventType.Response) {
        this.message = 'Document has been uploaded.';
        this.toastr.success(this.message);
        this.uploadFinished.emit(this.message);
        this.resetValues();
        this.onValueChange.emit('Input clear');
        this.getDocumentList();
      } else {
        // this.toastr.error('There was an error in uploading the document.', 'Document upload Failed!');
      }
    });
  }

  viewDocument = (FileName: string) => {
    // this.downloadStatus.emit( {status: ProgressStatusEnum.START});

    this.docServiceSub = this.documentService.viewDocument(FileName).subscribe(data => {
      switch (data.type) {
        // case HttpEventType.DownloadProgress:
        // this.downloadStatus.emit( {status: ProgressStatusEnum.IN_PROGRESS, percentage: Math.round((data.loaded / data.total) * 100)});
        //   break;
        case HttpEventType.Response:
          //  this.downloadStatus.emit( {status: ProgressStatusEnum.COMPLETE});
          const downloadedFile = new Blob([data.body], {
            type: data.body.type
          });
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(downloadedFile, FileName);
          } else {
            // window.open(URL.createObjectURL(downloadedFile), '_blank');
            this.commonService.OpenWindow(URL.createObjectURL(downloadedFile));
          }
          break;
      }
    },
      error => {
        this.toastr.error('There was an error in downloading the document.', 'Document download Failed!');
        // this.downloadStatus.emit( {status: ProgressStatusEnum.ERROR});
      }
    );
  }

  deleteDocument = (dataSelected: PACTDocument) => {
    dataSelected.pactApplicationID = this.pactApplicationID;
    this.docServiceSub = this.documentService.deleteDocument(dataSelected).subscribe(res => {
      if (res) {
        this.getDocumentList();
        this.toastr.success(this.message, 'Document has been deleted.');
      } else {
        this.toastr.error('There was an error in deleting the document.');
      }
    },
      error => {
        this.toastr.error('There was an error in deleting the document.');
      }
    );
  }

  // onFirstDataRendered(params) {
  //   params.api.sizeColumnsToFit();
  // }

  onGridReady = (params) => {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action'
        && column.colId !== 'docTypeDescription'
        && column.colId !== 'docName'
        && column.colId !== 'attachedBy'
        && column.colId !== 'docDescription') {
        allColumnIds.push(column.colId);
      }
    });
    // this.gridColumnApi.autoSizeColumns(allColumnIds);
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Documents To Show</span>';
    if (this.preSelectedDocType === 52) {
      this.gridColumnApi.setColumnsVisible(['clinicianTitle', 'clinicianName', 'licenseNo', 'evaluationDate', 'collaboratingMD', 'collaboratingClinicianTitle', 'collaboratingClinicianLicenseNo'], true);
    } else if (this.preSelectedDocType === 53) {
      this.gridColumnApi.setColumnsVisible(['clinicianName', 'evaluationDate'], true);
    } else if (this.originalPreSelectedDocType === 0 || this.preSelectedDocType === 54) {
      /* Below code is changed for OPRE-2313 */
      // this.gridColumnApi.setColumnsVisible(['collaboratingMD', 'clinicianTitle', 'clinicianName', 'licenseNo', 'evaluationDate'], true);
      this.gridColumnApi.setColumnsVisible(['clinicianTitle', 'clinicianName', 'licenseNo', 'evaluationDate'], true);
    }

    /** API call to get the grid data */
    this.getDocumentList();
  }

  docViewParent = (cell: PACTDocument) => {
    if (cell.docType === 331) {
      if (cell.capsReportID) {
        this.commonService.displaySurveySummaryReport(cell.capsReportID, this.userData.optionUserId);
        this.commonService.setIsOverlay(false);
      }
    } else if (cell.fileNetDocID) {
      this.docServiceSub = this.documentService.getFileNetDocumentURL(cell.fileNetDocID).subscribe(res => {
        const data = res as iDocumentLink;
        if (data) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    } else {
      this.viewDocument(cell.pactDocumentID + '.' + cell.docExtension);
      this.commonService.setIsOverlay(false);
    }
  }

  docDeleteParent = (cell: PACTDocument) => {
    if (cell.docType === 331) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error('Cannot delete Coordinated Assessment Survey Summary Report');
      }
    } else {
      const title = 'Confirm Delete';
      const primaryMessage = `The document will be deleted permanently.`;
      const secondaryMessage = `Are you sure you want to delete the selected document?`;
      const confirmButtonName = 'Yes';
      const dismissButtonName = 'No';

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        (positiveResponse) => this.deleteDocument(cell),
        (negativeResponse) => console.log(),
      );
    }

    // const title = 'Alert';
    // const primaryMessage = 'Changing the Firstname and Lastname will force the client re-match';
    // const secondaryMessage = 'It impacts Demographics and Housing homeless data.';
    // const confirmButtonName = 'OK';
    // const dismissButtonName = '';

    // this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName);
  }

  getDocumentList = () => {
    if (this.pactApplicationID > 0 || this.vcsReferralID > 0) {
      this.documentData.pactApplicationID = this.pactApplicationID;
      this.documentData.vcsReferralID = this.vcsReferralID;
      this.documentData.docType = this.preSelectedDocType;
      this.documentData.pactFile = null;
      this.docServiceSub = this.documentService.getDocumentList(this.documentData).subscribe(res => {
        this.rowData = res as PACTDocument[];
        this.docDataChanged.emit('docData Changed');
        this.rowData.forEach(row => {
          // if (row.createdByID != this.userData.optionUserId && this.deleteDisabled) {
          // Changed this condition to make it global.
          if (row.createdByID != this.userData.optionUserId) {
            row.deleteDisabled = true;
          } else {
            row.deleteDisabled = false;
          }
        });
      });
    } else {
      this.ClientApplicationServiceSub = this.clientApplicationService.getPactApplicationID().subscribe(appID => {
        this.pactApplicationID = appID;
        this.documentData.pactApplicationID = this.pactApplicationID;
        this.documentData.docType = this.preSelectedDocType;
        this.documentData.pactFile = null;
        this.docServiceSub = this.documentService.getDocumentList(this.documentData).subscribe(res => {
          this.rowData = res as PACTDocument[];
          this.docDataChanged.emit('docData Changed');
          this.rowData.forEach(row => {
            // if (row.createdByID != this.userData.optionUserId && this.deleteDisabled) {
            // Changed this condition to make it global.
            if (row.createdByID != this.userData.optionUserId) {
              row.deleteDisabled = true;
            } else {
              row.deleteDisabled = false;
            }
          });
        });
      });
    }
  }

  onPriorGridReady = (params) => {
    params.api.setDomLayout('autoHeight');
    this.priorGridApi = params.api;
    this.priorGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.priorGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action'
        && column.colId !== 'docTypeDescription'
        && column.colId !== 'docName'
        && column.colId !== 'attachedBy'
        && column.colId !== 'docDescription') {
        allColumnIds.push(column.colId);
      }
    });
    // this.gridColumnApi.autoSizeColumns(allColumnIds);
    this.priorOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Prior Documents To Show</span>';
    /** API call to get the grid data */
    if (this.preSelectedDocType === 55) {
      this.docServiceSub = this.documentService.getPriorDocumentList(this.pactApplicationID).subscribe(res => {
        this.priorRowData = res as PACTDocument[];
      });
    } else {
      this.priorRowData = null;
    }
  }

  priorDocViewParent = (cell: PACTDocument) => {
    if (cell.fileNetDocID) {
      this.docServiceSub = this.documentService.getFileNetDocumentURL(cell.fileNetDocID).subscribe(res => {
        const data = res as iDocumentLink;
        if (data) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    }
  }

  priorDocSaveParent = (cell: PACTDocument) => {
    if (cell.fileNetDocID) {
      this.documentData.pactApplicationID = this.pactApplicationID;
      this.documentData.pactDocumentID = cell.pactDocumentID;
      this.docServiceSub = this.documentService.savePriorHousingDocument(this.documentData).subscribe(res => {
        if (res) {
          if (res === -2) {
            this.toastr.error('There is already a document with the same name and description.');
          } else if (res === -1) {
            this.toastr.error('There was an issue in attaching the document. Please try again later.');
          } else if (res > 0) {
            this.toastr.success('The selected document has been attached to the current application.');
            this.getDocumentList();
          }
        }
      });
    }
  }

  // Verification section
  // On Consent check change
  onIsConsentCheckChange(event: { checked: boolean }) {
    this.isVerificationTouched = true;
    if (event.checked) {
      this.documentForm.get('isConsentVerifiedCtrl').setValue(true);
      this.verifiedBy = this.userData.optionUserId;
      this.verifiedByName = this.userData.firstName + ' ' + this.userData.lastName;
      this.verifiedDate = this.datePipe.transform(new Date(), 'MM/dd/yyyy');
    } else {
      this.documentForm.get('isConsentVerifiedCtrl').setValue(false);
      this.verifiedBy = null;
      this.verifiedByName = null;
      this.verifiedDate = null;
    }
  }
  // Get Selected Clinician ToolTip
  getToolTipData() {
    const selected = this.documentForm.get('clinicianTitleTypeCtrl').value != null ? this.documentForm.get('clinicianTitleTypeCtrl').value : null;
    if (selected) {
      if (this.clinicianTitleTypes && this.clinicianTitleTypes.length > 0) {
        const selectedTitleType = this.clinicianTitleTypes.find(({ refGroupDetailID }) => refGroupDetailID === selected);
        if (selectedTitleType) {
          return selectedTitleType.refGroupDetailDescription;
        }
      }
    }
  }

  //Get Selected Collaborating Clinician ToolTip
  getCollaboratingToolTipData() {
    var selected = this.documentForm.get('collaboratingClinicianTitleType').value != null ? this.documentForm.get('collaboratingClinicianTitleType').value : null;
    if (selected) {
      if (this.collaboratingClinicianTitleTypes && this.collaboratingClinicianTitleTypes.length > 0) {
        const selectedTitleType = this.collaboratingClinicianTitleTypes.find(({ refGroupDetailID }) => refGroupDetailID === selected);
        if (selectedTitleType) {
          return selectedTitleType.longDescription;
        }
      }
    }
  }

  // Validate Date in last 180 days
  validateDate() {
    const evaluationDate = this.datePipe.transform(this.documentForm.get('evaluationDateCtrl').value, 'MM/dd/yyyy');

    if (!moment(evaluationDate, 'MM/DD/YYYY', true).isValid()) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error('Invalid Date.');
      }

      this.isValid = false;
    } else {
      const days = moment(Date.now()).diff(moment(evaluationDate), 'day', true);
      if (days > 181 || days < -0) {
        if (!this.toastr.currentlyActive) {
          if (this.preSelectedDocType === 52) {
            this.toastr.error('Psychiatric Evaluation Date must be with 180 days of today’s date.');
          } else if (this.preSelectedDocType === 53) {
            this.toastr.error('Psychosocial Summary Date must be within 180 days of today\'s date.');
          }
        }
        this.isValid = false;
      } else {
        this.isValid = true;
      }
    }
  }


  resetValues = () => {
    this.message = '';
    this.progress = 0;

    if (this.originalPreSelectedDocType > 0) {
      this.preSelectedDocType = this.originalPreSelectedDocType;
    } else {
      this.preSelectedDocType = null;
    }

    this.documentForm.get('docTypeCtrl').setValue(this.originalPreSelectedDocType);
    this.documentForm.get('docFileCtrl').setValue('');
    this.documentForm.get('docDescriptionCtrl').setValue('');

    this.documentForm.get('isConsentVerifiedCtrl').setValue(false);
    this.documentForm.get('clinicianTitleTypeCtrl').setValue('');
    this.documentForm.get('clinicianNameCtrl').setValue('');
    this.documentForm.get('licenseNoCtrl').setValue('');
    this.documentForm.get('evaluationDateCtrl').setValue('');
    this.documentForm.get('collaboratingMDCtrl').setValue('');
    this.documentForm.controls['collaboratingClinicianTitleType'].setValue('');
    this.documentForm.controls['collaboratingClinicianName'].setValue('');
    this.documentForm.controls['collaboratingClinicianLicenseNo'].setValue('');
    this.isCollaboratingFieldsVisible = false;
    this.verifiedByName = null;
    this.verifiedDate = null;
    this.verifiedBy = null;


    this.clearValidators();
  }

  clearValidators = () => {
    this.documentForm.controls.docDescriptionCtrl.clearValidators();
    this.documentForm.controls.docFileCtrl.clearValidators();
    this.documentForm.controls.docTypeCtrl.clearValidators();

    this.documentForm.controls.docDescriptionCtrl.updateValueAndValidity();
    this.documentForm.controls.docFileCtrl.updateValueAndValidity();
    this.documentForm.controls.docTypeCtrl.updateValueAndValidity();
  }

  setValidators = () => {
    this.documentForm.controls.docTypeCtrl.setValidators([CustomValidators.dropdownRequired()]);
    this.documentForm.controls.docFileCtrl.setValidators([Validators.required, CustomValidators.fileNameExtn()]);
    this.documentForm.controls.docDescriptionCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);

    this.documentForm.controls.docDescriptionCtrl.updateValueAndValidity();
    this.documentForm.controls.docFileCtrl.updateValueAndValidity();
    this.documentForm.controls.docTypeCtrl.updateValueAndValidity();
  }

  showValidationErrors = () => {
    if (this.formErrors.docTypeCtrl) {
      this.toastr.error(this.formErrors.docTypeCtrl.toString());
    } else if (this.formErrors.docFileCtrl) {
      this.toastr.error(this.formErrors.docFileCtrl.toString());
    } else if (this.formErrors.docDescriptionCtrl) {
      this.toastr.error(this.formErrors.docDescriptionCtrl.toString());
    }
  }

  logValidationErrors = (group: FormGroup = this.documentForm): void => {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      } else {
        this.formErrors[key] = '';
        if (
          abstractControl &&
          !abstractControl.valid &&
          (abstractControl.touched || abstractControl.dirty)
        ) {
          const messages = this.validationMessages[key];
          for (const errorKey in abstractControl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + ' ';
            }
          }
        }
      }
    });
  }

  onChange() {
    this.onValueChange.emit('Input change');
  }
  private markFormGroupTouched = (formGroup: FormGroup) => {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  ngOnDestroy = () => {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.ClientApplicationServiceSub) {
      this.ClientApplicationServiceSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.docServiceSub) {
      this.docServiceSub.unsubscribe();
    }
    if (this.docFormSub) {
      this.docFormSub.unsubscribe();
    }
    if (this.vcsReferralIDObservableSub) {
      this.vcsReferralIDObservableSub.unsubscribe();
    }

  }

}
