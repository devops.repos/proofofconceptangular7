import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PACTDocument } from './document.model';
import { CommonService } from 'src/app/services/helper-services/common.service';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: "prior-doc-action",
    template: `<mat-icon  matTooltip='View Document' (click)="onPriorDocView()" class="doc-action-icon" color="primary">find_in_page</mat-icon>
  <mat-icon (click)="onPriorDocSave()" matTooltip='Attach Document to current application' class="doc-action-icon" color="primary">navigation</mat-icon>`,
    styles: [
        `
      .doc-action-icon {
        cursor: pointer;
      }
    `
    ],
})
export class PriorDocActionComponent implements ICellRendererAngularComp {
    private params: any;
    public cell: any;
    private docSelected: PACTDocument;

    constructor(
        private commonService: CommonService
    ) { }

    agInit(params: any): void {
        this.params = params;
        this.cell = { row: params.node.data, col: params.colDef.headerName };
    }

    onPriorDocView() {
        this.docSelected = {
            pactDocumentID: this.params.data.pactDocumentID,
            docExtension: this.params.data.docExtension,
            docType: this.params.data.docType,
            fileNetDocID: this.params.data.fileNetDocID,
            capsReportID: this.params.data.capsReportID
        };
        this.commonService.setIsOverlay(true);
        this.params.context.componentParent.priorDocViewParent(this.docSelected);
    }

    onPriorDocSave() {
        this.docSelected = {
            pactDocumentID: this.params.data.pactDocumentID,
            docExtension: this.params.data.docExtension,
            createdBy: this.params.data.createdBy,
            fileNetDocID: this.params.data.fileNetDocID,
            pactApplicationID: this.params.data.pactApplicationID,
            docType: this.params.data.docType
        };
        this.params.context.componentParent.priorDocSaveParent(this.docSelected);
    }

    refresh(): boolean {
        return false;
    }

}
