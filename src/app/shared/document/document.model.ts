export interface PACTDocument {
  pactApplicationID?: number;
  vcsReferralID?: number;
  pactDocumentID?: number;
  docType?: number;
  fileNetDocID?: number;
  pactFile?: File;
  docName?: string;
  docExtension?: string;
  docDescription?: string;
  docTypeDescription?: string;
  createdBy?: string;
  createdByID?: number;
  attachedBy?: string;
  attachedDate?: string;
  attachedTime?: string;
  agencyNo?: string;
  siteNo?: string;
  capsReportID?: string;
  deleteDisabled?: boolean;
  clinicianTitleType?: number;
  clinicianTitle?: string;
  clinicianName?: string;
  evaluationDate?: string;
  licenseNo?: string;
  collaboratingMD?: string;
  collaboratingClinicianTitleType? : number;
  collaboratingClinicianTitle?: string;
  collaboratingClinicianLicenseNo? : string;
}

export interface VCSAgencySiteDocument {
  pactFile: File;
  docName: string;
  docExtension: string;
  userID: number;
  userName: string;
  userEmail: string;
  agencyReqID: number;
  siteReqID: number;
  siteRequestType: number;
}
