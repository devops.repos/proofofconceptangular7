import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType,
  HttpHeaders,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { PACTDocument } from 'src/app/shared/document/document.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  SERVER_URL = environment.pactApiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  public getDocumentList = (documentData: PACTDocument) => {
    const getDocumentListURL = `${this.SERVER_URL}Document/GetDocumentList`;
    //  console.log(JSON.stringify(documentData));
    return this.httpClient.post(
      getDocumentListURL,
      JSON.stringify(documentData),
      this.httpOptions
    );
  }

  //  public saveDocument = (documentData: PACTDocument) => {
  public saveDocument = (formData: FormData) => {
    const saveDocumentURL = `${this.SERVER_URL}Document/SaveDocument`;

    return this.httpClient.post(
      saveDocumentURL,
      //  documentData,
      formData,
      {
        reportProgress: true,
        observe: 'events'
      }
    );
  }
  public viewDocument(fileName: string): Observable<HttpEvent<Blob>> {
    // console.log(fileName);
    const viewDocumentURL = `${this.SERVER_URL}Document/ViewDocument`;
    return this.httpClient.request(new HttpRequest(
      'GET',
      `${viewDocumentURL}?FileName=${fileName}`,
      null,
      {
        reportProgress: true,
        responseType: 'blob'
      }));
  }

  public deleteDocument = (documentData: PACTDocument) => {
    const deleteDocumentURL = `${this.SERVER_URL}Document/DeleteDocument`;
    return this.httpClient.post(
      deleteDocumentURL,
      JSON.stringify(documentData),
      this.httpOptions
    );
  }

  public getPriorDocumentList = (pactApplicationID: number) => {
    const getPriorDocumentListURL = `${this.SERVER_URL}Document/GetPriorHousingDocList`;
    return this.httpClient.post(
      getPriorDocumentListURL,
      pactApplicationID,
      this.httpOptions
    );
  }

  public savePriorHousingDocument = (documentData: PACTDocument) => {
    const savePriorDocumentURL = `${this.SERVER_URL}Document/SavePriorHousingDocument`;
    return this.httpClient.post(
      savePriorDocumentURL,
      JSON.stringify(documentData),
      this.httpOptions
    );
  }

  public getFileNetDocumentURL(fileNetDocID: number): Observable<any> {
    const getFileNetDocURL = `${this.SERVER_URL}Document/GetDocumentLink`;
    if (fileNetDocID) {
      return this.httpClient.post(getFileNetDocURL, JSON.stringify(fileNetDocID), this.httpOptions);
    }
  }


}
