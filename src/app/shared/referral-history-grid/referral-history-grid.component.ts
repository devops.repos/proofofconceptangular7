import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { GridOptions } from 'ag-grid-community';

export interface iReferralHistory {
  vcsReferralID: number;
  pactApplicationID: number;
  clientID: number;
  referralDate: string;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  approvalExpiryDate: string;
  referringAgencyID: number;
  referringAgencyNo: string;
  referringSiteID: number;
  referringSiteNo: string;
  referringAgencyName: string;
  referringSiteName: string;
  hpAgencyID: number;
  hpAgencyNo: string;
  hpSiteID: number;
  hpSiteNo: string;
  hpAgencyName: string;
  hpSiteName: string;
  ssn: string;
  clientEligibleFor: string;
  svaPrioritization: string;
  serviceNeeds: string;
  placementCriteria: string;
  primaryServiceContractID: number;
  primaryServiceContractName: string;
  agreementPopulationID: number;
  populationName: string;
  siteAgreementPopulationID: number;
  agreementTypeDescription: string;
  siteLocationType: number;
  siteLocationTypeDescription: string;
  siteAddress: string;
  unitsOccupied: number;
  unitsAvailable: number;
  totalUnits: number;
  pendingReferrals: number;
  referralStatusType: number;
  referralStatusTypeDescription: string;
  referralType: number;
  unitType: number;
  unitTypeDescription: string;
  matchPercentage: number;
  withdrawnReasonType: string;
  referralClosedComment: string;
  interviewDate: string;
  interviewTime: string;
  interviewLocation: string;
  canHPScheduleInterview: boolean;
  referredBy: string;
  referredDate: string;
  updatedBy: number;
  updatedDate: string;
}

@Component({
  selector: 'app-referral-history-grid',
  templateUrl: './referral-history-grid.component.html',
  styleUrls: ['./referral-history-grid.component.scss']
})

export class ReferralHistoryGridComponent implements OnInit {
  @Input() pactApplicationId: number;

  //Variables
  fullName: string;
  columnDefs = [];
  rowData: [];
  defaultColDef = {};
  gridOptions: GridOptions;
  overlayNoRowsTemplate: string;

  constructor(private httpClient: HttpClient) {
    //Grid Column Definitions
    this.columnDefs = [
      { headerName: 'Client Number', field: 'clientID', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Referral Date', field: 'referralDate', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Eligibility', field: 'clientEligibleFor', filter: 'agTextColumnFilter', width: 250 },
      { headerName: 'Prioritization', field: 'svaPrioritization', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Service Needs', field: 'serviceNeeds', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Placement Criteria', field: 'placementCriteria', filter: 'agTextColumnFilter', width: 250 },
      { headerName: 'Housing Agency', field: 'hpAgencyNo', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Housing Site', field: 'hpSiteNo', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Unit Type', field: 'unitTypeDescription', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Interview Date', field: 'interviewDate', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Interview Time', field: 'interviewTime', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Interview Location', field: 'interviewLocation', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Referred By', field: 'referredBy', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Referred Date', field: 'referredDate', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Status', field: 'referralStatusTypeDescription', filter: 'agTextColumnFilter', width: 200 }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  ngOnInit() {
  }

  referralHistoryURL = environment.pactApiUrl + 'VCSReferralRoster/GetVCSReferralRosterListByPactApplicationID';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  //Get referral history - Api call
  getReferralHistory(pactApplicationId: number): Observable<any> {
    if (pactApplicationId) {
      return this.httpClient.post(this.referralHistoryURL, JSON.stringify(pactApplicationId), this.httpOptions);
    }
  }

  //OnGrid Ready
  onGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.overlayNoRowsTemplate = '<span style="color: #337ab7">No Referral History To Show</span>';
    this.populateGrid();
  }

  //Populate Grid
  populateGrid() {
    if (this.pactApplicationId) {
      this.getReferralHistory(this.pactApplicationId)
        .subscribe(res => {
          if (res) {
            const data = res as iReferralHistory;
            if (data) {
              this.rowData = res;
            }
            else {
              this.rowData = null;
            }
          }
        });
    }
    else {
      this.rowData = null;
    }
  }
}
