import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralHistoryGridComponent } from './referral-history-grid.component';

describe('ReferralHistoryGridComponent', () => {
  let component: ReferralHistoryGridComponent;
  let fixture: ComponentFixture<ReferralHistoryGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralHistoryGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralHistoryGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
