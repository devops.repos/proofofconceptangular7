import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InformationResourceService {

  private apiURL = environment.pactApiUrl + 'InformationResource';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http : HttpClient) { }

  getInformationResource(informationType: number) {
    /* InformationType: 944 = Training, 945 = Announcement */
    return this.http.post(this.apiURL + '/GetInformationResources', JSON.stringify(informationType), this.httpOptions);
  }
}
