import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { IInformationResource } from '../information-resource.model';
import { InformationResourceService } from '../information-resource.service';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit, OnDestroy {

  trainingResources: IInformationResource[];

  trainingResourcesSub: Subscription;

  constructor(
    private informationResourceService: InformationResourceService,
    private commonService: CommonService
  ) { }

  ngOnInit() {
    this.trainingResourcesSub = this.informationResourceService.getInformationResource(944).subscribe((res: IInformationResource[]) => {
      if (res) {
        this.trainingResources = res;
        // console.log(this.trainingResources);
      }
    })

  }

  onResourceClicked(r: IInformationResource) {
    if (r.url) {
      this.commonService.OpenWindow(r.url);
    }

  }

  ngOnDestroy(): void {
    if (this.trainingResourcesSub) {
      this.trainingResourcesSub.unsubscribe();
    }

  }

}
