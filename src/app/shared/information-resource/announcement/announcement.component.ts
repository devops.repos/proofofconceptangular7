import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { IInformationResource } from '../information-resource.model';
import { InformationResourceService } from '../information-resource.service';

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss']
})
export class AnnouncementComponent implements OnInit, OnDestroy {

  announcementResources: IInformationResource[];

  announcementResourcesSub: Subscription;

  constructor(
    private informationResourceService: InformationResourceService,
    private commonService: CommonService
  ) { }

  ngOnInit() {
    this.announcementResourcesSub = this.informationResourceService.getInformationResource(945).subscribe((res: IInformationResource[]) => {
      if (res) {
        this.announcementResources = res;
        // console.log(this.announcementResources);
      }
    })

  }

  onResourceClicked(r: IInformationResource) {
    if (r.url) {
      this.commonService.OpenWindow(r.url);
    }

  }

  ngOnDestroy(): void {
    if (this.announcementResourcesSub) {
      this.announcementResourcesSub.unsubscribe();
    }

  }

}
