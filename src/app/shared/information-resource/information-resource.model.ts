export interface IInformationResource {
  description: string;
  url: string;
  informationType: number;
  resourceGroupType: number;
  seqNo: number;
}
