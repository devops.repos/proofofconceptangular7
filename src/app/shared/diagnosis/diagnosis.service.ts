import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

//Model References
import { PACTClinicalMHRDiagnosisDetailsInput } from 'src/app/shared/diagnosis/diagnosis.model';


@Injectable({
  providedIn: 'root'
})
export class DiagnosisService {
  //Save Urls
  savePACTClinicalMHRDiagnosisDetailsUrl = environment.pactApiUrl + 'Diagnosis/SavePACTClinicalMHRDiagnosisDetails';
  
  //Get Urls
  getGetPACTClinicalMHRDiagnosisDetailsUrl = environment.pactApiUrl + 'Diagnosis/GetPACTClinicalMHRDiagnosisDetails';
 
  //Delete Urls
  deletePACTClinicalMHRDiagnosisDetailsUrl = environment.pactApiUrl + 'Diagnosis/DeletePACTClinicalMHRDiagnosisDetails';
    
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),
    response : "json",
  };
  
  constructor(private httpClient: HttpClient) { }

  //Save Methods
  savePACTClinicalMHRDiagnosisDetails(pactClinicalMHRDiagnosisDetailsInput : PACTClinicalMHRDiagnosisDetailsInput) : Observable<any>
  {
    return this.httpClient.post(this.savePACTClinicalMHRDiagnosisDetailsUrl, JSON.stringify(pactClinicalMHRDiagnosisDetailsInput), this.httpOptions);
  }

  //Get Methods
  getGetPACTClinicalMHRDiagnosisDetails<T>(pactApplicationID : number) : Observable<T>
  {
     return this.httpClient.post<T>(this.getGetPACTClinicalMHRDiagnosisDetailsUrl, JSON.stringify(pactApplicationID), this.httpOptions);
  }

  //Delete Methods
  deletePACTClinicalMHRDiagnosisDetails(pactClinicalMHRDiagnosisDetailsInput : PACTClinicalMHRDiagnosisDetailsInput) : Observable<any>
  {
    return this.httpClient.post(this.deletePACTClinicalMHRDiagnosisDetailsUrl, JSON.stringify(pactClinicalMHRDiagnosisDetailsInput), this.httpOptions);
  }

}

