import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PACTClinicalMHRDiagnosisDetails } from './diagnosis.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "diagnosis-action",
  template: `<mat-icon (click)="onDelete()" matTooltip='Delete Diagnosis' color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .diagnosis-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class DiagnosisActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private selectedDiagnosis: PACTClinicalMHRDiagnosisDetails;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onDelete() {
    this.selectedDiagnosis = this.params.data;
    // this.selectedDiagnosis.isActive = false;
    // console.log(this.selectedDiagnosis);
    this.params.context.componentParent.onDeleteDiagnosis(this.selectedDiagnosis);
    
  }

  refresh(): boolean {
    return false;
  }
}
