// export interface DiagnosisDetails{
//     pactClinicalMHRDiagnosisDetailsID? : number;
//     pactClinicalMHRDiagnosisID? : number;
//     diagnosisType?: string;
//     diagnosisTypeID?: number;
//     diagnosisID?: number;
//     code? : string;
//     description? : string;
//     riskOf? : boolean;
//     historyOf? : boolean;
//     provisional? : boolean;
//     isActive? : boolean;
// }

// export interface Diagnosis{
//     id: number;
//     userId?: number;
//     diagnosisDetails : DiagnosisDetails
// }

export interface PACTClinicalMHRDiagnosisDetails{
    pactClinicalMHRDiagnosisDetailsID? : number;
    pactClinicalMHRDiagnosisID? : number;
    diagnosisType?: string;
    diagnosisTypeID?: number;
    diagnosisID?: number;
    code? : string;
    description? : string;
    riskOf? : boolean;
    historyOf? : boolean;
    provisional? : boolean;
    isActive? : boolean;
}

export interface PACTClinicalMHRDiagnosisDetailsInput{
    pactApplicationID: number;
    pactClinicalMHRDiagnosisDetails : PACTClinicalMHRDiagnosisDetails;
    userID?: number;
}
