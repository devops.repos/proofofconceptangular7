import { Component, OnInit, OnDestroy, Input,Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { PACTClinicalMHRDiagnosisDetails } from './diagnosis.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { GridOptions } from 'ag-grid-community';
import { DiagnosisActionComponent } from './diagnosis-action';
import { AgGridAngular } from 'ag-grid-angular';
import { ToastrService } from 'ngx-toastr';
import 'ag-grid-enterprise';

@Component({
  selector: 'app-diagnosis',
  templateUrl: './diagnosis.component.html',
  styleUrls: ['./diagnosis.component.scss']
})
export class DiagnosisComponent implements OnInit, OnDestroy {

  @Input('name')  titleName : string;
  @Input()  description : string;
  @Input()  hasDiagnosis : boolean;
  @Input() options : PACTClinicalMHRDiagnosisDetails;
  @Input('diagnosis') diagnosisLst : PACTClinicalMHRDiagnosisDetails[];
  @Input() allDiagnosisList : PACTClinicalMHRDiagnosisDetails[];
  @Output()
  addDiagnosis : EventEmitter<PACTClinicalMHRDiagnosisDetails> = new EventEmitter<PACTClinicalMHRDiagnosisDetails>();
  @Output()
  removeDiagnosis : EventEmitter<PACTClinicalMHRDiagnosisDetails> = new EventEmitter<PACTClinicalMHRDiagnosisDetails>();
  @Output()
  diagnosisChanged : EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  public gridOptions: GridOptions;
  rowData: PACTClinicalMHRDiagnosisDetails[];
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;
  
  context: any;
  diagnosisGroup: FormGroup;
  constructor (
              private formBuilder: FormBuilder,
              private confirmDialogService : ConfirmDialogService,
              private toastr: ToastrService,
              private ref: ChangeDetectorRef) 
              {
                this.diagnosisGroup = this.formBuilder.group({
                  searchDiagnosisCtrl : [''],
                  selectedDiagnosisCtrl : [''],
                  riskOfCtrl1: this.formBuilder.array([this.formBuilder.control('')]),
                  riskOfCtrl : [''],
                  historyOfCtrl : [''],
                  provisionalCtrl : [''],
                  hasRiskOfCtrl: [''],
                  hasHistoryOfCtrl : [''],
                  hasProvisionalCtrl : ['']
                }); 

                this.gridOptions = {
                  rowHeight: 30
                } as GridOptions;
                
                this.columnDefs = [
                  {
                    headerName: 'Diagnosis Details ID',
                    field: 'pactClinicalMHRDiagnosisDetailsID',
                    hide : true
                  },
                  {
                    headerName: 'Diagnosis Id',
                    field: 'diagnosisID',
                    hide : true
                  },
                  {
                    headerName: ' Diagnoses',
                    field: 'description',
                    width: 800,
                    filter: 'agTextColumnFilter',
                    sortable: false,
                    resizable: false,
                    suppressMenu: true,
                    autoHeight:true,
                    cellStyle: {
                      'white-space': 'normal'
                    },
                    
                  },
                  {
                    headerName: 'R/O',
                    field: 'riskOf',
                    width: 75,
                    filter: false,
                    sortable: false,
                    resizable: false,
                    suppressMenu: true,
                    hide : false,
                    cellRenderer: (params: { value: boolean }) => {
                      return params.value === true ? 'Yes' : 'No';    
                    },
                    
                  },
                  {
                    headerName: 'H/O',
                    field: 'historyOf',
                    width: 75,
                    filter: false,
                    sortable: false,
                    resizable: false,
                    suppressMenu: true,
                    hide : false,
                    cellRenderer: (params: { value: boolean }) => {
                      return params.value === true ? 'Yes' : 'No';    
                    },
                    
                  },
                  {
                    headerName: 'P/V',
                    field: 'provisional',
                    filter: false,
                    sortable: false,
                    resizable: false,
                    suppressMenu: true,
                    width: 75,
                    hide : false,
                    cellRenderer: (params: { value: boolean }) => {
                      return params.value === true ? 'Yes' : 'No';    
                    },
                    
                  },
                  
                  {
                    headerName: 'Actions',
                    field: 'action',
                    width: 65,
                    filter: false,
                    sortable: false,
                    resizable: false,
                    pinned: 'left',
                    suppressMenu: true,
                    suppressSizeToFit: true,
                    cellRenderer: 'actionRenderer',
                  }
                ];
                
                this.defaultColDef = {
                  filter: true,
                };
                this.pagination = true;
                this.context = { componentParent: this };
                this.frameworkComponents = {
                 actionRenderer: DiagnosisActionComponent
                };
                

              }
              
    ngOnDestroy () {
    }
    hasRiskOf : boolean;
    hasHistoryOf : boolean;
    hasProvisional : boolean;

    ngOnChanges() {
      this.refreshDiagnosisList();
      if (this.hasDiagnosis) {
        this.diagnosisGroup.disable();
      } else {
          this.diagnosisGroup.enable();
      }
    }
    ngOnInit() {
      this.refreshDiagnosisList();
      if (this.hasDiagnosis) {
        this.diagnosisGroup.disable();
      } else {
          this.diagnosisGroup.enable();
      }



    if(this.options.diagnosisTypeID === 48) {
      this.keywordPlaceholder = 'Search Medical Diagnoses...';
      this.tableHeader ='Medical';
    }
    else if (this.options.diagnosisTypeID === 49){      
      this.keywordPlaceholder = 'Search Psychiatric Diagnoses...';
      this.tableHeader ='Psychiatric';
    }
    else{      
      this.keywordPlaceholder = 'Search Diagnoses...';
      this.tableHeader ='Other';
    }
  }
    
  prvnl : boolean = true;
  diagnosisList : PACTClinicalMHRDiagnosisDetails[];
  keywordPlaceholder : string;
  tableHeader : string;
  newItem : PACTClinicalMHRDiagnosisDetails;

  onFirstDataRendered(params) {
    params.api.setDomLayout('autoHeight');
    params.api.sizeColumnsToFit();
  }

  onGridReady = (params: { api: any; columnApi: any }) => {
    this.refreshDiagnosisList();
    
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the diagnosis loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Diagnosis Available</span>';
    
    params.columnApi.getColumn('description').colDef.headerName= this.tableHeader + ' '+ this.gridOptions.columnDefs[1].headerName;
    
    this.gridOptions.api.refreshHeader();
    this.gridOptions.columnApi.setColumnVisible('riskOf', this.options.riskOf) //In that case we hide it
    this.gridOptions.columnApi.setColumnVisible('historyOf', this.options.historyOf) //In that case we hide it
    this.gridOptions.columnApi.setColumnVisible('provisional', this.options.provisional) //In that case we hide it
    params.api.setDomLayout('autoHeight');
    params.api.sizeColumnsToFit();
    
  }

  refreshDiagnosisList () {    
    if (this.diagnosisLst != undefined) {
      this.rowData = this.diagnosisLst.filter(x => x.isActive === true);
    }
  }

  itemSelected (itm : PACTClinicalMHRDiagnosisDetails) {
    this.newItem = {
              pactClinicalMHRDiagnosisDetailsID : itm.pactClinicalMHRDiagnosisDetailsID,
              pactClinicalMHRDiagnosisID : itm.pactClinicalMHRDiagnosisID,
              diagnosisType : itm.diagnosisType,
              diagnosisID: itm.diagnosisID,
              code : itm.code,
              description : itm.description,
              riskOf : itm.riskOf,
              historyOf : itm.historyOf,
              provisional : itm.provisional,
              isActive : itm.isActive      
            } 
    this.diagnosisGroup.get("selectedDiagnosisCtrl").setValue(itm.description);
    this.diagnosisGroup.get("searchDiagnosisCtrl").setValue('');
    
    this.diagnosisGroup.get("hasRiskOfCtrl").setValue(false);
    this.diagnosisGroup.get("hasHistoryOfCtrl").setValue(false);
    this.diagnosisGroup.get("hasProvisionalCtrl").setValue(false);
  }
    
  onChange ($event) {
    this.hasDiagnosis = $event.checked;
    if (this.diagnosisLst !== undefined && this.diagnosisLst.filter(x => x.isActive === true).length > 0 && this.hasDiagnosis) {
          const title = 'Confirm Change';
          const primaryMessage = 'All the selected ' + this.tableHeader +' Diagnosis will be deleted.';
          const secondaryMessage = 'Are you sure want to continue?';
          const confirmButtonName = 'Yes';
          const dismissButtonName = 'No';
      
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            (positiveResponse) => {          
                if (this.hasDiagnosis) {                  
                  this.diagnosisGroup.get("selectedDiagnosisCtrl").setValue('');
                  this.diagnosisGroup.get("hasRiskOfCtrl").setValue(false);
                  this.diagnosisGroup.get("hasHistoryOfCtrl").setValue(false);
                  this.diagnosisGroup.get("hasProvisionalCtrl").setValue(false);

                  this.diagnosisGroup.disable();
                }
                else{
                  this.diagnosisGroup.enable();
                }    
                this.diagnosisChanged.emit({'diagnosisID':this.options.diagnosisTypeID, 'value': this.hasDiagnosis});
            },
            (negativeResponse) => {
              this.hasDiagnosis = false;
              this.diagnosisGroup.enable();
            },
          );
    } 
    else {
      if (this.hasDiagnosis) {
        this.diagnosisGroup.disable();
      } else{      
          this.diagnosisGroup.enable();
      }
      this.diagnosisChanged.emit({'diagnosisID':this.options.diagnosisTypeID, 'value': this.hasDiagnosis});
    }
  }

  onAddDiagnosis () {
    if(this.diagnosisGroup.get('selectedDiagnosisCtrl').value !== undefined && this.diagnosisGroup.get('selectedDiagnosisCtrl').value.trim().length > 5 && !this.hasDiagnosis) {
        this.newItem.riskOf = this.diagnosisGroup.get('hasRiskOfCtrl').value == true;
        this.newItem.historyOf = this.diagnosisGroup.get('hasHistoryOfCtrl').value == true;
        this.newItem.provisional = this.diagnosisGroup.get('hasProvisionalCtrl').value == true;
        this.newItem.isActive = true;
        this.newItem.diagnosisTypeID = this.options.diagnosisTypeID;

        if (this.diagnosisLst.filter(x => x.diagnosisID === this.newItem.diagnosisID && x.isActive === true).length > 0){
          this.toastr.warning('Warning!', 'Selected item already in the list.');
          return;
        }
        this.addDiagnosis.emit(this.newItem);
        this.diagnosisGroup.get("selectedDiagnosisCtrl").setValue('');
        this.diagnosisGroup.get("hasRiskOfCtrl").setValue(false);
        this.diagnosisGroup.get("hasHistoryOfCtrl").setValue(false);
        this.diagnosisGroup.get("hasProvisionalCtrl").setValue(false);
    }
    else{      
      this.hasDiagnosis ? '' : this.toastr.warning('Warning!', 'Select one diagnosis from the search list.') ;
    }
  }  
  
  onDeleteDiagnosis (item : PACTClinicalMHRDiagnosisDetails) {
    const title = 'Confirm Delete';
    const primaryMessage = 'Diagnosis will be deleted permanently from the list.';
    const secondaryMessage = 'Are you sure want to delete?';
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      (positiveResponse) => {
        //console.log('hasDiagnosis', this.hasDiagnosis);
        if(this.hasDiagnosis === false) {
          //console.log('item ', item);
          this.removeDiagnosis.emit(item);
        }
      },
      (negativeResponse) => {console.log(negativeResponse)},
    );
  } 

  GetDiagnosisListByKey () :void {    
    //this.diagnosisList = this.allDiagnosisList.filter(x => x.diagnosisType === this.tableHeader && x.description.toLowerCase().indexOf(this.diagnosisGroup.get('searchDiagnosisCtrl').value.toLowerCase()) !== -1);
    this.diagnosisList = this.allDiagnosisList.filter(x => x.diagnosisType === this.tableHeader && x.description.toLowerCase().indexOf(this.diagnosisGroup.get('searchDiagnosisCtrl').value.toLowerCase()) === 0);
  }

  trackByItem (index : Number, item : PACTClinicalMHRDiagnosisDetails){
    return item.diagnosisID;
  }
}
