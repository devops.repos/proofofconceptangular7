import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import "ag-grid-enterprise";
import { ToastrModule } from 'ngx-toastr';

import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { DiagnosisComponent } from './diagnosis.component';
import { DiagnosisActionComponent } from './diagnosis-action';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      progressBar: true,
      progressAnimation: 'increasing'
    }),
    AgGridModule.withComponents([
        DiagnosisActionComponent
    ])
  ],
  declarations: [
    DiagnosisComponent,
    DiagnosisActionComponent
  ],
  exports: [
    DiagnosisComponent
  ]
})
export class DiagnosisModule { }
