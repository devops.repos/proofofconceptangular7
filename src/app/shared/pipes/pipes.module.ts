import { NgModule } from '@angular/core';
import { NoSanitizePipe } from './no-sanitize.pipe';

@NgModule({
    declarations: [ NoSanitizePipe],
    exports: [NoSanitizePipe]
})

export class PipesModule {
}
