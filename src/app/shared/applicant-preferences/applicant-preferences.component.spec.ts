import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantPreferencesComponent } from './applicant-preferences.component';

describe('ApplicantPreferencesComponent', () => {
  let component: ApplicantPreferencesComponent;
  let fixture: ComponentFixture<ApplicantPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantPreferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
