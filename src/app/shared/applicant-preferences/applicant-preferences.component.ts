import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CustomValidators } from '../custom-validator/custom.validators';
import { ApplicantPreference } from './applicant-preferences.model';

@Component({
  selector: 'app-applicant-preferences',
  templateUrl: './applicant-preferences.component.html',
  styleUrls: ['./applicant-preferences.component.scss']
})
export class ApplicantPreferencesComponent implements OnInit {

  @Input()  preference : ApplicantPreference;
  @Input()  boroughOptions : RefGroupDetails[];
  @Input()  apartmentOptions : RefGroupDetails[];
  //@Input()  residenceOptions = null;
  @Input()  appPrefFlag = null;
  @Input()  parentPageID = 1; // 1-PACT, 2:VCS
  @Output()
  onValueChange : EventEmitter<ApplicantPreference> = new EventEmitter<ApplicantPreference>();
  
  boroughGroup : FormGroup;
  apartmentGroup : FormGroup;
  housingGroup : FormGroup;

  constructor(
    private formBuilder: FormBuilder) {
      
      this.boroughGroup = this.formBuilder.group({
        preferenceOneCtrl: [''],
        preferenceTwoCtrl: [''],
        excludeBoroughCtrl: [''],
        serviceLocatedCtrl: [''],
        excludeCommentsCtrl: ['', Validators.compose([CustomValidators.requiredCharLength(2), this.dynamicRequiredValidator()])] 
      });

      this.apartmentGroup = this.formBuilder.group({
        question1Ctrl: [''],
        question2Ctrl: [''],
        question3Ctrl: [''],
        question4Ctrl: [''],
        question5Ctrl: [''],
        question6Ctrl: [''],
        question7Ctrl: [''],
        question8Ctrl: [''],
        question1CommentsCtrl: ['', Validators.compose([CustomValidators.requiredCharLength(2), this.dynamicRequiredValidator()])] ,        
        question8CommentsCtrl: ['', Validators.compose([CustomValidators.requiredCharLength(2), this.dynamicRequiredValidator()])] 
      });

      /* this.housingGroup = this.formBuilder.group({
        question9Ctrl: [''],
        question10Ctrl: [''],
        question10CommentsCtrl: ['',[CustomValidators.requiredCharLength(2)]], 
      }); */
   }

  serviceLocatedOptions : RefGroupDetails[];
  apartmentOptions1 : RefGroupDetails[];
  showExcludeComments : boolean
  showAptShareComments : boolean
  showOtheComments : boolean
  showHousingOtheComments: boolean;
 
  ngOnChanges() {
    if (this.preference  != undefined){
      this.boroughGroup.setValue({
        preferenceOneCtrl : this.preference.preference1,
        preferenceTwoCtrl : this.preference.preference2,
        excludeBoroughCtrl : this.preference.excludePreference,
        excludeCommentsCtrl : this.preference.excludeComments,
        serviceLocatedCtrl : this.preference.serviceLocated
      });
  
      this.apartmentGroup.setValue({
        question1Ctrl:this.preference.aptQuestion1,
        question1CommentsCtrl:this.preference.aptQuestion1Comment,
        question2Ctrl:this.preference.aptQuestion2,
        question3Ctrl:this.preference.aptQuestion3,
        question4Ctrl:this.preference.aptQuestion4,
        question5Ctrl:this.preference.aptQuestion5,
        question6Ctrl:this.preference.aptQuestion6,
        question7Ctrl:this.preference.aptQuestion7,
        question8Ctrl:this.preference.aptQuestion8,
        question8CommentsCtrl:this.preference.aptQuestion8Comment
      });

      // if(this.parentPageID == 2){
      //   this.housingGroup.setValue({
      //     question9Ctrl: this.preference.aptQuestion9,
      //     question10Ctrl: this.preference.aptQuestion10,
      //     question10CommentsCtrl: this.preference.aptQuestion10Comment
      //   });
      // }
  
      this.showExcludeComments = this.preference.excludePreference != null && this.preference.excludePreference != 0 &&  this.preference.excludePreference != 503 
      this.showAptShareComments = this.preference.aptQuestion1 == 34
      this.showOtheComments = this.preference.aptQuestion8 == 33
      //this.showHousingOtheComments = this.preference.aptQuestion10 == 33
    }

    if (this.boroughOptions  != undefined) {
      this.serviceLocatedOptions = this.boroughOptions.filter(x => x.refGroupDetailID != 503)
    }
    if (this.apartmentOptions  != undefined){
      this.apartmentOptions1 = this.apartmentOptions.filter(x => x.refGroupDetailID != 498)
    }
  }
  ngOnInit() {    
    // console.log(this.boroughOptions);
    // console.log(this.apartmentOptions);
    // console.log(this.preference);
    //console.log(this.parentPageID);
  }

  public dynamicRequiredValidator(){
    return (control: AbstractControl): { [key: string]: any } | null => {
    if(this.parentPageID == 2) {
      if(!control.value){
        return {required: true};
      }
    }    
    return null;
  }
}

  validateSelection (id : number,selected : number): void {

    if(id == 1){
      if (this.boroughGroup.get('preferenceTwoCtrl').value == selected && selected != 503){
        this.boroughGroup.get('preferenceTwoCtrl').setValue(0);
        this.preference.preference2 = 0;
      }
      else if (this.boroughGroup.get('excludeBoroughCtrl').value == selected && selected != 503){
        this.boroughGroup.get('excludeBoroughCtrl').setValue(0);
        this.preference.excludePreference = 0;
        this.preference.excludeComments ='';
        this.boroughGroup.get('excludeCommentsCtrl').setValue('');
        this.showExcludeComments = false;
      }
    }
    else if(id == 2){
      if (this.boroughGroup.get('preferenceOneCtrl').value == selected && selected != 503){
        this.boroughGroup.get('preferenceOneCtrl').setValue(0);
        this.preference.preference1 = 0;
      }
      else if (this.boroughGroup.get('excludeBoroughCtrl').value == selected && selected != 503){
        this.boroughGroup.get('excludeBoroughCtrl').setValue(0);
        this.preference.excludePreference = 0;
        this.preference.excludeComments ='';
        this.boroughGroup.get('excludeCommentsCtrl').setValue('');
        this.showExcludeComments = false;
      }
    }
    else if(id == 3 && selected != 503){
      if (this.boroughGroup.get('preferenceTwoCtrl').value == selected){
        this.boroughGroup.get('preferenceTwoCtrl').setValue(0);
        this.preference.preference2 = 0;
      }
      else if (this.boroughGroup.get('preferenceOneCtrl').value == selected){
        this.boroughGroup.get('preferenceOneCtrl').setValue(0);
        this.preference.preference1 = 0;
      }
    }    
  }

  onBoroughChange (id : number, $event) {

    this.validateSelection(id,$event.value);    
      if (id == 1){
        this.preference.preference1 = $event.value;      
      }else if (id == 2){
        this.preference.preference2 = $event.value;
      }else if (id == 3){        
        this.preference.excludePreference = $event.value;
        this.showExcludeComments = this.preference.excludePreference != 503
        if(this.preference.excludePreference == 503){
          this.preference.excludeComments ='';
            this.boroughGroup.get('excludeCommentsCtrl').setValue('');
        }
      }else if (id == 4){
        this.preference.serviceLocated = $event.value;
      }
  
      this.onValueChange.emit(this.preference);

  }

  onApartmentChange (id : number, $event:any) {
    
    if(id == 1){
      this.preference.aptQuestion1 = $event.value;
      this.showAptShareComments = this.preference.aptQuestion1 == 34
      if(this.preference.aptQuestion1 != 34){
        this.apartmentGroup.get('question1CommentsCtrl').setValue('');
        this.preference.aptQuestion1Comment ='';
      }
    }
    else if(id == 2){
      this.preference.aptQuestion2 = $event.value;
    }
    else if(id == 3){
      this.preference.aptQuestion3 = $event.value;
    }
    else if(id == 4){
      this.preference.aptQuestion4 = $event.value;
    }
    else if(id == 5){
      this.preference.aptQuestion5 = $event.value;
    }
    else if(id == 6){
      this.preference.aptQuestion6 = $event.value;
    }
    else if(id == 7){
      this.preference.aptQuestion7 = $event.value;
    }
    else if(id == 8){
      this.preference.aptQuestion8 = $event.value;
      this.showOtheComments = this.preference.aptQuestion8 == 33
      if(this.preference.aptQuestion8 != 33){
        this.apartmentGroup.get('question8CommentsCtrl').setValue('');
        this.preference.aptQuestion8Comment ='';
      }
    }
    /* else if (id == 9){
      this.preference.aptQuestion9 = $event.value;
    }
    else if(id == 10){
      this.preference.aptQuestion10 = $event.value;
      this.showHousingOtheComments = this.preference.aptQuestion10 == 33
      if(this.preference.aptQuestion10 != 33){
        this.housingGroup.get('question10CommentsCtrl').setValue('');
        this.preference.aptQuestion10Comment ='';
      }
    } */
    this.onValueChange.emit(this.preference);
  }
  
  onExcludeCommentsChange () {
    this.preference.excludeComments = this.boroughGroup.get('excludeCommentsCtrl').value;
    this.onValueChange.emit(this.preference);
  }
  
  onSharedCommentsChange () {
    this.preference.aptQuestion1Comment = this.apartmentGroup.get('question1CommentsCtrl').value;
    this.onValueChange.emit(this.preference);
  }
  
  onOtherTextChange () {
    this.preference.aptQuestion8Comment = this.apartmentGroup.get('question8CommentsCtrl').value;
    this.onValueChange.emit(this.preference);
  }

 /*  onHousingOtherTextChange() {
    this.preference.aptQuestion10Comment = this.housingGroup.get('question10CommentsCtrl').value;
    this.onValueChange.emit(this.preference);
  } */
}
