  export interface ApplicantPreference {
    preference1? : number,
    preference2? : number,
    excludePreference? : number,
    excludeComments? : string,
    serviceLocated?: number,
    aptQuestion1? : number,
    aptQuestion1Comment? : string,    
    aptQuestion2? : number,
    aptQuestion3? : number,
    aptQuestion4? : number,
    aptQuestion5? : number,
    aptQuestion6? : number,
    aptQuestion7? : number,
    aptQuestion8? : number,
    aptQuestion8Comment? : string,
   /*  aptQuestion9? : number,
    aptQuestion10? : number,
    aptQuestion10Comment? : string */
    primaryLanguage? : string 
  }