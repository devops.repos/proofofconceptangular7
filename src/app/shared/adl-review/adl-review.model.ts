export class AdlReview {
  detadlReviewID: number;
  pactApplicationID: number;
  personalHygieneType: number;
  personalHygieneDetails: string;
  personalHygieneSourceDescription: string;
  travelMobilityType: number;
  travelMobilityDetails: string;
  travelMobilitySourceDescription: string;
  shoppingAndMealType: number;
  shoppingAndMealDetails: string;
  shoppingAndMealSourceDescription: string;
  managingFinancesType: number;
  managingFinancesDetails: string;
  managingFinancesSourceDescription: string;
  apartmentType: number;
  apartmentDetails: string;
  apartmentSourceDescription: string;
  socialSkillsType: number;
  socialSkillsDetails: string;
  socialSkillsSourceDescription: string;
  healthAndBehavioralType: number;
  healthAndBehavioralDetails: string;
  healthAndBehavioralSourceDescription: string;
  otherType: number;
  otherDetails: string;
  explain: string;
  pactPersonalHygieneType: number;
  pactTravelMobilityType: number;
  pactShoppingAndMealType: number;
  pactManagingFinancesType: number;
  pactApartmentType: number;
  pactSocialSkillsType: number;
  pactHealthAndBehavioralType: number;
  pactSourceDescription: string;
  reviewerSourceDescription: string;
  userID: number;
  parentPage: string;
  isSaveRequired?: boolean;
}
