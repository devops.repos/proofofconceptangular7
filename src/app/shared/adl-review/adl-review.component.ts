import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { AdlReview } from './adl-review.model';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { AdlReviewService } from './adl-review.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

@Component({
  selector: 'app-adl-review',
  templateUrl: './adl-review.component.html',
  styleUrls: ['./adl-review.component.scss']
})
export class AdlReviewComponent implements OnInit, OnDestroy {

  adlFormGrp: FormGroup;
  supportTypes: RefGroupDetails[];
  adlReviewData = new AdlReview();

  @Input() pactApplicationID: number;
  @Input() parentPage = 'VCS'; // VCS, DET(Determination)

  isDisable = true;
  isCommentRequired = false;
  isSaveValid = false;

  commonServiceSub: Subscription;
  adlReviewServiceSub: Subscription;
  adlFormGrpSub: Subscription;

  constructor(
    private fb: FormBuilder,
    private adlReviewService: AdlReviewService,
    private toastr: ToastrService,
    private commonService: CommonService,
    private determinationSideNavService: DeterminationSideNavService
  ) {
  }

  ngOnInit() {
    this.buildFormCtrls();   // build the form controls
    this.loadRefGroupDetails();
    this.getADLReview();

    this.isDisable = this.parentPage.toUpperCase() === 'DET' ? false : true;
  }

  buildFormCtrls() {
    this.adlFormGrp = this.fb.group({
      personHygieneTypeCtrl: ['', CustomValidators.dropdownRequired()],
      travelMobilityTypeCtrl: ['', CustomValidators.dropdownRequired()],
      shoppingAndMealTypeCtrl: ['', CustomValidators.dropdownRequired()],
      manageFinanceTypeCtrl: ['', CustomValidators.dropdownRequired()],
      apartmentTypeCtrl: ['', CustomValidators.dropdownRequired()],
      socialSkillsTypeCtrl: ['', CustomValidators.dropdownRequired()],
      healthAndBehavioralTypeCtrl: ['', CustomValidators.dropdownRequired()],
      otherTypeCtrl: ['', CustomValidators.dropdownRequired()],
      reviewExplainCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
    });
  }

  loadRefGroupDetails = () => {
    const value = '3'; // 3 - SupportType
    this.commonServiceSub = this.commonService.getRefGroupDetails(value).subscribe(res => {
      const data = res.body as RefGroupDetails[];
      this.supportTypes = data.filter(d => d.refGroupID === 3);
    },
      error => {
        if (!this.toastr.currentlyActive) {
          this.toastr.error('Error loading the data. Please try again after sometime.');
        }
      }
    );
  }

  getADLReview = () => {
    if (this.pactApplicationID) {
      this.adlReviewData.pactApplicationID = this.pactApplicationID;
      this.adlReviewData.parentPage = this.parentPage;
      this.adlReviewServiceSub = this.adlReviewService.getADLReview(this.adlReviewData).subscribe(res => {
        this.adlReviewData = res as AdlReview;
        if (this.adlReviewData != null) {
          this.populateForm(this.adlReviewData);
        }
      },
        error => {
          if (!this.toastr.currentlyActive) {
            this.toastr.error('Error loading the data. Please try again after sometime.');
          }
        }
      );
    }
  }

  populateForm(formdata: AdlReview) {
    if (formdata != null) {
      this.adlFormGrp.get('personHygieneTypeCtrl').setValue(formdata.personalHygieneType);
      this.adlFormGrp.get('travelMobilityTypeCtrl').setValue(formdata.travelMobilityType);
      this.adlFormGrp.get('shoppingAndMealTypeCtrl').setValue(formdata.shoppingAndMealType);
      this.adlFormGrp.get('manageFinanceTypeCtrl').setValue(formdata.managingFinancesType);
      this.adlFormGrp.get('apartmentTypeCtrl').setValue(formdata.apartmentType);
      this.adlFormGrp.get('socialSkillsTypeCtrl').setValue(formdata.socialSkillsType);
      this.adlFormGrp.get('healthAndBehavioralTypeCtrl').setValue(formdata.healthAndBehavioralType);
      this.adlFormGrp.get('otherTypeCtrl').setValue(formdata.otherType);
      this.adlFormGrp.get('reviewExplainCtrl').setValue(formdata.explain);

      this.HasSourceTypeChanged();
    }
  }

  onSubmit() {

    if (this.isFormValid()) {
      const isSaveRequired = this.adlFormGrp.dirty ? true : false;
      this.adlReviewData.personalHygieneType = this.adlFormGrp.get('personHygieneTypeCtrl').value;
      this.adlReviewData.travelMobilityType = this.adlFormGrp.get('travelMobilityTypeCtrl').value;
      this.adlReviewData.shoppingAndMealType = this.adlFormGrp.get('shoppingAndMealTypeCtrl').value;
      this.adlReviewData.managingFinancesType = this.adlFormGrp.get('manageFinanceTypeCtrl').value;
      this.adlReviewData.apartmentType = this.adlFormGrp.get('apartmentTypeCtrl').value;
      this.adlReviewData.socialSkillsType = this.adlFormGrp.get('socialSkillsTypeCtrl').value;
      this.adlReviewData.healthAndBehavioralType = this.adlFormGrp.get('healthAndBehavioralTypeCtrl').value;
      this.adlReviewData.otherType = this.adlFormGrp.get('otherTypeCtrl').value;
      this.adlReviewData.explain = this.isCommentRequired ? this.adlFormGrp.get('reviewExplainCtrl').value : null;
      this.adlReviewData.isSaveRequired = isSaveRequired;

      this.adlReviewServiceSub = this.adlReviewService.saveADLReview(this.adlReviewData).subscribe(res => {
        this.adlReviewData = res as AdlReview;
        if (this.adlReviewData != null) {
          this.populateForm(this.adlReviewData);
          if (isSaveRequired) {
            if (!this.toastr.currentlyActive) {
              this.toastr.success('ADL Review has been saved.');
            }
          }
        }
      });
    }
  }

  onSupportTypeChange(type: string) {
    switch (type) {
      case 'PH': {
        this.adlFormGrp.get('personHygieneTypeCtrl').value === this.adlReviewData.pactPersonalHygieneType ?
          this.adlReviewData.personalHygieneSourceDescription = this.adlReviewData.pactSourceDescription :
          (this.adlReviewData.personalHygieneSourceDescription = this.adlReviewData.reviewerSourceDescription,
            this.isCommentRequired = true);
        break;
      }
      case 'TM': {
        this.adlFormGrp.get('travelMobilityTypeCtrl').value === this.adlReviewData.pactTravelMobilityType ?
          this.adlReviewData.travelMobilitySourceDescription = this.adlReviewData.pactSourceDescription :
          (this.adlReviewData.travelMobilitySourceDescription = this.adlReviewData.reviewerSourceDescription,
            this.isCommentRequired = true);
        break;
      }
      case 'SM': {
        this.adlFormGrp.get('shoppingAndMealTypeCtrl').value === this.adlReviewData.pactShoppingAndMealType ?
          this.adlReviewData.shoppingAndMealSourceDescription = this.adlReviewData.pactSourceDescription :
          (this.adlReviewData.shoppingAndMealSourceDescription = this.adlReviewData.reviewerSourceDescription,
            this.isCommentRequired = true);
        break;
      }
      case 'MF': {
        this.adlFormGrp.get('manageFinanceTypeCtrl').value === this.adlReviewData.pactManagingFinancesType ?
          this.adlReviewData.managingFinancesSourceDescription = this.adlReviewData.pactSourceDescription :
          (this.adlReviewData.managingFinancesSourceDescription = this.adlReviewData.reviewerSourceDescription,
            this.isCommentRequired = true);
        break;
      }
      case 'APT': {
        this.adlFormGrp.get('apartmentTypeCtrl').value === this.adlReviewData.pactApartmentType ?
          this.adlReviewData.apartmentSourceDescription = this.adlReviewData.pactSourceDescription :
          (this.adlReviewData.apartmentSourceDescription = this.adlReviewData.reviewerSourceDescription,
            this.isCommentRequired = true);
        break;
      }
      case 'SK': {
        this.adlFormGrp.get('socialSkillsTypeCtrl').value === this.adlReviewData.pactSocialSkillsType ?
          this.adlReviewData.socialSkillsSourceDescription = this.adlReviewData.pactSourceDescription :
          (this.adlReviewData.socialSkillsSourceDescription = this.adlReviewData.reviewerSourceDescription,
            this.isCommentRequired = true);
        break;
      }
      case 'HB': {
        this.adlFormGrp.get('healthAndBehavioralTypeCtrl').value === this.adlReviewData.pactHealthAndBehavioralType ?
          this.adlReviewData.healthAndBehavioralSourceDescription = this.adlReviewData.pactSourceDescription :
          (this.adlReviewData.healthAndBehavioralSourceDescription = this.adlReviewData.reviewerSourceDescription,
            this.isCommentRequired = true);
        break;
      }

    }
    this.HasSourceTypeChanged();
    this.disableNextSideNavigation(true);
  }

  HasSourceTypeChanged() {
    if ((this.adlReviewData.personalHygieneSourceDescription === this.adlReviewData.reviewerSourceDescription) ||
      (this.adlReviewData.travelMobilitySourceDescription === this.adlReviewData.reviewerSourceDescription) ||
      (this.adlReviewData.shoppingAndMealSourceDescription === this.adlReviewData.reviewerSourceDescription) ||
      (this.adlReviewData.managingFinancesSourceDescription === this.adlReviewData.reviewerSourceDescription) ||
      (this.adlReviewData.apartmentSourceDescription === this.adlReviewData.reviewerSourceDescription) ||
      (this.adlReviewData.socialSkillsSourceDescription === this.adlReviewData.reviewerSourceDescription) ||
      (this.adlReviewData.healthAndBehavioralSourceDescription === this.adlReviewData.reviewerSourceDescription)
    ) {
      this.isCommentRequired = true;
    } else {
      this.isCommentRequired = false;
    }
  }

  isFormValid() {
    this.isSaveValid = true;
    this.HasSourceTypeChanged();

    if (this.isCommentRequired) {
      if (!this.adlFormGrp.get('reviewExplainCtrl').value || this.adlFormGrp.get('reviewExplainCtrl').value.trim().length < 2) {
        this.isSaveValid = false;
      }
    }

    if (!this.isSaveValid) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error('Please Explain Changes');
      }
    }

    // if (this.isCommentRequired) {
    //   if (!this.adlFormGrp.get('reviewExplainCtrl').value) {
    //     this.isSaveValid = false;
    //   } else if (this.adlFormGrp.get('reviewExplainCtrl').value.trim().length > 1) {
    //     this.isSaveValid = true;
    //   }
    // } else {
    //   this.isSaveValid = true;
    // }

    return this.isSaveValid;
  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.adlReviewServiceSub) {
      this.adlReviewServiceSub.unsubscribe();
    }
    if (this.adlFormGrpSub) {
      this.adlFormGrpSub.unsubscribe();
    }
  }

  //Disable Next Side Navigation
  disableNextSideNavigation(value: boolean) {
    this.determinationSideNavService.setIsDeterminationSummaryDisabled(value);
    this.determinationSideNavService.setIsSignOffFollowUpDisabled(value);
  }
}
