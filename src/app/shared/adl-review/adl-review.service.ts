import { Injectable } from '@angular/core';
import {
    HttpClient,
    HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { AdlReview } from './adl-review.model';

@Injectable({
    providedIn: 'root'
})
export class AdlReviewService {
    SERVER_URL = environment.pactApiUrl;
    getADLReviewURL = `${this.SERVER_URL}DETSVA/GetADLReview`;
    saveADLReviewURL = `${this.SERVER_URL}DETSVA/SaveADLReview`;

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private httpClient: HttpClient) { }

    getADLReview = (adlReview: AdlReview): Observable<AdlReview> => {
        return this.httpClient.post<AdlReview>(
            this.getADLReviewURL,
            adlReview,
            this.httpOptions
        );
    }

    saveADLReview = (adlReview: AdlReview): Observable<AdlReview> => {
        return this.httpClient.post<AdlReview>(
            this.saveADLReviewURL,
            adlReview,
            this.httpOptions
        );
    }

}
