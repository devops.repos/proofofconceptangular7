import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { AdlReviewComponent } from './adl-review.component';
import { NavFooterModule } from '../nav-footer/nav-footer.module';

@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        MaterialModule,
        ErrorsModule,
        HttpClientModule,
        ReactiveFormsModule,
        NavFooterModule
    ],
    declarations: [
        AdlReviewComponent
    ],
    exports: [
        AdlReviewComponent
    ]
})
export class AdlReviewModule { }
