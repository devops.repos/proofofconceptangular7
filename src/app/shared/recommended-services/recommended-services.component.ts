import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CustomValidators } from '../custom-validator/custom.validators';
import { HousingLevelItem, Items, ApplicantPreference, RecommendedServices } from 'src/app/pact-modules/supportiveHousingSystem/housing-preferences/housing-preferences.model';

@Component({
  selector: 'app-recommended-services',
  templateUrl: './recommended-services.component.html',
  styleUrls: ['./recommended-services.component.scss']
})
export class RecommendedServicesComponent implements OnInit {
  
  @Input()  recommendedServices : RecommendedServices;
  @Input() parentPageID = 1; // 1-PACT, 2:VCS
  @Output()
  onValueChange : EventEmitter<RecommendedServices> = new EventEmitter<RecommendedServices>();

  psychosocialSummary : HousingLevelItem[];
  recommServiceGroup: FormGroup;
  isPageTouched: boolean;
  showComments : boolean;
  other : boolean;
  arr : number[]=[];
  constructor(
    private formBuilder: FormBuilder) {
    this.recommServiceGroup = this.formBuilder.group({
      ddlCtrl: [''],
      chkboxCtrl : [''],
      textareaCtrl: ['',[CustomValidators.requiredCharLength(2)]]
    }); 
   }

  ngOnInit() {
    if (this.parentPageID == 2){
      this.recommServiceGroup.disable();
    }
          
  }
  ngOnChanges() {
    if(this.recommendedServices != null){
      this.psychosocialSummary = this.recommendedServices.psychosocialSummary.filter(x => x.typeId != 390);
      this.arr = [];
      this.psychosocialSummary.filter( x => x.id != 0).forEach(x =>this.arr.push(x.typeId));
      if (this.arr.length > 0){
        this.recommServiceGroup.get('ddlCtrl').setValue(this.arr);
      }
      this.other = this.recommendedServices.psychosocialSummary.filter(x => x.typeId === 390 && x.id === 0).length > 0 ? false : true;
     this.recommServiceGroup.get('chkboxCtrl').setValue(this.other);
      if (this.other) {
        this.recommServiceGroup.get('textareaCtrl').enable();
        this.showComments =true;
        this.recommServiceGroup.get('textareaCtrl').setValue(this.recommendedServices.psychosocialSummary.filter(x => x.typeId === 390)[0].other);
      }
      else {      
          this.recommServiceGroup.get('textareaCtrl').disable();
          this.showComments =false;
      }          
}
  }
  ctrlChanged() {
    this.save();
  }
  onOtherChkChange ($event) {
    this.recommServiceGroup.get('textareaCtrl').setValue('');
    this.recommendedServices.psychosocialSummary.filter(x => x.typeId == 390)[0].other = '';
    if ($event.checked){
      this.showComments = true;
      this.recommServiceGroup.get('textareaCtrl').enable();    
      this.recommendedServices.psychosocialSummary.filter(x => x.typeId == 390)[0].id = 390;
    } else{
      this.showComments = false;
      this.recommServiceGroup.get('textareaCtrl').disable();
      this.recommendedServices.psychosocialSummary.filter(x => x.typeId == 390)[0].id = 0;
    }
    this.isPageTouched = true;
    this.save();
  }  
  onTextChange () {
    this.isPageTouched = true;
    this.recommendedServices.psychosocialSummary.filter(x => x.typeId == 390)[0].other = this.recommServiceGroup.get('textareaCtrl').value;
    this.save();
  }

  displayItem (i : number): string{
    if(this.recommendedServices.psychosocialSummary.filter(z => z.typeId == i).length > 0){
      return this.recommendedServices.psychosocialSummary.filter(z => z.typeId == i)[0].description;
    }
  }
  save() {
        this.recommendedServices.psychosocialSummary.filter(x => x.typeId != 390).forEach(z => z.id = 0);
        this.recommServiceGroup.get('ddlCtrl').value.forEach(
          (x: number) => x > 0 ? this.recommendedServices.psychosocialSummary.filter(z => z.typeId == x)[0].id = x :''
        );
        this.onValueChange.emit(this.recommendedServices);
  }
}
