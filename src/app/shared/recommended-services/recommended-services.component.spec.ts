import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecommendedServicesComponent } from './recommended-services.component';

describe('RecommendedServicesComponent', () => {
  let component: RecommendedServicesComponent;
  let fixture: ComponentFixture<RecommendedServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecommendedServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommendedServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
