import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import "ag-grid-enterprise";
import { ToastrModule } from 'ngx-toastr';

import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { RecommendedServicesComponent } from './recommended-services.component'

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      progressBar: true,
      progressAnimation: 'increasing'
    })
  ],
  declarations: [
    RecommendedServicesComponent
  ],
  exports: [
    RecommendedServicesComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecommendedServicesModule { }
