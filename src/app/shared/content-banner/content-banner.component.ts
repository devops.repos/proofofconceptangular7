import { Component, OnInit, ChangeDetectorRef, OnDestroy, Input, ViewChild, QueryList, ElementRef, ViewChildren } from '@angular/core';
import { Subscription } from 'rxjs';
import { NavService } from '../../core/sidenav-list/nav.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { UserService } from 'src/app/services/helper-services/user.service';
import { PactDashboardService } from 'src/app/core/pact-dashboard/pact-dashboard.service';
import { ApplicationNotifications, AppNotification } from 'src/app/core/pact-dashboard/pact-dashboard.model';
import { ConfirmDialogService } from '../confirm-dialog/confirm-dialog.service';
import { MatCheckbox } from '@angular/material';

@Component({
  selector: 'app-content-banner',
  templateUrl: './content-banner.component.html',
  styleUrls: ['./content-banner.component.scss']
})
export class ContentBannerComponent implements OnInit, OnDestroy {

  widthSideBar: number;
  widthSideBarSub: Subscription;
  mobileSize: MediaQueryList;
  normalSize: MediaQueryList;
  private mobileQueryListener: () => void;

  @Input() isDashboard: boolean;
  @Input() pageName: any;
  @Input() TutorialPageName: string;
  @Input() isTutorialRequired = false;
  @Input() tutorialIconColor = 'white';
  // @ViewChildren("checkbox") myCheckbox: QueryList<ElementRef>;

  environmentName = '';

  // App Notifications
  alertList: AppNotification[] = [];

  alertMsg = '';
  checkedBoxes = [];

  environmentNameSub: Subscription;
  appNotificationSub: Subscription;

  constructor(
    private navService: NavService,
    private userService: UserService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private dashboardService: PactDashboardService,
    private confirmDialogService: ConfirmDialogService
  ) {
    this.mobileSize = media.matchMedia('(max-width: 959px)');
    this.normalSize = media.matchMedia('(min-width: 960px)');

    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileSize.addListener(this.mobileQueryListener);
    this.normalSize.addListener(this.mobileQueryListener);
  }

  ngOnInit() {
    /** Getting the width of the sidenav to reflect the changes to nav toggle for adjusting the content Banner width */
    this.widthSideBarSub = this.navService.getWidthSideBar().subscribe(res => {
      this.widthSideBar = res;
      // console.log('content-banner- widthSideBar: ', this.widthSideBar);
    });

    /* Getting the environment Name from the user Info in UserService */
    this.environmentNameSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.environmentName = res.environment;
      }
    });

    /* Getting the App Notifications (Alert, PopUp, Announcements) */
    this.appNotificationSub = this.dashboardService.getAppNotification().subscribe((res: ApplicationNotifications) => {
      if (res) {
        // console.log('banner appNotification: ', res);
        this.alertList = res.appAlert;
        if (this.alertList.length > 0) {
          /* alertMsg for initial load */
          this.alertMsg = this.alertList[0].notificationMessage;
          var index = 1;
          setInterval(() => {
            // var index = Math.floor(Math.random() * this.alertList.length);
            if (index == this.alertList.length) {
              index = 0;
            }
            // console.log('index: ', index);
            this.alertMsg = this.alertList[index].notificationMessage;
            // console.log('alertMsg: ', this.alertMsg);
            index++;
          }, 10000);
        }
      }
    });
  }

  // onAlertCheckboxClicked(event: MatCheckbox, alert: AppNotification) {
  //   // console.log('event: ', event);
  //   if (event.checked) {
  //     const title = "Alert!";
  //     const primaryMessage = 'This is a one time Alert Notification.';
  //     const secondaryMessage = 'Clicking \'OK\' will remove the selected Notification from the List.';
  //     const confirmButtonName = 'OK';
  //     const dismissButtonName = 'Cancel';

  //     this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
  //       positiveResponse => {
  //         if (alert.appNotificationID > 0 && alert.isAlertDisplayOnce) {
  //           const selectedAlertMsg: AppNotification = {
  //             appNotificationID: alert.appNotificationID,
  //             isAlert: alert.isAlertDisplayOnce
  //           }
  //           this.appNotificationSub = this.dashboardService.saveAppNotificationUser(selectedAlertMsg).subscribe((res: ApplicationNotifications) => {
  //             if (res) {
  //               this.dashboardService.setAppNotification(res);
  //             }
  //           });
  //         }
  //       },
  //       negativeResponse => {
  //         this.myCheckbox.forEach((element: any) => {
  //           // console.log('element', element);
  //           if (element.checked == true) {
  //             element.checked = false;
  //           }
  //         });

  //       }
  //     );
  //   }
  // }

  onAlertClick(alert: AppNotification) {
    const title = alert.notificationTitle;
    const primaryMessage = (!alert.notificationLongMessage && alert.isAlertDisplayOnce)
      ? (alert.notificationMessage + '</br></br><b><i style="color:red;font-size: 0.9em;">Note:</i></b></br><i style="font-size: 0.8em;"><u>One time Alert notification.</u></br>Clicking \'OK\' will remove the selected Notification from the List.</i>')
      : (!alert.notificationLongMessage && !alert.isAlertDisplayOnce) ? alert.notificationMessage : '';
    const secondaryMessage = (alert.notificationLongMessage && alert.isAlertDisplayOnce)
      ? (alert.notificationLongMessage + '</br></br><b><i style="color:red;font-size: 0.9em;">Note:</i></b></br><i style="font-size: 0.8em;"><u>One time Alert notification.</u></br>Clicking \'OK\' will remove the selected Notification from the List.</i>')
      : (alert.notificationLongMessage && !alert.isAlertDisplayOnce) ? alert.notificationLongMessage : '';
    const confirmButtonName = 'OK';
    const dismissButtonName = alert.isAlertDisplayOnce ? 'Cancel' : '';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        if (alert.appNotificationID > 0 && alert.isAlertDisplayOnce) {
          const selectedAlertMsg: AppNotification = {
            appNotificationID: alert.appNotificationID,
            isAlert: alert.isAlertDisplayOnce
          }
          this.appNotificationSub = this.dashboardService.saveAppNotificationUser(selectedAlertMsg).subscribe((res: ApplicationNotifications) => {
            if (res) {
              this.dashboardService.setAppNotification(res);
            }
          });
        }
      },
      negativeResponse => { }
    );
  }

  ngOnDestroy() {
    this.mobileSize.removeListener(this.mobileQueryListener);
    this.normalSize.removeListener(this.mobileQueryListener);
    if (this.widthSideBarSub) {
      this.widthSideBarSub.unsubscribe();
    }
    if (this.appNotificationSub) {
      this.appNotificationSub.unsubscribe();
    }
  }

}
