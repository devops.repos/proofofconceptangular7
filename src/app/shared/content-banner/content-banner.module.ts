import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';

import { ContentBannerComponent } from './content-banner.component';
import { PipesModule } from '../pipes/pipes.module';
import { TutorialModule } from './../tutorial/tutorial.module';

@NgModule({
  declarations: [
   ContentBannerComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    PipesModule,
    TutorialModule
  ],
  exports: [
    ContentBannerComponent
  ]
})
export class ContentBannerModule { }
