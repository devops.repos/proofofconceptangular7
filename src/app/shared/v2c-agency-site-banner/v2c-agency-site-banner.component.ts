import { Component, OnInit } from '@angular/core';
import { IVacancyListing } from 'src/app/pact-modules/vacancyControlSystem/vacancy-listing/v2c-interface.model';
import { V2cService } from 'src/app/pact-modules/vacancyControlSystem/vacancy-listing/v2c.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MoreContactsDialogComponent } from './more-contacts-dialog/more-contacts-dialog.component';

@Component({
  selector: 'app-v2c-agency-site-banner',
  templateUrl: './v2c-agency-site-banner.component.html',
  styleUrls: ['./v2c-agency-site-banner.component.scss']
})
export class V2cAgencySiteBannerComponent implements OnInit {
  // Site Selected from VacancyListing Page
  vacancyListingSelected: IVacancyListing = {
    hpAgencyID: 0,
    hpAgencyNo: 0,
    hpSiteID: 0,
    hpSiteNo: '',
    hpAgencyName: '',
    hpSiteName: '',
    primaryServiceContractID: 0,
    primaryServiceContractName: '',
    agreementPopulationID: 0,
    populationName: '',
    siteAgreementPopulationID: 0,
    agreementTypeDescription: '',
    siteType: 0,
    siteTypeDescription: '',
    boroughType: 0,
    boroughTypeDescription: '',
    siteFeatures: '',
    siteLocationType: 0,
    siteLocationTypeDescription: '',
    rentalSubsidies: '',
    siteAddress: '',
    interviewAddress: '',
    interviewCity: '',
    interviewState: '',
    interviewZip: '',
    interviewPhoneNo: '',
    unitsOccupied: 0,
    unitsAvailable: 0,
    unitsOffline: 0,
    totalUnits: 0,
    pendingReferrals: 0,
    primarySiteContact: '',
    siteContacts: []
  };

  constructor(
    private v2cService: V2cService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.v2cService.getVacancyListingSelected().subscribe(res => {
      if (res) {
        this.vacancyListingSelected = res;
      } else {
        /* If user refresh(reload) any page which uses this v2c-agency-site-banner,
          the banner data will be empty, so redirect them back to VacancyListing Page
         */
        this.router.navigate(['/vcs/vacancy-listing']);
      }
    });
  }

  onMoreContactsClick(): void {
    const dialogRef = this.dialog.open(MoreContactsDialogComponent, {
      width: '40%',
      panelClass: 'transparent'
    });
  }

}
