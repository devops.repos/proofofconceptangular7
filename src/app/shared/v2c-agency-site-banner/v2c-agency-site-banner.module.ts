import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { V2cAgencySiteBannerComponent } from './v2c-agency-site-banner.component';
import { MoreContactsDialogComponent } from './more-contacts-dialog/more-contacts-dialog.component';

@NgModule({
  declarations: [V2cAgencySiteBannerComponent, MoreContactsDialogComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
  ],
  exports: [
    V2cAgencySiteBannerComponent
  ],
  entryComponents: [MoreContactsDialogComponent],
})
export class V2CAgencySiteBannerModule { }
