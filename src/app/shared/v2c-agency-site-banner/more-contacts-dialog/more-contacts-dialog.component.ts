import { Component, OnInit } from '@angular/core';
import { IVacancyListing } from 'src/app/pact-modules/vacancyControlSystem/vacancy-listing/v2c-interface.model';
import { MatDialogRef } from '@angular/material';
import { WithdrawnDialogComponent } from 'src/app/pact-modules/vacancyControlSystem/referral-roster/withdrawn-dialog/withdrawn-dialog.component';
import { V2cService } from 'src/app/pact-modules/vacancyControlSystem/vacancy-listing/v2c.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-more-contacts-dialog',
  templateUrl: './more-contacts-dialog.component.html',
  styleUrls: ['./more-contacts-dialog.component.scss']
})
export class MoreContactsDialogComponent implements OnInit {

  hasMoreContacts = false;
  // Site Selected from VacancyListing Page
  vacancyListingSelected: IVacancyListing = {
    hpAgencyID: 0,
    hpAgencyNo: 0,
    hpSiteID: 0,
    hpSiteNo: '',
    hpAgencyName: '',
    hpSiteName: '',
    primaryServiceContractID: 0,
    primaryServiceContractName: '',
    agreementPopulationID: 0,
    populationName: '',
    siteAgreementPopulationID: 0,
    agreementTypeDescription: '',
    siteType: 0,
    siteTypeDescription: '',
    boroughType: 0,
    boroughTypeDescription: '',
    siteFeatures: '',
    siteLocationType: 0,
    siteLocationTypeDescription: '',
    rentalSubsidies: '',
    siteAddress: '',
    interviewAddress: '',
    interviewCity: '',
    interviewState: '',
    interviewZip: '',
    interviewPhoneNo: '',
    unitsOccupied: 0,
    unitsAvailable: 0,
    unitsOffline: 0,
    totalUnits: 0,
    pendingReferrals: 0,
    primarySiteContact: '',
    siteContacts: []
  };

  constructor(
    public dialogRef: MatDialogRef<WithdrawnDialogComponent>,
    private v2cService: V2cService,
    private router: Router
  ) { }

  ngOnInit() {
    this.v2cService.getVacancyListingSelected().subscribe(res => {
      if (res) {
        this.vacancyListingSelected = res;
        if (res.siteContacts.length > 0) {
          this.hasMoreContacts = true;
        }
      } else {
        /* If user refresh(reload) any page which uses this v2c-agency-site-banner,
          the banner data will be empty, so redirect them back to VacancyListing Page
         */
        this.router.navigate(['/vcs/vacancy-listing']);
      }
    });
  }

  onOKClick(): void {
    this.dialogRef.close();
  }
}
