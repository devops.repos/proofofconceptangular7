import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { ClientBannerComponent } from './client-banner.component';
import { ClientDocumentsDialogModule } from '../client-documents-dialog/client-documents-dialog.module';

@NgModule({
  declarations: [ClientBannerComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    ClientDocumentsDialogModule,
  ],
  exports: [ClientBannerComponent]
})
export class ClientBannerModule { }
