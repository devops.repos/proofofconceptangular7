import { Component, OnInit, OnDestroy } from '@angular/core';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { SidenavStatus } from 'src/app/core/sidenav-list/sidenav-status.model';
import { Subscription } from 'rxjs';
import { ClientApplication, ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ClientDocumentsDialogComponent } from '../client-documents-dialog/client-documents-dialog.component';
import { MatDialog } from '@angular/material';

export interface iClientDocumentsData {
  firstName: string;
  lastName: string;
  dob: string;
  cin: string;
  ssn: string;
  pactClientId: number;
  pactClientSearchAuditId: number;
  optionUserId: number;
}

@Component({
  selector: 'app-client-banner',
  templateUrl: './client-banner.component.html',
  styleUrls: ['./client-banner.component.scss']
})

export class ClientBannerComponent implements OnInit, OnDestroy {
  userData: AuthData;
  userDataSub: Subscription;
  firstName: string;
  lastName: string;
  sidenavStatus: SidenavStatus;
  sidenavStatusSub: Subscription;
  progressPercentage: number;
  selectedApplicationID: number;
  clientApplicationData: ClientApplication;
  clientDocumentsData: iClientDocumentsData = { firstName: null, lastName: null, dob: null, cin: null, ssn: null, pactClientId: 0, pactClientSearchAuditId: 0, optionUserId: null };

  private clientApplicationDataSub: Subscription;
  private selectedApplicationIDSub: Subscription;
  private sidenavCompleteStatusSub: Subscription;

  constructor(
    private sidenavStatusService: SidenavStatusService,
    private clientApplicationService: ClientApplicationService,
    private userService: UserService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    //Get User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    /** Getting the Client Details for whom the application is being created from ClientApplicationService */
    this.clientApplicationDataSub = this.clientApplicationService.getClientApplicationData().subscribe(res => {
      if (res) {
        this.clientApplicationData = res;
      }
      //Set Client Documents Data for Client Documents
      if (this.clientApplicationData) {
        this.clientDocumentsData.firstName = this.clientApplicationData.firstName;
        this.clientDocumentsData.lastName = this.clientApplicationData.lastName;
        this.clientDocumentsData.dob = this.clientApplicationData.dob;
        this.clientDocumentsData.cin = this.clientApplicationData.cin === null ? "" : this.clientApplicationData.cin;
        this.clientDocumentsData.ssn = this.clientApplicationData.ssn;
        this.clientDocumentsData.pactClientId = this.clientApplicationData.pactClientId == null ? 0 : this.clientApplicationData.pactClientId;
        this.clientDocumentsData.optionUserId = this.userData.optionUserId;
      }
    });

    /** Getting the Complete/InComplete Status of sidenav List for a particular ApplicationID */
    this.selectedApplicationIDSub = this.sidenavStatusService.getApplicationIDForSidenavStatus().subscribe(res => {
      if (res) {
        this.selectedApplicationID = res;
        // console.log('selectedApplicationID: ',this.selectedApplicationID);
        this.sidenavCompleteStatusSub = this.sidenavStatusService.getSidenavCompleteStatus(this.selectedApplicationID).subscribe((rslt) => {
          if (rslt) {
            this.sidenavStatus = rslt;
            // console.log(this.sidenavStatus);

            this.progressPercentage = 0; // Reset the progressbar
            if (this.sidenavStatus.isConsentComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isDemographicComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isHousingHomelessComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isClinicalAssessmentComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isADLComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isMedicationsProvidersHospitalizationComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isTraumaAndChildWelfareComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isSymptomsSubstanceComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isHousingPreferenceComplete === 1) {
              this.progressPercentage += 1;
            }
            if (this.sidenavStatus.isPsychiatricPsychoMHRComplete === 1) {
              this.progressPercentage += 1;
            }
            let totalprogress = this.progressPercentage * 10; // 10 is the total number of sidenav items
            if (totalprogress === 100) {
              totalprogress = 99;
            }
            this.progressPercentage = totalprogress;
            // console.log('progressPercentage: ', this.progressPercentage);
          }
        });
      }
    });

  }

  //Open Client Documents Dialog
  openClientDocumentDialog(): void {
    this.dialog.open(ClientDocumentsDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: this.clientDocumentsData
    });
  }

  ngOnDestroy() {
    if (this.sidenavStatusSub) {
      this.sidenavStatusSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.clientApplicationDataSub) {
      this.clientApplicationDataSub.unsubscribe();
    }
    if (this.selectedApplicationIDSub) {
      this.selectedApplicationIDSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
  }
}
