import { Component, OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  OnDestroy } from '@angular/core';

@Component({
  selector: 'app-nav-footer',
  templateUrl: './nav-footer.component.html',
  styleUrls: ['./nav-footer.component.scss']
})
export class NavFooterComponent implements OnInit {

  @Output() Savebtn = new EventEmitter();
  @Output() Clearbtn = new EventEmitter();
  @Output() Exitbtn = new EventEmitter();
  @Output() Submitbtn = new EventEmitter();
  @Output() Previousbtn = new EventEmitter();
  @Output() Nextbtn = new EventEmitter();

  @Input() isSaveRequired = true;
  //@Input() isSaveRequired : boolean;
  @Input() isClearRequired = false;
  @Input() isExitRequired = false;
  @Input() isSubmitRequired = false;
  @Input() isNextRequired = true;
  @Input() isPreviousRequired = true;
  @Input() isTutorialRequired = false;
  @Input() page: string;

  constructor() { }

  ngOnInit() {
  }

  SaveClicked() {
    // console.log('Save Clicked');
    this.Savebtn.emit();
  }

  ClearClicked() {
    // console.log('Clear Clicked');
    this.Clearbtn.emit();
  }
  ExitClicked() {
    // console.log('Exit Clicked');
    this.Exitbtn.emit();
  }

  SubmitClicked() {
    this.Submitbtn.emit();
  }

  PreviousClicked() {
    // console.log('Previous clicked');
    this.Previousbtn.emit();

  }
  NextClicked() {
    // console.log('Next clicked');
    this.Nextbtn.emit();
  }

}
