import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms'; 
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { NavFooterComponent } from './nav-footer.component';  
import { TutorialModule } from 'src/app/shared/tutorial/tutorial.module';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    ReactiveFormsModule,
    TutorialModule       
  ],
  declarations: [
   NavFooterComponent
  ],
  exports: [
    NavFooterComponent
  ]
})
export class NavFooterModule { }
