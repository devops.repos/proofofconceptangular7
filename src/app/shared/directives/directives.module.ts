import { NgModule } from '@angular/core';
import { DateDirective } from './date.directive';
import { NumberDirective } from './number.directive';
import { DigitOnlyDirective } from './digit.directive';
import { AlphabetDirective } from './alphabet.directive';
import { AlphabetSpaceDirective } from './alphabetSpace.directive';
import { FocusDirective } from './focus.directive';

@NgModule({
    declarations: [ DateDirective, NumberDirective, DigitOnlyDirective, AlphabetDirective, AlphabetSpaceDirective, FocusDirective],
    exports: [DateDirective, NumberDirective, DigitOnlyDirective, AlphabetDirective, AlphabetSpaceDirective, FocusDirective]
})

export class DirectivesModule {
}
