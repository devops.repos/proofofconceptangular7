import { Directive, ElementRef, HostListener } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';

@Directive({
  selector: '[dateOnly]'
})
export class DateDirective {

  constructor(private _el: ElementRef 
              ,private toastrService : ToastrService) { }

  @HostListener('keydown', ['$event']) 
  onInputChange(e : KeyboardEvent) {
    
    //console.log(e.keyCode);
    if (e.keyCode !== 8)
    {
        if (e.keyCode === 9 || ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)))
        {
          if (this._el.nativeElement.value.length === 2 || this._el.nativeElement.value.length === 5)
            this._el.nativeElement.value = this._el.nativeElement.value + '/';  
        }
        else
        {
          e.preventDefault();
        }

        if (e.keyCode === 9 && this._el.nativeElement.value.length < 10)
        {
          this._el.nativeElement.value = null;
          //console.log(this._el.nativeElement.value);  
        }
          
    }
    else if (e.keyCode === 8)
    {
        var temp = this._el.nativeElement.value;
        if (this._el.nativeElement.value.length === 5)
            this._el.nativeElement.value = temp.substring(0,4); 
        else if (this._el.nativeElement.value.length === 2)
            this._el.nativeElement.value = temp.substring(0,1);   
    }

    if(this._el.nativeElement.value.length === 10)
    {
        //console.log(this._el.nativeElement.value);
        if (!moment(this._el.nativeElement.value, 'MM/DD/YYYY', true).isValid()) {
            if (!this.toastrService.currentlyActive) {
              this.toastrService.error("Invalid Date.");
              //this._el.nativeElement.value = null;
            }
          }
    }
  }

}