import { Directive, Input, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[focus]'
})
export class FocusDirective {

  @Input('focus') isFocused: boolean;

  constructor(private hostElement: ElementRef, private renderer: Renderer) { }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    if (this.isFocused) {
      this.renderer.invokeElementMethod(this.hostElement.nativeElement, 'focus');
    }
  }

}
