import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[alphabetSpace]'
})
export class AlphabetSpaceDirective {

   key;

  constructor(private _el: ElementRef) { }
 
  @HostListener('keydown', ['$event']) onKeydown(event: KeyboardEvent) {
    this.key = event.keyCode;
    //console.log(this.key);
    if ((this.key != 46 && this.key != 32 && this.key >= 15 && this.key <= 64) || (this.key >= 123) || (this.key >= 96 && this.key <= 105)) {
      event.preventDefault();
    }
  }

/*   @HostListener('paste', ['$event'])
  onPaste(event: ClipboardEvent) {
    event.preventDefault();
    const pastedInput: string = event.clipboardData
      .getData('text/plain')
      .replace(/^[a-zA-Z\s]+$/g, ''); // replace(/^[a-zA-Z ]*$/g, ''); //  // .replace(/^[a-zA-Z ]+$/g
    document.execCommand('insertText', false, pastedInput);
   // document.execCommand("paste", false, pastedInput);
  }

  @HostListener('drop', ['$event'])
  onDrop(event: DragEvent) {
    event.preventDefault();
    const textData: string = event.dataTransfer
      .getData('text').replace(/^[a-zA-Z\s]+$/g, '');//replace(/^[a-zA-Z ]*$/g, ''); // replace(/^[a-zA-Z ]+$/g,
    this._el.nativeElement.focus();
    document.execCommand('insertText', false, textData);
   // document.execCommand("paste", false, textData);
  } */

}
