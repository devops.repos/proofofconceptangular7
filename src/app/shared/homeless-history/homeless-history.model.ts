export class PACTHousingHomeless {
    applicationDate?: string;
    pactHousingEpisode?: HousingEpisode[];
    pactHousingChart?: HousingChart[];
    pactHousingPeriod?: HousingPeriod[];
}

export class HousingEpisode {
    pactApplicationID?: number;
    pactHousingHomelessID?: number;
    housingType?: number;
    episodeSourceType?: number;
    homelessStayDocumentedType?: number;
    nursingHomeRemedyMemberType?: number;
    residingInAdultHomeType?: number;
    canReturnToPriorResidenceType?: number;
    doesRequireSupportiveHousingType?: number;
    fromDate?: string;
    toDate?: string;
    housingTypeDescription?: string;
    facilityName?: string;
    streetAddress?: string;
    city?: string;
    state?: string;
    homelessStayDocumented?: string;
    episodeSource?: string;
    nursingHomeRemedyMember?: string;
    residingInAdultHome?: string;
    canReturnToPriorResidence?: string;
    canReturnToPriorResidenceComment?: string;
    doesRequireSupportiveHousing?: string;
    doesRequireSupportiveHousingComment?: string;
    cthType?: number;
    cthTypeDescription: string;
    isEpisodeAddedPostTransmission?: boolean;
    createdDate?: string;
    applicationDate?: string;
    parentPageID?: number;

    // CreatedBy?: number;
}
export class HousingChart {
    chartYear?: number;
    chartYearMonth?: number;
    chartJan?: string;
    chartFeb?: string;
    chartMar?: string;
    chartApr?: string;
    chartMay?: string;
    chartJun?: string;
    chartJul?: string;
    chartAug?: string;
    chartSep?: string;
    chartOct?: string;
    chartNov?: string;
    chartDec?: string;
    chartJanFull?: string;
    chartFebFull?: string;
    chartMarFull?: string;
    chartAprFull?: string;
    chartMayFull?: string;
    chartJunFull?: string;
    chartJulFull?: string;
    chartAugFull?: string;
    chartSepFull?: string;
    chartOctFull?: string;
    chartNovFull?: string;
    chartDecFull?: string;

    chartJanEpisode?: string;
    chartFebEpisode?: string;
    chartMarEpisode?: string;
    chartAprEpisode?: string;
    chartMayEpisode?: string;
    chartJunEpisode?: string;
    chartJulEpisode?: string;
    chartAugEpisode?: string;
    chartSepEpisode?: string;
    chartOctEpisode?: string;
    chartNovEpisode?: string;
    chartDecEpisode?: string;
    chartJanFullEpisode?: string;
    chartFebFullEpisode?: string;
    chartMarFullEpisode?: string;
    chartAprFullEpisode?: string;
    chartMayFullEpisode?: string;
    chartJunFullEpisode?: string;
    chartJulFullEpisode: string;
    chartAugFullEpisode?: string;
    chartSepFullEpisode?: string;
    chartOctFullEpisode?: string;
    chartNovFullEpisode?: string;
    chartDecFullEpisode?: string;

}
export class HousingPeriod {
    fromDate?: string;
    toDate?: string;
}

export class DeleteHousingEpisode {
    pactApplicationID?: number;
    pactHousingHomelessID?: number;
}

export class UpdateCTH {
    cthType: number;
    pactApplicationID?: number;
    pactHousingHomelessID?: number;
}