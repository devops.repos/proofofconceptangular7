import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { DeleteHousingEpisode, UpdateCTH } from './homeless-history.model';

@Component({
  selector: "housing-action",
  template: `
    <mat-icon *ngIf="params.data.parentPageID === 1" (click)="onDelete()" matTooltip="Delete Housing Episode" class="action-icon" color="warn">
      delete_forever
    </mat-icon>
    <div *ngIf="params.data.parentPageID === 3 && 
    (params.data.housingType === 411
    || params.data.housingType === 412
    || params.data.housingType === 413
    || params.data.housingType === 414
    || params.data.housingType === 415
    || params.data.housingType === 419
    || params.data.housingType === 421
    || params.data.housingType === 426
    || params.data.housingType === 428
    || params.data.housingType === 430
    || params.data.housingType === 431)"> 
    <mat-icon  matTooltip="Select CTH" class="action-icon" color="warn" [matMenuTriggerFor]="cthAction">
      more_vert
    </mat-icon>
    <mat-menu #cthAction="matMenu">
      <button mat-menu-item (click)="onCTH(33)" class="menu-button">Yes</button>
      <button mat-menu-item (click)="onCTH(34)" class="menu-button">No</button>
    </mat-menu>
    </div>
  `,
  styles: [
    `
      .action-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 30px;
        width: 100%;
        height: 30px;
      }
    `
  ]
})
export class HousingActionComponent implements ICellRendererAngularComp {
  public params: any;
  public cell: any;
  private docSelected = new DeleteHousingEpisode();
  private updateCTH = new UpdateCTH();

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  //On CTH
  onCTH(cthType: number) {
    this.updateCTH = {
      cthType: cthType,
      pactHousingHomelessID: this.params.data.pactHousingHomelessID,
      pactApplicationID: this.params.data.pactApplicationID
    }
    this.params.context.componentParent.updateCTHParent(this.updateCTH);
  }

  onDelete() {
    this.docSelected = {
      pactHousingHomelessID: this.params.data.pactHousingHomelessID,
      pactApplicationID: this.params.data.pactApplicationID
    };
    this.params.context.componentParent.episodeDeleteParent(this.docSelected);
  }

  refresh(): boolean {
    return false;
  }
}
