import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { ToastrModule } from 'ngx-toastr';

import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { HomelessHistoryComponent } from './homeless-history.component';
import { HousingActionComponent } from './housing-action.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { TutorialModule } from 'src/app/shared/tutorial/tutorial.module';

@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        MaterialModule,
        ErrorsModule,
        HttpClientModule,
        ReactiveFormsModule,
        DirectivesModule,
        TutorialModule,
        ToastrModule.forRoot({
            progressBar: true,
            progressAnimation: 'increasing'
        }),
        AgGridModule.withComponents([
            HousingActionComponent
        ])
    ],
    declarations: [
        HomelessHistoryComponent,
        HousingActionComponent
    ],
    exports: [
        HomelessHistoryComponent
    ]
})

export class HomelessHistoryModule {
}
