import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import {
  PACTHousingHomeless,
  HousingEpisode,
  DeleteHousingEpisode,
  UpdateCTH
} from './homeless-history.model';

@Injectable({
  providedIn: 'root'
})
export class HousingHistoryService {
  SERVER_URL = environment.pactApiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  getHousingHomeless = (
    PACTApplicationID: number
  ): Observable<PACTHousingHomeless> => {
    const getHousingHomelessURL = `${this.SERVER_URL}HousingHomeless/GetHousingHomeless`;
    return this.httpClient.post<PACTHousingHomeless>(
      getHousingHomelessURL,
      PACTApplicationID,
      this.httpOptions
    );
  }

  saveHousingHomeless = (
    housingEpisode: HousingEpisode
  ): Observable<number> => {
    const saveHousingHomelessURL = `${this.SERVER_URL}HousingHomeless/SaveHousingHomeless`;
    return this.httpClient.post<number>(
      saveHousingHomelessURL,
      housingEpisode,
      this.httpOptions
    );
  }

  deleteHousingHomeless = (
    delHousingEpisode: DeleteHousingEpisode
  ): Observable<boolean> => {
    const deleteHousingHomelessURL = `${this.SERVER_URL}HousingHomeless/DeleteHousingHomeless`;
    return this.httpClient.post<boolean>(
      deleteHousingHomelessURL,
      delHousingEpisode,
      this.httpOptions
    );
  }

  //Update CTH
  updateCTH(updateCTH: UpdateCTH): Observable<any> {
    if (updateCTH) {
      const updateCTHUrl = `${this.SERVER_URL}HousingHomeless/UpdateEpisodeCTH`;
      return this.httpClient.post(updateCTHUrl, JSON.stringify(updateCTH), this.httpOptions);
    }
  }
}
