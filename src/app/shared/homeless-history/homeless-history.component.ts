import { Component, OnInit, ViewChild, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs';

import { HousingHistoryService } from './homeless-history.service';
import {
  PACTHousingHomeless,
  HousingEpisode,
  DeleteHousingEpisode,
  UpdateCTH
} from './homeless-history.model';
import { environment } from 'src/environments/environment';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { HousingActionComponent } from './housing-action.component';

@Component({
  selector: 'app-homeless-history',
  templateUrl: './homeless-history.component.html',
  styleUrls: ['./homeless-history.component.scss'],
  providers: [DatePipe]
})

export class HomelessHistoryComponent implements OnInit, OnDestroy {

  SERVER_URL = environment.pactApiUrl;
  housingForm: FormGroup;
  housingType: RefGroupDetails[];
  responseType: RefGroupDetails[];
  housingData = new PACTHousingHomeless();
  housingEpisode = new HousingEpisode();
  housingTypeSelected: number;
  currentHousingDate: Date = new Date();
  housingTypeHomelessStay = false;
  housingTypeNursingHome = false;
  housingTypeCanReturn = false;
  housingTypeDoesRequire = false;
  canReturn = false;
  doesRequire = false;
  isCurrentDate = true;

  isCTHCompleted: boolean = false;
  isCTHUpdates: boolean = false;

  @Input() pactApplicationID: number;
  @Input() parentPageID: number; // 1-PACT, 2:VCS, 3:Determination
  @Input() panelExpanded: boolean;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('fromDatePicker') fromDatePicker;
  @ViewChild('toDatePicker') toDatePicker;
  @Output() homelessHistoryChanged = new EventEmitter();
  @Output() panelStateChanged = new EventEmitter();
  @Output() isCthChanged = new EventEmitter();

  validationMessages = {
    fromDateCtrl: {
      required: 'From Date is required.'
    },
    toDateCtrl: {
      required: 'To Date is required.'
    },
    housingTypeCtrl: {
      dropdownRequired: 'Housing type is required.'
    },
    facilityNameCtrl: {
      required: 'Facility Name is required.',
      charLength: 'Invalid Facility Name.'
    },
    streetCtrl: {
      required: 'Street Address is required.',
      charLength: 'Invalid Street Name.'
    },
    cityCtrl: {
      required: 'City is required.',
      charLength: 'Invalid City Name.'
    },
    stateCtrl: {
      required: 'State is required.',
      charLength: 'Invalid State Name.'
    },
    homelessStayDocCtrl: {
      required: 'Homeless Stay Documented is required.'
    },
    nursingHomeCtrl: {
      required: 'Nursing Home Remedy Member is required.'
    },
    canReturnCtrl: {
      required: 'Upon discharge can applicant return to his/her prior residence is required.'
    },
    doesRequireCtrl: {
      required: 'Does applicant require supportive services/housing to prevent him/her from becoming homeless is required.'
    },
    canReturnExplainCtrl: {
      required: 'Upon discharge can applicant return to his/her prior residence Explain is required.',
      charLength: 'Invalid: Upon discharge can applicant return to his/her prior residence Explain.'
    },
    doesRequireExplainCtrl: {
      required: 'Does applicant require supportive services/housing to prevent him/her from becoming homeless Explain is required.',
      charLength: 'Invalid: Does applicant require supportive services/housing to prevent him/her from becoming homeless Explain.'
    }
  };

  formErrors = {
    fromDateCtrl: '',
    toDateCtrl: '',
    housingTypeCtrl: '',
    facilityNameCtrl: '',
    streetCtrl: '',
    cityCtrl: '',
    stateCtrl: '',
    homelessStayDocCtrl: '',
    nursingHomeCtrl: '',
    canReturnCtrl: '',
    doesRequireCtrl: '',
    canReturnExplainCtrl: '',
    doesRequireExplainCtrl: ''
  };

  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  public gridOptions: GridOptions;
  rowData: HousingEpisode[];
  context: any;
  overlayNoRowsTemplate: string;

  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;
  commonServiceSub: Subscription;
  homelessServiceSub: Subscription;
  c2vServiceSub: Subscription;
  homelessFormSub: Subscription;

  constructor(
    private fb: FormBuilder,
    private housingHistoryService: HousingHistoryService,
    private commonService: CommonService,
    private confirmDialogService: ConfirmDialogService,
    private toastr: ToastrService,
    private datePipe: DatePipe
  ) {
    this.columnDefs = [
      {
        headerName: 'CTH',
        field: 'cthTypeDescription',
        width: 70,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'From Date',
        field: 'fromDate',
        width: 100,
        filter: 'agTextColumnFilter',
        comparator: this.commonService.dateComparator 
      },
      {
        headerName: 'To Date',
        field: 'toDate',
        width: 100,
        filter: 'agTextColumnFilter',
        comparator: this.commonService.dateComparator 
      },
      {
        headerName: 'Housing Type',
        field: 'housingTypeDescription',
        width: 300,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Facility Name',
        field: 'facilityName',
        width: 250,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Street Address',
        field: 'streetAddress',
        width: 250,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'City',
        field: 'city',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'State',
        field: 'state',
        width: 80,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Stay Documented',
        field: 'homelessStayDocumented',
        width: 140,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Source',
        field: 'episodeSource',
        cellRenderer: function (params: { value: string; data: { isEpisodeAddedPostTransmission: boolean }; }) {
          if (params.data.isEpisodeAddedPostTransmission) {
            return '<span style="color: red; font-size: large; vertical-align: middle">*</span>' + '&nbsp;' + params.value;
          }
          else {
            return params.value;
          }
        },
        width: 250,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Actions',
        field: 'action',
        width: 65,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRendererSelector(params: any) {
          const actionButton = {
            component: 'actionRenderer'
          };
          if (params.data.episodeSourceType === 363 || params.data.episodeSourceType === 367) {
            return actionButton;
          } else {
            return null;
          }
        },
        cellStyle: function (params: { data: { parentPageID: number; episodeSourceType: number; housingType: number }; }) {
          if (params.data.episodeSourceType === 363 || params.data.episodeSourceType === 367) {
            if (params.data.parentPageID === 3 &&
              (params.data.housingType === 411
                || params.data.housingType === 412
                || params.data.housingType === 413
                || params.data.housingType === 414
                || params.data.housingType === 415
                || params.data.housingType === 419
                || params.data.housingType === 421
                || params.data.housingType === 426
                || params.data.housingType === 428
                || params.data.housingType === 430
                || params.data.housingType === 431)) {
              return { backgroundColor: '#fefac0' };
            }
          }
        }
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.pagination = true;
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: HousingActionComponent
    };
  }

  onGridReady = (params: any) => {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach((column: any) => {
      if (
        column.colId !== 'action' &&
        column.colId !== 'fromDate' &&
        column.colId !== 'toDate' &&
        column.colId !== 'housingTypeDescription' &&
        column.colId !== 'homelessStayDocumented' &&
        column.colId !== 'state'
      ) {
        allColumnIds.push(column.colId);
      }
    });
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Housing Episodes To Show</span>';
    // this.gridColumnApi.autoSizeColumns(allColumnIds);
    if (this.parentPageID === 1 || this.parentPageID === 3) {
      this.gridColumnApi.setColumnVisible('action', true);
    } else {
      this.gridColumnApi.setColumnVisible('action', false);
    }
    if (this.parentPageID === 3) {
      this.gridColumnApi.setColumnVisible('cthTypeDescription', true);
    } else {
      this.gridColumnApi.setColumnVisible('cthTypeDescription', false);
    }

    /** API call to get the grid data */
    this.getHousingHomeless();
  }

  ngOnInit() {
    this.housingForm = this.fb.group({
      fromDateCtrl: ['', [Validators.required]],
      toDateCtrl: ['', [Validators.required]],
      housingTypeCtrl: [0, [CustomValidators.dropdownRequired()]],
      facilityNameCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
      streetCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
      cityCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
      stateCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
      homelessStayDocCtrl: ['', [Validators.required]],
      nursingHomeCtrl: ['', [Validators.required]],
      canReturnCtrl: ['', [Validators.required]],
      doesRequireCtrl: ['', [Validators.required]],
      canReturnExplainCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
      doesRequireExplainCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]]
    });

    this.loadRefGroupDetails();

    this.homelessFormSub = this.housingForm.valueChanges.subscribe(data => {
      this.logValidationErrors(this.housingForm);
    });

  }

  loadRefGroupDetails = () => {
    const value = '43, 7'; // 43 - housingType, 7 - Response Item
    this.commonServiceSub = this.commonService.getRefGroupDetails(value).subscribe(res => {
      const data = res.body as RefGroupDetails[];
      this.housingType = data.filter(d => d.refGroupID === 43 && (d.refGroupDetailID !== 484 &&
        d.refGroupDetailID !== 485));
      this.responseType = data.filter(
        d => d.refGroupDetailID === 33 || d.refGroupDetailID === 34
      );
    },
      error => console.error('Error!', error)
    );
  }

  getHousingHomeless = () => {
    if (this.pactApplicationID) {
      this.homelessServiceSub = this.housingHistoryService.getHousingHomeless(this.pactApplicationID).subscribe(res => {
        this.housingData = res as PACTHousingHomeless;
        this.rowData = this.housingData.pactHousingEpisode;
        if (this.housingData.pactHousingEpisode.find(r => r.cthType === null)) {
          this.isCTHCompleted = false;
        }
        else {
          this.isCTHCompleted = true;
        }
        this.currentHousingDate = new Date(this.housingData.applicationDate);
        this.homelessHistoryChanged.emit('history changed');
        this.rowData.forEach(row => {
          row.parentPageID = this.parentPageID;
        });
      });
    }
  }

  onSubmit = (): void => {
    this.setValidators();

    this.markFormGroupTouched(this.housingForm);
    this.logValidationErrors(this.housingForm);
    if (this.isFormValid()) {
      this.saveHousingHomeless();
    } else {
      this.showValidationErrors();
    }
  }

  showValidationErrors = () => {
    if (this.formErrors.fromDateCtrl) {
      this.toastr.error(this.formErrors.fromDateCtrl.toString());
    } else if (this.formErrors.toDateCtrl) {
      this.toastr.error(this.formErrors.toDateCtrl.toString());
    } else if (this.formErrors.housingTypeCtrl) {
      this.toastr.error(this.formErrors.housingTypeCtrl.toString());
    } else if (this.formErrors.facilityNameCtrl) {
      this.toastr.error(this.formErrors.facilityNameCtrl.toString());
    } else if (this.formErrors.streetCtrl) {
      this.toastr.error(this.formErrors.streetCtrl.toString());
    } else if (this.formErrors.cityCtrl) {
      this.toastr.error(this.formErrors.cityCtrl.toString());
    } else if (this.formErrors.stateCtrl) {
      this.toastr.error(this.formErrors.stateCtrl.toString());
    } else if (this.formErrors.homelessStayDocCtrl) {
      this.toastr.error(this.formErrors.homelessStayDocCtrl.toString());
    } else if (this.formErrors.nursingHomeCtrl) {
      this.toastr.error(this.formErrors.nursingHomeCtrl.toString());
    } else if (this.formErrors.canReturnCtrl) {
      this.toastr.error(this.formErrors.canReturnCtrl.toString());
    } else if (this.formErrors.canReturnExplainCtrl) {
      this.toastr.error(this.formErrors.canReturnExplainCtrl.toString());
    } else if (this.formErrors.doesRequireCtrl) {
      this.toastr.error(this.formErrors.doesRequireCtrl.toString());
    } else if (this.formErrors.doesRequireExplainCtrl) {
      this.toastr.error(this.formErrors.doesRequireExplainCtrl.toString());
    }
  }

  onChartSelect = (cellData: string) => {
    if (this.parentPageID === 1) {
      const formFromDate = this.datePipe.transform(this.housingForm.get('fromDateCtrl').value, 'MM/dd/yyyy');
      const formToDate = this.datePipe.transform(this.housingForm.get('toDateCtrl').value, 'MM/dd/yyyy');
      const chartID: string[] = cellData.split('-');

      if (formFromDate && !formToDate) {
        if (chartID[0] === 'missing') {
          const cEndDt = this.getLastDateOfMonth(chartID[2], +chartID[1]);
          let EDate = chartID[2] + '/' + cEndDt + '/' + chartID[1];
          const dateFrom: string[] = formFromDate.split('/');
          const homelessPeriod = this.housingData.pactHousingPeriod;

          for (let period = 0; period < homelessPeriod.length; period++) {
            const tempHomelessPeriod = homelessPeriod[period];
            let tempDateFrom = new Date(Date.parse(tempHomelessPeriod.fromDate));
            const fromDate = new Date(Date.parse(formFromDate));

            if (tempDateFrom > fromDate) {
              const dtFrom: string[] = tempHomelessPeriod.fromDate.split('/');
              if (chartID[2] === dtFrom[0] && chartID[1] === dtFrom[2]) {
                const tempDate = new Date(tempHomelessPeriod.fromDate);
                tempDateFrom = new Date(tempDate.setDate(tempDate.getDate() - 1));
                const tempToDateMonth = tempDateFrom.getMonth() + 1;
                const toDateDate = tempDateFrom.getDate() < 10 ? '0' + tempDateFrom.getDate() : tempDateFrom.getDate();
                const toDateYear = tempDateFrom.getFullYear();
                const toDateMonth = tempToDateMonth < 10 ? '0' + tempToDateMonth : tempToDateMonth;
                EDate = toDateMonth + '/' + toDateDate + '/' + toDateYear;
                period = 1000;
              }
            }

          }

          this.housingForm.get('toDateCtrl').setValue(new Date(EDate));
          this.toDatePicker.open();
          if (this.ValidateFromToDates() && this.ValidateOverlapDates()) {
            const isFuture = this.isFutureDate(EDate);
            if (!isFuture) {
              this.housingForm.get('toDateCtrl').setValue(new Date(EDate));
              this.toDatePicker.open();
              this.housingForm.get('housingTypeCtrl').setValue(0);
              this.onHousingTypeChange();
            } else {
              this.housingForm.get('toDateCtrl').setValue(this.currentHousingDate);
              this.toDatePicker.open();
            }
          } else {
            this.housingForm.get('toDateCtrl').setValue('');
          }
        }
        if (this.housingForm.get('toDateCtrl').value) {
          this.onToDateFocus();
        }
      } else {
        if (chartID[0] === 'missing') {
          const EDate = chartID[2] + '/01/' + chartID[1];
          let count = 0;
          const homelessPeriod = this.housingData.pactHousingPeriod;
          for (let period = 0; period < homelessPeriod.length; period++) {
            const tempHomelessPeriod = homelessPeriod[period];
            let tempHomelessPeriodNext = homelessPeriod[period];

            if (period + 1 < homelessPeriod.length) {
              tempHomelessPeriodNext = homelessPeriod[period + 1];
            }

            let tempHomelessPeriodPrevious = homelessPeriod[period];

            if (period > 0) {
              tempHomelessPeriodPrevious = homelessPeriod[period - 1];
            }
            const dateToNext = tempHomelessPeriodNext.fromDate.split('/');
            const dateToPrev = tempHomelessPeriodPrevious.toDate.split('/');
            let tempDateFrom = new Date(Date.parse(tempHomelessPeriod.fromDate));
            const tempDateTo = new Date(Date.parse(tempHomelessPeriod.toDate));
            const dateTo = tempHomelessPeriod.toDate.split('/');
            const dateFrom = tempHomelessPeriod.fromDate.split('/');

            if (chartID[2] === dateTo[0] && chartID[1] === dateTo[2] &&
              this.getLastDateOfMonth(dateTo[0], +dateTo[2]) > dateTo[1]) {
              let tempDate = new Date(tempHomelessPeriod.toDate);
              tempDate = new Date(tempDate.setDate(tempDate.getDate() + 1));
              tempDateFrom = tempDate;
              const fromDateMonth1 = tempDateFrom.getMonth() + 1;
              let fromDateDate = tempDateFrom.getDate() < 10 ? '0' + tempDateFrom.getDate() : tempDateFrom.getDate();
              const fromDateMonth = fromDateMonth1 < 10 ? '0' + fromDateMonth1 : fromDateMonth1;

              if (+dateFrom[1] > 6 && dateFrom[0] === chartID[2] && dateFrom[2] === chartID[1] && dateToPrev[0] !== dateFrom[0]) {
                fromDateDate = '01';
              }
              const fromDateYear = tempDateFrom.getFullYear();
              const popDate = fromDateMonth + '/' + fromDateDate + '/' + fromDateYear;
              const isFuture = this.isFutureDate(popDate);

              if (!isFuture) {
                this.housingForm.get('fromDateCtrl').setValue(new Date(popDate));
                this.fromDatePicker.open();
                this.housingForm.get('toDateCtrl').setValue('');
                this.housingForm.get('housingTypeCtrl').setValue(0);
                this.onHousingTypeChange();
                period = 1000;
              } else {
                this.housingForm.get('fromDateCtrl').setValue('');
                this.housingForm.get('toDateCtrl').setValue('');
                this.housingForm.get('housingTypeCtrl').setValue(0);
                this.onHousingTypeChange();
              }

              const fromDateDateAdded = +fromDateDate + 5;
              if ((fromDateDateAdded < +dateToNext[1] && dateToNext[0] === fromDateMonth && +dateToNext[2] === fromDateYear) ||
                (fromDateDateAdded > +dateToNext[1] && dateToNext[0] > fromDateMonth && +dateToNext[2] === fromDateYear)) {
                count = 1;
                period = 1000;
              }

            }
            if (chartID[2] === dateTo[0] && chartID[1] === dateTo[2] && count === 0 &&
              this.getLastDateOfMonth(dateTo[0], +dateTo[2]) > dateTo[1]) {
              count = 1;
            }
          }

          if (count === 0) {
            const isFuture = this.isFutureDate(EDate);

            if (!isFuture) {
              this.housingForm.get('fromDateCtrl').setValue(new Date(EDate));
              this.fromDatePicker.open();
              this.housingForm.get('toDateCtrl').setValue('');
              this.housingForm.get('housingTypeCtrl').setValue(0);
              this.onHousingTypeChange();

            } else {
              this.housingForm.get('fromDateCtrl').setValue('');
              this.housingForm.get('toDateCtrl').setValue('');
              this.housingForm.get('housingTypeCtrl').setValue(0);
              this.onHousingTypeChange();
            }
          }


        } else {
          this.housingForm.get('fromDateCtrl').setValue('');
          this.housingForm.get('toDateCtrl').setValue('');
          this.housingForm.get('housingTypeCtrl').setValue(0);
          this.onHousingTypeChange();
        }
        if (this.housingForm.get('fromDateCtrl').value) {
          // this.housingForm.get('fromDateCtrl').focus();
          // clearUnknown();
        }
      }
    }
  }

  onToDateFocus = () => {
    if (!this.housingForm.get('toDateCtrl').value) {
      const homelessPeriod = this.housingData.pactHousingPeriod;
      for (let period = 0; period < homelessPeriod.length; period++) {
        const tempHomelessPeriod = homelessPeriod[period];
        let tempDateFrom = new Date(Date.parse(tempHomelessPeriod.fromDate));
        const fromDate = new Date(Date.parse(this.housingForm.get('fromDateCtrl').value));
        if (tempDateFrom > fromDate) {
          let tempDate = new Date(tempHomelessPeriod.fromDate);
          tempDate = new Date(tempDate.setDate(tempDate.getDate() - 1));
          tempDateFrom = tempDate;
          const tempToDateMonth = tempDateFrom.getMonth() + 1;
          const toDateDate = tempDateFrom.getDate() < 10 ? '0' + tempDateFrom.getDate() : tempDateFrom.getDate();
          const toDateYear = tempDateFrom.getFullYear();
          const toDateMonth = tempToDateMonth < 10 ? '0' + tempToDateMonth : tempToDateMonth;
          const cTempEndDate = toDateMonth + '/' + toDateDate + '/' + toDateYear;
          this.housingForm.get('toDateCtrl').setValue(new Date(cTempEndDate));
          this.toDatePicker.open();
          period = 1000;
        } else {

        }
      }
      if (!this.housingForm.get('toDateCtrl').value) {
        this.housingForm.get('toDateCtrl').setValue(new Date(this.currentHousingDate));
        this.toDatePicker.open();
        // console.log('endless todate');
        // $("#EndDate").datepicker({ maxDate: +0 });
      }
      // clearUnknown();
    }

  }

  onToDateChange = () => {
    if (this.isFutureDate(this.datePipe.transform(this.housingForm.get('toDateCtrl').value, 'MM/dd/yyyy'))) {
      this.housingForm.get('toDateCtrl').setValue(this.currentHousingDate);
    }
    this.housingForm.get('housingTypeCtrl').setValue(0);
    this.onHousingTypeChange();
  }

  ValidateFromToDates = () => {
    if (this.housingForm.get('fromDateCtrl').value > this.housingForm.get('toDateCtrl').value) {
      this.toastr.error('FromDate should be before ToDate.');
      return false;
    } else {
      return true;
    }
  }

  ValidateOverlapDates = () => {
    const tempDateStart = this.housingForm.get('fromDateCtrl').value;
    const tempDateEnd = this.housingForm.get('toDateCtrl').value;
    const HomelessData = this.housingData.pactHousingPeriod;
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < HomelessData.length; i++) {
      const Homeless = HomelessData[i];
      const tempDateFrom = Date.parse(Homeless.fromDate);
      const tempDateTo = Date.parse(Homeless.toDate);

      if (((tempDateStart <= tempDateFrom) && (tempDateEnd >= tempDateFrom)) ||
        ((tempDateStart <= tempDateTo) && (tempDateEnd >= tempDateTo)) ||
        ((tempDateStart >= tempDateFrom) && (tempDateEnd <= tempDateTo))) {
        this.toastr.error('Dates are overlapping with the existing episode.');
        return false;
      }
    }
    return true;
  }

  isFutureDate(inputDate) {
    const now = new Date(this.currentHousingDate);
    const futureDate = new Date(Date.parse(inputDate));
    if (futureDate > now) {
      this.toastr.info('Housing homeless history is required only until ' + this.datePipe.transform(now, 'MM/dd/yyyy'));
      return true;
    } else {
      return false;
    }
  }

  leapYear(year: number) {
    return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
  }

  getLastDateOfMonth(cMonth: string, cYear: number) {
    let cDate = '';
    if (cMonth === '01' || cMonth === '03' || cMonth === '05' || cMonth === '07' || cMonth === '08' || cMonth === '10' || cMonth === '12') {
      cDate = '31';
    } else if (cMonth === '04' || cMonth === '06' || cMonth === '09' || cMonth === '11') {
      cDate = '30';
    } else {
      cDate = this.leapYear(cYear) ? '29' : '28';
    }
    return cDate;
  }

  saveHousingHomeless = () => {
    this.housingEpisode.pactApplicationID = this.pactApplicationID;
    this.housingEpisode.fromDate = this.housingForm.get('fromDateCtrl').value;
    this.housingEpisode.toDate = this.housingForm.get('toDateCtrl').value;
    this.housingEpisode.housingType = this.housingForm.get(
      'housingTypeCtrl'
    ).value;
    this.housingEpisode.facilityName = this.housingForm.get(
      'facilityNameCtrl'
    ).value;
    this.housingEpisode.streetAddress = this.housingForm.get(
      'streetCtrl'
    ).value;
    this.housingEpisode.city = this.housingForm.get('cityCtrl').value;
    this.housingEpisode.state = this.housingForm.get('stateCtrl').value;
    this.housingEpisode.homelessStayDocumentedType = this.housingForm.get(
      'homelessStayDocCtrl'
    ).value;
    this.housingEpisode.nursingHomeRemedyMemberType = this.housingForm.get(
      'nursingHomeCtrl'
    ).value;
    this.housingEpisode.canReturnToPriorResidenceType = this.housingForm.get(
      'canReturnCtrl'
    ).value;
    this.housingEpisode.doesRequireSupportiveHousingType = this.housingForm.get(
      'doesRequireCtrl'
    ).value;
    this.housingEpisode.canReturnToPriorResidenceComment = this.housingForm.get(
      'canReturnExplainCtrl'
    ).value;
    this.housingEpisode.doesRequireSupportiveHousingComment = this.housingForm.get(
      'doesRequireExplainCtrl'
    ).value;

    this.homelessServiceSub = this.housingHistoryService.saveHousingHomeless(this.housingEpisode).subscribe(res => {
      if (res) {
        this.getHousingHomeless();
        this.resetValues();
        this.toastr.success('Homeless Data has been saved.');
      } else {
        this.toastr.error('There was an error in saving the housing homeless data');
      }
    },
      error => {
        this.toastr.error('There was an error in saving the housing homeless data');
      }
    );
  }

  //Update CTH
  updateCTHParent(updateCTH: UpdateCTH) {
    this.homelessServiceSub = this.housingHistoryService.updateCTH(updateCTH)
      .subscribe(res => {
        if (res) {
          this.getHousingHomeless();
          this.isCthChanged.emit();
          if (!this.toastr.currentlyActive) {
            this.toastr.success('CTH has been updated.');
          }
        } else {
          if (!this.toastr.currentlyActive) {
            this.toastr.error('There was an error in updating the CTH.');
          }
        }
      },
        error => {
          if (!this.toastr.currentlyActive) {
            this.toastr.error('There was an error in updating the CTH.');
          }
        }
      );
  }

  episodeDeleteParent = (dataSelected: DeleteHousingEpisode) => {
    const title = 'Confirm Delete';
    const primaryMessage = 'The Housing Episode will be deleted permanently.';
    const secondaryMessage =
      'Are you sure you want to delete the selected episode?';
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService
      .confirmDialog(
        title,
        primaryMessage,
        secondaryMessage,
        confirmButtonName,
        dismissButtonName
      )
      .then(
        positiveResponse => {
          this.homelessServiceSub = this.housingHistoryService.deleteHousingHomeless(dataSelected).subscribe(res => {
            if (res) {
              this.getHousingHomeless();
              this.toastr.success('Homeless data has been deleted.');
            } else {
              this.toastr.error('There was an error in deleting the homeless data.');
            }
          },
            error => {
              this.toastr.error('There was an error in deleting the document.');
            }
          );
        },
        negativeResponse => console.log()
      );
  }

  onHousingTypeChange = () => {
    this.housingTypeSelected = this.housingForm.get('housingTypeCtrl').value;
    const formToDate = this.datePipe.transform(this.housingForm.get('toDateCtrl').value, 'MM/dd/yyyy');
    if (!formToDate && this.housingTypeSelected > 0) {
      this.housingForm.get('housingTypeCtrl').setValue(0);
      this.toastr.error('Please set FromDate and ToDate to proceed.');
    }
    const appDate = this.datePipe.transform(this.currentHousingDate, 'MM/dd/yyyy');
    // console.log(formToDate + '----' + appDate);
    this.isCurrentDate = formToDate === appDate ? true : false;

    if (this.housingTypeSelected === 429) {
      this.housingForm.get('facilityNameCtrl').setValue('UNKNOWN');
      this.housingForm.get('streetCtrl').setValue('UNKNOWN');
      this.housingForm.get('cityCtrl').setValue('UNKNOWN');
      this.housingForm.get('stateCtrl').setValue('UN');
    } else {
      this.housingForm.get('facilityNameCtrl').setValue('');
      this.housingForm.get('streetCtrl').setValue('');
      this.housingForm.get('cityCtrl').setValue('');
      this.housingForm.get('stateCtrl').setValue('');
    }

    if (
      this.housingTypeSelected === 411 ||
      this.housingTypeSelected === 412 ||
      this.housingTypeSelected === 415 ||
      this.housingTypeSelected === 419 ||
      this.housingTypeSelected === 426 ||
      this.housingTypeSelected === 428 ||
      this.housingTypeSelected === 430 ||
      this.housingTypeSelected === 431 ||
      this.housingTypeSelected === 538
    ) {
      this.housingTypeHomelessStay = true;
    } else {
      this.housingTypeHomelessStay = false;
      this.housingForm.get('homelessStayDocCtrl').setValue('');
    }
    if (this.housingTypeSelected === 422 && this.isCurrentDate) {
      this.housingTypeNursingHome = true;
    } else {
      this.housingTypeNursingHome = false;
      this.housingForm.get('nursingHomeCtrl').setValue('');
    }
    if (
      (this.housingTypeSelected === 409 ||
        this.housingTypeSelected === 410 ||
        this.housingTypeSelected === 417 ||
        this.housingTypeSelected === 418 ||
        this.housingTypeSelected === 422 ||
        this.housingTypeSelected === 423 ||
        this.housingTypeSelected === 424 ||
        this.housingTypeSelected === 425 ||
        this.housingTypeSelected === 537 ||
        this.housingTypeSelected === 539) &&
      this.isCurrentDate
    ) {
      this.housingTypeCanReturn = true;
      this.housingTypeDoesRequire = true;
    } else {
      this.housingTypeCanReturn = false;
      this.housingTypeDoesRequire = false;
      this.housingForm.get('canReturnCtrl').setValue('');
      this.housingForm.get('doesRequireCtrl').setValue('');
      this.onCanReturnChange();
      this.onDoesRequireChange();
    }
  }

  onCanReturnChange = () => {
    if (this.housingForm.get('canReturnCtrl').value === 34) {
      this.canReturn = true;
    } else {
      this.canReturn = false;
      this.housingForm.get('canReturnExplainCtrl').setValue('');
    }
  }

  onDoesRequireChange = () => {
    if (this.housingForm.get('doesRequireCtrl').value === 33) {
      this.doesRequire = true;
    } else {
      this.doesRequire = false;
      this.housingForm.get('doesRequireExplainCtrl').setValue('');
    }
  }

  isFormValid = (): boolean => {
    if (
      this.formErrors.fromDateCtrl ||
      this.formErrors.toDateCtrl ||
      this.formErrors.housingTypeCtrl ||
      this.formErrors.facilityNameCtrl ||
      this.formErrors.streetCtrl ||
      this.formErrors.cityCtrl ||
      this.formErrors.stateCtrl ||
      this.formErrors.homelessStayDocCtrl ||
      this.formErrors.nursingHomeCtrl ||
      this.formErrors.canReturnCtrl ||
      this.formErrors.doesRequireCtrl ||
      this.formErrors.canReturnExplainCtrl ||
      this.formErrors.doesRequireExplainCtrl ||
      !this.ValidateFromToDates() ||
      !this.ValidateOverlapDates() ||
      this.isFutureDate(this.datePipe.transform(this.housingForm.get('toDateCtrl').value, 'MM/dd/yyyy'))
    ) {
      return false;
    } else {
      return true;
    }
  }

  // openTrainingVideo = () => {
  //   this.commonService.OpenWindow('./assets/video/HUDTraining.swf');
  // }

  resetValues = () => {
    this.housingForm.get('fromDateCtrl').setValue('');
    this.housingForm.get('toDateCtrl').setValue('');
    this.housingForm.get('housingTypeCtrl').setValue(0);
    this.housingForm.get('facilityNameCtrl').setValue('');
    this.housingForm.get('streetCtrl').setValue('');
    this.housingForm.get('cityCtrl').setValue('');
    this.housingForm.get('stateCtrl').setValue('');
    this.onHousingTypeChange();
    this.onCanReturnChange();
    this.onDoesRequireChange();

    this.clearValidators();
  }

  clearValidators = () => {
    this.housingForm.controls.fromDateCtrl.clearValidators();
    this.housingForm.controls.toDateCtrl.clearValidators();
    this.housingForm.controls.housingTypeCtrl.clearValidators();
    this.housingForm.controls.facilityNameCtrl.clearValidators();
    this.housingForm.controls.streetCtrl.clearValidators();
    this.housingForm.controls.cityCtrl.clearValidators();
    this.housingForm.controls.stateCtrl.clearValidators();
    this.housingForm.controls.homelessStayDocCtrl.clearValidators();
    this.housingForm.controls.nursingHomeCtrl.clearValidators();
    this.housingForm.controls.canReturnCtrl.clearValidators();
    this.housingForm.controls.doesRequireCtrl.clearValidators();
    this.housingForm.controls.canReturnExplainCtrl.clearValidators();
    this.housingForm.controls.doesRequireExplainCtrl.clearValidators();

    this.updateValidators();
  }

  updateValidators = () => {
    this.housingForm.controls.fromDateCtrl.updateValueAndValidity();
    this.housingForm.controls.toDateCtrl.updateValueAndValidity();
    this.housingForm.controls.housingTypeCtrl.updateValueAndValidity();
    this.housingForm.controls.facilityNameCtrl.updateValueAndValidity();
    this.housingForm.controls.streetCtrl.updateValueAndValidity();
    this.housingForm.controls.cityCtrl.updateValueAndValidity();
    this.housingForm.controls.stateCtrl.updateValueAndValidity();
    this.housingForm.controls.homelessStayDocCtrl.updateValueAndValidity();
    this.housingForm.controls.nursingHomeCtrl.updateValueAndValidity();
    this.housingForm.controls.canReturnCtrl.updateValueAndValidity();
    this.housingForm.controls.doesRequireCtrl.updateValueAndValidity();
    this.housingForm.controls.canReturnExplainCtrl.updateValueAndValidity();
    this.housingForm.controls.doesRequireExplainCtrl.updateValueAndValidity();
  }

  setValidators = () => {
    this.housingForm.controls.fromDateCtrl.setValidators([Validators.required]);
    this.housingForm.controls.toDateCtrl.setValidators([Validators.required]);
    this.housingForm.controls.housingTypeCtrl.setValidators([
      CustomValidators.dropdownRequired()
    ]);
    this.housingForm.controls.facilityNameCtrl.setValidators([
      Validators.required, CustomValidators.requiredCharLength(2)
    ]);
    this.housingForm.controls.streetCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);
    this.housingForm.controls.cityCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);
    this.housingForm.controls.stateCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);

    this.housingForm.controls.homelessStayDocCtrl.setValidators([
      Validators.required
    ]);
    this.housingForm.controls.nursingHomeCtrl.setValidators([
      Validators.required
    ]);
    this.housingForm.controls.canReturnCtrl.setValidators([
      Validators.required
    ]);
    this.housingForm.controls.doesRequireCtrl.setValidators([
      Validators.required
    ]);
    this.housingForm.controls.canReturnExplainCtrl.setValidators([
      Validators.required, CustomValidators.requiredCharLength(2)
    ]);
    this.housingForm.controls.doesRequireExplainCtrl.setValidators([
      Validators.required, CustomValidators.requiredCharLength(2)
    ]);

    this.updateValidators();

    // this.housingForm.updateValueAndValidity();
  }

  logValidationErrors = (group: FormGroup = this.housingForm): void => {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      } else {
        this.formErrors[key] = '';
        if (
          abstractControl &&
          !abstractControl.valid &&
          (abstractControl.touched || abstractControl.dirty)
        ) {
          const messages = this.validationMessages[key];
          for (const errorKey in abstractControl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + ' ';
            }
          }
        }
      }
    });
    if (
      !(this.formErrors.homelessStayDocCtrl && this.housingTypeHomelessStay)
    ) {
      this.formErrors.homelessStayDocCtrl = '';
    }
    if (!(this.formErrors.nursingHomeCtrl && this.housingTypeNursingHome)) {
      this.formErrors.nursingHomeCtrl = '';
    }
    if (!(this.formErrors.canReturnCtrl && this.housingTypeCanReturn)) {
      this.formErrors.canReturnCtrl = '';
    }
    if (!(this.formErrors.doesRequireCtrl && this.housingTypeDoesRequire)) {
      this.formErrors.doesRequireCtrl = '';
    }
    if (!(this.formErrors.canReturnExplainCtrl && this.canReturn)) {
      this.formErrors.canReturnExplainCtrl = '';
    }
    if (!(this.formErrors.doesRequireExplainCtrl && this.doesRequire)) {
      this.formErrors.doesRequireExplainCtrl = '';
    }
  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  togglePanel = () => {
    // console.log(this.panelExpanded);
    // this.panelExpanded = !this.panelExpanded;
    this.panelStateChanged.emit(this.panelExpanded);
  }

  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.homelessServiceSub) {
      this.homelessServiceSub.unsubscribe();
    }
    if (this.c2vServiceSub) {
      this.c2vServiceSub.unsubscribe();
    }
    if (this.homelessFormSub) {
      this.homelessFormSub.unsubscribe();
    }
  }


}

