import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Tutorial } from './tutorial.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit {

  videoLink: Tutorial;

  @Input() pageName: string;
  @Input() tutorialIconColor = '#3f51b5';

  constructor(private commonService: CommonService,
              public dialog: MatDialog,
              private toastr: ToastrService, ) { }

  ngOnInit() {
    if (this.pageName) {
      this.commonService.getVideoURL(this.pageName).subscribe(res => {
        if (res) {
          this.videoLink = res.body as Tutorial;
        }
      },
        error => console.error('Error!', error)
      );
    }
  }


  openVideo() {
    if (this.videoLink && this.videoLink.url) {
      this.commonService.OpenWindow(this.videoLink.url);
    } else {
      this.toastr.error('Video tutorial is not available', '');
    }
  }
}
