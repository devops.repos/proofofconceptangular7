import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IDeleteCalendarEvent, IVCSUpdateHPSitePreferredSlots } from './pact-calendar-interface.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PactCalendarService {
  private apiURL = environment.pactApiUrl;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  //#region Getting the Calender Events for VCS C2V
  // getVCSC2VCalendarEventsList(pactApplicationID: number) {
  //   return this.http.post(this.apiURL + 'VCSReferral/GetVCSC2VCalendarEventsList', pactApplicationID, this.httpOptions);
  // }
  //#endregion

  //#region Delete the Calendar Event for C2V
  deleteVCSCalendarEvent(event: IDeleteCalendarEvent) {
    return this.http.post(this.apiURL + 'VCSReferral/DeleteVCSInterviewScheduled', JSON.stringify(event), this.httpOptions);
  }
  //#endregion

  //#region Getting the Calender Events for VCS V2C
  getVCSSiteCalendarEventsList(siteID: number) {
    return this.http.post(this.apiURL + 'VCSReferral/GetVCSSiteCalendarEventsList', siteID, this.httpOptions);
  }
  //#endregion

  getVCSHPSitePreferredSlots(hpSiteId: number) {
    return this.http.post(this.apiURL + 'VCSReferral/GetVCSHPSitePreferredSlots', hpSiteId, this.httpOptions);
  }

  updateVCSHPSitePreferredSlots(input: IVCSUpdateHPSitePreferredSlots) {
    return this.http.post(this.apiURL + 'VCSReferral/UpdateVCSHPSitePreferredSlots', JSON.stringify(input), this.httpOptions);
  }

}
