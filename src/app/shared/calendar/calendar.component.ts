import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject, Subscription, BehaviorSubject } from 'rxjs';
// import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  CalendarMonthViewDay,
  CalendarWeekViewBeforeRenderEvent,
  CalendarDateFormatter
} from 'angular-calendar';
import { IVCSSiteCalendarEvents, IVCSAgencySiteInfoForCalendar, IVCSHPSitePreferredSlots, IVCSUpdateHPSitePreferredSlots } from './pact-calendar-interface.model';
import { CalendarFor, UserSiteType } from 'src/app/models/pact-enums.enum';
import { PactCalendarService } from './pact-calendar.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ConfirmDialogService } from '../confirm-dialog/confirm-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { WeekViewHour, WeekViewHourColumn } from 'calendar-utils';
import { AuthData } from 'src/app/models/auth-data.model';

const colors: any = {
  red: {
    primary: '#B71C1C',
    secondary: '#FFCDD2'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  green: {
    primary: '#22A213',
    secondary: '#B7F1BE'
  }
};

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, OnDestroy {
  @Input() calendarFor: string;    // HPSiteObservable, HPSite
  @Input() agencySiteInfo: IVCSAgencySiteInfoForCalendar;   // for Agency/Site Info to be displayed on top of calendar
  @Input() hpSiteID: number;   // for regular one time call, (if no refresh required)
  @Input() hpSiteIDObservable: BehaviorSubject<number> = new BehaviorSubject<number>(0);  // If calendar has to be refereshed (called again and again) because of some action
  @Input() preferredSlotEdit: boolean = false;   // to check uncheck the time slot (to color code the time slot, green color slot)
  @Input() calendarViewFor: string = "Month";  // Default view of Calendar
  @Input() editDeleteAction: boolean = false;  // edit/delete action button on events
  selectedHpSiteID: number = 0;

  @Output() eventToBeEdited = new EventEmitter();

  private userLanID: string;
  private userData: AuthData;

  // @ViewChild("modalContent") modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<span class="calendar-event-edit-icon"></span>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edit', event);
      }
    },
    {
      label: '<span class="calendar-event-delete-icon"></span>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        // this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Delete', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  // createdBy;

  events: CalendarEvent[];
  siteEvents: IVCSSiteCalendarEvents[] = [];

  selectedDayViewDate: Date;
  hourColumns: WeekViewHourColumn[];
  preferredSlots: any[] = [];

  activeDayIsOpen: boolean = true;

  hpSiteIDObservableSub: Subscription;

  constructor(
    private pactCalendarService: PactCalendarService,
    private userService: UserService,
    private confirmDialogService: ConfirmDialogService,
    private toastr: ToastrService,
    // private modal: NgbModal
  ) { }

  ngOnInit() {
    /** Get the current user info */
    this.userService.getUserDataAsPromise().then(usrData => {
      if (usrData) {
        this.userData = usrData;
        this.userLanID = usrData.lanId;
          if (this.hpSiteID > 0 && this.calendarFor == CalendarFor.HPSite) {
            this.selectedHpSiteID = this.hpSiteID;
            this.getHpPreferredInterviewSlots();
            this.setCalendarView();
            this.getSiteCalendarEvents();
          } else if (this.calendarFor == CalendarFor.HPSiteObservable) {
            this.hpSiteIDObservableSub = this.hpSiteIDObservable.subscribe(siteId => {
              if (siteId > 0) {
                this.selectedHpSiteID = siteId;
                this.getHpPreferredInterviewSlots();
                this.setCalendarView();
                this.getSiteCalendarEvents();
              }
            });
          }
      }

    });
  }

  setCalendarView() {
    /* Set Calendar View */
    if (this.calendarViewFor.toString().toLowerCase() == "month") {
      this.setView(CalendarView.Month);
    } else if (this.calendarViewFor.toString().toLowerCase() == "week") {
      this.setView(CalendarView.Week);
    } else if (this.calendarViewFor.toString().toLowerCase() == "day") {
      this.setView(CalendarView.Day);
    }
  }

  getHpPreferredInterviewSlots() {
    /* Get the HP preferred Interview Slots */
    if (this.selectedHpSiteID > 0) {
      this.preferredSlots = [];
      this.pactCalendarService.getVCSHPSitePreferredSlots(this.selectedHpSiteID).subscribe((pref: IVCSHPSitePreferredSlots[]) => {
        if (pref.length > 0) {
          pref.forEach(p => {
            if (p.hour08Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 8)).getTime());
            }
            if (p.hour09Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 9)).getTime());
            }
            if (p.hour10Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 10)).getTime());
            }
            if (p.hour11Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 11)).getTime());
            }
            if (p.hour12Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 12)).getTime());
            }
            if (p.hour13Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 13)).getTime());
            }
            if (p.hour14Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 14)).getTime());
            }
            if (p.hour15Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 15)).getTime());
            }
            if (p.hour16Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 16)).getTime());
            }
            if (p.hour17Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 17)).getTime());
            }
            if (p.hour18Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 18)).getTime());
            }
            if (p.hour19Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 19)).getTime());
            }
            if (p.hour20Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 20)).getTime());
            }
            if (p.hour21Preferred) {
              this.preferredSlots.push((addHours(new Date(p.interviewDate), 21)).getTime());
            }
          });
          // console.log(this.preferredSlots);
        }
      })
    }
  }

  getSiteCalendarEvents() {
    if (this.selectedHpSiteID > 0) {
      this.viewDate = new Date();
      // console.log('selectedHPSiteID: ', this.selectedHpSiteID);
      this.pactCalendarService.getVCSSiteCalendarEventsList(this.selectedHpSiteID).subscribe((res: IVCSSiteCalendarEvents[]) => {
        this.siteEvents = res;
        // console.log('calender Events: ', this.siteEvents);
        this.events = [];
        const eventList = [];
        this.siteEvents.forEach(val => {
          /** Converting time to 24 hours format to inject the hour value in calendar */
          const timee = val.interviewTime;
          const splt = timee.split(':');
          let hrs = +splt[0];
          const mns = +splt[1].substr(0, 2);
          const AMPM = splt[1].substr(2, 2);
          if (AMPM == 'PM' && hrs < 12) { hrs = hrs + 12; }
          if (AMPM == 'AM' && hrs == 12) { hrs = hrs - 12; }
          let sHours = hrs.toString();
          let sMinutes = mns.toString();
          if (hrs < 10) { sHours = '0' + sHours; }
          if (mns < 10) { sMinutes = '0' + sMinutes; }
          const createdBy = val.createdBy || val.updatedBy;
          const obj = {
            id: val.vcsReferralID,
            start: addHours(new Date(val.interviewDate), hrs),
            title: createdBy == this.userLanID ? (val.firstName + val.lastName + ' @' + val.interviewTime) : (val.interviewTime + ' Occupied'),
            color: colors.red,
            // actions: createdBy == this.userLanID && this.editDeleteAction ? this.actions : null
            actions: null
          };
          eventList.push(obj);
        });
        this.events = eventList;


        // this.events = [];
        // this.siteEvents = res;
        // /* ######### listing out all the distinct interview Dates from the calendar event ####### */
        // var intrvwDates = [];
        // this.siteEvents.forEach(s => { intrvwDates.push(s.interviewDate) });
        // const distinct = (value, index, self) => {
        //   return self.indexOf(value) === index;
        // }
        // const distinctIntrvwDates = intrvwDates.filter(distinct);
        // console.log('distinctIntrvwDates: ', distinctIntrvwDates);
        // /* ###################################################################################### */
        // var eventList: CalendarEvent[] = [];
        // distinctIntrvwDates.forEach(d => {
        //   eventList = this.createEmptyEvents(d);
        //   this.siteEvents.forEach(val => {
        //     if (d == val.interviewDate) {
        //       /** Converting time to 24 hours format to inject the hour value in calendar */
        //       const timee = val.interviewTime;
        //       const splt = timee.split(':');
        //       let hrs = +splt[0];
        //       // const mns = +splt[1].substr(0, 2);
        //       const AMPM = splt[1].substr(2, 2);
        //       if (AMPM == 'PM' && hrs < 12) { hrs = hrs + 12; }
        //       if (AMPM == 'AM' && hrs == 12) { hrs = hrs - 12; }
        //       const createdBy = val.createdBy || val.updatedBy;
        //       eventList.forEach(e => {
        //         if (e.id == hrs) {
        //           e.id = val.vcsReferralID;
        //           e.start = addHours(new Date(val.interviewDate), hrs);
        //           e.title = (val.interviewTime + ' - Interview Scheduled for ' + val.firstName + val.lastName);
        //           e.color = colors.red;
        //           e.actions = createdBy == this.userLanID && this.calendarFor == CalendarFor.HPSite ? this.actions : null;
        //           // e.meta = { incrementsBadgeTotal: true };
        //         }
        //       });
        //       // eventList.push(obj);
        //     }
        //   });
        //   this.events = eventList;
        // });
      });
    }
  }

  createEmptyEvents(date: string): CalendarEvent[] {
    var emptyEvents: CalendarEvent<{ incrementsBadgeTotal: boolean }>[] = [
      {
        id: 8,
        start: addHours(new Date(date), 8),
        title: '8:00AM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 9,
        start: addHours(new Date(date), 9),
        title: '9:00AM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 10,
        start: addHours(new Date(date), 10),
        title: '10:00AM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 11,
        start: addHours(new Date(date), 11),
        title: '11:00AM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 12,
        start: addHours(new Date(date), 12),
        title: '12:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 13,
        start: addHours(new Date(date), 13),
        title: '1:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 14,
        start: addHours(new Date(date), 14),
        title: '2:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 15,
        start: addHours(new Date(date), 15),
        title: '3:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 16,
        start: addHours(new Date(date), 16),
        title: '4:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 17,
        start: addHours(new Date(date), 17),
        title: '5:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 18,
        start: addHours(new Date(date), 18),
        title: '6:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 19,
        start: addHours(new Date(date), 19),
        title: '7:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 20,
        start: addHours(new Date(date), 20),
        title: '8:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      },
      {
        id: 21,
        start: addHours(new Date(date), 21),
        title: '9:00PM - Available',
        color: colors.green,
        meta: {
          incrementsBadgeTotal: false,
        },
      }
    ];
    return emptyEvents;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true)) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  // eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
  //   this.events = this.events.map(iEvent => {
  //     if (iEvent === event) {
  //       return {
  //         ...event,
  //         start: newStart,
  //         end: newEnd
  //       };
  //     }
  //     return iEvent;
  //   });
  //   this.handleEvent('Dropped or resized', event);
  // }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    // this.modal.open(this.modalContent, { size: "lg" });
    // console.log(this.modalData);

    //#region  Deleting the Calendar Event
    if (action == 'Delete') {
      let eventToBeDeleted;
      if (this.calendarFor == CalendarFor.HPSite) {
        eventToBeDeleted = this.siteEvents;
        eventToBeDeleted.forEach(intrvw => {
          if (intrvw.vcsReferralID == event.id) {
            let intrvwToBeDeleted = {
              VCSReferralID: intrvw.vcsReferralID,
              SiteAgreementPopulationID: intrvw.siteAgreementPopulationID,
              InterviewDate: intrvw.interviewDate,
              SlotName: intrvw.slotName,
              UpdatedBy: intrvw.updatedBy
            };
            // alert('delete : ' + intrvwToBeDeleted.VCSReferralID + ', ' + intrvwToBeDeleted.SiteAgreementPopulationID + ', ' + intrvwToBeDeleted.InterviewDate + ', ' + intrvwToBeDeleted.SlotName + ', ' + intrvwToBeDeleted.UpdatedBy);
            const title = 'Confirm Delete';
            const primaryMessage = `The Interview will be deleted permanently.`;
            const secondaryMessage = `Are you sure you want to delete the selected Interview?`;
            const confirmButtonName = 'Yes';
            const dismissButtonName = 'No';

            this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
              (positiveResponse) => {
                this.pactCalendarService.deleteVCSCalendarEvent(intrvwToBeDeleted).subscribe(refID => {
                  if (refID > 0) {
                    this.deleteEvent(event);
                    this.toastr.success('Inteview has been Deleted Successfully !', 'Deletion Successful.');
                  }
                });
              },
              (negativeResponse) => { },
            );
          }
        });
      }
    }
    //#endregion

    //#region Edit the Calendar Event (Edit Interview Date/Time)
    if (action == 'Edit') {
      let evntToBeEdited;
      if (this.calendarFor == CalendarFor.HPSite) {
        evntToBeEdited = this.siteEvents;
      }
      evntToBeEdited.forEach(intrvw => {
        if (intrvw.vcsReferralID == event.id) {
          this.eventToBeEdited.emit(intrvw);
        }
      });
    }
    //#endregion

  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  refreshView(): void {
    /* call this function to refresh the Calendar */
    // this.cssClass = this.cssClass === RED_CELL ? BLUE_CELL : RED_CELL;
    this.refresh.next();
  }

  // beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
  //   body.forEach((day) => {
  //     day.badgeTotal = day.events.filter((event) => event.meta.incrementsBadgeTotal).length;
  //   });
  // }

  hourSegmentClickedOnWeekView(dateClicked: Date) {
    var date = new Date();
    date.setDate(date.getDate() + 5);
    this.selectedDayViewDate = dateClicked;
    const pe = this.userData.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_PE);
    if (dateClicked >= date && this.preferredSlotEdit && pe.length == 0) {
      this.addSelectedDayViewClass();
    }
  }

  beforeWeekOrDayViewRender(event: CalendarWeekViewBeforeRenderEvent) {
    this.hourColumns = event.hourColumns;
    this.hourColumns.forEach((column) => {
      column.hours.forEach((hourSegment) => {
        hourSegment.segments.forEach((segment) => {
          const dateIndex = this.preferredSlots.indexOf(segment.date.getTime());
          if (dateIndex > -1) {
            segment.cssClass = 'cal-day-selected';
          }
        });
      });
    });
  }


  private addSelectedDayViewClass() {
    this.hourColumns.forEach((column) => {
      column.hours.forEach((hourSegment) => {
        hourSegment.segments.forEach((segment) => {
          if (this.selectedDayViewDate && segment.date.getTime() === this.selectedDayViewDate.getTime()) {
            const dateIndex = this.preferredSlots.indexOf(this.selectedDayViewDate.getTime());
            if (dateIndex > -1) {
              /* Unchecking the preferred interview Slot */
              delete segment.cssClass;
              this.preferredSlots.splice(dateIndex, 1);
              this.updateVCSHPSitePreferredSlots(segment.date, false);

            } else {
              this.preferredSlots.push(this.selectedDayViewDate.getTime());
              segment.cssClass = 'cal-day-selected';
              segment.date = this.selectedDayViewDate;
              this.updateVCSHPSitePreferredSlots(segment.date, true);
            }
          }
        });
      });
    });
  }

  updateVCSHPSitePreferredSlots(segment: Date, state: boolean) {
    const interviewDate = segment.getFullYear() + '-' + (segment.getMonth() + 1) + '-' + segment.getDate();
    var pSlotName;
    if (segment.getHours() < 10) {
      pSlotName = 'Hour0' + segment.getHours().toString() + 'Preferred';
    } else if (segment.getHours() >= 10) {
      pSlotName = 'Hour' + segment.getHours().toString() + 'Preferred';
    }
    const updateInput: IVCSUpdateHPSitePreferredSlots = {
      HPSiteID: this.selectedHpSiteID,
      InterviewDate: interviewDate,
      PreferredSlotName: pSlotName,
      PreferredSlotValue: state
    }
    this.pactCalendarService.updateVCSHPSitePreferredSlots(updateInput).subscribe(res => {
      if (res <= 0) {
        this.toastr.error('Something went wrong while unchecking the preferred available Interview slot');
      }
    });
  }

  ngOnDestroy() {
    if (this.hpSiteIDObservableSub) {
      this.hpSiteIDObservableSub.unsubscribe();
    }
  }

}
