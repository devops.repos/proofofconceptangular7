export interface IDeleteCalendarEvent {
  VCSReferralID: number;
  SiteAgreementPopulationID: number;
  InterviewDate: string;
  SlotName: string;
  UpdatedBy: string;
}

export interface IVCSSiteCalendarEvents {
  vcsReferralID: number;
  pactApplicationID: number;
  vcsID: number;
  clientSourceType: number;
  referringAgencyID: number;
  referringAgencyNo: string;
  referringAgencyName: string;
  referringSiteID: number;
  referringSiteNo: string;
  referringSiteName: string;
  hpAgencyID: number;
  hpAgencyNo: string;
  hpAgencyName: string;
  hpSiteID: number;
  hpSiteNo: string;
  hpSiteName: string;
  clientID: number;
  clientNo: number;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  referralDate: string;
  ssn: string;
  clientEligibleFor: string;
  svaPrioritization: string;
  serviceNeeds: string;
  approvalFromDate: string;
  approvalToDate: string;
  placementCriteria: string;
  isReferralPackageReady: number;
  pendingReferrals: number;
  isHUD: boolean;
  isReferralHold: number;
  primaryServiceContractID: number;
  primaryServiceContractName: string;
  agreementPopulationID: number;
  populationName: string;
  siteAgreementPopulationID: number;
  agreementTypeDescription: string;
  siteLocationType: number;
  siteLocationTypeDescription: string;
  siteAddress: string;
  unitsOccupied: number;
  unitsAvailable: number;
  totalUnits: number;
  assignUnitType: number;
  assignUnitTypeDescription: string;
  matchPercentage: number;
  referralGroupGUID: string;
  referralType: number;
  isScheduleMandatory: number;
  referralStatusType: number;
  coCType: number;
  withdrawnReasonType: number;
  referralClosedComment: string;
  interviewDate: string;
  interviewTime: string;
  slotName: string;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhoneNo: string;
  createdBy: string;
  updatedBy: string;
}

export interface IVCSAgencySiteInfoForCalendar {
  agencyID: number;
  agencyNo: string;
  agencyName: string;
  siteID: number;
  siteNo: string;
  siteName: string;
}

export interface IVCSHPSitePreferredSlots {
  interviewDate: string;
  hour08Preferred: boolean;
  hour09Preferred: boolean;
  hour10Preferred: boolean;
  hour11Preferred: boolean;
  hour12Preferred: boolean;
  hour13Preferred: boolean;
  hour14Preferred: boolean;
  hour15Preferred: boolean;
  hour16Preferred: boolean;
  hour17Preferred: boolean;
  hour18Preferred: boolean;
  hour19Preferred: boolean;
  hour20Preferred: boolean;
  hour21Preferred: boolean;
}

export interface IVCSUpdateHPSitePreferredSlots {
  HPSiteID: number;
  InterviewDate: string;
  PreferredSlotName: string;
  PreferredSlotValue: boolean;
}
