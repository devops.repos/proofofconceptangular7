import { Injectable } from '@angular/core';
import {
  ConfirmDialogModel,
  ConfirmDialogComponent
} from './confirm-dialog.component';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDialogService {

  constructor(public dialog: MatDialog) { }

  // tslint:disable-next-line: max-line-length
  confirmDialog = (title: string, primaryMessage: string, secondaryMessage: string, confirmButtonName: string, dismissButtonName: string) => {
    const promise = new Promise((resolve, reject) => {
      const dialogData = new ConfirmDialogModel(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName);
      // console.log('value received: ', title);
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        // console.log('afterClosed value: ', dialogResult);
        if (dialogResult) {
          resolve();
        } else {
          reject();
        }
      });
    });
    return promise;
  }
}
