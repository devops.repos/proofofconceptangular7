import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  title: string;
  primaryMessage: string;
  secondaryMessage: string;
  confirmButtonName: string;
  dismissButtonName: string;

  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogModel) {
    // Update view with given values
    this.title = data.title;
    this.primaryMessage = data.primaryMessage;
    this.secondaryMessage = data.secondaryMessage;
    this.confirmButtonName = data.confirmButtonName;
    this.dismissButtonName = data.dismissButtonName;
  }

  ngOnInit() {
  }

  onConfirm(): void {
    // Close the dialog, return true
    this.dialogRef.close(true);
  }

  onDismiss(): void {
    // Close the dialog, return false
    this.dialogRef.close(false);
  }

}

export class ConfirmDialogModel {
  constructor(public title: string
             ,public primaryMessage: string
             ,public secondaryMessage: string
             ,public confirmButtonName: string
             ,public dismissButtonName: string) {
  }
}
