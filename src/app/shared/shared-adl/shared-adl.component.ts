import { Component, OnInit, OnDestroy, HostListener,

  Output,
  EventEmitter,
  Input,
  ViewChild
  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

import {RefGroupDetails} from '../../models/refGroupDetailsDropDown.model'
import { SharedADLService } from './shared-adls.service';
import { Subscription } from 'rxjs';
import { PACTADL } from './shared-adls.model';
import { CommonService } from '../../services/helper-services/common.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
import { ToastrService } from 'ngx-toastr';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { C2vService } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v.service';


@Component({
  selector: 'app-shared-adl',
  templateUrl: './shared-adl.component.html',
  styleUrls: ['./shared-adl.component.scss']
})
export class SharedAdlComponent implements OnInit, OnDestroy {
  constructor(private fb: FormBuilder,
    private adlService: SharedADLService,
    private commonService: CommonService,
    private userService: UserService,
    private route: ActivatedRoute,
    private sidenavStatusService: SidenavStatusService,
    private clientApplicationService: ClientApplicationService,
    private toastr: ToastrService,
    private clientService: ClientApplicationService,
    private router: Router,
    private c2vService: C2vService

  ) {

  }


  @Input() parentPage: string; //PACT
  @Input() applicationID: number =0;
  @Output() SaveAdl = new EventEmitter();
  // preformdData : any;
  adlFormgrp: FormGroup;

  modelPactADL = new PACTADL();
  userData: AuthData;
  userDataSub: Subscription;
  isTabComplete: boolean;
  tab1Status = 2; // 0 = Partial; 1 = Complete; 2 = null
  radiobtnValidation: boolean = true;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  routeSub: any; // Subscription to route observer

  supportTypes: RefGroupDetails[];
  radioBtnOptions: RefGroupDetails[];

   disabelSave: boolean = true;

   appID: number;
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  otherADlRequired:boolean = false;


  //Save Data on page refresh
  @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
    if (this.adlFormgrp.dirty) {
      this.saveADL();
    }
  };

  ngOnInit() {

    
    this.buildFormCtrls();   // build the form controls

console.log("VCS");
    //this.sidenavStatusService._sidenavStatusReload(this.route);

    if(this.parentPage == "VCS")
    {
      this.disabelSave = false;
      this.c2vService.getClientAwaitingPlacementSelected().subscribe(c2vRes => {
        this.applicationID = c2vRes.pactApplicationID;
        if(this.applicationID > 0) {
          this.loadReqData();
        }
      });
      this.adlFormgrp.disable() ;
    }
    else{

      
    /** To Load the Sidenav Completion Status for all the Application related Pages */
    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        console.log(appid);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.appID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.appID);
             /** If you have any logic that requires applicationID and if the Logic should be implemented in ngOnInit() (on component load). DO IT HERE */
             /** async logic that require applicationID, DO IT HERE */
            
             this.applicationID = this.appID;
             if(this.applicationID > 0) {
               this.loadReqData();
             }
        }
      }
    });

       
    }



    // if(this.parentPage == "VCS")
    // {
    //   this.disabelSave = false;
    //   this.c2vService.getClientAwaitingPlacementSelected().subscribe(c2vRes => {
    //     this.applicationID = c2vRes.pactApplicationID;
    //     if(this.applicationID > 0) {
    //       this.loadReqData();
    //     }
    //   });
    //   this.adlFormgrp.disable() ;

    // }
    // else{
    //   this.clientApplicationService.getPactApplicationID().subscribe(appID => {
    //     this.applicationID = appID;
    //     if(this.applicationID > 0) {
    //       this.loadReqData();
    //     }
    //   });

    // }

    this.adlFormgrp.get('otherADLSupportTypeCtrl').valueChanges.subscribe(val => {
       if (val == 10) {
        this.adlFormgrp.get('otherADLDetailsCtrl').clearValidators();
      }
        });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.adlFormgrp.touched && event.navigationTrigger != 'popstate') {
          this.saveADL();
        }
        if (event.navigationTrigger == 'popstate' && this.adlFormgrp.dirty) {
          this.saveADL();
        }

      };
    });
  }

  buildFormCtrls() {

    this.adlFormgrp = this.fb.group({

      personHygieneSupportTypeCtrl: ['', CustomValidators.dropdownRequired()],
      personHygieneDetailsCtrl: ['', Validators.compose([Validators.required])],

      travelMobilitySupportTypeCtrl: ['', CustomValidators.dropdownRequired()],
      travelMobilityDetailsCtrl: ['', Validators.compose([Validators.required])],

      shoppingSupportTypeCtrl: ['', CustomValidators.dropdownRequired()],
      shoppingDetailsCtrl: ['', Validators.compose([Validators.required])],

      manageFinanceSupportTypeCtrl: ['', CustomValidators.dropdownRequired()],
      manageFinanceDetailsCtrl: ['', Validators.compose([Validators.required])],

      aptUpkeepSupportTypeCtrl: ['', CustomValidators.dropdownRequired()],
      aptRoomUpkeepDetailsCtrl: ['', Validators.compose([Validators.required])],

      socialSkillsSupportTypeCtrl: ['', CustomValidators.dropdownRequired()],
      socialSkillsDetailsCtrl: ['', Validators.compose([Validators.required])],

      manageHealthSupportTypeCtrl: ['', CustomValidators.dropdownRequired()],
      manageHealthDetailsCtrl: ['', Validators.compose([Validators.required])],

     // otherADLSupportTypeCtrl: ['', CustomValidators.dropdownRequired()],
     otherADLSupportTypeCtrl: [''],
      otherADLDetailsCtrl: [''],

      // isReceivingAssistanceCtrl: ['', Validators.compose([Validators.required])],
      // receivingAssistanceDetailsDetailsCtrl: ['', Validators.compose([Validators.required])],

    });

  }


  loadReqData() {

    // Get RefGroup Details

    var value = '3,7';
    this.commonService.getRefGroupDetails(value)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.supportTypes = data.filter(d => d.refGroupID == 3);
          this.radioBtnOptions = data.filter(d => (d.refGroupDetailID == 33 || d.refGroupDetailID == 34));
        },
        error => console.error('Error!', error)
      );

    this.userService.getUserData().subscribe(res => {
      this.userData = res;

    });

    // Load Form Details based on Applicaiton ID
    this.adlService.getADL<PACTADL>(this.applicationID)
      .subscribe(
        res => {
          this.modelPactADL = res[0] as PACTADL;
          this.populateForm(this.modelPactADL);
        },
        // error =>
        // {
        //   throw new Error('Not Authorized');
        // }
      );
  }

  setdrpCtrlValue(ctrl: AbstractControl, ctrlValue: any) {
    ctrl.setValue(ctrlValue);
  }

  populateForm(formdata: PACTADL) {
    let ctrlValue: any;
    let ctrl: AbstractControl;

    if (formdata != null) {

      this.setdrpCtrlValue(this.adlFormgrp.controls['personHygieneSupportTypeCtrl'], formdata.personalHygieneType);

      this.adlFormgrp.controls['personHygieneDetailsCtrl'].setValue(formdata.personalHygieneDetails);
      this.setdrpCtrlValue(this.adlFormgrp.controls['personHygieneDetailsCtrl'], formdata.personalHygieneDetails);


      this.setdrpCtrlValue(this.adlFormgrp.controls['travelMobilitySupportTypeCtrl'], formdata.travelMobilityType);
      this.setdrpCtrlValue(this.adlFormgrp.controls['travelMobilityDetailsCtrl'], formdata.travelMobilityDetails);


      this.setdrpCtrlValue(this.adlFormgrp.controls['shoppingSupportTypeCtrl'], formdata.shoppingAndMealType);
      this.setdrpCtrlValue(this.adlFormgrp.controls['shoppingDetailsCtrl'], formdata.shoppingAndMealDetails);

      this.setdrpCtrlValue(this.adlFormgrp.controls['manageFinanceSupportTypeCtrl'], formdata.managingFinancesType);
      this.setdrpCtrlValue(this.adlFormgrp.controls['manageFinanceDetailsCtrl'], formdata.managingFinancesDetails);

      this.setdrpCtrlValue(this.adlFormgrp.controls['aptUpkeepSupportTypeCtrl'], formdata.apartmentType);
      this.setdrpCtrlValue(this.adlFormgrp.controls['aptRoomUpkeepDetailsCtrl'], formdata.apartmentDetails);

      this.setdrpCtrlValue(this.adlFormgrp.controls['socialSkillsSupportTypeCtrl'], formdata.socialSkillsType);
      this.setdrpCtrlValue(this.adlFormgrp.controls['socialSkillsDetailsCtrl'], formdata.socialSkillsDetails);

      this.setdrpCtrlValue(this.adlFormgrp.controls['manageHealthSupportTypeCtrl'], formdata.healthAndBehavioralType);
      this.setdrpCtrlValue(this.adlFormgrp.controls['manageHealthDetailsCtrl'], formdata.healthAndBehavioralDetails);

      this.setdrpCtrlValue(this.adlFormgrp.controls['otherADLSupportTypeCtrl'], formdata.otherType);
      this.setdrpCtrlValue(this.adlFormgrp.controls['otherADLDetailsCtrl'], formdata.otherDetails);

      // this.setdrpCtrlValue(this.adlFormgrp.controls['isReceivingAssistanceCtrl'], formdata.isReceivingAssistance);


      // this.setdrpCtrlValue(this.adlFormgrp.controls['receivingAssistanceDetailsDetailsCtrl'], formdata.receivingAssistanceDetails);
      if(formdata.otherType == 9 || formdata.otherType == 11)
      {
        this.otherADlRequired = true;
       //this.adlFormgrp.controls.otherADLDetailsCtrl.setValidators([Validators.required]);
      }
      else
      {
       this.otherADlRequired = false;
       //this.adlFormgrp.controls.otherADLDetailsCtrl.clearValidators();
      }
      this.tab1Status = formdata.isADLTabComplete == true ? 1 : 0;
      this.SaveAdl.emit( this.tab1Status);
    } else {
      this.modelPactADL = new PACTADL();
      this.modelPactADL.pactApplicationID = this.applicationID;
      this.modelPactADL.pactadlid = null;
      this.modelPactADL.createdDate = this.modelPactADL.updatedDate = new Date();
      this.modelPactADL.isActive = true;

      this.adlFormgrp.controls.personHygieneSupportTypeCtrl.setValue(0);
      this.adlFormgrp.controls.travelMobilitySupportTypeCtrl.setValue(0);
      this.adlFormgrp.controls.shoppingSupportTypeCtrl.setValue(0);
      this.adlFormgrp.controls.manageFinanceSupportTypeCtrl.setValue(0);
      this.adlFormgrp.controls.aptUpkeepSupportTypeCtrl.setValue(0);
      this.adlFormgrp.controls.socialSkillsSupportTypeCtrl.setValue(0);
      this.adlFormgrp.controls.manageHealthSupportTypeCtrl.setValue(0);
      this.adlFormgrp.controls.otherADLSupportTypeCtrl.setValue(0);
    }
  }

  detialsValidator(AC: AbstractControl) {
    return null;
  }

  IsReceivingAssistnace(data) {
    this.adlFormgrp.controls.receivingAssistanceDetailsDetailsCtrl.setValue('');
  }

  changeSupportType(data, type) {

    switch (type) {

      case 'PH': { this.adlFormgrp.controls.personHygieneDetailsCtrl.setValue(''); break; }
      case 'TM': { this.adlFormgrp.controls.travelMobilityDetailsCtrl.setValue(''); break; }
      case 'SM': { this.adlFormgrp.controls.shoppingDetailsCtrl.setValue(''); break; }
      case 'MF': { this.adlFormgrp.controls.manageFinanceDetailsCtrl.setValue(''); break; }
      case 'RU': { this.adlFormgrp.controls.aptRoomUpkeepDetailsCtrl.setValue(''); break; }
      case 'SK': { this.adlFormgrp.controls.socialSkillsDetailsCtrl.setValue(''); break; }
      case 'MH': { this.adlFormgrp.controls.manageHealthDetailsCtrl.setValue(''); break; }
      case 'OADL':{
         
       if(data.value == 9 || data.value == 11)
       {
         this.otherADlRequired = true;
        //this.adlFormgrp.controls.otherADLDetailsCtrl.setValidators([Validators.required]);
       }
       else
       {
        this.otherADlRequired = false;
        //this.adlFormgrp.controls.otherADLDetailsCtrl.clearValidators();
       }
       
      }
          }
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }

  }
  saveADL() {
     if (this.disabelSave)
     {


    this.isTabComplete = true;

    this.modelPactADL.personalHygieneType = this.adlFormgrp.controls.personHygieneSupportTypeCtrl.value == 0 ? null : this.adlFormgrp.controls.personHygieneSupportTypeCtrl.value;
    this.modelPactADL.personalHygieneDetails = this.adlFormgrp.controls.personHygieneDetailsCtrl.value;
    this.isTabComplete = this.verifyTabComplete('personHygieneSupportTypeCtrl', 'personHygieneDetailsCtrl');

    this.modelPactADL.travelMobilityType = this.adlFormgrp.controls.travelMobilitySupportTypeCtrl.value == 0 ? null : this.adlFormgrp.controls.travelMobilitySupportTypeCtrl.value;
    this.modelPactADL.travelMobilityDetails = this.adlFormgrp.controls.travelMobilityDetailsCtrl.value;
    this.isTabComplete = (this.isTabComplete == true) ? this.verifyTabComplete('travelMobilitySupportTypeCtrl', 'travelMobilityDetailsCtrl') : false;

    this.modelPactADL.shoppingAndMealType = this.adlFormgrp.controls.shoppingSupportTypeCtrl.value == 0 ? null : this.adlFormgrp.controls.shoppingSupportTypeCtrl.value;;
    this.modelPactADL.shoppingAndMealDetails = this.adlFormgrp.controls.shoppingDetailsCtrl.value;
    this.isTabComplete = (this.isTabComplete == true) ? this.verifyTabComplete('shoppingSupportTypeCtrl', 'shoppingDetailsCtrl') : false;

    this.modelPactADL.managingFinancesType = this.adlFormgrp.controls.manageFinanceSupportTypeCtrl.value == 0 ? null : this.adlFormgrp.controls.manageFinanceSupportTypeCtrl.value;;
    this.modelPactADL.managingFinancesDetails = this.adlFormgrp.controls.manageFinanceDetailsCtrl.value;
    this.isTabComplete = (this.isTabComplete == true) ? this.verifyTabComplete('manageFinanceSupportTypeCtrl', 'manageFinanceDetailsCtrl') : false;

    this.modelPactADL.apartmentType = this.adlFormgrp.controls.aptUpkeepSupportTypeCtrl.value == 0 ? null : this.adlFormgrp.controls.aptUpkeepSupportTypeCtrl.value;;
    this.modelPactADL.apartmentDetails = this.adlFormgrp.controls.aptRoomUpkeepDetailsCtrl.value;
    this.isTabComplete = (this.isTabComplete == true) ? this.verifyTabComplete('aptUpkeepSupportTypeCtrl', 'aptRoomUpkeepDetailsCtrl') : false;

    this.modelPactADL.socialSkillsType = this.adlFormgrp.controls.socialSkillsSupportTypeCtrl.value == 0 ? null : this.adlFormgrp.controls.socialSkillsSupportTypeCtrl.value;;
    this.modelPactADL.socialSkillsDetails = this.adlFormgrp.controls.socialSkillsDetailsCtrl.value;
    this.isTabComplete = (this.isTabComplete == true) ? this.verifyTabComplete('socialSkillsSupportTypeCtrl', 'socialSkillsDetailsCtrl') : false;

    this.modelPactADL.healthAndBehavioralType = this.adlFormgrp.controls.manageHealthSupportTypeCtrl.value == 0 ? null : this.adlFormgrp.controls.manageHealthSupportTypeCtrl.value;;
    this.modelPactADL.healthAndBehavioralDetails = this.adlFormgrp.controls.manageHealthDetailsCtrl.value;
    this.isTabComplete = (this.isTabComplete == true) ? this.verifyTabComplete('manageHealthSupportTypeCtrl', 'manageHealthDetailsCtrl') : false;

    this.modelPactADL.otherType = this.adlFormgrp.controls.otherADLSupportTypeCtrl.value == 0 ? null : this.adlFormgrp.controls.otherADLSupportTypeCtrl.value;;
    this.modelPactADL.otherDetails = this.adlFormgrp.controls.otherADLDetailsCtrl.value;
    console.log("0",this.modelPactADL.otherType);
    if(this.modelPactADL.otherType != 10 && this.modelPactADL.otherType != null && this.modelPactADL.otherDetails.replace(/\s/g, '') == '')
    {
      console.log("1",this.modelPactADL.otherType);
      this.isTabComplete = (this.isTabComplete == true) ? this.verifyTabComplete('otherADLSupportTypeCtrl', 'otherADLDetailsCtrl') : false;
     // this.toastr.error("Other ADL Impairments, describe is required");
     
    }
   
       

    // this.modelPactADL.isReceivingAssistance = this.adlFormgrp.controls['isReceivingAssistanceCtrl'].value;
    // this.modelPactADL.receivingAssistanceDetails = this.adlFormgrp.controls['receivingAssistanceDetailsDetailsCtrl'].value;

    // if (this.modelPactADL.isReceivingAssistance == null || this.modelPactADL.isReceivingAssistance.toString() == ' ') {
    //   this.isTabComplete = false;
    //   this.radiobtnValidation = false;
    // }
    // if (this.modelPactADL.isReceivingAssistance == 33 && this.adlFormgrp.controls['receivingAssistanceDetailsDetailsCtrl'].value == "") {
    //   this.isTabComplete = false;
    // }


    this.modelPactADL.createdBy = this.modelPactADL.updatedBy = this.userData.optionUserId;
    this.modelPactADL.isADLTabComplete = this.isTabComplete;

    this.tab1Status = this.isTabComplete == true ? 1 : 0;
    this.adlService.saveADL(this.modelPactADL)
      .subscribe(
        data => {
          if (this.tab1Status == 1) {
            this.toastr.success('ADL is Completed', ' ');
          } else {
            this.toastr.info('ADL is Partially Completed', '');
          }
         // this.sidenavStatusService._sidenavStatusReload(this.route);
         if (this.appID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.appID);
        }


          this.SaveAdl.emit( this.tab1Status);
        },
        // error => {

        //   this.toastr.warning('Error in Saving ADL!', '');
        // ///  throw new Error('Not Authorized');
        // }
      );
    }
  }

  verifyTabComplete(ctrl1: string, ctrl2: string): boolean {
    if (ctrl1 == 'otherADLSupportTypeCtrl' && this.adlFormgrp.controls[ctrl1].value != 0 && this.adlFormgrp.controls[ctrl2].value.replace(/\s/g, '') != '') {
      return true;
    } else if (this.adlFormgrp.controls[ctrl1].value != 0 && this.adlFormgrp.controls[ctrl1].value != 10 && this.adlFormgrp.controls[ctrl2].value.replace(/\s/g, '') != '') {
      return true;
    }
    //else if (this.adlFormgrp.controls[ctrl1].value == 10 && ctrl1 != 'otherADLSupportTypeCtrl') {
      else if (this.adlFormgrp.controls[ctrl1].value == 10 ) {
      return true;
    } else {
       return false;
       };

  }

  saveForm() {
    this.saveADL();
  }

  PreviousPage() {
      this.sidenavStatusService.routeToPreviousPage(this.router, this.route);
  }

  NextPage() {
    this.sidenavStatusService.routeToNextPage(this.router, this.route);
  }
}
