export class PACTADL {
    pactadlid: number ;
    pactApplicationID: number ;
    personalHygieneType: number ;
    personalHygieneDetails: string ;
    travelMobilityType: number ;
    travelMobilityDetails: string ;
    shoppingAndMealType: number ;
    shoppingAndMealDetails: string ;
    managingFinancesType: number ;
    managingFinancesDetails: string ;
    apartmentType: number ;
    apartmentDetails: string ;
    socialSkillsType: number ;
    socialSkillsDetails: string ;
    healthAndBehavioralType: number ;
    healthAndBehavioralDetails: string ;
    otherType: number ;
    otherDetails: string ;
    // isReceivingAssistance: number ;
    // receivingAssistanceDetails: string ;
    isADLTabComplete: boolean  ;
    isActive: boolean  ;
    createdBy: number ;
      createdDate: Date ;
    updatedBy: number ;
      updatedDate: Date ;

}


export interface AppResponse {
      status : number;
      message : string;
      data: object;
}