import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedAdlComponent } from './shared-adl.component';

describe('SharedAdlComponent', () => {
  let component: SharedAdlComponent;
  let fixture: ComponentFixture<SharedAdlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedAdlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedAdlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
