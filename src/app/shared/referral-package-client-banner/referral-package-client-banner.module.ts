import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';

import { ReferralPackageClientBannerComponent } from './referral-package-client-banner/referral-package-client-banner.component';

@NgModule({
  declarations: [ReferralPackageClientBannerComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
  ],
  exports: [
    ReferralPackageClientBannerComponent
  ]
})
export class ReferralPackageClientBannerModule { }
