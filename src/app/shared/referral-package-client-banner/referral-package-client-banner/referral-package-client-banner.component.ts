import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { C2vService } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v.service';
import { Router } from '@angular/router';
import { IClientAwaitingPlacementData } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v-interfaces.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { Subscription } from 'rxjs';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { VCSCoCReferralData } from 'src/app/pact-modules/vacancyControlSystem/coc-referral-queue/coc-referral-queue.model';
import { CoCService } from 'src/app/pact-modules/vacancyControlSystem/coc-referral-queue/coc-referral-queue.service';

@Component({
  selector: 'app-referral-package-client-banner',
  templateUrl: './referral-package-client-banner.component.html',
  styleUrls: ['./referral-package-client-banner.component.scss']
})
export class ReferralPackageClientBannerComponent implements OnInit, OnDestroy {

  @Input() parentPageID = 1; // 1-CAP, 2:CoC Referral

  showEligibility: boolean = false;
  showService: boolean = false;
  showPlacement: boolean = false;

  // Client Selected from ClientAwaitingPlacement Page
  clientSelected: IClientAwaitingPlacementData = {
    vcsClientAwaitingPlacementID: 0,
    pactApplicationID: 0,
    referringAgencyID: 0,
    referringAgencyNo: '',
    referringSiteID: 0,
    referringSiteNo: '',
    pactClientID: 0,
    firstName: '',
    lastName: '',
    dateOfBirth: '',
    referralDate: '',
    ssn: '',
    agencyName: '',
    siteName: '',
    clientEligibleFor: '',
    svaPrioritization: '',
    serviceNeeds: '',
    approvalFromDate: '',
    approvalToDate: '',
    placementCriteria: '',
    siteType: '',
    gender: '',
    ethnicity: '',
    age: 0,
    borough: '',
    applicationStatus: '',
    // isReferralPackageReady: true,
    isPackageReferralReady: 0,
    packageReferralReadyComment: '',
    isClientReferralReady: 0,
    clientReferralReadyComment: '',
    clientNotReadyReason: 0,
    clientNotReadyOtherSpecify: '',
    clientNotReadyAddlComments: '',
    clientPackageUpdatedBy: '',
    clientPackageUpdatedDate: '',
    pendingReferrals: 0,
    isHUD: false,
    // isReferralHold: false,
    // referralHoldReason: 0,
    isActive: false,
    createdBy: 0,
    createdDate: '',
    updatedBy: 0,
    updatedDate: ''
  };

  currentUserSub: Subscription;
  clientSelectedSub: Subscription;

  constructor(
    private c2vService: C2vService,
    private router: Router,
    private userService: UserService,
    private cocService: CoCService
  ) { }

  ngOnInit() {
    if (this.parentPageID == 1) {
      this.clientSelectedSub = this.c2vService.getClientAwaitingPlacementSelected().subscribe(res => {
        if (res) {
          this.clientSelected = res;
        } else {
          /* If user refresh(reload) any page which uses this referral package client banner
            the banner data will be empty, so redirect them back to ClientAwaitingPlacement Page (If user is PE)
            or redirect them back to Referral Roster Page (If user is HP)
          */
          /** Getting the currently active user info */
          this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
            if (userdata) {
              /** Checking the User siteCategoryType either PE or HP */
              userdata.siteCategoryType.forEach(ct => {
                if (ct.siteCategory === UserSiteType.SH_PE) {
                  this.router.navigate(['/vcs/client-awaiting-placement']);
                } else if (ct.siteCategory === UserSiteType.SH_HP) {
                  this.router.navigate(['/vcs/referral-roster']);
                }
              });
            }
          });
        }
      });
    }
    else if (this.parentPageID == 2) {
      this.clientSelectedSub = this.cocService.getCoCReferralSelected().subscribe(res => {
        if (res) {
          //this.clientSelected = res;
          this.clientSelected.pactClientID = res.pactClientID;
          this.clientSelected.referralDate = res.referralDate;
          this.clientSelected.pactApplicationID = res.pactApplicationID;
          this.clientSelected.ssn = res.ssn;
          this.clientSelected.firstName = res.firstName;
          this.clientSelected.lastName = res.lastName;
          this.clientSelected.clientEligibleFor = res.clientEligibleFor;
          this.clientSelected.referringAgencyID = res.referringAgencyID;
          this.clientSelected.referringSiteID = res.referringSiteID;
          this.clientSelected.approvalToDate = res.approvalToDate;
          this.clientSelected.placementCriteria = res.placementCriteria

        } else {
          /* If user refresh(reload) any page which uses this referral package client banner
            the banner data will be empty, so redirect them back
          */
          this.router.navigate(['/vcs/coc-referral-queue']);

        }
      });
    }
  }

  toggleEligibility() {
    this.showEligibility = !this.showEligibility;
  }

  toggleService() {
    this.showService = !this.showService;
  }

  togglePlacement() {
    this.showPlacement = !this.showPlacement;
  }

  ngOnDestroy() {
    if (this.clientSelectedSub) {
      this.clientSelectedSub.unsubscribe();
    }
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
  }

}
