import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { GridOptions } from 'ag-grid-community';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CaseNotesActionComponent } from './case-notes-action.component';

//Service
import { UserService } from 'src/app/services/helper-services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmDialogService } from '../../confirm-dialog/confirm-dialog.service';

//Models
import { AuthData } from 'src/app/models/auth-data.model';
import { environment } from '../../../../environments/environment';

export interface PACTClientCaseNotes {
  pactClientCaseNotesID?: number;
  pactApplicationID?: number;
  pactClientId?: number;
  systemModuleType?: number;
  description: string;
  referralNo?: number;
  referralDate: string;
  caseNote: string;
  caseNotesTabTimeIn?: Date;
  caseNotesTabTimeOut?: Date;
  isActive?: boolean;
  createdBy: string;
  createdDate: string;
  updatedBy: string;
  updatedDate: string;
  canDelete?: boolean;
}

export interface PACTClientCaseNotesInput {
  pactClientID: number;
  optionUserID: number;
}

@Component({
  selector: 'app-case-notes',
  templateUrl: './case-notes.component.html',
  styleUrls: ['./case-notes.component.scss']
})

export class CaseNotesComponent implements OnInit, OnDestroy {
  @Input() clientData: {
    firstName: string;
    lastName: string;
    ssn: string;
    dob: string;
    genderType: number;
    genderTypeDescription: string;
    cin: string;
    pactClientId: number;
    pactApplicationId: number;
    referralDate: string;
  };

  userData: AuthData;
  userDataSub: Subscription;
  context: any;
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  caseNotesColumnDefs = [];
  caseNotesRowData: PACTClientCaseNotes[];
  caseNotesDefaultColDef = {};
  caseNotesGridOptions: GridOptions;
  caseNotesOverlayNoRowsTemplate: string;

  pactClientCaseNotesGroup: FormGroup;
  message: string;

  pactClientCaseNotes: PACTClientCaseNotes = {
    pactClientCaseNotesID: null, pactApplicationID: null, pactClientId: null, systemModuleType: null, description: null, referralNo: null, referralDate: null,
    caseNote: null, caseNotesTabTimeIn: null, caseNotesTabTimeOut: null, isActive: null, createdBy: null, createdDate: null, updatedBy: null, updatedDate: null
  }

  pactClientCaseNotesInput: PACTClientCaseNotesInput = {
    pactClientID: null,
    optionUserID: null
  }

  getClientCaseNotesURL = environment.pactApiUrl + 'DETClientCaseFolder/GetClientCaseNotesByClientID';
  saveClientCaseNotesURL = environment.pactApiUrl + 'DETClientCaseFolder/SaveClientCaseNotes';
  deleteClientCaseNotesURL = environment.pactApiUrl + 'DETClientCaseFolder/DeleteClientCaseNotes';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    response: 'json',
  };

  constructor(private toastr: ToastrService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private confirmDialogService: ConfirmDialogService,
    private httpClient: HttpClient) {

    this.pactClientCaseNotesGroup = this.formBuilder.group({
      pactClientCaseNotesIDCtrl: [0]
      , pactApplicationIDCtrl: [0]
      , pactClientIdCtrl: [0]
      , systemModuleTypeCtrl: [0]
      , descriptionCtrl: ['']
      , referralNoCtrl: [0]
      , referralDateCtrl: ['']
      , caseNotesCtrl: ['']
      , caseNotesTabTimeInCtrl: ['']
      , caseNotesTabTimeOutCtrl: ['']
      , isActiveCtrl: [0]
      , createdByCtrl: [0]
      , createdDateCtrl: ['']
      , updatedByCtrl: [0]
      , updatedDateCtrl: ['']
    });

    //Case Note Grid Column Definitions
    this.caseNotesColumnDefs = [
      { headerName: 'CaseNoteID', field: 'pactClientCaseNotesID', hide: true },
      { headerName: 'System', field: 'description', filter: 'agTextColumnFilter', width: 145 },
      { headerName: 'Source Client #', field: 'pactClientId', filter: 'agTextColumnFilter', width: 125 },
      { headerName: 'Source Referral #', field: 'referralNo', filter: 'agTextColumnFilter', width: 135 },
      { headerName: 'Source Referral Date', field: 'referralDate', filter: 'agTextColumnFilter', width: 165 },
      { headerName: 'Notes', field: 'caseNote', filter: 'agTextColumnFilter', width: 400, tooltipField: 'caseNote' },
      { headerName: 'Created By', field: 'createdBy', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Created Date/Time', field: 'createdDate', filter: 'agTextColumnFilter', width: 150 },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.caseNotesDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: CaseNotesActionComponent
    };
  }

  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        this.populateClientCaseNoteGrid();
      }
    });
  }

  //On Destroy
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

  //Client Case Notes Grid Ready
  caseNotesGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.caseNotesOverlayNoRowsTemplate = '<span style="color: #337ab7">No Notes Found</span>';
    this.populateClientCaseNoteGrid();
  }

  //Populate Client Case Grid
  populateClientCaseNoteGrid() {
    if (this.clientData && this.clientData.pactClientId) {
      this.pactClientCaseNotesInput.pactClientID = this.clientData.pactClientId;
      this.pactClientCaseNotesInput.optionUserID = this.userData.optionUserId;
      this.getCaseNotesByClientID(this.pactClientCaseNotesInput).subscribe(res => {
        if (res) {
          this.caseNotesRowData = res as PACTClientCaseNotes[];
        }
        else {
          this.caseNotesRowData = null;
        }
      });
    }
    else {
      this.caseNotesRowData = null;
    }
  }

  saveClientCaseNotes() {
    if (!this.ClientCaseNotesTabDateValidationCheck()) { return; }
    this.GetClientCaseNotesDataFromUI();
    this.saveCaseNotes(this.pactClientCaseNotes).subscribe(res => {
      this.message = 'Notes saved successfully.';
      if (!this.toastr.currentlyActive) {
        this.toastr.success(this.message, 'Save');
      }
      this.populateClientCaseNoteGrid();
      this.pactClientCaseNotesGroup.get('caseNotesCtrl').setValue(null);
    });
  }

  saveClientCaseNotes_NoPopupMessage() {
    if (!this.ClientCaseNotesTabDateValidationCheck_NoPopupMessage()) { return; }

    this.GetClientCaseNotesDataFromUI();

    this.saveCaseNotes(this.pactClientCaseNotes).subscribe(res => {
      this.populateClientCaseNoteGrid();
      this.pactClientCaseNotesGroup.get('caseNotesCtrl').setValue(null);

    }, error => {
      this.message = 'Notes did not Save.';
      if (!this.toastr.currentlyActive) {
        this.toastr.error(this.message, 'Save');
      }
    });
  }

  ClientCaseNotesTabDateValidationCheck_NoPopupMessage(): boolean {
    if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value != null) {

      if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value == '') {
        return false;
      }
      else if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value.length == 1) {
        this.pactClientCaseNotesGroup.get('caseNotesCtrl').setValue(null);
        return false;
      }
      else if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value.length > 250) {
        this.pactClientCaseNotesGroup.get('caseNotesCtrl').setValue(null);
        return false;

      } else {
        this.pactClientCaseNotesGroup.controls.caseNotesCtrl.clearValidators();
        return true;
      }
    }
  }

  ClientCaseNotesTabDateValidationCheck(): boolean {
    if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value != null) {

      if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value == '') {
        this.message = 'Case Notes can not be blank.';
        if (!this.toastr.currentlyActive) {
          this.toastr.error(this.message, 'Save');
        }
        this.pactClientCaseNotesGroup.controls.caseNotesCtrl.setValidators([Validators.required]);
        this.pactClientCaseNotesGroup.controls.caseNotesCtrl.updateValueAndValidity();
        return false;
      }
      else if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value.length == 1) {

        this.message = 'Case Notes can not be less than 2 characters.';
        if (!this.toastr.currentlyActive) {
          this.toastr.error(this.message, 'Save');
        }

        this.pactClientCaseNotesGroup.controls.caseNotesCtrl.setValidators([Validators.required]);
        this.pactClientCaseNotesGroup.controls.caseNotesCtrl.updateValueAndValidity();
        return false;
      }
      else if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value.length > 2000) {
        this.message = 'Case Notes can not be more than 2000 characters.';
        if (!this.toastr.currentlyActive) {
          this.toastr.error(this.message, 'Save');
        }

        this.pactClientCaseNotesGroup.controls.caseNotesCtrl.setValidators([Validators.required]);
        this.pactClientCaseNotesGroup.controls.caseNotesCtrl.updateValueAndValidity();
        this.pactClientCaseNotesGroup.get('caseNotesCtrl').setValue(null);
        return false;

      } else {
        this.pactClientCaseNotesGroup.controls.caseNotesCtrl.clearValidators();
        return true;
      }
    }
  }

  GetClientCaseNotesDataFromUI() {
    this.pactClientCaseNotes.pactClientCaseNotesID = 0;
    this.pactClientCaseNotes.pactClientId = this.clientData.pactClientId;
    this.pactClientCaseNotes.systemModuleType = 767;
    this.pactClientCaseNotes.referralNo = this.clientData.pactApplicationId;
    this.pactClientCaseNotes.referralDate = this.clientData.referralDate;
    if (this.pactClientCaseNotesGroup.get('caseNotesCtrl').value != null) {
      this.pactClientCaseNotes.caseNote = this.pactClientCaseNotesGroup.get('caseNotesCtrl').value.trim();
    }
    this.pactClientCaseNotes.isActive = true;
  }

  deleteCaseNotesParent = (cell: PACTClientCaseNotes) => {
    const title = 'Confirm Delete';
    const primaryMessage = '';
    const secondaryMessage = `Are you sure you want to delete the selected Case Note?`;
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => this.deleteCaseNotes(cell),
        (negativeResponse) => console.log(),
      );
  }

  deleteCaseNotes = (dataSelected: PACTClientCaseNotes) => {
    this.deleteClientCaseNotes(dataSelected.pactClientCaseNotesID).subscribe(res => {
      this.message = 'Case Notes deleted successfully.';
      if (!this.toastr.currentlyActive) {
        this.toastr.success(this.message, 'Delete');
      }
      this.populateClientCaseNoteGrid();
      this.pactClientCaseNotesGroup.get('caseNotesCtrl').setValue(null);
    },
      error => {
        this.message = 'Case Notes did not delete.';
        if (!this.toastr.currentlyActive) {
          this.toastr.error(this.message, 'Delete');
        }
      }
    );
  }

  //Get Case Notes
  getCaseNotesByClientID(pactClientCaseNotesInput: PACTClientCaseNotesInput): Observable<any> {
    if (pactClientCaseNotesInput) {
      return this.httpClient.post(this.getClientCaseNotesURL, JSON.stringify(pactClientCaseNotesInput), this.httpOptions);
    }
  }

  //Save Case Notes
  saveCaseNotes(pactClientCaseNotes: PACTClientCaseNotes): Observable<any> {
    return this.httpClient.post(this.saveClientCaseNotesURL, JSON.stringify(pactClientCaseNotes), this.httpOptions);
  }

  //Delete Client Case Notes 
  deleteClientCaseNotes(pactClientCaseNotesID: number): Observable<any> {
    return this.httpClient.post(this.deleteClientCaseNotesURL, JSON.stringify(pactClientCaseNotesID), this.httpOptions);
  }
}
