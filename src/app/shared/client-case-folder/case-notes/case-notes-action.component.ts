import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

export interface PACTClientCaseNotes {
    pactClientCaseNotesID?: number;
    pactApplicationID?: number;
    pactClientId?: number;
    systemModuleType?: number;
    description: string;
    referralNo?: number;
    referralDate: string;
    caseNote: string;
    caseNotesTabTimeIn?: Date;
    caseNotesTabTimeOut?: Date;
    isActive?: boolean;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;
    canDelete: boolean;
}


@Component({
    selector: "case-notes-action",
    template: `
    <mat-icon (click)="onDelete()" matTooltip='Delete Notes' class="casenotes-action-icon" color="warn" *ngIf="params.data.canDelete">delete_forever</mat-icon>
    `
    ,
    styles: [
        `
      .casenotes-action-icon {
        cursor: pointer;
      }
    `
    ],
})
export class CaseNotesActionComponent implements ICellRendererAngularComp {
    params: any;
    public cell: any;
    noteSelected: PACTClientCaseNotes;

    constructor() { }

    agInit(params: any): void {
        this.params = params;
        this.cell = { row: params.node.data, col: params.colDef.headerName };
    }

    onDelete() {
        this.noteSelected = {
            pactClientCaseNotesID: this.params.data.pactClientCaseNotesID,
            pactApplicationID: this.params.data.pactApplicationID,
            pactClientId: this.params.data.pactClientId,
            systemModuleType: this.params.data.systemModuleType,
            description: this.params.data.description,
            referralNo: this.params.data.referralNo,
            referralDate: this.params.data.referralDate,
            caseNote: this.params.data.caseNote,
            caseNotesTabTimeIn: this.params.data.caseNotesTabTimeIn,
            caseNotesTabTimeOut: this.params.data.caseNotesTabTimeOut,
            isActive: this.params.data.isActive,
            createdBy: this.params.data.createdBy,
            createdDate: this.params.data.createdDate,
            updatedBy: this.params.data.updatedBy,
            updatedDate: this.params.data.updatedDate,
            canDelete: this.params.data.canDelete
        };
        this.params.context.componentParent.deleteCaseNotesParent(this.noteSelected);
    }

    refresh(): boolean {
        return false;
    }
}
