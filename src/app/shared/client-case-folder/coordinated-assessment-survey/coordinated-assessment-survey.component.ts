import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GridOptions } from 'ag-grid-community';
import { CommonService } from 'src/app/services/helper-services/common.service';

export interface iSubmittedAssessmentDetailsInput {
  capsClientId: number;
  optionUserId: number;
}

export interface iSubmittedAsessmentDetails {
  client: iClient;
  submittedAssessmentsByClientList: iSubmittedAssessmentsByClientList[];
}

export interface iClient {
  clientID?: number;
  lastName: string;
  firstName: string;
  clientSSN: string;
  cin: string;
  dob: string;
  age?: number;
  genderID?: number;
  gender: string;
  caseNumber: string;
}

export interface iSubmittedAssessmentsByClientList {
  clientID: number;
  assessmentID: number;
  clientName: string;
  assessmentDate: string;
  enteredBy: string;
  createdBy: number;
  agency: string;
  site: string;
  housingPrograms: string;
  agencySiteNames: string;
  cin: string;
  ssn: string;
  dob: string;
  firstName: string;
  lastName: string;
  gender: string;
  applicationNo: string;
  reportID: string;
}

@Component({
  selector: 'app-coordinated-assessment-survey',
  templateUrl: './coordinated-assessment-survey.component.html',
  styleUrls: ['./coordinated-assessment-survey.component.scss']
})

export class CoordinatedAssessmentSurveyComponent implements OnInit, OnDestroy {
  @Input() capsClientId: number;
  @Input() optionUserId: number;

  getSubmittedAssessmentDetailsURL = environment.pactApiUrl + 'DETClientCaseFolder/GetSubmittedAssessmentDetails';

  submittedAssessmentDetails: iSubmittedAsessmentDetails;
  assessmentDetailsSubs: Subscription;
  submittedAssessmentDetailsInput: iSubmittedAssessmentDetailsInput = {
    capsClientId: null,
    optionUserId: null
  }

  coordinatedAssessmentSurveyFormGroup: FormGroup;

  submittedSurveyColumnDefs = [];
  submittedSurveyRowData: iSubmittedAssessmentsByClientList[];
  submittedSurveyDefaultColDef = {};
  submittedSurveyGridOptions: GridOptions;
  submittedSurveyOverlayNoRowsTemplate: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    response: 'json',
  };

  constructor(private httpClient: HttpClient,
    private formBuilder: FormBuilder,
    private commonService: CommonService) {

    this.coordinatedAssessmentSurveyFormGroup = this.formBuilder.group({
    });

    //Submitted Assessment Grid Column Definitions
    this.submittedSurveyColumnDefs = [
      { headerName: 'Name', field: 'clientName', filter: 'agTextColumnFilter', width: 150 },
      {
        headerName: 'Survey #',
        field: 'assessmentID',
        cellRenderer: (params: { value: string; data: { reportID: string }; }) => {
          var link = document.createElement('a');
          link.href = '#';
          link.innerText = params.value;
          link.addEventListener('click', (e) => {
            e.preventDefault();
            if (params.data.reportID) {
              this.commonService.displaySurveySummaryReport(params.data.reportID, this.optionUserId);
            }
          });
          return link;
        },
        filter: 'agTextColumnFilter',
        width: 150
      },
      { headerName: 'Survey Date', field: 'assessmentDate', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Entered By', field: 'enteredBy', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Agency/Site', field: 'agency', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Application #', field: 'applicationNo', filter: 'agTextColumnFilter', width: 150 },
      {
        headerName: 'Housing Programs',
        field: 'housingPrograms',
        cellRenderer: function (params: { value: string; data: { housingPrograms: string }; }) {
          return params.data.housingPrograms;
        },
        filter: 'agTextColumnFilter',
        width: 1700
      },
    ];
    this.submittedSurveyDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  ngOnInit() {
    if (this.capsClientId && this.optionUserId) {
      this.submittedAssessmentDetailsInput.capsClientId = this.capsClientId;
      this.submittedAssessmentDetailsInput.optionUserId = this.optionUserId;
      this.assessmentDetailsSubs = this.getSubmittedAssessmentDetails(this.submittedAssessmentDetailsInput)
        .subscribe(res => {
          if (res) {
            this.submittedAssessmentDetails = res as iSubmittedAsessmentDetails;
            if (this.submittedAssessmentDetails) {
              this.submittedSurveyRowData = this.submittedAssessmentDetails.submittedAssessmentsByClientList;
            }
            else {
              this.submittedSurveyRowData = null;
            }
          }
          else {
            this.submittedAssessmentDetails = null;
          }
        });
    }
  }

  //On Destroy
  ngOnDestroy() {
    if (this.assessmentDetailsSubs) {
      this.assessmentDetailsSubs.unsubscribe();
    }
  }

  //Submitted Assessment Grid Ready
  submittedSurveyGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.submittedSurveyOverlayNoRowsTemplate = '<span style="color: #337ab7">No Submitted Surveys Found</span>';
    if (this.submittedAssessmentDetails) {
      this.submittedSurveyRowData = this.submittedAssessmentDetails.submittedAssessmentsByClientList;
    }
    else {
      this.submittedSurveyRowData = null;
    }
  }
  
  //Get Submitted Assessment Details
  getSubmittedAssessmentDetails(submittedAssessmentDetailsInput: iSubmittedAssessmentDetailsInput): Observable<any> {
    if (submittedAssessmentDetailsInput) {
      return this.httpClient.post(this.getSubmittedAssessmentDetailsURL, JSON.stringify(submittedAssessmentDetailsInput), this.httpOptions);
    }
  }
}
