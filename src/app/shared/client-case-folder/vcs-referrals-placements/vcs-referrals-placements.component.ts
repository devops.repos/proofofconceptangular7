import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { GridOptions } from 'ag-grid-community';

export interface iVCSClientData {
  vcsClientID: number;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
};

export interface iReferralHistory {
  vcsReferralID: number;
  pactApplicationID: number;
  clientID: number;
  referralDate: string;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  approvalExpiryDate: string;
  referringAgencyID: number;
  referringAgencyNo: string;
  referringSiteID: number;
  referringSiteNo: string;
  referringAgencyName: string;
  referringSiteName: string;
  hpAgencyID: number;
  hpAgencyNo: string;
  hpSiteID: number;
  hpSiteNo: string;
  hpAgencyName: string;
  hpSiteName: string;
  ssn: string;
  clientEligibleFor: string;
  svaPrioritization: string;
  serviceNeeds: string;
  placementCriteria: string;
  primaryServiceContractID: number;
  primaryServiceContractName: string;
  agreementPopulationID: number;
  populationName: string;
  siteAgreementPopulationID: number;
  agreementTypeDescription: string;
  siteLocationType: number;
  siteLocationTypeDescription: string;
  siteAddress: string;
  unitsOccupied: number;
  unitsAvailable: number;
  totalUnits: number;
  pendingReferrals: number;
  referralStatusType: number;
  referralStatusTypeDescription: string;
  referralType: number;
  unitType: number;
  unitTypeDescription: string;
  matchPercentage: number;
  withdrawnReasonType: string;
  referralClosedComment: string;
  interviewDate: string;
  interviewTime: string;
  interviewLocation: string;
  canHPScheduleInterview: boolean;
  referredBy: string;
  referredDate: string;
  updatedBy: number;
  updatedDate: string;
}

export interface iClientPlacementHistory {
  clientID: number;
  clientSourceType: number;
  isPendingVerificationRequired: number;
  vcsid: string;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  gender: string;
  hasPriorPlacements: boolean;
  hasActiveApproval: boolean;
  userID: number;
  clientPlacementHistoryList: iClientPlacementHistoryList[];
}

export interface iClientPlacementHistoryList {
  clientID: number;
  referralDate: string;
  hpAgencyNo: string;
  hpAgencyName: string;
  hpSiteNo: string;
  hpSiteName: string;
  placementNo: number;
  placementType: number;
  placementTypeDesription: string;
  raAgencyName: string;
  raSiteName: string;
  moveInDate: string;
  moveOutDate: string;
  moveOutReasonDescription: string;
  moveToAgencyName: string;
  verificationUpdatedBy: string;
  verificationUpdatedDate: string;
}

export interface iClientPlacementHistoryInput {
  clientID: number;
  clientSourceType: number;
  isPendingVerificationRequired: number;
}

@Component({
  selector: 'app-vcs-referrals-placements',
  templateUrl: './vcs-referrals-placements.component.html',
  styleUrls: ['./vcs-referrals-placements.component.scss']
})

export class VcsReferralsPlacementsComponent implements OnInit {
  @Input() pactClientId: number;

  getVCSClientURL = environment.pactApiUrl + 'DETClientCaseFolder/GetVCSClient';
  referralRosterListURL = environment.pactApiUrl + 'VCSReferralRoster/GetVCSReferralRosterListByPactClientID';
  clientPlacementHistoryURL = environment.pactApiUrl + 'VCSPlacementVerification/GetClientPlacementHistory';

  vcsReferralsPlacementsFormGroup: FormGroup;

  vcsReferralsColumnDefs = [];
  vcsReferralsRowData: iReferralHistory[];
  vcsReferralsDefaultColDef = {};
  vcsReferralsGridOptions: GridOptions;
  vcsReferralsOverlayNoRowsTemplate: string;

  vcsPlacementsColumnDefs = [];
  vcsPlacementsRowData: iClientPlacementHistoryList[];
  vcsPlacementsDefaultColDef = {};
  vcsPlacementsGridOptions: GridOptions;
  vcsPlacementsOverlayNoRowsTemplate: string;

  vcsClientData: iVCSClientData = {
    vcsClientID: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null
  }

  clientPlacementHistoryInput: iClientPlacementHistoryInput = {
    clientID: null,
    clientSourceType: null,
    isPendingVerificationRequired: null
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    response: 'json',
  };

  constructor(private formBuilder: FormBuilder,
    private httpClient: HttpClient) {

    this.vcsReferralsPlacementsFormGroup = this.formBuilder.group({
    });

    //VCS Referrals Grid Column Definitions
    this.vcsReferralsColumnDefs = [
      {
        headerName: 'Client # - Referral Date',
        field: 'clientID',
        cellRenderer: function (params: { value: string; data: { referralDate: string }; }) {
          if (params.data.referralDate) {
            return params.value + ' - ' + params.data.referralDate;
          }
          else {
            return params.value;
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Client Name',
        field: 'firstName',
        cellRenderer: function (params: { value: string; data: { lastName: string }; }) {
          return params.data.lastName + ', ' + params.value;
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Referring Agency/Site',
        field: 'referringAgencyName',
        cellRenderer: function (params: { value: string; data: { referringAgencyNo: string; referringAgencyName: string; referringSiteNo: string; referringSiteName: string }; }) {
          if (params.value) {
            return params.data.referringAgencyNo + ' - ' + params.data.referringAgencyName + ' / ' + params.data.referringSiteNo + ' - ' + params.data.referringSiteName;
          }
        },
        filter: 'agTextColumnFilter',
        width: 350
      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        filter: 'agTextColumnFilter',
        width: 350
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        cellRenderer: function (params: { value: string }) {
          if (params.value) {
            return 'SVA - ' + params.value;
          }
        },
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter',
        width: 350
      },
      {
        headerName: 'Placement Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter',
        width: 550
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Housing Agency/Site',
        field: 'hpAgencyName',
        cellRenderer: function (params: { value: string; data: { hpSiteName: string }; }) {
          if (params.value) {
            return params.value + '/' + params.data.hpSiteName;
          }
        },
        filter: 'agTextColumnFilter',
        width: 300
      },
      {
        headerName: 'Interview Date - Time',
        field: 'interviewDate',
        cellRenderer: function (params: { value: string; data: { interviewTime: string }; }) {
          if (params.value) {
            return params.value + ' - ' + params.data.interviewTime;
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Interview Location',
        field: 'interviewLocation',
        filter: 'agTextColumnFilter',
        width: 250
      },
      {
        headerName: 'Referred By - Date',
        field: 'referredBy',
        cellRenderer: function (params: { value: string; data: { referredDate: string }; }) {
          if (params.value) {
            return params.value + ' - ' + params.data.referredDate;
          }
        },
        filter: 'agTextColumnFilter',
        width: 250
      },
      {
        headerName: 'Status',
        field: 'referralStatusTypeDescription',
        filter: 'agTextColumnFilter',
        width: 250
      },
      {
        headerName: 'Last Updated By - Date',
        field: 'updatedBy',
        cellRenderer: function (params: { value: string; data: { updatedDate: string }; }) {
          if (params.value) {
            return params.value + ' - ' + params.data.updatedDate;
          }
        },
        filter: 'agTextColumnFilter',
        width: 250
      }
    ];
    this.vcsReferralsDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    //VCS Placements Grid Column Definitions
    this.vcsPlacementsColumnDefs = [
      {
        headerName: 'Client # - Referral Date',
        field: 'clientID',
        cellRenderer: function (params: { value: string; data: { referralDate: string }; }) {
          if (params.data.referralDate) {
            return params.value + ' - ' + params.data.referralDate;
          }
          else {
            return params.value;
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Placement #',
        field: 'placementNo',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Placement Type',
        field: 'placementTypeDesription',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Provider Agency/Site',
        field: 'hpAgencyName',
        cellRenderer: function (params: { value: string; data: { hpAgencyNo: string; hpAgencyName: string; hpSiteNo: string; hpSiteName: string }; }) {
          if (params.value) {
            return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
          }
        },
        filter: 'agTextColumnFilter',
        width: 450
      },
      {
        headerName: 'Move In',
        field: 'moveInDate',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Move Out',
        field: 'moveOutDate',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Reason Moved',
        field: 'moveOutReasonDescription',
        filter: 'agTextColumnFilter',
        width: 350
      },
      {
        headerName: 'Moved To',
        field: 'moveToAgencyName',
        filter: 'agTextColumnFilter',
        width: 350
      },
      {
        headerName: 'Updated By - Date',
        field: 'verificationUpdatedBy',
        cellRenderer: function (params: { value: string; data: { verificationUpdatedDate: string }; }) {
          if (params.value) {
            return params.value + ' - ' + params.data.verificationUpdatedDate;
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
    ];
    this.vcsPlacementsDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  ngOnInit() {
    if (this.pactClientId) {
      this.getVCSClient(this.pactClientId)
        .subscribe(res => {
          if (res) {
            const data = res as iVCSClientData;
            if (data) {
              this.vcsClientData = data;
            }
          }
        });
    }
  }

  //VCS Referrals OnGrid Ready
  vcsReferralsOnGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.vcsReferralsOverlayNoRowsTemplate = '<span style="color: #337ab7">No Referrals To Show</span>';
    this.populateReferralRosterGrid();
  }

  //Populate Referral Roster Grid
  populateReferralRosterGrid() {
    if (this.pactClientId) {
      this.getReferralRoster(this.pactClientId)
        .subscribe(res => {
          if (res) {
            const data = res as iReferralHistory[];
            if (data) {
              this.vcsReferralsRowData = data;
            }
            else {
              this.vcsReferralsRowData = null;
            }
          }
        });
    }
    else {
      this.vcsReferralsRowData = null;
    }
  }

  //VCS Placements OnGrid Ready
  vcsPlacementsOnGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.vcsPlacementsOverlayNoRowsTemplate = '<span style="color: #337ab7">No Placements To Show</span>';
    this.populatePlacementsGrid();

  }

  //Populate VCS Placements Grid
  populatePlacementsGrid() {
    if (this.pactClientId) {
      this.clientPlacementHistoryInput.clientID = this.pactClientId;
      this.clientPlacementHistoryInput.clientSourceType = 573;
      this.clientPlacementHistoryInput.isPendingVerificationRequired = 1;
      this.getClientPlacementHistory(this.clientPlacementHistoryInput)
        .subscribe(res => {
          if (res) {
            const data = res as iClientPlacementHistory;
            if (data) {
              this.vcsPlacementsRowData = data.clientPlacementHistoryList;
            }
            else {
              this.vcsPlacementsRowData = null;
            }
          }
        });
    }
    else {
      this.vcsPlacementsRowData = null;
    }
  }

  //Get VCS Client
  getVCSClient(pactClientId: number): Observable<any> {
    if (pactClientId) {
      return this.httpClient.post(this.getVCSClientURL, JSON.stringify(pactClientId), this.httpOptions);
    }
  }

  //Get Referral Roster
  getReferralRoster(pactClientId: number): Observable<any> {
    if (pactClientId) {
      return this.httpClient.post(this.referralRosterListURL, JSON.stringify(pactClientId), this.httpOptions);
    }
  }

  //Get Client Placement History
  getClientPlacementHistory(clientPlacementHistoryInput: iClientPlacementHistoryInput): Observable<any> {
    if (clientPlacementHistoryInput) {
      return this.httpClient.post(this.clientPlacementHistoryURL, JSON.stringify(clientPlacementHistoryInput), this.httpOptions);
    }
  }
}
