import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-vps-referrals',
  templateUrl: './vps-referrals.component.html',
  styleUrls: ['./vps-referrals.component.scss']
})
export class VpsReferralsComponent implements OnInit {
  @Input() vpsClientData: {
    vpsClientId: number;
    firstName: string;
    lastName: string;
    ssn: string;
    dob: string;
    genderType: number;
    genderTypeDescription: string;
    cin: string;
  };
  
  vpsReferralsFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.vpsReferralsFormGroup = this.formBuilder.group({
    });
  }

  ngOnInit() {
  }

}
