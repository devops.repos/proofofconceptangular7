import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { CaseClientDocumentsComponent } from './case-client-documents.component';
import { AgGridModule } from 'ag-grid-angular';
import { NgxMaskModule } from 'ngx-mask';
import { ReactiveFormsModule } from '@angular/forms';
import { HousingApplicationSupportingDocumentsModule } from '../../housing-application-supporting-documents/housing-application-supporting-documents.module';

@NgModule({
  declarations: [CaseClientDocumentsComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    ReactiveFormsModule,
    HousingApplicationSupportingDocumentsModule,
    AgGridModule.withComponents(),
    NgxMaskModule
  ],
  exports: [CaseClientDocumentsComponent]
})

export class CaseClientDocumentsModule { }