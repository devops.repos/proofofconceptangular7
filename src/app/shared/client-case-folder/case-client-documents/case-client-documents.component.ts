import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

export class iHousingApplicationSupportingDocumentsData {
  agencyNumber: string;
  siteNumber: string;
  firstName: string;
  lastName: string;
  dob: string;
  cin: string;
  ssn: string;
  pactClientId: number;
  approvalExpiryDate: string;
  pactApplicationId: number;
  expandSection: number;
};

@Component({
  selector: 'app-case-client-documents',
  templateUrl: './case-client-documents.component.html',
  styleUrls: ['./case-client-documents.component.scss']
})

export class CaseClientDocumentsComponent implements OnInit {
  @Input() clientDocumentsData: {
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    ssn: string;
    dob: string;
    genderType: number;
    genderTypeDescription: string;
    cin: string;
    pactClientId: number;
    approvalExpiryDate: string;
    pactApplicationId: number;
  };

  caseClientDocumentsFormGroup: FormGroup;

  housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null,
    expandSection: null
  }

  constructor(private formBuilder: FormBuilder) {
    this.caseClientDocumentsFormGroup = this.formBuilder.group({
    });
  }

  ngOnInit() {
    if (this.clientDocumentsData) {
      this.housingApplicationSupportingDocumentsData.agencyNumber = this.clientDocumentsData.agencyNumber;
      this.housingApplicationSupportingDocumentsData.siteNumber = this.clientDocumentsData.siteNumber;
      this.housingApplicationSupportingDocumentsData.firstName = this.clientDocumentsData.firstName;
      this.housingApplicationSupportingDocumentsData.lastName = this.clientDocumentsData.lastName;
      this.housingApplicationSupportingDocumentsData.dob = this.clientDocumentsData.dob;
      this.housingApplicationSupportingDocumentsData.cin = this.clientDocumentsData.cin;
      this.housingApplicationSupportingDocumentsData.ssn = this.clientDocumentsData.ssn;
      this.housingApplicationSupportingDocumentsData.pactClientId = this.clientDocumentsData.pactClientId;
      this.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.clientDocumentsData.approvalExpiryDate;
      this.housingApplicationSupportingDocumentsData.pactApplicationId = this.clientDocumentsData.pactApplicationId;
      this.housingApplicationSupportingDocumentsData.expandSection = 4;
    }
  }
}
