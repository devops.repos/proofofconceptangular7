import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GridOptions } from 'ag-grid-community';
import { PreviousApplicationsDeterminationsActionComponent } from './previous-applications-determinations-action.component';

//Models
import { iPriorReferrals } from 'src/app/pact-modules/supportiveHousingSystem/consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications.model';

//Service
import { PreviousApplicationsDeterminationsService } from './previous-applications-determinations.service';

@Component({
  selector: 'app-previous-applications-determinations',
  templateUrl: './previous-applications-determinations.component.html',
  styleUrls: ['./previous-applications-determinations.component.scss']
})
export class PreviousApplicationsDeterminationsComponent implements OnInit {
  @Input() clientData: {
    firstName: string;
    lastName: string;
    ssn: string;
    dob: string;
    genderType: number;
    genderTypeDescription: string;
    cin: string;
    pactClientId: number;
    referralDate: string;
  };

  previousApplicationsDeterminationsFormGroup: FormGroup;

  existingApprovalsColumnDefs = [];
  existingApprovalsRowData: iPriorReferrals[];
  existingApprovalsDefaultColDef = {};
  existingApprovalsGridOptions: GridOptions;
  existingApprovalsOverlayNoRowsTemplate: string;
  existingApprovalsFrameworkComponents: any;
  existingApprovalsGridColumnApi: any;
  existingApprovalsContext: any;

  constructor(private formBuilder: FormBuilder,
    private previousApplicationsDeterminationsService: PreviousApplicationsDeterminationsService) {
    //Form Group
    this.previousApplicationsDeterminationsFormGroup = this.formBuilder.group({
    });

    //Existing Approvals Grid Column Definitions
    this.existingApprovalsColumnDefs = [
      { headerName: 'Referral Date', field: 'referralDate', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Referring Agency/Site', field: 'referringAgencySite', filter: 'agTextColumnFilter', width: 400 },
      { headerName: 'Eligibility', field: 'eligibility', filter: 'agTextColumnFilter', width: 400 },
      { headerName: 'Prioritization', field: 'prioritization', filter: 'agTextColumnFilter', width: 130 },
      { headerName: 'Service Needs', field: 'serviceNeeds', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Type', field: 'type', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Approval Period', field: 'approvalPeriod', filter: 'agTextColumnFilter', width: 160 },
      { headerName: 'Placement Agency/Site', field: 'placementAgencySite', filter: 'agTextColumnFilter', width: 400 },
      { headerName: 'Move In/Move Out', field: 'moveInMoveOut', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Reason Moved', field: 'reasonMoved', filter: 'agTextColumnFilter', width: 250 },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.existingApprovalsDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.existingApprovalsContext = {
      componentParent: this
    };
    this.existingApprovalsFrameworkComponents = {
      actionRenderer: PreviousApplicationsDeterminationsActionComponent
    };
  }

  ngOnInit() {
  }

  //Existing Approvals Grid Ready
  existingApprovalsGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.existingApprovalsOverlayNoRowsTemplate = '<span style="color: #337ab7">No Previous Applications And Determinations To Show</span>';
    if (this.clientData && this.clientData.pactClientId) {
      this.populateExistingApprovalsGrid(this.clientData.pactClientId);
    }
  }

  //Populate Ecisting Approvals Grid
  populateExistingApprovalsGrid(pactClientId: number) {
    if (pactClientId > 0) {
      this.previousApplicationsDeterminationsService.getPreviousApplicationsAndDeterminations(pactClientId)
        .subscribe(res => {
          if (res) {
            const data = res as iPriorReferrals[];
            if (data && data.length > 0) {
              this.existingApprovalsRowData = res as iPriorReferrals[];
            }
            else {
              this.existingApprovalsRowData = null;
            }
          }
        });
    }
  }

  //On Email
  onEmail() {
    alert("Email");
  }
}
