import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MatDialog } from '@angular/material';
import { iHousingApplicationSupportingDocumentsData } from 'src/app/shared/housing-application-supporting-documents/housing-application-supporting-document.model';
import { SupportingDocumentsDialogComponent } from '../../supporting-documents-dialog/supporting-documents-dialog.component'

@Component({
  selector: "previous-applications-determinations-action",
  template: `
    <mat-icon
      class="menu-icon"
      color="warn"
      [matMenuTriggerFor]="outcomeAction">
      more_vert
    </mat-icon>
    <mat-menu #outcomeAction="matMenu">
      <button mat-menu-item (click)="onSupportingDocuments()" class="menu-button">Supporting Documents</button>
      <button mat-menu-item (click)="onEmail()" class="menu-button">Email</button>
    </mat-menu>
  `,
  styles: [
    `
      .menu-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 35px;
        width: 100%;
        height: 35px;
      }
    `
  ]
})
export class PreviousApplicationsDeterminationsActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;

  housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null,
    expandSection: null
  }

  constructor(public dialog: MatDialog) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  //On Supporting Documents
  onSupportingDocuments() {
    this.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    this.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    this.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    this.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    this.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    this.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    this.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    this.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientID;
    this.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalTo;
    this.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
    this.housingApplicationSupportingDocumentsData.expandSection = 1;
    this.openDialog(this.housingApplicationSupportingDocumentsData);
  }

  //On Email
  onEmail() {
    this.params.context.componentParent.onEmail(this.params.data.clientId);
  }

  refresh(): boolean {
    return false;
  }

  //Open Housing Application Supporting Documents Dialog
  openDialog(housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData): void {
    this.dialog.open(SupportingDocumentsDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: housingApplicationSupportingDocumentsData
    });
  }
}
