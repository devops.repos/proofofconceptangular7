import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { PreviousApplicationsDeterminationsComponent } from './previous-applications-determinations.component';
import { AgGridModule } from 'ag-grid-angular';
import { NgxMaskModule } from 'ngx-mask';
import { ReactiveFormsModule } from '@angular/forms';
import { SupportingDocumentsDialogModule } from '../../supporting-documents-dialog/supporting-documents-dialog.module';

@NgModule({
  declarations: [PreviousApplicationsDeterminationsComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    HttpClientModule,
    ReactiveFormsModule,
    SupportingDocumentsDialogModule,
    AgGridModule.withComponents(),
    NgxMaskModule
  ],
  exports: [PreviousApplicationsDeterminationsComponent]
})

export class PreviousApplicationsDeterminationsModule { }