import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class PreviousApplicationsDeterminationsService {
    getPreviousApplicationsAndDeterminationsURL = environment.pactApiUrl + 'DETApplicationReview/GetPreviousApplicationsAndDeterminations';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private httpClient: HttpClient) {
    }

     //Get Previous Applications & Determinations
     getPreviousApplicationsAndDeterminations(pactClientId: number): Observable<any> {
        if (pactClientId) {
            return this.httpClient.post(this.getPreviousApplicationsAndDeterminationsURL, JSON.stringify(pactClientId), this.httpOptions);
        }
    }
}