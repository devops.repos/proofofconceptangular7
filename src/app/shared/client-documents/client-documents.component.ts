import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-documents',
  templateUrl: './client-documents.component.html',
  styleUrls: ['./client-documents.component.scss']
})

export class ClientDocumentsComponent implements OnInit {
  @Input() clientDocumentsData: {
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    pactClientSearchAuditId: number;
    optionUserId: number;
  };

  //Variables
  fullName: string;

  constructor() {
  }
  
  ngOnInit() {
    if (this.clientDocumentsData
      && this.clientDocumentsData.lastName != null
      && this.clientDocumentsData.firstName != null) {
      this.fullName = this.clientDocumentsData.lastName + ", " + this.clientDocumentsData.firstName;
    }
  }
}
