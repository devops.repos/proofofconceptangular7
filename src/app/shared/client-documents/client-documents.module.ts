import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
import { ClientDocumentsComponent } from './client-documents.component';
import { NgxMaskModule } from 'ngx-mask';
import { ClientDocumentsGridModule } from '../client-documents-grid/client-documents-grid.module'

@NgModule({
  declarations: [ClientDocumentsComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    NgxMaskModule,
    ClientDocumentsGridModule
  ],
  exports: [ClientDocumentsComponent]
})

export class ClientDocumentsModule { }