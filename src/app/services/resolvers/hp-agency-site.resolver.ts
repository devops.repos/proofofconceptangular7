import { Injectable, OnDestroy } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { IVCSAgencyInput, IVCSRaHpAgenciesSites } from 'src/app/pact-modules/vacancyControlSystem/referral-roster/referral-roster-interface.model';
import { ReferralRosterService } from 'src/app/pact-modules/vacancyControlSystem/referral-roster/referral-roster.service';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class HpAgencySiteResolver implements Resolve<any>, OnDestroy {

  currentUser: AuthData;
  raHpAgenciesSites: IVCSRaHpAgenciesSites;

  currentUserSub: Subscription;
  raHpAgenciesSitesSub: Subscription;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private referralRosterService: ReferralRosterService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<IVCSRaHpAgenciesSites> {
    // console.log('HpAgencySiteResolver');
    return new Promise((resolve, reject) => {
      // console.log('HpAgencySiteResolver token valid');
      /** Getting the currently active user info */
      this.userService.getUserDataAsPromise().then(userdata => {
        if (userdata) {
          this.currentUser = userdata;
          // console.log('HpAgencySiteResolver UserData: ', this.currentUser);
          if (this.currentUser.siteCategoryType.length > 0) {
            /** Checking the User siteCategoryType either PE or HP */
            var shhp = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_HP);
            var shpe = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_PE);
            if (this.authService.isUserTokenValid()) {
              /* Get only HP Agencies/Sites with UserSecurity */
              const inp: IVCSAgencyInput = {
                DropdownSourceType: 2,
                CheckUserSecurity: (shhp.length > 0) ? true : false
              }
              this.raHpAgenciesSitesSub = this.referralRosterService.getRaHpAgenciesSites(inp).subscribe((res: IVCSRaHpAgenciesSites) => {
                if (res) {
                  // console.log('HpAgencySiteResolver getRaHpAgenciesSites');
                  this.raHpAgenciesSites = res;
                  resolve(this.raHpAgenciesSites);
                } else {
                  reject(null);
                }
              });
            } else {
              reject(null);
            }
          }
        }
      });
    });
  }

  ngOnDestroy(): void {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.raHpAgenciesSitesSub) {
      this.raHpAgenciesSitesSub.unsubscribe();
    }
  }
}
