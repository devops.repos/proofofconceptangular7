import { Injectable, OnDestroy } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { IVCSAgencyInput, IVCSRaHpAgenciesSites } from 'src/app/pact-modules/vacancyControlSystem/referral-roster/referral-roster-interface.model';
import { ReferralRosterService } from 'src/app/pact-modules/vacancyControlSystem/referral-roster/referral-roster.service';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ReferralRosterResolver implements Resolve<any>, OnDestroy {

  currentUser: AuthData;
  raHpAgenciesSites: IVCSRaHpAgenciesSites;

  currentUserSub: Subscription;
  raHpAgenciesSitesSub: Subscription;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private referralRosterService: ReferralRosterService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<IVCSRaHpAgenciesSites> {
    return new Promise((resolve, reject) => {
      // console.log('ReferralRosterResolver');
      /** Getting the currently active user info */
      this.userService.getUserDataAsPromise().then(userdata => {
        if (userdata) {
          this.currentUser = userdata;
          if (this.authService.isUserTokenValid()) {
            if (this.currentUser.siteCategoryType.length > 0) {
              /** Checking the User siteCategoryType either PE or HP */
              var shhp = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_HP);
              var shpe = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_PE);

              /* Load the RA/HP Agencies and Sites */
              if (shhp.length > 0) {
                /* Get only HP Agencies/Sites */
                const inp: IVCSAgencyInput = {
                  DropdownSourceType: 2,
                  CheckUserSecurity: true
                }
                this.raHpAgenciesSitesSub = this.referralRosterService.getRaHpAgenciesSites(inp).subscribe((res: IVCSRaHpAgenciesSites) => {
                  if (res) {
                    this.raHpAgenciesSites = res;
                    resolve(this.raHpAgenciesSites);
                    // console.log('HP raHpAgenciesSites: ', this.raHpAgenciesSites);
                    // return this.raHpAgenciesSites.asObservable();
                  } else {
                    reject(null);
                  }

                });
              } else {
                /* Get both RA/HP Agencies/Sites */
                const inp: IVCSAgencyInput = {
                  DropdownSourceType: 1,
                  CheckUserSecurity: false
                }
                this.raHpAgenciesSitesSub = this.referralRosterService.getRaHpAgenciesSites(inp).subscribe((res: IVCSRaHpAgenciesSites) => {
                  if (res) {
                    this.raHpAgenciesSites = res;
                    resolve(this.raHpAgenciesSites);
                    // console.log('HP raHpAgenciesSites: ', this.raHpAgenciesSites);
                    // return this.raHpAgenciesSites.asObservable();
                  } else {
                    reject(null);
                  }
                });
              }
            }
          } else {
            reject(null);
          }
        }
      });
    });
  }

  ngOnDestroy(): void {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.raHpAgenciesSitesSub) {
      this.raHpAgenciesSitesSub.unsubscribe();
    }
  }
}
