import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-juniper-authentication',
  templateUrl: './juniper-authentication.component.html',
  styleUrls: ['./juniper-authentication.component.scss'],
})
export class JuniperAuthenticationComponent implements OnInit {
  tempKey: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params) => {
      if (params) {
        this.tempKey = params.get('tempKey');
        // alert('tempKey: ' + this.tempKey);
        if (this.tempKey && this.tempKey !== null) {
          this.authService.authUser(null, this.tempKey);
        }
      }
    },
      (err) => {
        console.log('error during Juniper Authentication: ', err)
      }
    );
  }
}
