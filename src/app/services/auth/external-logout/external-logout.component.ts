import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-external-logout',
  templateUrl: './external-logout.component.html',
  styleUrls: ['./external-logout.component.scss']
})
export class ExternalLogoutComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.setIsJuniperUserFlag(true);
    setTimeout(() => {
      this.authService.logout();
    }, 100);
  }

}
