import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  Router
} from '@angular/router';
import { AuthService } from '../auth.service';
import { UserService } from '../../helper-services/user.service';
import { CommonService } from '../../helper-services/common.service';
import { ShRaAuthGuard } from './sh-ra-auth.guard';
import { ShPeAuthGuard } from './sh-pe-auth.guard';
import { ShHpAuthGuard } from './sh-hp-auth.guard';
import { CasAuthGuard } from './cas-auth.guard';
import { MlDhsLiasianAuthGuard } from './ml-dhs-liasian-auth.guard';
import { MlTeamAuthGuard } from './ml-team-auth.guard';
import { MlHpAuthGuard } from './ml-hp-auth.guard';
import { MlIreaAuthGuard } from './ml-irea-auth.guard';
import { SiteTypeGuard, MixedAuthGuard, UserRoleGuard } from 'src/app/models/pact-enums.enum';
import { ImpersonateAuthGuard } from './impersonate-auth.guard';
import { CasSysAdminAuthGuard } from '../combined-role-auth-guards/cas-sys-admin-auth.guard';
import { AllSiteCategoryAuthGuard } from '../combined-role-auth-guards/all-site-category-auth.guard';

import { StaffAuthGuard } from '../user-role-auth-guards/staff-auth.guard';
import { SupervisorAuthGuard } from '../user-role-auth-guards/supervisor-auth.guard';
import { ManagerAuthGuard } from '../user-role-auth-guards/manager-auth.guard';
import { SysAdminAuthGuard } from '../user-role-auth-guards/sys-admin-auth.guard';
import { ShHpManagerAuthGuard } from '../combined-role-auth-guards/sh-hp-manager-auth.guard';
import { ShHpSupervisorAuthGuard } from '../combined-role-auth-guards/sh-hp-supervisor-auth.guard';
import { ShHpSysAdminAuthGuard } from '../combined-role-auth-guards/sh-hp-sys-admin-auth.guard';
import { ShPeManagerAuthGuard } from '../combined-role-auth-guards/sh-pe-manager-auth.guard';
import { ShPeSysAdminAuthGuard } from '../combined-role-auth-guards/sh-pe-sys-admin-auth.guard';
import { ShRaManagerAuthGuard } from '../combined-role-auth-guards/sh-ra-manager-auth.guard';
import { ShRaSupervisorAuthGuard } from '../combined-role-auth-guards/sh-ra-supervisor-auth.guard';
import { ShRaSysAdminAuthGuard } from '../combined-role-auth-guards/sh-ra-sys-admin-auth.guard';
import { ShPeSupervisorAuthGuard } from '../combined-role-auth-guards/sh-pe-supervisor-auth.guard';
import { CasSupervisorAuthGuard } from '../combined-role-auth-guards/cas-supervisor-auth.guard';
import { CasStaffAuthGuard } from '../combined-role-auth-guards/cas-staff-auth.guard';

@Injectable({
  providedIn: 'root'
})
export class SiteTypeMasterAuthGuard implements CanActivate {
  // you may need to include dependencies of individual guards if specified in guard constructor
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private commonService: CommonService,
    private router: Router
  ) { }

  private route: ActivatedRouteSnapshot;
  private state: RouterStateSnapshot;

  // This method gets triggered when the route is hit
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.route = route;
    this.state = state;

    if (!route.data) {
      console.log('!route.data');
      this.router.navigate(['/dashboard']);
      Promise.resolve(true);
      return;
    }

    // this.route.data.guards is an array of strings set in routing configuration

    if (!this.route.data.guards || !this.route.data.guards.length) {
      // No data has been set for SiteTypeAuthGuard in routing.
      // for now redirecting to dashboard, change the expected logic here.
      this.router.navigate(['/dashboard']);
      Promise.resolve(true);
      return;
    }
    return this.executeSiteTypeAuthGuards();
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  // Execute all the guards sent from the route's data.guards
  private executeSiteTypeAuthGuards(guardIndex: number = 0): boolean {
    let rslt = false;
    do {
      // console.log('this.route.data.guards.length: ',this.route.data.guards.length);
      rslt = this.activateGuard(this.route.data.guards[guardIndex]);
      // console.log('this.activateGuard(' + this.route.data.guards[guardIndex] + ') : ', rslt);
      if (rslt) {
        break;
      }
      // console.log('result: ', rslt);
      guardIndex = guardIndex + 1;
      // console.log('guardIndex++ : ', guardIndex);
    } while (guardIndex < this.route.data.guards.length);

    return rslt;
  }
  // Create an instance of the guard and fire canActivate method returning a boolean
  private activateGuard(guardKey: string): boolean {
    // tslint:disable-next-line: max-line-length
    let guard: ShRaAuthGuard | ShPeAuthGuard | ShHpAuthGuard | CasAuthGuard | MlDhsLiasianAuthGuard |
      MlTeamAuthGuard | MlHpAuthGuard | MlIreaAuthGuard | ImpersonateAuthGuard |
      AllSiteCategoryAuthGuard | StaffAuthGuard | SupervisorAuthGuard | ManagerAuthGuard | SysAdminAuthGuard |
      ShHpManagerAuthGuard | ShHpSupervisorAuthGuard | ShHpSysAdminAuthGuard |
      ShPeManagerAuthGuard | ShPeSupervisorAuthGuard | ShPeSysAdminAuthGuard |
      ShRaManagerAuthGuard | ShRaSupervisorAuthGuard | ShRaSysAdminAuthGuard |
      CasStaffAuthGuard | CasSysAdminAuthGuard | CasSupervisorAuthGuard;
    // console.log('activateGuard(guardKey = '+ guardKey + ')');

    switch (guardKey) {
      // Register cases for all the AuthGuards
      case SiteTypeGuard.SH_RA:
        guard = new ShRaAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case SiteTypeGuard.SH_PE:
        guard = new ShPeAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case SiteTypeGuard.SH_HP:
        guard = new ShHpAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case SiteTypeGuard.CAS:
        guard = new CasAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case SiteTypeGuard.ML_DHS_Liasian:
        guard = new MlDhsLiasianAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case SiteTypeGuard.ML_Team:
        guard = new MlTeamAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case SiteTypeGuard.ML_HP:
        guard = new MlHpAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case SiteTypeGuard.ML_IREA:
        guard = new MlIreaAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case SiteTypeGuard.Impersonate:
        guard = new ImpersonateAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.CAS_STAFF:
        guard = new CasStaffAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.CAS_SUPERVISOR:
        guard = new CasSupervisorAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.CAS_SYS_ADMIN:
        guard = new CasSysAdminAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.AllSiteCategory:
        guard = new AllSiteCategoryAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_RA_MANAGER:
        guard = new ShRaManagerAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_RA_SUPERVISOR:
        guard = new ShRaSupervisorAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_RA_SYS_ADMIN:
        guard = new ShRaSysAdminAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_HP_MANAGER:
        guard = new ShHpManagerAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_HP_SUPERVISOR:
        guard = new ShHpSupervisorAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_HP_SYS_ADMIN:
        guard = new ShHpSysAdminAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_PE_MANAGER:
        guard = new ShPeManagerAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_PE_SUPERVISOR:
        guard = new ShPeSupervisorAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case MixedAuthGuard.SH_PE_SYS_ADMIN:
        guard = new ShPeSysAdminAuthGuard(this.authService, this.userService, this.commonService);
        break;
      case UserRoleGuard.STAFF:
        guard = new StaffAuthGuard(this.authService, this.userService);
        break;
      case UserRoleGuard.SUPERVISOR:
        guard = new SupervisorAuthGuard(this.authService, this.userService);
        break;
      case UserRoleGuard.MANAGER:
        guard = new ManagerAuthGuard(this.authService, this.userService);
        break;
      case UserRoleGuard.SYS_ADMIN:
        guard = new SysAdminAuthGuard(this.authService, this.userService);
        break;
      default:
        break;
    }
    return guard.canActivate(this.route, this.state);
  }
}
