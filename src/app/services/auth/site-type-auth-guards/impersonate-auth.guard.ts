import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from '../../helper-services/user.service';
import { CommonService } from '../../helper-services/common.service';
import { UserSiteType, Impersonate } from 'src/app/models/pact-enums.enum';

@Injectable({
  providedIn: 'root'
})
export class ImpersonateAuthGuard implements CanActivate {
   userData: AuthData;
   userDataSub: Subscription;
   isShRa = false;
   isShPe = false;
   isShHp = false;
   isCas = false;
   isMlDhsLiasian = false;
   isMlTeam = false;
   isMlHp = false;
   isMlIrea = false;
   isImpersonate = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private commonService: CommonService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // console.log('Login requesting URL: ', state.url);
    if (state.url) {
      this.authService.setRequestingSource(state.url);
    }

    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        // User Data exist
        if (!this.authService.isUserTokenValid()) {
          // User token has expired
          this.authService.authUser();
        } else {
          if (this.userData.siteCategoryType.length > 0) {
            this.isShRa = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_RA);
            this.isShPe = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_PE);
            this.isShHp = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_HP);
            this.isCas = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.CAS);
            this.isMlDhsLiasian = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_DHS_Liasian);
            this.isMlTeam = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_Team);
            this.isMlHp = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_HP);
            this.isMlIrea = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_IREA);
          }
          if (this.userData.userFunctions.length > 0) {
            // console.log("userFunctions length: ", this.userData.userFunctions.length);
            this.isImpersonate = this.commonService._doesValueExistInJson(this.userData.userFunctions, Impersonate.Impersonate);
          }
        }
      } else {
        // login again
        this.authService.authUser();
      }
    });
    // tslint:disable-next-line: max-line-length
    if ((this.isShRa || this.isShPe || this.isShHp || this.isCas || this.isMlDhsLiasian || this.isMlTeam || this.isMlHp || this.isMlIrea) && this.isImpersonate) {
        return true;
    }
  }
}
