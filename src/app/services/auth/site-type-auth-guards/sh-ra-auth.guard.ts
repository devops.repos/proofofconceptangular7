import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
} from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from '../../helper-services/user.service';
import { CommonService } from '../../helper-services/common.service';
import { UserSiteType } from 'src/app/models/pact-enums.enum';

@Injectable({
  providedIn: 'root'
})
export class ShRaAuthGuard implements CanActivate {
  userData: AuthData;
  userDataSub: Subscription;
  isShRa = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private commonService: CommonService
 ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // console.log('Login requesting URL: ', state.url);
    if (state.url) {
      this.authService.setRequestingSource(state.url);
    }

    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        // User Data exist
        if (!this.authService.isUserTokenValid()) {
          // User token has expired
          this.authService.authUser();
        } else if (this.userData.siteCategoryType.length > 0) {
          this.isShRa = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_RA);
        }
      } else {
        // login again
        this.authService.authUser();
      }
    });
    // console.log('isShRa : ', this.isShRa);
    return this.isShRa;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }
}
