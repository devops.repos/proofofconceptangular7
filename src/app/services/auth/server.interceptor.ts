import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, finalize } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CommonService } from '../helper-services/common.service';

@Injectable()
export class ServerInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private commonService: CommonService
  ) { }

  reqApplicaiton = "PACT";
  private requestCount: number = 0;

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const excludeIdentity = '/api/identity';
    const excludeLogger = '/api/Logger/Error/';

    const activeSys = this.router.url.split('/');
    if (activeSys[1] != "") {
      this.reqApplicaiton = activeSys[1];
    }

    this.requestCount++;
    //console.log(request.url + ' = ' + this.requestCount);
    if (this.requestCount === 1) {
      Promise.resolve(null).then(() => this.commonService.setIsOverlay(true));
    }

    /* Excluding the bearer token being added in api calls for the Identity Service api call */
    if (!request.url.endsWith(excludeIdentity)) {
      const authToken = this.authService.getToken();
      let headers = request.headers
        .set('reqApplication', this.reqApplicaiton)
        .set('Authorization', `Bearer ${authToken}`);

      //request = request.clone({setHeaders: { Authorization: `Bearer ${authToken}` }});.
      request = request.clone({ headers });

      return next.handle(request).pipe(
        finalize(() => {
          this.requestCount--;
          if (this.requestCount === 0) {
            setTimeout(() => {
              this.commonService.setIsOverlay(false);
            }, 500);
          }
        }),
        catchError((err: any) => {
          this.commonService.setIsOverlay(false);
          if (err instanceof HttpErrorResponse) {
            throw new HttpErrorResponse(err);
          }
          return of(err);
        })
      );
    } else {
      return next.handle(request).pipe(
        finalize(() => {
          this.requestCount--;
          if (this.requestCount === 0) {
            setTimeout(() => {
              this.commonService.setIsOverlay(false);
            }, 500);
          }
        })
      );
    }
  }
}
