import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-external-authentication',
  templateUrl: './external-authentication.component.html',
  styleUrls: ['./external-authentication.component.scss']
})
export class ExternalAuthenticationComponent implements OnInit {
  private lanID: string;
  private targetURL: string;
  private parameter: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.lanID = params.lanID;
      this.targetURL = params.targetURL;
      this.parameter = params.parameter;
      // console.log('lanID: ', this.lanID);
      // console.log('targetURL: ', this.targetURL);
      // console.log('parameter: ', this.parameter);

      if (this.lanID) {
        this.authService.setIsJuniperUserFlag(true);
        if (this.targetURL && this.parameter) {
          this.authService.authUser(this.lanID, null, this.targetURL, this.parameter);
        } else if (this.targetURL) {
          this.authService.authUser(this.lanID, null, this.targetURL);
        } else {
          this.authService.authUser(this.lanID);
        }
      }
    });
  }

  ngOnInit() {


    // this.activatedRoute.paramMap.subscribe((params) => {
    //   this.lanID = params.get('lanID');
    //   this.targetURL = params.get('targetURL');
    //   this.parameter = params.get('parameter');
    //   if (this.targetURL == 'newApp') {
    //     this.targetURL = 'shs/' + this.targetURL;
    //   } else if (this.targetURL == 'pendingApp') {
    //     this.targetURL = 'shs/pendingApp/consent-search'
    //   } else if (this.targetURL == 'transmittedApps') {
    //     this.targetURL = 'shs/transmittedApps';
    //   }
    //   console.log('lanID: ', this.lanID);
    //   console.log('targetURL: ', this.targetURL);
    //   console.log('parameter: ', this.parameter);
    //   // alert('lanID: ' + this.lanID);
    //   if (this.lanID) {
    //     this.authService.setIsJuniperUserFlag(true);
    //     this.authService.authUser(this.lanID, null, this.targetURL, this.parameter);
    //   }
    // });

  }

}
