import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
} from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from '../../helper-services/user.service';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { CommonService } from '../../helper-services/common.service';

@Injectable({
  providedIn: 'root'
})
export class AllSiteCategoryAuthGuard implements CanActivate {
  userData: AuthData;
  userDataSub: Subscription;
  isShRa = false;
  isShPe = false;
  isShHp = false;
  isCas = false;
  isMlDhsLiasian = false;
  isMlTeam = false;
  isMlHp = false;
  isMlIrea = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private commonService: CommonService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    // console.log('Login requesting URL: ', state.url);
    if (state.url) {
      this.authService.setRequestingSource(state.url);
    }

    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        /* User Data exist */
        if (!this.authService.isUserTokenValid()) {
          /* User token has expired */
          this.authService.authUser();
        } else if (this.userData.siteCategoryType.length > 0) {
            this.isShRa = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_RA);
            this.isShPe = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_PE);
            this.isShHp = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_HP);
            this.isCas = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.CAS);
            this.isMlDhsLiasian = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_DHS_Liasian);
            this.isMlIrea = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_IREA);
            this.isMlHp = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_HP);
            this.isMlTeam = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.ML_Team);
          }
      } else {
        /* login again */
        this.authService.authUser();
      }
    });
    // console.log('isSysAdmin : ', this.isSysAdmin);
    // console.log('isCas: ', this.isCas);
    if (this.isShRa || this.isShPe || this.isShHp || this.isCas || this.isMlDhsLiasian || this.isMlTeam || this.isMlIrea || this.isMlHp) {
        return true;
    }
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return this.canActivate(route, state);
  }
}
