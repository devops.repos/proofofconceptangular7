import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthData } from '../../models/auth-data.model';
import { environment } from '../../../environments/environment';
import { UserService } from '../helper-services/user.service';
import { CommonService } from '../helper-services/common.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { IdleDialogBoxComponent } from 'src/app/core/session-out/idle-dialog-box/idle-dialog-box.component';
import { UserSiteType } from '../../models/pact-enums.enum';

import * as jwt_decode from 'jwt-decode';
import { NavigationService } from 'src/app/core/sidenav-list/navigation.service';
import { catchError } from 'rxjs/operators';
import { INavigationItems } from 'src/app/core/sidenav-list/navigation.interface';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private URL = environment.pactApiUrl + 'identity';
  private finalURL = this.URL;
  private loginURL = environment.pactApiUrl + 'Identity/Login';

  private isJuniperUser = new BehaviorSubject<boolean>(false);

  private token: string;
  private tokenTimer: any;
  expiresInDuration: number;

  private source = '/';

  waitingTime = environment.idleDialogWaitTime; // Default 600 seconds = 10 minutes
  idleDialogRef: MatDialogRef<IdleDialogBoxComponent>;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private lanIDAuthenticationCompletedEvent = new BehaviorSubject<string>(null);

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private userService: UserService,
    private commonService: CommonService,
    private dialog: MatDialog,
    private navigationService: NavigationService
  ) { }

  getToken() {
    return this.token;
  }

  getRequestingSource() {
    return this.source;
  }

  setRequestingSource(src: string) {
    this.source = src;
    // console.log('sourceURL: ', this.source);
  }

  authUser(lanID?: string, juniperKey?: string, targetURL?: string, parameter?: string) {
    if (juniperKey != null) {
      this.finalURL = '';
      this.isJuniperUser.next(true);
      this.finalURL = environment.pactApiUrl + 'juniper/JuKeyLogin';
      this.httpClient.post(this.finalURL, JSON.stringify(juniperKey), this.httpOptions).subscribe(res => {
        const data = res as AuthData;
        if (data.optionUserId > 0) {
          this.authUserResponse(data);
        }
      });
    } else {
      this.finalURL = this.URL;
      if (lanID) {
        // console.log('Impersonation Occured');
        this.finalURL = '';
        this.finalURL = this.URL + '?LanID=' + lanID;
      }

      let dataObsrv: Observable<any> = this.httpClient.get(this.finalURL, { withCredentials: true, observe: 'response' }).pipe(
        catchError((error) => {
          if (error.status == 403) {
            throw error;
          } else if (error.status == 404) {
            let msg: string = error.error.toString();
            let arrayString: string[] = msg.split(' ');
            // console.log('LanID from IdentityService = ' + arrayString[1]);
            this.emitAuthenticationCompletedEvent(arrayString[1]);
            this.router.navigate(['/login']); // redirect to login screen to capture userid / password
          } else {
            throw error;
          }

          return of(null as AuthData) //
        })
      );

      dataObsrv.subscribe(res => {
        const data = res ? (res.body as AuthData) : null;
        if (data.optionUserId > 0) {
          this.authUserResponse(data, targetURL, parameter);
        }
      })
    }
  }

  getIsJuniperUserFlag() {
    return this.isJuniperUser.asObservable();
  }
  setIsJuniperUserFlag(flag: boolean) {
    this.isJuniperUser.next(flag);
  }

  authUserResponse(data: AuthData, targetURL?: string, parameter?: string) {
    const is_SH_RA = this.commonService._doesValueExistInJson(data.siteCategoryType, UserSiteType.SH_RA);
    const is_SH_PE = this.commonService._doesValueExistInJson(data.siteCategoryType, UserSiteType.SH_PE);
    const is_SH_HP = this.commonService._doesValueExistInJson(data.siteCategoryType, UserSiteType.SH_HP);
    const is_CAS = this.commonService._doesValueExistInJson(data.siteCategoryType, UserSiteType.CAS);
    const is_ML_DHS_Liasian = this.commonService._doesValueExistInJson(data.siteCategoryType, UserSiteType.ML_DHS_Liasian);
    const is_ML_Team = this.commonService._doesValueExistInJson(data.siteCategoryType, UserSiteType.ML_Team);
    const is_ML_HP = this.commonService._doesValueExistInJson(data.siteCategoryType, UserSiteType.ML_HP);
    const is_ML_IREA = this.commonService._doesValueExistInJson(data.siteCategoryType, UserSiteType.ML_IREA);
    this.userService.setUserData(data);
    this.token = data.authToken;
    if (this.token) {
      /* Fetch the Navigation Items For New login Request */
      this.navigationService.fetchNavigationItems(data.optionUserId).subscribe((res: INavigationItems) => {
        if (res) {
          this.navigationService.setNavigationItems(res);

          const tokenValue = jwt_decode(this.token);
          // console.log('token values: ', tokenValue);
          // console.log('LanId from token : ', tokenValue.unique_name);
          // const expiresInDuration = tokenValue.exp - (new Date().getTime() / 1000);
          this.expiresInDuration = 3600;
          // console.log('expiresInDuration: ', expiresInDuration);
          this.setAuthTimer(this.expiresInDuration);
          const now = new Date();
          const expirationDate = new Date(now.getTime() + this.expiresInDuration * 1000);
          // console.log('expirationDate: ', expirationDate);
          this.saveAuthData(this.token, expirationDate);
          // console.log('current URL : ', this.source);
          if (is_SH_RA || is_SH_PE || is_SH_HP || is_CAS || is_ML_DHS_Liasian || is_ML_HP || is_ML_Team || is_ML_IREA) {
            /* TagetURL and parameter are currently used for Juniper redirection From CAPS survey */
            if (targetURL && parameter) {
              this.source = '/' + targetURL + '/' + parameter;
              // console.log('source External: ', this.source);
            } else if (targetURL) {
              this.source = '/' + targetURL;
              // console.log('source External: ', this.source);
            }

            if (this.source.startsWith('/session-out') || this.source.startsWith('/impersonate')) {
              this.router.navigate(['/dashboard']);
            } else if (this.source !== '') {
              this.router.navigate([this.source]);
            } else {
              this.router.navigate(['/dashboard']);
            }
          } else {
            throw new Error('You don\'t have proper access right.');
          }
        }
      });

    } else {
      throw new Error('Client not Authorized to access this application.');
    }
  }

  isUserTokenValid(): boolean {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return false;
    } else {
      const now = new Date();
      const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
      // console.log('autoAuthUser: expiresIn : ' + expiresIn);
      if (expiresIn > 0) {
        this.token = authInformation.token;
        this.setAuthTimer(expiresIn / 1000);
      } else {
        this.clearAuthData();
        return false;
      }
      return true;
    }
  }

  login(userID: string, password: string, LanID: string): Observable<any> {
    return this.httpClient.post(this.loginURL, JSON.stringify({ "userID": userID, "password": password, "LanID": LanID }), this.httpOptions);
  }

  logout() {
    this.token = null;
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    if (this.isJuniperUser.value == true) {
      // console.log('IsJuniperUser Flag: ', this.isJuniperUser.value);
      this.isJuniperUser.next(false);
      window.location.href = environment.juniperLogoutUrl;
    }
  }

  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(token: string, expirationDate: Date) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
  }

  private getAuthData() {
    const tkn = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    if (!tkn || !expirationDate) {
      return;
    }
    return {
      token: tkn,
      expirationDate: new Date(expirationDate)
    };
  }

  emitAuthenticationCompletedEvent(lanID: string) {
    this.lanIDAuthenticationCompletedEvent.next(lanID);
  }

  subscribeAuthenticationEventListner() {
    return this.lanIDAuthenticationCompletedEvent.asObservable();
  }

}
