import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';

export interface ClientApplication {
  pactApplicationId?: number;
  srno: number;
  siteId: number;
  housingProgramType: number;
  housingProgram: string;
  applicationType: number;
  application: string;
  pactClientId: number;
  applicationCreatedDate: string;
  pactDemographicId: number;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  age: number;
  genderType: number;
  gender: string;
  cin: string;
  supportiveHousingType: string;
  isHoliday: boolean;
  validationMessage: string;
}

@Injectable({
  providedIn: 'root'
})
export class ClientApplicationService {

  defaultClientApplicationData: ClientApplication;
  private clientApplicationData = new BehaviorSubject<ClientApplication>(this.defaultClientApplicationData);
  private pactApplicationId = new BehaviorSubject<number>(0);
  private pactApiURL = environment.pactApiUrl + 'SidenavStatus';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    response: 'json',
  };

  constructor(private httpClient: HttpClient) { }

  setClientApplicationData(clientdata: ClientApplication) {
    if (clientdata) {
      this.clientApplicationData.next(clientdata);
      if (clientdata.pactApplicationId) {
        this.pactApplicationId.next(clientdata.pactApplicationId);
      }
    }
  }

  getClientApplicationData() {
    return this.clientApplicationData.asObservable();

  }
  getPactApplicationID() {
    return this.pactApplicationId.asObservable();
  }

  //Set client application data with the application id from pending applications list.
  setClientApplicationDataWithApplicationId(appid: number) {
    this.httpClient
      .post(
        this.pactApiURL + '/GetClientApplication/',
        JSON.stringify(appid),
        this.httpOptions
      )
      .subscribe(result => {
        const data = result as ClientApplication;
        this.setClientApplicationData(
          data
        );
      });
  }
}
