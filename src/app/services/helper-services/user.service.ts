import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserAgencyType } from 'src/app/models/pact-enums.enum';
import { AuthData } from 'src/app/models/auth-data.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userData = new BehaviorSubject<AuthData>(null);
  private userData1: AuthData;

  private user = new BehaviorSubject<string>(null);
  private currentUser = this.user.asObservable();

  private userAgencyType = new BehaviorSubject<string>(UserAgencyType.ReferringAgency);
  private currentUserAgencyType = this.userAgencyType.asObservable();

  constructor() { }

  setUserData(userdata: AuthData) {
    this.userData.next(userdata);
    this.userData1 = userdata;
  }

  getUserData() {
    return this.userData.asObservable();
  }
  getUserDataAsPromise(): Promise<AuthData> {
    // return this.userData.asObservable();
    return new Promise((resolve, reject) => {
      if (this.userData1) {
        resolve(this.userData1);
      } else {
        reject();
      }
    });
  }

  getCurrentUser() {
    return this.currentUser;
  }

  setCurrentUser(user: string) {
    this.user.next(user);
  }

  getCurrentUserAgencyType() {
    return this.currentUserAgencyType;
  }

  setCurrentUserAgencyType(userAgencyType: string) {
    this.userAgencyType.next(userAgencyType);
  //   if (userRole === 'SuperAdmin') {
  //     this.router.navigate(['/']);
  //   } else if (userRole === 'PlacementEntity') {
  //     this.router.navigate(['/vcs/pe-dashboard']);
  //   } else if (userRole === 'HousingProvider') {
  //     this.router.navigate(['/vcs/hp-dashboard']);
  //   } else if (userRole === 'Reviewer') {
  //     this.router.navigate(['/vcs/reviewer-dashboard']);
  //   } else {
  //     throw new Error('Access denied. No userRole assigned.');
  //   }
  }
}
