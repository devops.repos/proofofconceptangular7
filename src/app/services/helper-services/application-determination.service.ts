import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { Subscription } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class ApplicationDeterminationService implements OnDestroy {

    private pactApiURL = environment.pactApiUrl + 'DETSideNav';
    applicationDataForDeterminationWithApplicationSub: Subscription;

    defaultApplicationDeterminationData: iApplicationDeterminationData;
    private applicationDeterminationData = new BehaviorSubject<iApplicationDeterminationData>(this.defaultApplicationDeterminationData);

    determinationApplicationId = new BehaviorSubject<number>(0);

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        }),
        response: 'json',
    };

    //Constructor
    constructor(private httpClient: HttpClient) {
    }

    //Set Application ID For Determination
    setApplicationIdForDetermination(applicationId: number) {
        this.applicationDataForDeterminationWithApplicationSub = this.setApplicationDataForDeterminationWithApplicationId(applicationId)
            .subscribe(result => {
                const data = result as iApplicationDeterminationData;
                this.setApplicationDeterminationData(data);
            });
        this.determinationApplicationId.next(applicationId);
    }

    //Set Application ID For Determination - Clinical Review
    setApplicationIdForDetermination1(applicationId: number): Promise<Boolean> {
        return new Promise((resolve, reject) => {
            this.determinationApplicationId.next(applicationId);
            this.applicationDataForDeterminationWithApplicationSub = this.setApplicationDataForDeterminationWithApplicationId(applicationId)
                .subscribe(result => {
                    if (result) {
                        const data = result as iApplicationDeterminationData;
                        this.setApplicationDeterminationData(data);
                        resolve(true);
                    } else {
                        reject(false);
                    }
                });
        })
    }

    //Get Application ID For Determination
    getApplicationIdForDetermination() {
        return this.determinationApplicationId.asObservable();
    }

    //Set Application Determination Data
    setApplicationDeterminationData(applicationdata: iApplicationDeterminationData) {
        if (applicationdata) {
            this.applicationDeterminationData.next(applicationdata);
            if (applicationdata.pactApplicationId) {
                this.determinationApplicationId.next(applicationdata.pactApplicationId);
            }
        }
    }

    //Get Application Determination Data
    getApplicationDeterminationData() {
        return this.applicationDeterminationData.asObservable();
    }

    //Application data for determination with application id
    setApplicationDataForDeterminationWithApplicationId(pactApplicationId: number): Observable<any> {
        if (pactApplicationId) {
            return this.httpClient.post(this.pactApiURL + '/GetClientApplicationForDetermination/', JSON.stringify(pactApplicationId), this.httpOptions);
        }
    }

    //On Destroy
    ngOnDestroy() {
        if (this.applicationDataForDeterminationWithApplicationSub) {
            this.applicationDataForDeterminationWithApplicationSub.unsubscribe();
        }
    }
}
