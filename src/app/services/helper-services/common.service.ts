import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
import * as _ from 'lodash';

//Models
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { DETTabTimeDuration } from 'src/app/models/determination-common.model'

//Month To Comparable Number
export function monthToComparableNumber(date: any): number {
  if (date === undefined || date === null || date.length !== 10) {
    return null;
  }
  var d: Date = new Date(date);
  if (d) {
    let yearNumber = d.getFullYear();
    let monthNumber = d.getMonth() + 1;
    let dayNumber = d.getDate();
    let result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
    return result;
  } else {
    return null;
  }
}

@Injectable({
  providedIn: 'root'
})

export class CommonService {
  getRefGroupDetailsURL = environment.pactApiUrl + 'Common/GetRefGroupDetailsByRefGroupIds';
  getRefDiagnosisURL = environment.pactApiUrl + 'Common/GetAllDiagnosisList';
  GetAllPrimaryServiceContractURL = environment.pactApiUrl + 'Common/GetAllPrimaryServiceContract';
  getPrimaryServiceAgreementPopulationURL = environment.pactApiUrl + 'Common/GetPrimaryServiceAgreementPopulation';
  getAllPrimaryServiceAgreementPopulationURL = environment.pactApiUrl + 'Common/GetAllPrimaryServiceAgreementPopulation';
  getAllPrimaryServiceAgreementPopulationsRentalSubsidiesURL = environment.pactApiUrl + 'Common/GetPrimaryServiceAgreementPopulationsRentalSubsidies';
  generateGUIDUrl = environment.pactApiUrl + 'Common/GenerateGUID';
  generateReportUrl = environment.pactApiUrl + 'Common/GenerateReport';
  applicationPackageURL = environment.pactApiUrl + 'Document/GetApplicationPackage';
  documentLinkURL = environment.pactApiUrl + 'Document/GetDocumentLink';
  saveTabTimeDurationURL = environment.pactApiUrl + 'DETCommon/SaveTabTimeDuration';
  getVideoTutorialUrl = environment.pactApiUrl + 'Common/GetTuturialURL';
  getCasReviewerURL = environment.pactApiUrl + 'Common/GetCASReviewerList';

  
  isOverlay = new BehaviorSubject<boolean>(false);
  applicationSummaryGuid: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    response: 'json',
  };

  constructor(private httpClient: HttpClient) { }

  // get ref group details
  getRefGroupDetails(value: string) {
    return this.httpClient.get(this.getRefGroupDetailsURL, { params: { refGroupIds: value }, observe: 'response' });
  }

  getRefDiagnosis() {
    return this.httpClient.get(this.getRefDiagnosisURL, this.httpOptions);
  }

  getCasReviewer(){
    return this.httpClient.get(this.getCasReviewerURL,this.httpOptions);
  }

  getAllPrimaryServiceContract() {
    return this.httpClient.get(this.GetAllPrimaryServiceContractURL, this.httpOptions);
  }

  getPrimaryServiceAgreementPopulation(siteId: number) {
    return this.httpClient.get(this.getPrimaryServiceAgreementPopulationURL + '?SiteID=' + siteId, this.httpOptions);
  }

  getAllPrimaryServiceAgreementPopulation() {
    return this.httpClient.get(this.getAllPrimaryServiceAgreementPopulationURL, { observe: 'response' });
  }

  getPrimaryServiceAgreementPopulationsRentalSubsidies() {
    return this.httpClient.get(this.getAllPrimaryServiceAgreementPopulationsRentalSubsidiesURL, { observe: 'response' });
  }

  //Get Document Package
  getDocumentPackage(pactApplicationId: number): Observable<any> {
    if (pactApplicationId) {
      return this.httpClient.post(this.applicationPackageURL, JSON.stringify(pactApplicationId), this.httpOptions);
    }
  }

  //Get FileNet Document Link
  getFileNetDocumentLink(fileNetDocID: string): Observable<any> {
    if (fileNetDocID) {
      return this.httpClient.post(this.documentLinkURL, JSON.stringify(fileNetDocID), this.httpOptions);
    }
  }

  //Save Determination Tab Time Duration
  saveTabTimeDuration(detTabTimeDuration: DETTabTimeDuration): Observable<any> {
    if (detTabTimeDuration) {
      return this.httpClient.post(this.saveTabTimeDurationURL, JSON.stringify(detTabTimeDuration), this.httpOptions);
    }
  }

  //Generate GUID For PACT Reports
  generateGUID(pactReportParameters: PACTReportParameters[]): Observable<any> {
    return this.httpClient.post(this.generateGUIDUrl, JSON.stringify(pactReportParameters), this.httpOptions);
  }

  //Generate Reports
  generateReport(pactReportUrlParams: PACTReportUrlParams): Observable<any> {
    return this.httpClient.request(new HttpRequest(
      'GET',
      `${this.generateReportUrl}?reportParameterID=${pactReportUrlParams.reportParameterID}&reportName=${pactReportUrlParams.reportName}&reportFormat=${pactReportUrlParams.reportFormat}`,
      null,
      {
        reportProgress: true,
        responseType: 'blob'
      }));
  }

  getVideoURL(page: string) {
    //return this.httpClient.get(this.getVideoTutorialUrl + page);
    return this.httpClient.get(this.getVideoTutorialUrl, { params: { pageName: page }, observe: 'response' });
  }

  /** function to check if the value exists in the given json of any level */
  _doesValueExistInJson(json: any, value: any) {
    let contains = false;
    Object.keys(json).some(key => {
      contains = typeof json[key] === 'object' ? this._doesValueExistInJson(json[key], value) : json[key] === value;
      return contains;
    });
    return contains;
  }

  //Open Window
  OpenWindow(url: string) {
    var width = 800;
    var height = 600;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;
    var params = 'width=' + width + ', height=' + height;
    params += ', top=' + top + ', left=' + left;
    params += ', directories = no';
    params += ', location = no';
    params += ', menubar = no';
    params += ', resizable = yes';
    params += ', status = no';
    params += ', toolbar = no';
    params += ', scrollbars = 1';
    const newwin = window.open(url, '_blank', params);
    if (newwin != null) {
      newwin.focus();
    }
    return false;
  }

  //Set is-Overlay flag
  setIsOverlay(value: boolean) {
    this.isOverlay.next(value);
  }

  //Get is-Overlay flag
  getIsOverlay() {
    return this.isOverlay.asObservable();
  }

  //Display Survey Summary Report
  displaySurveySummaryReport(capsReportId: string, optionUserId: number) {
    this.setIsOverlay(true);
    var reportParams: PACTReportUrlParams;
    reportParams = { reportParameterID: capsReportId, reportName: "AssessmentSurveyReport", "reportFormat": "PDF" };
    this.generateReport(reportParams)
      .subscribe(
        res => {
          if (res) {
            var data = new Blob([res.body], { type: 'application/pdf' });
            if (data.size > 512) {
              this.OpenWindow(URL.createObjectURL(data));
              this.setIsOverlay(false);
            }
          }
        },
        error => {
          throw new Error("Unable to genarate report due to invalid survey number!");
        }
      );
  }

  //Display Application Summary Report
  displayApplicationSummaryReport(selectedApplicationID: string, optionUserId: number) {
    this.setIsOverlay(true);
    var reportParameters: PACTReportParameters[] = [];
    reportParameters = [
      { parameterName: 'applicationID', parameterValue: selectedApplicationID.toString(), CreatedBy: optionUserId },
      { parameterName: 'reportName', parameterValue: "PACTReportApplicationSummary", CreatedBy: optionUserId }
    ];
    this.generateGUID(reportParameters)
      .subscribe(
        res => {
          if (res) {
            this.applicationSummaryGuid = res;
            this.generateApplicationSummaryReport(res, "PACTReportApplicationSummary", optionUserId);
          }
        }
      );
  }

  //Generate Application Summary Report
  generateApplicationSummaryReport(reportGuid: string, reportName: string, optionUserId: number) {
    var reportParams: PACTReportUrlParams;
    reportParams = { reportParameterID: reportGuid, reportName: reportName, "reportFormat": "PDF" };
    this.generateReport(reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.OpenWindow(URL.createObjectURL(data));
            this.setIsOverlay(false);
          }
        },
        error => {
          throw new error("Unable to generate application summary report!");
        }
      );
  }

  getDropDownText(id: number, object) {
    const selObj = _.filter(object, function (o: any) {

      if (o.uniqueID && Number(o.uniqueID) > 0 && Number(o.uniqueID) == id)
        return o;
      else
        return (_.includes(id, o.id));
    });
    return selObj;
  }

  getDropDownTextMultiSelect(id: [], object: any) {
    const selObj = _.filter(object, function (o: any) {
      return (_.includes(id, o.id));
    });
    return selObj;
  }

  //Date Comparator
  dateComparator(date1: any, date2: any) {
    let date1Number = monthToComparableNumber(date1);
    let date2Number = monthToComparableNumber(date2);
    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  //Validate Determination
  validateDetermination(caseAssignedTo?: number, caseStatusType?: number, optionUserId?: number) {
    if (caseAssignedTo == 0 || caseAssignedTo == null) {
      throw new Error("This case is not yet assigned to anyone for housing determination!");
    }
    // else if (caseAssignedTo && caseAssignedTo !== optionUserId) {
    //   throw new Error("This case is assigned to another reviewer!");
    // }
    else if (caseStatusType && caseStatusType === 875) {
      throw new Error("This case is signed-off and completed!");
    }
  }

  isBusinessHours8AMto8PM(): boolean {
    var sysDate = new Date();
    var year = sysDate.getFullYear();
    var month = sysDate.getMonth();
    var day = sysDate.getDate();
    var weekDay = sysDate.getDay();  // 0 = sunday, 6 = saturday

    var openHour = new Date(year, month, day, 8, 0, 0);     // Today 8:00AM
    var closeHour = new Date(year, month, day, 20, 0, 0);   // Today 8:00PM

    if (sysDate > openHour && sysDate < closeHour && weekDay > 0 && weekDay < 6) {
      return true;
    } else {
      return false;
    }
  }
}
