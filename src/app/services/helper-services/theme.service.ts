import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  selectedTheme = new BehaviorSubject<string>('indigo-pink');

  constructor() {}

  setSelectedTheme(selectedTheme: string) {
    this.selectedTheme.next(selectedTheme);
  }

  getSelectedTheme() {
    return this.selectedTheme.asObservable();
  }
}
