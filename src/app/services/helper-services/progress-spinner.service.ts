import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressSpinnerService {

  private parentProgressSpinner = new BehaviorSubject<boolean>(true);

  constructor() { }

  setParentProgressSpinner(val: boolean) {
    this.parentProgressSpinner.next(val);
  }

  getParentProgressSpinner() {
    return this.parentProgressSpinner.asObservable();
  }
}
