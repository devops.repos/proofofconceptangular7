import { Component, OnInit, OnDestroy } from '@angular/core';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GridOptions } from 'ag-grid-community';
import { IClientSearch } from '../admin-interface.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { CSActionComponent } from './cs-action.component';
import { PlacementsVerificationService } from '../../placements-awaiting-verification/placements-verification.service';
import { IVCSPlacementClientSelected } from '../../placements-awaiting-verification/placements-verification-interface.model';
import { AdminToolService } from '../../Administration/admin-tools/admin-tools.service';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';
import * as moment from 'moment';


@Component({
  selector: 'app-admin-client-search',
  templateUrl: './admin-client-search.component.html',
  styleUrls: ['./admin-client-search.component.scss'],
  providers: [DatePipe]
})
export class AdminClientSearchComponent implements OnInit, OnDestroy {

  genders: RefGroupDetails[];
  sourceSystems: RefGroupDetails[];
  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  clientAgeInYears: number;
  clientAgeInMonths: number;

  clientSearchGroup: FormGroup;

  csGridApi;
  csrColumnApi;
  csColumnDefs;
  defaultColDef;
  frameworkComponents;
  context;

  public csGridOptions: GridOptions;
  csRowData: IClientSearch[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private placementsVerificationService: PlacementsVerificationService,
    private datePipe: DatePipe,
    private router: Router,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private adminToolService:AdminToolService,
  ) {
    this.clientSearchGroup = this.formBuilder.group({
      firstNameCtrl: [''],
      lastNameCtrl: [''],
      ssnCtrl: ['',[Validators.pattern(SocialSecurityPattern.Pattern)]],
      cinCtrl: [''],
      dobCtrl: [''],
      genderCtrl: [0],
      sourceSystemCtrl: [0],
      hraIDCtrl: [''],
      referralDateFromCtrl: [''],
      referralDateToCtrl: [''],
    });
    this.csGridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.csColumnDefs = [
      {
        headerName: 'Source System',
        field: 'clientSourceDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'HRA ID',
        field: 'clientID',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Client Name (L,F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.lastName && params.data.firstName) {
            return params.data.lastName + ', ' + params.data.firstName;
          } else if (params.data.lastName) {
            return params.data.lastName;
          } else if (params.data.firstName) {
            return params.data.firstName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'SSN',
        field: 'ssn',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.ssn) {
            return 'XXX-XX-' + params.data.ssn.substr(5, 4);
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'DOB',
        field: 'dob',
        filter: 'agDateColumnFilter',
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Gender',
        field: 'gender',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'csrActionRenderer'
      },
    ];
    this.defaultColDef = {
      flex: 1,
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      csrActionRenderer: CSActionComponent
    };
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '4,71';
    this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        // console.log(data);
        this.genders = data.filter(d => d.refGroupID === 4);
        this.sourceSystems = data.filter(d => d.refGroupID === 71 && d.refGroupDetailID !== 575); //hiding 575= Master Leasing for this release
      },
      error => {
        throw new Error(error.message);
      }
    );
    this.adminToolService.getclientSearchParameter().subscribe((res: IClientSearch) => {
      if (res) {
        const data = res as IClientSearch;
        this.setclientSeacrhParameter(data);
        this.clientSearch(data);
      }
    });
  }

  onCSGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.csGridApi = params.api;
    this.csrColumnApi = params.columnApi;

    // const allColumnIds = [];
    // this.csrColumnApi.getAllColumns().forEach(column => {
    //   if (column.colId !== 'action') {
    //     allColumnIds.push(column.colId);
    //   }
    // });
    // this.csrColumnApi.autoSizeColumns(allColumnIds);
    this.csGridApi.sizeColumnsToFit();
    // params.api.expandAll();

  }

  onDeletePlacementClickParent(csSelected: IClientSearch) {
    // console.log(csrSelected);
    const input: IVCSPlacementClientSelected = {
      clientID: csSelected.clientID,
      clientSourceType: csSelected.clientSourceType,
      isPendingVerificationRequired: false
    }
    this.placementsVerificationService.setIsDeletePlacement(true);
    this.placementsVerificationService.setPlacementClientSelected(input);
    this.router.navigate(['/vcs/client-placements-history']);

  }

  onClearClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to clear the Form?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        /* Clear the Form */
        this.clientSearchGroup.reset();
        this.clientSearchGroup.get('genderCtrl').setValue(0);
        this.clientSearchGroup.get('sourceSystemCtrl').setValue(0);
        this.csRowData = [];
      },
      negativeResponse => { }
    );
  }

  onSearchClick() {
    if (
      this.clientSearchGroup.get('firstNameCtrl').invalid ||
      this.clientSearchGroup.get('lastNameCtrl').invalid ||
      (this.clientSearchGroup.get('ssnCtrl').touched && this.clientSearchGroup.get('ssnCtrl').invalid && this.clientSearchGroup.get('ssnCtrl').value !== '') ||
      this.clientSearchGroup.get('cinCtrl').invalid ||
      (this.clientSearchGroup.get('dobCtrl').touched && this.clientSearchGroup.get('dobCtrl').invalid && this.clientSearchGroup.get('dobCtrl').value !== '') ||
      this.clientSearchGroup.get('genderCtrl').invalid ||
      this.clientSearchGroup.get('sourceSystemCtrl').invalid ||
      this.clientSearchGroup.get('hraIDCtrl').invalid ||
      (this.clientSearchGroup.get('referralDateFromCtrl').touched && this.clientSearchGroup.get('referralDateFromCtrl').invalid && this.clientSearchGroup.get('referralDateFromCtrl').value !== '') ||
      (this.clientSearchGroup.get('referralDateToCtrl').touched && this.clientSearchGroup.get('referralDateToCtrl').invalid && this.clientSearchGroup.get('referralDateToCtrl').value !== '')
    ) {
      /* Form not valid, return to form */
      return;
    }
    if (this.clientSearchGroup.get('dobCtrl').touched && this.clientSearchGroup.get('dobCtrl').valid) {
      // Validate Age - Age should be between 17years 8months to 119years
      if (!this.validateAge()) {
        return;
      }
    }

    this.csRowData = [];
    const firstName = this.clientSearchGroup.get('firstNameCtrl').value || null;
    const lastName = this.clientSearchGroup.get('lastNameCtrl').value || null;
    const ssn = this.clientSearchGroup.get('ssnCtrl').value || null;
    const cin = this.clientSearchGroup.get('cinCtrl').value || null;
    const dob = this.clientSearchGroup.get('dobCtrl').value || null;
    const gender = this.clientSearchGroup.get('genderCtrl').value > 0 ? this.clientSearchGroup.get('genderCtrl').value : null;
    const sourceSystem = this.clientSearchGroup.get('sourceSystemCtrl').value > 0 ? this.clientSearchGroup.get('sourceSystemCtrl').value : null;
    const hraID = this.clientSearchGroup.get('hraIDCtrl').value || null;
    const referralDateFrom = this.clientSearchGroup.get('referralDateFromCtrl').value || null;
    const referralDateTo = this.clientSearchGroup.get('referralDateToCtrl').value || null;

    let length = 0;

    if (firstName) { length++; }
    if (lastName) { length++; }
    if (ssn) { length++; }
    if (cin) { length++; }
    if (dob) { length++; }
    if (gender > 0) { length++; }
    if (sourceSystem > 0) { length++; }
    if (hraID) { length++; }
    if (referralDateFrom) { length++; }
    if (referralDateTo) { length++; }

    if (length <= 0) {
      const title = 'Verify';
      const primaryMessage = `Please enter at least one search criteria`;
      const secondaryMessage = ``;
      const confirmButtonName = 'OK';
      const dismissButtonName = '';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => { },
        negativeResponse => { }
      );
    } else {
      const clientSearchInput: IClientSearch = {
        clientID: hraID,
        clientSourceType: sourceSystem,
        firstName: firstName,
        lastName: lastName,
        ssn: ssn,
        dob: dob,
        genderType: gender,
        cin: cin,
        referralDateFrom: referralDateFrom,
        referralDateTo: referralDateTo
      }
      // console.log('clientSearchInput: ', clientSearchInput);
      this.clientSearch(clientSearchInput);
    }
  }

  clientSearch(clientSearchInput: IClientSearch) {
    this.placementsVerificationService.getPlacementMatchedClient(clientSearchInput).subscribe((res: IClientSearch[]) => {
      if (res.length > 0) {
        this.csRowData = res;
        this.adminToolService.setclientSearchParameter(clientSearchInput,true);
        // console.log(this.csRowData);
      } else {
        this.toastr.warning('No results found');
      }
    });
  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.adminToolService.setclientSearchParameter(null,false);
        this.router.navigate(['/dashboard']);
      },
      negativeResponse => { }
    );
  }

  //Calculate Age Of The Client In Years and Months
  calculateAge() {
    if (this.clientSearchGroup.get('dobCtrl').value && !moment(this.clientSearchGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Invalid Date of Birth.");
      }
      this.clientSearchGroup.controls['dobCtrl'].setValue("");
      return;
    }
    if (this.clientSearchGroup.get('dobCtrl').value) {
      const starts = moment(this.clientSearchGroup.get('dobCtrl').value, 'MM/DD/YYYY');
      const ends = moment(Date.now());
      const years = ends.diff(starts, 'year');
      starts.add(years, 'years');
      const months = ends.diff(starts, 'months');
      starts.add(months, 'months');
      this.clientAgeInYears = years;
      this.clientAgeInMonths = months;
    }
  }


  //Validate Client Age
  validateAge() {
    if (this.clientAgeInYears < 17) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
      }
      return false;
    }
    if (this.clientAgeInYears === 17 && this.clientAgeInMonths < 8) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
      }
      return false;
    }
    if (this.clientAgeInYears > 119) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Client is over 119 years old. Please check the DOB or Age.");
      }
      return false;
    }
    return true;
  }


  refreshAgGrid() {
    this.csGridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'ClientSearchResultReport-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.csGridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
  }

  onUpdateDeterminationClickParent(csSelected: IClientSearch) {

    this.adminToolService.setIsExtendDetermination(true);
    this.adminToolService.setSHClientSelected(csSelected);
      this.router.navigate(['/admin/extendDetermination']);



  }
  onLiftSignOffClickParent(csSelected: IClientSearch) {
   this.adminToolService.setIsLiftSignOff(true);
    this.adminToolService.setSHClientSelected(csSelected);
    this.router.navigate(['/admin/liftSignoff']);

  }
  onMoveReferralClickParent(csSelected: IClientSearch) {
    // console.log(csrSelected);

    this.adminToolService.setIsMoveReferral(true);
    this.adminToolService.setSHClientSelected(csSelected);
    this.router.navigate(['/admin/moveReferral']);

  }
  onDeleteReferralClickParent(csSelected: IClientSearch) {
    // console.log(csrSelected);
    this.adminToolService.setIsDeleteReferral(true);
    this.adminToolService.setSHClientSelected(csSelected);
    this.router.navigate(['/admin/deleteReferral']);

  }

  onReferralHoldClickParent(csSelected: IClientSearch) {
    this.adminToolService.setSHClientSelected(csSelected);
    this.router.navigate(['/admin/referral-hold']);
  }

  onUpdateClientClickParent(csSelected: IClientSearch) {
    // console.log(csrSelected);
    this.adminToolService.setIsUpdateClient(true);
    this.adminToolService.setSHClientSelected(csSelected);
    this.router.navigate(['/admin/updateClient']);

  }

  onClientCaseFolderClickParent(csSelected: IClientSearch) {
    // console.log(csrSelected);
    this.adminToolService.setIsClientCaseFolder(true);
    this.adminToolService.setSHClientSelected(csSelected);

    
    this.adminToolService.getPACTApplicationByClientId(csSelected.clientID.toString()).subscribe( res=>  {
    
      if (res.body) {
        const data = res.body as number;
        //alert(data);
        if (data === 0) {
          this.toastr.warning('No application found for this client.');
         }
        else {
            this.router.navigate(['/admin/client-case-folder',data]);
          }
      }
      else
      {
        this.toastr.warning('No application found for this client.');
      }
    },
      error => console.error('Error in retrieving the application id ...!', error)
      );
      
  }


  
  setclientSeacrhParameter(sp: IClientSearch )
  {
    this.clientSearchGroup.get('firstNameCtrl').setValue(sp.firstName);
    const lastName = this.clientSearchGroup.get('lastNameCtrl').setValue(sp.lastName);
    const ssn = this.clientSearchGroup.get('ssnCtrl').setValue(sp.ssn);
    const cin = this.clientSearchGroup.get('cinCtrl').setValue(sp.cin);
    const dob = this.clientSearchGroup.get('dobCtrl').setValue(sp.dob);
    const gender = this.clientSearchGroup.get('genderCtrl').setValue(sp.genderType);
    const sourceSystem = this.clientSearchGroup.get('sourceSystemCtrl').setValue(sp.clientSourceType);
    const hraID = this.clientSearchGroup.get('hraIDCtrl').setValue(sp.clientID);
    const referralDateFrom = this.clientSearchGroup.get('referralDateFromCtrl').setValue(sp.referralDateFrom);
    const referralDateTo = this.clientSearchGroup.get('referralDateToCtrl').setValue(sp.referralDateTo);

  }

}
