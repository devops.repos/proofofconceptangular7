import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IClientSearch } from '../admin-interface.model';
import { INavigationItems, INavigationItemsOnly, INavigationItemsWithFunctionality } from 'src/app/core/sidenav-list/navigation.interface';
import { Subscription } from 'rxjs';
import { NavigationService } from 'src/app/core/sidenav-list/navigation.service';

@Component({
  selector: 'app-cs-action',
  template: `
    <mat-icon
      class="csr-icon"
      color="warn"
      [matMenuTriggerFor]="csAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #csAction="matMenu">

      <button mat-menu-item
      *ngIf="params.data.clientSourceType === 573 && isExtendDeterminationVisible"
      (click)="onUpdateDeterminationClick()" >
        <mat-icon color="primary">assignment</mat-icon>Extend Determination
      </button>
      <button mat-menu-item  *ngIf="params.data.clientSourceType === 573 && isLiftSignoffVisible"
      (click)="onLiftSignOffClick()">
        <mat-icon color="primary">assignment</mat-icon>Lift Signoff
      </button>
      <button mat-menu-item  *ngIf="params.data.clientSourceType === 573 && isMoveReferralVisible"
      (click)="onMoveReferralClick()">
        <mat-icon color="primary">assignment</mat-icon>Move Referral
      </button>
      <button mat-menu-item  *ngIf="params.data.clientSourceType === 573 && isDeleteReferralVisible"
      (click)="onDeleteReferralClick()" >
        <mat-icon color="primary">assignment</mat-icon>Delete Referral
      </button>
      <button mat-menu-item  *ngIf="params.data.clientSourceType === 573 && isUpdateClientVisible"
      (click)="onUpdateClientClick()" >
        <mat-icon color="primary">assignment</mat-icon>Update Client
      </button>
      <button mat-menu-item  *ngIf="params.data.clientSourceType === 573 && isClientCaseFolderVisible"
      (click)="onClientCaseFolderClick()" >
        <mat-icon color="primary">assignment</mat-icon>Client Case Folder
      </button>
      <button mat-menu-item (click)="onDeletePlacementClick()" *ngIf="isDeletePlacementVisible">
      <mat-icon color="warn">delete_forever</mat-icon>Delete Placement
    </button>
      <button mat-menu-item  *ngIf="params.data.clientSourceType === 573 && isReferralHoldVisible"
      (click)="onReferralHoldClick()" >
        <mat-icon color="primary">assignment</mat-icon>Referral Hold
      </button>

    </mat-menu>
  `,
  styles: [
    `
      .csr-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class CSActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  csSelected: IClientSearch;

  navigatonItems: INavigationItems;
  navigationItemsOnly: INavigationItemsOnly[] = [];  // only Navigation Items, no functionalities
  nvgItmsWithFunc: INavigationItemsWithFunctionality[] = []; // Normalize list, will have duplication Navigation items with differrent functionalities

  isDeletePlacementVisible = false;
  isExtendDeterminationVisible = false;
  isLiftSignoffVisible = false;
  isMoveReferralVisible = false;
  isDeleteReferralVisible = false;
  isReferralHoldVisible = false;
  isUpdateClientVisible =false;
  isClientCaseFolderVisible = false;

  private navigationItemsSub: Subscription;

  constructor(
    private navigationService: NavigationService
  ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    /* Get NavigationItems for Sidenav */
    this.navigationItemsSub = this.navigationService.getNavigationItems().subscribe((res: INavigationItems) => {
      if (res) {
        this.navigatonItems = res;
        this.navigationItemsOnly = res.navigationItemsOnly;
        this.nvgItmsWithFunc = res.navigationItemsWithFunctionality;

        (this.nvgItmsWithFunc.filter(d => d.functionID === 111).length > 0) ? this.isExtendDeterminationVisible = true : this.isExtendDeterminationVisible = false;
        (this.nvgItmsWithFunc.filter(d => d.functionID === 112).length > 0) ? this.isMoveReferralVisible = true : this.isMoveReferralVisible = false;
        (this.nvgItmsWithFunc.filter(d => d.functionID === 113).length > 0) ? this.isDeleteReferralVisible = true : this.isDeleteReferralVisible = false;
        (this.nvgItmsWithFunc.filter(d => d.functionID === 114).length > 0) ? this.isLiftSignoffVisible = true : this.isLiftSignoffVisible = false;
        (this.nvgItmsWithFunc.filter(d => d.functionID === 115).length > 0) ? this.isDeletePlacementVisible = true : this.isDeletePlacementVisible = false;
        (this.nvgItmsWithFunc.filter(d => d.functionID === 116).length > 0) ? this.isReferralHoldVisible = true : this.isReferralHoldVisible = false;
        (this.nvgItmsWithFunc.filter(d => d.functionID === 131).length > 0) ? this.isUpdateClientVisible = true : this.isUpdateClientVisible = false;
        (this.nvgItmsWithFunc.filter(d => d.functionID === 130).length > 0) ? this.isClientCaseFolderVisible = true : this.isClientCaseFolderVisible = false;


        // console.log('Functionalities: ', this.nvgItmsWithFunc);
      }
    });
  }

  onDeletePlacementClick() {
    this.csSelected = this.params.data;
    this.params.context.componentParent.onDeletePlacementClickParent(this.csSelected);
   }

   onUpdateDeterminationClick() {
    this.csSelected = this.params.data;
    this.params.context.componentParent.onUpdateDeterminationClickParent(this.csSelected);
   }
   onLiftSignOffClick() {
    this.csSelected = this.params.data;
    this.params.context.componentParent.onLiftSignOffClickParent(this.csSelected);
   }
   onMoveReferralClick() {
    this.csSelected = this.params.data;
    this.params.context.componentParent.onMoveReferralClickParent(this.csSelected);
   }

   onDeleteReferralClick() {
    this.csSelected = this.params.data;
    this.params.context.componentParent.onDeleteReferralClickParent(this.csSelected);
   }

   onReferralHoldClick(){
     this.csSelected = this.params.data;
    this.params.context.componentParent.onReferralHoldClickParent(this.csSelected);
    }

    onUpdateClientClick() {
      this.csSelected = this.params.data;
      this.params.context.componentParent.onUpdateClientClickParent(this.csSelected);
     }
     onClientCaseFolderClick() {
      this.csSelected = this.params.data;
      this.params.context.componentParent.onClientCaseFolderClickParent(this.csSelected);
     }


  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.navigationItemsSub) {
      this.navigationItemsSub.unsubscribe();
    }
  }
}
