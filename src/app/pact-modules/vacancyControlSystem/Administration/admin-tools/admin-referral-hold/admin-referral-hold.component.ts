import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { VCSReferralReady } from 'src/app/pact-modules/vacancyControlSystem/referral-package/referral-package.model';
import { ReferralService } from "src/app/pact-modules/vacancyControlSystem/referral-package/referral-package.service";
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { AdminToolService } from "../admin-tools.service";
import { ApplicationInfo } from "../admin-tools.model"
import { IClientSearch } from '../../admin-interface.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router, Event } from '@angular/router';
import { UpdateHoldActionComponent } from './update-hold-action.component';
import { ToastrService } from "ngx-toastr";
import { ReferralHoldComponent } from 'src/app/shared/referral-hold/referral-hold.component';

@Component({
  selector: 'app-admin-referral-hold',
  templateUrl: './admin-referral-hold.component.html',
  styleUrls: ['./admin-referral-hold.component.scss']
})
export class AdminReferralHoldComponent implements OnInit, OnDestroy {
  getClientSub: Subscription;
  getAppsSub: Subscription;
  getClientDataSub: Subscription;
  saveRefSub: Subscription;

  hraClientID: number;
  applicationID: number;

  clientDocData: VCSReferralReady = new VCSReferralReady();
  clientInfo: IClientSearch = {};
  rowData: ApplicationInfo[];
  isDisabled: boolean = true;

  public gridOptions: GridOptions;
  overlayNoRowsTemplate;
  overlayLoadingTemplate;
  gridApi;
  gridColumnApi;
  
  rowSelection;
  context;
  columnDefs;
  defaultColDef
  pagination;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;

  @ViewChild(ReferralHoldComponent) docChild: ReferralHoldComponent;
  @ViewChild('applicationListGrid') agGrid: AgGridAngular;
  
  constructor(private referralService: ReferralService,
              private adminService: AdminToolService,
              private router: Router,
              private toastr: ToastrService,) {

    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Application list is loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Applications Available</span>';
    this.gridOptions = {
      rowHeight: 35,
    } as GridOptions;

    this.context = { componentParent: this };
    this.rowSelection = 'single';
    this.columnDefs = [
      {
        headerName: 'Client Name',
        filter: 'agTextColumnFilter',
        valueGetter: function (params) {
          return params.data.lastName + ', ' + params.data.firstName;
        }
      },
      {
        headerName: 'HRA Client #',
        field: 'pactClientID',
        filter: 'agTextColumnFilter',        
      },
      {
        headerName: 'Referral Date',
        field: 'referralDate',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referring Agency/Site Name',
        filter: 'agTextColumnFilter',
        valueGetter: function (params) {
          return params.data.referralAgencyName+'/'+params.data.referralSiteName;
        }
      },
      {
        headerName: 'Client Referral Status',
        field: 'clientStatus',
        filter: 'agTextColumnFilter', 
      },
      {
        headerName: 'Last Updated Date',
        field: 'lastUpdatedDate',
        filter: 'agTextColumnFilter',       
      },
      {
        headerName: 'Comments',
        field: 'comments',
        filter: 'agTextColumnFilter',       
      },  
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        //cellRenderer: 'actionRenderer'    
        cellRendererSelector(params) {
          const actionButton = {
            component: 'actionRenderer'
          };
          if (params.data.capID != 0 || params.data.clientNotReadyReason == 509) { 
            return actionButton;
          } else {
            return null;
          }
        }
      }
    ];


    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    
    this.pagination = true;
    this.frameworkComponents = {
      actionRenderer: UpdateHoldActionComponent
    };
   }

  ngOnInit() {
    this.hraClientID = 100;

    this.getClientSub = this.adminService.getSHClientSelected().subscribe((res: IClientSearch) => {
      if (res) {
        this.clientInfo = res;
        this.hraClientID = this.clientInfo.clientID;
        this.getAppsSub = this.adminService.getApplicationsByClient(this.hraClientID).subscribe(res => {
          this.rowData = res as ApplicationInfo[];
        })
      } else {
        this.router.navigate(['/admin/admin-tools']);
      }
    });
  }


  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  
    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, true);
    this.gridApi.sizeColumnsToFit(allColumnIds);

    this.gridApi.setDomLayout('autoHeight');
  }

  contactViewParent(applicationSelected: ApplicationInfo){
    this.applicationID = applicationSelected.pactApplicationID;
    this.isDisabled = false;
    this.getRefReadyData(applicationSelected.pactApplicationID);
  }

  getRefReadyData(applicationID: number){
    this.getClientDataSub = this.referralService.getClientDocData(applicationID).subscribe(res => {
      if (res != null){
        this.clientDocData = res as VCSReferralReady;
      
        if (this.clientDocData.isClientRefReady == null)
        {
            this.clientDocData.isClientRefReady = 0;
        }
      }  
      // else {
      //   this.clientDocData.vcsClientPackageID = null;
      //   this.clientDocData.pactApplicationID = null;
      //   this.clientDocData.isPkgRefReady = null;
      //   this.clientDocData.pkgRefReadyComments = null;
      //   this.clientDocData.isClientRefReady = 0;
      //   this.clientDocData.clientRefReadyComment = null;
      //   this.clientDocData.clientNotReadyReason = null;
      //   this.clientDocData.clientNotReadyOtherSpecify = null;
      //   this.clientDocData.clientNotReadyAddlComments = null;
        
      // }
      
      //this.oldClientDocData = JSON.parse(JSON.stringify(this.clientDocData));
    });
  }


  onReferralHoldChange(referralChange: VCSReferralReady){
  
    this.clientDocData.isClientRefReady = referralChange.isClientRefReady;
    this.clientDocData.clientRefReadyComment = referralChange.clientRefReadyComment;
    this.clientDocData.clientNotReadyReason = referralChange.clientNotReadyReason;
    this.clientDocData.clientNotReadyOtherSpecify = referralChange.clientNotReadyOtherSpecify;
    this.clientDocData.clientNotReadyAddlComments = referralChange.clientNotReadyAddlComments;

    
  }

  onSave() {
    this.clientDocData.pactApplicationID = this.applicationID;
    if (this.clientDocData.isClientRefReady == 0) {
      this.clientDocData.isClientRefReady = null;
    }

    this.saveRefSub = this.referralService.saveReferral(this.clientDocData).subscribe(
      res => {      
          this.toastr.success("The information was saved successfully.", "");
          if (this.clientDocData.isClientRefReady == null)
          {
            this.clientDocData.isClientRefReady = 0;
          }
          //this.oldClientDocData = JSON.parse(JSON.stringify(this.clientDocData));
          
            this.docChild.getRefReadyHistory(res as number); 
            this.getAppsSub = this.adminService.getApplicationsByClient(this.hraClientID).subscribe(res => {
              this.rowData = res as ApplicationInfo[];
            })
      },
      error =>
        this.toastr.error("The information was not saved.", "Error while saving")
    );
  }
  onExit(){
    this.router.navigate(['/admin/admin-tools']);
  }
  
  ngOnDestroy() {    
    if (this.getClientSub) {
      this.getClientSub.unsubscribe();
    }
    if (this.getAppsSub) {
      this.getAppsSub.unsubscribe();
    }
    if (this.getClientDataSub) {
      this.getClientDataSub.unsubscribe();
    }
    if (this.saveRefSub) {
      this.saveRefSub.unsubscribe();
    }   
   
  }
  
}
