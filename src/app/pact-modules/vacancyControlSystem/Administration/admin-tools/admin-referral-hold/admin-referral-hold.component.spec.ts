import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminReferralHoldComponent } from './admin-referral-hold.component';

describe('AdminReferralHoldComponent', () => {
  let component: AdminReferralHoldComponent;
  let fixture: ComponentFixture<AdminReferralHoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminReferralHoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminReferralHoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
