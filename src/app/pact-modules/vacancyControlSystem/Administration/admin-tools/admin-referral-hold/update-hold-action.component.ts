import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ApplicationInfo } from "../admin-tools.model"

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "referral-action",
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="actionMenu"
    >
      more_vert
    </mat-icon>
    <mat-menu #actionMenu="matMenu">
      <button mat-menu-item  (click)="onView()">Update Refferal Hold</button>
    </mat-menu>
  `,
  styles: [
    `
    .pendingMenu-icon {
      cursor: pointer;
    }
    `
  ],
})
export class UpdateHoldActionComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;  
  private applicationSelected: ApplicationInfo;
  constructor() { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {

    this.applicationSelected = {
        pactClientID: this.params.data.pactClientID,
        pactApplicationID: this.params.data.pactApplicationID,
        firstName: this.params.data.firstName,
        lastName: this.params.data.lastName,
        referralDate: this.params.data.referralDate,
        referralAgencyName: this.params.data.referralAgencyName,
        referralSiteName: this.params.data.referralSiteName,
        clientStatus: this.params.data.clientStatus,
        lastUpdatedDate: this.params.data.lastUpdatedDate,
        comments: this.params.data.comments,
        approvalTo: this.params.data.approvalTo,
        capID: this.params.data.capID,
        clientNotReadyReason: this.params.data.clientNotReadyReason
    };

    this.params.context.componentParent.contactViewParent(this.applicationSelected);
  }

  refresh(): boolean {
    return false;
  }
}
