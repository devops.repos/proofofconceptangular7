import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendDeterminationComponent } from './extend-determination.component';

describe('ExtendDeterminationComponent', () => {
  let component: ExtendDeterminationComponent;
  let fixture: ComponentFixture<ExtendDeterminationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendDeterminationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendDeterminationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
