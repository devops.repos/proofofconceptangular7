
export interface determinationApprovalExtension
{
    seqNo?:number;
    detSeq:string;
    pactApplicationID?:number;
    approvalTo:string;
    extendedTo:string;
    extendedReason:string;
    extendedBy ?:number;
    extendedByUser:string;
    createdDate:string;
    isSuccess?:boolean
}

export interface removeSignOff
{
    pactApplicationID?:number;
    userId?:number;
}

export interface deleteReferral
{
    pactApplicationID?:number;
    userId?:number;
    deleteReason :string;
}

export interface moveReferral
{
    pactApplicationIDs:string;
    userId?:number;
    newPactClientID :number;
}

export interface NewClient {
    pactApplicationId?: number;
    firstName: string;
    lastName: string;
    ssn: string;
    dob: string;
    genderType?: number;
    cin: string;
    optionUserId?: number;
    pactClientId?: number;
}

export interface ISHClientSelected
{
    clientId?:number;
    firstName:string;
    lastname:string;
    dob:string;
    ssn:string;
    sourceSystem:number;
}


export interface determinationInfo {
    referralDate:string;
    pactApplicationID?:number;
    pactClientID?:number;
    referralAgencyNo:string;
    referralAgencyName:string
    referralSiteNo:string;
    referralSiteName:string;
    referringAgencySite:string;
    eligibility:string;
    approvalFrom:string;
    approvalTo:string;
    placementAgencyNo:string;
    placementAgencyName:string;
    placementSiteNo:string;
    placementSiteName:string;
    moveInDate:string;
    moveOutDate:string;
    moveOutReasonType ?:number;
    moveOutReasonDescription:string;
    prioritization:string;
    serviceNeed:string;
    housingType:string;
    signoff?:boolean;
    isIAH?:number;
    signoffByUser:string;
    signoffDate:string;
    select?:boolean;
   
}

export class ApplicationInfo {
    pactClientID:number;
    pactApplicationID:number;
    firstName:string;
    lastName:string;
    referralDate:string;
    referralAgencyName:string;
    referralSiteName:string;
    clientStatus:string;
    lastUpdatedDate:string;
    comments:string;
    approvalTo: Date;
    capID: number;
    clientNotReadyReason: number;
}


export class UpdateClientInfo
{
    hraclientID:number;
    vpsClientID:number;
    firstName:string;
    lastName: string;
    akaFirstName: string;
    akaLastName: string;
    ssn:string;
    dob: string;
    genderType?: number;
    ethnicityType?: number;
    interpreter: boolean;
    primaryLanguageType ?: number;
    scims: string;
    age?:number;
    noOfPactReferral ?:number;
    noOfVPSReferral ?:number;
    userId ?:number;
    sourceType?:number;

}