import { Component, OnInit, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import { determinationInfo, determinationApprovalExtension } from "./admin-tools.model"
import { HttpClient } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { ActivatedRoute, Router, Event } from '@angular/router';
import { AdminToolService } from './admin-tools.service';
import { IClientSearch } from '../admin-interface.model';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Alert } from 'selenium-webdriver';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';


@Component({
  selector: 'app-extend-determination',
  templateUrl: './extend-determination.component.html',
  styleUrls: ['./extend-determination.component.scss']
})
export class ExtendDeterminationComponent implements OnInit {
  gridApi;
  gridColumnApi;
  extendApprovalPeriodGridApi
  extendApprovalPeriodGridColumnApi
  columnDefs;
  defaultColDef;
  pagination;
  rowSelection;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;
  extendApprovalPeriodDef


  currentUserData: AuthData;
  userDataSub: Subscription;
  context;
  overlayLoadingTemplate: string = '';
  overlayNoRowsTemplate: string = '';
  overlayLoadinghistoryTemplate: string = '';
  overlayNoRowshistoryTemplate: string = '';
  extendDeterminatonForm: FormGroup;
  public determinationinfogridOptions: GridOptions;
  public extendApprovalPeriodgridOptions: GridOptions;
  determinationInfoData: determinationInfo[] = [];
  extendApprovalPeriodData: determinationApprovalExtension[] = [];
  extendApprovalPeriod: determinationApprovalExtension ;
  activatedRouteSub: Subscription;
  clientInfo: IClientSearch = {};
  ClientSelectedSub: Subscription;
  enableUpdate: boolean = false;
  approvalmaxdate = new Date();
  approvalmindate = new Date();
  pactApplicationID :number=0;
  currentApprovalTo :string;
  //clientID:number =240544;
  @Input() clientID: number = 240544;
  formErrors = {
    approvalEndDateCtrl: '',
    extendReasonCtrl: '',

  };



  constructor(private http: HttpClient,
    private sidenavService: NavService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private adminToolService: AdminToolService,
    private userService: UserService,
    private message: ToastrService,
    private confirmDialogService: ConfirmDialogService,
  ) {
    this.determinationinfogridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;

    this.extendApprovalPeriodgridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;

    //this.gridOptions.api.hideOverlay();

    this.columnDefs = [
      {
        headerName: 'Referral Date',
        filter: 'agTextColumnFilter',
        field: "referralDate",
        width: 100,

      },
      {
        headerName: 'Referring Agency/Site',
        field: 'referringAgencySite',
        width: 200,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Eligibility',
        field: 'eligibility',
        width: 250,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'prioritization',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeed',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Type',
        field: 'housingType',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Approval Period',
        field: 'approvalFrom',
        width: 200,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.approvalFrom + ' - ' + params.data.approvalTo;
        }
      },
      {
        headerName: 'Placement Agency/Site',
        field: 'PlacementAgencySite',
        width: 200,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.placementAgencyNo + ' - ' + params.data.placementAgencyName + ' / ' + params.data.placementSiteNo + ' - ' + params.data.placementSiteName;
        }

      },
      {
        headerName: 'Move In',
        field: 'moveInDate',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Move Out',
        field: 'moveOutDate',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Reason Moved',
        field: 'moveOutReasonDescription',
        width: 200,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Select',
        field: 'select',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        checkboxSelection: true
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: false
    };



    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.pagination = true;
    this.frameworkComponents = {
      //actionRenderer: AgencySiteMaintenanceActionComponent
    };
    this.extendApprovalPeriodDef = [
      {
        headerName: 'Determination Update',
        filter: 'agTextColumnFilter',
        field: "detSeq",
        width: 150,

      },
      {
        headerName: 'Approval From',
        field: 'approvalTo',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Approval To',
        field: 'extendedTo',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Reviewer Name',
        field: 'extendedByUser',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Comments',
        field: 'extendedReason',
        width: 350,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Updated Date',
        field: 'createdDate',
        width: 150,
        filter: 'agTextColumnFilter'
      }
    ];


  }

  ngOnInit() {
    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUserData = userdata;
       
      }
    });
    // alert('i am here');
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait Application deterimnation are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Application deterimnation are available</span>';

    this.overlayLoadinghistoryTemplate = '<span class="ag-overlay-loading-center">Please wait Application deterimnation are loading.</span>';
    this.overlayNoRowshistoryTemplate = '<span style="color:#337ab7;">Not currently extended</span>';

    this.ClientSelectedSub = this.adminToolService.getSHClientSelected().subscribe((res: IClientSearch) => {
      if (res) {
        this.clientInfo = res;
        this.clientID = this.clientInfo.clientID;
        // console.log('placementClientSelected: ', this.placementClientSelected);
      } else {
        this.router.navigate(['/admin']);
      }
    });
    this.extendDeterminatonForm = this.formBuilder.group({
      extendReasonCtrl: ['', Validators.compose([Validators.required])],
      approvalEndDateCtrl: ['', Validators.required]
    });
    this.determinationInfogridRefresh();
  }
  ngOnChanges() {
    this.determinationInfogridRefresh();

  }

  refreshDeterminationInfoAgGrid() {
    this.determinationinfogridOptions.api.setFilterModel(null);
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {

      if (column.colId !== 'select') {

        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);

      }

    });
  //  this.gridColumnApi.autoSizeColumns(allColumnIds);

  }
  extendApprovalPeriodGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.extendApprovalPeriodGridApi = params.api;
    this.extendApprovalPeriodGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.extendApprovalPeriodGridColumnApi.getAllColumns().forEach(column => {

      if (column.colId !== 'select') {

        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);

      }

    });


  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onDeterminationInfogridRowSelected(event) {
    
    if (event.node.selected) {
      this.clearPage();
      this.extendApprovalPeriodgridRefresh(event.node.data.pactApplicationID);
      this.enableUpdate = true;
      this.pactApplicationID = event.node.data.pactApplicationID;
      console.log(event.node.data);
      if (event.node.data.approvalFrom !== null) {

        let momentVariable = moment(event.node.data.approvalFrom,'MM-DD-YYYY');
        let momentVariable1 = moment(event.node.data.approvalFrom,'MM-DD-YYYY');
        this.approvalmindate = new Date(momentVariable.add(180,"days").format('MM-DD-YYYY'));
        //this.approvalmindate.setDate(momentVariable.add(180,"days")) ;
        if (event.node.data.isIAH === 0) {
       // this.approvalmaxdate.setDate(momentVariable.getDate() + 548);
          this.approvalmaxdate = new Date(momentVariable1.add(548,"days").format('MM-DD-YYYY'));
        }
        else {
          this.approvalmaxdate = new Date(momentVariable1.add(1825,"days").format('MM-DD-YYYY'));
         // this.approvalmaxdate.setDate(momentVariable.getDate() + (5 * 365));
        }
       // alert(this.approvalmindate);
        //alert(this.approvalmaxdate);
     
      }
      if (event.node.data.approvalTo !== null) {
       this.currentApprovalTo  = event.node.data.approvalTo;
       
      }

    }
    if (this.gridApi.getSelectedNodes().length === 0 )
       this.enableUpdate = false;
     else
     {
       this.enableUpdate = true;
      }

     // alert(this.approvalmaxdate);
/*    else
    {
      this.enableUpdate = false;
    }*/
    //alert (this.enableUpdate);
  }



  onRowSelected(event) {
    if (event.node.selected) {

    }
  }
  determinationInfogridRefresh() {
    if (this.clientID) {
      this.adminToolService.getDeterminationByClient(this.clientID.toString(),"0")
        .subscribe(
          res => {
            this.determinationInfoData = res.body as determinationInfo[];
          },
          error => console.error('Error!', error)
        );
    }
  }
  extendApprovalPeriodgridRefresh(PactapplicationId: number) {
    if (PactapplicationId) {
      this.adminToolService.GetDeterminationApprovalExtension(PactapplicationId.toString())
        .subscribe(
          res => {
            this.extendApprovalPeriodData = res.body as determinationApprovalExtension[];
          },
          error => console.error('Error!', error)
        );
    }
  }

  ngOnDestroy() {
    if (this.ClientSelectedSub) {
      this.ClientSelectedSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }

  }
  exitPage() {
    this.router.navigate(['admin/admin-tools']);
  }

  validateApprovalEndDate() {
    if (this.extendDeterminatonForm.get('approvalEndDateCtrl').value && !moment(this.extendDeterminatonForm.get('approvalEndDateCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid Approval End Date");
      }
      this.extendDeterminatonForm.controls['approvalEndDateCtrl'].setValue("");
      return;
    }
  }

  resetformerror() {
    this.formErrors = {
      approvalEndDateCtrl: '',
      extendReasonCtrl: '',
    };
  }

  ValidateExtendDetermination = (): boolean => {

    //this.clearValidators();
    var messages: any;
    var key: any;
    this.resetformerror();
    this.extendApprovalPeriod = {
      seqNo: null,
      detSeq:null,
      pactApplicationID:this.pactApplicationID,
      approvalTo:this.currentApprovalTo,
      extendedTo: this.extendDeterminatonForm.get('approvalEndDateCtrl').value,
      extendedReason:this.extendDeterminatonForm.get('extendReasonCtrl').value,
      extendedBy : this.currentUserData.optionUserId,
      extendedByUser:null,
      createdDate:null,
      isSuccess:null
     
    }
   // alert(momentVariable);
    if (this.extendDeterminatonForm.get('approvalEndDateCtrl').value && !moment(this.extendDeterminatonForm.get('approvalEndDateCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      key = "approvalEndDateCtrl";
      this.formErrors[key] += "Invalid Approval End Date" + '\n';

    }
    else {
      key = "approvalEndDateCtrl";
      var momentVariable = new Date(this.extendDeterminatonForm.get('approvalEndDateCtrl').value);
      if (momentVariable > this.approvalmaxdate || momentVariable < this.approvalmindate ) {
        this.formErrors[key] += "Approval period must be a minimum of 6 months and a maximum of 18 months." + '\n';
      }

    }

    if (this.extendDeterminatonForm.get('extendReasonCtrl').value.trim() === '') {
      key = "extendReasonCtrl";
      this.formErrors[key] += "please enter the extension reason" + '\n';
    }

    var errormessage = '';
    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '') {
      this.message.error(errormessage, "Validation Error");
      return false;
    }
    else {
      return true;
    }
  }

  OnSave(event: Event) {
    if (this.ValidateExtendDetermination())
    {
      const title = 'Confirm Update';
    const primaryMessage = '';
    const secondaryMessage = "Are you sure to Update the Determination Period for " + this.pactApplicationID + " ?";
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => 
        {
          this.adminToolService.saveDeterminationApprovalPeriod(this.extendApprovalPeriod)
          .subscribe(
            data => {
              this.message.success("Approval Extension successfully done", 'Save Success');
              this.determinationInfogridRefresh();
              this.extendApprovalPeriodgridRefresh(this.pactApplicationID);
              this.clearPage();
              this.enableUpdate =false;
            },
            error => { this.message.error("Save Failed", 'Save Failed'); }
          );
        },
        (negativeResponse) => console.log(),
      );

  }
  }     

  onSubmit() {
   
  }
  clearPage()
  {
    
    this.extendDeterminatonForm.get('approvalEndDateCtrl').setValue('');
    this.extendDeterminatonForm.get('extendReasonCtrl').setValue('');
  }


}


