

import { Component, OnInit, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import { determinationInfo, moveReferral,NewClient } from "./admin-tools.model"
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { ActivatedRoute, Router, Event } from '@angular/router';
import { AdminToolService } from './admin-tools.service';
import { IClientSearch } from '../admin-interface.model';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Alert } from 'selenium-webdriver';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { RefGroupDetails } from '../../../../models/refGroupDetailsDropDown.model';
import { CommonService } from '../../../../services/helper-services/common.service';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';
import { MatchSourcesService } from '../../../../pact-modules/determination/match-sources/match-sources.service';
import { iMatchedClients, ClientSearch  } from '../../../../pact-modules/determination/match-sources/match-sources.model';
import { environment } from '../../../../../environments/environment';
import { DatePipe } from '@angular/common';


export interface iVCSClientData {
  vcsClientID: number;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
};



@Component({
  selector: 'app-move-referral',
  templateUrl: './move-referral.component.html',
  styleUrls: ['./move-referral.component.scss'],
  providers: [DatePipe]
})

export class MoveReferralComponent implements OnInit {

  getVCSClientURL = environment.pactApiUrl + 'DETClientCaseFolder/GetVCSClient';
  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  pagination;
  rowSelection;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;


  currentUserData: AuthData;
  userDataSub: Subscription;
  context;
  overlayLoadingTemplate: string = '';
  overlayNoRowsTemplate: string = '';
  overlayLoadinghistoryTemplate: string = '';
  overlayNoRowshistoryTemplate: string = '';
  moveReferralForm: FormGroup;
  public determinationinfogridOptions: GridOptions;
  determinationInfoData: determinationInfo[] = [];
  moveReferralModel: moveReferral;
  activatedRouteSub: Subscription;
  clientInfo: IClientSearch = {};
  ClientSelectedSub: Subscription;
  enableUpdate: boolean = false;
  pactApplicationID: number = 0;
  genders: RefGroupDetails[];
  isSaveEnable: boolean = false;
  housingProgramClientRowData: iMatchedClients[];
  selectedApplicationIDS:string ='';
  clientAgeInYears :number =0;
  clientAgeInMonths:number=0;

  //clientID:number =240544;
  @Input() clientID: number = 240544;

  clientSearchData: ClientSearch = {
    pactApplicationId: null,
    sourceType: null,
    clientId: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    cin: null,
    referralDate: null,
    optionUserId: null
  };

  newClient: NewClient = {
    pactApplicationId: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    cin: null,
    optionUserId: null,
    pactClientId: null
  }

  formErrors = {
    toClientNoCtrl: '',
    lastNameCtrl: '',
    firstNameCtrl: '',
    dobCtrl: '',
    genderCtrl: '',
    ssnCtrl: ''

  };
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    response: 'json',
  };
  vcsClientData: iVCSClientData = {
    vcsClientID: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null
  }



  constructor(private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private adminToolService: AdminToolService,
    private userService: UserService,
    private message: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private commonService: CommonService,
    private httpClient: HttpClient,
    private matchSourcesService: MatchSourcesService,
    private datePipe: DatePipe
  ) {
    this.determinationinfogridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;


    //this.gridOptions.api.hideOverlay();

    this.columnDefs = [
      {
        headerName: 'Referral Date',
        filter: 'agTextColumnFilter',
        field: "referralDate",
        width: 100,

      },
      {
        headerName: 'Referring Agency/Site',
        field: 'referringAgencySite',
        width: 200,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Eligibility',
        field: 'eligibility',
        width: 250,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'prioritization',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeed',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Type',
        field: 'housingType',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Approval Period',
        field: 'approvalFrom',
        width: 200,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.approvalFrom + ' - ' + params.data.approvalTo;
        }
      },
      {
        headerName: 'Placement Agency/Site',
        field: 'PlacementAgencySite',
        width: 200,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.placementAgencyNo + ' - ' + params.data.placementAgencyName + ' / ' + params.data.placementSiteNo + ' - ' + params.data.placementSiteName;
        }

      },
      {
        headerName: 'Move In',
        field: 'moveInDate',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Move Out',
        field: 'moveOutDate',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Reason Moved',
        field: 'moveOutReasonDescription',
        width: 200,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Select',
        field: 'select',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        checkboxSelection: true
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: false
    };



    this.rowSelection = 'multiple';
    this.context = { componentParent: this };
    this.pagination = true;
    this.frameworkComponents = {
      //actionRenderer: AgencySiteMaintenanceActionComponent
    };
  }

  ngOnInit() {
    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUserData = userdata;

      }
    });
    // alert('i am here');
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait Application deterimnation are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Application deterimnation are available</span>';

    this.overlayLoadinghistoryTemplate = '<span class="ag-overlay-loading-center">Please wait Application deterimnation are loading.</span>';
    this.overlayNoRowshistoryTemplate = '<span style="color:#337ab7;">Not currently extended</span>';

    this.ClientSelectedSub = this.adminToolService.getSHClientSelected().subscribe((res: IClientSearch) => {
      if (res) {
        this.clientInfo = res;
        this.clientID = this.clientInfo.clientID;
        // console.log('placementClientSelected: ', this.placementClientSelected);
      } else {
        this.router.navigate(['/admin']);
      }
    });
    var refGroupList = "4";
    this.commonService.getRefGroupDetails(refGroupList)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.genders = data.filter(d => { return d.refGroupID === 4 });

        },
        error => {
          throw new Error(error.message);
        }
      );

    this.moveReferralForm = this.formBuilder.group({
      lastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      firstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      dobCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required],
      ssnCtrl: ['', [Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      toClientNoCtrl: ['']

    });
    this.determinationInfogridRefresh();
  }
  ngOnChanges() {
    this.determinationInfogridRefresh();

  }

  refreshDeterminationInfoAgGrid() {
    this.determinationinfogridOptions.api.setFilterModel(null);
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {

      if (column.colId !== 'select') {

        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);

      }

    });
    //  this.gridColumnApi.autoSizeColumns(allColumnIds);



  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
    if (this.determinationInfoData.length === 1)
      {
        this.gridApi.forEachNode( (node) => {
          this.gridApi.selectNode(node, true);
        /*  alert(node.selected);*/
        });
      // this.gridApi.selectAll();
      }
  }

  onDeterminationInfogridRowSelected(event) {

    /*if (event.node.selected) {
      this.enableUpdate = true;
      this.pactApplicationID = event.node.data.pactApplicationID;
    //  alert(this.pactApplicationID);
      this.pactApplicationID = event.node.data.pactApplicationID;
    }*/

    if (this.gridApi.getSelectedNodes().length === 0)
      this.enableUpdate = false;
    else {
      this.enableUpdate = true;
    }
  }

  onRowSelected(event) {
    if (event.node.data.selected) {

    }
  }
  determinationInfogridRefresh() {
    if (this.clientID) {
      this.adminToolService.getReferralByClient(this.clientID.toString())
        .subscribe(
          res => {
            this.determinationInfoData = res.body as determinationInfo[];
          },
          error => console.error('Error!', error)
        );
    }
  }

  ngOnDestroy() {
    if (this.ClientSelectedSub) {
      this.ClientSelectedSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }

  }
  exitPage() {
    this.router.navigate(['admin/admin-tools']);
  }


  resetformerror() {
    this.formErrors = {
      toClientNoCtrl: '',
      lastNameCtrl: '',
      firstNameCtrl: '',
      dobCtrl: '',
      genderCtrl: '',
      ssnCtrl: ''

    };
  }

  ValidateMoveReferral = (): boolean => {

    //this.clearValidators();
    var messages: any;
    var key: any;
    this.resetformerror();
    this.moveReferralModel = {
      pactApplicationIDs: this.selectedApplicationIDS,
      userId: this.currentUserData.optionUserId,
      newPactClientID: this.moveReferralForm.get('toClientNoCtrl').value

    }

    // alert(momentVariable);
    //alert(this.moveReferralForm.get('toClientNoCtrl').value);

    if (this.moveReferralForm.get('toClientNoCtrl').value === '') {
      key = "toClientNoCtrl";
      this.formErrors[key] += "Please enter the client" + '\n';
    }

    var errormessage = '';
    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '') {
      this.message.error(errormessage, "Validation Error");
      return false;
    }
    else {
      return true;
    }
  }

  OnSave(event: Event) {
    this.getSelectedApplicationIDS();
   // alert(this.selectedApplicationIDS);
   
    if (this.selectedApplicationIDS !==  null  &&  this.selectedApplicationIDS  !== ''){
      if (this.ValidateMoveReferral()) {
      //  alert(this.moveReferralModel.newPactClientID.toString());
      // alert(this.pactApplicationID);
     //   if (this.pactApplicationID) {
          this.getVCSClient(this.moveReferralModel.newPactClientID)
            .subscribe(res => {
              if (res) {
                const data = res as iVCSClientData;
                if (data) {
                  this.vcsClientData = data;
                  this.SaveMovereferral();
                }
              }
            });
       // }
      }
    }
    else
    {
    this.message.error('No Application is selected', "Validation Error");
    }

  }
  SaveMovereferral() {
    const title = 'Confirm Update';
    const primaryMessage = '';
    const secondaryMessage = "Are you sure to Move referral for  " + this.selectedApplicationIDS + " ?";
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => {
          this.adminToolService.saveMoveReferral(this.moveReferralModel)
            .subscribe(
              data => {
                this.message.success("Move referral successfully done", 'Save Success');
                this.determinationInfogridRefresh();
                this.clearPage();
                this.enableUpdate =false;
              },
              error => { this.message.error("Save Failed", 'Save Failed'); }
            );
        },
        (negativeResponse) => console.log(),
      );


  }



  onSubmit() {

  }


  clearPage() {
    this.moveReferralForm.get('firstNameCtrl').setValue('');
    this.moveReferralForm.get('lastNameCtrl').setValue('');
    this.moveReferralForm.get('ssnCtrl').setValue('');
    this.moveReferralForm.get('dobCtrl').setValue('');
    this.moveReferralForm.get('genderCtrl').setValue('');
    this.moveReferralForm.get('toClientNoCtrl').setValue('');

  }





  CreateClient() {

    if (this.ValidateCreateClient())
    {
    this.clientSearchData.sourceType = 367;
    this.clientSearchData.clientId = null;
    this.clientSearchData.firstName = this.moveReferralForm.get('firstNameCtrl').value;
    this.clientSearchData.lastName = this.moveReferralForm.get('lastNameCtrl').value;
    this.clientSearchData.ssn = this.moveReferralForm.get('ssnCtrl').value;
    this.clientSearchData.dob = this.datePipe.transform(this.moveReferralForm.get('dobCtrl').value, 'MM/dd/yyyy');
    this.clientSearchData.genderType = this.moveReferralForm.get('genderCtrl').value;;
    this.clientSearchData.cin = null;
    //Api call to search client
    this.matchSourcesService.searchClient(this.clientSearchData)
      .subscribe(res => {
        if (res) {
          const data = res as iMatchedClients[];
          if (data && data.length > 0) {
            this.housingProgramClientRowData = data;
            this.message.success("Client already exits", 'info');
          }
          else {
            // save logic
            this.saveclient();
          }
        }
        else {
          this.saveclient();
          // save logic
          //this.noClientMatchDialog(this.clientSearchData);
        }
      });
    }
  }


  //Get VCS Client
  getVCSClient(pactClientId: number): Observable<any> {
    if (pactClientId) {
      return this.httpClient.post(this.getVCSClientURL, JSON.stringify(pactClientId), this.httpOptions);
    }
  }
  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  clientNoValidate() {
   // alert(this.moveReferralForm.get('toClientNoCtrl').value);
    if (this.moveReferralForm.get('toClientNoCtrl').value != "") {
      this.getVCSClient(this.moveReferralForm.get('toClientNoCtrl').value)
        .subscribe(res => {
          if (res) {
            const data = res as iVCSClientData;
            console.log(data);
            if (data) {
              this.vcsClientData = data;
              this.moveReferralForm.get('firstNameCtrl').disable();
              this.moveReferralForm.get('lastNameCtrl').disable();
              this.moveReferralForm.get('ssnCtrl').disable();
              this.moveReferralForm.get('dobCtrl').disable();
              this.moveReferralForm.get('genderCtrl').disable();
              this.moveReferralForm.get('firstNameCtrl').setValue(data.firstName);
              this.moveReferralForm.get('lastNameCtrl').setValue(data.lastName);
              this.moveReferralForm.get('ssnCtrl').setValue(data.ssn);
              this.moveReferralForm.get('dobCtrl').setValue(data.dob);
              this.moveReferralForm.get('genderCtrl').setValue(data.genderType);

              this.isSaveEnable = true;
            }
            else {
              this.moveReferralForm.get('firstNameCtrl').enable();
              this.moveReferralForm.get('lastNameCtrl').enable();
              this.moveReferralForm.get('ssnCtrl').enable();
              this.moveReferralForm.get('dobCtrl').enable();
              this.moveReferralForm.get('genderCtrl').enable();
              this.isSaveEnable = false;
              this.clearPage();
              this.message.info("Client is not found.","Move Referral");

            }

          }
          else {
            this.moveReferralForm.get('firstNameCtrl').enable();
            this.moveReferralForm.get('lastNameCtrl').enable();
            this.moveReferralForm.get('ssnCtrl').enable();
            this.moveReferralForm.get('dobCtrl').enable();
            this.moveReferralForm.get('genderCtrl').enable();
            this.isSaveEnable = false;
            this.clearPage();
            this.message.info("Client is not found.","Move Referral");
          }
        });
    }
  }

  saveclient(){
  this.newClient =
            {
              pactApplicationId: null,
              firstName: this.clientSearchData.firstName,
              lastName: this.clientSearchData.lastName,
              ssn: this.clientSearchData.ssn,
              dob: this.clientSearchData.dob,
              genderType: this.clientSearchData.genderType,
              cin: null,
              optionUserId: this.currentUserData.optionUserId,
              pactClientId: null

            }
            this.adminToolService.saveClient(this.newClient)
            .subscribe(
              data => {
                  const data1 = data as number;
                 // alert(data1);
                  this.moveReferralForm.get('toClientNoCtrl').setValue(data1);
                  this.message.success("Created the new client", 'Save Success');
                  this.clientNoValidate()
                
               // this.determinationInfogridRefresh();
              },
              error => { this.message.error("Save Failed", 'Save Failed'); }
            );

            
    }

    getSelectedApplicationIDS(){
     // alert(this.gridApi.getSelectedNodes().length);
      if (this.gridApi.getSelectedNodes().length > 0) {
        const selectedNodes = this.gridApi.getSelectedNodes();
        const selectedData = selectedNodes.map(node => node.data);
        this.selectedApplicationIDS = '';
        let tselectedApplicationIDS = null;

          if (selectedData) {
            selectedData.forEach(nodedata => {
              tselectedApplicationIDS = tselectedApplicationIDS ? (tselectedApplicationIDS + ' ,' + nodedata.pactApplicationID.toString()) : nodedata.pactApplicationID.toString();
            });
            this.selectedApplicationIDS=tselectedApplicationIDS;
          }
        }
      
      }
    
      validateAge() {
        if (this.clientAgeInYears < 17) {
          if (!this.message.currentlyActive) {
            this.message.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
          }
          return false;
        }
        if (this.clientAgeInYears === 17 && this.clientAgeInMonths < 8) {
          if (!this.message.currentlyActive) {
            this.message.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
          }
          return false;
        }
        if (this.clientAgeInYears > 119) {
          if (!this.message.currentlyActive) {
            this.message.error("Client is over 119 years old. Please check the DOB or Age.");
          }
          return false;
        }
        return true;
      }

      ValidateCreateClient = (): boolean => {

        //this.clearValidators();
        var messages: any;
        var key: any;
        this.resetformerror();
        
     
    
          if (this.moveReferralForm.get('lastNameCtrl').invalid) {
            key = "lastNameCtrl";
            this.formErrors[key] += "please enter First Name" + '\n';
          }
         
          if (this.moveReferralForm.get('firstNameCtrl').invalid) {
            key = "firstNameCtrl";
            this.formErrors[key] += "please enter First  Name" + '\n';
          }
    
          if (this.moveReferralForm.get('ssnCtrl').invalid) {
            key = "ssnCtrl";
            this.formErrors[key] += "please enter valid SSN" + '\n';
          }
          if (this.moveReferralForm.get('dobCtrl').invalid) {
            key = "dobCtrl";
            this.formErrors[key] += "please enter valid DOB" + '\n';
          }
          if (this.moveReferralForm.get('genderCtrl').invalid) {
            key = "genderCtrl";
            this.formErrors[key] += "please enter valid Gender" + '\n';
          }
      /*    if (!this.validateAge()) {
            return false;
          }*/
          this.calculateAge()
    
          if (this.clientAgeInYears < 17) {
            key = "dobCtrl";
            this.formErrors[key] += "Client must be at least 17 years and 8 months of age." + '\n';
           
          }
          if (this.clientAgeInYears === 17 && this.clientAgeInMonths < 8) {
            key = "dobCtrl";
            this.formErrors[key] += "Client must be at least 17 years and 8 months of age." + '\n';
          }
          if (this.clientAgeInYears > 119) {
            key = "dobCtrl";
            this.formErrors[key] += "Client is over 119 years old. Please check the DOB or Age." + '\n';
           }
           
          
         
    
        var errormessage = '';
        for (const errorkey in this.formErrors) {
          if (errorkey) {
            errormessage += this.formErrors[errorkey];
          }
        }
        if (errormessage != '') {
          this.message.error(errormessage, "Validation Error");
          return false;
        }
        else {
          return true;
        }
      }
    
      calculateAge() {
        if (this.moveReferralForm.get('dobCtrl').value && !moment(this.moveReferralForm.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
          if (!this.message.currentlyActive) {
            this.message.error("Invalid Date of Birth.");
          }
          this.moveReferralForm.controls['dobCtrl'].setValue("");
          return;
        }
        if (this.moveReferralForm.get('dobCtrl').value) {
          const starts = moment(this.moveReferralForm.get('dobCtrl').value, 'MM/DD/YYYY');
          const ends = moment(Date.now());
          const years = ends.diff(starts, 'year');
          starts.add(years, 'years');
          const months = ends.diff(starts, 'months');
          starts.add(months, 'months');
          this.clientAgeInYears = years;
          this.clientAgeInMonths = months;
         // this.moveReferralForm.controls['ageCtrl'].setValue(this.clientAgeInYears);
        }
      }
    
    
}


