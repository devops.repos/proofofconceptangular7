import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiftSignoffComponent } from './lift-signoff.component';

describe('LiftSignoffComponent', () => {
  let component: LiftSignoffComponent;
  let fixture: ComponentFixture<LiftSignoffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiftSignoffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiftSignoffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
