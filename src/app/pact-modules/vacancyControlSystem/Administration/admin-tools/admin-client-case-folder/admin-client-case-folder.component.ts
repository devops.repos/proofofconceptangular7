import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';

//Service
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model';

export interface ClientData {
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
  pactClientId: number;
  pactApplicationId: number;
  referralDate: string;
};

export interface iClientDocumentsData {
  agencyNumber: string;
  siteNumber: string;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
  pactClientId: number;
  approvalExpiryDate: string;
  pactApplicationId: number;
};

@Component({
  selector: 'app-admin-client-case-folder',
  templateUrl: './admin-client-case-folder.component.html',
  styleUrls: ['./admin-client-case-folder.component.scss']
})

export class AdminClientCaseFolderComponent implements OnInit, OnDestroy {
  //Global Varaibles
  tabSelectedIndex: number = 0;
  currentTabIndex: number = 0;
  capsClientId: number = 0;
  optionUserId: number = 0;
  pactClientId: number = 0;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  clientData: ClientData = {
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null,
    pactClientId: null,
    pactApplicationId: null,
    referralDate: null
  };

  clientDocumentsData: iClientDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null
  }

  //Constructor
  constructor(private route: ActivatedRoute,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService) {
  }

  //On Init
  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        if (this.userData) {
          this.optionUserId = this.userData.optionUserId;
        }
      }
    });

    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationDataForDeterminationWithApplicationId(numericApplicationId)
              .subscribe(res => {
                if (res) {
                  const data = res as iApplicationDeterminationData;
                  if (data) {
                    this.applicationDeterminationData = data;
                    if (this.applicationDeterminationData) {
                      this.applicationDeterminationService.setApplicationDeterminationData(this.applicationDeterminationData);
                      this.pactClientId = this.applicationDeterminationData.pactClientId;
                      //Client Data
                      this.clientData.firstName = this.applicationDeterminationData.clientFirstName;
                      this.clientData.lastName = this.applicationDeterminationData.clientLastName;
                      this.clientData.ssn = this.applicationDeterminationData.clientSSN;
                      this.clientData.dob = this.applicationDeterminationData.clientDOB;
                      this.clientData.genderType = this.applicationDeterminationData.clientGenderType;
                      this.clientData.genderTypeDescription = this.applicationDeterminationData.clientGenderDescription;
                      this.clientData.cin = this.applicationDeterminationData.clientCIN;
                      this.clientData.pactClientId = this.applicationDeterminationData.pactClientId;
                      this.clientData.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
                      this.clientData.referralDate = this.applicationDeterminationData.referralDate;
                      this.capsClientId = this.applicationDeterminationData.capsClientId;
                      //Housing Application Supporting Documents Data
                      this.clientDocumentsData.agencyNumber = this.applicationDeterminationData.agencyNumber;
                      this.clientDocumentsData.siteNumber = this.applicationDeterminationData.siteNumber;
                      this.clientDocumentsData.firstName = this.applicationDeterminationData.clientFirstName;
                      this.clientDocumentsData.lastName = this.applicationDeterminationData.clientLastName;
                      this.clientDocumentsData.ssn = this.applicationDeterminationData.clientSSN;
                      this.clientDocumentsData.dob = this.applicationDeterminationData.clientDOB;
                      this.clientDocumentsData.genderType = this.applicationDeterminationData.clientGenderType;
                      this.clientDocumentsData.genderTypeDescription = this.applicationDeterminationData.clientGenderDescription;
                      this.clientDocumentsData.cin = this.applicationDeterminationData.clientCIN;
                      this.clientDocumentsData.pactClientId = this.applicationDeterminationData.pactClientId;
                      this.clientDocumentsData.approvalExpiryDate = this.applicationDeterminationData.approvalTo;
                      this.clientDocumentsData.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
                    }
                  }
                }
              });
          }
          else {
            return;
          }
        }
      }
    });
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

  //Next Button Click
  nextTab() {
    this.tabSelectedIndex = this.tabSelectedIndex + 1;
  }

  //Previous Button Click
  previousTab() {
    this.tabSelectedIndex = this.tabSelectedIndex - 1;
  }
}