import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../../../../environments/environment';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { determinationApprovalExtension,ISHClientSelected,removeSignOff,deleteReferral,moveReferral,NewClient, UpdateClientInfo } from './admin-tools.model';
import { IClientSearch } from '../admin-interface.model';


@Injectable({
  providedIn: 'root'
})
export class AdminToolService {
  AdminToolUrl = environment.pactApiUrl + 'AdminTool';
  private SHClientSelected = new BehaviorSubject<IClientSearch>(null);
  private clientSearchParameter = new BehaviorSubject<IClientSearch>(null);
 
  private IsExtendDetermination = new BehaviorSubject<boolean>(false);
  private IsLiftSignOff = new BehaviorSubject<boolean>(false);
  private IsMoveReferral = new BehaviorSubject<boolean>(false);
  private IsDeleteReferral = new BehaviorSubject<boolean>(false);
  private IsUpdateClient = new BehaviorSubject<boolean>(false);
  private IsClientCaseFolder = new BehaviorSubject<boolean>(false);

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  getDeterminationByClient(value: string,t:string) {
    return this.httpClient.get(this.AdminToolUrl + "/GetDeterminationByClient", { params: { PACTClientID: value,type:t }, observe: 'response' });
  }
  getReferralByClient(value: string) {
    return this.httpClient.get(this.AdminToolUrl + "/GetReferralByClient", { params: { PACTClientID: value }, observe: 'response' });
  }
  GetDeterminationApprovalExtension(value: string) {
    return this.httpClient.get(this.AdminToolUrl + "/GetDeterminationApprovalExtension", { params: { PACTApplicationID: value }, observe: 'response' });
  }
  
  saveDeterminationApprovalPeriod(det: determinationApprovalExtension) {
    return this.httpClient.post(this.AdminToolUrl + "/SaveDeterminationApprovalPeriod", JSON.stringify(det), this.httpOptions);
  }
  saveRemoveSignOff(rs: removeSignOff) {
    return this.httpClient.post(this.AdminToolUrl + "/SaveRemoveSignOff", JSON.stringify(rs), this.httpOptions);
  }
  saveDeleteReferral(rs: deleteReferral) {
    return this.httpClient.post(this.AdminToolUrl + "/DeleteReferral", JSON.stringify(rs), this.httpOptions);
  }
  
  saveMoveReferral(rs: moveReferral) {
    return this.httpClient.post(this.AdminToolUrl + "/MoveReferral", JSON.stringify(rs), this.httpOptions);
  }

  saveClient(cl: NewClient) {
    return this.httpClient.post(this.AdminToolUrl + "/SaveClient", JSON.stringify(cl), this.httpOptions);
  }
 
 setIsExtendDetermination(flag: boolean) {
  this.IsExtendDetermination.next(flag);
}
getIsExtendDetermination() {
  return this.IsExtendDetermination.asObservable();
}
setIsLiftSignOff(flag: boolean) {
  this.IsLiftSignOff.next(flag);
}
getIsLiftSignOff() {
  return this.IsLiftSignOff.asObservable();
}
setIsMoveReferral(flag: boolean) {
  this.IsMoveReferral.next(flag);
}
getIsMoveReferral() {
  return this.IsMoveReferral.asObservable();
}
setIsDeleteReferral(flag: boolean) {
  this.IsDeleteReferral.next(flag);
}
getIsDeleteReferral() {
  return this.IsDeleteReferral.asObservable();
}

setIsUpdateClient(flag: boolean) {
  this.IsUpdateClient.next(flag);
}
getIsUpdateClient() {
  return this.IsUpdateClient.asObservable();
}


setIsClientCaseFolder(flag: boolean) {
  this.IsClientCaseFolder.next(flag);
}
getIsClientCaseFolder() {
  return this.IsClientCaseFolder.asObservable();
}



//Referral Hold
getApplicationsByClient(clientID: number){
  const getUrl = this.AdminToolUrl + '/GetApplicationsByClient/';
  return this.httpClient.get(getUrl + clientID);
 }



//#region Get and Set  Client Selected in the grid
setSHClientSelected(pav: IClientSearch) {
  this.SHClientSelected.next(pav);
}

getSHClientSelected() {
  return this.SHClientSelected.asObservable();
}

setclientSearchParameter(pav: IClientSearch,isInsert:boolean) {
  this.clientSearchParameter= new BehaviorSubject<IClientSearch>(null);
  if (isInsert === true)
    this.clientSearchParameter.next(pav);
}

getclientSearchParameter() {
  return this.clientSearchParameter.asObservable();
}

//Referral Hold
getUpdateClientInfo(clientId: string,sourcetype:string){
  return this.httpClient.get(this.AdminToolUrl + "/GetUpdateClient", { params: { ClientId: clientId,SourceType:sourcetype }, observe: 'response' });
  
 }
 saveUpdateClientInfo(upc: UpdateClientInfo) {
  return this.httpClient.post(this.AdminToolUrl + "/SaveUpdateClient", JSON.stringify(upc), this.httpOptions);
}

getPACTApplicationByClientId(value: string) {
  return this.httpClient.get(this.AdminToolUrl + "/GetPACTApplicationByClientId", { params: { clientId: value }, observe: 'response' });
}


}