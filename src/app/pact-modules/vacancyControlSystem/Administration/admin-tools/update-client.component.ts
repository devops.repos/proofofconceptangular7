import { Component, OnInit, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import {  UpdateClientInfo } from "./admin-tools.model"
import { HttpClient } from '@angular/common/http';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { ActivatedRoute, Router, Event } from '@angular/router';
import { AdminToolService } from './admin-tools.service';
import { IClientSearch } from '../admin-interface.model';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Alert } from 'selenium-webdriver';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-update-client',
  templateUrl: './update-client.component.html',
  styleUrls: ['./update-client.component.scss'],
  providers: [DatePipe]
})
export class UpdateClientComponent implements OnInit {
 
  currentUserData: AuthData;
  userDataSub: Subscription;
  UpdateClientGroup: FormGroup;
  activatedRouteSub: Subscription;
  clientInfo: IClientSearch = {};
  updateClientInfo:UpdateClientInfo;
  ClientSelectedSub: Subscription;
  commonServiceSub: Subscription;
  genders: RefGroupDetails[];
  languages: RefGroupDetails[];
  ethnicities: RefGroupDetails[];
  yesNoTypes:RefGroupDetails[];
  sourceType :number = 573;
  currentDate = this.datePipe.transform(new Date(), "MM/dd/yyyy hh:mm a");
  clientAgeInYears :number =0;
  clientAgeInMonths:number=0;

  //clientID:number =240544;
  @Input() clientID: number = 240544;
  formErrors = {
    hraClientIdCtrl:'',
    vpsClientIdCtrl: '',
    clientfirstNameCtrl: '',
    clientLastNameCtrl: '',
    akafirstNameCtrl: '',
    akaLastNameCtrl: '',
    ssnCtrl:'',
    dobCtrl: '',
    genderCtrl: '',
    ethenicityCtrl: '',
    interpreterCtrl: '',
    languageCtrl: '',
    scimsCtrl: '',
    ageCtrl:'',
    updatedDateCtrl:'',
    updatedByCtrl:'',
    pactReferralCtrl:'',
    vpsReferralCtrl:'',

    
  };

  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };


  constructor(private http: HttpClient,
    private sidenavService: NavService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private adminToolService: AdminToolService,
    private userService: UserService,
    private message: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private commonService: CommonService,
    private datePipe: DatePipe,
  ) {
   
  }
   ngOnInit() {

    this.UpdateClientGroup = this.formBuilder.group({
      hraClientIdCtrl: ['' ],
      vpsClientIdCtrl: [''],
      clientfirstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      clientLastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      akafirstNameCtrl: ['' ],
      akaLastNameCtrl: [''],
      ssnCtrl: ['',[Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      dobCtrl: ['',Validators.required],
      genderCtrl: ['',Validators.required],
      ethenicityCtrl: [''],
      interpreterCtrl: [''],
      languageCtrl: [''],
      scimsCtrl: [''],
      ageCtrl:[''],
      updatedDateCtrl:[''],
      updatedByCtrl:[''],
      pactReferralCtrl:[''],
      vpsReferralCtrl:[''],
  });
  this.loadRefGroupDetails();


    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUserData = userdata;
       
      }
    });
    // alert('i am here');
    this.ClientSelectedSub = this.adminToolService.getSHClientSelected().subscribe((res: IClientSearch) => {
      if (res) {
        this.clientInfo = res;
        this.clientID = this.clientInfo.clientID;
        this.sourceType = this.clientInfo.clientSourceType;

        if (this.clientID) {
          this.adminToolService.getUpdateClientInfo(this.clientID.toString(),this.sourceType.toString())
            .subscribe(
              res => {
                this.updateClientInfo = res.body as UpdateClientInfo;
                if (this.updateClientInfo)
                   this.loadClientInfo();
                  else
                  {
                  this.message.info("This client is Inactive",'Update client');
                  this.router.navigate(['/admin']);
                  }

              },
              error => console.error('Error!', error)
            );
        }
        // console.log('placementClientSelected: ', this.placementClientSelected);
      } else {
        this.router.navigate(['/admin']);
      }
    });
   
  }
  ngOnChanges() {
 
  }

  
  
  
  ngOnDestroy() {
    if (this.ClientSelectedSub) {
      this.ClientSelectedSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.commonServiceSub){
      this.commonServiceSub.unsubscribe();
    }

  }
  exitPage() {
    this.router.navigate(['admin/admin-tools']);
  }

  
  resetformerror() {
    this.formErrors = {
      hraClientIdCtrl:'',
      vpsClientIdCtrl: '',
      clientfirstNameCtrl: '',
      clientLastNameCtrl: '',
      akafirstNameCtrl: '',
      akaLastNameCtrl: '',
      ssnCtrl:'',
      dobCtrl: '',
      genderCtrl: '',
      ethenicityCtrl: '',
      interpreterCtrl: '',
      languageCtrl: '',
      scimsCtrl: '',  
      ageCtrl:'',
      updatedDateCtrl:'',
    updatedByCtrl:'',
    pactReferralCtrl:'',
    vpsReferralCtrl:'',

    };
  }

  ValidateUpdateClient = (): boolean => {

    //this.clearValidators();
    var messages: any;
    var key: any;
    this.resetformerror();
    
    this.updateClientInfo = {
      hraclientID:this.UpdateClientGroup.get('hraClientIdCtrl').value,
    vpsClientID:this.UpdateClientGroup.get('vpsClientIdCtrl').value,
    firstName:this.UpdateClientGroup.get('clientfirstNameCtrl').value,
    lastName: this.UpdateClientGroup.get('clientLastNameCtrl').value,
    akaFirstName: this.UpdateClientGroup.get('akafirstNameCtrl').value,
    akaLastName: this.UpdateClientGroup.get('akaLastNameCtrl').value,
    ssn:this.UpdateClientGroup.get('ssnCtrl').value,
    dob: this.UpdateClientGroup.get('dobCtrl').value,
    genderType :this.UpdateClientGroup.get('genderCtrl').value,
    ethnicityType:this.UpdateClientGroup.get('ethenicityCtrl').value,
    interpreter: (this.UpdateClientGroup.get('ethenicityCtrl').value ===33) ? true : false,
    primaryLanguageType: this.UpdateClientGroup.get('languageCtrl').value,
    scims:this.UpdateClientGroup.get('scimsCtrl').value,
    age : this.UpdateClientGroup.get('ageCtrl').value,
    noOfPactReferral :this.UpdateClientGroup.get('pactReferralCtrl').value,
    noOfVPSReferral :this.UpdateClientGroup.get('vpsReferralCtrl').value,
    userId : this.currentUserData.optionUserId,
     
    }

      if (this.UpdateClientGroup.get('clientfirstNameCtrl').invalid) {
        key = "clientfirstNameCtrl";
        this.formErrors[key] += "please enter First Name" + '\n';
      }
     
      if (this.UpdateClientGroup.get('clientLastNameCtrl').invalid) {
        key = "clientLastNameCtrl";
        this.formErrors[key] += "please enter Last  Name" + '\n';
      }

      if (this.UpdateClientGroup.get('akafirstNameCtrl').invalid) {
        key = "akafirstNameCtrl";
        this.formErrors[key] += "please enter Valid Aka fisst  Name" + '\n';
      }
      if (this.UpdateClientGroup.get('akaLastNameCtrl').invalid) {
        key = "akaLastNameCtrl";
        this.formErrors[key] += "please enter aka Last Name" + '\n';
      }
      if (this.UpdateClientGroup.get('ssnCtrl').invalid) {
        key = "ssnCtrl";
        this.formErrors[key] += "please enter valid SSN" + '\n';
      }
      if (this.UpdateClientGroup.get('dobCtrl').invalid) {
        key = "dobCtrl";
        this.formErrors[key] += "please enter valid DOB" + '\n';
      }
      if (this.UpdateClientGroup.get('genderCtrl').invalid) {
        key = "genderCtrl";
        this.formErrors[key] += "please enter valid Gender" + '\n';
      }
  /*    if (!this.validateAge()) {
        return false;
      }*/

      if (this.clientAgeInYears < 17) {
        key = "dobCtrl";
        this.formErrors[key] += "Client must be at least 17 years and 8 months of age." + '\n';
       
      }
      if (this.clientAgeInYears === 17 && this.clientAgeInMonths < 8) {
        key = "dobCtrl";
        this.formErrors[key] += "Client must be at least 17 years and 8 months of age." + '\n';
      }
      if (this.clientAgeInYears > 119) {
        key = "dobCtrl";
        this.formErrors[key] += "Client is over 119 years old. Please check the DOB or Age." + '\n';
       }
       
      
     

    var errormessage = '';
    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '') {
      this.message.error(errormessage, "Validation Error");
      return false;
    }
    else {
      return true;
    }
  }


  loadRefGroupDetails = () => {
    const ethnicityGroupId = 8; const languageGroupId = 18; const genderGroupId = 4; const yesNoRefGroupId=7;
    this.commonServiceSub = this.commonService.getRefGroupDetails(ethnicityGroupId + ',' + languageGroupId + ',' + genderGroupId + ',' + yesNoRefGroupId).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.genders = data.filter(d => (d.refGroupID === genderGroupId));
        this.languages = data.filter(d => (d.refGroupID === languageGroupId ));
        this.ethnicities = data.filter(d => d.refGroupID === ethnicityGroupId );
        this.yesNoTypes = data.filter(d => d.refGroupID === yesNoRefGroupId && (d.refGroupDetailDescription.toUpperCase() === 'YES' || d.refGroupDetailDescription.toUpperCase() === 'NO'));
      },
      error => console.error('Error!', error)
    );
  }

  OnSave(event: Event) {
    if (this.ValidateUpdateClient())
    {
      const title = 'Confirm Update';
    const primaryMessage = '';
    const secondaryMessage = "Are you sure to Update Client info " + this.clientID + " ?";
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => 
        {
         this.adminToolService.saveUpdateClientInfo(this.updateClientInfo)
          .subscribe(
            data => {
              this.message.success("Client Update successfully done", 'Save Success');
              
            },
            error => { this.message.error("Save Failed", 'Save Failed'); }
          );
        },
        (negativeResponse) => console.log(),
      );

  }
  }  

  
  
  calculateAge() {
    if (this.UpdateClientGroup.get('dobCtrl').value && !moment(this.UpdateClientGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid Date of Birth.");
      }
      this.UpdateClientGroup.controls['dobCtrl'].setValue("");
      return;
    }
    if (this.UpdateClientGroup.get('dobCtrl').value) {
      const starts = moment(this.UpdateClientGroup.get('dobCtrl').value, 'MM/DD/YYYY');
      const ends = moment(Date.now());
      const years = ends.diff(starts, 'year');
      starts.add(years, 'years');
      const months = ends.diff(starts, 'months');
      starts.add(months, 'months');
      this.clientAgeInYears = years;
      this.clientAgeInMonths = months;
      this.UpdateClientGroup.controls['ageCtrl'].setValue(this.clientAgeInYears);
    }
  }

  onSubmit() {
   
  }

  loadClientInfo()
  {
    console.log(this.updateClientInfo)
   
         this.UpdateClientGroup.get('hraClientIdCtrl').disable();
              this.UpdateClientGroup.get('vpsClientIdCtrl').disable();
              this.UpdateClientGroup.get('ageCtrl').disable();
              this.UpdateClientGroup.get('updatedByCtrl').disable();
              this.UpdateClientGroup.get('updatedDateCtrl').disable();
              this.UpdateClientGroup.get('pactReferralCtrl').disable();
              this.UpdateClientGroup.get('vpsReferralCtrl').disable();
              this.UpdateClientGroup.get('hraClientIdCtrl').setValue(this.updateClientInfo.hraclientID);
              this.UpdateClientGroup.get('vpsClientIdCtrl').setValue(this.updateClientInfo.vpsClientID);
              this.UpdateClientGroup.get('clientfirstNameCtrl').setValue(this.updateClientInfo.firstName);
              this.UpdateClientGroup.get('clientLastNameCtrl').setValue(this.updateClientInfo.lastName);
              this.UpdateClientGroup.get('akafirstNameCtrl').setValue(this.updateClientInfo.akaFirstName);
              this.UpdateClientGroup.get('akaLastNameCtrl').setValue(this.updateClientInfo.akaLastName);
              this.UpdateClientGroup.get('ssnCtrl').setValue(this.updateClientInfo.ssn);
              
             this.UpdateClientGroup.get('dobCtrl').setValue(this.updateClientInfo.dob);
              this.UpdateClientGroup.get('genderCtrl').setValue(this.updateClientInfo.genderType);
              this.UpdateClientGroup.get('ethenicityCtrl').setValue(this.updateClientInfo.ethnicityType);
              this.UpdateClientGroup.get('interpreterCtrl').setValue((this.updateClientInfo.interpreter === true) ? 33 : 34)
              this.UpdateClientGroup.get('scimsCtrl').setValue(this.updateClientInfo.scims);
              this.UpdateClientGroup.get('ageCtrl').setValue(this.updateClientInfo.age);
              this.UpdateClientGroup.get('languageCtrl').setValue(this.updateClientInfo.primaryLanguageType);
              this.UpdateClientGroup.get('updatedByCtrl').setValue(this.currentUserData.lastName  + ' , ' + this.currentUserData.firstName );
              this.UpdateClientGroup.get('updatedDateCtrl').setValue(this.currentDate );
              this.UpdateClientGroup.get('pactReferralCtrl').setValue(this.updateClientInfo.noOfPactReferral);
              this.UpdateClientGroup.get('vpsReferralCtrl').setValue(this.updateClientInfo.noOfVPSReferral);
           this.calculateAge();

  }
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  clearPage() {
    this.router.navigate(['admin/admin-tools']);

  }


  validateAge() {
    if (this.clientAgeInYears < 17) {
      if (!this.message.currentlyActive) {
        this.message.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
      }
      return false;
    }
    if (this.clientAgeInYears === 17 && this.clientAgeInMonths < 8) {
      if (!this.message.currentlyActive) {
        this.message.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
      }
      return false;
    }
    if (this.clientAgeInYears > 119) {
      if (!this.message.currentlyActive) {
        this.message.error("Client is over 119 years old. Please check the DOB or Age.");
      }
      return false;
    }
    return true;
  }

}


