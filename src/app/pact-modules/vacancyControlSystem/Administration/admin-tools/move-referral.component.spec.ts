import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveReferralComponent } from './move-referral.component';

describe('MoveReferralComponent', () => {
  let component: MoveReferralComponent;
  let fixture: ComponentFixture<MoveReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoveReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
