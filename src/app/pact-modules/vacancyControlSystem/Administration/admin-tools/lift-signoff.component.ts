
import { Component, OnInit, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import { determinationInfo, removeSignOff } from "./admin-tools.model"
import { HttpClient } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { ActivatedRoute, Router, Event } from '@angular/router';
import { AdminToolService } from './admin-tools.service';
import { CommonService } from '../../../../services/helper-services/common.service';
import { IClientSearch } from '../admin-interface.model';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Alert } from 'selenium-webdriver';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';


@Component({
  selector: 'app-lift-signoff',
  templateUrl: './lift-signoff.component.html',
  styleUrls: ['./lift-signoff.component.scss']
})
export class LiftSignoffComponent implements OnInit {
  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  pagination;
  rowSelection;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;
  


  currentUserData: AuthData;
  userDataSub: Subscription;
  context;
  overlayLoadingTemplate: string = '';
  overlayNoRowsTemplate: string = '';
  overlayLoadinghistoryTemplate: string = '';
  overlayNoRowshistoryTemplate: string = '';
  liftsignoffForm: FormGroup;
  public determinationinfogridOptions: GridOptions;
  determinationInfoData: determinationInfo[] = [];
  lisftSignOffModel: removeSignOff ;
  activatedRouteSub: Subscription;
  clientInfo: IClientSearch = {};
  ClientSelectedSub: Subscription;
  enableUpdate: boolean = false;
  approvalmaxdate = new Date();
  approvalmindate = new Date();
  pactApplicationID :number=0;
  currentApprovalTo :string;
  signoffByUser:string;
  signoffDate:string;

  //clientID:number =240544;
  @Input() clientID: number = 240544;
  formErrors = {
   

  };



  constructor(private http: HttpClient,
    private sidenavService: NavService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private adminToolService: AdminToolService,
    private userService: UserService,
    private confirmDialogService: ConfirmDialogService,
    private message: ToastrService
  ) {
    this.determinationinfogridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;

 
    //this.gridOptions.api.hideOverlay();

    this.columnDefs = [
      {
        headerName: 'Referral Date',
        filter: 'agTextColumnFilter',
        field: "referralDate",
        width: 100,

      },
      {
        headerName: 'Referring Agency/Site',
        field: 'referringAgencySite',
        width: 200,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Eligibility',
        field: 'eligibility',
        width: 250,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'prioritization',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeed',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Type',
        field: 'housingType',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Approval Period',
        field: 'approvalFrom',
        width: 200,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.approvalFrom + ' - ' + params.data.approvalTo;
        }
      },
      {
        headerName: 'Placement Agency/Site',
        field: 'PlacementAgencySite',
        width: 200,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.placementAgencyNo + ' - ' + params.data.placementAgencyName + ' / ' + params.data.placementSiteNo + ' - ' + params.data.placementSiteName;
        }

      },
      {
        headerName: 'Move In',
        field: 'moveInDate',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Move Out',
        field: 'moveOutDate',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Reason Moved',
        field: 'moveOutReasonDescription',
        width: 200,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Select',
        field: 'select',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        checkboxSelection: true
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: false
    };



    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.pagination = true;
    this.frameworkComponents = {
      //actionRenderer: AgencySiteMaintenanceActionComponent
    };
   
  }

  ngOnInit() {
    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUserData = userdata;
       
      }
    });
    // alert('i am here');
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait Application deterimnation are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Application deterimnation are available</span>';

    this.overlayLoadinghistoryTemplate = '<span class="ag-overlay-loading-center">Please wait Application deterimnation are loading.</span>';
    this.overlayNoRowshistoryTemplate = '<span style="color:#337ab7;">Not currently extended</span>';

    this.ClientSelectedSub = this.adminToolService.getSHClientSelected().subscribe((res: IClientSearch) => {
      if (res) {
        this.clientInfo = res;
        this.clientID = this.clientInfo.clientID;
        // console.log('placementClientSelected: ', this.placementClientSelected);
      } else {
        this.router.navigate(['/admin']);
      }
    });
    this.liftsignoffForm = this.formBuilder.group({
     
    });
    this.determinationInfogridRefresh();
  }
  ngOnChanges() {
    this.determinationInfogridRefresh();

  }

  refreshDeterminationInfoAgGrid() {
    this.determinationinfogridOptions.api.setFilterModel(null);
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {

      if (column.colId !== 'select') {

        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);

      }

    });
  //  this.gridColumnApi.autoSizeColumns(allColumnIds);



  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onDeterminationInfogridRowSelected(event) {
    
    if (event.node.selected) {
    
      this.enableUpdate = true;
      this.pactApplicationID = event.node.data.pactApplicationID;
      console.log(event.node.data);
      this.signoffByUser= event.node.data.signoffByUser
      this.signoffDate= event.node.data.signoffDate
      
    }
    if (this.gridApi.getSelectedNodes().length === 0 )
      this.enableUpdate = false;
     else
    {
       this.enableUpdate = true;
    }
   /* else
    {
      this.enableUpdate = false;
    }*/
  }

  onRowSelected(event) {
    if (event.node.selected) {

    }
  }
  determinationInfogridRefresh() {
    if (this.clientID) {
      this.adminToolService.getDeterminationByClient(this.clientID.toString(),"1")
        .subscribe(
          res => {
            this.determinationInfoData = res.body as determinationInfo[];
          },
          error => console.error('Error!', error)
        );
    }
  }
 

  ngOnDestroy() {
    if (this.ClientSelectedSub) {
      this.ClientSelectedSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }

  }
  exitPage() {
    this.router.navigate(['admin/admin-tools']);
  }

 

  resetformerror() {
    this.formErrors = {
     
    };
  }

  ValidateLiftSignoff = (): boolean => {

    //this.clearValidators();
    var messages: any;
    var key: any;
    this.resetformerror();
    this.lisftSignOffModel = {
     pactApplicationID: this.pactApplicationID,
     userId : this.currentUserData.optionUserId
 
    }
   
    var errormessage = '';
    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '') {
      this.message.error(errormessage, "Validation Error");
      return false;
    }
    else {
      return true;
    }
  }

  OnSave(event: Event) {
    if (this.ValidateLiftSignoff() === true) 
    {
    const title = 'Confirm Lift sign off';
    const primaryMessage = '';
    const secondaryMessage = "Are you sure to Lift sign off for " + this.pactApplicationID + " ?";
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => 
        {
          console.log(this.lisftSignOffModel)
              this.adminToolService.saveRemoveSignOff(this.lisftSignOffModel).subscribe(
            data => {
              this.message.success("Sign off Lifted Sucessfull", 'LiftSign off success');
              this.determinationInfogridRefresh();
              this.enableUpdate =false;
            
            },
            error => { this.message.error("Save Failed", 'Liftsign off failed'); }
          );
        },
        (negativeResponse) => console.log(),
      );

    } 
  }

  onSubmit() {
   
  }
  


}


