export interface IClientSearch {
  clientID?: number;
  clientSourceType?: number;
  clientSourceDescription?: string;
  vcsid?: string;
  firstName?: string;
  lastName?: string;
  ssn?: string;
  dob?: string;
  genderType?: number;
  gender?: string;
  cin?: number;
  referralDateFrom?: string;
  referralDateTo?: string;
}

