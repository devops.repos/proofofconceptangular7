import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SiteList } from './assign-tad-liaison.model';

@Injectable({
  providedIn: 'root'
})
export class TADLiaisonService {
  private apiURL = environment.pactApiUrl + 'TADLiaison/'; 

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  

  getTADLiaisonSiteListByAgency(agencyID: number) {
    const getUrl = this.apiURL + 'GetTADLiaisonSiteListByAgency/';
    return this.http.get(getUrl + agencyID);
   }

   getTADLiaisonUserList(){
    const getUrl = this.apiURL + 'GetTADLiaisonUserList/';
    return this.http.get(getUrl);
   }

   saveTADLiaison(sitelist: SiteList[]){
     const postUrl = this.apiURL + 'SaveTADLiaison/';
     return this.http.post(postUrl, sitelist, this.httpOptions);
   }

}
