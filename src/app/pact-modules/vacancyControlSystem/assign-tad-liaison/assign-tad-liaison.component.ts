import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
//import { CommonService } from '../../../services/helper-services/common.service';
//import { RefGroupDetails } from "src/app/models/refGroupDetailsDropDown.model";
//import { UserRole } from 'src/app/models/pact-enums.enum';
import { AgencyData } from '../user-administration/user-administration.model';
import { UserAdminService } from '../user-administration/user-administration.service';
import { SiteList, LiaisonUserList } from './assign-tad-liaison.model';
import { TADLiaisonService } from './assign-tad-liaison.service';
import { ToastrService } from "ngx-toastr";
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { Router } from '@angular/router';

  
@Component({
  selector: 'app-assign-tad-liaison',
  templateUrl: './assign-tad-liaison.component.html',
  styleUrls: ['./assign-tad-liaison.component.scss']
})
export class AssignTadLiaisonComponent implements OnInit, OnDestroy {

  agencyListSub: Subscription;
  siteListSub: Subscription;
  rowDataSub: Subscription;
  liaisonListSub: Subscription;
  saveTADLiaisonSub: Subscription;

  agencyList: AgencyData[] = [];
  siteList: SiteList[] = [];
  liaisonList: LiaisonUserList[] = [];
  selectedAgencyID: number = 0;
  selectedSiteID: number = 0;
  selectedLiaisonID: number = 0;
  selectedSitesList: SiteList[] = [];
  
  is_DropDown_Disabled: boolean = true;
  displayClass: boolean = true;

  rowData: SiteList[]= [];
  
  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  rowSelection;
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;

  public gridOptions: GridOptions;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  constructor(private userAdminService: UserAdminService,
              private toastr: ToastrService,
              private tadLiaisonService: TADLiaisonService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router
              ) {

 
   
    this.gridOptions = {
      rowHeight: 35
    } as GridOptions;

    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the site list is loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Sites Available</span>';

    this.columnDefs = [      
      {
        headerName: 'Agency/Site#',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.agencyNo + '/' + params.data.siteNo;
        }        
      },      
      {
        headerName: 'Site Name',
        filter: 'agTextColumnFilter',
        field: 'siteName'
      },
      {
        headerName: 'Site Status',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          var status = params.data.isActive ? "Active" : "Inactive" 
          return status;
        }   
      },
      {
        headerName: 'Site Type',
        field: 'siteType',
        width: 200,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Location',
        filter: 'agTextColumnFilter',
        field: 'siteLocation'
      },
      {
        headerName: 'Site TAD Liaison',
        field: 'tadLiaisonName',
        filter: 'agTextColumnFilter',
      },
      {
        headerCheckboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
        headerName: 'Select all',
        field: 'select',
        width: 65,
        filter: false,
        sortable: false,
        resizable: true,
        pinned: 'left',
        suppressMenu: true,
        checkboxSelection: true,               
    } 
    ];
   
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.rowSelection = 'multiple';

  }

  ngOnInit() {  
            
  /** Get the Agency List */
            
  this.agencyListSub = this.userAdminService.getAgencyList().subscribe(res => {
    if (res) {
      this.agencyList = res as AgencyData[];
    }
  });

  this.liaisonListSub = this.tadLiaisonService.getTADLiaisonUserList().subscribe(res => {
    if(res) {
      this.liaisonList = res as LiaisonUserList[];
    }
  });

  }

  onAgencySelected(){
    if(this.selectedAgencyID != 0){
      this.is_DropDown_Disabled = false;
      
      this.siteListSub = this.tadLiaisonService.getTADLiaisonSiteListByAgency(this.selectedAgencyID).subscribe(res => {
        if (res) {
          this.siteList = res as SiteList[];     
        }
      });
    }
    else {
      this.is_DropDown_Disabled = true;
    }
    
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;   

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'select' && column.colId !== 'siteType') {
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, true);
    this.gridApi.sizeColumnsToFit(allColumnIds);
    
  }

  onRowSelected() {
    
    if (this.agGrid.api.getSelectedNodes().length > 0) {
      this.displayClass = false;
    } else {
      this.displayClass = true;
    }
  }


  getSelectedRows() {
    
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
      
    selectedData.map((node: SiteList) => {
      this.selectedSitesList.push(node);
    });
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'SiteList-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.gridApi.exportDataAsExcel(params);
  }

  onGo(){
    if(this.selectedAgencyID == 0) {
      this.toastr.error("Please select Agency.", "");
      return;
    }
    else {
      if(this.selectedSiteID == 0){
        this.rowData = this.siteList;
      }
      else
      {
        this.rowData = this.siteList.filter(res => {
          return res.siteID == this.selectedSiteID;
        })
      }
    }
    this.displayClass = true;
    this.selectedSitesList = [];
    this.agGrid.api.deselectAll();
  }
  onSave(){
    if (this.agGrid.api.getSelectedNodes().length > 0) {
      if(this.selectedLiaisonID != 0){
        var liaisonName = this.liaisonList.filter(item => { return item.liaisonID == this.selectedLiaisonID});

        const title = 'Warning';
        const primaryMessage = `Do you want to update all the selected sites with ` + liaisonName[0].liaisonName + ` as TAD Liaison`;
        const secondaryMessage = ``;
        const confirmButtonName = 'Yes';
        const dismissButtonName = 'No';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              this.getSelectedRows();
    
              this.selectedSitesList.forEach(site => { site.tadLiaisonID = this.selectedLiaisonID});
              this.saveTADLiaisonSub = this.tadLiaisonService.saveTADLiaison(this.selectedSitesList).subscribe(res => {
                this.toastr.success("TAD Liaison assignment was saved successfully.");
                this.siteListSub = this.tadLiaisonService.getTADLiaisonSiteListByAgency(this.selectedAgencyID).subscribe(res => {
                  if (res) {
                    this.siteList = res as SiteList[];     
                    if(this.selectedSiteID == 0){
                      this.rowData = this.siteList;
                    }
                    else
                    {
                      this.rowData = this.siteList.filter(res => {
                        return res.siteID == this.selectedSiteID;
                      })
                    }
                    this.selectedSitesList = [];
                    this.displayClass = true;
                  }
                });

              },
              error =>
                this.toastr.error("TAD Liaison assignment was not saved.", "Error while saving"));
            },
            negativeResponse => {}
          );
      }
      else {
        this.toastr.error('Please select TAD Liaison to proceed');
      }
    }
    else {
      this.toastr.error('Please select at least one Site to proceed');
    }
  }

  onExit(){
    this.router.navigate(['/dashboard']);
  }

  ngOnDestroy() {    
    if (this.agencyListSub) {
      this.agencyListSub.unsubscribe();
    }
    if (this.siteListSub) {
      this.siteListSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.liaisonListSub) {
      this.liaisonListSub.unsubscribe();
    }
    
   
  }
  
}


