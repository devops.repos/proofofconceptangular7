import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignTadLiaisonComponent } from './assign-tad-liaison.component';

describe('AssignTadLiaisonComponent', () => {
  let component: AssignTadLiaisonComponent;
  let fixture: ComponentFixture<AssignTadLiaisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignTadLiaisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignTadLiaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
