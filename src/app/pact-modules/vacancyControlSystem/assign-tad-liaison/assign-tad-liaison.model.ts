export class SiteList {
    siteID: number;
    siteNo: string;
    siteName: string;
    agencyNo: string;
    isActive: boolean;
    siteType: string;
    siteLocation: string;
    tadLiaisonID: number;
    tadLiaisonName: string;
}

export class LiaisonUserList {
    liaisonID: number;
    liaisonName: string;

}