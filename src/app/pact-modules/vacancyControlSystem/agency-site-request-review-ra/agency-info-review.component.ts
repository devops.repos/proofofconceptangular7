import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import {  FormGroup, FormBuilder, Validators} from '@angular/forms';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { Subscription } from 'rxjs';
import { siteDemographic,AgencyData,housingProgram } from 'src/app/pact-modules/vacancyControlSystem/agency-site-maintenance/agency-site-model';
import { SiteAdminService } from 'src/app/pact-modules/vacancyControlSystem/agency-site-maintenance/site-admin.service';
import { ToastrService } from 'ngx-toastr';
import {  MatSelectChange } from '@angular/material';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { RaAgencySiteRequestModel } from "./ra-agency-site-request.model";
import {ReferringAgencyRequestReviewEventService} from './agency-site-request-review-ra-event.service';

@Component({
  selector: 'app-agency-info-review',
  templateUrl: './agency-info-review.component.html',
 // styleUrls: ['./add-primary-service-contract.component.scss'],
})
export class AgencyInfoReviewComponent implements OnInit, OnDestroy {

  refGroupDetailsData : RefGroupDetails[];
  saveOrAddLabel3 : string = "Add";
  tabSelectedIndex: number = 0;
  preSelectedAgencyType : any;

  agencyInfo : any;
  siteApprovalData : any;
  isYesNoList: any;

  agencyInfoReviewFormGroup: FormGroup;

  @Input() agencyTypes: housingProgram[];
  @Input() agencyRequestID: number;
  @Input() siteRequestID: number;
  @Input() requestTypeID: number;
  @Input() agencySiteData : RaAgencySiteRequestModel;
  
constructor(
  private formBuilder: FormBuilder,
  private commonService : CommonService, private raRequestReviewEventService : ReferringAgencyRequestReviewEventService,
  private siteAdminservice : SiteAdminService,
  private toastr: ToastrService,

) {
      this.refGroupDetailsData = [];
  }

  ngOnInit() {
    let that = this;
   this.agencyInfoReviewFormGroup = this.formBuilder.group({
        agencyNameCtrl: [{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9 ]+')]],
        agencyTypeCtrl: [{value: that.preSelectedAgencyType, disabled: (that.requestTypeID === 321 ? true : false) }, [CustomValidators.dropdownRequired()]],
        agencyAddressCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(200)]],
        agencyCityCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z][a-zA-Z ]*')]],
        agencyStateCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
        agencyZipCodeCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(5), Validators.pattern('[0-9]+')]],

        agConFirstCtrl: [{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
        agConLastCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
        agConTitleCtrl: [{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.maxLength(25)]], // , Validators.pattern('^[A-Za-z.]+$')
        agConEmailCtrl: [{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(50), Validators.email]],
        agConCellCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.maxLength(10), Validators.pattern('[0-9]+')]],
        agConPhoneCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.required, Validators.maxLength(10), Validators.pattern('[0-9]+')]],
        agConExtnCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.maxLength(4), Validators.pattern('[0-9]+')]],
        agConFaxCtrl:[{value: '', disabled: (that.requestTypeID === 321 ? true : false) }, [Validators.maxLength(10), Validators.pattern('[0-9]+')]],
    });

  }

  readonly validationMssg = {
    agencyNameCtrl: {
      'required': 'Please enter Agency Name.',
      'pattern': 'Only Alphabet characters and/or space allowed.'
    },
    agencyTypeCtrl: {
      'required': 'Please select Agency Type.',
    },
    agencyAddressCtrl: {
      'required': 'Please enter Agency Address.',
    },
    agencyCityCtrl: {
      'required': 'Please enter Agency City.',
      'pattern': 'Please enter alphabets. space character allowed.'
    },
    agencyStateCtrl: {
      'required': 'Please Agency State.',
      'pattern': 'Only Alphabet characters allowed.'
    },
    agencyZipCodeCtrl: {
      'required': 'Please enter Agency Zip.',
      'pattern': 'Please enter only Numbers 0 to 9.',
    },
    agConFirstCtrl: {
      'required': 'Please enter Contact First Name.',
      'pattern': 'Only Alphabet characters allowed.'
    },
    agConLastCtrl: {
      'required': 'Please enter Contact Last Name.',
      'pattern': 'Only Alphabet characters allowed.'
    },
    agConTitleCtrl: {
      'maxLength': 'Please enter max 25 characters.',
    },
    agConEmailCtrl: {
      'required': 'Please enter Contact Email.',
      'email': 'Please enter a valid Email format'
    },
    agConCellCtrl: {
      'required': 'Please enter Contact Cell.',
      'pattern': 'Please enter only Numbers 0 to 9.',
    },
    agConPhoneCtrl: {
      'required': 'Please enter Contact Phone.',
      'pattern': 'Please enter only Numbers 0 to 9.',
    },
    agConExtnCtrl: {
      'required': 'Please enter Phone Extn.',
      'pattern': 'Please enter only Numbers 0 to 9.',
    },
    agConFaxCtrl: {
      'required': 'Please enter Contact Fax.',
      'pattern': 'Please enter only Numbers 0 to 9.',
    },
  };

  get f() { return this.agencyInfoReviewFormGroup.controls; }

  checkDuplicateAgency(event){

  }

  clearPlacementAgency(){
    // this.agreePopSelectedUniqueID = 0;
    // this.agreePopSelectedID = 0;
    // this.agreePopSelectedText = '';
    // this.placementAgenciesSelectedText = '';
    // this.additionalInfoFormGroup.get('populationNamesCtrl').setValue(null);
    // this.additionalInfoFormGroup.get('placementAgencyCtrl').setValue(null);
    // this.placementAgencyDataModel = null;
    this.saveOrAddLabel3 = "Add";
  }


  onSubmit(){
    this.markFormGroupTouched(this.agencyInfoReviewFormGroup);
    if(this.agencyInfoReviewFormGroup.valid){
      this.raRequestReviewEventService.emitAgencyReqInfoValid(this.agencyRequestID, true);
    }
    else
    this.raRequestReviewEventService.emitAgencyReqInfoValid(this.agencyRequestID, false);
  }

  private markFormGroupTouched (formGroup: FormGroup) {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  ngOnDestroy() {
    // if(this.evtSvcSub2) this.evtSvcSub2.unsubscribe();
    // if(this.placementAgencySub) this.placementAgencySub.unsubscribe();
  }

}