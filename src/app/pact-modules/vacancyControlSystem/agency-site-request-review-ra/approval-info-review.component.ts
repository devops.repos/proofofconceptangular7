import { Component, OnInit, OnDestroy, AfterViewInit, Input } from '@angular/core';
import {  FormGroup, FormBuilder, Validators} from '@angular/forms';
import { SiteContact,AgencyData } from '../agency-site-maintenance/agency-site-model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { GridOptions } from 'ag-grid-community';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import 'ag-grid-enterprise';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { MatRadioChange, MatSelectChange } from '@angular/material';
import {AgencySiteRequestReviewService} from '../agency-site-request-review/agency-site-request-review.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { RaAgencySiteRequestModel } from "./ra-agency-site-request.model";
import {ReferringAgencyRequestReviewEventService} from './agency-site-request-review-ra-event.service';

@Component({
  selector: 'app-approval-info-review',
  templateUrl: './approval-info-review.component.html',
 // styleUrls: ['./add-primary-service-contract.component.scss'],
})
export class ApprovalInfoReviewComponent implements OnInit, AfterViewInit ,OnDestroy {

  refGroupDetailsData : RefGroupDetails[];
  saveOrAddLabel3 : string = "Add";
  tabSelectedIndex: number = 0;
  classAgncyNameCtrl : string = "required-label-color";
  classSysAdminCtrl : string = "required-label-color";
  classCapsSurveyCtrl : string = "required-label-color";

  placementAgencySub: Subscription;
  evtSvcSub2: Subscription;
  agSiteReqReviewSvcSub: Subscription;
  siteAdminSvcSub1: Subscription;

  agencyList:AgencyData[];
  agencyInfo: any;
  isYesNoList : RefGroupDetails[];
  ifSubmitted : boolean = false;

  SiteContactsData: SiteContact[] = [];
  SiteContactsAdminData: SiteContact[] = [];

  defaultColDef: any;
  gridOptions: GridOptions;
  pagination: any;
  rowSelection: any;
  context: any;
  placementGridComponents: any;

  approvalFormGroup: FormGroup;
  
  @Input() agencyRequestID: number;
  @Input() siteRequestID: number;
  @Input() requestTypeID: number;
  @Input() agencySiteApprovalData : RaAgencySiteRequestModel;
  
constructor(
   private formBuilder: FormBuilder,
  private commonService : CommonService,
  private agSiteReqReviewSvc: AgencySiteRequestReviewService, private siteAdminservice : SiteAdminService,
  private toastr: ToastrService,   private raRequestReviewEventService : ReferringAgencyRequestReviewEventService
) {
      this.refGroupDetailsData = [];
  }

  readonly validationMssg = {
    isApproveAgencyRadioCtrl: {
      'required': 'Please select Approve Agency option.',
    },
    isApproveSiteRadioCtrl: {
      'required': 'Please select Approve Site option.',
    },
    siteSysAdminCtrl: {
      'required': 'Please select Sys Admin.',
    },
    isCapsSurveyRadioCtrl: {
      'required': 'Please select CAPS Survey.',
    },
    agencyNameForApproveCtrl: {
      'required': 'Please select Agency Name for Approval.',
    },
    additionalcommentsCtrl: {
      'required': 'Please enter Additional Comments.',
    },
  };

  ngOnInit() {
    let that = this;
   this.approvalFormGroup = this.formBuilder.group({
    isApproveAgencyRadioCtrl :  [this.agencySiteApprovalData.isApproveAgency, [CustomValidators.radioGroupRequired()]], // Validators.required
    isApproveSiteRadioCtrl :  [this.agencySiteApprovalData.isApproveSite, [CustomValidators.radioGroupRequired()]],
    siteSysAdminCtrl :  [this.agencySiteApprovalData.siteSysAdminContactID, [CustomValidators.dropdownRequired()]],
    isCapsSurveyRadioCtrl :  [this.agencySiteApprovalData.isCapsMandate, [CustomValidators.radioGroupRequired()]],
    agencyNameForApproveCtrl :  [this.agencySiteApprovalData.agencyIdToApprove, [CustomValidators.dropdownRequired()]],
    additionalcommentsCtrl :  [this.agencySiteApprovalData.addlApproverComments, [Validators.required]],
    });


    this.loadRefGroupDetails();

    if (this.siteRequestID != null) {
      this.getContactsList();
    }
    
    this.loadagencylist();

      setTimeout(() => {
          if(that.requestTypeID === 321){
            that.agencySiteApprovalData.isApproveAgency = 33;
            that.approvalFormGroup.get('isApproveAgencyRadioCtrl').disable();
            that.approvalFormGroup.controls.agencyNameForApproveCtrl.setValue(null);
            that.approvalFormGroup.controls.agencyNameForApproveCtrl.disable();
            that.classAgncyNameCtrl = "optional-label-color";
          }

      }, 1500);

  }

  ngAfterViewInit(){
    if(this.agencySiteApprovalData.isApproveAgency === 33){
        this.approvalFormGroup.controls.agencyNameForApproveCtrl.setValue(null);
        this.approvalFormGroup.controls.agencyNameForApproveCtrl.disable();
        this.classAgncyNameCtrl = "optional-label-color";
    }
    if(this.agencySiteApprovalData.isApproveSite === 34){
        this.approvalFormGroup.controls.siteSysAdminCtrl.setValue(null);
        this.approvalFormGroup.controls.siteSysAdminCtrl.disable();
        this.classSysAdminCtrl = "optional-label-color";

        this.approvalFormGroup.controls.isCapsSurveyRadioCtrl.setValue(null);
        this.approvalFormGroup.controls.isCapsSurveyRadioCtrl.disable();
        this.classCapsSurveyCtrl = "optional-label-color";
    }

  }

  get f() { return this.approvalFormGroup.controls; }

  loadRefGroupDetails() {
    let that = this;
    let value = "7";
    this.commonService.getRefGroupDetails(value)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          that.isYesNoList = data.filter(d => { return ((d.refGroupID === 7) && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34)) });
        },
        error => console.error('Error!', error)
      );
  }

    loadagencylist()
    {
        let that = this;
        this.siteAdminSvcSub1 = this.siteAdminservice.getAgencyList(null,null).subscribe(
            res => {
                let result = res.body as AgencyData[];
                that.agencyList = result.sort((a, b) => a.name.localeCompare(b.name));
            },
            error => console.error('Error!', error)
          );
    }

  getContactsList() {
    let that = this;
    this.agSiteReqReviewSvcSub = this.agSiteReqReviewSvc
    .getSiteRequestContacts(this.siteRequestID)
    .subscribe(
      (res) => {
        if (res) {
          that.SiteContactsData = res as SiteContact[];
          if(that.SiteContactsData && that.SiteContactsData.length > 0)
            that.SiteContactsAdminData = that.SiteContactsData.filter(it => it.isSysAdmin == true);

            if(this.SiteContactsAdminData.length == 0){
              this.approvalFormGroup.controls.siteSysAdminCtrl.setValue(null);
            }
        }
      },
      (error) =>
        console.error(
          "Error in retrieving the Contact Data By Site ID...!",
          error
        )
    );
  }

  onSubmit(){
    this.ifSubmitted = true;
    this.markFormGroupTouched(this.approvalFormGroup);
    if(this.approvalFormGroup.valid){
      this.raRequestReviewEventService.emitApprovalFormValid(this.agencyRequestID, true);
    }
    else
      this.raRequestReviewEventService.emitApprovalFormValid(this.agencyRequestID, false);
  }

  private markFormGroupTouched (formGroup: FormGroup) {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  agencyRadioChange(evnt: MatRadioChange) {
    if (evnt.source.name === 'isApproveAgencyRadioCtrl' && evnt.value == '33') {
      this.approvalFormGroup.controls.agencyNameForApproveCtrl.setValue(null);
      this.approvalFormGroup.controls.agencyNameForApproveCtrl.disable();
      this.classAgncyNameCtrl = "optional-label-color";
    }
    else if (evnt.source.name === 'isApproveAgencyRadioCtrl' && evnt.value == '34') {
      this.approvalFormGroup.controls.agencyNameForApproveCtrl.enable();
      this.classAgncyNameCtrl = "required-label-color";

        if(this.agencySiteApprovalData.isApproveSite === 34){ // select NO for Agency and NO for Site.
          this.approvalFormGroup.controls.agencyNameForApproveCtrl.setValue(null);
          this.approvalFormGroup.controls.agencyNameForApproveCtrl.disable();
          this.classAgncyNameCtrl = "optional-label-color";
        }
    }
  }

  siteRadioChange(evnt: MatRadioChange) {
    if (evnt.source.name === 'isApproveSiteRadioCtrl' && evnt.value == '33') {
      this.approvalFormGroup.controls.siteSysAdminCtrl.enable();
      this.classSysAdminCtrl = "required-label-color";

      this.approvalFormGroup.controls.isCapsSurveyRadioCtrl.enable();
      this.classCapsSurveyCtrl = "required-label-color";

      if(this.agencySiteApprovalData.isApproveAgency === 34){
        this.approvalFormGroup.controls.agencyNameForApproveCtrl.enable();
        this.classAgncyNameCtrl = "required-label-color";
      }
    }
    else if (evnt.source.name === 'isApproveSiteRadioCtrl' && evnt.value == '34') {
      this.approvalFormGroup.controls.siteSysAdminCtrl.setValue(null);
      this.approvalFormGroup.controls.siteSysAdminCtrl.disable();
      this.classSysAdminCtrl = "optional-label-color";

      this.approvalFormGroup.controls.isCapsSurveyRadioCtrl.setValue(null);
      this.approvalFormGroup.controls.isCapsSurveyRadioCtrl.disable();
      this.classCapsSurveyCtrl = "optional-label-color";

      if(this.agencySiteApprovalData.isApproveAgency === 34){ // select NO for Agency and NO for Site.
        this.approvalFormGroup.controls.agencyNameForApproveCtrl.setValue(null);
        this.approvalFormGroup.controls.agencyNameForApproveCtrl.disable();
        this.classAgncyNameCtrl = "optional-label-color";
      }

    }
  }

  ngOnDestroy() {
    if(this.evtSvcSub2) this.evtSvcSub2.unsubscribe();
    if(this.placementAgencySub) this.placementAgencySub.unsubscribe();
    if (this.agSiteReqReviewSvcSub) this.agSiteReqReviewSvcSub.unsubscribe();
    if(this.siteAdminSvcSub1)  this.siteAdminSvcSub1.unsubscribe();
  }

}