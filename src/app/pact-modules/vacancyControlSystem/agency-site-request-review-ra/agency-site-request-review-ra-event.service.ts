import { Observable, BehaviorSubject, from } from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
  })

export class ReferringAgencyRequestReviewEventService{

    private requestAgencyInfoFormIsValid = new BehaviorSubject<Map<number, boolean>>(new Map());// agencyRequestID, boolenFlag
    private requestSiteInfoFormIsValid = new BehaviorSubject<Map<number, boolean>>(new Map());// agencyRequestID, boolenFlag
    private requestApprovalFormIsValid = new BehaviorSubject<Map<number, boolean>>(new Map());// agencyRequestID, boolenFlag 
   
        emitAgencyReqInfoValid(agencyRequestID : number, flagValue : boolean) {
            let mapObject = this.requestAgencyInfoFormIsValid.getValue();
            mapObject.set(agencyRequestID, flagValue);
            this.requestAgencyInfoFormIsValid.next(mapObject);
        }
        subscribeAgencyReqInfoValid(): Observable<Map<number, boolean>> {
            return this.requestAgencyInfoFormIsValid.asObservable();
        }

        emitSiteReqInfoValid(siteRequestID : number, flagValue : boolean) {
            let mapObject = this.requestAgencyInfoFormIsValid.getValue();
            mapObject.set(siteRequestID, flagValue);
            this.requestSiteInfoFormIsValid.next(mapObject);
        }
        subscribeSiteReqInfoValid(): Observable<Map<number, boolean>> {
            return this.requestSiteInfoFormIsValid.asObservable();
        }

        emitApprovalFormValid(agencyRequestID : number, flagValue : boolean) {
            let mapObject = this.requestAgencyInfoFormIsValid.getValue();
            mapObject.set(agencyRequestID, flagValue);
            this.requestApprovalFormIsValid.next(mapObject);
        }
        subscribeApprovalFormValid(): Observable<Map<number, boolean>> {
            return this.requestApprovalFormIsValid.asObservable();
        }
   
  }