export interface RaAgencySiteRequestModel {

   agencyRequestID: number;
   agencyName : string;
   agencyAddress : string;
   agencyCity : string;
   agencyState : string;
   agencyZip : string;
   agencyType : string;
   agencyContactFirstName : string;
   agencyContactLastName : string;
   agencyContactTitle : string;
   agencyContactOfficePhone : string;
   agencyContactOfficeExtension : string;
   agencyContactCellPhone : string;
   agencyContactFax : string;
   agencyContactEmail : string;

   siteRequestID: number;
   siteName : string;
   siteType : string;
   siteAddress : string;
   siteCity : string;
   siteState : string;
   siteZip : string;

   isApproveAgency : number;
   isApproveSite : number;
   siteSysAdminContactID? : number;
   isCapsMandate? : number;
   agencyIdToApprove? : number; // can be NULL if AgencyRequest is APPROVED then NEW agencyID will be assigned after records is created.
   addlApproverComments : string;

    userId: number;
    userName: string;
    userEmail: string;

    requestStatusType? : number;
    requestTypeID? : number;
    
  }
