import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import {  FormGroup, FormBuilder, Validators} from '@angular/forms';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { GridOptions } from 'ag-grid-community';
import { SiteAdminService } from 'src/app/pact-modules/vacancyControlSystem/agency-site-maintenance/site-admin.service';
import 'ag-grid-enterprise';
import { Subscription } from 'rxjs';
import { siteDemographic,housingProgram, UserOptions } from 'src/app/pact-modules/vacancyControlSystem/agency-site-maintenance/agency-site-model';
import { ToastrService } from 'ngx-toastr';
import { AuthData } from 'src/app/models/auth-data.model';
import { RaAgencySiteRequestModel } from "./ra-agency-site-request.model";
import {SiteContactRequestComponent} from '../site-request/site-contact-request.component';
import { UserService } from 'src/app/services/helper-services/user.service';
import {ReferringAgencyRequestReviewEventService} from './agency-site-request-review-ra-event.service';

@Component({
  selector: 'app-site-info-review',
  templateUrl: './site-info-review.component.html',
 // styleUrls: ['./add-primary-service-contract.component.scss'],
})
export class SiteInfoReviewComponent implements OnInit, OnDestroy {

  siteInfoReviewFormGroup: FormGroup;
  duplicateSiteExists : boolean = true;
  message: string;
  @Input() agencyRequestID: number;
  @Input() siteRequestID: number;
  @Input() siteTypes: housingProgram[];
  @Input() agencySiteData : RaAgencySiteRequestModel;
  
  @ViewChild(SiteContactRequestComponent) siteContactRequestComponent: SiteContactRequestComponent;

  userData: AuthData;

  userDataSub: Subscription;
  agSiteReqReviewSvcSub: Subscription;
  siteRequestSvcSub1: Subscription;
  siteRequestSvcSub2: Subscription;
  commonSvcSub: Subscription;
  evtSvcSub: Subscription;
  siteAdminServiceDuplicateSiteSub: Subscription;
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  context;
  public gridOptions: GridOptions;
  overlayLoadingTemplate: string='';
  overlayNoRowsTemplate: string='';
  useroptions: UserOptions ={
    RA : false,
    isPE : false,
    isHP : false,
    isIH : false,
    isDisableForHP:false,
    isDisableForInactiveSite:false,
    isdisableActiveRadiobutton:false,
    isActive  : false,
    optionUserId:0,
    isNewPage:false
};

  preSelectedSiteType : any;
  
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private userService :UserService,
    private raRequestReviewEventService : ReferringAgencyRequestReviewEventService,
    private commonService : CommonService, private siteAdminservice : SiteAdminService
  ) {


  }

  readonly validationMssg = {
    siteNameCtrl: {
      'required': 'Please enter Site Name.',
    },
    siteTypeCtrl: {
      'required': 'Please select Site Type.',
    },
    siteAddressCtrl: {
      'required': 'Please enter Address.',
    },
    siteCityCtrl: {
      'required': 'Please enter City.',
      'pattern': 'Please enter alphabets. space character allowed.'
    },
    siteStateCtrl: {
      'required': 'Please enter State.',
      'pattern': 'Only Alphabet characters allowed.'
    },
    siteZipCodeCtrl: {
      'required': 'Please enter Zip Code.',
      'pattern': 'Only Numbers allowed.'
    },
  };

  ngOnInit() {

    this.siteInfoReviewFormGroup = this.fb.group({
      siteNameCtrl:['', [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9 ]+')]],
      siteTypeCtrl: [this.preSelectedSiteType, [CustomValidators.dropdownRequired()]],
      siteAddressCtrl:['', [Validators.required, Validators.maxLength(200)]],
      siteCityCtrl:['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z][a-zA-Z ]*')]],
      siteStateCtrl:['', [Validators.required, Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
      siteZipCodeCtrl:['', [Validators.required, Validators.maxLength(5), Validators.pattern('[0-9]+')]],
    });
    this.getUserRolesInfo();

  }

  get f() { return this.siteInfoReviewFormGroup.controls; }


  getUserRolesInfo()
  {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if(this.userData) {
        if(this.userData.siteCategoryType.length > 0) {
          this.useroptions.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
          this.useroptions.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
          this.useroptions.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
          this.useroptions.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
          this.useroptions.optionUserId = this.userData.optionUserId;  
        }
      
          this.useroptions.isActive = true;
          if (this.useroptions.isIH == true) {
            this.useroptions.isDisableForInactiveSite=false;
            this.useroptions.isDisableForHP = false;
            this.useroptions.isdisableActiveRadiobutton=false;
          }
          else{
            this.useroptions.isDisableForInactiveSite=false;
            this.useroptions.isDisableForHP = false;
            this.useroptions.isdisableActiveRadiobutton=true;
          }
      
        }  
      }
    );
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onSubmit(): void {

    if(this.siteContactRequestComponent){
      this.siteContactRequestComponent.onSubmit();
    }
    if (this.siteInfoReviewFormGroup.valid) {
      this.raRequestReviewEventService.emitSiteReqInfoValid(this.siteRequestID, true);
    }
    else
      this.raRequestReviewEventService.emitSiteReqInfoValid(this.siteRequestID, false);

    this.markFormGroupTouched(this.siteInfoReviewFormGroup);
  }

  private markFormGroupTouched (formGroup: FormGroup) {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  clearPrimaryServiceContractFields() {
      this.siteInfoReviewFormGroup.get('hdnSiteAgreementPopulationIDCtrl').setValue(0);
      this.siteInfoReviewFormGroup.get('totalUnitsCtrl').setValue(0);
      this.siteInfoReviewFormGroup.get('agrrementPopulationCtrl').setValue('');
  }

  checkDuplicateSite(event1: any){
        let that = this;
        let _name : string = this.siteInfoReviewFormGroup.get('siteNameCtrl').value;
        if(_name === "" || _name.trim().length === 0){
          this.siteInfoReviewFormGroup.get('siteNameCtrl').setValue("");
          return;
        }
        const  newSite: siteDemographic = {
          siteID:0,
          siteNo:null,
          agencyID:0,
          housingProgram:null,
          name:this.siteInfoReviewFormGroup.get('siteNameCtrl').value,
          address:this.siteInfoReviewFormGroup.get('siteAddressCtrl').value,
          city:this.siteInfoReviewFormGroup.get('siteCityCtrl').value,
          state:this.siteInfoReviewFormGroup.get('siteStateCtrl').value,
          zip:this.siteInfoReviewFormGroup.get('siteZipCodeCtrl').value,
          locationType:null,
          referralAvailableType:null,
          tcoReadyType:null,
          tcoContractStartDate:null,
          taxCreditType:null,
          maxIncomeForStudio:null,
          maxIncomeForOneBR:null,
          levelOfCareType:null,
          contractType:null,
          contractStartDate:null,
          siteTrackedType:null,
          tadLiasion:null,
          sameAsSiteInterviewAddressType:null,
          interviewAddress:null,
          interviewCity:null,
          interviewState:null,
          interviewZip:null,
          interviewPhone:null,
          siteFeatures:null,
          siteTypeDesc:null,
          agencyName:null,
          agencyNo:null,
          userId:this.userData.optionUserId,
          siteCategoryType:null,
          boroughType:null
        }

      this.siteAdminServiceDuplicateSiteSub = this.siteAdminservice.checDuplicateSite(newSite)
          .subscribe(
                    data => {
                      let checkagency = data as boolean
                      if (!checkagency)
                      {
                        that.duplicateSiteExists = false;
                        that.toastr.success("No Duplicate Site Exists.", '');
                      }
                      else
                      {
                        that.duplicateSiteExists = true;
                        that.toastr.error('Duplicate Site Exists.', '');
                      }
                    },
                    error => {that.toastr.error("checkDuplicateSite", 'Duplicate Agency Exists');}
                  );
  }

  ngOnDestroy() {
    if(this.agSiteReqReviewSvcSub)  this.agSiteReqReviewSvcSub.unsubscribe();
    if(this.siteRequestSvcSub1) this.siteRequestSvcSub1.unsubscribe();
    if(this.siteRequestSvcSub2) this.siteRequestSvcSub2.unsubscribe();
    if(this.commonSvcSub) this.commonSvcSub.unsubscribe();
    if(this.evtSvcSub) this.evtSvcSub.unsubscribe();
    if(this.userDataSub) this.userDataSub.unsubscribe();
    if(this.siteAdminServiceDuplicateSiteSub) this.siteAdminServiceDuplicateSiteSub.unsubscribe();
  }



}