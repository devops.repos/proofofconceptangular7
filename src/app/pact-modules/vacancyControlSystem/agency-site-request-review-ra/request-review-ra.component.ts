import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import {  FormGroup, FormBuilder } from '@angular/forms';
import { CommonService } from './../../../services/helper-services/common.service';
import { AgencySiteRequestReviewService } from '../agency-site-request-review/agency-site-request-review.service';
import { housingProgram, UserOptions } from "../agency-site-maintenance/agency-site-model";
import {SiteRequestEventService} from '../site-request/site-request-event.service';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { AgencyInfoReviewComponent } from './agency-info-review.component';
import { SiteInfoReviewComponent } from './site-info-review.component';
import { ApprovalInfoReviewComponent } from './approval-info-review.component';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent, SELECT_PANEL_INDENT_PADDING_X } from '@angular/material';
import { RaAgencySiteRequestModel } from "./ra-agency-site-request.model";
import { RefGroupDetails } from './../../../models/refGroupDetailsDropDown.model';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import {ReferringAgencyRequestReviewEventService} from './agency-site-request-review-ra-event.service';

@Component({
  selector: 'app-request-review-ra',
  templateUrl: './request-review-ra.component.html',
  styleUrls: ['./request-review-ra.component.scss'],
})
export class RequestReviewRaComponent implements OnInit, OnDestroy {
 
  requestStatusTypes: RefGroupDetails[];
  housingProgramData:housingProgram[];
  siteRequestID: number;
  agencyRequestID: number;
  requestTypeID: number;

  raAgencySiteRequestModel : RaAgencySiteRequestModel;
  agencyFullAddress : string = '';
  siteFullAddress : string = '';
  
  useroptions: UserOptions ={
    RA : false,
    isPE : false,
    isHP : false,
    isIH : false,
    isDisableForHP:false,
    isDisableForInactiveSite:false,
    isdisableActiveRadiobutton:false,
    isActive  : false,
    optionUserId:0,
    isNewPage:false
  };
 
  tabSelectedIndex: number = 0;
  userData: AuthData;
  showValidationFlag : boolean = false;

  userDataSub: Subscription;
  siteDemographicSub: Subscription;
  siteApprovalSub: Subscription;
  siteAgreementSub: Subscription;
  commonServiceSub: Subscription;
  evtSvcSub1: Subscription;
  evtSvcSub2: Subscription;
  evtSvcSub3: Subscription;
  evtSvcSub4: Subscription;
  evtSvcSub5: Subscription;
  siteRequestSvcSub1: Subscription;
  siteAdminSvcSub1: Subscription;
  siteAdminSvcSub2: Subscription;
  agencySiteReviewSaveSub: Subscription;
  agencySiteReviewSaveSub2: Subscription;
  agnySiteReqReviewSvcSub1: Subscription;
  agnySiteReqReviewSvcSub2: Subscription;

  requestAgencyInfoFormIsValid : boolean = false;
  requestSiteInfoFormIsValid : boolean = false;
  requestSiteContactsIsValid : boolean = false;
  requestApprovalFormIsValid : boolean = false;

  requestReviewRaFormGroup: FormGroup;

  @ViewChild(AgencyInfoReviewComponent) agencyInfoReviewComponent : AgencyInfoReviewComponent;
  @ViewChild(SiteInfoReviewComponent) siteInfoReviewComponent : SiteInfoReviewComponent;
  @ViewChild(ApprovalInfoReviewComponent) approvalInfoReviewComponent: ApprovalInfoReviewComponent;

  
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private agencySiteReviewSvc : AgencySiteRequestReviewService,
    private siteRequestEventSvc : SiteRequestEventService,
    private siteAdminSvc : SiteAdminService,
    private userService :UserService,
    private commonService : CommonService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private raRequestReviewEventService : ReferringAgencyRequestReviewEventService
  ) {

    this.housingProgramData = [];
    this.requestStatusTypes = [];

    this.raAgencySiteRequestModel = {

        agencyRequestID : 0,
        agencyName : "",
        agencyAddress : "",
        agencyCity  : "",
        agencyState : "",
        agencyZip : "",
        agencyType : "",
        agencyContactFirstName  : "",
        agencyContactLastName : "",
        agencyContactTitle : "",
        agencyContactOfficePhone  : "",
        agencyContactOfficeExtension  : "",
        agencyContactCellPhone : "",
        agencyContactFax  : "",
        agencyContactEmail  : "",
    
        siteRequestID : 0,
        siteName : "",
        siteType  : "",
        siteAddress : "",
        siteCity  : "",
        siteState : "",
        siteZip : "",
    
        userId : 0,
        userName : "",
        userEmail : "",

        isApproveAgency : null,
        isApproveSite :  null,
        siteSysAdminContactID :  null,
        isCapsMandate :  null,
        agencyIdToApprove :  null,
        addlApproverComments :  null,

        requestStatusType : null
  
        }
        
    }

  ngOnInit() {

    this.getUserRolesInfo();
    this.loadRefGroupDetails();
  
   this.requestReviewRaFormGroup = this.formBuilder.group({

    });
    let that = this;
    this.activatedRoute.paramMap.subscribe(params => {
        that.siteRequestID = Number(params.get('siteReqID'));
        that.agencyRequestID = Number(params.get('agencyReqID'));
        that.requestTypeID = Number(params.get('requestTypeID'));
    });

    if(that.requestTypeID === 321){
        that.requestAgencyInfoFormIsValid = true; // no validation required for Agency fields as they are disabled.
    } else {
        this.evtSvcSub1 = this.raRequestReviewEventService.subscribeAgencyReqInfoValid().subscribe(res => {
          if(res && res.has(that.agencyRequestID)) { // agencyRequestID has??
              that.requestAgencyInfoFormIsValid = res.get(that.agencyRequestID); // agencyRequestID
          }
          else
              that.requestAgencyInfoFormIsValid = false;
        });
    }

    this.evtSvcSub2 = this.raRequestReviewEventService.subscribeSiteReqInfoValid().subscribe(res => {
      if(res && res.has(that.siteRequestID)) {
          that.requestSiteInfoFormIsValid = res.get(that.siteRequestID);
      }
      else
        that.requestSiteInfoFormIsValid = false;
    });
    this.evtSvcSub3 = this.siteRequestEventSvc.subscribeSiteRequestSiteContacts().subscribe(res => {
      if(res && res.has(that.siteRequestID)) { // siteRequestID..!!!! has??
          that.requestSiteContactsIsValid = res.get(that.siteRequestID); // siteRequestID..!!!
      }
      else
        that.requestSiteContactsIsValid = false;
    });
    this.evtSvcSub4 = this.raRequestReviewEventService.subscribeApprovalFormValid().subscribe(res => {
      if(res && res.has(that.agencyRequestID)) { // agencyRequestID has??
          that.requestApprovalFormIsValid = res.get(that.agencyRequestID); // agencyRequestID
      }
      else
        that.requestApprovalFormIsValid = false;
    });

      this.loadHousingPrograms();

      setTimeout(() => {
          that.loadAgencySiteData();
      }, 1000);
      
  }

  loadRefGroupDetails()
  {
    let value = "70";
    this.commonServiceSub = this.commonService.getRefGroupDetails(value)
            .subscribe(
              res => {
                const data = res.body as RefGroupDetails[];
             //  this.refGroupDetailsData = data;
                this.requestStatusTypes = data.filter(d => (d.refGroupID === 70));
              },
              error => console.error('Error while fetching getRefGroupDetails data.', error)
            );
  }

  loadAgencySiteData()
   {
     let that = this;
      this.siteDemographicSub = this.agencySiteReviewSvc.getRaAgencySiteRequest(that.agencyRequestID, that.siteRequestID)
        .subscribe(
          res => {
              that.raAgencySiteRequestModel = res as RaAgencySiteRequestModel;
              if(that.raAgencySiteRequestModel){
                that.agencyFullAddress = that.raAgencySiteRequestModel.agencyAddress + ', ' + that.raAgencySiteRequestModel.agencyCity+ ', ' + that.raAgencySiteRequestModel.agencyState + ', ' + that.raAgencySiteRequestModel.agencyZip;
                that.siteFullAddress = that.raAgencySiteRequestModel.siteAddress + ', ' + that.raAgencySiteRequestModel.siteCity+ ', ' + that.raAgencySiteRequestModel.siteState + ', ' + that.raAgencySiteRequestModel.siteZip;
              }
          },
          error => console.error('Error!', error)
        );
  }

   loadHousingPrograms()
   {
    this.siteAdminSvcSub1 = this.siteAdminSvc.getHousingPrograms()
      .subscribe(
        res => {
          this.housingProgramData = res.body as housingProgram[];
        },
        error => console.error('Error!', error)
      );
   }

    Save(isRequestApproved: boolean)
    {
        let raAgencySiteRequestClone : RaAgencySiteRequestModel = this.raAgencySiteRequestModel; // CLONE
        raAgencySiteRequestClone.userId = this.userData.optionUserId;
        raAgencySiteRequestClone.userName = this.userData.lanId;
        raAgencySiteRequestClone.userEmail = this.userData.email;
        raAgencySiteRequestClone.requestTypeID = this.requestTypeID;

        raAgencySiteRequestClone.isApproveAgency = ((this.raAgencySiteRequestModel.isApproveAgency === 33) ? // if AGENCY is approved.
                this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Approved').refGroupDetailID 
              : this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Disapproved').refGroupDetailID);

        raAgencySiteRequestClone.isApproveSite = ((this.raAgencySiteRequestModel.isApproveSite === 33) ? // if SITE is approved.
              this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Approved').refGroupDetailID 
            : this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Disapproved').refGroupDetailID);

        raAgencySiteRequestClone.requestStatusType = ((isRequestApproved === true) ? // FINAL status
                this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Approved').refGroupDetailID 
              : this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Disapproved').refGroupDetailID);

        this.agencySiteReviewSaveSub = this.agencySiteReviewSvc.saveReviewedRaAgencySiteRequest(raAgencySiteRequestClone).subscribe(
              data => {
                this.toastr.success('Successfully Saved !!', '');
                this.router.navigate(['/dashboard']);
              },
              error => {
                this.toastr.warning('Error while Saving !!', '');
              }
            );
    }

    approveClicked(evnt : Event){
      let that = this;
      if(this.validateForm()){
          setTimeout(() => {

            if(that.IsFormDataValidInTabs()){
                  that.Save(true);
            } else {
                that.toastr.warning("Please check all the Tabs, and fill required fields.", "Validation Failed.");
            }
          }, 1000);
      }
    }

    disApproveClicked(evnt : Event){
        if(this.approvalInfoReviewComponent && this.approvalInfoReviewComponent.agencySiteApprovalData && this.approvalInfoReviewComponent.agencySiteApprovalData.addlApproverComments 
                && this.approvalInfoReviewComponent.agencySiteApprovalData.addlApproverComments.trim().length > 0)
            this.Save(false);
        else
            this.toastr.warning("Please additional comments to Disapprove the Request.", "Validation Failed.");
    }

    nextPage() {

      if(this.validateTab()){

        this.showValidationFlag = false;

        if (this.tabSelectedIndex < 2)
          this.tabSelectedIndex = this.tabSelectedIndex + 1;
      }
    }

    previousPage() {

      if(this.validateTab()){

        this.showValidationFlag = false;

        if (this.tabSelectedIndex > 0) {
          this.tabSelectedIndex = this.tabSelectedIndex - 1;
        }
      }
    }

    tabChanged(tabChangeEvent: MatTabChangeEvent) {

      if(this.showValidationFlag){
        this.validateTab();
      }
      this.showValidationFlag = true; // reset
    }

    validateForm() : boolean{

      if(this.agencyInfoReviewComponent && this.agencyInfoReviewComponent.agencyInfoReviewFormGroup.invalid && this.requestAgencyInfoFormIsValid == false){ // viewing agencyInfoReviewComponent
        this.toastr.warning("Please enter the data in required fields in Agency Information Tab.", "Validation Failed.");
        return false;
      }
      else if(this.siteInfoReviewComponent && this.siteInfoReviewComponent.siteInfoReviewFormGroup.invalid  && this.requestSiteInfoFormIsValid == false){ // viewing siteInfoReviewComponent
        this.toastr.warning("Please enter the data in required fields in Site Information Tab.", "Validation Failed.");
        return false;
      }
      else if(this.approvalInfoReviewComponent && this.approvalInfoReviewComponent.approvalFormGroup.invalid  && this.requestApprovalFormIsValid == false){ // viewing approvalInfoReviewComponent
        this.toastr.warning("Please enter the data in required fields in Approval Tab.", "Validation Failed.");
        return false;
      }
      else if((this.siteInfoReviewComponent && this.siteInfoReviewComponent.siteContactRequestComponent.SiteContactsData
                 && this.siteInfoReviewComponent.siteContactRequestComponent.SiteContactsData.length == 0) && this.requestSiteContactsIsValid == false){ // viewing siteInfoReviewComponent
          this.toastr.warning("Please enter at least one Site Contact.", "Validation Failed.");
          return false;
      }
      else if (this.siteInfoReviewComponent && this.siteInfoReviewComponent.duplicateSiteExists === true) {
        this.toastr.warning("Please fix the duplicate Site Name.", "Validation Failed.");
        return false;
      }
      return true;
    }

    IsFormDataValidInTabs() : boolean {
      if(this.requestAgencyInfoFormIsValid === true && this.requestSiteInfoFormIsValid === true && this.requestApprovalFormIsValid === true && this.requestSiteContactsIsValid)
          return true;
        else 
            return false;
    }

    validateTab() : boolean{

      if(this.tabSelectedIndex == 0 && this.agencyInfoReviewComponent && this.agencyInfoReviewComponent.agencyInfoReviewFormGroup.invalid && this.requestAgencyInfoFormIsValid == false){ // viewing agencyInfoReviewComponent
        this.toastr.warning("Please enter the data in required fields in Agency Information Tab.", "Validation Failed.");
        return false;
      }
      else if(this.tabSelectedIndex == 1 && this.siteInfoReviewComponent && this.siteInfoReviewComponent.siteInfoReviewFormGroup.invalid  && this.requestSiteInfoFormIsValid == false){ // viewing siteInfoReviewComponent
        this.toastr.warning("Please enter the data in required fields in Site Information Tab.", "Validation Failed.");
        return false;
      }
      else if(this.tabSelectedIndex == 2 && this.approvalInfoReviewComponent && this.approvalInfoReviewComponent.approvalFormGroup.invalid  && this.requestApprovalFormIsValid == false){ // viewing approvalInfoReviewComponent
        this.toastr.warning("Please enter the data in required fields in Approval Tab.", "Validation Failed.");
        return false;
      }
      else if((this.tabSelectedIndex == 1 && this.siteInfoReviewComponent && this.siteInfoReviewComponent.siteContactRequestComponent.SiteContactsData
                 && this.siteInfoReviewComponent.siteContactRequestComponent.SiteContactsData.length == 0) && this.requestSiteContactsIsValid == false){ // viewing siteInfoReviewComponent
          this.toastr.warning("Please enter at least one Site Contact.", "Validation Failed.");
          return false;
      }
      return true;
    }

    get IsApproveButtonDisabled() : boolean {

      if(this.tabSelectedIndex < 2)
          return true;
      else if(this.approvalInfoReviewComponent && this.approvalInfoReviewComponent.agencySiteApprovalData && this.approvalInfoReviewComponent.agencySiteApprovalData.isApproveSite === 34)
          return true;
      else 
          return false;
    }

    get IsDisapproveButtonDisabled() : boolean {

      if(this.tabSelectedIndex < 2)
          return true;
      if(this.approvalInfoReviewComponent && this.approvalInfoReviewComponent.agencySiteApprovalData && this.approvalInfoReviewComponent.agencySiteApprovalData.isApproveSite === 33)
          return true;
      else 
          return false;
    }

    async onSubmit(){
      if(this.agencyInfoReviewComponent) await this.agencyInfoReviewComponent.onSubmit();
      if(this.siteInfoReviewComponent) await this.siteInfoReviewComponent.onSubmit();
      if(this.siteInfoReviewComponent && this.siteInfoReviewComponent.siteContactRequestComponent) await this.siteInfoReviewComponent.siteContactRequestComponent.onSubmit();
      if(this.approvalInfoReviewComponent) await this.approvalInfoReviewComponent.onSubmit();

      if(this.requestReviewRaFormGroup.valid){
        
      }
    }

    getUserRolesInfo()
    {
        this.userDataSub = this.userService.getUserData().subscribe(res => {
          this.userData = res;
          if(this.userData) {
            if(this.userData.siteCategoryType.length > 0) {
              this.useroptions.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
              this.useroptions.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
              this.useroptions.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
              this.useroptions.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
              this.useroptions.optionUserId = this.userData.optionUserId;  
            }
            // if (this.siteSelectedDetail.siteStatus == "InActive")
            // {
            //   this.useroptions.isActive = false;
            //   this.useroptions.isDisableForInactiveSite=true;
            //   this.useroptions.isDisableForHP = true;
            //   if (this.useroptions.isIH == true) {
            //   this.useroptions.isdisableActiveRadiobutton=false;
            //   }
            //   else { 
            //     this.useroptions.isdisableActiveRadiobutton=true;
            //   }
            // }
            // else
            // {
              this.useroptions.isActive = true;
              if (this.useroptions.isIH == true) {
                this.useroptions.isDisableForInactiveSite=false;
                this.useroptions.isDisableForHP = false;
                this.useroptions.isdisableActiveRadiobutton=false;
              }
              else{
                this.useroptions.isDisableForInactiveSite=false;
                this.useroptions.isDisableForHP = true;
                this.useroptions.isdisableActiveRadiobutton=true;
              }
            // }

            }  
          }
        );
      }

    ngOnDestroy() {
      if(this.siteApprovalSub) this.siteApprovalSub.unsubscribe();
      if(this.siteDemographicSub) this.siteDemographicSub.unsubscribe();
      if(this.siteAgreementSub) this.siteAgreementSub.unsubscribe();
      if(this.userDataSub) this.userDataSub.unsubscribe();
      if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
      if(this.siteAdminSvcSub1) this.siteAdminSvcSub1.unsubscribe();
      if(this.siteAdminSvcSub2) this.siteAdminSvcSub2.unsubscribe();
      if(this.agencySiteReviewSaveSub) this.agencySiteReviewSaveSub.unsubscribe();
      if(this.agencySiteReviewSaveSub2) this.agencySiteReviewSaveSub2.unsubscribe();
      if(this.agnySiteReqReviewSvcSub1) this.agnySiteReqReviewSvcSub1.unsubscribe();
      if(this.agnySiteReqReviewSvcSub2) this.agnySiteReqReviewSvcSub2.unsubscribe();
      if(this.evtSvcSub1) this.evtSvcSub1.unsubscribe();
      if(this.evtSvcSub2) this.evtSvcSub2.unsubscribe();
      if(this.evtSvcSub3) this.evtSvcSub3.unsubscribe();
      if(this.evtSvcSub4) this.evtSvcSub4.unsubscribe();
      if(this.evtSvcSub5) this.evtSvcSub5.unsubscribe();
      if(this.siteRequestSvcSub1) this.siteRequestSvcSub1.unsubscribe();
      
    }

}