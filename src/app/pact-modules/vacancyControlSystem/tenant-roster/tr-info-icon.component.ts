import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { CommonService } from 'src/app/services/helper-services/common.service';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-tr-info-icon',
  template: `
    <span class="tr-info-icon">
        {{params.value}}<mat-icon class="tr-mat-info-icon" matTooltip="{{tooltipMessage}}" matTooltipPosition="after" matTooltipClass="vcs-info-tooltip">info</mat-icon>
    </span>
  `,
  styles: [` `]
})
export class TrInfoIconComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;
  tooltipMessage: string;

  // referralHoldReasons: RefGroupDetails[];
  // referralHoldReason = '';

  constructor(
    private commonService: CommonService
  ) { }

  agInit(params: any): void {
    this.params = params;
    // console.log(this.params);
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    this.tooltipMessage = (!params.data.isPlacementReady && params.data.siteLocationType == 233) ?
     `TCOContractStartDate for this Congregate Site is not Valid, Please correct the data` :
     (!params.data.isPlacementReady && params.data.siteLocationType == 234) ?
     `ContractStartDate for this Scatter Site is not Valid, Please correct the data`
     : (params.data.unitID && params.data.unitStatusType == 473 && params.data.offlineReasonType !== 6) ?
      `This Unit is offline, if you would like to update this unit as online, please go to the Agency/Site Setup and update the status of the Unit.` +
      `\n Offline Reason: ` + params.data.offlineReasonTypeDescription +
      `\n Expected Available Date: ` + params.data.expectedAvailableDate
      : (params.data.unitID && params.data.unitStatusType == 473 && params.data.offlineReasonType == 6) ?
        `This Unit is offline, if you would like to update this unit as online, please go to the Agency/Site Setup and update the status of the Unit.` +
        `\n Offline Reason: ` + params.data.offlineOtherSpecify +
        `\n Expected Available Date: ` + params.data.expectedAvailableDate
        : (params.data.unitID && params.data.moveInVerificationStatusType == 519) ?
          'Client\'s Move-In is under Verification process.'
          : (params.data.unitID && params.data.moveOutVerificationStatusType == 522) ?
            'Client\'s Move-Out is under Verification process.'
            : (params.data.unitID && (params.data.isApplicationMatchRequired == null || !params.data.isSideDoorAllowed) && params.data.isVerified == false) ?
              'Direct Referral MoveIn Not allowed / Unit Under Verification.'
              : (params.data.unitID && params.data.unitStatusType == 472 && (params.data.isApplicationMatchRequired == null || !params.data.isSideDoorAllowed)) ?
                'Direct Referral MoveIn Not allowed.'
                : (params.data.unitID && (params.data.isVerified == false)) ?
                  'Unit Under Verification.'
                  : '';

    // Get Refgroup Details
    // const refGroupList = '58';
    // this.commonService.getRefGroupDetails(refGroupList).subscribe(
    //   res => {
    //     const data = res.body as RefGroupDetails[];
    //     console.log(data);
    //     this.referralHoldReasons = data.filter(d => d.refGroupID === 58);
    //     this.referralHoldReasons.forEach(reason => {
    //       if (reason.refGroupDetailID == this.params.data.referralHoldReason) {
    //         this.referralHoldReason = reason.refGroupDetailDescription;
    //         console.log(this.referralHoldReason);
    //       }
    //     });
    //   },
    //   error => {
    //     throw new Error(error.message);
    //   }
    // );
  }

  refresh(): boolean {
    return false;
  }
}
