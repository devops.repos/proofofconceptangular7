import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { C2vService } from '../client-awaiting-placement/c2v.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { IVCSTenantRosterList } from './tenant-roster-interface.model';

@Component({
  selector: 'app-tr-my-tenants-action',
  template: `
    <!-- <mat-icon
      *ngIf="params.data.unitID && (params.data.unitStatusType == 473 ||
      (params.data.unitStatusType !==473 && (params.data.isApplicationMatchRequired == null || !params.data.isSideDoorAllowed)) ||
      (params.data.moveInVerificationStatusType == 519))"
      class="tr-mat-info-icon"
      [matTooltip]="tooltipMessage"
      matTooltipClass="unit-offline-tooltip"
    >
      info
    </mat-icon> -->
    <mat-icon
      *ngIf="(params.data.unitStatusType !== 473 &&
              params.data.moveInVerificationStatusType !== 519 &&
              params.data.moveOutVerificationStatusType !== 522 &&
              (params.data.isVerified == true || params.data.isVerified == null)
             ) &&
              (params.data.isSideDoorAllowed || params.data.moveInDate) &&
              params.data.isPlacementReady
              || !params.data.unitID"
      class="tr-my-tenants-icon"
      color="warn"
      [matMenuTriggerFor]="trMyTenantsAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #trMyTenantsAction="matMenu">
      <button mat-menu-item
        *ngIf="!params.data.unitID && params.data.moveInDate && params.data.siteTrackedType == 33"
        (click)="onAssignTenantToUnitClick()">
        <mat-icon color="primary">assignment</mat-icon>Assign Tenant to Unit
      </button>
      <button mat-menu-item
        *ngIf="params.data.unitStatusType == 474 && (params.data.moveInVerificationStatusType == 520 || params.data.moveInVerificationStatusType == 758) && params.data.moveOutVerificationStatusType !== 522"
        (click)="onUpdateTenantProfileClick()">
        <mat-icon color="primary">assignment</mat-icon>Update Tenant Profile
      </button>
      <button mat-menu-item
      *ngIf="(params.data.unitStatusType == 474 && (params.data.moveInVerificationStatusType == 520 || params.data.moveInVerificationStatusType == 758) && params.data.moveOutVerificationStatusType !== 522) || (!params.data.unitID && params.data.moveInDate)"
        (click)="onMoveOutTenantClick()">
        <mat-icon color="primary">assignment</mat-icon>Moveout Tenant
      </button>
      <button mat-menu-item
      *ngIf="params.data.unitStatusType == 472 && params.data.isApplicationMatchRequired !== null  && params.data.isSideDoorAllowed && (params.data.isVerified || params.data.isVerified == null)"
        (click)="onMoveInTenantClick()">
        <mat-icon color="primary">assignment</mat-icon>Move-In Tenant
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .tr-my-tenants-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class TRMyTenantsActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  trSelected: IVCSTenantRosterList;
  // vcuAdminVerificationStatus = true;

  // tooltipMessage: string;

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private c2vService: C2vService,
    private router: Router,
  ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    // this.tooltipMessage = (params.data.unitID && params.data.unitStatusType == 473 && params.data.offlineReasonType !== 478) ?
    //                       `This Unit is offline, if you would like to update this unit as online, please go to the Agency/Site Setup and update the status of the Unit.` +
    //                       `\n Offline Reason: ` + params.data.offlineReasonTypeDescription +
    //                       `\n Expected Available Date: ` + params.data.expectedAvailableDate
    //                       : (params.data.unitID && params.data.unitStatusType == 473 && params.data.offlineReasonType == 478) ?
    //                       `This Unit is offline, if you would like to update this unit as online, please go to the Agency/Site Setup and update the status of the Unit.` +
    //                       `\n Offline Reason: ` + params.data.offlineOtherSpecify +
    //                       `\n Expected Available Date: ` + params.data.expectedAvailableDate
    //                       : (params.data.unitID && params.data.unitStatusType !==473 && (params.data.isApplicationMatchRequired == null || !params.data.isSideDoorAllowed)) ?
    //                       'Sidedoor MoveIn is not allowed for this Unit'
    //                       : (params.data.unitID && params.data.moveInVerificationStatusType == 519) ?
    //                       'Client\'s Move-In is under Verification process.'
    //                       : '';
  }

  onAssignTenantToUnitClick() {
    this.trSelected = this.params.data;
    this.params.context.componentParent.onAssignTenantToUnitClickParent(this.trSelected);
   }

  onUpdateTenantProfileClick() {
    this.trSelected = this.params.data;
    this.params.context.componentParent.onUpdateTenantProfileClickParent(this.trSelected);
   }

  onMoveOutTenantClick() {
    this.trSelected = this.params.data;
    this.params.context.componentParent.onMoveOutTenantClickParent(this.trSelected);
   }

  onMoveInTenantClick() {
    this.trSelected = this.params.data;
    this.params.context.componentParent.onMoveInTenantClickParent(this.trSelected);
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() { }
}
