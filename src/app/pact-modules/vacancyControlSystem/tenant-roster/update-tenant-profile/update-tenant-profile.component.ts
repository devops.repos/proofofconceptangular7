import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TenantRosterService } from '../tenant-roster.service';
import { IVCSTenantRosterList, IVCSHpAgenciesWithTrackedSites, IVCSUpdateTenantProfile, IVCSAgencies } from '../tenant-roster-interface.model';
import { Router } from '@angular/router';
import { MatTabChangeEvent } from '@angular/material';
import { IPlacementOutcome, IVCSIncomeSourceType } from '../../referral-roster/referral-roster-interface.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { TadService } from '../../tad-submission/tad.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-tenant-profile',
  templateUrl: './update-tenant-profile.component.html',
  styleUrls: ['./update-tenant-profile.component.scss']
})
export class UpdateTenantProfileComponent implements OnInit, OnDestroy {

  referralType = 0;   // 525 = Regular Referral, 526 = Supportive Housing Sidedoor Referral, 610 = Non-Supportive Housing Sidedoor Referral
  panelExpanded = true;
  selectedTab = 0;

  trSelected: IVCSTenantRosterList;
  existingPlacementOutcome: IPlacementOutcome = {
    vcsReferralID: 0,
    interviewDate: '',
    interviewTime: '',
    interviewLocation: '',
    wasInterviewConducted: 0,
    interviewOutcomeType: 0,
    placementOutcomeType: 0,
    expectedMoveInDate: '',
    expectedUnitID: 0,
    initialMoveInDate: '',
    moveInDate: '',
    unitID: 0,
    unitName: '',
    initialClientRentalContribution: 0,
    initialIncomeSourceList: [],
    initialIncomeSourceOtherSpecify: '',
    clientRentalContribution: 0,
    incomeSourceList: [],
    incomeSourceOtherSpecify: '',
    reasonType: 0,
    comments: '',
    referralReceivedType: 0,
    referralReceivedDate: '',
    referralReceivedOtherSpecify: '',
    pactApplicationID: 0,
    vcsID: '',
    clientID: 0,
    clientSourceType: 0,
    referringAgencyID: 0,
    referringAgencyNo: '',
    referringAgencyName: '',
    referringSiteID: 0,
    hpAgencyID: 0,
    hpSiteID: 0,
    siteAgreementPopulationID: 0,
    referralGroupGUID: '',
    referralType: 0,
    isScheduleMandatory: 0,
    unitType: 0,
    matchPercent: 0,
    referralStatusType: 0,
    cocType: 0,
    withdrawnReasonType: 0,
    referralClosedComment: '',
    isEligibilityCriteriaMatched: false,
    isApprovalActive: false,
    placementMatchCriteriaList: [],
    firstName: '',
    lastName: '',
    ssn: '',
    dob: '',
    gender: '',
    referralDate: '',
    eligibility: '',
    placementCriteria: '',
    svaPrioritization: '',
    serviceNeeds: '',
    createdByName: '',
    createdDate: '',
    updatedByName: '',
    updatedDate: '',
  }

  interviewOutcomeGroup: FormGroup;
  updateTenantProfileGroup: FormGroup;

  selectedIncomeSourceForForm: number[] = [];
  selectedInitialIncomeSourceForForm: number[] = [];
  selectedIncomeSource: IVCSIncomeSourceType[] = [];
  isThereOtherIncomeSource = false;
  wasThereInitialOtherIncomeSource = false;
  isThereOtherIncomeSourceForUpdate = false;

  incomeSourceUpdated = false;
  isIncomeSourceOtherSpecifyUpdated = false;
  isClientRentalContributionUpdated = false;

  interviewOutcome: RefGroupDetails[];
  placementOutcome: RefGroupDetails[];
  wasInterviewConducted: RefGroupDetails[];
  interviewNotConductedReasons: RefGroupDetails[];
  hpDidNotAcceptClientReasons: RefGroupDetails[];
  clientDidNotAcceptHousingReasons: RefGroupDetails[];
  interviewOutcomePendingReasons: RefGroupDetails[];
  incomeSource: RefGroupDetails[];

  allActiveAgencies: IVCSHpAgenciesWithTrackedSites[];

  isTadWorkflow = false;

  commonServiceSub: Subscription;
  trSelectedSub: Subscription;
  raHpAgenciesSub: Subscription;
  existingPlacementOutcomeSub: Subscription;
  updateTenantProfileSub: Subscription;
  istadWorkflowSub: Subscription;

  constructor(
    private tenantRosterService: TenantRosterService,
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private toastrService: ToastrService,
    private router: Router,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService
  ) {
    this.interviewOutcomeGroup = this.formBuilder.group({
      interviewDateCtrl: ['', Validators.required],
      interviewTimeCtrl: [-1, Validators.required],
      // wasInterviewConductedCtrl: [-1, Validators.required],
      wasInterviewConductedCtrl: [{ value: -1, disabled: true }, Validators.required],
      // referralReceivedFromCtrl: [-1, Validators.required],
      referralReceivedFromCtrl: [{ value: -1, disabled: true }, Validators.required],
      specifyReferralReceivedCtrl: ['', Validators.required],
      referralReceivedDateCtrl: ['', Validators.required],
      // reasonCtrl: [-1, Validators.required],
      reasonCtrl: [{ value: -1, disabled: true }, Validators.required],
      commentCtrl: [''],
      // interviewOutcomeCtrl: [-1, Validators.required],
      interviewOutcomeCtrl: [{ value: -1, disabled: true }, Validators.required],
      // placementOutcomeCtrl: [-1, Validators.required],
      placementOutcomeCtrl: [{ value: -1, disabled: true }, Validators.required],
      expectedMoveInDateCtrl: ['', Validators.required],
      moveInDateCtrl: ['', Validators.required],
      unitNumberCtrl: [-1, Validators.required],
      clientRentalContributionCtrl: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(1)]],
      // incomeSourceCtrl: [this.selectedIncomeSource, Validators.required],
      incomeSourceCtrl: [{ value: this.selectedIncomeSource, disabled: true }, Validators.required],
      specifyIncomeSourceCtrl: ['', Validators.required]
    });

    this.updateTenantProfileGroup = this.formBuilder.group({
      updateClientRentalContributionCtrl: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(1)]],
      updateIncomeSourceCtrl: [this.selectedIncomeSource, Validators.required],
      updateSpecifyIncomeSourceCtrl: ['', Validators.required]
    });
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '7,63,64,65,66,67,68,69';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.interviewOutcome = data.filter(d => d.refGroupID === 63);
        this.placementOutcome = data.filter(d => d.refGroupID === 64);
        this.wasInterviewConducted = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
        this.interviewNotConductedReasons = data.filter(d => d.refGroupID === 65);
        this.hpDidNotAcceptClientReasons = data.filter(d => d.refGroupID === 66);
        this.clientDidNotAcceptHousingReasons = data.filter(d => d.refGroupID === 67);
        this.interviewOutcomePendingReasons = data.filter(d => d.refGroupID === 68);
        this.incomeSource = data.filter(d => d.refGroupID === 69);
      },
      error => {
        throw new Error(error.message);
      }
    );

    /* Required for sideDoor MoveIn and MoveOut */
    /** Get all the active HP Agencies List with only tracked Sites for the ReferralReceivedFrom dropdown */
    // this.raHpAgenciesSub = this.tenantRosterService.getVCSHpAgenciesWithTrackedSites().subscribe((agencies: IVCSHpAgenciesWithTrackedSites[]) => {
    //   if (agencies) {
    //     this.allActiveAgencies = agencies;
    //   }
    // });
    this.raHpAgenciesSub = this.tenantRosterService.getVCSAllActiveAgencies().subscribe((agencies: IVCSAgencies[]) => {
      if (agencies) {
        this.allActiveAgencies = agencies;
      }
    });

    /* Land User Default on UpdateTenantProfile Tab */
    this.selectedTab = 1;

    /** Get the selected TenantRoster Details from the TenantRosterService */
    this.trSelectedSub = this.tenantRosterService.getTenantRosterSelected().subscribe(tr => {
      if (tr) {
        this.trSelected = tr;
        this.referralType = this.trSelected.referralType;
        // console.log('trSelected: ', this.trSelected);

        /** Get the PlacementOutcome if available for the selected TenantRoster */
        if (this.trSelected.vcsPlacementMoveInID > 0) {
          // console.log('trSelected.vcsPlacementMoveInID: ', this.trSelected.vcsPlacementMoveInID);
          /** Get the PlacementOutcome if available for the selected TenantRoster */
          this.existingPlacementOutcomeSub = this.tenantRosterService.getVCSPlacementOutcomeByVCSPlacementMoveInID(this.trSelected.vcsPlacementMoveInID).subscribe((po: IPlacementOutcome) => {
            if (po) {
              // console.log('ExistingPlacementOutcome: ', po);
              this.existingPlacementOutcome = po;
              this.interviewOutcomeGroup.disable();
              po.incomeSourceList.forEach((is, index) => {
                this.selectedIncomeSourceForForm.push(is.incomeSourceType);
                if (is.incomeSourceType == 566) {
                  this.isThereOtherIncomeSource = true;
                }
                if (index == po.incomeSourceList.length - 1) {
                  this.updateTenantProfileGroup.get('updateIncomeSourceCtrl').setValue(this.selectedIncomeSourceForForm);
                }
              });
              po.initialIncomeSourceList.forEach(iis => {
                this.selectedInitialIncomeSourceForForm.push(iis.incomeSourceType);
                if (iis.incomeSourceType == 566) {
                  this.wasThereInitialOtherIncomeSource = true;
                }
              });

              this.interviewOutcomeGroup.get('wasInterviewConductedCtrl').setValue(po.wasInterviewConducted);
              this.interviewOutcomeGroup.get('interviewOutcomeCtrl').setValue(po.interviewOutcomeType);
              this.interviewOutcomeGroup.get('placementOutcomeCtrl').setValue(po.placementOutcomeType ? po.placementOutcomeType : -1);
              this.interviewOutcomeGroup.get('referralReceivedFromCtrl').setValue(po.referralReceivedType);
              // this.interviewOutcomeGroup.get('specifyReferralReceivedCtrl').setValue(po.referralReceivedOtherSpecify);
              // this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue(po.initialClientRentalContribution);
              this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue(this.selectedInitialIncomeSourceForForm);
              // this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(po.initialIncomeSourceOtherSpecify);
              // this.interviewOutcomeGroup.get('moveInDateCtrl').setValue(moveInDate);
              this.interviewOutcomeGroup.get('reasonCtrl').setValue(po.reasonType);
              // this.interviewOutcomeGroup.get('commentCtrl').setValue(po.comments);

              this.updateTenantProfileGroup.get('updateClientRentalContributionCtrl').setValue(po.clientRentalContribution);

              this.updateTenantProfileGroup.get('updateSpecifyIncomeSourceCtrl').setValue(po.incomeSourceOtherSpecify);

              // if (po.placementOutcomeType === 545) {
              //   // 545 = Pending Approval
              //   this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(po.expectedUnitID);
              // } else if (po.placementOutcomeType === 546) {
              //   this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(po.unitID);
              // } else {
              //   this.interviewOutcomeGroup.get('unitNumberCtrl').setValue(-1);
              // }
              // if (this.placementOutcomeFormEditable) {
              //   this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue(po.clientRentalContribution);
              //   this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue(this.selectedIncomeSourceForForm);
              //   this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(po.incomeSourceOtherSpecify);
              // } else {
              //   this.interviewOutcomeGroup.get('clientRentalContributionCtrl').setValue(po.initialClientRentalContribution);
              //   this.interviewOutcomeGroup.get('incomeSourceCtrl').setValue(this.selectedInitialIncomeSourceForForm);
              //   this.interviewOutcomeGroup.get('specifyIncomeSourceCtrl').setValue(po.initialIncomeSourceOtherSpecify);
              // }
            } else {
              this.toastrService.error('no existing Placement');
            }
          });
        }
      } else {
        this.router.navigate(['/vcs/tenant-roster']);
      }
    });

    /* Check if the workFlow is coming from TAD, if so, on submit or exit, redirect back to TAD workflow */
    this.istadWorkflowSub = this.tadService.getIsTadWorkflow().subscribe(flag => {
      if (flag) {
        this.isTadWorkflow = flag;
      }
    });
  }

  onExpansionPanelToggle() {
    this.panelExpanded = !this.panelExpanded;
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;
  }

  onUpdateMultiIncomeSourceSelected(event) {
    if (!event) {
      // console.log('event: ', event);
      const valueSelected = this.updateTenantProfileGroup.get('updateIncomeSourceCtrl').value;
      this.selectedIncomeSource = [];
      this.incomeSourceUpdated = false;
      let v1: IVCSIncomeSourceType;
      let c1: number[] = [];
      let c2: number[] = [];
      let missing: number[] = [];
      /* Get existing income source */
      if (this.existingPlacementOutcome.incomeSourceList.length > 0) {
        this.existingPlacementOutcome.incomeSourceList.forEach(is => {
          c1.push(is.incomeSourceType);
        });
      }
      /* Get new selected income source */
      if (valueSelected.length > 0) {
        valueSelected.forEach(vs => {
          c2.push(vs);
        });
      }
      if (c1.length > 0) {
        if (c2.length > 0 && c2.length > c1.length) {
          /* checking if user selected any new Income source */
          missing = c2.filter(item => c1.indexOf(item) < 0);
          if (missing.length > 0) {
            /* new income source selected */
            this.incomeSourceUpdated = true;
            valueSelected.forEach(vs => {
              v1 = { vcsIncomeSourceID: 0, incomeSourceType: vs }
              this.selectedIncomeSource.push(v1);
            });
          } else {
            this.incomeSourceUpdated = false;
          }
        }
        if (c2.length > 0 && c2.length <= c1.length) {
          /* checking if user unSelected any existing income source */
          missing = c1.filter(item => c2.indexOf(item) < 0);
          if (missing.length > 0) {
            /* existing income source unselected */
            /* Updated the income source */
            this.incomeSourceUpdated = true;
            valueSelected.forEach(vs => {
              v1 = { vcsIncomeSourceID: 0, incomeSourceType: vs }
              this.selectedIncomeSource.push(v1);
            });
            /* If existing Other income source type is unselected reset the specify comment value */
            missing.forEach(mis => {
              if (mis == 566) {
                this.isThereOtherIncomeSource = false;
                this.updateTenantProfileGroup.get('updateSpecifyIncomeSourceCtrl').setValue('');
              }
            })
          } else {
            /* Nothing Updated */
            this.incomeSourceUpdated = false;
            valueSelected.forEach(vs => {
              if (vs == 566) {
                /* Resetting the value for other specify if exist */
                this.isThereOtherIncomeSource = true;
                this.updateTenantProfileGroup.get('updateSpecifyIncomeSourceCtrl').setValue(this.existingPlacementOutcome.incomeSourceOtherSpecify);
              }
            });

          }
        }
      } else {
        // all new
        this.incomeSourceUpdated = true;
        valueSelected.forEach(vs => {
          v1 = { vcsIncomeSourceID: 0, incomeSourceType: vs }
          this.selectedIncomeSource.push(v1);
        });
      }
      if (this.selectedIncomeSource.length > 0) {
        this.selectedIncomeSource.forEach(el => {
          if (el.incomeSourceType == 566) {
            this.isThereOtherIncomeSource = true;
          } else {
            this.isThereOtherIncomeSource = false;
            this.updateTenantProfileGroup.get('updateSpecifyIncomeSourceCtrl').setValue('');
          }
        });
      }
    }
  }

  onUpdate() {
    const clientRentalContribution = this.updateTenantProfileGroup.get('updateClientRentalContributionCtrl').value;
    // const incomeSrc = this.updateTenantProfileGroup.get('updateIncomeSourceCtrl').value;

    const incomeSrc = this.selectedIncomeSource;
    const specifyComment = this.updateTenantProfileGroup.get('updateSpecifyIncomeSourceCtrl').value;

    // console.log('selectedIncomeSource: ', this.selectedIncomeSource);
    // console.log('updateSpecifyIncomeSourceCtrl: ', specifyComment);
    // console.log('incomeSourceUpdated: ', this.incomeSourceUpdated);

    if (this.existingPlacementOutcome.incomeSourceOtherSpecify !== specifyComment) {
      this.isIncomeSourceOtherSpecifyUpdated = true;
    } else {
      this.isIncomeSourceOtherSpecifyUpdated = false;
    }
    if (this.existingPlacementOutcome.clientRentalContribution !== clientRentalContribution) {
      this.isClientRentalContributionUpdated = true;
    } else {
      this.isClientRentalContributionUpdated = false;
    }

    const updateTenantProfileData: IVCSUpdateTenantProfile = {
      VCSPlacementMoveInID: this.trSelected.vcsPlacementMoveInID,
      ClientRentalContribution: clientRentalContribution,
      IncomeSourceList: incomeSrc,
      IncomeSourceOtherSpecify: specifyComment,
      IsIncomeSourceUpdated: this.incomeSourceUpdated,
      IsIncomeSourceOtherSpecifyUpdated: this.isIncomeSourceOtherSpecifyUpdated,
      IsClientRentalContributionUpdated: this.isClientRentalContributionUpdated
    }

    if (clientRentalContribution === '') {
      this.toastrService.error('Please provide the client rental contribution');
    } else if (incomeSrc === []) {
      this.toastrService.error('Please select the Income Source');
    } else if (this.isThereOtherIncomeSource && specifyComment === '') {
      this.toastrService.error('Please write some Income Source comments');
    } else if (!this.incomeSourceUpdated && !this.isIncomeSourceOtherSpecifyUpdated && !this.isClientRentalContributionUpdated) {
      this.toastrService.error('No updates to save');
    } else {
      // Update the Tenant Profile
      this.vcsUpdateTenantProfile(updateTenantProfileData);
    }
  }

  vcsUpdateTenantProfile(inputData: IVCSUpdateTenantProfile) {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to Update the Tenant Profile ? `;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.updateTenantProfileSub = this.tenantRosterService.VCSUpdateTenantProfile(inputData).subscribe(rslt => {
          if (rslt >= 0) {
            this.toastrService.success('Tenant Profile Updated successfully.');
            if (this.isTadWorkflow) {
              this.router.navigate(['/vcs/tad/tad-submission']);
            } else {
              this.router.navigate(['/vcs/tenant-roster']);
            }
          }
        });
      },
      negativeResponse => { }
    );

  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        if (this.isTadWorkflow) {
          this.router.navigate(['/vcs/tad/tad-submission']);
        } else {
          this.router.navigate(['/vcs/tenant-roster']);
        }
      },
      negativeResponse => { }
    );

  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.raHpAgenciesSub) {
      this.raHpAgenciesSub.unsubscribe();
    }
    if (this.existingPlacementOutcomeSub) {
      this.existingPlacementOutcomeSub.unsubscribe();
    }
    if (this.updateTenantProfileSub) {
      this.updateTenantProfileSub.unsubscribe();
    }
    if (this.istadWorkflowSub) {
      this.istadWorkflowSub.unsubscribe();
    }
  }

}
