import { Component, OnInit, OnDestroy } from '@angular/core';
import { IVCSTenantRosterList, IVCSUnitOfflineReason, IVCSHpAgenciesWithTrackedSites, IVCSMoveOutTenant } from '../tenant-roster-interface.model';
import { Subscription } from 'rxjs';
import { TenantRosterService } from '../tenant-roster.service';
import { Router } from '@angular/router';
import { IPlacementOutcome } from '../../referral-roster/referral-roster-interface.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { MatTabChangeEvent } from '@angular/material';
import { TadService } from '../../tad-submission/tad.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-move-out-tenant',
  templateUrl: './move-out-tenant.component.html',
  styleUrls: ['./move-out-tenant.component.scss'],
  providers: [DatePipe]
})
export class MoveOutTenantComponent implements OnInit, OnDestroy {

  referralType = 0; //525 = Regular Referral, 526 = SideDoor Referral
  // trSelected: IVCSTenantRosterList;
  // trSelectedSub: Subscription;

  isTadWorkflow = false;
  panelExpanded = true;
  selectedTab = 0;
  existingPlacementOutcome: IPlacementOutcome;
  moveOutTenantGroup: FormGroup;

  moveOutReasonType: RefGroupDetails[];
  moveOutLocationType: RefGroupDetails[];
  moveOutType: RefGroupDetails[];
  unitStatusType: RefGroupDetails[];
  unitOfflineReasons: IVCSUnitOfflineReason[];
  expectedAvailableDateTimeFrame = 0;

  hpAgenciesWithTrackedSites: IVCSHpAgenciesWithTrackedSites[];
  trSelected: IVCSTenantRosterList;

  commonServiceSub: Subscription;
  raHpAgenciesSub: Subscription;
  trSelectedSub: Subscription;
  unitOfflineReasonsSub: Subscription;
  existingPlacementOutcomeSub: Subscription;
  moveOutTenantSub: Subscription;
  reasonMovedCtrlSub: Subscription;
  locationMoveToCtrlSub: Subscription;
  agencyMovedToCtrlSub: Subscription;
  unitStatusTypeCtrlSub: Subscription;
  reasonOfflineCtrlSub: Subscription;
  istadWorkflowSub: Subscription;

  constructor(
    private tenantRosterService: TenantRosterService,
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private toastrService: ToastrService,
    private datePipe: DatePipe,
    private router: Router,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService
  ) {
    this.moveOutTenantGroup = this.formBuilder.group({
      moveOutDateCtrl: ['', Validators.required],
      reasonMovedCtrl: [-1, Validators.required],
      locationMoveToCtrl: [-1, Validators.required],
      moveOutTypeCtrl: [-1, Validators.required],
      agencyMovedToCtrl: [-1, Validators.required],
      agencyMovedToOtherSpecifyCtrl: ['', Validators.required],
      unitStatusTypeCtrl: [-1, Validators.required],
      reasonOfflineCtrl: [-1, Validators.required],
      reasonOfflineOtherSpecifyCtrl: ['', Validators.required],
      expectedAvailableDateCtrl: ['', Validators.required],
      moveOutCommentsCtrl: ['']

    });
  }

  ngOnInit() {
    const refGroupList = '52,80,81,82';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.moveOutType = data.filter(d => d.refGroupID === 80);
        this.moveOutLocationType = data.filter(d => d.refGroupID === 81);
        this.moveOutReasonType = data.filter(d => d.refGroupID === 82);
        this.unitStatusType = data.filter(d => d.refGroupID === 52 && (d.refGroupDetailID === 472 || d.refGroupDetailID === 473));
      },
      error => {
        throw new Error(error.message);
      }
    );

    /** Get all the active HP Agencies List with only tracked Sites for the ReferralReceivedFrom dropdown */
    this.raHpAgenciesSub = this.tenantRosterService.getVCSHpAgenciesWithTrackedSites().subscribe((agencies: IVCSHpAgenciesWithTrackedSites[]) => {
      if (agencies) {
        this.hpAgenciesWithTrackedSites = agencies;
      }
    });

    /** Get the selected TenantRoster Details from the TenantRosterService */
    this.trSelectedSub = this.tenantRosterService.getTenantRosterSelected().subscribe(tr => {
      if (tr) {
        this.trSelected = tr;
        this.referralType = this.trSelected.referralType;
        // console.log('update tenant || moveOut trSelected: ', this.trSelected);
        /* Get conditional Unit offline reasons dependent to Unit's contracting Agency Type */
        this.unitOfflineReasonsSub = this.tenantRosterService.VCSGetUnitOfflineReasons().subscribe((res: IVCSUnitOfflineReason[]) => {
          if (res) {
            if (this.trSelected.contractingAgencyType && this.trSelected.contractingAgencyType == 488) {
              this.unitOfflineReasons = res.filter(d => (d.unitOfflineReasonID == 1 || d.unitOfflineReasonID == 2 || d.unitOfflineReasonID == 3 || d.unitOfflineReasonID == 4 || d.unitOfflineReasonID == 5 || d.unitOfflineReasonID == 6));
            } else {
              this.unitOfflineReasons = res.filter(d => (d.unitOfflineReasonID == 7 || d.unitOfflineReasonID == 8 || d.unitOfflineReasonID == 9 || d.unitOfflineReasonID == 6));
            }
          }
        });
        /** Get the PlacementOutcome if available for the selected TenantRoster */
        if (this.trSelected.vcsPlacementMoveInID > 0) {
          /** Get the PlacementOutcome if available for the selected TenantRoster */
          this.existingPlacementOutcomeSub = this.tenantRosterService.getVCSPlacementOutcomeByVCSPlacementMoveInID(this.trSelected.vcsPlacementMoveInID).subscribe((po: IPlacementOutcome) => {
            if (po) {
              // console.log('Sidedoor ExistingPlacementOutcome: ', po);
              this.existingPlacementOutcome = po;
            }
          });
        }
      } else {
        this.router.navigate(['/vcs/tenant-roster']);
      }
    });

    /* Resetting the MoveOut form value based on runtime value selection */
    this.reasonMovedCtrlSub = this.moveOutTenantGroup.get('reasonMovedCtrl').valueChanges.subscribe(res => {
      if (res == 639) {
        // 639 = deceased
        this.moveOutTenantGroup.get('locationMoveToCtrl').setValue(634);  // 634 = Deceased
        this.moveOutTenantGroup.get('locationMoveToCtrl').disable();
        this.moveOutTenantGroup.get('moveOutTypeCtrl').setValue(617);   // 616 = planned, 617 = unplanned
        this.moveOutTenantGroup.get('moveOutTypeCtrl').disable();
      } else if (res == 643) {
        // 643 = HOSPITALIZED
        this.moveOutTenantGroup.get('locationMoveToCtrl').setValue(624);  // 624 = HOSPITAL
        this.moveOutTenantGroup.get('locationMoveToCtrl').disable();
        this.moveOutTenantGroup.get('moveOutTypeCtrl').setValue(617);   // 616 = planned, 617 = unplanned
        this.moveOutTenantGroup.get('moveOutTypeCtrl').disable();
      } else {
        this.moveOutTenantGroup.get('locationMoveToCtrl').setValue(-1);
        this.moveOutTenantGroup.get('locationMoveToCtrl').enable();
        this.moveOutTenantGroup.get('moveOutTypeCtrl').setValue(-1);
        this.moveOutTenantGroup.get('moveOutTypeCtrl').enable();
      }
    });
    this.locationMoveToCtrlSub = this.moveOutTenantGroup.get('locationMoveToCtrl').valueChanges.subscribe(res => {
      if (res == 619 || res == 623 || res == 625 || res == 626 || res == 628 || res == 631 || res == 632 || res == 635 || res == 636) {
        this.moveOutTenantGroup.get('moveOutTypeCtrl').setValue(617); // 616 = planned, 617 = unplanned
        this.moveOutTenantGroup.get('moveOutTypeCtrl').disable();
        this.moveOutTenantGroup.get('agencyMovedToCtrl').setValue(-1);
      } else if (res == 634 && this.moveOutTenantGroup.get('reasonMovedCtrl').value !== 639) {
        // 634 = DECEASED
        this.moveOutTenantGroup.get('moveOutTypeCtrl').setValue(617);
        this.moveOutTenantGroup.get('moveOutTypeCtrl').disable();
        this.moveOutTenantGroup.get('reasonMovedCtrl').setValue(639);
      } else if (res == 622) {
        // 622 = SUPPORTED HOUSING
        this.moveOutTenantGroup.get('moveOutTypeCtrl').setValue(616);
        this.moveOutTenantGroup.get('moveOutTypeCtrl').disable();
      } else {
        this.moveOutTenantGroup.get('moveOutTypeCtrl').setValue(-1);
        this.moveOutTenantGroup.get('moveOutTypeCtrl').enable();
        this.moveOutTenantGroup.get('agencyMovedToCtrl').setValue(-1);
      }
    });
    this.agencyMovedToCtrlSub = this.moveOutTenantGroup.get('agencyMovedToCtrl').valueChanges.subscribe(res => {
      if (res !== -2) {
        this.moveOutTenantGroup.get('agencyMovedToOtherSpecifyCtrl').setValue('');
      }
    });
    this.unitStatusTypeCtrlSub = this.moveOutTenantGroup.get('unitStatusTypeCtrl').valueChanges.subscribe(res => {
      if (res !== 473) {
        // 473 = Unit Offline
        this.moveOutTenantGroup.get('reasonOfflineCtrl').setValue(-1);
        this.moveOutTenantGroup.get('reasonOfflineOtherSpecifyCtrl').setValue('');
        this.moveOutTenantGroup.get('expectedAvailableDateCtrl').setValue('');
      }
    });
    this.reasonOfflineCtrlSub = this.moveOutTenantGroup.get('reasonOfflineCtrl').valueChanges.subscribe(res => {
      if (res == 0 || res !== 6) {
        // 6 = other
        this.moveOutTenantGroup.get('reasonOfflineOtherSpecifyCtrl').setValue('');
      }
      /* Get the UnitOffline Time frame to validate the expected Available date */
      this.expectedAvailableDateTimeFrame = 0;
      this.unitOfflineReasons.forEach(rsl => {
        if (rsl.unitOfflineReasonID == res && rsl.timeFrame > 0) {
          this.expectedAvailableDateTimeFrame = rsl.timeFrame;
        }
      })
    });

    /* Check if the workFlow is coming from TAD, if so, on submit or exit, redirect back to TAD workflow */
    this.istadWorkflowSub = this.tadService.getIsTadWorkflow().subscribe(flag => {
      if (flag) {
        this.isTadWorkflow = flag;
      }
    });
  }

  onExpansionPanelToggle() {
    this.panelExpanded = !this.panelExpanded;
  }
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;
  }

  onMoveOutDateSelected() {
    if (this.datePipe.transform(this.moveOutTenantGroup.get('moveOutDateCtrl').value, 'MM/dd/yyyy')) {
      let moveInDate;
      const sysDate = new Date();
      const moveOutDatePicked = new Date(this.moveOutTenantGroup.get('moveOutDateCtrl').value);
      if (this.trSelected.moveInDate) {
        moveInDate = new Date(this.trSelected.moveInDate);
        if (moveOutDatePicked < moveInDate) {
          this.toastrService.error('Move-Out date cannot be before MoveIn Date ' + (moveInDate.getMonth() + 1) + '/' + moveInDate.getDate() + '/' + moveInDate.getFullYear());
        }
      }
      if (moveOutDatePicked > sysDate) {
        this.toastrService.error('Move-Out date cannot be a future date');
      }
    }
  }

  onExpectedAvailableDateSelected() {
    if (this.datePipe.transform(this.moveOutTenantGroup.get('expectedAvailableDateCtrl').value, 'MM/dd/yyyy')) {
      const expectedAvailableDate = new Date(this.moveOutTenantGroup.get('expectedAvailableDateCtrl').value);
      // const sysDate = new Date();
      // const yyyy = sysDate.getFullYear();
      // const mm = sysDate.getMonth();
      // const dd = sysDate.getDate();
      var moveOutDate;
      if (this.datePipe.transform(this.moveOutTenantGroup.get('moveOutDateCtrl').value, 'MM/dd/yyyy')) {
        moveOutDate = new Date(this.moveOutTenantGroup.get('moveOutDateCtrl').value);
        const yyyy = moveOutDate.getFullYear();
        const mm = moveOutDate.getMonth();
        const dd = moveOutDate.getDate() - 1;
        if (this.expectedAvailableDateTimeFrame > 0) {
          const validDateRange = new Date(yyyy, mm, dd + this.expectedAvailableDateTimeFrame);
          if (expectedAvailableDate < moveOutDate) {
            this.toastrService.error('Expected Available Date must be on or After MoveOut Date');
          } else if (validDateRange < expectedAvailableDate) {
            this.toastrService.error('Expected Available Date must be on or before ' + (validDateRange.getMonth() + 1) + '/' + validDateRange.getDate() + '/' + validDateRange.getFullYear());
          }
        } else if (expectedAvailableDate < moveOutDate) {
          this.toastrService.error('Expected Available Date must be on or After MoveOut Date');
        }
      }


    }
  }

  onMoveOut() {
    let moveInDate;
    const sysDate = new Date();
    if (this.trSelected.moveInDate) {
      moveInDate = new Date(this.trSelected.moveInDate);
    }
    const moveOutDate = this.moveOutTenantGroup.get('moveOutDateCtrl').value ? new Date(this.moveOutTenantGroup.get('moveOutDateCtrl').value) : this.moveOutTenantGroup.get('moveOutDateCtrl').value;
    const reasonMoved = this.moveOutTenantGroup.get('reasonMovedCtrl').value;
    const locationMoveTo = this.moveOutTenantGroup.get('locationMoveToCtrl').value;
    const moveOutType = this.moveOutTenantGroup.get('moveOutTypeCtrl').value;
    const agencyMovedTo = this.moveOutTenantGroup.get('agencyMovedToCtrl').value;
    const agencyMovedToOtherSpecify = this.moveOutTenantGroup.get('agencyMovedToOtherSpecifyCtrl').value;
    const unitStatus = this.moveOutTenantGroup.get('unitStatusTypeCtrl').value;
    const reasonOffline = this.moveOutTenantGroup.get('reasonOfflineCtrl').value;
    const reasonOfflineOtherSpecify = this.moveOutTenantGroup.get('reasonOfflineOtherSpecifyCtrl').value;
    const expectedAvailableDate = this.moveOutTenantGroup.get('expectedAvailableDateCtrl').value ? new Date(this.moveOutTenantGroup.get('expectedAvailableDateCtrl').value) : this.moveOutTenantGroup.get('expectedAvailableDateCtrl').value;
    const moveOutcomments = this.moveOutTenantGroup.get('moveOutCommentsCtrl').value;

    const moveOutTenantData: IVCSMoveOutTenant = {
      VCSPlacementMoveInID: this.trSelected.vcsPlacementMoveInID,
      MoveOutDate: moveOutDate,
      MoveOutReasonType: (reasonMoved === 0 || reasonMoved === -1) ? null : reasonMoved,
      MoveOutLocationType: (locationMoveTo === 0 || locationMoveTo === -1) ? null : locationMoveTo,
      MoveOutType: (moveOutType === 0 || moveOutType === -1) ? null : moveOutType,
      MoveToAgencyID: (agencyMovedTo === 0 || agencyMovedTo === -1) ? null : agencyMovedTo,
      MoveToAgencyOtherSpecify: agencyMovedToOtherSpecify,
      UnitStatusType: (unitStatus === 0 || unitStatus === -1) ? null : unitStatus,
      UnitOfflineReasonID: (reasonOffline === 0 || reasonOffline === -1) ? null : reasonOffline,
      UnitOfflineOtherSpecify: reasonOfflineOtherSpecify,
      ExpectedAvailableDate: expectedAvailableDate ? expectedAvailableDate : null,
      MoveOutComments: moveOutcomments,
      DiscrepancyType: null
    }

    if (!moveOutDate) {
      this.toastrService.error('Please enter the Move Out-Date.');
    } else if (moveOutDate < moveInDate) {
      this.toastrService.error('Move-Out date cannot be before MoveIn Date ' + (moveInDate.getMonth() + 1) + '/' + moveInDate.getDate() + '/' + moveInDate.getFullYear());
    } else if (moveOutDate > sysDate) {
      this.toastrService.error('Move-Out date cannot be a future date');
    } else if (reasonMoved === 0 || reasonMoved === -1) {
      this.toastrService.error('Please select the reason for MoveOut');
    } else if (locationMoveTo === 0 || locationMoveTo === -1) {
      this.toastrService.error('Please select the Location moved To');
    } else if (locationMoveTo === 622 && (agencyMovedTo === 0 || agencyMovedTo === -1)) {
      this.toastrService.error('Please select agency Moved To');
    } else if (agencyMovedTo === -2 && agencyMovedToOtherSpecify === '') {
      this.toastrService.error('Please specify comments for agency Moved To ');
    } else if (moveOutType === 0 || moveOutType === -1) {
      this.toastrService.error('Please select Move Out Type');
    } else if ((unitStatus === 0 || unitStatus === -1) && this.trSelected.siteTrackedType == 33 && this.trSelected.unitID > 0) {
      this.toastrService.error('Please select the Unit Status.');
    } else if (unitStatus === 473) {
      if (reasonOffline === 0 || reasonOffline === -1) {
        this.toastrService.error('Please select the ReasonOffline');
      } else if (reasonOffline === 6 && reasonOfflineOtherSpecify === '') {
        this.toastrService.error('Please specify comments for Unit Reason Offline');
      } else if (!expectedAvailableDate) {
        this.toastrService.error('Please select the Expected Available Date for Unit');
      } else if (expectedAvailableDate) {
        // const sysDate = new Date();
        const yyyy = moveOutDate.getFullYear();
        const mm = moveOutDate.getMonth();
        const dd = moveOutDate.getDate() - 1;
        if (this.expectedAvailableDateTimeFrame > 0) {
          /* This case will only be valid for selected Unit having contractingAgencyType as 488 */
          const validDateRange = new Date(yyyy, mm, dd + this.expectedAvailableDateTimeFrame);
          // alert ('sysDate: '+ sysDate + '     validDateRange: ' + validDateRange);
          if (expectedAvailableDate < moveOutDate) {
            this.toastrService.error('Expected Available Date must be on or after moveOut Date');
          } else if (validDateRange < expectedAvailableDate) {
            this.toastrService.error('Expected Available Date must be on or before ' + (validDateRange.getMonth() + 1) + '/' + validDateRange.getDate() + '/' + validDateRange.getFullYear());
          } else {
            // MoveOut
            this.vcsMoveOutTenant(moveOutTenantData);
          }
        } else if (expectedAvailableDate < moveOutDate) {
          this.toastrService.error('Expected Available Date must be on or after moveOut Date');
        } else {
          // MoveOut
          this.vcsMoveOutTenant(moveOutTenantData);
        }
      } else {
        // MoveOut
        this.vcsMoveOutTenant(moveOutTenantData);
      }
    } else {
      // MoveOut
      this.vcsMoveOutTenant(moveOutTenantData);
    }
  }

  vcsMoveOutTenant(moveOutTenantData: IVCSMoveOutTenant) {
    // console.log('MoveOutData: ', moveOutTenantData);
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to Move out the Tenant? `;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.moveOutTenantSub = this.tenantRosterService.VCSMoveOutTenant(moveOutTenantData).subscribe(rslt => {
          if (rslt >= 0) {
            this.toastrService.success('Tenant Moved Out successfully.');
            if (this.isTadWorkflow) {
              this.router.navigate(['/vcs/tad/tad-submission']);
            } else {
              this.router.navigate(['/vcs/tenant-roster']);
            }
          }
        });
      },
      negativeResponse => { }
    );

  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        if (this.isTadWorkflow) {
          this.router.navigate(['/vcs/tad/tad-submission']);
        } else {
          this.router.navigate(['/vcs/tenant-roster']);
        }
      },
      negativeResponse => { }
    );

  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.raHpAgenciesSub) {
      this.raHpAgenciesSub.unsubscribe();
    }
    if (this.trSelectedSub) {
      this.trSelectedSub.unsubscribe();
    }
    if (this.existingPlacementOutcomeSub) {
      this.existingPlacementOutcomeSub.unsubscribe();
    }
    if (this.moveOutTenantSub) {
      this.moveOutTenantSub.unsubscribe();
    }
    if (this.istadWorkflowSub) {
      this.istadWorkflowSub.unsubscribe();
    }
    if (this.reasonMovedCtrlSub) {
      this.reasonMovedCtrlSub.unsubscribe();
    }
    if (this.locationMoveToCtrlSub) {
      this.locationMoveToCtrlSub.unsubscribe();
    }
    if (this.agencyMovedToCtrlSub) {
      this.agencyMovedToCtrlSub.unsubscribe();
    }
    if (this.unitStatusTypeCtrlSub) {
      this.unitStatusTypeCtrlSub.unsubscribe();
    }
    if (this.reasonOfflineCtrlSub) {
      this.reasonOfflineCtrlSub.unsubscribe();
    }
  }

}
