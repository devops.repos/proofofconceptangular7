import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { IVCSTenantRosterList, ISideDoorClientSearch, ISideDoorClientSearchResult, IVCSMoveInValidator, IVCSAssignTenantToUnit, IVCSUpdateTenantProfile, IVCSMoveOutTenant, TRInput } from './tenant-roster-interface.model';

@Injectable({
  providedIn: 'root'
})
export class TenantRosterService {
  private apiURL = environment.pactApiUrl + 'VCSTenantRoster';
  private vacancyMonitorURL = environment.pactApiUrl + 'VacancyMonitor';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private tenantRosterSelected = new BehaviorSubject<IVCSTenantRosterList>(null);
  private sideDoorClientSearchResult = new BehaviorSubject<ISideDoorClientSearchResult>(null);

  private agencySelected = new BehaviorSubject<number>(0);
  private siteSelected = new BehaviorSubject<number>(0);
  private currentTenantRosterTabIndex = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) { }

  //#region Getting the TenantRosterData from api
  getTenantRosterData(input: TRInput) {
    return this.http.post(this.apiURL + '/GetTenantRoster', JSON.stringify(input), this.httpOptions);
  }
  //#endregion

  //#region Get and Set TenantRoster Selected in the grid
  setTenantRosterSelected(tr: IVCSTenantRosterList) {
    this.tenantRosterSelected.next(tr);
  }

  getTenantRosterSelected() {
    return this.tenantRosterSelected.asObservable();
  }
  //#endregion

  //#region Get and Set Agency/Site Selected
  setTRAgencySelected(agencyID: number) {
    this.agencySelected.next(agencyID);
  }
  getTRAgencySelected() {
    return this.agencySelected.asObservable();
  }
  setTRSiteSelected(siteID: number) {
    this.siteSelected.next(siteID);
  }
  getTRSiteSelected() {
    return this.siteSelected.asObservable();
  }
  //#endregion

  //#region Get and Set current Referral Roster tab index
  getCurrentTRTabIndex() {
    return this.currentTenantRosterTabIndex.asObservable();
  }

  setCurrentTRTabIndex(index: number) {
    this.currentTenantRosterTabIndex.next(index);
  }

  //#region Get and Set SideDoor Client Search
  getSideDoorClientSearch(clientSearchInput: ISideDoorClientSearch) {
    return this.http.post(this.apiURL + '/GetSideDoorClient', JSON.stringify(clientSearchInput), this.httpOptions);
  }

  getSideDoorClientSearchResult() {
    return this.sideDoorClientSearchResult.asObservable();
  }

  setSideDoorClientSearchResult(val: ISideDoorClientSearchResult) {
    this.sideDoorClientSearchResult.next(val);
  }
  //#endregion

  //#region Get all the active RA and HP Agencies
  getVCSHpAgenciesWithTrackedSites() {
    return this.http.get(this.apiURL + '/GetVCSRaHpAgencies');
  }

  getVCSAllActiveAgencies() {
    return this.http.get(this.apiURL + '/GetVCSAllAgencies');
  }
  //#endregion

  //#region Get the VCSMoveInValidators
  getVCSMoveInValidators(inputs: IVCSMoveInValidator) {
    return this.http.post(this.apiURL + '/GetMoveInValidators', JSON.stringify(inputs), this.httpOptions);
  }
  //#endregion

  //#region Get the OnlineUnits by siteID
  getVCSOnlineUnitsBySiteID(siteID: number) {
    return this.http.post(this.apiURL + '/GetVCSOnlineUnitsBySiteID', JSON.stringify(siteID), this.httpOptions);
  }
  //#endregion

  VCSSaveAssignTenantToUnit(input: IVCSAssignTenantToUnit) {
    return this.http.post(this.apiURL + '/VCSAssignTenantToUnit', JSON.stringify(input), this.httpOptions);
  }

  getVCSPlacementOutcomeByVCSPlacementMoveInID(vcsPlacementMoveInID: number) {
    return this.http.post(this.apiURL + '/GetVCSPlacementOutcomeByVCSPlacementMoveInID', JSON.stringify(vcsPlacementMoveInID), this.httpOptions);
  }

  VCSUpdateTenantProfile(input: IVCSUpdateTenantProfile) {
    return this.http.post(this.apiURL + '/VCSUpdateTenantProfile', JSON.stringify(input), this.httpOptions);
  }

  VCSGetUnitOfflineReasons() {
    return this.http.get(this.vacancyMonitorURL + '/GetUnitOfflineReason');
  }

  VCSMoveOutTenant(input: IVCSMoveOutTenant) {
    return this.http.post(this.apiURL + '/VCSMoveOutTenant', JSON.stringify(input), this.httpOptions);
  }
}
