import { VCSPlacementMatchCriteria } from "../client-awaiting-placement/c2v-interfaces.model";
import { IVCSIncomeSourceType } from '../referral-roster/referral-roster-interface.model';

export interface IVCSTenantRoster {
  totalUnits: number;
  occupied: number;
  vacant: number;
  offlineUnits: number;
  moveIns: number;
  moveOuts: number;
  isPlacementReady: boolean;
  siteTrackedType: number;
  tenantRosterList: IVCSTenantRosterList[];
}

export interface IVCSTenantRosterList {
  hpAgencyID: number;
  hpAgencyNo: string;
  hpSiteID: number;
  hpSiteNo: string;
  hpAgencyName: string;
  hpSiteName: string;
  primaryServiceContractID: number;
  primaryServiceContractName: string;
  agreementPopulationID: number;
  populationName: string;
  siteAgreementPopulationID: number;
  agreementTypeDescription: string;
  unitID: number;
  unitName: string;
  unitType: number;
  unitTypeDescription: string;
  unitStatusType: number;
  unitStatusTypeDescription: string;
  rentalSubsidies: string;
  isSideDoorAllowed: boolean;
  isMonitorVacancy: boolean;
  referralType: number;
  tenantFirstName: string;
  tenantLastName: string;
  raAgencyID: number;
  raAgencyNo: string;
  raAgencyName: string;
  raSiteID: number;
  raSiteNo: string;
  raSiteName: string;
  tenantEligibility: string;
  svaPrioritization: string;
  moveInDate: string;
  moveOutDate: string;
  moveOutReasonDescription?: string;
  moveOutLocationDescription?: string;
  moveInVerificationStatusType: number;
  moveInVerificationStatusDescription: string;
  moveInVerificationDate: string;
  moveOutVerificationStatusType: number;
  moveOutVerificationStatusDescription: string;
  moveOutVerificationDate: string;
  applicableTab: number;
  isApplicationMatchRequired: boolean;
  clientID: number;
  clientSourceType: number;
  vcsPlacementMoveInID: number;
  vcsPlacementMoveInVerificationID: number;
  vcsPlacementMoveOutID: number;
  vcsPlacementMoveOutVerificationID: number;
  siteLocationType: number;
  siteLocationTypeDescription: string;
  siteAddress: string;
  isVerified: boolean;
  offlineReasonType: number;
  offlineReasonTypeDescription: string;
  offlineOtherSpecify: string;
  expectedAvailableDate: string;
  unitAddlComment: string;
  unitFeatures: string;
  siteTrackedType: number;
  contractingAgencyType: number;
  referralDate: string;
  isPlacementReady?: boolean;
  moveInSubmittedBy: string;
  moveInSubmittedDate: string;
  moveOutSubmittedBy: string;
  moveOutSubmittedDate: string;
}

export interface ISideDoorClientSearch {
  UnitID: number;
  ClientSourceType: number;
  FirstName: string;
  LastName: string;
  ClientID: number;
  ReferralDate: string;
  SSN: string;
  DOB: string;
  GenderType: number;
}

export interface ISideDoorClientSearchResult {
  sideDoorClient: {
    unitID: number;
    clientSourceType: number;
    clientID: number;
    vcsid: string;
    firstName: string;
    lastName: string;
    referralDate: string;
    ssn: string;
    dob: string;
    genderType: number;
    genderTypeDescription: string;
    userID: number;
  };
  sideDoorClientReferral: {
    pactApplicationID: number;
    raAgencyID: number;
    raAgencyNo: string;
    raAgencyName: string;
    raSiteID: number;
    raSiteNo: string;
    raSiteName: string;
    eligibility: string;
    serviceNeeds: string;
    placementCriteria: string;
    svaPrioritization: string,
    isEligibilityCriteriaMatched: boolean;
    isReferralPending: boolean;
    isApprovalActive: boolean;
    matchPercentage: number;
  };
  placementMatchCriteria: VCSPlacementMatchCriteria[];
}

export interface IVCSHpAgenciesWithTrackedSites {
  agencyID: number;
  agencyNo: string;
  agencyName: string;
}
export interface IVCSAgencies {
  agencyID: number;
  agencyNo: string;
  agencyName: string;
}

export interface IVCSMoveInValidator {
  unitID: number;
  moveInDate: string;
  clientID: number;
  clientSourceType: number;
  pactApplicationID?: number;
  isUnitAvailable?: boolean;
  wasUnitOccupied?: boolean;
  wasUnitOffline?: boolean;
  isClientResiding?: boolean;
  wasClientResiding?: boolean;
  currentlyResidingUnit?: string;
  wasResidingUnit?: string;
  isEligibilityCriteriaMatched?: boolean;
  isApprovalActive?: boolean;
  vcsReferralID?: number;
  referralType?: number;
}

export interface IVCSAssignTenantToUnit {
  UnitID: number;
  UnitName?: string;
  VCSPlacementMoveInID: number;
}

export interface IVCSUpdateTenantProfile {
  VCSPlacementMoveInID: number;
  ClientRentalContribution: number;
  IncomeSourceList: IVCSIncomeSourceType[];
  IncomeSourceOtherSpecify: string;
  IsIncomeSourceUpdated: boolean;
  IsIncomeSourceOtherSpecifyUpdated: boolean;
  IsClientRentalContributionUpdated: boolean;
}

export interface IVCSMoveOutTenant {
  VCSPlacementMoveInID: number;
  MoveOutDate: string;
  MoveOutReasonType: number;
  MoveOutLocationType: number;
  MoveOutType: number;
  MoveToAgencyID: number;
  MoveToAgencyOtherSpecify: string;
  UnitStatusType: number;
  UnitOfflineReasonID: number;
  UnitOfflineOtherSpecify: string;
  ExpectedAvailableDate: string;
  MoveOutComments: string;
  DiscrepancyType: number;
}

export interface IVCSUnitOfflineReason {
  unitOfflineReasonID: number;
  unitOfflineReasonDescription: string;
  timeFrame: number;
}

export interface TRInput {
  siteID: number;
  applicableTab: number;
}

