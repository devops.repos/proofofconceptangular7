import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IVCSTenantRosterList, ISideDoorClientSearch, ISideDoorClientSearchResult } from '../tenant-roster-interface.model';
import { Subscription } from 'rxjs';
import { TenantRosterService } from '../tenant-roster.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { TadService } from '../../tad-submission/tad.service';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';
import * as moment from 'moment';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

@Component({
  selector: 'app-side-door-client-search',
  templateUrl: './side-door-client-search.component.html',
  styleUrls: ['./side-door-client-search.component.scss']
})
export class SideDoorClientSearchComponent implements OnInit, OnDestroy {

  trSelected: IVCSTenantRosterList = {
    hpAgencyID: 0,
    hpAgencyNo: '',
    hpSiteID: 0,
    hpSiteNo: '',
    hpAgencyName: '',
    hpSiteName: '',
    primaryServiceContractID: 0,
    primaryServiceContractName: '',
    agreementPopulationID: 0,
    populationName: '',
    siteAgreementPopulationID: 0,
    agreementTypeDescription: '',
    unitID: 0,
    unitName: '',
    unitType: 0,
    unitTypeDescription: '',
    unitStatusType: 0,
    unitStatusTypeDescription: '',
    rentalSubsidies: '',
    isSideDoorAllowed: false,
    isMonitorVacancy: false,
    referralType: 0,
    tenantFirstName: '',
    tenantLastName: '',
    raAgencyID: 0,
    raAgencyNo: '',
    raAgencyName: '',
    raSiteID: 0,
    raSiteNo: '',
    raSiteName: '',
    tenantEligibility: '',
    svaPrioritization: '',
    moveInDate: '',
    moveOutDate: '',
    moveInVerificationStatusType: 0,
    moveInVerificationStatusDescription: '',
    moveInVerificationDate: '',
    moveOutVerificationStatusType: 0,
    moveOutVerificationStatusDescription: '',
    moveOutVerificationDate: '',
    applicableTab: 0,
    isApplicationMatchRequired: false,
    clientID: 0,
    clientSourceType: 0,
    vcsPlacementMoveInID: 0,
    vcsPlacementMoveInVerificationID: 0,
    vcsPlacementMoveOutID: 0,
    vcsPlacementMoveOutVerificationID: 0,
    siteLocationType: 0,
    siteLocationTypeDescription: '',
    siteAddress: '',
    isVerified: false,
    offlineReasonType: 0,
    offlineReasonTypeDescription: '',
    offlineOtherSpecify: '',
    expectedAvailableDate: '',
    unitAddlComment: '',
    unitFeatures: '',
    siteTrackedType: 0,
    contractingAgencyType: 0,
    referralDate: '',
    moveInSubmittedBy: '',
    moveInSubmittedDate: '',
    moveOutSubmittedBy: '',
    moveOutSubmittedDate: '',
  }

  referralType = 0; // 526 = Supportive Housing sideDoor Referral  610 = Non-Supportive Housing Sidedoor Referral

  genders: RefGroupDetails[];
  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  sideDoorClientSearchGroup: FormGroup;

  isTadWorkflow = false;

  clientAgeInYears: number;
  clientAgeInMonths: number;

  trSelectedSub: Subscription;
  sideDoorClientSearchSub: Subscription;
  istadWorkflowSub: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private tenantRosterService: TenantRosterService,
    private router: Router,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService
  ) {
    this.sideDoorClientSearchGroup = this.formBuilder.group({
      firstNameCtrl: ['', Validators.required],
      lastNameCtrl: ['', Validators.required],
      hraClientIDCtrl: ['', Validators.required],
      referralDateCtrl: ['', Validators.required],
      ssnCtrl: ['', [Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      dobCtrl: ['', Validators.required],
      genderCtrl: [0, [Validators.required, CustomValidators.dropdownRequired()]]
    });
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '4';
    this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        // console.log(data);
        this.genders = data.filter(d => d.refGroupID === 4);
      },
      error => {
        throw new Error(error.message);
      }
    );

    /** Get the selected TenantRoster Details from the TenantRosterService */
    this.trSelectedSub = this.tenantRosterService.getTenantRosterSelected().subscribe(tr => {
      if (tr) {
        this.trSelected = tr;
        // console.log(this.trSelected);
        if (this.trSelected.isApplicationMatchRequired == true) {
          this.referralType = 526; // Supportive Housing Sidedoor Referral
        } else if (this.trSelected.isApplicationMatchRequired == false) {
          this.referralType = 610; // Non-Supportive Housing Sidedoor Referral
        } else {
          this.referralType = 0;
        }
      } else {
        this.router.navigate(['/vcs/tenant-roster']);
      }
    });

    /* Check if the workFlow is coming from TAD, if so, on submit or exit, redirect back to TAD workflow */
    this.istadWorkflowSub = this.tadService.getIsTadWorkflow().subscribe(flag => {
      if (flag) {
        this.isTadWorkflow = flag;
      }
    });
  }

  onSearchClick() {
    if (this.sideDoorClientSearchGroup.get('firstNameCtrl').invalid ||
      this.sideDoorClientSearchGroup.get('lastNameCtrl').invalid ||
      this.sideDoorClientSearchGroup.get('hraClientIDCtrl').invalid ||
      this.sideDoorClientSearchGroup.get('referralDateCtrl').invalid
    ) {
      this.sideDoorClientSearchGroup.get('firstNameCtrl').markAsTouched();
      this.sideDoorClientSearchGroup.get('lastNameCtrl').markAsTouched();
      this.sideDoorClientSearchGroup.get('hraClientIDCtrl').markAsTouched();
      this.sideDoorClientSearchGroup.get('referralDateCtrl').markAsTouched();
      return;
    }
    // PactClient Search
    const firstName = this.sideDoorClientSearchGroup.get('firstNameCtrl').value;
    const lastName = this.sideDoorClientSearchGroup.get('lastNameCtrl').value;
    const hraClientID = this.sideDoorClientSearchGroup.get('hraClientIDCtrl').value;
    const referralDate = this.sideDoorClientSearchGroup.get('referralDateCtrl').value ? new Date(this.sideDoorClientSearchGroup.get('referralDateCtrl').value) : this.sideDoorClientSearchGroup.get('referralDateCtrl').value;

    if (!firstName) {
      this.toastr.error('Please enter the FirstName');
    } else if (!lastName) {
      this.toastr.error('Please enter the LastName');
    } else if (!hraClientID) {
      this.toastr.error('Please enter the HRAClientID');
    } else if (!referralDate) {
      this.toastr.error('Please select a ReferralDate');
    } else {
      const clientSearchInput: ISideDoorClientSearch = {
        UnitID: this.trSelected.unitID,
        ClientSourceType: 573,
        FirstName: firstName,
        LastName: lastName,
        ClientID: hraClientID,
        ReferralDate: referralDate,
        SSN: null,
        DOB: null,
        GenderType: null,
      };
      this.sideDoorClientSearchSub = this.tenantRosterService.getSideDoorClientSearch(clientSearchInput).subscribe((rslt: ISideDoorClientSearchResult) => {
        if (rslt.sideDoorClient) {
          // console.log('Direct Referral Client: ', rslt);
          this.tenantRosterService.setSideDoorClientSearchResult(rslt);
          this.router.navigate(['/vcs/tenant-roster/side-door-client-search-result']);
        } else {
          const title = 'Client Not Found ';
          const primaryMessage = `Matching record does not exist for applicant's LastName, FirstName, HRAClient, HRAReferralDate`;
          const secondaryMessage = ``;
          const confirmButtonName = 'OK';
          const dismissButtonName = '';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => { },
            negativeResponse => { }
          );
        }
      });

    }
  }

  onSubmitClick() {
    if (this.sideDoorClientSearchGroup.get('firstNameCtrl').invalid ||
      this.sideDoorClientSearchGroup.get('lastNameCtrl').invalid ||
      this.sideDoorClientSearchGroup.get('ssnCtrl').invalid ||
      this.sideDoorClientSearchGroup.get('dobCtrl').invalid ||
      this.sideDoorClientSearchGroup.get('genderCtrl').invalid
    ) {
      this.sideDoorClientSearchGroup.get('firstNameCtrl').markAsTouched();
      this.sideDoorClientSearchGroup.get('lastNameCtrl').markAsTouched();
      this.sideDoorClientSearchGroup.get('ssnCtrl').markAsTouched();
      this.sideDoorClientSearchGroup.get('dobCtrl').markAsTouched();
      this.sideDoorClientSearchGroup.get('genderCtrl').markAsTouched();
      return;
    }
    // Validate Age - Age should be between 17years 8months to 119years
    if (!this.validateAge()) {
      return;
    }
    // Community Client Search
    const firstName = this.sideDoorClientSearchGroup.get('firstNameCtrl').value;
    const lastName = this.sideDoorClientSearchGroup.get('lastNameCtrl').value;
    const ssn = this.sideDoorClientSearchGroup.get('ssnCtrl').value;
    const dob = this.sideDoorClientSearchGroup.get('dobCtrl').value ? new Date(this.sideDoorClientSearchGroup.get('dobCtrl').value) : this.sideDoorClientSearchGroup.get('dobCtrl').value;
    const gender = this.sideDoorClientSearchGroup.get('genderCtrl').value;

    if (!firstName) {
      this.toastr.error('Please enter the FirstName');
    } else if (!lastName) {
      this.toastr.error('Please enter the LastName');
    } else if (!ssn) {
      this.toastr.error('Please enter the SSN');
    } else if (!dob) {
      this.toastr.error('Please select a Date of Birth');
    } else if (gender === 0) {
      this.toastr.error('Please select a Gender');
    } else {
      const clientSearchInput: ISideDoorClientSearch = {
        UnitID: this.trSelected.unitID,
        ClientSourceType: 574,
        FirstName: firstName,
        LastName: lastName,
        ClientID: null,
        ReferralDate: null,
        SSN: ssn,
        DOB: dob,
        GenderType: gender
      }
      this.sideDoorClientSearchSub = this.tenantRosterService.getSideDoorClientSearch(clientSearchInput).subscribe((rslt: ISideDoorClientSearchResult) => {
        if (rslt) {
          this.tenantRosterService.setSideDoorClientSearchResult(rslt);
          this.router.navigate(['/vcs/tenant-roster/side-door-move-in']);
        } else {
          this.toastr.error('No Client Found');
        }
      });
    }
  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        if (this.isTadWorkflow) {
          this.router.navigate(['/vcs/tad/tad-submission']);
        } else {
          this.router.navigate(['/vcs/tenant-roster']);
        }
      },
      negativeResponse => { }
    );

  }

  //Calculate Age Of The Client In Years and Months
  calculateAge() {
    if (this.sideDoorClientSearchGroup.get('dobCtrl').value && !moment(this.sideDoorClientSearchGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Invalid Date of Birth.");
      }
      this.sideDoorClientSearchGroup.controls['dobCtrl'].setValue("");
      return;
    }
    if (this.sideDoorClientSearchGroup.get('dobCtrl').value) {
      const starts = moment(this.sideDoorClientSearchGroup.get('dobCtrl').value, 'MM/DD/YYYY');
      const ends = moment(Date.now());
      const years = ends.diff(starts, 'year');
      starts.add(years, 'years');
      const months = ends.diff(starts, 'months');
      starts.add(months, 'months');
      this.clientAgeInYears = years;
      this.clientAgeInMonths = months;
    }
  }


  //Validate Client Age
  validateAge() {
    if (this.clientAgeInYears < 17) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
      }
      return false;
    }
    if (this.clientAgeInYears === 17 && this.clientAgeInMonths < 8) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
      }
      return false;
    }
    if (this.clientAgeInYears > 119) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Client is over 119 years old. Please check the DOB or Age.");
      }
      return false;
    }
    return true;
  }

  ngOnDestroy() {
    if (this.trSelectedSub) {
      this.trSelectedSub.unsubscribe();
    }
    if (this.sideDoorClientSearchSub) {
      this.sideDoorClientSearchSub.unsubscribe();
    }
    if (this.istadWorkflowSub) {
      this.istadWorkflowSub.unsubscribe();
    }
  }

}
