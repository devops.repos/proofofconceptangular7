import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantRosterComponent } from './tenant-roster.component';

describe('TenantRosterComponent', () => {
  let component: TenantRosterComponent;
  let fixture: ComponentFixture<TenantRosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TenantRosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantRosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
