import { Component, OnInit, ViewChild, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { AuthData } from 'src/app/models/auth-data.model';
import { IVCSAgencyList, IVCSSiteList, IVCSAgencyInput, IVCSSiteInput, IVCSRaHpAgenciesSites, IVCSRaHpAgencyList, IVCSRaHpSiteList } from '../referral-roster/referral-roster-interface.model';
import { IVCSTenantRoster, IVCSTenantRosterList, IVCSAssignTenantToUnit, TRInput } from './tenant-roster-interface.model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ReferralRosterService } from '../referral-roster/referral-roster.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { TenantRosterService } from './tenant-roster.service';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { MatTabChangeEvent, MatDialog } from '@angular/material';
import { TRMyTenantsActionComponent } from './tr-my-tenants-action.component';
import { TrInfoIconComponent } from './tr-info-icon.component';
import { AssignTenantToUnitComponent } from './assign-tenant-to-unit/assign-tenant-to-unit.component';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { TadMoveOutInfoIconComponent } from '../tad-submission/tad-hp/moveOut-info-icon.component';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-tenant-roster',
  templateUrl: './tenant-roster.component.html',
  styleUrls: ['./tenant-roster.component.scss']
})
export class TenantRosterComponent implements OnInit, OnDestroy {
  @Input() isTAD: boolean = false;
  @Input() hpSiteIDforTAD: number = 0;
  @Input() hpSiteNoforTAD: string = '';
  @Input() hpSiteNameforTAD: string = '';
  @Input() hpAgencyIDforTAD: number = 0;
  @Input() hpAgencyNoforTAD: string = '';
  @Input() hpAgencyNameforTAD: string = '';
  @Input() tadValidationMsg: string = '';
  @Output() callTadValidator = new EventEmitter();
  currentUser: AuthData;
  is_CAS = false;
  is_SH_HP = false;
  is_SH_PE = false;

  @ViewChild('myTenantsGrid') myTenantsGrid: AgGridAngular;
  @ViewChild('transmittedAgGrid') transmittedAgGrid: AgGridAngular;
  agencyList: IVCSAgencyList[] = [];
  siteList: IVCSSiteList[] = [];
  selectedAgencyID = 0;
  selectedSiteID = 0;

  totalUnits = 0;
  occupied = 0;
  online = 0;
  offline = 0;
  moveIns = 0;
  moveOuts = 0;
  flagClicked = '';

  selectedTab = 0;

  myTenantsGridApi;
  myTenantsGridColumnApi;
  myTenantsColumnDefs;
  mOverlayLoadingTemplate;
  mOverlayNoRowsTemplate;
  transmittedGridApi;
  transmittedGridColumnApi;
  transmittedColumnDefs;
  tOverlayLoadingTemplate;
  tOverlayNoRowsTemplate;

  defaultColDef;
  frameworkComponents;
  context;

  public myTenantsGridOptions: GridOptions;
  public transmittedGridOptions: GridOptions;
  tenantRosterData: IVCSTenantRoster;
  myTenantsRowData: IVCSTenantRosterList[] = [];
  transmittedRowData: IVCSTenantRosterList[];

  raHpAgenciesSites: IVCSRaHpAgenciesSites;
  raHpAgencies: IVCSRaHpAgencyList[];
  raHpSites: IVCSRaHpSiteList[];
  savedAgencyID: number;
  savedSiteID: number;

  currentUserSub: Subscription;
  tenantRosterDataSub: Subscription;
  assignTenantToUnitSub: Subscription;
  selectedAgencySub: Subscription;
  selectedSiteSub: Subscription;
  selectedTabSub: Subscription;
  routeSub: Subscription;

  constructor(
    private userService: UserService,
    private tenantRosterService: TenantRosterService,
    private referralRosterService: ReferralRosterService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private commonService: CommonService,
    private confirmDialogService: ConfirmDialogService
  ) {

    /* Getting all the RA/HP Agencies and Sites before Page Load */
    this.route.data.subscribe((res) => {
      // console.log('route data: ', res.rahpAgenciesSites);
      if (res.rahpAgenciesSites.raHpSites) {
        this.raHpAgenciesSites = res.rahpAgenciesSites;
        this.raHpAgencies = res.rahpAgenciesSites.raHpAgencies;
        this.raHpSites = res.rahpAgenciesSites.raHpSites;
        // console.log('raHpAgenciesSites: ', this.raHpAgenciesSites);
      }
    });

    // this.mOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Tenants list is loading.</span>';
    this.mOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Tenants Available</span>';
    // this.tOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Transmitted Tenants list is loading.</span>';
    this.tOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Transmitted Tenants Available</span>';
    this.myTenantsGridOptions = {
      rowHeight: 35,
      getRowStyle(params) {
        if (params.data.unitStatusType == 473) {
          return { background: '#E5E8E8', color: 'rgba(0, 0, 0, 0.38)' };
        }
        if (!params.data.unitID && params.data.moveInDate) {
          return { background: 'sandybrown' };
        }
      }
    } as GridOptions;
    this.transmittedGridOptions = {
      rowHeight: 35
    } as GridOptions;

    this.myTenantsColumnDefs = [
      {
        headerName: 'Client# - ReferralDate',
        field: 'clientNoReferralDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.clientID && params.data.referralDate) {
            return params.data.clientID + ' - ' + params.data.referralDate;
          } else if (params.data.clientID) {
            return params.data.clientID;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Tenant Name (L,F)',
        field: 'tenantName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantLastName + ', ' + params.data.tenantFirstName;
          } else if (params.data.tenantFirstName && !params.data.tenantLastName) {
            return params.data.tenantLastName;
          } else if (!params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantFirstName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Unit#',
        field: 'unitName',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Unit Status',
        field: 'unitStatusTypeDescription',
        width: 180,
        filter: 'agTextColumnFilter',
        cellRendererSelector(params) {
          const infoIcon = {
            component: 'infoIconRenderer'
          };
          if (params.data.unitID && ((params.data.unitStatusType == 473) ||
            !params.data.isPlacementReady ||
            (params.data.unitStatusType == 472 && (params.data.isApplicationMatchRequired == null || !params.data.isSideDoorAllowed)) ||
            (params.data.moveInVerificationStatusType == 519) ||
            (params.data.moveOutVerificationStatusType == 522) ||
            (params.data.isVerified == false))) {
            return infoIcon;
          } else {
            return null;
          }
        },
        cellStyle: function (params) {
          if (params.data.unitStatusType == 472 && (params.data.isApplicationMatchRequired !== null && params.data.isSideDoorAllowed)) {
            //mark MoveOut Verified cells as green
            return { color: 'green' };
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Primary Service Contract',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Rental Subsidies',
        field: 'rentalSubsidies',
        filter: 'agTextColumnFilter'
      },

      {
        headerName: 'Referring Agency/Site',
        field: 'raAgencyName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.raSiteName && params.data.raAgencyName) {
            return params.data.raAgencyNo + ' - ' + params.data.raAgencyName + ' / ' + params.data.raSiteNo + ' - ' + params.data.raSiteName;
          } else if (params.data.raAgencyName) {
            return params.data.raAgencyNo + ' - ' + params.data.raAgencyName;
          } else if (params.data.raSiteName) {
            return params.data.raSiteNo + ' - ' + params.data.raSiteName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Tenant Eligibility',
        field: 'tenantEligibility',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Move-In Date',
        field: 'moveInDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'myTenantsActionRenderer'
      }
    ];
    this.transmittedColumnDefs = [
      {
        headerName: 'Client# - ReferralDate',
        field: 'clientNoReferralDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.clientID && params.data.referralDate) {
            return params.data.clientID + ' - ' + params.data.referralDate;
          } else if (params.data.clientID) {
            return params.data.clientID;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Tenant Name (L,F)',
        field: 'tenantName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantLastName + ', ' + params.data.tenantFirstName;
          } else if (params.data.tenantFirstName && !params.data.tenantLastName) {
            return params.data.tenantLastName;
          } else if (!params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantFirstName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Verification Status',
        field: 'verificationStatus',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.moveOutDate) {
            return params.data.moveOutVerificationStatusDescription;
          } else if (params.data.moveInDate) {
            return params.data.moveInVerificationStatusDescription;
          }
        },
        cellStyle: function (params) {
          if (params.data.moveOutDate) {
            if (params.data.moveOutVerificationStatusType == 524) {
              //mark MoveOut Rejected cells as red
              return { color: 'red' };
            } else if (params.data.moveOutVerificationStatusType == 523) {
              //mark MoveOut Verified cells as green
              return { color: 'green' };
            } else if (params.data.moveOutVerificationStatusType == 522) {
              //mark MoveOut Pending cells as orange
              return { color: '#FF9900' };
            }
          } else if (params.data.moveInDate) {
            if (params.data.moveInVerificationStatusType == 521) {
              //mark MoveIn Rejected cells as red
              return { color: 'red' };
            } else if (params.data.moveInVerificationStatusType == 520) {
              //mark MoveIn Verified cells as green
              return { color: 'green' };
            } else if (params.data.moveInVerificationStatusType == 519) {
              //mark MoveIn Pending cells as orange
              return { color: '#FF9900' };
            }
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Unit#',
        field: 'unitName',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Primary Service Contract',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Rental Subsidies',
        field: 'rentalSubsidies',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referring Agency/Site',
        field: 'raAgencyName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.raSiteName && params.data.raAgencyName) {
            return params.data.raAgencyNo + ' - ' + params.data.raAgencyName + ' / ' + params.data.raSiteNo + ' - ' + params.data.raSiteName;
          } else if (params.data.raAgencyName) {
            return params.data.raAgencyNo + ' - ' + params.data.raAgencyName;
          } else if (params.data.raSiteName) {
            return params.data.raSiteNo + ' - ' + params.data.raSiteName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Tenant Eligibility',
        field: 'tenantEligibility',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Move In - Move Out Date',
        field: 'moveInMoveOutDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.moveInDate && params.data.moveOutDate) {
            return params.data.moveInDate + ' - ' + params.data.moveOutDate;
          } else if (params.data.moveInDate && !params.data.moveOutDate) {
            return params.data.moveInDate;
          } else if (!params.data.moveInDate && params.data.moveOutDate) {
            return 'unknown - ' + params.data.moveOutDate;
          } else {
            return '';
          }
        },
        cellRendererSelector(params) {
          const infoIcon = {
            component: 'moveOutInfoIconRenderer'
          };
          if (params.data.moveOutDate) {
            return infoIcon;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Verification Date',
        field: 'verificationDate',
        filter: 'agDateColumnFilter',
        valueGetter(params) {
          if (params.data.moveOutDate) {
            return params.data.moveOutVerificationDate;
          } else if (params.data.moveInDate) {
            return params.data.moveInVerificationDate;
          }
        },
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'MoveIn Submitted By/Date',
        field: 'moveInSubmittedByDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.moveInSubmittedBy && params.data.moveInSubmittedDate) {
            return params.data.moveInSubmittedBy + ' - ' + params.data.moveInSubmittedDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'MoveOut Submitted By/Date',
        field: 'moveOutSubmittedByDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.moveOutSubmittedBy && params.data.moveOutSubmittedDate) {
            return params.data.moveOutSubmittedBy + ' - ' + params.data.moveOutSubmittedDate;
          } else {
            return '';
          }
        }
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      myTenantsActionRenderer: TRMyTenantsActionComponent,
      infoIconRenderer: TrInfoIconComponent,
      moveOutInfoIconRenderer: TadMoveOutInfoIconComponent,
    };
  }



  ngOnInit() {
    /** Get the current tab index */
    this.selectedTabSub = this.tenantRosterService.getCurrentTRTabIndex().subscribe(res => {
      this.selectedTab = res;
    });

    /* Get Saved AgencyID and SiteID */
    this.selectedAgencySub = this.tenantRosterService.getTRAgencySelected().subscribe(agencyID => {
      if (agencyID > 0) {
        this.savedAgencyID = agencyID;
      }
    });

    this.selectedSiteSub = this.tenantRosterService.getTRSiteSelected().subscribe(siteID => {
      if (siteID > 0) {
        this.savedSiteID = siteID;
      }
    });

    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
        if (this.currentUser.siteCategoryType.length > 0) {
          /** Checking the User siteCategoryType either PE or HP */
          var shhp = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_HP);
          var shpe = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_PE);
          var cas = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.CAS);
          if (shhp.length > 0) {
            /* HP User */
            this.is_SH_HP = true;
            this.selectedAgencyID = this.currentUser.agencyId;
            /** Get the HPSite List */
            if (this.isTAD) {
              /* Tenant Roster being called from TAD */
              if (this.hpSiteIDforTAD > 0) {
                this.selectedSiteID = this.hpSiteIDforTAD;
                this.onGo();
              }
            } else {
              /* Tenant Roster normal call */
              this.getSiteSelected();
            }
          } else {
            /* PE or CAS User */
            if (shpe.length > 0) {
              this.is_SH_PE = true;
            } else if (cas.length > 0) {
              this.is_CAS = true;
            }
            /** Get the HPSite List */
            if (this.isTAD) {
              /* Tenant Roster being called from TAD */
              if (this.hpSiteIDforTAD > 0) {
                this.selectedSiteID = this.hpSiteIDforTAD;
                this.onGo();
              }
            } else {
              /** Get the HP Agency List */
              if (this.raHpAgencies.length > 0) {
                this.agencyList = this.raHpAgencies;
                // console.log('agencyList: ', this.agencyList);
                if (this.savedAgencyID > 0) {
                  if (this.commonService._doesValueExistInJson(this.agencyList, this.savedAgencyID)) {
                    this.selectedAgencyID = this.savedAgencyID;
                    // console.log('callOnAgencySelected1');
                    this.onAgencySelected();
                  }
                  // else {
                  //   this.selectedAgencyID = this.agencyList[0].agencyID;
                  //   // console.log('callOnAgencySelected2');
                  //   this.onAgencySelected();
                  // }
                }
                // else {
                //   this.selectedAgencyID = this.agencyList[0].agencyID;
                //   // console.log('callOnAgencySelected3');
                //   this.onAgencySelected();
                // }
              }
            }
          }
        }
      }
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        /* save the selected AgencyID and SiteID before leaving the page */
        this.tenantRosterService.setTRAgencySelected(this.selectedAgencyID);
        this.tenantRosterService.setTRSiteSelected(this.selectedSiteID);
      };
    });
  }

  onAgencySelected() {
    if (this.selectedAgencyID > 0) {
      // console.log('onAgencySelected');
      this.getSiteSelected();
    }
  }

  getSiteSelected() {
    this.selectedSiteID = 0;
    if (this.raHpSites.length > 0) {
      this.siteList = this.raHpSites.filter(d => d.agencyID == this.selectedAgencyID);
      // console.log('getSiteSelected2 RA/HP Site');
      if (this.siteList.length > 0) {
        /** Selected Default HP Site */
        if (this.savedSiteID > 0) {
          if (this.commonService._doesValueExistInJson(this.siteList, this.savedSiteID)) {
            this.selectedSiteID = this.savedSiteID;
            // console.log('getSiteSelected11');
            this.onGo();
          }
          // else {
          //   this.selectedSiteID = this.siteList[0].siteID;
          //   // console.log('getSiteSelected12');
          //   this.onGo();
          // }
        }
        // else {
        //   this.selectedSiteID = this.siteList[0].siteID;
        //   // console.log('getSiteSelected13');
        //   this.onGo();
        // }
      }
    }
  }

  onGo() {
    if (this.selectedSiteID > 0) {
      /* save the selected AgencyID and SiteID */
      // this.tenantRosterService.setTRAgencySelected(this.selectedAgencyID);
      // this.tenantRosterService.setTRSiteSelected(this.selectedSiteID);
      var input: TRInput = {
        siteID: this.selectedSiteID,
        applicableTab: this.selectedTab == 0 ? 1 : 2
      }
      // alert('agencyID: ' + this.selectedAgencyID + '   siteID: ' + this.selectedSiteID);
      this.tenantRosterDataSub = this.tenantRosterService.getTenantRosterData(input).subscribe((res: IVCSTenantRoster) => {
        // console.log(res.referralRosterList);
        this.tenantRosterData = res;
        this.totalUnits = res.totalUnits;
        this.occupied = res.occupied;
        this.online = res.vacant;
        this.offline = res.offlineUnits;
        this.moveIns = res.moveIns;
        this.moveOuts = res.moveOuts;
        this.myTenantsRowData = [];
        this.transmittedRowData = [];
        res.tenantRosterList.forEach(ten => {
          ten.isPlacementReady = res.isPlacementReady;
          ten.siteTrackedType = res.siteTrackedType;
          if (this.selectedTab == 0) {
            this.myTenantsRowData.push(ten);
          }
          if (this.selectedTab == 1) {
            this.transmittedRowData.push(ten);
          }
        });
        // if (this.selectedTab == 0) {
        //   this.myTenantsRowData = res.tenantRosterList;
        // } else if (this.selectedTab == 1) {
        //   this.transmittedRowData = res.tenantRosterList;
        // }
      });
    } else {
      this.toastr.error('Please select both Agency and Site.');
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;
    this.tenantRosterService.setCurrentTRTabIndex(this.selectedTab);
    // console.log('selectedTab: ',this.selectedTab);

    if (this.selectedAgencyID > 0 && this.selectedSiteID > 0) {
      var input: TRInput = {
        siteID: this.selectedSiteID,
        applicableTab: this.selectedTab == 0 ? 1 : 2
      }
      // alert('agencyID: ' + this.selectedAgencyID + '   siteID: ' + this.selectedSiteID);
      this.tenantRosterDataSub = this.tenantRosterService.getTenantRosterData(input).subscribe((res: IVCSTenantRoster) => {
        // console.log(res);
        this.tenantRosterData = res;
        this.totalUnits = res.totalUnits;
        this.occupied = res.occupied;
        this.online = res.vacant;
        this.offline = res.offlineUnits;
        this.moveIns = res.moveIns;
        this.moveOuts = res.moveOuts;
        this.myTenantsRowData = [];
        this.transmittedRowData = [];
        res.tenantRosterList.forEach(ten => {
          ten.isPlacementReady = res.isPlacementReady;
          ten.siteTrackedType = res.siteTrackedType;
          if (this.selectedTab == 0) {
            this.myTenantsRowData.push(ten);
          }
          if (this.selectedTab == 1) {
            this.transmittedRowData.push(ten);
          }
        });
        // if (this.selectedTab == 0) {
        //   this.myTenantsRowData = res.tenantRosterList;
        // } else if (this.selectedTab == 1) {
        //   this.transmittedRowData = res.tenantRosterList;
        // }
      });
    }
  }

  onMyTenantsGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.myTenantsGridApi = params.api;
    this.myTenantsGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.myTenantsGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'unitStatusTypeDescription') {
          // console.log('column.colID: ' + column.colId);
          allColumnIds.push(column.colId);
        }
      }
    });
    if (this.is_SH_PE) {
      this.myTenantsGridColumnApi.setColumnVisible('action', false);
    }
    this.myTenantsGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
  }
  onTransmittedGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.transmittedGridApi = params.api;
    this.transmittedGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.transmittedGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'clientEligibleFor') {
          // console.log('column.colID: ' + column.colId);
          allColumnIds.push(column.colId);
        }
      }
    });
    this.transmittedGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
  }

  onAssignTenantToUnitClickParent(trSelected: IVCSTenantRosterList) {
    // console.log('trSelected: ', trSelected);
    const dialogRef = this.dialog.open(AssignTenantToUnitComponent, {
      width: '40%',
      panelClass: 'transparent',
      disableClose: true,
      data: trSelected
    });

    dialogRef.afterClosed().subscribe((assignTenantToUnit: IVCSAssignTenantToUnit) => {
      if (assignTenantToUnit) {
        const title = 'Verify';
        const primaryMessage = `Are you sure you want to assign tenant "` + trSelected.tenantLastName + `, ` + trSelected.tenantFirstName + `" to Unit "` + assignTenantToUnit.UnitName + `" ? `;
        const secondaryMessage = ``;
        const confirmButtonName = 'OK';
        const dismissButtonName = 'Cancel';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => {
            this.assignTenantToUnitSub = this.tenantRosterService.VCSSaveAssignTenantToUnit(assignTenantToUnit).subscribe(res => {
              if (res >= 0) {
                this.toastr.success('Client has been assigned to Unit successfully.');
                this.onGo();
                /* If TAD workflow, call the TAD validator to update the Tenant Roster status */
                if (this.isTAD && this.hpSiteIDforTAD > 0) {
                  this.callTadValidator.emit(this.hpSiteIDforTAD);
                }
              }
            });
          },
          negativeResponse => { }
        );
      }
    });
  }

  onUpdateTenantProfileClickParent(trSelected) {
    this.tenantRosterService.setTenantRosterSelected(trSelected);
    this.referralRosterService.setPlacementOutcomeEditableFlag(false);
    this.router.navigate(['/vcs/tenant-roster/update-tenant-profile']);
  }

  onMoveOutTenantClickParent(trSelected) {
    this.tenantRosterService.setTenantRosterSelected(trSelected);
    this.referralRosterService.setPlacementOutcomeEditableFlag(false);
    this.router.navigate(['/vcs/tenant-roster/move-out-tenant']);
  }

  onMoveInTenantClickParent(trSelected) {
    this.tenantRosterService.setTenantRosterSelected(trSelected);
    this.router.navigate(['/vcs/tenant-roster/side-door-client-search']);
  }

  refreshAgGrid(val: string) {
    this.flagClicked = '';
    if (val === 'd') {
      this.myTenantsGridOptions.api.setFilterModel(null);
    } else if (val === 't') {
      this.transmittedGridOptions.api.setFilterModel(null);
    }
  }

  onBtExport(val: string) {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: (val === 'd' ? 'MyTenantsReport-' : val === 't' ? 'TransmittedTenantsReport-' : 'TenantRosterReport') + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    if (val === 'd') {
      this.myTenantsGridApi.exportDataAsExcel(params);
    } else if (val === 't') {
      this.transmittedGridApi.exportDataAsExcel(params);
    }
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.tenantRosterDataSub) {
      this.tenantRosterDataSub.unsubscribe();
    }
    if (this.assignTenantToUnitSub) {
      this.assignTenantToUnitSub.unsubscribe();
    }
    if (this.selectedAgencySub) {
      this.selectedAgencySub.unsubscribe();
    }
    if (this.selectedSiteSub) {
      this.selectedSiteSub.unsubscribe();
    }
    if (this.selectedTabSub) {
      this.selectedTabSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }
}
