import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISideDoorClientSearchResult, IVCSTenantRosterList } from '../tenant-roster-interface.model';
import { Subscription } from 'rxjs';
import { TenantRosterService } from '../tenant-roster.service';
import { Router } from '@angular/router';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { VCSDeleteReferral } from '../../referral-roster/referral-roster-interface.model';
import { TadService } from '../../tad-submission/tad.service';

@Component({
  selector: 'app-side-door-client-search-result',
  templateUrl: './side-door-client-search-result.component.html',
  styleUrls: ['./side-door-client-search-result.component.scss']
})
export class SideDoorClientSearchResultComponent implements OnInit, OnDestroy {

  trSelected: IVCSTenantRosterList = {
    hpAgencyID: 0,
    hpAgencyNo: '',
    hpSiteID: 0,
    hpSiteNo: '',
    hpAgencyName: '',
    hpSiteName: '',
    primaryServiceContractID: 0,
    primaryServiceContractName: '',
    agreementPopulationID: 0,
    populationName: '',
    siteAgreementPopulationID: 0,
    agreementTypeDescription: '',
    unitID: 0,
    unitName: '',
    unitType: 0,
    unitTypeDescription: '',
    unitStatusType: 0,
    unitStatusTypeDescription: '',
    rentalSubsidies: '',
    isSideDoorAllowed: false,
    isMonitorVacancy: false,
    referralType: 0,
    tenantFirstName: '',
    tenantLastName: '',
    raAgencyID: 0,
    raAgencyNo: '',
    raAgencyName: '',
    raSiteID: 0,
    raSiteNo: '',
    raSiteName: '',
    tenantEligibility: '',
    svaPrioritization: '',
    moveInDate: '',
    moveOutDate: '',
    moveInVerificationStatusType: 0,
    moveInVerificationStatusDescription: '',
    moveInVerificationDate: '',
    moveOutVerificationStatusType: 0,
    moveOutVerificationStatusDescription: '',
    moveOutVerificationDate: '',
    applicableTab: 0,
    isApplicationMatchRequired: false,
    clientID: 0,
    clientSourceType: 0,
    vcsPlacementMoveInID: 0,
    vcsPlacementMoveInVerificationID: 0,
    vcsPlacementMoveOutID: 0,
    vcsPlacementMoveOutVerificationID: 0,
    siteLocationType: 0,
    siteLocationTypeDescription: '',
    siteAddress: '',
    isVerified: false,
    offlineReasonType: 0,
    offlineReasonTypeDescription: '',
    offlineOtherSpecify: '',
    expectedAvailableDate: '',
    unitAddlComment: '',
    unitFeatures: '',
    siteTrackedType: 0,
    contractingAgencyType: 0,
    referralDate: '',
    moveInSubmittedBy: '',
    moveInSubmittedDate: '',
    moveOutSubmittedBy: '',
    moveOutSubmittedDate: '',
  }

  sideDoorClientSearchResult: ISideDoorClientSearchResult = {
    sideDoorClient: {
      unitID: 0,
      clientSourceType: 0,
      clientID: 0,
      vcsid: '',
      firstName: '',
      lastName: '',
      referralDate: '',
      ssn: '000000000',
      dob: '',
      genderType: 0,
      genderTypeDescription: '',
      userID: 0,
    },
    sideDoorClientReferral: {
      pactApplicationID: 0,
      raAgencyID: 0,
      raAgencyNo: '',
      raAgencyName: '',
      raSiteID: 0,
      raSiteNo: '',
      raSiteName: '',
      eligibility: '',
      serviceNeeds: '',
      placementCriteria: '',
      svaPrioritization: '',
      isEligibilityCriteriaMatched: false,
      isReferralPending: false,
      isApprovalActive: false,
      matchPercentage: 0,
    },
    placementMatchCriteria: []
  }

  isTadWorkflow = false;

  trSelectedSub: Subscription;
  sideDoorClientSearchResultSub: Subscription;
  istadWorkflowSub: Subscription;

  constructor(
    private tenantRosterService: TenantRosterService,
    private router: Router,
    private confirmDialogService: ConfirmDialogService,
    private toastr: ToastrService,
    private tadService: TadService
  ) { }

  ngOnInit() {
    /** Get the selected TenantRoster Details from the TenantRosterService */
    this.trSelectedSub = this.tenantRosterService.getTenantRosterSelected().subscribe(tr => {
      if (tr) {
        this.trSelected = tr;
        /** Get the sideDoor Client Search Result */
        this.sideDoorClientSearchResultSub = this.tenantRosterService.getSideDoorClientSearchResult().subscribe(rslt => {
          if (rslt) {
            this.sideDoorClientSearchResult = rslt;
            // console.log('this.sideDoorClientSearchResult: ', this.sideDoorClientSearchResult);
          }
        });
      } else {
        this.router.navigate(['/vcs/tenant-roster']);
      }
    });

    /* Check if the workFlow is coming from TAD, if so, on submit or exit, redirect back to TAD workflow */
    this.istadWorkflowSub = this.tadService.getIsTadWorkflow().subscribe(flag => {
      if (flag) {
        this.isTadWorkflow = flag;
      }
    });
  }

  onProceedClicked() {
    if (this.sideDoorClientSearchResult.sideDoorClientReferral) {
      // console.log(this.sideDoorClientSearchResult);
      if (this.sideDoorClientSearchResult.sideDoorClientReferral.isReferralPending) {
        const title = 'Verify';
        const primaryMessage = `A pending referral exists for this client ` + this.sideDoorClientSearchResult.sideDoorClient.lastName + ' ' + this.sideDoorClientSearchResult.sideDoorClient.firstName
          + ', ' + this.sideDoorClientSearchResult.sideDoorClient.clientID + ', ' + this.sideDoorClientSearchResult.sideDoorClient.referralDate +
          ` in the “Referral Roster”.`;
        const secondaryMessage = `  Please go to the “Referral Roster” to complete the referral outcome`;
        const confirmButtonName = 'OK';
        const dismissButtonName = '';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => this.router.navigate(['/vcs/referral-roster']),
          negativeResponse => { }
        );
      } else if (!this.sideDoorClientSearchResult.sideDoorClientReferral.isEligibilityCriteriaMatched && !this.sideDoorClientSearchResult.sideDoorClientReferral.isApprovalActive) {
        const title = 'Verify';
        const primaryMessage = `This Client's Approval is expired, and Approval does not match with the Unit Profile.`;
        const secondaryMessage = `are you sure you want to proceed?`;
        const confirmButtonName = 'Yes';
        const dismissButtonName = 'No';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => this.router.navigate(['/vcs/tenant-roster/side-door-move-in']),
          negativeResponse => { }
        );
      } else if (!this.sideDoorClientSearchResult.sideDoorClientReferral.isEligibilityCriteriaMatched) {
        const title = 'Verify';
        const primaryMessage = `This Client's Eligibility Criteria does not match with the Unit Profile you selected.`;
        const secondaryMessage = `Do you still want to continue?`;
        const confirmButtonName = 'Yes';
        const dismissButtonName = 'No';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => this.router.navigate(['/vcs/tenant-roster/side-door-move-in']),
          negativeResponse => { }
        );
      } else if (!this.sideDoorClientSearchResult.sideDoorClientReferral.isApprovalActive) {
        const title = 'Verify';
        const primaryMessage = `This Client's Approval is expired.`;
        const secondaryMessage = `Do you still want to continue?`;
        const confirmButtonName = 'Yes';
        const dismissButtonName = 'No';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => this.router.navigate(['/vcs/tenant-roster/side-door-move-in']),
          negativeResponse => { }
        );
      }
      else {
        this.router.navigate(['/vcs/tenant-roster/side-door-move-in']);
      }
    } else {
      this.toastr.error('Client record not found.');
    }
  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        if (this.isTadWorkflow) {
          this.router.navigate(['/vcs/tad/tad-submission']);
        } else {
          this.router.navigate(['/vcs/tenant-roster']);
        }
      },
      negativeResponse => { }
    );

  }

  ngOnDestroy() {
    if (this.trSelectedSub) {
      this.trSelectedSub.unsubscribe();
    }
    if (this.sideDoorClientSearchResultSub) {
      this.sideDoorClientSearchResultSub.unsubscribe();
    }
    if (this.istadWorkflowSub) {
      this.istadWorkflowSub.unsubscribe();
    }
  }
}
