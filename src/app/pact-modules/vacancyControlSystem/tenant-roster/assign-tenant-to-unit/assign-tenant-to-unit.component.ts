import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IVCSOnlineUnits } from '../../referral-roster/referral-roster-interface.model';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { TenantRosterService } from '../tenant-roster.service';
import { IVCSTenantRosterList, IVCSAssignTenantToUnit } from '../tenant-roster-interface.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-assign-tenant-to-unit',
  templateUrl: './assign-tenant-to-unit.component.html',
  styleUrls: ['./assign-tenant-to-unit.component.scss']
})
export class AssignTenantToUnitComponent implements OnInit, OnDestroy {

  assignTenantToUnitGroup: FormGroup;
  onlineUnits: IVCSOnlineUnits[];

  onlineUnitsSub: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AssignTenantToUnitComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IVCSTenantRosterList,
    private tenantRosterService: TenantRosterService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.assignTenantToUnitGroup = this.formBuilder.group({
      unitIDCtrl: [Validators.compose([Validators.required, CustomValidators.dropdownRequired()])]
    });
  }

  ngOnInit() {
    // console.log('Data: ', this.data);
    this.onlineUnitsSub = this.tenantRosterService.getVCSOnlineUnitsBySiteID(this.data.hpSiteID).subscribe((res: IVCSOnlineUnits[]) => {
      if (res) {
        this.onlineUnits = res;
      }
    });
  }

  onOKClick() {
    let unitName = [];
    unitName = this.onlineUnits.filter(d => d.unitID == this.assignTenantToUnitGroup.get('unitIDCtrl').value);

    // alert('UnitID: ' + value.UnitID + '   VCSPlacementMoveInID: ' + value.VCSPlacementMoveInID);
    setTimeout(() => {
      if (unitName.length > 0) {
        const value: IVCSAssignTenantToUnit = {
          UnitID: this.assignTenantToUnitGroup.get('unitIDCtrl').value,
          UnitName: unitName[0].unitName,
          VCSPlacementMoveInID: this.data.vcsPlacementMoveInID
        };
        this.dialogRef.close(value);
      } else {
        this.toastr.error("Couldn't get Unit Details.");
      }
    }, 100);
  }

  onCancelClick(): void {
    this.dialogRef.close(null);
  }

  ngOnDestroy() {
    if (this.onlineUnitsSub) {
      this.onlineUnitsSub.unsubscribe();
    }
  }
}
