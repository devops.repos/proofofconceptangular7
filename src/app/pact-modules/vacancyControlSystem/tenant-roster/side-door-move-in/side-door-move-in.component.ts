import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISideDoorClientSearchResult } from '../tenant-roster-interface.model';
import { TenantRosterService } from '../tenant-roster.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ReferralRosterService } from '../../referral-roster/referral-roster.service';

@Component({
  selector: 'app-side-door-move-in',
  templateUrl: './side-door-move-in.component.html',
  styleUrls: ['./side-door-move-in.component.scss']
})
export class SideDoorMoveInComponent implements OnInit, OnDestroy {

  referralType = 0; //525 = Regular Referral, 526 = Supportive Housing SideDoor Referral, 610 = Non-Supportive Housing SideDoor Referral

  private sideDoorClientSearchResult: ISideDoorClientSearchResult;

  sideDoorClientSearchResultSub: Subscription;

  constructor(
    private tenantRosterService: TenantRosterService,
    private router: Router,
    private referralRosterService: ReferralRosterService
  ) { }

  ngOnInit() {
    this.sideDoorClientSearchResultSub = this.tenantRosterService.getSideDoorClientSearchResult().subscribe(rslt => {
      if (rslt) {
        this.sideDoorClientSearchResult = rslt;
        if (this.sideDoorClientSearchResult.sideDoorClient) {
          if (this.sideDoorClientSearchResult.sideDoorClient.clientSourceType === 573) {
            // Pact Client
            this.referralType = 526; // Supportive Housing SideDoor Referral
          } else if (this.sideDoorClientSearchResult.sideDoorClient.clientSourceType === 574) {
            // Community Unit Client
            this.referralType = 610; // Non-Supportive Housing SideDoor Referral
          }
          /** Set PlacementOutcomeEditableFlag to make the outcome form editable for sidedoor moveIn */
          this.referralRosterService.setPlacementOutcomeEditableFlag(true);
        }
      } else {
        this.router.navigate(['/vcs/tenant-roster']);
      }
    })
  }

  ngOnDestroy() {
    if (this.sideDoorClientSearchResultSub) {
      this.sideDoorClientSearchResultSub.unsubscribe();
    }
  }

}
