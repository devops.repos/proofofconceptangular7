import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { IVCSTadValidators, IVCSTadCurrentPageIndex, IVCSTadWorkflow, IVCSTadSummary } from './tad-interface.model';
import { TadSite } from '../tad/tad.model';

@Injectable({
  providedIn: 'root'
})
export class TadService {
  private apiURL = environment.pactApiUrl + 'VCSTAD';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  tadDefaultSelectedHpSite: TadSite = {
    vcsTadID: 0,
    agencyID: 0,
    agencyNo: '',
    agencyName: '',
    siteID: 0,
    siteNo: '',
    siteName: '',
    tadMonth: 0,
    tadMonthName: '',
    tadYear: 0,
    tadStatusType: 0,
    tadStatusName: '',
    tadLiasion: 0,
    tadLiasionFirstName: '',
    tadLiasionLastName: '',
    tadLiasionPhone: '',
    tadLiasionEmail: '',
    isUnitRosterComplete: false,
    isReferralRosterComplete: false,
    isTenantRosterComplete: false,
    createdBy: '',
    createdByID: 0,
    createdDate: '',
    updatedBy: '',
    updatedByID: 0,
    updatedDate: '',
    submittedBy: '',
    submittedByID: 0,
    submittedDate: '',
    verifiedBy: '',
    verifiedByID: 0,
    verifiedDate: '',
    transmittedTADReportID: '',
    verifiedTADReportID: '',
    housingLiasionList: []
  }

  tadDefaultPage: IVCSTadCurrentPageIndex = {
    hpSiteID: 0,
    tadMonth: 0,
    tadYear: 0,
    pageIndex: 0
  }

  private isTadWorkflow = new BehaviorSubject<boolean>(false);
  private tadSelectedHpSite = new BehaviorSubject<TadSite>(this.tadDefaultSelectedHpSite);
  private isTadVerificationWorkflow = new BehaviorSubject<boolean>(false);
  private tadVSelectedHpSite = new BehaviorSubject<TadSite>(this.tadDefaultSelectedHpSite);
  private tadCurrentPageIndex = new BehaviorSubject<IVCSTadCurrentPageIndex>(this.tadDefaultPage);  //hpSiteID- as user selected and pageIdex => 1=Unit-roster, 2-referral-roster, 3-tenant-roster, 4-tad
  private isUnitRosterComplete = new BehaviorSubject<boolean>(false);
  private isReferralRosterComplete = new BehaviorSubject<boolean>(false);
  private isTenantRosterComplete = new BehaviorSubject<boolean>(false);
  private isTadHpComplete = new BehaviorSubject<boolean>(false);

  // ####################### TEST DATA ###############################
  validate: IVCSTadValidators[] = [];
  // [{
  //   hpSiteID: null,
  //   rosterNo: 2,
  //   rosterName: "Referral Roster",
  //   validatorID: 7,
  //   ruleName: "Accepted/Pending Approval",
  //   isRuleHard: true,
  //   missingCount: 1,
  //   validationMessage:
  //     "1 Expected Movein date(s) with past date. Please update the expected Movein date to a future date so that you may proceed with yout TAD"
  // }];

  private validd = new BehaviorSubject<IVCSTadValidators[]>(this.validate);

  // #####################################################################

  constructor(private http: HttpClient) { }

  //#region  Get and Set TAD workflow flag
  setIsTadWorkflow(flag: boolean) {
    this.isTadWorkflow.next(flag);
  }
  getIsTadWorkflow() {
    return this.isTadWorkflow.asObservable();
  }
  //#endregion

  //#region  Get and Set TAD Verification workflow flag
  setIsTadVerificationWorkflow(flag: boolean) {
    this.isTadVerificationWorkflow.next(flag);
  }
  getIsTadVerificationWorkflow() {
    return this.isTadVerificationWorkflow.asObservable();
  }
  //#endregion

  //#region Get and Set Selected Hp Site
  setTadSelectedHpSiteID(Site: TadSite) {
    this.tadSelectedHpSite.next(Site);
  }
  getTadSelectedHpSiteID() {
    return this.tadSelectedHpSite.asObservable();
  }
  //#endregion
  //#region Get and Set Selected Hp Site
  setTadVSelectedHpSiteID(Site: TadSite) {
    this.tadVSelectedHpSite.next(Site);
  }
  getTadVSelectedHpSiteID() {
    return this.tadVSelectedHpSite.asObservable();
  }
  //#endregion

  //#region Get and Set Current TAD page selected
  setTadCurrentPageIndex(page: IVCSTadCurrentPageIndex) {
    this.tadCurrentPageIndex.next(page);
  }
  getTadCurrentPageIndex() {
    return this.tadCurrentPageIndex.asObservable();
  }
  //#endregion

  //#region Save the TADWorkflow
  saveTADWorkflow(input: IVCSTadWorkflow) {
    return this.http.post(this.apiURL + '/SaveVCSTadWorkflow', JSON.stringify(input), this.httpOptions);
  }
  //#endregion

  getTadValidators(input: IVCSTadValidators) {
    return this.http.post(this.apiURL + '/GetTADValidators', JSON.stringify(input), this.httpOptions);
    // return this.validd.asObservable();
  }

  //#region get and set TAD sidenav Status
  getIsUnitRosterComplete() {
    return this.isUnitRosterComplete.asObservable();
  }
  setIsUnitRosterComplete(stat: boolean) {
    this.isUnitRosterComplete.next(stat);
  }

  getIsReferralRosterComplete() {
    return this.isReferralRosterComplete.asObservable();
  }
  setIsReferralRosterComplete(stat: boolean) {
    this.isReferralRosterComplete.next(stat);
  }

  getIsTenantRosterComplete() {
    return this.isTenantRosterComplete.asObservable();
  }
  setIsTenanatRosterComplete(stat: boolean) {
    this.isTenantRosterComplete.next(stat);
  }

  getIsTadHpComplete() {
    return this.isTadHpComplete.asObservable();
  }
  setIsTadHpComplete(stat: boolean) {
    this.isTadHpComplete.next(stat);
  }
  //#endregion

  //#region Get and Save TadSummary
  getAndSaveTadSummary(input: IVCSTadSummary) {
    return this.http.post(this.apiURL + '/GetAndSaveTADSummary', JSON.stringify(input), this.httpOptions);
  }
  //#endregion

}
