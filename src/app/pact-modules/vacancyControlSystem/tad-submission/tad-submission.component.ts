import { Component, OnInit, OnDestroy } from '@angular/core';
import { TadService } from './tad.service';
import { IVCSTadValidators, IVCSTadCurrentPageIndex } from './tad-interface.model';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { Router } from '@angular/router';
import { TadSite } from '../tad/tad.model';

@Component({
  selector: 'app-tad-submission',
  templateUrl: './tad-submission.component.html',
  styleUrls: ['./tad-submission.component.scss']
})
export class TadSubmissionComponent implements OnInit, OnDestroy {

  selectedHpSite: TadSite;

  tadValidators: IVCSTadValidators[] = [];

  tadValidatorsSub: Subscription;

  constructor(
    private tadService: TadService,
    private commonService: CommonService,
    private router: Router
  ) { }

  ngOnInit() {
    /* Set IsTadWorkFlow flag to true */
    this.tadService.setIsTadWorkflow(true);

    /* Get the HpSiteID selected in TAD page */
    this.tadService.getTadSelectedHpSiteID().subscribe((site: TadSite) => {
      if (site.siteID > 0) {
        this.selectedHpSite = site;

        /* Call TadValidator to set the correct status of each page in sidenav */
        const validate: IVCSTadValidators = {
          hpSiteID: this.selectedHpSite.siteID,
          rosterNo: null,  // null - gets all the pending validations for all the pages
          rosterName: null,
          validatorID: null,
          ruleName: null,
          isRuleHard: null,
          missingCount: null,
          validationMessage: null,
        }
        this.tadValidatorsSub = this.tadService.getTadValidators(validate).subscribe((res: IVCSTadValidators[]) => {
          this.tadValidators = res
          if (this.selectedHpSite.tadStatusType == 810) {
            // 810 = InProgress
            this.tadValidators.filter(d => d.rosterNo === 1).length <= 0 ? this.tadService.setIsUnitRosterComplete(true) : this.tadService.setIsUnitRosterComplete(false);
            this.tadValidators.filter(d => d.rosterNo === 2).length <= 0 ? this.tadService.setIsReferralRosterComplete(true) : this.tadService.setIsReferralRosterComplete(false);
            this.tadValidators.filter(d => d.rosterNo === 3).length <= 0 ? this.tadService.setIsTenanatRosterComplete(true) : this.tadService.setIsTenanatRosterComplete(false);
            this.tadService.setIsTadHpComplete(false);
          }
          /* get the last Page selected from the previous TAD workflow if any
          and redirect them to that last page where the TAD Workflow was stopped,
          This helps to redirect back to same page from where user left to update referral/Tenant roster */
          this.tadService.getTadCurrentPageIndex().subscribe((res: IVCSTadCurrentPageIndex) => {
            if (res.hpSiteID > 0) {
              // console.log('pageIndex, hpSiteID: ', res.hpSiteID);
              if (this.selectedHpSite.siteID > 0 && res.hpSiteID == this.selectedHpSite.siteID &&
                  res.tadMonth == this.selectedHpSite.tadMonth && res.tadYear == this.selectedHpSite.tadYear) {
                /* it is the same Hpsite, tadMonth, TadYear TAD Workflow previous done */
                if (res.pageIndex == 1) {
                  this.router.navigate(['/vcs/tad/unit-roster']);
                } else if (res.pageIndex == 2) {
                  this.router.navigate(['/vcs/tad/referral-roster']);
                } else if (res.pageIndex == 3) {
                  this.router.navigate(['/vcs/tad/tenant-roster']);
                } else if (res.pageIndex == 4) {
                  this.router.navigate(['/vcs/tad/tad-hp']);
                } else {
                  this.router.navigate(['/vcs/tad/unit-roster']);
                }
              } else {
                /* Different Site Selected, reset the sidenav status */
                this.navigateToSpecificPage();
              }
            } else {
              /* Different Site Selected, reset the sidenav status */
              this.navigateToSpecificPage();
            }
          });
        });
      } else {
        this.router.navigate(['/vcs/tad']);
      }
    });
  }

  navigateToSpecificPage() {
    if (this.selectedHpSite.tadStatusType == 808 || this.selectedHpSite.tadStatusType == 809) {
      // 808 = Not Submitted, 809 = Overdue
      this.tadService.setIsUnitRosterComplete(false);
      this.tadService.setIsReferralRosterComplete(false);
      this.tadService.setIsTenanatRosterComplete(false);
      this.tadService.setIsTadHpComplete(false);
      this.router.navigate(['/vcs/tad/unit-roster']);
    } else if (this.selectedHpSite.tadStatusType == 810) {
      // 810 = InProgress
      if (!this.selectedHpSite.isUnitRosterComplete) {
        this.router.navigate(['/vcs/tad/unit-roster']);
      } else if (!this.selectedHpSite.isReferralRosterComplete) {
        this.router.navigate(['/vcs/tad/referral-roster']);
      } else if (!this.selectedHpSite.isTenantRosterComplete) {
        this.router.navigate(['/vcs/tad/tenant-roster']);
      } else {
        this.router.navigate(['vcs/tad/tad-hp']);
      }
    }
  }

  ngOnDestroy() {
    if (this.tadValidatorsSub) {
      this.tadValidatorsSub.unsubscribe();
    }
  }

}
