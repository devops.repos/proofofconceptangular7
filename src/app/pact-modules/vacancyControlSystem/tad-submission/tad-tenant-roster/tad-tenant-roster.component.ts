import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { TadService } from '../tad.service';
import { IVCSTadCurrentPageIndex, IVCSTadValidators, IVCSTadWorkflow } from '../tad-interface.model';
import { Subscription } from 'rxjs';
import { TadSite } from '../../tad/tad.model';

@Component({
  selector: 'app-tad-tenant-roster',
  templateUrl: './tad-tenant-roster.component.html',
  styleUrls: ['./tad-tenant-roster.component.scss']
})
export class TadTenantRosterComponent implements OnInit, OnDestroy {

  selectedHpSite: TadSite;
  tadValidators: IVCSTadValidators[] = [];
  isTenantRosterComplete = false;
  tadValidationMsg = '';

  tadSelectedHpSiteIDSub: Subscription;
  tadValidatorsSub: Subscription;
  routeSub: Subscription;

  constructor(
    private sidenavStatusService: SidenavStatusService,
    private router: Router,
    private route: ActivatedRoute,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService
  ) { }

  ngOnInit() {
    /* Get the HpSite selected in TAD page */
    this.tadSelectedHpSiteIDSub = this.tadService.getTadSelectedHpSiteID().subscribe((site: TadSite) => {
      if (site.siteID > 0) {
        this.selectedHpSite = site;
        const selectedPage: IVCSTadCurrentPageIndex = {
          hpSiteID: site.siteID,
          tadMonth: site.tadMonth,
          tadYear: site.tadYear,
          pageIndex: 3
        }
        this.tadService.setTadCurrentPageIndex(selectedPage);
        this.callTadValidator(site.siteID);
      } else {
        this.router.navigate(['/vcs/tad']);
      }
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        /* Save the TADWorkFlow */
        if (this.selectedHpSite.siteID > 0) {
          const saveData: IVCSTadWorkflow = {
            VCSTADID: this.selectedHpSite.vcsTadID,
            TADStatusType: 810, //810 = InProgress
            IsTenantRosterComplete: this.isTenantRosterComplete
          }
          this.tadService.saveTADWorkflow(saveData).subscribe(res => {
            if (res > 0) {
              // console.log('tenantRosterWorkFlow Saved.');
            }
          })
        }
      };
    });
  }

  @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
    /* Save TADWorkFlow on Browser Reload */
    if (this.selectedHpSite.siteID > 0) {
      const saveData: IVCSTadWorkflow = {
        VCSTADID: this.selectedHpSite.vcsTadID,
        TADStatusType: 810, //810 = InProgress
        IsTenantRosterComplete: this.isTenantRosterComplete
      }
      this.tadService.saveTADWorkflow(saveData).subscribe(res => {
        if (res > 0) {
          // console.log('tenantRosterWorkFlow Saved.');
        }
      })
    }
  };

  callTadValidator(siteID) {
    /* Get all the TAD validators for Tenant Roster*/
    if (siteID > 0) {
      const validate: IVCSTadValidators = {
        hpSiteID: siteID,
        rosterNo: 3,  // 3- Tenant Roster
        rosterName: null,
        validatorID: null,
        ruleName: null,
        isRuleHard: null,
        missingCount: null,
        validationMessage: null,
      }
      this.tadValidatorsSub = this.tadService.getTadValidators(validate).subscribe((res: IVCSTadValidators[]) => {
        this.tadValidators = res
        // console.log('TR Validators: ', this.tadValidators);
        /* Check and set the sidenav status on first time load */
        if (this.tadValidators.filter(d => d.rosterNo == 3).length > 0) {
          this.isTenantRosterComplete = false;
        } else {
          this.isTenantRosterComplete = true;
        }
        /* Set the TAD sidenav Status on first load */
        this.tadService.setIsTenanatRosterComplete(this.isTenantRosterComplete);

        /* Show the Pop Up Dialog with the list of pending task */
        let tenantRosterValidators = this.tadValidators.filter(d => d.rosterNo === 3);
        this.tadValidationMsg = '';
        if (tenantRosterValidators.length > 0) {
          this.tadValidationMsg += `<i><b>
                Tenant Roster: ` + tenantRosterValidators.length + ` Field(s) Requiring Entry or Correction
              </b>
              <ul>`
          tenantRosterValidators.forEach(t => {
            this.tadValidationMsg += `<li><b>` + t.ruleName + `:</b> ` + t.validationMessage + `</li>`
          });
          this.tadValidationMsg += `</ul></i>`
        }
        // if (this.tadValidationMsg !== '') {
        //   const title = 'Verify';
        //   const primaryMessage = `Please complete the below incomplete section(s) to finalize the survey.`;
        //   const secondaryMessage = this.tadValidationMsg;
        //   const confirmButtonName = 'OK';
        //   const dismissButtonName = '';
        //   this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        //     positiveResponse => { },
        //     negativeResponse => { }
        //   );
        // }
      });
    }
  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.tadService.setIsTadWorkflow(false);
        this.router.navigate(['/vcs/tad']);
      },
      negativeResponse => { }
    );
  }

  NextTadPage() {
    this.sidenavStatusService.routeToTadNextPage(this.router, this.route);
  }

  PreviousTadPage() {
    this.sidenavStatusService.routeToTadPreviousPage(this.router, this.route);
  }

  ngOnDestroy() {
    if (this.tadSelectedHpSiteIDSub) {
      this.tadSelectedHpSiteIDSub.unsubscribe();
    }
    if (this.tadValidatorsSub) {
      this.tadValidatorsSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

}
