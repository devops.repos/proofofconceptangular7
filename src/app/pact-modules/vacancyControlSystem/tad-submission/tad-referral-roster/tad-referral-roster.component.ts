import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { TadService } from '../tad.service';
import { Subscription } from 'rxjs';
import { IVCSTadCurrentPageIndex, IVCSTadValidators, IVCSTadWorkflow } from '../tad-interface.model';
import { TadSite } from '../../tad/tad.model';

@Component({
  selector: 'app-tad-referral-roster',
  templateUrl: './tad-referral-roster.component.html',
  styleUrls: ['./tad-referral-roster.component.scss']
})
export class TadReferralRosterComponent implements OnInit, OnDestroy {

  selectedHpSite: TadSite;
  tadValidators: IVCSTadValidators[] = [];
  isReferralRosterComplete = false;
  referralValidatorIDs = [];
  tadValidationMsg = '';

  tadSelectedHpSiteIDSub: Subscription;
  tadValidatorsSub: Subscription;
  routeSub: Subscription;

  constructor(
    private sidenavStatusService: SidenavStatusService,
    private router: Router,
    private route: ActivatedRoute,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService
  ) { }

  ngOnInit() {
    /* Get the HpSiteID selected in TAD page */
    this.tadSelectedHpSiteIDSub = this.tadService.getTadSelectedHpSiteID().subscribe((site: TadSite) => {
      if (site.siteID > 0) {
        this.selectedHpSite = site;
        const selectedPage: IVCSTadCurrentPageIndex = {
          hpSiteID: site.siteID,
          tadMonth: site.tadMonth,
          tadYear: site.tadYear,
          pageIndex: 2
        }
        this.tadService.setTadCurrentPageIndex(selectedPage);
        /* Get all the TAD validators for Referral Roster */
        const validate: IVCSTadValidators = {
          hpSiteID: site.siteID,
          rosterNo: 2,  // 2- Referral Roster
          rosterName: null,
          validatorID: null,
          ruleName: null,
          isRuleHard: null,
          missingCount: null,
          validationMessage: null,
        }
        this.tadValidatorsSub = this.tadService.getTadValidators(validate).subscribe((res: IVCSTadValidators[]) => {
          this.tadValidators = res
          // console.log('RR Validators: ', this.tadValidators);
          /* Check and set the sidenav status on first time load */
          if (this.tadValidators.filter(d => d.rosterNo == 2).length > 0) {
            this.isReferralRosterComplete = false;
          } else {
            this.isReferralRosterComplete = true;
          }
          /* Set the TAD sidenav Status on first load */
          this.tadService.setIsReferralRosterComplete(this.isReferralRosterComplete);

          /* Show the Pop Up Dialog with the list of pending task */
          let referralRosterValidators = this.tadValidators.filter(d => d.rosterNo === 2);
          this.tadValidationMsg = '';
          if (referralRosterValidators.length > 0) {
            this.tadValidationMsg += `<i><b>
                      Referral Roster: ` + referralRosterValidators.length + ` Field(s) Requiring Entry or Correction
                    </b>
                    <ul>`
            referralRosterValidators.forEach(r => {
              this.referralValidatorIDs.push(r.validatorID);
              this.tadValidationMsg += `<li><b>` + r.ruleName + `:</b> ` + r.validationMessage + `</li>`
            });
            this.tadValidationMsg += `</ul></i>`
          }
          // if (this.tadValidationMsg !== '') {
          //   const title = 'Verify';
          //   const primaryMessage = `Please complete the below incomplete section(s) to finalize the survey.`;
          //   const secondaryMessage = this.tadValidationMsg;
          //   const confirmButtonName = 'OK';
          //   const dismissButtonName = '';
          //   this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          //     positiveResponse => { },
          //     negativeResponse => { }
          //   );
          // }
        });
      } else {
        this.router.navigate(['/vcs/tad']);
      }
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        /* Save the TADWorkFlow */
        if (this.selectedHpSite.siteID > 0) {
          const saveData: IVCSTadWorkflow = {
            VCSTADID: this.selectedHpSite.vcsTadID,
            TADStatusType: 810, //810 = InProgress
            IsReferralRosterComplete: this.isReferralRosterComplete
          }
          this.tadService.saveTADWorkflow(saveData).subscribe(res => {
            if (res > 0) {
              // console.log('ReferralRosterWorkFlow Saved.');
            }
          })
        }
      };
    });
  }

  @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
    /* Save TADWorkFlow on Browser Reload */
    if (this.selectedHpSite.siteID > 0) {
      const saveData: IVCSTadWorkflow = {
        VCSTADID: this.selectedHpSite.vcsTadID,
        TADStatusType: 810, //810 = InProgress
        IsReferralRosterComplete: this.isReferralRosterComplete
      }
      this.tadService.saveTADWorkflow(saveData).subscribe(res => {
        if (res > 0) {
          // console.log('ReferralRosterWorkFlow Saved.');
        }
      })
    }
  };

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.tadService.setIsTadWorkflow(false);
        this.router.navigate(['/vcs/tad']);
      },
      negativeResponse => { }
    );
  }

  NextTadPage() {
    this.sidenavStatusService.routeToTadNextPage(this.router, this.route);
  }

  PreviousTadPage() {
    this.sidenavStatusService.routeToTadPreviousPage(this.router, this.route);
  }

  ngOnDestroy() {
    if (this.tadSelectedHpSiteIDSub) {
      this.tadSelectedHpSiteIDSub.unsubscribe();
    }
    if (this.tadValidatorsSub) {
      this.tadValidatorsSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

}
