
export interface IVCSTadValidators {
  hpSiteID?: number;
  rosterNo?: number;
  rosterName?: string;
  validatorID?: number;
  ruleName?: string;
  isRuleHard?: boolean;
  missingCount?: number;
  validationMessage?: string;
}

export interface IVCSTadSidenavStatus {
  isUnitRosterComplete: number,
  isReferralRosterComplete: number,
  isTenantRosterComplete: number,
  isTadHpComplete: number,
}

export interface IVCSTadCurrentPageIndex {
  hpSiteID: number;
  tadMonth: number;
  tadYear: number;
  pageIndex: number;
}

export interface IVCSTadWorkflow {
  VCSTADID: number;
	TADStatusType: number;
	IsUnitRosterComplete?: boolean;
	IsReferralRosterComplete?: boolean;
	IsTenantRosterComplete?: boolean;
}

export interface IVCSTadSummary {
  vcstadid?: number;
  hpAgencyID?: number;
  hpAgencyNo?: string;
  hpAgencyName?: string;
  hpSiteID?: number;
  hpSiteNo?: string;
  hpSiteName?: string;
  siteType?: number;
  siteTypeDescription?: string;
  siteAddress?: string;
  reportingMonth?: number;
  reportingYear?: number;
  submittedBy?: string;
  submittedDate?: string;
  phoneNo?: string;
  email?: string;
  userID?: number;
  actionID?: number;
  isHoliday?: boolean;
  tadSummary?: IVCSTadSummaryList[];
  tadDetails?: IVCSTadDetailsList[];
}

export interface IVCSTadSummaryList {
  primaryFundingSource: string;
  units: number;
  unitsOccupied: number;
  discrepancies: number;
  unitsOffline: number;
  unitsVacant: number;
  moveIns: number;
  moveOuts: number;
}

export interface IVCSTadDetailsList {
  unitID: number;
  unitName: string;
  primaryFundingSource: string;
  secondaryFundingSource: string;
  tenantFirstName: string;
  tenantLastName: string;
  discrepancyType: number;
  discrepancyTypeDescription: string;
  priorityPlacementType: number;
  priorityPlacementDescription: string;
  clientID: number;
  clientSourceType: number;
  referralDate: string;
  tenantEligibility: string;
  svaPrioritization: string;
  raAgencyID: number;
  raAgencyNo: string;
  raAgencyName: string;
  raSiteID: number;
  raSiteNo: string;
  raSiteName: string;
  moveInDate: string;
  moveOutDate: string;
  moveOutReasonDescription: string;
  moveOutLocationDescription: string;
  unitStatusType: number;
  unitStatusTypeDescription: string;
  unitOfflineReasonID: number;
  unitOfflineReasonDescription: string;
  offlineOtherSpecify: string;
  expectedAvailableDate: string;
  unitAddlComment: string;
  verificationStatusType: number;
  verificationStatusDescription: string;
  verifiedBy: string;
  verificationDate: string;
  verificationReasonDescription: string;
  footNotes: string;
}
