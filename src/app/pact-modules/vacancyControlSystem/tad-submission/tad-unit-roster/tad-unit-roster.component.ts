import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { Subscription } from 'rxjs';
import { TadService } from '../tad.service';
import { IVCSTadCurrentPageIndex, IVCSTadValidators, IVCSTadWorkflow } from '../tad-interface.model';
import { TadSite } from '../../tad/tad.model';

@Component({
  selector: 'app-tad-unit-roster',
  templateUrl: './tad-unit-roster.component.html',
  styleUrls: ['./tad-unit-roster.component.scss']
})
export class TadUnitRosterComponent implements OnInit, OnDestroy {

  selectedHpSite: TadSite;
  tadValidators: IVCSTadValidators[] = [];
  isUnitRosterComplete = false;
  tadValidationMsg = '';

  tadSelectedHpSiteIDSub: Subscription;
  tadValidatorsSub: Subscription;
  routeSub: Subscription;

  constructor(
    private sidenavStatusService: SidenavStatusService,
    private router: Router,
    private route: ActivatedRoute,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService
  ) { }

  ngOnInit() {
    /* Get the HpSiteID selected in TAD page */
    this.tadSelectedHpSiteIDSub = this.tadService.getTadSelectedHpSiteID().subscribe((site: TadSite) => {
      if (site.siteID > 0) {
        this.selectedHpSite = site;
        const selectedPage: IVCSTadCurrentPageIndex = {
          hpSiteID: site.siteID,
          tadMonth: site.tadMonth,
          tadYear: site.tadYear,
          pageIndex: 1
        }
        this.tadService.setTadCurrentPageIndex(selectedPage);
        /* Get all the TAD validators for Unit Roster*/
        const validate: IVCSTadValidators = {
          hpSiteID: site.siteID,
          rosterNo: 1,  // 1- Unit Roster
          rosterName: null,
          validatorID: null,
          ruleName: null,
          isRuleHard: null,
          missingCount: null,
          validationMessage: null,
        }
        this.tadValidatorsSub = this.tadService.getTadValidators(validate).subscribe((res: IVCSTadValidators[]) => {
          this.tadValidators = res
          // console.log('UR Validators: ', this.tadValidators);
          /* Check and set the sidenav status on first time load */
          if (this.tadValidators.filter(d => d.rosterNo == 1).length > 0) {
            this.isUnitRosterComplete = false;
          } else {
            this.isUnitRosterComplete = true;
          }
          /* Set the UnitRoster sidenav Status on page load */
          this.tadService.setIsUnitRosterComplete(this.isUnitRosterComplete);

          /* Show the Pop Up Dialog with the list of pending task */
          let unitRosterValidators = this.tadValidators.filter(d => d.rosterNo === 1);
          this.tadValidationMsg = '';
          if (unitRosterValidators.length > 0) {
            this.tadValidationMsg += `<i><b>
                      Unit Roster: ` + unitRosterValidators.length + ` Field(s) Requiring Entry or Correction
                    </b>
                    <ul>`
            unitRosterValidators.forEach(u => {
              this.tadValidationMsg += `<li><b>` + u.ruleName + `:</b> ` + u.validationMessage + `</li>`
            });
            this.tadValidationMsg += `</ul></i>`
          }
          // if (this.tadValidationMsg !== '') {
          //   const title = 'Verify';
          //   const primaryMessage = `Please complete the below incomplete section(s) to finalize the survey.`;
          //   const secondaryMessage = this.tadValidationMsg;
          //   const confirmButtonName = 'OK';
          //   const dismissButtonName = '';
          //   this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          //     positiveResponse => { },
          //     negativeResponse => { }
          //   );
          // }
        });
      } else {
        this.router.navigate(['/vcs/tad']);
      }
    });
    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        /* Save the TADWorkFlow */
        if (this.selectedHpSite.siteID > 0) {
          const saveData: IVCSTadWorkflow = {
            VCSTADID: this.selectedHpSite.vcsTadID,
            TADStatusType: 810, //810 = InProgress
            IsUnitRosterComplete: this.isUnitRosterComplete
          }
          this.tadService.saveTADWorkflow(saveData).subscribe(res => {
            if (res > 0) {
              // console.log('UnitRosterWorkFlow Saved.');
            }
          })
        }
      };
    });
  }

  @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
    /* Save TADWorkFlow on Browser Reload */
    if (this.selectedHpSite.siteID > 0) {
      const saveData: IVCSTadWorkflow = {
        VCSTADID: this.selectedHpSite.vcsTadID,
        TADStatusType: 810, //810 = InProgress
        IsUnitRosterComplete: this.isUnitRosterComplete
      }
      this.tadService.saveTADWorkflow(saveData).subscribe(res => {
        if (res > 0) {
          // console.log('UnitRosterWorkFlow Saved.');
        }
      })
    }
  };

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.tadService.setIsTadWorkflow(false);
        this.router.navigate(['/vcs/tad']);
      },
      negativeResponse => { }
    );
  }

  NextTadPage() {
    this.sidenavStatusService.routeToTadNextPage(this.router, this.route);
  }

  PreviousTadPage() {
    this.sidenavStatusService.routeToTadPreviousPage(this.router, this.route);
  }

  ngOnDestroy() {
    if (this.tadSelectedHpSiteIDSub) {
      this.tadSelectedHpSiteIDSub.unsubscribe();
    }
    if (this.tadValidatorsSub) {
      this.tadValidatorsSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

  loadTADValidationMsg()
  {
    this.tadSelectedHpSiteIDSub = this.tadService.getTadSelectedHpSiteID().subscribe((site: TadSite) => {
      if (site.siteID > 0) {
        this.selectedHpSite = site;
        const selectedPage: IVCSTadCurrentPageIndex = {
          hpSiteID: site.siteID,
          tadMonth: site.tadMonth,
          tadYear: site.tadYear,
          pageIndex: 1
        }
        this.tadService.setTadCurrentPageIndex(selectedPage);
        /* Get all the TAD validators for Unit Roster*/
        const validate: IVCSTadValidators = {
          hpSiteID: site.siteID,
          rosterNo: 1,  // 1- Unit Roster
          rosterName: null,
          validatorID: null,
          ruleName: null,
          isRuleHard: null,
          missingCount: null,
          validationMessage: null,
        }
        this.tadValidatorsSub = this.tadService.getTadValidators(validate).subscribe((res: IVCSTadValidators[]) => {
          this.tadValidators = res
          // console.log('UR Validators: ', this.tadValidators);
          /* Check and set the sidenav status on first time load */
          if (this.tadValidators.filter(d => d.rosterNo == 1).length > 0) {
            this.isUnitRosterComplete = false;
          } else {
            this.isUnitRosterComplete = true;
          }
          /* Set the UnitRoster sidenav Status on page load */
          this.tadService.setIsUnitRosterComplete(this.isUnitRosterComplete);

          /* Show the Pop Up Dialog with the list of pending task */
          let unitRosterValidators = this.tadValidators.filter(d => d.rosterNo === 1);
          this.tadValidationMsg = '';
          if (unitRosterValidators.length > 0) {
            this.tadValidationMsg += `<i><b>
                      Unit Roster: ` + unitRosterValidators.length + ` Field(s) Requiring Entry or Correction
                    </b>
                    <ul>`
            unitRosterValidators.forEach(u => {
              this.tadValidationMsg += `<li><b>` + u.ruleName + `:</b> ` + u.validationMessage + `</li>`
            });
            this.tadValidationMsg += `</ul></i>`
          }
          // if (this.tadValidationMsg !== '') {
          //   const title = 'Verify';
          //   const primaryMessage = `Please complete the below incomplete section(s) to finalize the survey.`;
          //   const secondaryMessage = this.tadValidationMsg;
          //   const confirmButtonName = 'OK';
          //   const dismissButtonName = '';
          //   this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          //     positiveResponse => { },
          //     negativeResponse => { }
          //   );
          // }
        });
      } else {
        this.router.navigate(['/vcs/tad']);
      }
    });

  }

}
