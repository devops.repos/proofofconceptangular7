import { Component, OnInit, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import 'ag-grid-enterprise';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { Subscription } from 'rxjs';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { TadService } from '../tad.service';
import { IVCSTadCurrentPageIndex, IVCSTadValidators, IVCSTadSummary, IVCSTadSummaryList, IVCSTadDetailsList } from '../tad-interface.model';
import { TadSite } from '../../tad/tad.model';
import { ToastrService } from 'ngx-toastr';
import { TadOfflineInfoIconComponent } from './offline-info-icon.component';
import { TadMoveOutInfoIconComponent } from './moveOut-info-icon.component';
import { TadDiscrepancyIconComponent } from './discrepancy-icon.component';
import { CommonService } from 'src/app/services/helper-services/common.service';

@Component({
  selector: 'app-tad-hp',
  templateUrl: './tad-hp.component.html',
  styleUrls: ['./tad-hp.component.scss']
})
export class TadHPComponent implements OnInit, OnDestroy {

  selectedHpSite: TadSite;

  currentUser: AuthData;
  currentDate = new Date();

  sGridApi;
  sGridColumnApi;
  sColumnDefs;
  tadGridApi;
  tadGridColumnApi;
  tadColumnDefs;

  sdefaultColDef;
  defaultColDef;
  frameworkComponents;
  context;

  public sGridOptions: GridOptions;
  public tadGridOptions: GridOptions;
  tadSummaryData: IVCSTadSummary;
  sRowData: IVCSTadSummaryList[] = [];
  tadRowData: IVCSTadDetailsList[] = [];

  isTadConsent = false;
  tadValidators: IVCSTadValidators[] = [];

  currentUserSub: Subscription;
  tadSelectedHpSiteIDSub: Subscription;
  tadValidatorsSub: Subscription;

  constructor(
    private userService: UserService,
    private sidenavStatusService: SidenavStatusService,
    private router: Router,
    private route: ActivatedRoute,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService,
    private toastr: ToastrService,
    private commonService: CommonService
  ) {
    this.sGridOptions = {
      rowHeight: 35,
    } as GridOptions;
    this.tadGridOptions = {
      rowHeight: 35,
      headerHeight: 50,
      skipHeaderOnAutoSize: true,
      getRowStyle(params) {
        if (params.data.verificationStatusType == 519 || params.data.verificationStatusType == 522) {
          return { background: 'sandybrown' };
        }
        if (params.data.unitStatusType == 473) {
          return { background: '#E5E8E8', color: 'rgba(0, 0, 0, 0.38)' };
        }
      }
    } as GridOptions;
    this.sColumnDefs = [
      {
        headerName: 'Primary Service Contract',
        field: 'primaryFundingSource',
        minWidth: 400,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Total Units',
        field: 'units',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Occupied',
        field: 'unitsOccupied',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Discrepancies',
        field: 'discrepancies',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Offline',
        field: 'unitsOffline',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Vacant',
        field: 'unitsVacant',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Move-Ins',
        field: 'moveIns',
        minWidth: 100,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Move-Outs',
        field: 'moveOuts',
        minWidth: 100,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },

    ];
    this.tadColumnDefs = [
      {
        headerName: 'Unit#',
        field: 'unitName',
        width: 80,
        suppressSizeToFit: true,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Primary Service Contract',
        field: 'primaryFundingSource',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Rental Subsidies',
        field: 'secondaryFundingSource',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Client# - ReferralDate',
        field: 'clientNoReferralDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.clientID && params.data.referralDate) {
            return params.data.clientID + ' - ' + params.data.referralDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Tenant Name (L,F)',
        field: 'tenantName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.tenantFirstName && params.data.tenantLastName && params.data.footNotes) {
            return params.data.tenantLastName + ', ' + params.data.tenantFirstName + ' ' + params.data.footNotes;
          } else if (params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantLastName + ', ' + params.data.tenantFirstName;
          } else if (params.data.tenantFirstName && !params.data.tenantLastName) {
            return params.data.tenantLastName + ' ' + params.data.footNotes;
          } else if (!params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantFirstName + ' ' + params.data.footNotes;
          } else {
            return '';
          }
        },
        // cellRendererSelector(params) {
        //   const discrepancyIcon = {
        //       component: 'discrepancyIconRenderer'
        //   };
        //   if (params.data.discrepancyType > 0 || params.data.priorityPlacementType > 0) {
        //       return discrepancyIcon;
        //   } else {
        //       return null;
        //   }
        // }
      },
      // {
      //   headerName: 'Client# - Referral Date',
      //   field: 'clientNoReferralDate',
      //   filter: 'agTextColumnFilter',
      //   valueGetter(params) {
      //     if (params.data.clientID && params.data.referralDate) {
      //       return params.data.clientID + ' - ' + params.data.referralDate;
      //     } else if (params.data.clientID) {
      //       return params.data.clientID;
      //     } else if (params.data.referralDate) {
      //       return params.data.referralDate;
      //     } else {
      //       return '';
      //     }
      //   }
      // },
      {
        headerName: 'Referring Agency/Site',
        field: 'raAgencySiteName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.raAgencyID && params.data.raSiteID) {
            return params.data.raAgencyNo + ' - ' + params.data.raAgencyName + ' / ' + params.data.raSiteNo + ' - ' + params.data.raSiteName;
          } else if (params.data.raAgencyID) {
            return params.data.raAgencyNo + ' - ' + params.data.raAgencyName;
          } else if (params.data.raSiteID) {
            return params.data.raSiteNo + ' - ' + params.data.raSiteName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Tenant Eligibility',
        field: 'tenantEligibility',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Move-In Date',
        field: 'moveInDate',
        width: 90,
        suppressSizeToFit: true,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Move-Out Date',
        field: 'moveOutDate',
        width: 120,
        suppressSizeToFit: true,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
        cellRendererSelector(params) {
          const infoIcon = {
            component: 'moveOutInfoIconRenderer'
          };
          if (params.data.moveOutDate) {
            return infoIcon;
          } else {
            return null;
          }
        }
      },
      // {
      //   headerName: 'Unit Status',
      //   field: 'unitStatusTypeDescription',
      //   width: 120,
      //   suppressSizeToFit: true,
      //   filter: 'agTextColumnFilter',
      //   cellRendererSelector(params) {
      //     const infoIcon = {
      //       component: 'offlineInfoIconRenderer'
      //     };
      //     if (params.data.unitStatusType == 473) {
      //       return infoIcon;
      //     } else {
      //       return null;
      //     }
      //   }
      // },
      {
        headerName: 'Verification Status',
        field: 'verificationStatusDescription',
        width: 180,
        suppressSizeToFit: true,
        filter: 'agTextColumnFilter'
      },
    ];

    this.sdefaultColDef = {
      sortable: true,
      resizable: true,
    };
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      cellStyle: { 'white-space': 'normal' },
      autoHeight: true,
      floatingFilter: true
    };
    this.frameworkComponents = {
      offlineInfoIconRenderer: TadOfflineInfoIconComponent,
      moveOutInfoIconRenderer: TadMoveOutInfoIconComponent,
      discrepancyIconRenderer: TadDiscrepancyIconComponent
    };
  }

  ngOnInit() {
    /* Get the HpSiteID selected in TAD page */
    this.tadSelectedHpSiteIDSub = this.tadService.getTadSelectedHpSiteID().subscribe((site: TadSite) => {
      if (site.siteID > 0) {
        this.selectedHpSite = site;
        const selectedPage: IVCSTadCurrentPageIndex = {
          hpSiteID: site.siteID,
          tadMonth: site.tadMonth,
          tadYear: site.tadYear,
          pageIndex: 4
        }
        this.tadService.setTadCurrentPageIndex(selectedPage);
        /* Set the TAD sidenav Status on first load */
        this.tadService.setIsTadHpComplete(false);

        /* Get the TAD summary */
        const tadSummaryInput: IVCSTadSummary = {
          vcstadid: this.selectedHpSite.vcsTadID,
          actionID: 1   // 1 = Get, 2 = submit
        }
        this.tadService.getAndSaveTadSummary(tadSummaryInput).subscribe((sum: IVCSTadSummary) => {
          if (sum.vcstadid > 0) {
            this.tadSummaryData = sum;
            // console.log(this.tadSummaryData);
            if (sum.tadSummary.length > 0) {
              this.sRowData = sum.tadSummary;
              setTimeout(() => { this.sGridApi.sizeColumnsToFit(); }, 50);
            }
            if (sum.tadDetails.length > 0) {
              this.tadRowData = sum.tadDetails;
              setTimeout(() => { this.tadGridApi.sizeColumnsToFit(); }, 50);
            }
          }
        });
      } else {
        this.router.navigate(['/vcs/tad']);
      }
    });

    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
      }
    });
  }

  onSGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.sGridApi = params.api;
    this.sGridColumnApi = params.columnApi;
  }
  onTadGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.tadGridApi = params.api;
    this.tadGridColumnApi = params.columnApi;

    // const allColumnIds = [];
    // this.tadGridColumnApi.getAllColumns().forEach(column => {
    //   if (column.colId !== 'action') {
    //     if (column.colId !== 'unitStatusTypeDescription') {
    //       // console.log('column.colID: ' + column.colId);
    //       allColumnIds.push(column.colId);
    //     }
    //   }
    // });
    // this.tadGridColumnApi.autoSizeColumns(allColumnIds);
    // // params.api.expandAll();
  }

  refreshAgGrid(val: string) {
    this.tadGridOptions.api.setFilterModel(null);
  }

  onBtExport(val: string) {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'TADReport-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.tadGridApi.exportDataAsExcel(params);
  }

  //On Consent Check Change
  onIsTadConsentCheckChange(event: { checked: Boolean }) {
    if (event.checked) {
      this.isTadConsent = true;
      this.tadService.setIsTadHpComplete(true);
    }
    else {
      this.isTadConsent = false;
      this.tadService.setIsTadHpComplete(false);
    }
  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.tadService.setIsTadWorkflow(false);
        this.router.navigate(['/vcs/tad']);
      },
      negativeResponse => { }
    );
  }

  onSubmitClick() {
    // Check for Holidays and business Hours 8:00M to 8:00PM
    if (!this.tadSummaryData.isHoliday && this.commonService.isBusinessHours8AMto8PM()) {
      /* Get all the TAD validators */
      if (this.selectedHpSite.siteID > 0 && this.isTadConsent) {
        const validate: IVCSTadValidators = {
          hpSiteID: this.selectedHpSite.siteID,
          rosterNo: null,  // null - gets all the pending validations for all the pages
          rosterName: null,
          validatorID: null,
          ruleName: null,
          isRuleHard: null,
          missingCount: null,
          validationMessage: null,
        }
        this.tadValidatorsSub = this.tadService.getTadValidators(validate).subscribe((res: IVCSTadValidators[]) => {
          this.tadValidators = res
          let unitRosterHardValidators = this.tadValidators.filter(d => d.rosterNo === 1 && d.isRuleHard);
          let referralRosterHardValidators = this.tadValidators.filter(d => d.rosterNo === 2 && d.isRuleHard);
          let tenantRosterHardValidators = this.tadValidators.filter(d => d.rosterNo === 3 && d.isRuleHard);
          let unitRosterSoftValidators = this.tadValidators.filter(d => d.rosterNo === 1 && !d.isRuleHard);
          let referralRosterSoftValidators = this.tadValidators.filter(d => d.rosterNo === 2 && !d.isRuleHard);
          let tenantRosterSoftValidators = this.tadValidators.filter(d => d.rosterNo === 3 && !d.isRuleHard);

          if (unitRosterHardValidators.length > 0 || referralRosterHardValidators.length > 0 || tenantRosterHardValidators.length > 0) {
            let msg = '';
            if (unitRosterHardValidators.length > 0) {
              msg += `<p><b>
                        Unit Roster: ` + unitRosterHardValidators.length + ` Field(s) Requiring Entry or Correction
                      </b></p>
                      <ul>`
              unitRosterHardValidators.forEach(u => {
                msg += `<li><b>` + u.ruleName + `:</b> ` + u.validationMessage + `</li>`
              });
              msg += `</ul>`
            }
            if (referralRosterHardValidators.length > 0) {
              msg += `<p><b>
                        Referral Roster: ` + referralRosterHardValidators.length + ` Field(s) Requiring Entry or Correction
                      </b></p>
                      <ul>`
              referralRosterHardValidators.forEach(r => {
                msg += `<li><b>` + r.ruleName + `:</b> ` + r.validationMessage + `</li>`
              });
              msg += `</ul>`
            }
            if (tenantRosterHardValidators.length > 0) {
              msg += `<p><b>
                        Tenant Roster: ` + tenantRosterHardValidators.length + ` Field(s) Requiring Entry or Correction
                      </b></p>
                      <ul>`
              tenantRosterHardValidators.forEach(t => {
                msg += `<li><b>` + t.ruleName + `:</b> ` + t.validationMessage + `</li>`
              });
              msg += `</ul>`
            }
            // console.log('RR Validators: ', this.tadValidators);
            const title = 'Verify';
            const primaryMessage = `Please complete the below incomplete section(s) to finalize the survey.`;
            const secondaryMessage = msg;
            const confirmButtonName = 'OK';
            const dismissButtonName = '';
            this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
              positiveResponse => { },
              negativeResponse => { }
            );
          } else if (unitRosterSoftValidators.length > 0 || referralRosterSoftValidators.length > 0 || tenantRosterSoftValidators.length > 0) {
            let msg = '';
            if (unitRosterSoftValidators.length > 0) {
              msg += `<p><b>
                        Unit Roster: ` + unitRosterSoftValidators.length + ` Field(s) Requiring Entry or Correction
                      </b></p>
                      <ul>`
              unitRosterSoftValidators.forEach(u => {
                msg += `<li><b>` + u.ruleName + `:</b> ` + u.validationMessage + `</li>`
              });
              msg += `</ul>`
            }
            if (referralRosterSoftValidators.length > 0) {
              msg += `<p><b>
                        Referral Roster: ` + referralRosterSoftValidators.length + ` Field(s) Requiring Entry or Correction
                      </b></p>
                      <ul>`
              referralRosterSoftValidators.forEach(r => {
                msg += `<li><b>` + r.ruleName + `:</b> ` + r.validationMessage + `</li>`
              });
              msg += `</ul>`
            }
            if (tenantRosterSoftValidators.length > 0) {
              msg += `<p><b>
                        Tenant Roster: ` + tenantRosterSoftValidators.length + ` Field(s) Requiring Entry or Correction
                      </b></p>
                      <ul>`
              tenantRosterSoftValidators.forEach(t => {
                msg += `<li><b>` + t.ruleName + `:</b> ` + t.validationMessage + `</li>`
              });
              msg += `</ul>`
            }
            // console.log('RR Validators: ', this.tadValidators);
            const title = 'Verify';
            const primaryMessage = `<p>You still have below incomplete section(s) to finalize the survey.</p>Do you Still want to continue?`;
            const secondaryMessage = msg;
            const confirmButtonName = 'Yes';
            const dismissButtonName = 'Cancel';
            this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
              positiveResponse => {
                /* Ready for TAD Submission */
                this.submitTadReport();
              },
              negativeResponse => { }
            );
          } else {
            /* Ready for TAD Submission */
            this.submitTadReport();
          }
        });
      }
    } else {
      const title = 'Alert';
      const primaryMessage = `TAD can be submitted only From Monday through Friday on HRA business days from 8:00AM to 8:00PM.`;
      const secondaryMessage = 'Please try during business hours.';
      const confirmButtonName = 'OK';
      const dismissButtonName = '';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => { },
        negativeResponse => { }
      );
    }

  }

  submitTadReport() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to Submit the TAD? `;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        /* Ready for TAD Submission */
        const tadSummaryInput: IVCSTadSummary = {
          vcstadid: this.selectedHpSite.vcsTadID,
          actionID: 2   // 1 = Get, 2 = submit
        }
        this.tadService.getAndSaveTadSummary(tadSummaryInput).subscribe((sum: IVCSTadSummary) => {
          if (sum.vcstadid > 0) {
            this.toastr.success('TAD has been submitted.');
            this.tadService.setIsTadWorkflow(false);
            this.router.navigate(['/vcs/tad']);
          }
        });
      },
      negativeResponse => { }
    );

  }

  NextTadPage() {
    this.sidenavStatusService.routeToTadNextPage(this.router, this.route);
  }

  PreviousTadPage() {
    this.sidenavStatusService.routeToTadPreviousPage(this.router, this.route);
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.tadSelectedHpSiteIDSub) {
      this.tadSelectedHpSiteIDSub.unsubscribe();
    }
    if (this.tadValidatorsSub) {
      this.tadValidatorsSub.unsubscribe();
    }
  }
}
