import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-discrepancy-icon',
  template: `
    <p  class="tad-discrepancy-icon">
      <span>{{ params.value }}
        <span matBadge="{{params.data.discrepancyType ? 'D' : '' }}" matBadgeOverlap="false"></span>
        <span matBadge="{{params.data.priorityPlacementType ? 'P' : '' }}" matBadgeOverlap="false">{{params.data.priorityPlacementType ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : ''}}</span>
      </span>
      <!-- <span *ngIf="params.data.priorityPlacementType" matBadge="P" matBadgeOverlap="false">{{ params.value }}</span> -->
    </p>
  `,
  styles: [``]
})
export class TadDiscrepancyIconComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }
}
