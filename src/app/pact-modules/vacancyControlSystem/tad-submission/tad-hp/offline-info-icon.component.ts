import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-tad-offline-info-icon',
  template: `
    <span class="tr-info-icon">
        {{params.value}}<mat-icon class="tr-mat-info-icon" matTooltip="{{tooltipMessage}}" matTooltipClass="vcs-info-tooltip">info</mat-icon>
    </span>
  `,
  styles: [` `]
})
export class TadOfflineInfoIconComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;
  tooltipMessage: string;

  agInit(params: any): void {
    this.params = params;
    // console.log(this.params);
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    this.tooltipMessage = (params.data.unitID && params.data.unitStatusType == 473 && params.data.unitOfflineReasonID !== 6) ?
      `This Unit is offline, if you would like to update this unit as online, please go to the Agency/Site Setup and update the status of the Unit.` +
      `\n Offline Reason: ` + params.data.offlineReasonTypeDescription +
      `\n Expected Available Date: ` + params.data.expectedAvailableDate +
      `\n Comments: ` + params.data.unitAddlComment
      : (params.data.unitID && params.data.unitStatusType == 473 && params.data.unitOfflineReasonID == 6) ?
        `This Unit is offline, if you would like to update this unit as online, please go to the Agency/Site Setup and update the status of the Unit.` +
        `\n Offline Reason: ` + params.data.offlineOtherSpecify +
        `\n Expected Available Date: ` + params.data.expectedAvailableDate +
        `\n Comments: ` + params.data.unitAddlComment
        : '';
  }

  refresh(): boolean {
    return false;
  }
}
