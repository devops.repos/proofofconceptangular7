import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-tad-moveout-info-icon',
  template: `
    <span class="tr-info-icon">
        {{params.value}}<mat-icon class="tr-mat-info-icon" matTooltip="{{tooltipMessage}}" matTooltipClass="vcs-info-tooltip">info</mat-icon>
    </span>
  `,
  styles: [` `]
})
export class TadMoveOutInfoIconComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;
  tooltipMessage: string;

  agInit(params: any): void {
    this.params = params;
    // console.log(this.params);
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    this.tooltipMessage = (params.data.moveOutDate) ?
      `\n Reason Moved: ` + params.data.moveOutReasonDescription +
      `\n Location Moved To: ` + params.data.moveOutLocationDescription
      : '';
  }

  refresh(): boolean {
    return false;
  }
}
