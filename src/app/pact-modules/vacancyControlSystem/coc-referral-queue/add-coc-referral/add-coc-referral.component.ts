import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { CoCService } from './../coc-referral-queue.service';
import { VCSReferralSearchData } from '../coc-referral-queue.model';
import { ToastrService } from "ngx-toastr";
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-add-coc-referral',
  templateUrl: './add-coc-referral.component.html',
  styleUrls: ['./add-coc-referral.component.scss']
})
export class AddCocReferralComponent implements OnInit, OnDestroy {

  addCoCSub: Subscription;
  searchReferralSub: Subscription;

  selectedDate: Date;
  selectedClient: number;
  add_btn_disable: boolean = false;
  add_btn_visible: boolean = false;
  selectedReferralID: number;
  selectedClientName: string;

  now = new Date();

  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  rowSelection;  

  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;
  
  public gridOptions: GridOptions;
  rowData: VCSReferralSearchData[];
  searchresult: VCSReferralSearchData[];

  constructor(private cocService: CoCService,
              private toastr: ToastrService,
              private confirmDialogService: ConfirmDialogService) { 
    
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please provide valid search parameters.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Referrals Available</span>';

    this.gridOptions = {
      rowHeight: 35     
    } as GridOptions;

    this.columnDefs = [  
      {
        headerName: 'VCSReferralID',
        field: 'vcsReferralID',
        hide: true
      },    
      {
        headerName: 'Client Name',
        //field: 'clientName',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName ;
        }
        
      },   
      {
        headerName: 'HRA Client#',
        field: 'pactClientID'
      }, 
      {
        headerName: 'Referral Date',
        field: 'referralDate'
      }, 
      {
        headerName: 'Referring Agency/Site',
        //field: 'referringAgencySite',
        valueGetter(params) {
          if (params.data.referringAgencyName !== '' && params.data.referringSiteName !== '') {
            return params.data.referringAgencyName + '/' + params.data.referringSiteName;
          } else if (
            params.data.referringAgencyName !== '' &&
            params.data.referringSiteName === ''
          ) {
            return params.data.referringAgencyName;
          } else if (
            params.data.referringAgencyName === '' &&
            params.data.referringSiteName !== ''
          ) {
            return params.data.referringSiteName;
          }
        }
      },   
      {
        headerName: 'Referred By',
        field: 'referredBy'
      },
      {
        headerName: 'Housing Provider Agency/Site',
        valueGetter(params) {
          if (params.data.hpAgencyName !== '' && params.data.hpSiteName !== '') {
            return params.data.hpAgencyName + '/' + params.data.hpSiteName;
          } else if (
            params.data.hpAgencyName !== '' &&
            params.data.hpSiteName === ''
          ) {
            return params.data.hpAgencyName;
          } else if (
            params.data.hpAgencyName === '' &&
            params.data.hpSiteName !== ''
          ) {
            return params.data.hpSiteName;
          }
        }
      },
      {
        headerName: 'Select',
        field: 'select',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        checkboxSelection: true
    }
    ];
    this.defaultColDef = {
      resizable: true
    };
    this.rowSelection = 'single';
  }

  ngOnInit() {
  }


  onRefresh() {    
    if(this.selectedClient && this.selectedDate) {
      this.searchReferralSub = this.cocService.searchReferrals(this.selectedDate.toString(), this.selectedClient.toString()).subscribe(
        data => {
          if (data){
            this.searchresult = data.body as VCSReferralSearchData[];
            if(this.searchresult.length == 0){
              this.toastr.error("No referrals available.", "Error");
            }
            else if(this.searchresult[0].pactClientID == 0){
              this.toastr.error("Client does not exists.", "Error");
            }
            else {
              this.rowData = this.searchresult;
              this.add_btn_visible = true;  
              this.add_btn_disable = false;       
            } 
          }
      },
      error =>
        this.toastr.error("Could not perform search.", "Error"));

    }
    else {
      this.toastr.error("Please provide Client ID and Referral Date.", "Error");
    }
  }

  onRowSelected(event) {
    if (event.node.selected) {
     if(event.node.data.isCoC) {
      this.add_btn_disable = true;
      this.toastr.error("The referral exists in the CoC Referral queue.", "")
     }
     else {
      this.add_btn_disable = false;
      this.selectedReferralID = event.node.data.vcsReferralID;
      this.selectedClientName = event.node.data.lastName + ', ' + event.node.data.firstName;
     }
    }
    
  }

  onAddClick(){
    this.addCoCSub = this.cocService.addToCoCqueue(this.selectedReferralID).subscribe(
      data => {
        const title = 'CoC innitiated';
        const primaryMessage = `The CoC Referral has been innititated for ` + this.selectedClientName;
        const secondaryMessage = ``;
        const confirmButtonName = 'OK';
        const dismissButtonName = '';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              this.onRefresh();
            },
            negativeResponse => console.log()
          );

    },
      error =>
          this.toastr.error("Referral was not added to CoC", "Error"));

  }
  
  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;    

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'select') {
        
          allColumnIds.push(column.colId);
        
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
    this.gridApi.sizeColumnsToFit();
  }

  ngOnDestroy(){
    if(this.addCoCSub) {
      this.addCoCSub.unsubscribe();
    }
    if(this.searchReferralSub) {
      this.searchReferralSub.unsubscribe();
    }

  }
  
}
