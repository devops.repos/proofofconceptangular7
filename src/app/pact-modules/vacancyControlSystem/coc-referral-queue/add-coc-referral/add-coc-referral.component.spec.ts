import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCocReferralComponent } from './add-coc-referral.component';

describe('AddCocReferralComponent', () => {
  let component: AddCocReferralComponent;
  let fixture: ComponentFixture<AddCocReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCocReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCocReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
