import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { VCSCoCReferralData, VCSCoCPendingReview } from './coc-referral-queue.model';

@Injectable({
  providedIn: 'root'
})
export class CoCService {
  private apiURL = environment.pactApiUrl + 'VCSCoCReferral'; 

  private cocReferralSelected = new BehaviorSubject<VCSCoCReferralData>(null);

  //private referralSiteSelectedList = new BehaviorSubject<CoCReferralData[]>(null);

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  setCoCReferralSelected(data: VCSCoCReferralData){
    this.cocReferralSelected.next(data);  
  }

  getCoCReferralSelected(): Observable<VCSCoCReferralData> {
    return this.cocReferralSelected.asObservable();
  }

   getCoCReferralList(aID: string, sID: string, sType: string) {
     //return this.http.get(this.apiURL + '/GetCoCReferralList');
     const getUrl = environment.pactApiUrl + 'VCSCoCReferral/GetCoCReferralList/';
     return this.http.get(getUrl, { params: { agencyID: aID, siteID: sID, siteType: sType }, observe: 'response'});
   }

   getCoCAgencyList(agencyID: number){
    const getUrl = environment.pactApiUrl + 'VCSCoCReferral/GetCoCAgencyList/';
    return this.http.get(getUrl + agencyID);
   }

   getCoCSiteList(agencyID: number){
    const getUrl = environment.pactApiUrl + 'VCSCoCReferral/GetCoCSiteList/';
    return this.http.get(getUrl + agencyID);
   }

   getUserSite(optionUserId: number){
    const getUrl = environment.pactApiUrl + 'VCSCoCReferral/GetUserSite/';
    return this.http.get(getUrl + optionUserId);
   }

   updateCoCReferral(cocReferral: VCSCoCReferralData){
     const postUrl = environment.pactApiUrl + 'VCSCoCReferral/UpdateCoCReferral/';
     return this.http.post(postUrl, cocReferral, this.httpOptions);
   }

   getCoCReferralActivity(vcsCoCRefID: number){
    const getUrl = environment.pactApiUrl + 'VCSCoCReferral/GetCoCReferralActivity/';
    return this.http.get(getUrl + vcsCoCRefID);
   }


///Add CoC Referrals
searchReferrals(selectedDate: string, selectedClient: string){
  const getUrl = environment.pactApiUrl + 'VCSCoCReferral/SearchReferrals/';
  return this.http.get(getUrl, { params: { refDate: selectedDate, clientID: selectedClient }, observe: 'response'});
}

addToCoCqueue(selectedReferralID: number){
  const postUrl = environment.pactApiUrl + 'VCSCoCReferral/AddToCoCQueue/';
  return this.http.post(postUrl, selectedReferralID, this.httpOptions);
}


savePendingCoCReferral(reviewReasonList: VCSCoCPendingReview[]){
  const postUrl = environment.pactApiUrl + 'VCSCoCReferral/SavePendingCoCReferral/';
     return this.http.post(postUrl, reviewReasonList, this.httpOptions);
}

getCoCReferralReasonsList(vcsCoCReferralID: number){
  const getUrl = environment.pactApiUrl + 'VCSCoCReferral/GetCoCReferralReasonsList/';
  return this.http.get(getUrl + vcsCoCReferralID);
}
}
