import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { VCSCoCReferralData, VCSCoCAgencyList, VCSCoCSiteList } from './coc-referral-queue.model';
import { CoCService } from './coc-referral-queue.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { CommonService } from '../../../services/helper-services/common.service';
import { CoCActionComponent } from './coc-action.component';
import { RefGroupDetails } from "src/app/models/refGroupDetailsDropDown.model";
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { UserSiteType } from 'src/app/models/pact-enums.enum';

@Component({
  selector: 'app-coc-referral-queue',
  templateUrl: './coc-referral-queue.component.html',
  styleUrls: ['./coc-referral-queue.component.scss']
})
export class CocReferralQueueComponent implements OnInit, OnDestroy {


  numberOfCoC: number = 0;
  //widthSideBar: number;
  commonServiceSub: Subscription;
  //widthSideBarSub:Subscription;
  rowDataSub: Subscription;
  agencyListSub: Subscription;
  siteListSub: Subscription

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  rowSelection;
  frameworkComponents;
  context;

  userData: AuthData;
  is_SH_PE = false;
  is_SH_HP = false;
  is_CAS = false;
  fullAgencyList: VCSCoCAgencyList[] = [];
  fullSiteList: VCSCoCSiteList[] = [];
  agencyList: VCSCoCAgencyList[] = [];
  siteList: VCSCoCSiteList[] = [];
  approvalStatusList: RefGroupDetails[];
  selectedAgencyID: number = 0; 
  selectedSiteID: number = 0;
  selectedStatusID: number = 0;

  userDataSub: Subscription;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  public gridOptions: GridOptions;
  rowData: VCSCoCReferralData[] = [];
  fullRowData: VCSCoCReferralData[] = [];


  constructor(private cocService: CoCService,
              private userService: UserService,
              private commonService:CommonService,
              private navService: NavService) { 

    this.gridOptions = {
      rowHeight: 35     
    } as GridOptions;

    this.columnDefs = [      
      {
        headerName: 'Client Name',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.firstName + ' ' + params.data.lastName;
        }
        
      },      
      {
        headerName: 'Referring Agency/Site',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.referringAgencyName && params.data.referringSiteName) {
            return params.data.referringAgencyName + '/' + params.data.referringSiteName;
          } else if ( params.data.referringAgencyName ) {
            return params.data.referringAgencyName;
          } else if (params.data.referringAgencyName ) {
            return params.data.referringSiteName;
          }
        }
      },
      {
        headerName: 'HP Agency/Site',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.hpAgencyName && params.data.hpSiteName) {
            return params.data.hpAgencyName + '/' + params.data.hpSiteName;
          } else if (params.data.hpAgencyName ) {
            return params.data.hpAgencyName;
          } else if ( params.data.hpAgencyName ) {
            return params.data.hpSiteName;
          }
        }
      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        width: 250,
        suppressSizeToFit: true,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },    
      {
        headerName: 'Placements Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },   
      {
        headerName: 'CoC Approval Status',
        field: 'coCStatusName',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Last Updated By',
        field: 'updatedBy',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Last Updated Date',
        field: 'updatedDate',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Last Email Notification',
        field: 'lastEmailNotification',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'       
      } 
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: CoCActionComponent
    };
  }
  

  ngOnInit() {
    // /** Getting the width of the sidenav to reflect the changes to nav toggle */
    // this.widthSideBarSub = this.navService.getWidthSideBar().subscribe(res => {
    //   this.widthSideBar = res;
    //   console.log(this.widthSideBar);
    // });

    /** Getting the currently active user info */
    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.userData = userdata;
        /** Checking the User siteCategoryType either PE or HP */
        if(this.userData.siteCategoryType.length > 0) {
          this.is_SH_PE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_PE);
          this.is_SH_HP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_HP);
          this.is_CAS = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.CAS);
        }  
        if  (this.is_SH_HP){
          this.selectedAgencyID = this.userData.agencyId;
        }
        
        /** Get the Agency List */
        this.agencyListSub = this.cocService.getCoCAgencyList(this.selectedAgencyID).subscribe(res => {
          if (res) {
            this.agencyList = this.fullAgencyList = res as VCSCoCAgencyList[];
          }
        });
         
        this.siteListSub = this.cocService.getCoCSiteList(this.selectedAgencyID).subscribe(res => {
          if (res) {
            this.siteList = this.fullSiteList = res as VCSCoCSiteList[];
            if(this.is_SH_HP) {
              this.siteList = this.siteList.filter( d=> { return d.siteCategoryType === 7});
            }
          }
        });
      }

    /** API call to get the grid data */
    var siteType = this.is_SH_HP ? '7' : '0'
    this.rowDataSub = this.cocService.getCoCReferralList(this.selectedAgencyID.toString(), this.selectedSiteID.toString(), siteType).subscribe(res => {
      this.rowData = this.fullRowData = res.body as VCSCoCReferralData[];
      
        if(this.rowData != null){
          this.numberOfCoC = this.rowData.length;
        }
      });
    });


    this.commonServiceSub = this.commonService.getRefGroupDetails("56").subscribe(
      res => {
        const data = res.body as RefGroupDetails[];  
          
        this.approvalStatusList = data.filter(d => { return d.refGroupID === 56;
        });
             
      },
      error => console.error("Error!", error)
    );


  }

  
  onAgencySelected() {
    if (this.selectedAgencyID > 0) {
      if (this.selectedStatusID > 0){
        if (this.is_SH_HP){
          this.rowData = this.fullRowData.filter(d => { return d.hpAgencyID == this.selectedAgencyID  && d.coCStatusType == this.selectedStatusID;});
        } else {     
          this.rowData = this.fullRowData.filter(d => { return d.referringAgencyID == this.selectedAgencyID  && d.coCStatusType == this.selectedStatusID;});}
        }
      else {
        if (this.is_SH_HP) {
          this.rowData = this.fullRowData.filter(d => { return d.hpAgencyID == this.selectedAgencyID;});
        }  
        else {
          this.rowData = this.fullRowData.filter(d => { return d.referringAgencyID == this.selectedAgencyID;});
        }
       
      }
      this.siteList = this.fullSiteList.filter(d => { return d.agencyID == this.selectedAgencyID;  });
      this.selectedSiteID = 0;
      /* this.siteListSub = this.cocService.getCoCSiteList(this.selectedAgencyID).subscribe(res => {
        this.siteList = res as VCSCoCSiteList[];
      }); */
    }  
    else {
      //this.agencyList = this.fullAgencyList 
      this.siteList = this.fullSiteList;
      this.selectedSiteID = 0;
      if (this.selectedStatusID > 0){
        this.rowData = this.fullRowData.filter(d => { return d.coCStatusType == this.selectedStatusID;}); 
      }
      else {
        this.rowData = this.fullRowData;
      }
    }
    if(this.rowData != null){
      this.numberOfCoC = this.rowData.length;
    }
    else {
      this.numberOfCoC = 0;
    }
  }

  onSiteSelected(){
    if (this.selectedSiteID > 0) {
      if (this.selectedStatusID > 0){
        if (this.is_SH_HP){
          this.rowData = this.fullRowData.filter(d => { return d.hpSiteID == this.selectedSiteID && d.coCStatusType == this.selectedStatusID});
        }  
        else {
          this.rowData = this.fullRowData.filter(d => { return d.referringSiteID == this.selectedSiteID && d.coCStatusType == this.selectedStatusID});}
        }
      else {
        if (this.is_SH_HP){
          this.rowData = this.fullRowData.filter(d => { return d.hpSiteID == this.selectedSiteID;});
        } 
        else{ 
          this.rowData = this.fullRowData.filter(d => { return d.referringSiteID == this.selectedSiteID;});
        }
      }
      this.selectedAgencyID = this.fullSiteList.filter( d => { return d.siteID == this.selectedSiteID; })[0].agencyID;
      if(this.rowData != null){
        this.numberOfCoC = this.rowData.length;
      }
      else {
        this.numberOfCoC = 0;
      }
    }
    else { 
      if(this.is_SH_HP){
        this.selectedAgencyID = this.userData.agencyId;
      }
      this.onAgencySelected();
    }

  }

  onStatusSelected(){
    this.onSiteSelected();
    
  }


  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;    

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'clientEligibleFor') {
          allColumnIds.push(column.colId);
        }
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, true);
    this.gridApi.sizeColumnsToFit(allColumnIds);
  }


  selectedClientCatchedInParentPage(cell) {    
    this.cocService.setCoCReferralSelected(cell);
  }


  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'CoCReferralQueue-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };

    //console.log('CoCReferralQueue-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.agencyListSub) {
      this.agencyListSub.unsubscribe();
    }
    if (this.siteListSub) {
      this.siteListSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    // if (this.widthSideBarSub) {
    //   this.widthSideBarSub.unsubscribe();
    // }
  }

}
