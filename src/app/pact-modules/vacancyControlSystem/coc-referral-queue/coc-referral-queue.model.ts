export class VCSCoCReferralData {
    vcsCoCReferralID: number;
    pactApplicationID: number;
    referringAgencyID: number;
    referringAgencyNo: string;
    referringAgencyName: string;
    referringSiteID: number;
    referringSiteNo: string;
    referringSiteName: string;
    hpAgencyID: number;
    hpAgencyName: string;
    hpSiteID: number;
    hpSiteName: number;
    pactClientID: number;
    firstName: string;
    lastName: string;
    dateOfBirth: string;
    referralDate: string;
    ssn: string;
    clientEligibleFor: string;
    svaPrioritization: string;
    serviceNeeds: string;
    approvalToDate: string;
    placementCriteria: string;
    pendingReferrals: number;
    coCStatusType: number;
    coCStatusName: string;
    //coCFlag: number;
    coCWish2Disc: number;
    coCDiscAddlComment: string;
    reviewComments: string;
    lastEmailNotification: string;
    isActive: boolean;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;
}

export class VCSReferralSearchData {
    vcsReferralID: number;
    firstName: string;
    lastName: string;
    pactClientID: number;
    referralDate: string;
    referringAgencyName: string;
    referringSiteName: string;
    hpAgencyName: string;
    hpSiteName: string;
    referredBy: string;
    isCoC: boolean;
}
  
export class VCSCoCAgencyList {
    agencyID: number;
    agencyNo: string;
    agencyName: string;
}
  
export class VCSCoCSiteList {
    siteID: number;
    siteNo: string;
    agencyID: number;
    siteName: string;
    siteCategoryType: number;
    siteCategoryName: string;
    siteTrackedType: number;
}

export class VCSCoCReferralActivity {
    vcsCoCReferralActivityID: number;
	vcsCoCReferralID: number;
    coCStatusType: number;
    coCStatusDescription: string;
    agencyName: string;
	//coCDate: string;
	//reviewComments: string;
	//IsActive: boolean;
	createdBy: string;
    createdDate: string;
    lastEmailNotification: string;
	//updatedBy: string;
	updatedDate: string;
}

export class VCSCoCPendingReview {
    vcsCoCPendingReviewReasonID: number;
    vcsCoCReferralID: number;
    pendingReviewReasonType: number;
    reviewOtherSpecify: string;
}