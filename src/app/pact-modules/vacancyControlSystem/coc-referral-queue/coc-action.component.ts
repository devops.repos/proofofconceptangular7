import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { VCSCoCReferralData } from './coc-referral-queue.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-cap-action',
  template:
  
   `
  
   <button class="btn"  mat-button routerLink="/vcs/coc-referral-queue/coc-referral-details" (click)="onClientSelected()">
    <mat-icon
      class="pendingMenu-icon"
      color="warn">
      more_vert
    </mat-icon>
    </button> 
   
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        min-width: 10px !important;
        line-height: initial;
        padding: 0px;
      }
    `
  ]
})
export class CoCActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private cocSelected: VCSCoCReferralData;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onClientSelected() {
    this.cocSelected = this.params.data;
      
    this.params.context.componentParent.selectedClientCatchedInParentPage(this.cocSelected);
  }

  refresh(): boolean {
    return false;
  }
}
