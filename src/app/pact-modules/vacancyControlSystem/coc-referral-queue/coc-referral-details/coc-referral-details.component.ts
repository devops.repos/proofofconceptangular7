import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { Subscription } from 'rxjs';
import { iHousingApplicationSupportingDocumentsData } from 'src/app/pact-modules/supportiveHousingSystem/consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications.model'
import { CoCService } from './../coc-referral-queue.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
//import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { ReferralPackageNavService } from 'src/app/core/sidenav-list/referral-package-nav.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { ToastrService } from "ngx-toastr";
import { VCSCoCReferralData, VCSCoCReferralActivity, VCSCoCPendingReview, VCSCoCSiteList } from '../coc-referral-queue.model';
import { GridOptions } from 'ag-grid-community';
import { Router } from '@angular/router';
import { RefGroupDetails } from "src/app/models/refGroupDetailsDropDown.model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { UserSiteType } from 'src/app/models/pact-enums.enum';

@Component({
  selector: 'app-coc-referral-details',
  templateUrl: './coc-referral-details.component.html',
  styleUrls: ['./coc-referral-details.component.scss']
})
export class CocReferralDetailsComponent implements OnInit, OnDestroy {

  selectedTab = 0;
  //widthSideBar: number;
  disableNavBtn = false;
  is_Next_Required = true;
  is_Prev_Required = false;
  //widthSideBarSub: Subscription;
  sidenavTabSub: Subscription;
  userDataSub: Subscription;
  transmitSub: Subscription;
  rowDataSub: Subscription;
  commonServiceSub: Subscription;
  selectedClientSub: Subscription;
  getUserSiteSub: Subscription;
  savePendingReviewSub: Subscription;
  getReferralSub: Subscription;
  
  cocReviewForm: FormGroup;
  userData: AuthData;
  is_SH_PE = false;
  is_SH_HP = false;
  is_SH_HPD = false;
  is_CAS = false;
  is_ReferralPending = false;
  

  //vcsCoCRefID: number;
  cocReferralSelected: VCSCoCReferralData;
  refStatusHistory: VCSCoCReferralActivity[] = [];
  responseItems: RefGroupDetails[];
  reviewStatusItems: RefGroupDetails[];
  pendingReviewReasons: RefGroupDetails[];
  reviewReasonList: VCSCoCPendingReview[] = [];
  peSiteList: VCSCoCSiteList[] = [];
  is_CoC_disc = 0;
  coc_disc_comm = '';
  is_coc_disabled = false;
  is_transmit_pe_disable = true;
  is_transmit_hp_disable = true;
  transmit_btn_visible = false;
  trans_btn_disable = true;
  is_transmit_checked = false;
  showOtherComment = false;

  housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null,
    expandSection: null
  };
  
  gridColumnApi: any;  
  ///context: any; 
  defaultColDef: any;
  gridApi: any;
  gridOptions: GridOptions;
  columnDefs: any;

  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string; 

  constructor(//private navService: NavService,
              private referralPackageNavService: ReferralPackageNavService,
              private cocService: CoCService,
              private userService: UserService,
              private commonService:CommonService,
              private toastr: ToastrService,
              private router: Router,
              private formBuilder: FormBuilder,
              private confirmDialogService: ConfirmDialogService) {

    this.gridOptions = {
      rowHeight: 50
      } as GridOptions;  
    //this.context = { componentParent: this };
    this.defaultColDef = {
      sortable: false,
      resizable: false,
      filter: false
    };

    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the history is loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No History Available</span>';
            

    this.columnDefs = [       
    {
      headerName: 'Action Date',
      field: 'createdDate'
    },
    {
      headerName: 'Name',
      field: 'coCStatusDescription'
    },
    {
      headerName: 'Agency',
      field: 'agencyName'
    },
    {
      headerName: 'Action Updated By',
      field: 'createdBy'
    },
    {
      headerName: 'Last Email Notification Sent',
      valueGetter: function(params) {
        return params.data.lastEmailNotification ? params.data.lastEmailNotification :'N/A'
      }
    }];
            
  }

  ngOnInit() {
      // /** Getting the width of the sidenav to reflect the changes to nav toggle */
      // this.widthSideBarSub = this.navService.getWidthSideBar().subscribe(res => {
      //   this.widthSideBar = res;
      //   console.log(this.widthSideBar);
      // });

      this.sidenavTabSub = this.referralPackageNavService.getCurrentCoCReferralTabIndex().subscribe(res => {
        this.selectedTab = res;
      });

      
      this.commonServiceSub = this.commonService.getRefGroupDetails("7,70,77").subscribe(
        res => {
          const data = res.body as RefGroupDetails[];  
            
          this.responseItems = data.filter(d => {
            return d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34);
          });
          this.reviewStatusItems = data.filter(d => {
            return d.refGroupID === 70 && (d.refGroupDetailID === 570 || d.refGroupDetailID === 571 || d.refGroupDetailID === 568);
          });
               
          this.pendingReviewReasons = data.filter(d => {
            return d.refGroupID === 77;
          });

        },
        error => console.error("Error!", error)
      );
  


      this.userDataSub = this.userService.getUserData().subscribe(userdata => {
        if (userdata) {
          this.userData = userdata;
          /** Checking the User siteCategoryType either PE or HP */
          if(this.userData.siteCategoryType.length > 0) {
            this.is_SH_PE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_PE);
            this.is_SH_HP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_HP);
            this.is_CAS = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.CAS);
            
          }  
          //if(this.is_SH_PE){ this.is_SH_HPD = true; this.is_SH_PE = false}

          if (this.is_SH_PE && this.userData.agencyNo == '2092') {
           
            this.getUserSiteSub = this.cocService.getUserSite(this.userData.optionUserId).subscribe(res => {
                this.peSiteList = res as VCSCoCSiteList[]
                this.peSiteList.forEach(item => {
                  if(item.siteNo == '999'){
                    this.is_SH_HPD = true;
                    this.is_SH_PE = false;
                  }
                })
            });
          }   
        }
      });
    this.selectedClientSub = this.cocService.getCoCReferralSelected().subscribe(client => {
      if (client) {
        this.cocReferralSelected = client as VCSCoCReferralData;
        if(this.cocReferralSelected.coCStatusType == 494 || this.cocReferralSelected.coCStatusType == 495 || this.cocReferralSelected.coCStatusType == 595){
          this.is_coc_disabled = true;
          
          //this.coc_disc_comm = this.cocReferralSelected.coCDiscAddlComment;
          if(this.cocReferralSelected.coCStatusType == 595) {
            this.is_CoC_disc = 33;     
          }     
        } 
       

        if (this.cocReferralSelected.coCStatusType == 572) {
          this.is_transmit_pe_disable = false;
          this.is_transmit_hp_disable = true;
          this.trans_btn_disable = false;
        } else if (this.cocReferralSelected.coCStatusType == 593) {
          this.is_transmit_pe_disable = true;
          this.is_transmit_hp_disable = false;          
          this.trans_btn_disable = false;
        } 
        else {
          this.is_transmit_hp_disable = true;
          this.is_transmit_pe_disable = true;
          this.trans_btn_disable = true;
        }

        this.buildForm();
        if(this.cocReferralSelected.coCStatusType != 594 ){
          this.cocReviewForm.disable();
        } 
        if(this.cocReferralSelected.coCStatusType == 594 && this.is_SH_HPD){
          this.getReferralPendingReviewReasonList();
        }
        this.getCoCReferralActivity();
        //this.setInitialValues();
        
        this.getApplicationPackage();   

      } else {
        /* If user refresh(reload) the page the client selected data will be empty, so redirect them back to CoC Queue Page */
        this.router.navigate(['/vcs/coc-referral-queue']);
      }
    });
    
  }
  buildForm() {
    
    this.cocReviewForm = this.formBuilder.group({
      reviewStatus: [0],
      reviewStatusComments: ["", Validators.required ],
      specifyOther: ["", Validators.required ],
      599: false,
      600: false,
      601: false,
      602: false,
      603: false,
      604: false,
      605: false
    });
  }

  getApplicationPackage(){
    this.housingApplicationSupportingDocumentsData.agencyNumber = this.cocReferralSelected.referringAgencyNo;
    this.housingApplicationSupportingDocumentsData.siteNumber = this.cocReferralSelected.referringSiteNo;
    this.housingApplicationSupportingDocumentsData.firstName = this.cocReferralSelected.firstName;
    this.housingApplicationSupportingDocumentsData.lastName = this.cocReferralSelected.lastName;
    this.housingApplicationSupportingDocumentsData.dob = this.cocReferralSelected.dateOfBirth;
    this.housingApplicationSupportingDocumentsData.cin = null;
    this.housingApplicationSupportingDocumentsData.ssn = this.cocReferralSelected.ssn;
    this.housingApplicationSupportingDocumentsData.pactClientId = this.cocReferralSelected.pactClientID;
    this.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.cocReferralSelected.approvalToDate;
    this.housingApplicationSupportingDocumentsData.pactApplicationId = this.cocReferralSelected.pactApplicationID;
    this.housingApplicationSupportingDocumentsData.expandSection = 1; 
  }
  setInitialValues(){
    //if(this.is_SH_HPD && this.selectedTab == 1){
      //Reset Approved and Dissaproved dropdown and comment
      if (this.cocReferralSelected.coCStatusType == 494) {
        this.cocReviewForm.controls.reviewStatus.setValue(570); 
      } else if(this.cocReferralSelected.coCStatusType == 495) {
        this.cocReviewForm.controls.reviewStatus.setValue(571); 
      } else if(this.cocReferralSelected.coCStatusType == 594) {
        if (!this.is_ReferralPending){
          this.cocReviewForm.controls.reviewStatus.setValue(0); 
        }
        else {
          this.cocReviewForm.controls.reviewStatus.setValue(568);           
        }
        this.getReferralPendingReviewReasonList();
      }
      this.cocReviewForm.controls["reviewStatusComments"].setValue(this.cocReferralSelected.reviewComments);
    //}

    //if(this.selectedTab == 3) {
    //Set discontinue Comment
    this.cocReferralSelected.coCWish2Disc == 33 ? this.is_CoC_disc = this.cocReferralSelected.coCWish2Disc : this.is_CoC_disc = 0; 

      this.coc_disc_comm = this.cocReferralSelected.coCDiscAddlComment;
    //}
  }

  onReviewReasonChange(event: { checked: boolean; }, reasonID: number) {
    if (event.checked) {
      var newReason = new VCSCoCPendingReview();
      newReason.pendingReviewReasonType = reasonID;
      newReason.vcsCoCReferralID = this.cocReferralSelected.vcsCoCReferralID;
      if (reasonID == 605){
        //if (this.cocReviewForm.controls.specifyOther.value == ''){ 
        //this.toastr.error("Specify reason if other is selected.", "Error while saving");
        //this.reviewReasonsID.push(reasonID);
        this.showOtherComment = true;
      }
      this.reviewReasonList.push(newReason);
        
    }
    else {
      
        //this.reviewReasonsID.splice(this.reviewReasonsID.indexOf(reasonID), 1);
        this.reviewReasonList = this.reviewReasonList.filter( obj => {
          return obj.pendingReviewReasonType !== reasonID;
        });
        if(reasonID == 605){
          this.showOtherComment = false;
          this.cocReviewForm.controls["specifyOther"].setValue("");
        }
    }
  }

  getCoCReferralActivity(){
    /** API call to get the grid data */
    this.rowDataSub = this.cocService.getCoCReferralActivity(this.cocReferralSelected.vcsCoCReferralID).subscribe(res => {
      this.refStatusHistory =  res as VCSCoCReferralActivity[]; 
      this.refStatusHistory.forEach(item => {
        if ( item.coCStatusType == 594 && item.updatedDate != null){
          this.is_ReferralPending = true;
        }
        if (this.is_SH_HP && item.coCStatusType == 594){          
          this.is_transmit_pe_disable = true;
          this.trans_btn_disable = true;
          this.transmit_btn_visible = true;
          this.is_transmit_checked = true;
        }
        if ((this.is_SH_PE || this.is_CAS) && item.coCStatusType == 593){
          this.is_transmit_hp_disable = true;
          this.trans_btn_disable = true;
          this.transmit_btn_visible = true;
          this.is_transmit_checked = true;
        }
      })    

      this.setInitialValues();
    });
  }

  onGridReady(params: { api: any; columnApi: any })  {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    params.api.setDomLayout('autoHeight'); 

    const allColumnIds = [];
    
    this.gridColumnApi.getAllColumns().forEach(function (column) {
        allColumnIds.push(column.colId);
    }); 
    this.gridColumnApi.autoSizeColumns(allColumnIds); 
    this.gridApi.sizeColumnsToFit();
 
    
    /** API call to get the grid data */
    //this.getContactsList();

  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {   
    this.selectedTab = tabChangeEvent.index;
    if(this.is_SH_PE){
      if (this.selectedTab == 3 )
      {
        this.disableNavBtn = true;
      } else { 
        this.disableNavBtn = false; 
      }
    }
    if(this.is_SH_HPD){
      if (this.selectedTab == 1 || this.selectedTab == 3)
      {
        this.disableNavBtn = true;
      } else { 
        this.disableNavBtn = false; 
      }
    }
    if(this.is_CAS){
      if (this.selectedTab == 2 || this.selectedTab == 4)
      {
        this.disableNavBtn = true;
      } else { 
        this.disableNavBtn = false; 
      }
    }

    this.is_Next_Required = true;  
    this.is_Prev_Required = true;
    if (this.selectedTab == 0) {
      this.is_Prev_Required = false;
    }

    if(this.is_SH_PE || this.is_SH_HPD){
      if (this.selectedTab == 4) {
        this.is_Next_Required = false;
      }     
    }
    if(this.is_CAS){
      if (this.selectedTab == 5) {
        this.is_Next_Required = false;
      }     
    }
    else if(this.is_SH_HP){
      if (this.selectedTab == 3) {
        this.is_Next_Required = false;
      }       
    }
  }

  onChange(event: { checked: Boolean }) {
    if (event.checked) {
      this.transmit_btn_visible = true;
    }
    else {
      this.transmit_btn_visible = false;
    }
  }

  onTransmitClick(){
    if (this.is_SH_PE || this.is_CAS){
      //CoC Status Pending at Housing Provider
      this.updateCoCReferral(593, true);
      this.is_transmit_pe_disable = true;
      this.trans_btn_disable = true; 
      this.disableNavBtn = false;     
    }
    else if (this.is_SH_HP){
      //CoC Status - Pending HPD Approval
      this.updateCoCReferral(594, true);
      this.is_transmit_hp_disable = true;
      this.trans_btn_disable = true;
      this.disableNavBtn = false;
    }
    //this.getCoCReferralActivity();
  }

  onSave(showToaster: boolean) {
    if((((this.is_SH_PE || this.is_SH_HPD) && this.selectedTab == 3) || (this.is_CAS && this.selectedTab == 4))  && !this.is_coc_disabled) {
      
      if(this.is_CoC_disc != 0 && this.coc_disc_comm && this.coc_disc_comm.trim() != ''){      
        
        this.cocReferralSelected.coCWish2Disc = this.is_CoC_disc;
        this.cocReferralSelected.coCDiscAddlComment = this.coc_disc_comm;
        this.cocReferralSelected.coCStatusType = 595;

        this.transmitSub = this.cocService.updateCoCReferral(this.cocReferralSelected).subscribe(
          res => {
              this.toastr.success("The CoC Referral was discontinued.", "");

              this.is_coc_disabled = true;
              this.disableNavBtn = false;
              this.is_transmit_pe_disable = true;
              this.is_transmit_hp_disable = true;
              this.cocReviewForm.disable();
              this.getCoCReferralActivity();
          },
          error =>
            this.toastr.error("The information was not discontinued.", "Error while saving")
        );
      }
      else {
        this.toastr.error("Comment is required to discontinue the referral.", "Error while saving")
      }
    }

    if((this.is_SH_HPD && this.selectedTab == 1) ||(this.is_CAS && this.selectedTab == 2)) {
      //approved
      if(this.cocReviewForm.controls.reviewStatus.value == 570) {
        if(this.cocReviewForm.controls.reviewStatusComments.value != '' && this.cocReviewForm.controls.reviewStatusComments.value != null) {
          this.cocReferralSelected.reviewComments = this.cocReviewForm.controls.reviewStatusComments.value;
          this.updateCoCReferral(494, true);
          this.cocReviewForm.disable();
          this.is_coc_disabled = true;
          this.disableNavBtn = false;
          this.getCoCReferralActivity();
        } else {
          this.toastr.error("Comments are required.", "");
        }
      } 
      //disapproved
      else if(this.cocReviewForm.controls.reviewStatus.value == 571) {
        if(this.cocReviewForm.controls.reviewStatusComments.value != '' && this.cocReviewForm.controls.reviewStatusComments.value != null) {
          const title = 'Confirmation required';
          const primaryMessage = `Is this your finale decision, the client will not be approved for HPD CoC rental assistance`;
          const secondaryMessage = `Are you sure you want to continue?`;
          const confirmButtonName = 'Yes';
          const dismissButtonName = 'No';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
              positiveResponse => {
                this.cocReferralSelected.reviewComments = this.cocReviewForm.controls.reviewStatusComments.value;
                this.updateCoCReferral(495, true);
                this.cocReviewForm.disable();
                this.is_coc_disabled = true;
                this.disableNavBtn = false;
                this.getCoCReferralActivity();
              },
              negativeResponse => console.log()
            );
          }  else {
            this.toastr.error("Comments are required.", "");
          }
        
      } //pending
      else if(this.cocReviewForm.controls.reviewStatus.value == 568) {
        var submitForm = true;
        //if (this.cocReviewForm.controls.reviewStatusComments.value != '' && this.cocReviewForm.controls.reviewStatusComments.value != null) {
          this.cocReferralSelected.reviewComments = this.cocReviewForm.controls.reviewStatusComments.value;
          this.updateCoCReferral(594, true);
        //}       
        //this.reviewReasonList = [];
        this.reviewReasonList.forEach(id => {
          //var newReason = new VCSCoCPendingReview();
          //newReason.pendingReviewReasonType = id;
          //newReason.vcsCoCReferralID = this.cocReferralSelected.vcsCoCReferralID;
          if (id.pendingReviewReasonType == 605){
            if (this.cocReviewForm.controls.specifyOther.value == ''){ 
            this.toastr.error("Please specify if other reason is selected.", "Error while saving");
            submitForm = false;
            } else {
              id.reviewOtherSpecify = this.cocReviewForm.controls.specifyOther.value;
            }
          }

          //this.reviewReasonList.push(newReason);
        });
        if(submitForm && this.reviewReasonList.length>0){
          this.savePendingReviewSub = this.cocService.savePendingCoCReferral(this.reviewReasonList).subscribe(
            res => {
                //this.toastr.success("The information was saved successfully.", "");
                this.getReferralPendingReviewReasonList();
            },
            error =>
              this.toastr.error("Reasons were not saved.", "Error while saving")
          );
        }
      }
    }
  }

  getReferralPendingReviewReasonList(){
    this.cocReviewForm.controls[599].setValue(false); 
    this.cocReviewForm.controls[600].setValue(false); 
    this.cocReviewForm.controls[601].setValue(false); 
    this.cocReviewForm.controls[602].setValue(false); 
    this.cocReviewForm.controls[603].setValue(false); 
    this.cocReviewForm.controls[604].setValue(false); 
    this.cocReviewForm.controls[605].setValue(false); 
    this.getReferralSub = this.cocService.getCoCReferralReasonsList(this.cocReferralSelected.vcsCoCReferralID).subscribe(res => {
      this.reviewReasonList = res as VCSCoCPendingReview[];
      this.reviewReasonList.forEach(data => {
        var ctr = data.pendingReviewReasonType;
        this.cocReviewForm.controls[ctr].setValue(true);  
        if (ctr == 605) {
          this.showOtherComment = true;
          this.cocReviewForm.controls.specifyOther.setValue(data.reviewOtherSpecify);
        }    
        //this.cocReviewForm.patchValue(this.reviewReasonList);
      })
    })
  }
  
  onClear() {
    this.setInitialValues();
  }

  updateCoCReferral(cocStatusID: number, showToaster: boolean){
    this.cocReferralSelected.coCStatusType = cocStatusID;
    this.transmitSub = this.cocService.updateCoCReferral(this.cocReferralSelected).subscribe(
      res => {
        if(showToaster){
          this.toastr.success("The information was saved successfully.", "");
        }
          this.getCoCReferralActivity();
      },
      error =>
        this.toastr.error("The information was not saved.", "Error while saving")
    );
  }

// Next button click
  nextPage() {
    if(this.is_SH_PE || this.is_SH_HPD){
      if (this.selectedTab < 4) {
        this.selectedTab = this.selectedTab + 1;
        this.is_Prev_Required = true;
      } else if (this.selectedTab == 4) {
        this.is_Next_Required = false;
        //this.selectedTab = 0;
      }
    } else if(this.is_CAS){
      if (this.selectedTab < 5) {
        this.is_Prev_Required = true;
        this.selectedTab = this.selectedTab + 1;
      } else if (this.selectedTab == 5) {
        this.is_Next_Required = false;
        //this.selectedTab = 0;
      }
    }
         else if(this.is_SH_HP){
      if (this.selectedTab < 3) {
        this.is_Prev_Required = true;
        this.selectedTab = this.selectedTab + 1;
      } else if (this.selectedTab == 3) {
        this.is_Next_Required = false;
        //this.selectedTab = 0;
      }
    }
  }

  // Previous button click
  previousPage() {
    if (this.selectedTab != 0) {
      this.selectedTab = this.selectedTab - 1;
      this.is_Next_Required = true;
    } else if (this.selectedTab == 0) {
      this.is_Prev_Required = false;        
    }    
  }

  ngOnDestroy() {
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.transmitSub) {
      this.transmitSub.unsubscribe();
    }
    if (this.selectedClientSub) {
      this.selectedClientSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.sidenavTabSub) {
       this.sidenavTabSub.unsubscribe();
    }
    if (this.getUserSiteSub) {
      this.getUserSiteSub.unsubscribe();
    }
    if (this.savePendingReviewSub) {
      this.savePendingReviewSub.unsubscribe();
    }
    if (this.getReferralSub) {
      this.getReferralSub.unsubscribe();
    }
    
  }
}

