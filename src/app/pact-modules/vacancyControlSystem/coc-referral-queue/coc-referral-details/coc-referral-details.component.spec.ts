import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CocReferralDetailsComponent } from './coc-referral-details.component';

describe('CocReferralDetailsComponent', () => {
  let component: CocReferralDetailsComponent;
  let fixture: ComponentFixture<CocReferralDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CocReferralDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CocReferralDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
