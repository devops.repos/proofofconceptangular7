import { Component, OnInit, ViewChild, OnDestroy, Input, HostListener } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';

import {
  IReferralRosterData,
  IReferralRosterList,
  IReferralRosterInput,
  ReferralRosterTabStatus,
  IVCSAgencyList,
  IVCSSiteList,
  IVCSAgencyInput,
  IVCSSiteInput,
  ReferralRosterWithdrawnData, IVCSRaHpAgenciesSites, IVCSRaHpAgencyList, IVCSRaHpSiteList
} from './referral-roster-interface.model';
import { ReferralRosterService } from './referral-roster.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { UserSiteType, ReferralStatusType } from 'src/app/models/pact-enums.enum';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { RRDraftActionComponent } from './rr-draft-action.component';
import { RRTransmittedActionComponent } from './rr-transmitted-action.component';
import { RRPendingActionComponent } from './rr-pending-action.component';
import { RRCompletedActionComponent } from './rr-completed-action.component';
import { VCSReferralData, VCSPlacementMatchCriteria } from '../client-awaiting-placement/c2v-interfaces.model';
import { C2vService } from '../client-awaiting-placement/c2v.service';
import { RRInfoIconComponent } from './rr-info-icon.component';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { CommonService } from 'src/app/services/helper-services/common.service';

export interface RRAgencyType {
  type: number;
  description: string;
}

@Component({
  selector: 'app-referral-roster',
  templateUrl: './referral-roster.component.html',
  styleUrls: ['./referral-roster.component.scss']
})
export class ReferralRosterComponent implements OnInit, OnDestroy {
  @Input() isTAD: boolean = false;
  @Input() hpSiteIDforTAD: number = 0;
  @Input() hpSiteNoforTAD: string = '';
  @Input() hpSiteNameforTAD: string = '';
  @Input() hpAgencyIDforTAD: number = 0;
  @Input() hpAgencyNoforTAD: string = '';
  @Input() hpAgencyNameforTAD: string = '';
  @Input() referralValidatorIDs = [];
  @Input() tadValidationMsg: string = '';
  currentUser: AuthData;
  is_SH_PE = false;
  is_SH_HP = false;
  is_CAS = false;

  @ViewChild('draftAgGrid') draftAgGrid: AgGridAngular;
  @ViewChild('transmittedAgGrid') transmittedAgGrid: AgGridAngular;
  @ViewChild('pendingAgGrid') pendingAgGrid: AgGridAngular;
  @ViewChild('completedAgGrid') completedAgGrid: AgGridAngular;
  agencyList: IVCSRaHpAgencyList[];
  siteList: IVCSRaHpSiteList[];
  selectedAgencyID = 0;
  selectedSiteID = 0;

  draft = 0;
  pending = 0;
  overdue = 0;
  acceptedPendingApproval = 0;
  inProgress = 0;
  moveIn = 0;
  notAccepted = 0;
  external = 0;
  flagClicked = '';

  selectedTab = 0;

  draftGridApi;
  draftGridColumnApi;
  draftColumnDefs;
  dOverlayLoadingTemplate;
  dOverlayNoRowsTemplate
  transmittedGridApi;
  transmittedGridColumnApi;
  transmittedColumnDefs;
  tOverlayLoadingTemplate;
  tOverlayNoRowsTemplate
  pendingGridApi;
  pendingGridColumnApi;
  pendingColumnDefs;
  pOverlayLoadingTemplate;
  pOverlayNoRowsTemplate
  completedGridApi;
  completedGridColumnApi;
  completedColumnDefs;
  completedColumnDefs1;
  cOverlayLoadingTemplate;
  cOverlayNoRowsTemplate


  defaultColDef;
  frameworkComponents;
  context;

  public dgridOptions: GridOptions;
  public tgridOptions: GridOptions;
  public pgridOptions: GridOptions;
  public cgridOptions: GridOptions;
  referralRosterData: IReferralRosterData;
  draftRowData: IReferralRosterList[] = [];
  transmittedRowData: IReferralRosterList[];
  pendingRowData: IReferralRosterList[] = [];
  completedRowData: IReferralRosterList[];

  agencyDropdownTypeSelected: number = 1;
  agencyDropdownTypes: RRAgencyType[] = [
    {
      type: 1,
      description: 'Applications Submitted By'
    },
    {
      type: 2,
      description: 'Referrals transmitted to'
    }
  ];

  raHpAgenciesSites: IVCSRaHpAgenciesSites;
  raHpAgencies: IVCSRaHpAgencyList[];
  raHpSites: IVCSRaHpSiteList[];
  savedAgencyID: number;
  savedSiteID: number;

  currentUserSub: Subscription;
  selectedTabSub: Subscription;
  referralRosterDataSub: Subscription;
  saveVCSReferralSub: Subscription;
  selectedAgencySub: Subscription;
  selectedSiteSub: Subscription;
  routeSub: Subscription;

  constructor(
    private userService: UserService,
    private referralRosterService: ReferralRosterService,
    private toastr: ToastrService,
    private c2vService: C2vService,
    private router: Router,
    private commonService: CommonService,
    private route: ActivatedRoute
  ) {

    /* Getting all the RA/HP Agencies and Sites before Page Load */
    this.route.data.subscribe((res) => {
      // console.log('route data: ', res.rahpAgenciesSites);
      if (res.rahpAgenciesSites) {
        this.raHpAgenciesSites = res.rahpAgenciesSites;
        this.raHpAgencies = res.rahpAgenciesSites.raHpAgencies;
        this.raHpSites = res.rahpAgenciesSites.raHpSites;
      }
    });


    // this.dOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Draft Referral list is loading.</span>';
    this.dOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Referrals Available</span>';
    // this.tOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Transmitted Referral list is loading.</span>';
    this.tOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Referrals Available</span>';
    // this.pOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Pending Referral list is loading.</span>';
    this.pOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Referrals Available</span>';
    // this.cOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Completed Referral list is loading.</span>';
    this.cOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Referrals Available</span>';
    this.dgridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.tgridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.pgridOptions = {
      rowHeight: 35,
      getRowStyle: (params) => {
        if (this.isTAD &&
          (
            (this.referralValidatorIDs.includes(6) && params.data.referralStatusType == 513) ||
            (this.referralValidatorIDs.includes(7) && params.data.referralStatusType == 518 &&
              params.data.expectedMoveInDate && new Date(params.data.expectedMoveInDate) < new Date()
            )
          )
        ) {
          return { background: 'sandybrown' };
        }
      }
    } as GridOptions;
    this.cgridOptions = {
      rowHeight: 35,
      getRowStyle: (params) => {
        if (params.node.data.referralType == 526) {
          return { background: 'sandybrown' };
        }
      }
    } as GridOptions;

    this.draftColumnDefs = [
      {
        headerName: 'Status',
        field: 'referralStatusTypeDescription',
        // width: 80,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Client# - Referral Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.clientID + ' - ' + params.data.referralDate;
        }
      },
      {
        headerName: 'Client Name(L,F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName;
        }
      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Placement Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Housing Agency/Site',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
        }
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Interview Date-Time',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.interviewDate && params.data.interviewTime) {
            return params.data.interviewDate + ' - ' + params.data.interviewTime;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Interview Location',
        field: 'interviewLocation',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referredby-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.referredBy + ' - ' + params.data.referredDate;
        }
      },
      {
        headerName: 'UpdatedBy-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.updatedBy && params.data.updatedDate) {
            return params.data.updatedBy + ' - ' + params.data.updatedDate;
          } else {
            return '';
          }
        }
      },
      // {
      //   headerName: 'Updatedby-Date',
      //   filter: 'agTextColumnFilter',
      //   valueGetter(params) {
      //     return params.data.updatedBy + ' - ' + params.data.updatedDate;
      //   }
      // },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'draftActionRenderer'
      }
    ];
    this.transmittedColumnDefs = [
      {
        headerName: 'Status',
        field: 'referralStatusTypeDescription',
        // width: 80,
        filter: 'agTextColumnFilter',
        cellStyle: function (params) {
          if (params.data.referralStatusType == 513) {
            //mark overdue cells as red
            return { color: 'red' };
          } else if (params.data.referralStatusType == 512 || params.data.referralStatusType == 518 || params.data.referralStatusType == 529) {
            //mark overdue cells as orange
            return { color: '#FF9900' };
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Client# - Referral Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.clientID + ' - ' + params.data.referralDate;
        }
      },
      {
        headerName: 'Client Name(L,F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName;
        }
      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Placement Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Housing Agency/Site',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
        }
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Interview Date-Time',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.interviewDate && params.data.interviewTime) {
            return params.data.interviewDate + ' - ' + params.data.interviewTime;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Interview Location',
        field: 'interviewLocation',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referredby-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.referredBy + ' - ' + params.data.referredDate;
        }
      },
      {
        headerName: 'UpdatedBy-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.updatedBy && params.data.updatedDate) {
            return params.data.updatedBy + ' - ' + params.data.updatedDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'transmittedActionRenderer'
      }
    ];
    this.pendingColumnDefs = [
      {
        headerName: 'Status',
        field: 'referralStatusTypeDescription',
        width: 120,
        suppressSizeToFit: true,
        filter: 'agTextColumnFilter',
        cellStyle: function (params) {
          if (params.data.referralStatusType == 513) {
            //mark overdue cells as red
            return { color: 'red' };
          } else if (params.data.referralStatusType == 512 || params.data.referralStatusType == 518 || params.data.referralStatusType == 529) {
            //mark overdue cells as orange
            return { color: '#FF9900' };
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Client# - Referral Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.clientID + ' - ' + params.data.referralDate;
        }
      },
      {
        headerName: 'Client Name(L,F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName;
        }
      },
      {
        headerName: 'Referring Agency/Site',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.referringAgencyNo + ' - ' + params.data.referringAgencyName + ' / ' + params.data.referringSiteNo + ' - ' + params.data.referringSiteName;
        }
      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Placement Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Interview Date-Time',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.interviewDate && params.data.interviewTime) {
            return params.data.interviewDate + ' - ' + params.data.interviewTime;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Interview Location',
        field: 'interviewLocation',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referredby-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.referredBy + ' - ' + params.data.referredDate;
        }
      },
      {
        headerName: 'UpdatedBy-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.updatedBy && params.data.updatedDate) {
            return params.data.updatedBy + ' - ' + params.data.updatedDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'pendingActionRenderer'
      }
    ];
    this.completedColumnDefs = [
      // Completed Tab for PE/CAS
      {
        headerName: 'Status',
        field: 'referralStatusTypeDescription',
        width: 240,
        suppressSizeToFit: true,
        filter: 'agTextColumnFilter',
        cellRendererSelector(params) {
          const infoIcon = {
            component: 'infoIconRenderer'
          };
          if (params.data.referralStatusType === ReferralStatusType.ClientAcceptedAlternateHousing ||
            params.data.referralStatusType === ReferralStatusType.Withdrawn ||
            params.data.referralStatusType === ReferralStatusType.AdministrativelyClosed) {
            return infoIcon;
          } else {
            return null;
          }
        },
        cellStyle: function (params) {
          if (params.data.referralStatusType == 521) {
            //mark MoveIn rejected cells as red
            return { color: 'red' };
          } else if (params.data.referralStatusType == 520) {
            //MoveIn Verified as green
            return { color: 'green' };
          } else if (params.data.referralStatusType == 519) {
            //MoveIn Pending as orange
            return { color: '#FF9900' };
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Client# - Referral Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.clientID + ' - ' + params.data.referralDate;
        }
      },
      {
        headerName: 'Client Name(L,F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName;
        }
      },
      {
        headerName: 'Housing Agency/Site',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
        }
      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Placement Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Interview Date-Time',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.interviewDate && params.data.interviewTime) {
            return params.data.interviewDate + ' - ' + params.data.interviewTime;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Interview Location',
        field: 'interviewLocation',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referredby-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.referredBy + ' - ' + params.data.referredDate;
        }
      },
      {
        headerName: 'UpdatedBy-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.updatedBy && params.data.updatedDate) {
            return params.data.updatedBy + ' - ' + params.data.updatedDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'completedActionRenderer'
      }
    ];
    this.completedColumnDefs1 = [
      // Completed Tab for HP
      {
        headerName: 'Status',
        field: 'referralStatusTypeDescription',
        width: 240,
        suppressSizeToFit: true,
        filter: 'agTextColumnFilter',
        cellRendererSelector(params) {
          const infoIcon = {
            component: 'infoIconRenderer'
          };
          if (params.data.referralStatusType === ReferralStatusType.ClientAcceptedAlternateHousing ||
            params.data.referralStatusType === ReferralStatusType.Withdrawn ||
            params.data.referralStatusType === ReferralStatusType.AdministrativelyClosed) {
            return infoIcon;
          } else {
            return null;
          }
        },
        cellStyle: function (params) {
          if (params.data.referralStatusType == 521) {
            //mark MoveIn rejected cells as red
            return { color: 'red' };
          } else if (params.data.referralStatusType == 520) {
            //MoveIn Verified as green
            return { color: 'green' };
          } else if (params.data.referralStatusType == 519) {
            //MoveIn Pending as orange
            return { color: '#FF9900' };
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Client# - Referral Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.clientID + ' - ' + params.data.referralDate;
        }
      },
      {
        headerName: 'Client Name(L,F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName;
        }
      },
      {
        headerName: 'Referring Agency/Site',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.referringAgencyNo + ' - ' + params.data.referringAgencyName + ' / ' + params.data.referringSiteNo + ' - ' + params.data.referringSiteName;
        }
      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Placement Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Interview Date-Time',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.interviewDate && params.data.interviewTime) {
            return params.data.interviewDate + ' - ' + params.data.interviewTime;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Interview Location',
        field: 'interviewLocation',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referredby-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.referredBy + ' - ' + params.data.referredDate;
        }
      },
      {
        headerName: 'UpdatedBy-Date',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.updatedBy && params.data.updatedDate) {
            return params.data.updatedBy + ' - ' + params.data.updatedDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'completedActionRenderer'
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      draftActionRenderer: RRDraftActionComponent,
      transmittedActionRenderer: RRTransmittedActionComponent,
      pendingActionRenderer: RRPendingActionComponent,
      completedActionRenderer: RRCompletedActionComponent,
      infoIconRenderer: RRInfoIconComponent
    };
  }

  ngOnInit() {
    /* Reset */
    this.referralRosterService.setisRoutedFromRR(false);
    this.referralRosterService.setCurrentRRTabNo(0);

     /** Get the current tab index */
     this.selectedTabSub = this.referralRosterService.getCurrentRRTabIndex().subscribe(res => {
      this.selectedTab = res;
    });

    /* Get Saved AgencyID and SiteID */
    this.selectedAgencySub = this.referralRosterService.getRRAgencySelected().subscribe(agencyID => {
      if (agencyID > 0) {
        this.savedAgencyID = agencyID;
      }
    });

    this.selectedSiteSub = this.referralRosterService.getRRSiteSelected().subscribe(siteID => {
      if (siteID > 0) {
        this.savedSiteID = siteID;
      }
    });

    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
        if (this.currentUser.siteCategoryType.length > 0) {
          /** Checking the User siteCategoryType either PE or HP */
          var shhp = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_HP);
          var shpe = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_PE);
          var cas = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.CAS);

          this.referralRosterService.getRRAgencyDropdownTypeSelected().subscribe((type: number) => {
            if (type > 0) {
              // type = 1 (Applications Submitted By (RA/HP)) ; type = 2 (Referrals transmitted to)
              this.agencyDropdownTypeSelected = type;

              if (shhp.length > 0) {
                /* HP */
                this.is_SH_HP = true;
                this.agencyDropdownTypeSelected = 2;
                this.selectedAgencyID = this.currentUser.agencyId;
                /* Get the HPSite List */
                if (this.isTAD) {
                  /* referral Roster being called from TAD */
                  if (this.hpSiteIDforTAD > 0) {
                    this.selectedSiteID = this.hpSiteIDforTAD;
                    this.onGo();
                  }
                } else {
                  /* Referral Roster normal call for HP */
                  this.getSiteSelected();
                }
              } else {
                if (shpe.length > 0) {
                  this.is_SH_PE = true;
                } else if (cas.length > 0) {
                  this.is_CAS = true;
                }
                /* Get the RA/HP Agency/Site List */
                if (this.isTAD) {
                  /* referral Roster being called from TAD */
                  if (this.hpSiteIDforTAD > 0) {
                    this.agencyDropdownTypeSelected = 2;
                    this.selectedSiteID = this.hpSiteIDforTAD;
                    this.onGo();
                  }
                } else {
                  /* Get the RA/HP Agency List based on agencyDropdownTypeSelected */
                  if (this.agencyDropdownTypeSelected == 2) {
                    /* Only HP Agency List */
                    if (this.raHpAgencies.length > 0) {
                      this.agencyList = this.raHpAgencies.filter(d => d.siteCategoryType == 7);
                      // console.log('agencyList: ', this.agencyList);
                      this.callOnAgencySelected();
                    }
                  } else {
                    /* Both RA/HP Agency List */
                    if (this.raHpAgencies.length > 0) {
                      // this.agencyList = this.raHpAgencies;
                      this.agencyList = this.raHpAgencies.filter((ag, index, self) =>
                        index === self.findIndex((t) => (
                          t.agencyID === ag.agencyID
                        ))
                      );
                      // console.log('agencyList: ', this.agencyList);
                      this.callOnAgencySelected();
                    }
                  }
                }
              }
            }
          });
        }
      }
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        /* save the selected AgencyID and SiteID before leaving the page */
        this.referralRosterService.setRRAgencySelected(this.selectedAgencyID);
        this.referralRosterService.setRRSiteSelected(this.selectedSiteID);
      };
    });
  }

  callOnAgencySelected() {
    this.selectedAgencyID = 0;
    if (this.agencyList.length > 0) {
      /* Select Default Agency */
      if (this.savedAgencyID > 0) {
        if (this.commonService._doesValueExistInJson(this.agencyList, this.savedAgencyID)) {
          this.selectedAgencyID = this.savedAgencyID;
          // console.log('callOnAgencySelected1');
          this.onAgencySelected();
        }
        // else {
        //   this.selectedAgencyID = this.agencyList[0].agencyID;
        //   // console.log('callOnAgencySelected2');
        //   this.onAgencySelected();
        // }
      }
      // else {
      //   this.selectedAgencyID = this.agencyList[0].agencyID;
      //   // console.log('callOnAgencySelected3');
      //   this.onAgencySelected();
      // }
    }
  }
  onAgencyDropdownTypeSelected() {
    this.selectedAgencyID = 0;
    this.selectedSiteID = 0;
    this.referralRosterService.setRRAgencyDropdownTypeSelected(this.agencyDropdownTypeSelected);
  }

  onAgencySelected() {
    if (this.selectedAgencyID > 0) {
      // console.log('onAgencySelected');
      this.getSiteSelected();
    }
  }

  getSiteSelected() {
    if (this.agencyDropdownTypeSelected == 2) {
      /* Only HP Site List */
      if (this.raHpSites.length > 0) {
        this.siteList = this.raHpSites.filter(d => d.agencyID == this.selectedAgencyID && d.siteCategoryType == UserSiteType.SH_HP);
        // console.log('getSiteSelected1 HPSite Only');
        this.getSiteSelected1();
      }
    } else {
      /* Both RA/HP Site List */
      if (this.raHpSites.length > 0) {
        this.siteList = this.raHpSites.filter(d => d.agencyID == this.selectedAgencyID);
        // console.log('getSiteSelected2 RA/HP Site');
        this.getSiteSelected1();
      }
    }
  }

  getSiteSelected1() {
    this.selectedSiteID = 0;
    if (this.siteList.length > 0) {
      /** Selected Default HP Site */
      if (this.savedSiteID > 0) {
        if (this.commonService._doesValueExistInJson(this.siteList, this.savedSiteID)) {
          this.selectedSiteID = this.savedSiteID;
          // console.log('getSiteSelected11');
          this.onGo();
        }
        // else {
        //   this.selectedSiteID = this.siteList[0].siteID;
        //   // console.log('getSiteSelected12');
        //   this.onGo();
        // }
      }
      // else {
      //   this.selectedSiteID = this.siteList[0].siteID;
      //   // console.log('getSiteSelected13');
      //   this.onGo();
      // }
    }
  }

  onGo() {
    if (this.selectedSiteID > 0) {
      /* save the selected AgencyID and SiteID */
      // this.referralRosterService.setRRAgencySelected(this.selectedAgencyID);
      // this.referralRosterService.setRRSiteSelected(this.selectedSiteID);
      // console.log('selectedTab: ', this.selectedTab);

      // alert('agencyID: ' + this.selectedAgencyID + '   siteID: ' + this.selectedSiteID);
      const rrInput: IReferralRosterInput = {
        SiteID: this.selectedSiteID,          // RA/HP
        UserType: ((this.is_SH_HP || this.isTAD) ? 2 : this.is_CAS ? 3 : 1),    //1 = PE, 2= HP, 3 = CAS
        DropdownSourceType: ((this.is_SH_HP || this.isTAD) ? null : (this.agencyDropdownTypeSelected == 2) ? 2 : 1),                 //1 = Application Submitted By (Agency/Site both RA/HP), 2 = Referral Submitted To (Only HP Agency/Site)
        TabStatus: ((!this.is_SH_HP && !this.isTAD) ? (this.selectedTab === 0 ? ReferralRosterTabStatus.Draft
                                                        : this.selectedTab === 1 ? ReferralRosterTabStatus.Transmitted
                                                          : ReferralRosterTabStatus.Completed
                                                      )
          : (this.is_SH_HP || this.isTAD) ? (this.selectedTab === 0 ? ReferralRosterTabStatus.Pending : ReferralRosterTabStatus.Completed) : 4),
      };
      // console.log('onGo rrInput: ', rrInput);
      this.referralRosterDataSub = this.referralRosterService.getReferralRosterData(rrInput).subscribe((res: IReferralRosterData) => {
        // console.log(res.referralRosterList);
        this.referralRosterData = res;
        this.draft = res.draft;
        this.pending = res.pending;
        this.overdue = res.overdue;
        this.acceptedPendingApproval = res.acceptedPendingApproval;
        this.inProgress = res.inProgress;
        this.moveIn = res.moveIn;
        this.notAccepted = res.notAccepted;
        this.external = res.externalReferral;
        if (this.is_SH_HP || this.isTAD) {
          if (this.selectedTab === 0) {
            this.pendingRowData = res.referralRosterList;
          } else if (this.selectedTab === 1) {
            this.completedRowData = res.referralRosterList;
          }
        } else {
          if (this.selectedTab === 0) {
            this.draftRowData = res.referralRosterList;
          } else if (this.selectedTab === 1) {
            this.transmittedRowData = res.referralRosterList;
          } else if (this.selectedTab === 2) {
            this.completedRowData = res.referralRosterList;
          }
        }
      });
    } else {
      this.toastr.error('Please select both Agency and Site.');
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;
    this.referralRosterService.setCurrentRRTabIndex(this.selectedTab);

    if (this.selectedAgencyID > 0 && this.selectedSiteID > 0) {
      // alert('agencyID: ' + this.selectedAgencyID + '   siteID: ' + this.selectedSiteID);
      const rrInput: IReferralRosterInput = {
        SiteID: this.selectedSiteID,          // RA/HP
        UserType: ((this.is_SH_HP || this.isTAD) ? 2 : this.is_CAS ? 3 : 1),    //1 = PE, 2= HP, 3 = CAS
        DropdownSourceType: ((this.is_SH_HP || this.isTAD) ? null : (this.agencyDropdownTypeSelected == 2) ? 2 : 1),                 //1 = Application Submitted By (Agency/Site both RA/HP), 2 = Referral Submitted To (Only HP Agency/Site)
        TabStatus: ((!this.is_SH_HP && !this.isTAD) ? (this.selectedTab === 0 ? ReferralRosterTabStatus.Draft
          : this.selectedTab === 1 ? ReferralRosterTabStatus.Transmitted
            : ReferralRosterTabStatus.Completed
        )
          : (this.is_SH_HP || this.isTAD) ? (this.selectedTab === 0 ? ReferralRosterTabStatus.Pending : ReferralRosterTabStatus.Completed) : 4),
      };
      // console.log('tabChanged rrInput: ', rrInput);
      this.referralRosterDataSub = this.referralRosterService.getReferralRosterData(rrInput).subscribe((res: IReferralRosterData) => {
        // console.log(res.referralRosterList);
        this.referralRosterData = res;
        this.draft = res.draft;
        this.pending = res.pending;
        this.overdue = res.overdue;
        this.acceptedPendingApproval = res.acceptedPendingApproval;
        this.inProgress = res.inProgress;
        this.moveIn = res.moveIn;
        this.notAccepted = res.notAccepted;
        this.external = res.externalReferral;
        if (this.is_SH_HP || this.isTAD) {
          if (this.selectedTab === 0) {
            this.pendingRowData = res.referralRosterList;
          } else if (this.selectedTab === 1) {
            this.completedRowData = res.referralRosterList;
          }
        } else {
          if (this.selectedTab === 0) {
            this.draftRowData = res.referralRosterList;
          } else if (this.selectedTab === 1) {
            this.transmittedRowData = res.referralRosterList;
          } else if (this.selectedTab === 2) {
            this.completedRowData = res.referralRosterList;
          }
        }
      });
    }
  }

  onDraftGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.draftGridApi = params.api;
    this.draftGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.draftGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'clientEligibleFor') {
          allColumnIds.push(column.colId);
        }
      }
    });
    this.draftGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
  }
  onTransmittedGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.transmittedGridApi = params.api;
    this.transmittedGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.transmittedGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'clientEligibleFor') {
          allColumnIds.push(column.colId);
        }
      }
    });
    this.transmittedGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
  }
  onPendingGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.pendingGridApi = params.api;
    this.pendingGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.pendingGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'referralStatusTypeDescription') {
          allColumnIds.push(column.colId);
        }
      }
    });
    this.pendingGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
  }
  onCompletedGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.completedGridApi = params.api;
    this.completedGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.completedGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'referralStatusTypeDescription') {
          allColumnIds.push(column.colId);
        }
      }
    });
    this.completedGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
  }

  onVCSReferralDeleted(referralID: number) {
    /** re-loading the ag-grid */
    this.onGo();
  }

  withdrawnReasonParentPage(result: ReferralRosterWithdrawnData, cell: IReferralRosterList) {
    // alert('reason: ' + result.withdrawnReason + '   <br/> comment: ' + result.withdrawnComment);
    // alert('cell: ' + cell.agreementTypeDescription);
    const vcsReferralList: VCSReferralData[] = [];
    if (result && cell) {
      const referralObj: VCSReferralData = {
        vcsReferralID: cell.vcsReferralID,
        pactApplicationID: cell.pactApplicationID,
        vcsID: null,
        clientID: cell.clientID,
        referralDate: cell.referralDate,
        clientSourceType: null,
        referringAgencyID: cell.referringAgencyID,
        referringSiteID: cell.referringSiteID,
        hpAgencyID: cell.hpAgencyID,
        hpSiteID: cell.hpSiteID,
        siteAgreementPopulationID: cell.siteAgreementPopulationID,
        referralGroupGUID: null,
        referralType: cell.referralType,
        referralReceivedType: null,
        referralReceivedDate: null,
        referralReceivedOtherSpecify: null,
        isScheduleMandatory: null,
        unitType: cell.unitType,
        matchPercent: cell.matchPercentage,
        referralStatusType: ReferralStatusType.Withdrawn,
        cocType: null,
        withdrawnReasonType: result.withdrawnReason,
        referralClosedComment: result.withdrawnComment,
        interviewDate: cell.interviewDate || null,
        interviewTime: cell.interviewTime || null,
        interviewAddress: cell.interviewAddress,
        interviewCity: cell.interviewCity,
        interviewState: cell.interviewState,
        interviewZip: cell.interviewZip,
        interviewPhoneNo: cell.interviewPhoneNo,
        placementMatchCriteria: null,
        createdBy: null,
        updatedBy: null
      };
      vcsReferralList.push(referralObj);
      if (vcsReferralList) {
        this.saveVCSReferralSub = this.c2vService.saveVCSReferral(vcsReferralList).subscribe(res => {
          if (res >= 0) {
            this.toastr.success('Client Referral Withdrawn.');
            this.onGo();
          }
        });
      }
    }
  }

  onInterviewOutcomeClickParent(rrSelected, editable = false) {
    this.referralRosterService.setReferralRosterSelected(rrSelected);
    this.referralRosterService.setPlacementOutcomeEditableFlag(editable);
    this.router.navigate(['/vcs/referral-roster/interview-outcome']);
  }

  // selectStatsFilter(value: string) {
  //   if (value === 'pending') {
  //     this.flagClicked = 'Pending Referrals: ';
  //     this.setFilter('referralStatusType', ReferralStatusType.Pending);
  //   } else if (value === 'overdue') {
  //     this.flagClicked = 'Overdue Referrals:';
  //     this.setFilter('referralStatusType', ReferralStatusType.Overdue);
  //   } else if (value === 'acceptedPendingApproval') {
  //     this.flagClicked = 'Accepted but Pending Approval Referrals:';
  //     this.setFilter('referralStatusType', ReferralStatusType.AcceptedPendingApproval);
  //   } else if (value === 'moveIn') {
  //     this.flagClicked = 'MoveIn Referrals:';
  //     this.setFilter('referralStatusType', ReferralStatusType.MoveInVerified);
  //   } else if (value === 'notAccepted') {
  //     this.flagClicked = 'Not Accepted Referrals:';
  //     this.setFilter('referralStatusType', ReferralStatusType.NotAccepted);
  //   } else if (value === 'External') {
  //     this.flagClicked = 'External Referrals:';
  //     this.setFilter('referralType', 526);
  //   }
  // }

  // setFilter(value: string, statusCode: number) {
  //   if (this.is_SH_PE) {
  //     if (statusCode === 512 || statusCode === 513 || statusCode === 518) {
  //       this.selectedTab = 1;
  //       this.gridOptions.api.setFilterModel(null);
  //       const instance = this.transmittedGridApi.getFilterInstance(value);
  //       instance.selectNothing(value, statusCode);
  //       instance.selectValue(statusCode);
  //       instance.applyModel();
  //       this.transmittedGridApi.onFilterChanged();
  //     } else if (statusCode === 517 || statusCode === 520 || statusCode === 526) {
  //       this.selectedTab = 2;
  //       this.gridOptions.api.setFilterModel(null);
  //       const instance = this.completedGridApi.getFilterInstance(value);
  //       instance.selectNothing(value);
  //       instance.selectValue(statusCode);
  //       instance.applyModel();
  //       this.completedGridApi.onFilterChanged();
  //     }
  //   } else if (this.is_SH_HP) {
  //     if (statusCode === 512 || statusCode === 513 || statusCode === 518) {
  //       this.selectedTab = 0;
  //       this.gridOptions.api.setFilterModel(null);
  //       const instance = this.pendingGridApi.getFilterInstance(value);
  //       instance.selectNothing(value, statusCode);
  //       instance.selectValue(statusCode);
  //       instance.applyModel();
  //       this.pendingGridApi.onFilterChanged();
  //     } else if (statusCode === 517 || statusCode === 520 || statusCode === 526) {
  //       this.selectedTab = 1;
  //       this.gridOptions.api.setFilterModel(null);
  //       const instance = this.completedGridApi.getFilterInstance(value);
  //       instance.selectNothing(value);
  //       instance.selectValue(statusCode);
  //       instance.applyModel();
  //       this.completedGridApi.onFilterChanged();
  //     }
  //   }
  // }

  // selectNothing(value: string, statusCode: number) {
  //   if (this.is_SH_PE) {
  //     if (statusCode === 512 || statusCode === 513 || statusCode === 518) {
  //       const instance = this.transmittedGridApi.getFilterInstance(value);
  //       instance.selectNothing();
  //       instance.applyModel();
  //       this.transmittedGridApi.onFilterChanged();
  //     } else if (statusCode === 517 || statusCode === 520 || statusCode === 526) {
  //       const instance = this.completedGridApi.getFilterInstance(value);
  //       instance.selectNothing();
  //       instance.applyModel();
  //       this.completedGridApi.onFilterChanged();
  //     }
  //   } else if (this.is_SH_HP) {
  //     if (statusCode === 512 || statusCode === 513 || statusCode === 518) {
  //       const instance = this.pendingGridApi.getFilterInstance(value);
  //       instance.selectNothing();
  //       instance.applyModel();
  //       this.pendingGridApi.onFilterChanged();
  //     } else if (statusCode === 517 || statusCode === 520 || statusCode === 526) {
  //       const instance = this.completedGridApi.getFilterInstance(value);
  //       instance.selectNothing();
  //       instance.applyModel();
  //       this.completedGridApi.onFilterChanged();
  //     }
  //   }
  // }

  refreshAgGrid(val: string) {
    this.flagClicked = '';
    if (val === 'd') {
      this.dgridOptions.api.setFilterModel(null);
    } else if (val === 't') {
      this.tgridOptions.api.setFilterModel(null);
    } else if (val === 'p') {
      this.pgridOptions.api.setFilterModel(null);
    } else if (val === 'c') {
      this.cgridOptions.api.setFilterModel(null);
    }
  }

  onBtExport(val: string) {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: (val === 'd' ? 'draftReferralReport-' : val === 't' ? 'TransmittedReferralReport-' : val === 'p' ? 'PendingReferralReport-' : 'CompletedReferralReport') + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    if (val === 'd') {
      this.draftGridApi.exportDataAsExcel(params);
    } else if (val === 't') {
      this.transmittedGridApi.exportDataAsExcel(params);
    } else if (val === 'p') {
      this.pendingGridApi.exportDataAsExcel(params);
    } else if (val === 'c') {
      this.completedGridApi.exportDataAsExcel(params);
    }
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.selectedTabSub) {
      this.selectedTabSub.unsubscribe();
    }
    if (this.referralRosterDataSub) {
      this.referralRosterDataSub.unsubscribe();
    }
    if (this.saveVCSReferralSub) {
      this.saveVCSReferralSub.unsubscribe();
    }
    if (this.selectedAgencySub) {
      this.selectedAgencySub.unsubscribe();
    }
    if (this.selectedSiteSub) {
      this.selectedSiteSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }
}
