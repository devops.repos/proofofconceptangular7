import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewOutcomeComponent } from './interview-outcome.component';

describe('InterviewOutcomeComponent', () => {
  let component: InterviewOutcomeComponent;
  let fixture: ComponentFixture<InterviewOutcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewOutcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewOutcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
