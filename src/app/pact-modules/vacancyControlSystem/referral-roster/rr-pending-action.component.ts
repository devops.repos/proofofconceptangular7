import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IReferralRosterList } from './referral-roster-interface.model';
import { C2vService } from '../client-awaiting-placement/c2v.service';
import { IClientAwaitingPlacementData, IC2VReferralHandshakeData } from '../client-awaiting-placement/c2v-interfaces.model';
import { Router } from '@angular/router';
import { iHousingApplicationSupportingDocumentsData } from '../../supportiveHousingSystem/consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications.model';
import { RrApplicationPackageDialogComponent } from './rr-application-package-dialog/rr-application-package-dialog.component';
import { MatDialog } from '@angular/material';
import { ReferralType } from 'src/app/models/pact-enums.enum';
import { Subscription } from 'rxjs';
import { ReferralRosterService } from './referral-roster.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-rr-pending-action',
  template: `
    <mat-icon
      class="rr-pending-icon"
      color="warn"
      [matMenuTriggerFor]="rrPendingAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #rrPendingAction="matMenu">
      <button mat-menu-item (click)="onApplicationPackageClick()">
        <mat-icon color="primary">assignment</mat-icon>Application Package
      </button>
      <button mat-menu-item (click)="onUpdateOutcomeClick()">
        <mat-icon color="primary">assignment</mat-icon>Update Outcome
      </button>
      <button mat-menu-item
        *ngIf="params.data.referralType == regularReferral &&
              ((!params.data.interviewDate && !params.data.interviewTime) ||
                params.data.canHPScheduleInterview)"
        (click)="onUpdateSelected()">
        <mat-icon color="primary">update</mat-icon>
        <span>Update Interview Details</span>
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .rr-pending-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class RRPendingActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  rrSelected: IReferralRosterList;

  regularReferral = ReferralType.Regular;
  sidedoorReferral = ReferralType.Sidedoor;

  // Global Variables
  housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null,
    expandSection: null
  };

  capByPactApplicationIDSub: Subscription;

  constructor(
    private c2vService: C2vService,
    private router: Router,
    private dialog: MatDialog,
    private referralRosterService: ReferralRosterService,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService
  ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onApplicationPackageClick() {
    // this.housingApplicationSupportingDocumentsData.agencyNumber = '1402';
    // this.housingApplicationSupportingDocumentsData.siteNumber = '003';
    // this.housingApplicationSupportingDocumentsData.firstName = 'KRISTEL';
    // this.housingApplicationSupportingDocumentsData.lastName = 'MONDENSIR';
    // this.housingApplicationSupportingDocumentsData.dob = '07/21/1986';
    // this.housingApplicationSupportingDocumentsData.cin = '';
    // this.housingApplicationSupportingDocumentsData.ssn = '064725878';
    // this.housingApplicationSupportingDocumentsData.pactClientId = null;
    // this.housingApplicationSupportingDocumentsData.approvalExpiryDate = '12/12/2019';
    // this.housingApplicationSupportingDocumentsData.pactApplicationId = 212416;
    // this.housingApplicationSupportingDocumentsData.expandSection = 1;

    this.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.referringAgencyNo;
    this.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.referringSiteNo;
    this.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    this.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    this.housingApplicationSupportingDocumentsData.dob = this.params.data.dateOfBirth;
    this.housingApplicationSupportingDocumentsData.cin = '';
    this.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    this.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.clientID;
    this.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalExpiryDate;
    this.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
    this.housingApplicationSupportingDocumentsData.expandSection = 1;
    // console.log(this.housingApplicationSupportingDocumentsData);
    this.openDialog();
  }

  // Open Housing Application Supporting Documents Dialog
  openDialog(): void {
    this.dialog.open(RrApplicationPackageDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: this.housingApplicationSupportingDocumentsData
    });
  }

  onUpdateOutcomeClick() {
    this.rrSelected = this.params.data;
    this.params.context.componentParent.onInterviewOutcomeClickParent(this.rrSelected, true);
  }

  onUpdateSelected() {
    this.rrSelected = this.params.data;
    const handshakeDataFinal: IC2VReferralHandshakeData[] = [];
    const handshakeData: IC2VReferralHandshakeData = {
      hpAgencyID: this.rrSelected.hpAgencyID,
      hpSiteID: this.rrSelected.hpSiteID,
      hpAgencyNo: +this.rrSelected.hpAgencyNo,
      hpSiteNo: this.rrSelected.hpSiteNo,
      hpAgencyName: this.rrSelected.hpAgencyName,
      hpSiteName: this.rrSelected.hpSiteName,
      primaryServiceContractID: this.rrSelected.primaryServiceContractID,
      primaryServiceContractName: this.rrSelected.primaryServiceContractName,
      agreementPopulationID: this.rrSelected.agreementPopulationID,
      populationName: this.rrSelected.populationName,
      siteAgreementPopulationID: this.rrSelected.siteAgreementPopulationID,
      agreementTypeDescription: this.rrSelected.agreementTypeDescription,
      siteLocationType: this.rrSelected.siteLocationType,
      siteLocationTypeDescription: this.rrSelected.siteLocationTypeDescription,
      siteAddress: this.rrSelected.siteAddress,
      interviewAddress: this.rrSelected.interviewAddress,
      interviewCity: this.rrSelected.interviewCity,
      interviewState: this.rrSelected.interviewState,
      interviewZip: this.rrSelected.interviewZip,
      interviewPhoneNo: this.rrSelected.interviewPhoneNo,
      unitsOccupied: this.rrSelected.unitsOccupied,
      unitsAvailable: this.rrSelected.unitsAvailable,
      totalUnits: this.rrSelected.totalUnits,
      pendingReferrals: this.rrSelected.pendingReferrals,
      assignUnitType: [],
      selectedUnitType: this.rrSelected.unitType,
      assignUnitTypeDescription: this.rrSelected.unitTypeDescription,
      matchPercentage: this.rrSelected.matchPercentage,
      placementMatchCriteria: null,
      interviewDate: this.rrSelected.interviewDate ? new Date(this.rrSelected.interviewDate) : null,
      interviewTime: this.rrSelected.interviewTime,
      changedInterviewDate: this.rrSelected.interviewDate ? new Date(this.rrSelected.interviewDate) : null,
      changedInterviewTime: this.rrSelected.interviewTime,
      availableInterviewTimeSlot: null
    };
    handshakeDataFinal.push(handshakeData);
    this.capByPactApplicationIDSub = this.c2vService.getClientAwaitingPlacementByPactApplicationID(this.rrSelected.pactApplicationID).subscribe((res: IClientAwaitingPlacementData) => {
      // alert(res.clientName);
      if (res) {
        this.c2vService.setClientAwaitingPlacementSelected(res);
        this.c2vService.setReferralSiteSelectedList(handshakeDataFinal);
        this.referralRosterService.setisRoutedFromRR(true);
        this.referralRosterService.setCurrentRRTabNo(3); // 3=Pending Tab
        this.router.navigate(['/vcs/c2v-referral-handshake-success']);
      } else {
        // this.toastr.error('Client is not in ClientAwaitingPlacement List');
        const title = 'Alert';
        const primaryMessage = `You cannot schedule an interview for this client, their application approval is expired.`;
        const secondaryMessage = `Housing Providers can select ‘Update Outcome’ to update the interview details for this client.`;
        const confirmButtonName = 'OK';
        const dismissButtonName = '';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => { },
          negativeResponse => { }
        );
      }
    });
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.capByPactApplicationIDSub) {
      this.capByPactApplicationIDSub.unsubscribe();
    }
  }
}
