import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IReferralRosterList } from './referral-roster-interface.model';
import { iHousingApplicationSupportingDocumentsData } from '../../supportiveHousingSystem/consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications.model';
import { MatDialog } from '@angular/material';
import { RrApplicationPackageDialogComponent } from './rr-application-package-dialog/rr-application-package-dialog.component';
import { ReferralStatusType } from 'src/app/models/pact-enums.enum';
// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-rr-completed-action',
  template: `
    <mat-icon
      class="rr-completed-icon"
      color="warn"
      [matMenuTriggerFor]="rrCompletedAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #rrCompletedAction="matMenu">
      <button mat-menu-item (click)="onApplicationPackageClick()">
        <mat-icon color="primary">assignment</mat-icon>Application Package
      </button>
      <button mat-menu-item
        *ngIf="params.data.referralStatusType == notAccepted ||
                params.data.referralStatusType == moveInPendingVerification ||
                params.data.referralStatusType == moveInVerified ||
                params.data.referralStatusType == moveInRejected"
        (click)="onUpdateOutcomeClick()"
        >
        <mat-icon color="warn">assignment</mat-icon>View
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .rr-completed-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class RRCompletedActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  rrSelected: IReferralRosterList;

  notAccepted = ReferralStatusType.NotAccepted;
  moveInPendingVerification = ReferralStatusType.MoveInPendingVerification;
  moveInVerified = ReferralStatusType.MoveInVerified;
  moveInRejected = ReferralStatusType.MoveInRejected;

  // Global Variables
  housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null,
    expandSection: null
  };

  constructor(
    public dialog: MatDialog,
  ) {}

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onApplicationPackageClick() {
    // this.housingApplicationSupportingDocumentsData.agencyNumber = '1402';
    // this.housingApplicationSupportingDocumentsData.siteNumber = '003';
    // this.housingApplicationSupportingDocumentsData.firstName = 'KRISTEL';
    // this.housingApplicationSupportingDocumentsData.lastName = 'MONDENSIR';
    // this.housingApplicationSupportingDocumentsData.dob = '07/21/1986';
    // this.housingApplicationSupportingDocumentsData.cin = '';
    // this.housingApplicationSupportingDocumentsData.ssn = '064725878';
    // this.housingApplicationSupportingDocumentsData.pactClientId = null;
    // this.housingApplicationSupportingDocumentsData.approvalExpiryDate = '12/12/2019';
    // this.housingApplicationSupportingDocumentsData.pactApplicationId = 212416;
    // this.housingApplicationSupportingDocumentsData.expandSection = 1;

    this.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.referringAgencyNo;
    this.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.referringSiteNo;
    this.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    this.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    this.housingApplicationSupportingDocumentsData.dob = this.params.data.dateOfBirth;
    this.housingApplicationSupportingDocumentsData.cin = '';
    this.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    this.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.clientID;
    this.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalExpiryDate;
    this.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
    this.housingApplicationSupportingDocumentsData.expandSection = 1;
    // console.log(this.housingApplicationSupportingDocumentsData);
    this.openDialog();
  }

  // Open Housing Application Supporting Documents Dialog
  openDialog(): void {
    this.dialog.open(RrApplicationPackageDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: this.housingApplicationSupportingDocumentsData
    });
  }

  onUpdateOutcomeClick() {
    this.rrSelected = this.params.data;
    // alert(JSON.stringify(this.capSelected));
      // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
    this.params.context.componentParent.onInterviewOutcomeClickParent(this.rrSelected);
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
  }
}
