import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IdleDialogBoxComponent } from 'src/app/core/session-out/idle-dialog-box/idle-dialog-box.component';
import { ReferralRosterWithdrawnData } from '../referral-roster-interface.model';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-withdrawn-dialog',
  templateUrl: './withdrawn-dialog.component.html',
  styleUrls: ['./withdrawn-dialog.component.scss']
})
export class WithdrawnDialogComponent implements OnInit, OnDestroy {

  withdrawnFormGroup: FormGroup;
  withdrawnReason: RefGroupDetails[];

  commonServiceSub: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<WithdrawnDialogComponent>,
    private router: Router,
    private commonService: CommonService
  ) {
    this.withdrawnFormGroup = this.formBuilder.group({
      withdrawnReasonCtrl: [0, Validators.compose([Validators.required, CustomValidators.dropdownRequired()])],
      withdrawnCommentCtrl: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '62';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.withdrawnReason = data.filter(d => d.refGroupID === 62);
      },
      error => {
        throw new Error(error.message);
      }
    );
  }

  onOKClick() {
    const value: ReferralRosterWithdrawnData = {
      withdrawnReason: this.withdrawnFormGroup.get('withdrawnReasonCtrl').value,
      withdrawnComment: this.withdrawnFormGroup.get('withdrawnCommentCtrl').value
    };
    // alert('Reason: ' + value.withdrawnReason + '   comment: ' + value.withdrawnComment);
    this.dialogRef.close(value);
  }

  onCancelClick(): void {
    this.dialogRef.close(null);
  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
  }
}
