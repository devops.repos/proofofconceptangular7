import { VCSPlacementMatchCriteria } from '../client-awaiting-placement/c2v-interfaces.model';

export interface IReferralRosterData {
  draft: number;
  pending: number;
  overdue: number;
  acceptedPendingApproval: number;
  inProgress: number;
  moveIn: number;
  notAccepted: number;
  externalReferral: number;
  referralRosterList: IReferralRosterList[];
}

export interface IReferralRosterList {
  vcsReferralID: number;
  pactApplicationID: number;
  clientID: number;
  referralDate: string;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  approvalExpiryDate: string;
  referringAgencyID: number;
  referringAgencyNo: string;
  referringSiteID: number;
  referringSiteNo: string;
  referringAgencyName: string;
  referringSiteName: string;
  hpAgencyID: number;
  hpAgencyNo: string;
  hpSiteID: number;
  hpSiteNo: string;
  hpAgencyName: string;
  hpSiteName: string;
  ssn: string;
  clientEligibleFor: string;
  svaPrioritization: string;
  serviceNeeds: string;
  placementCriteria: string;
  primaryServiceContractID: number;
  primaryServiceContractName: string;
  agreementPopulationID: number;
  populationName: string;
  siteAgreementPopulationID: number;
  agreementTypeDescription: string;
  siteLocationType: number;
  siteLocationTypeDescription: string;
  siteAddress: string;
  unitsOccupied: number;
  unitsAvailable: number;
  totalUnits: number;
  pendingReferrals: number;
  referralStatusType: number;
  referralStatusTypeDescription: string;
  referralType: number;
  unitType: number;
  unitTypeDescription: string;
  matchPercentage: number;
  withdrawnReasonType: number;
  referralClosedComment: string;
  interviewDate: string;
  interviewTime: string;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhoneNo: string;
  interviewLocation: string;
  canHPScheduleInterview: boolean;
  expectedMoveInDate?: string;
  referredBy: string;
  referredDate: string;
  updatedBy: string;
  updatedDate: string;
  vcsPlacementMoveInID: number;
}

export interface IReferralRosterInput {
  SiteID: number;               // RA/HP
	TabStatus: number;            //1=draft, 2=transmitted, 3=pending, 4=completed
	UserType: number;             //1 (PE/CAS), 2= HP
	DropdownSourceType: number;   //1 = Application Submitted By (Agency/Site both RA/HP), 2 = Referral Submitted To (Only HP Agency/Site)
}
export enum ReferralRosterTabStatus {
  Draft = 1,
  Transmitted = 2,
  Pending = 3,
  Completed = 4
}

export interface IVCSAgencyList {
  agencyID: number;
  agencyNo: string;
  agencyName: string;
}

export interface IVCSSiteList {
  siteID: number;
  siteNo: string;
  agencyID: number;
  siteName: string;
}

export interface IVCSAgencyInput {
  DropdownSourceType?: number;
  CheckUserSecurity: boolean;
}

export interface IVCSSiteInput {
  AgencyID: number;
  DropdownSourceType: number;
  CheckUserSecurity: boolean;
}

export interface VCSDeleteReferral {
  VCSReferralID: number;
  SiteAgreementPopulationID: number;
  UpdatedBy: string;
}

export interface ReferralRosterWithdrawnData {
  withdrawnReason: number;
  withdrawnComment: string;
}

export interface IVCSOnlineUnits {
  unitID: number;
  unitName: string;
}

export interface IPlacementOutcome {
  vcsReferralID: number;
  interviewDate: string;
  interviewTime: string;
  interviewLocation?: string;
  wasInterviewConducted: number;
  interviewOutcomeType: number;
  placementOutcomeType: number;
  expectedMoveInDate: string;
  expectedUnitID: number;
  initialMoveInDate?: string;
  moveInDate: string;
  unitID: number;
  unitName: string;
  initialClientRentalContribution: number;
  initialIncomeSourceList: IVCSIncomeSourceType[];
  initialIncomeSourceOtherSpecify: string;
  clientRentalContribution: number;
  incomeSourceList: IVCSIncomeSourceType[];
  incomeSourceOtherSpecify: string;
  reasonType: number;
  comments: string;
  referralReceivedType: number;
  referralReceivedDate: string;
  referralReceivedOtherSpecify: string;
  pactApplicationID: number;
  vcsID: string;
  clientID: number;
  clientSourceType: number;
  referringAgencyID: number;
  referringAgencyNo: string;
  referringAgencyName: string;
  referringSiteID: number;
  hpAgencyID: number;
  hpSiteID: number;
  siteAgreementPopulationID: number;
  referralGroupGUID: string;
  referralType: number;
  isScheduleMandatory: number;
  unitType: number;
  matchPercent: number;
  referralStatusType: number;
  cocType: number;
  withdrawnReasonType: number;
  referralClosedComment: string;
  isEligibilityCriteriaMatched: boolean;
  isApprovalActive: boolean;
  placementMatchCriteriaList: VCSPlacementMatchCriteria[];
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  gender: string;
  referralDate: string;
  eligibility: string;
  placementCriteria: string;
  svaPrioritization: string;
  serviceNeeds: string;
  createdByName: string;
  createdDate: string;
  updatedByName: string;
  updatedDate: string;
}
export interface IVCSIncomeSourceType {
  vcsIncomeSourceID: number;
  incomeSourceType: number;
}

export interface IVCSRaHpAgenciesSites {
  raHpAgencies: IVCSRaHpAgencyList[],
  raHpSites: IVCSRaHpSiteList[]
}

export interface IVCSRaHpAgencyList {
  agencyID: number;
  agencyNo: string;
  agencyName: string;
  siteCategoryType: number;
}

export interface IVCSRaHpSiteList {
  siteID: number;
  siteNo: string;
  agencyID: number;
  siteName: string;
  siteCategoryType: number;
}

