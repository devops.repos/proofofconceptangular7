import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { IReferralRosterInput, IVCSAgencyInput, IVCSSiteInput, VCSDeleteReferral, IReferralRosterList, IPlacementOutcome } from './referral-roster-interface.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReferralRosterService {
  private apiURL = environment.pactApiUrl + 'VCSReferralRoster';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private currentReferralRosterTabIndex = new BehaviorSubject<number>(0);
  private currentReferralRosterTabNo = new BehaviorSubject<number>(0);

  private referralRosterSelected = new BehaviorSubject<IReferralRosterList>(null);

  private editableFlag = new BehaviorSubject<boolean>(false);

  private agencySelected = new BehaviorSubject<number>(0);
  private siteSelected = new BehaviorSubject<number>(0);

  private isRoutedFromRR = new BehaviorSubject<boolean>(false);

  private AgencyDropdownTypeSelected = new BehaviorSubject<number>(1);

  constructor(private http: HttpClient) {}

  //#region Getting the ReferralRosterData from api
  getReferralRosterData(rrInput: IReferralRosterInput) {
    return this.http.post(this.apiURL + '/GetReferralRoster', JSON.stringify(rrInput), this.httpOptions);
  }
  //#endregion

  //#region Get the List of Agency and Site
  getVCSAgencyList(aInput: IVCSAgencyInput) {
    return this.http.post(this.apiURL + '/GetVCSAgencyList', JSON.stringify(aInput), this.httpOptions);
  }
  getVCSSiteList(sInput: IVCSSiteInput) {
    return this.http.post(this.apiURL + '/GetVCSSiteList', JSON.stringify(sInput), this.httpOptions);
  }
  //#endregion

  //#region Get and Set current Referral Roster tab index
  getCurrentRRTabIndex() {
    return this.currentReferralRosterTabIndex.asObservable();
  }

  setCurrentRRTabIndex(index: number) {
    this.currentReferralRosterTabIndex.next(index);
  }
  //#endregion

  //#region Get and Set current Referral Roster tab No
  /* 1=Draft, 2=transmitted, 3=Pending, 4=Completed */
  getCurrentRRTabNo() {
    return this.currentReferralRosterTabNo.asObservable();
  }

  setCurrentRRTabNo(no: number) {
    this.currentReferralRosterTabNo.next(no);
  }

  //#region Get and Set isRoutedFromRR for exit button in C2V HandShake Screen
  getisRoutedFromRR() {
    return this.isRoutedFromRR.asObservable();
  }
  setisRoutedFromRR(flag: boolean) {
    this.isRoutedFromRR.next(flag);
  }
  //#endregion

  //#region Delete the VCSReferral, VCSReferralSchedule, VCSInterviewSlot, VCSMatchCriteriaMapping
  deleteVCSReferral(input: VCSDeleteReferral) {
    return this.http.post(this.apiURL + '/DeleteVCSReferral', JSON.stringify(input), this.httpOptions);
  }
  //#endregion

  //#region Get and Set referralRoster Selected in the grid
  setReferralRosterSelected(rr: IReferralRosterList) {
    this.referralRosterSelected.next(rr);
  }

  getReferralRosterSelected() {
    return this.referralRosterSelected.asObservable();
  }
  //#endregion

  //#region Get the Online Units for the given SiteAgreementPopulationID
  getVCSUnitOnlineBySiteAgreementPopulationID(siteAgreementPopulationID: number) {
    return this.http.post(this.apiURL + '/GetVCSUnitBySiteAgreementPopulationID', JSON.stringify(siteAgreementPopulationID), this.httpOptions);
  }
  //#endregion

  //#region Get and save PlacementOutcome
  saveVCSPlacementOutcome(outcome: IPlacementOutcome) {
    return this.http.post(this.apiURL + '/SaveVCSPlacementOutcome', JSON.stringify(outcome), this.httpOptions);
  }

  getVCSPlacementOutcomeByVCSReferralID(vcsReferralId: number) {
    return this.http.post(this.apiURL + '/GetVCSPlacementOutcomeByVCSReferralID', JSON.stringify(vcsReferralId), this.httpOptions);
  }
  //#endregion

  //#region Get and Set PlacementOutcome Editable flag
  getPlacementOutcomeEditableFlag() {
    return this.editableFlag.asObservable();
  }

  setPlacementOutcomeEditableFlag(flag: boolean) {
    this.editableFlag.next(flag);
  }
  //#endregion

  getVCSMatchCriteriaByVCSReferralID(vcsReferralID: number) {
    return this.http.post(this.apiURL + '/GetVCSMatchCriteriaByVCSReferralID', JSON.stringify(vcsReferralID), this.httpOptions);
  }

  //#region Get and Set Agency/Site Selected
  setRRAgencySelected(agencyID: number) {
    this.agencySelected.next(agencyID);
  }
  getRRAgencySelected() {
    return this.agencySelected.asObservable();
  }
  setRRSiteSelected(siteID: number) {
    this.siteSelected.next(siteID);
  }
  getRRSiteSelected() {
    return this.siteSelected.asObservable();
  }

  setRRAgencyDropdownTypeSelected(type: number) {
    this.AgencyDropdownTypeSelected.next(type);
  }
  getRRAgencyDropdownTypeSelected() {
    return this.AgencyDropdownTypeSelected.asObservable();
  }

  getRaHpAgenciesSites(input: IVCSAgencyInput) {
    return this.http.post(this.apiURL + '/GetRaHpAgenciesSites', JSON.stringify(input), this.httpOptions);
  }
  //#endregion

}
