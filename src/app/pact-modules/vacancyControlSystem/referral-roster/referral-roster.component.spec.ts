import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralRosterComponent } from './referral-roster.component';

describe('ReferralRosterComponent', () => {
  let component: ReferralRosterComponent;
  let fixture: ComponentFixture<ReferralRosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralRosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralRosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
