import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { iHousingApplicationSupportingDocumentsData } from 'src/app/pact-modules/supportiveHousingSystem/consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications.model';

@Component({
  selector: 'app-rr-application-package-dialog',
  templateUrl: './rr-application-package-dialog.component.html',
  styleUrls: ['./rr-application-package-dialog.component.scss'],
})
export class RrApplicationPackageDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData,
    private dialogRef: MatDialogRef<RrApplicationPackageDialogComponent>
  ) {}

  // Close the dialog on close button
  CloseDialog() {
    this.dialogRef.close(true);
  }

  // On Init
  ngOnInit() {}
}
