import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ReferralStatusType } from 'src/app/models/pact-enums.enum';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { Subscription } from 'rxjs';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-rr-info-icon',
  template: `
    <span class="rr-info-icon">
        {{params.value}}
        <mat-icon matTooltip="{{params.data.referralStatusType === withdrawn ? (withdrawnReason + ' / ' + params.data.referralClosedComment) :
                                params.data.referralStatusType === clientAcceptedAlternativeHousing ? 'Client has accepted alternative housing' :
                                params.data.referralStatusType === administrativelyClosed ? params.data.referralClosedComment : ''}}">info</mat-icon>
    </span>
  `,
  styles: [``]
})
export class RRInfoIconComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;

  clientAcceptedAlternativeHousing = ReferralStatusType.ClientAcceptedAlternateHousing;
  withdrawn = ReferralStatusType.Withdrawn;
  administrativelyClosed = ReferralStatusType.AdministrativelyClosed;

  withdrawnReasons: RefGroupDetails[];
  withdrawnReason: string;

  commonServiceSub: Subscription;

  constructor(
    private commonService: CommonService
  ) {}

  agInit(params: any): void {
    this.params = params;
    // console.log(this.params);
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    // Get Refgroup Details
    const refGroupList = '62';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.withdrawnReasons = data.filter(d => d.refGroupID === 62);
        for (const val of this.withdrawnReasons) {
          if (val.refGroupDetailID == this.params.data.withdrawnReasonType) {
            this.withdrawnReason = val.refGroupDetailDescription;
          }
        }
      },
      error => {
        throw new Error(error.message);
      }
    );
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
  }
}
