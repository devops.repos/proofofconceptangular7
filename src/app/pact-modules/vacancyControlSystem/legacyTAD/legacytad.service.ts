import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpRequest,HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { LegacyTADDocument } from "./legacytad.model"

@Injectable({
    providedIn: 'root'
  })

  export class LegacyTADService {
    LeagacyTADUrl = environment.pactApiUrl + 'VCSLegacyTAD';


    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    constructor(private httpClient: HttpClient) { }
    
    getLegacyTADAgencyList(aID: string) {
     
      return this.httpClient.get(this.LeagacyTADUrl + "/GetLegacyTADAgencyList", { params: { agencyID: aID }, observe: 'response' });
      
    }

    getLegacyTADSiteList(aID: string,uid:string) {
      return this.httpClient.get(this.LeagacyTADUrl + "/GetLegacyTADSiteList", { params: { agencyID: aID,userID:uid }, observe: 'response'});
    }

    getLegacyTADDocument(lagacyTadDocument: LegacyTADDocument) {
      return this.httpClient.post(this.LeagacyTADUrl + "/GetLegacyTADDocument", JSON.stringify(lagacyTadDocument), this.httpOptions);
      
    }

    public viewDocument(fileName: string,filepath:string): Observable<HttpEvent<Blob>> {
      // console.log(fileName);
      const viewDocumentURL = `${this.LeagacyTADUrl}/ViewDocument`;
      return this.httpClient.request(new HttpRequest(
        'GET',
        `${viewDocumentURL}?FileName=${fileName}&FilePath=${filepath}`,
        null,
        {
          reportProgress: true,
          responseType: 'blob'
        }));
    }
   
  }
