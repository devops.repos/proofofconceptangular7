import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { LegacyTadAgency, LegacyTadSite,LegacyTADDocument,legacyReportMonth,legacyReprotType,legacyReprotYear,iDocumentLink } from './legacytad.model'
import { CommonService } from '../../../services/helper-services/common.service';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { LegacyTADService } from './legacytad.service';
import { ToastrService } from "ngx-toastr";
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { Router } from '@angular/router';
import { HttpEventType } from '@angular/common/http';


@Component({
  selector: 'app-legacytad',
  templateUrl: './legacytad.component.html',
  styleUrls: ['./legacytad.component.scss']
})
export class LegacyTadComponent implements OnInit, OnDestroy {



  userDataSub: Subscription;
  siteListSub: Subscription;
  agencyListSub: Subscription;


  siteList: LegacyTadSite[]=[];
  agencyList: LegacyTadAgency[]=[];
  reportmonthList:legacyReportMonth[]
  =[
    {leagcyTadMonth:'01',leagcyTadMonthName:'JAN'},
    {leagcyTadMonth:'02',leagcyTadMonthName:'FEB'},
    {leagcyTadMonth:'03',leagcyTadMonthName:'MAR'},
    {leagcyTadMonth:'04',leagcyTadMonthName:'APR'},
    {leagcyTadMonth:'05',leagcyTadMonthName:'MAY'},
    {leagcyTadMonth:'06',leagcyTadMonthName:'JUN'},
    {leagcyTadMonth:'07',leagcyTadMonthName:'JUL'},
    {leagcyTadMonth:'08',leagcyTadMonthName:'AUG'},
    {leagcyTadMonth:'09',leagcyTadMonthName:'SEP'},
    {leagcyTadMonth:'10',leagcyTadMonthName:'OCT'},
    {leagcyTadMonth:'11',leagcyTadMonthName:'NOV'},
    {leagcyTadMonth:'12',leagcyTadMonthName:'DEC'},
  ]
  reportYearList : legacyReprotYear[] =[
     {legacyTadYear:'2013'},
     {legacyTadYear:'2014'},
     {legacyTadYear:'2015'},
     {legacyTadYear:'2016'},
     {legacyTadYear:'2017'},
     {legacyTadYear:'2018'},
     {legacyTadYear:'2019'},
     {legacyTadYear:'2020'},
    ];
  reportTypelist :legacyReprotType[]=
  [
    {TADType:'S',Description:'Sumbmited TAD'},
    {TADType:'V',Description:'Verified TAD'},
  ]
  ;

  tadSubmitData : LegacyTADDocument;
  legacyTADDocumentinfo : LegacyTADDocument;


  //hpAgenciesWithTrackedSites: IVCSHpAgenciesWithTrackedSites[];

  selectedSiteID: number = 0;
  selectedAgencyID: number = 0;
  selectedAgencyNo: string  = '';
  slectedSiteNo :string='';
  selectedAgencyName:string='';

  

  
  is_SH_HP = false;
  is_CAS = false;
  canSubmit = false;

  context;
  userData: AuthData;
  legacyTadGroup: FormGroup;
  
  constructor(
    private userService: UserService,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private legacytadService: LegacyTADService,
    private router: Router,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
  ) {
   
  }

  ngOnInit() {
    this.legacyTadGroup = this.formBuilder.group({
      agencyCtrl: [''],
      siteCtrl: [''],
      reportYearCtrl: [''],
      reportMonthCtrl: [''],
      reportTypeCtrl: [''],
    });

    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.userData = userdata;
        // console.log(this.userData);
        // console.log(this.commonService._doesValueExistInJson(this.userData.userFunctions, 37));
        if (this.userData.siteCategoryType.length > 0) {
          this.is_SH_HP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_HP);
          this.is_CAS = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.CAS);
          // this.is_CAS = true;
        }

        if (this.is_SH_HP) {
        this.canSubmit = this.commonService._doesValueExistInJson(this.userData.userFunctions, 37);
         this.selectedAgencyID= this.userData.agencyId;
         this.selectedAgencyName=  this.userData.agencyName;
         this.selectedAgencyNo=  this.userData.agencyNo;

         this.loadAgencys(this.selectedAgencyID);
         //this.refershSites(this.selectedAgencyID);
        }

        if (this.is_CAS) {
          this.canSubmit = true;
          this.loadAgencys(0);
        }
      }
    });
   setTimeout(() => {
      if (this.selectedAgencyID > 0) {
       this.refershSites(this.selectedAgencyID);
      }
    }, 1000);
  }

  onAgencySelected() {
    
  }

  onSubmitClick()
  {
    if (this.validationInput())
    {
      this.tadSubmitData = {
        agencyNo:this.selectedAgencyNo,
        siteNo :this.legacyTadGroup.get('siteCtrl').value,
        siteId:this.selectedSiteID,
      reportMonth: this.legacyTadGroup.get('reportMonthCtrl').value,
       reportYear:this.legacyTadGroup.get('reportYearCtrl').value,
      reportType:this.legacyTadGroup.get('reportTypeCtrl').value,
      filenetDocid:null,
      documentFileName:'',
      documentFilePath:'',
      userId:this.userData.optionUserId
      }
       this.legacytadService.getLegacyTADDocument(this.tadSubmitData).subscribe(res => {
        if (res) {
          this.legacyTADDocumentinfo = res as LegacyTADDocument;
          this.openLegacyTADReport(this.legacyTADDocumentinfo.filenetDocid,this.legacyTADDocumentinfo.documentFileName,this.legacyTADDocumentinfo.documentFilePath);
          //alert(this.legacyTADDocumentinfo.documentFileName);
        }
        else {
        this.toastr.error('Legacy TAD not found.');
        }
      });
  
    
    }
  }

  validationInput = (): boolean => {
    /*alert(this.legacyTadGroup.get('agencyCtrl').value);
    alert(this.legacyTadGroup.get('siteCtrl').value);
    alert(this.legacyTadGroup.get('reportYearCtrl').value);
    alert(this.legacyTadGroup.get('reportMonthCtrl').value);
    alert(this.legacyTadGroup.get('reportTypeCtrl').value);*/
    if (this.is_CAS) {
    if ( this.legacyTadGroup.get('agencyCtrl').value === null || this.legacyTadGroup.get('agencyCtrl').value === 0 || this.legacyTadGroup.get('agencyCtrl').value === ''  ) {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Please select agency.");
        return false;
      }
    }
  }
    if (this.legacyTadGroup.get('siteCtrl').value === null || this.legacyTadGroup.get('siteCtrl').value === 0 || this.legacyTadGroup.get('siteCtrl').value === '') {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Please select site.");
        return false;
      }
    }
   
    else if (this.legacyTadGroup.get('reportYearCtrl').value === null || this.legacyTadGroup.get('reportYearCtrl').value === 0 || this.legacyTadGroup.get('reportYearCtrl').value === '') {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Please select report year.");
        return false;
      }
    }
    else if (this.legacyTadGroup.get('reportMonthCtrl').value === null || this.legacyTadGroup.get('reportMonthCtrl').value === 0 || this.legacyTadGroup.get('reportMonthCtrl').value === '') {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Please select report month.");
        return false;
      }
    }

    else if (this.legacyTadGroup.get('reportTypeCtrl').value === null || this.legacyTadGroup.get('reportTypeCtrl').value === 0 || this.legacyTadGroup.get('reportTypeCtrl').value === '') {
      if (!this.toastr.currentlyActive) {
        this.toastr.error("Please select report type.");
        return false;
      }
    }
    return true;
  }


 
  loadAgencys(agid :number) {
    this.agencyListSub = this.legacytadService.getLegacyTADAgencyList(agid.toString()).subscribe(res => {
      if (res) {
        this.agencyList = res.body as LegacyTadAgency[];
      }
    });
  }


  refershSites(agid:number) {
    this.selectedAgencyID= agid;
    var itm = this.agencyList.find(x => x.agencyId === this.selectedAgencyID) as LegacyTadAgency
    //alert(this.selectedAgencyNo);

      if (itm !== null) {
        console.log(itm);
       this.selectedAgencyNo = itm.agencyNo;
      }
    //  alert(this.selectedAgencyNo);
      
    this.siteListSub = this.legacytadService.getLegacyTADSiteList(agid.toString(),this.userData.optionUserId.toString()).subscribe(res => {
      if (res) {
        this.siteList = res.body as LegacyTadSite[];
      }
    });
  }


  
  openLegacyTADReport(LegacyTADFilenetDocID: number,documentFileName:string,documentfilepath:string) {
    if (LegacyTADFilenetDocID) {
      this.commonService.getFileNetDocumentLink(LegacyTADFilenetDocID.toString()).subscribe(res => {
        const data = res as iDocumentLink;
        if (data && data.linkURL) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    }
    else {
      if (documentFileName) {
        this.legacytadService.viewDocument(documentFileName,documentfilepath).subscribe(
          data => {
            switch (data.type) {
              case HttpEventType.Response:
                const downloadedFile = new Blob([data.body], {
                  type: data.body.type
                });
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                  window.navigator.msSaveOrOpenBlob(downloadedFile, documentFileName);
                } else {
                  this.commonService.OpenWindow(URL.createObjectURL(downloadedFile));
                  this.commonService.setIsOverlay(false);
                }
                break;
            }
          }, error => {
            if (!this.toastr.currentlyActive) {
              this.toastr.error('There was an error in opening the document.', 'Viewing Document Failed!');
            }
          });
      }
    }
  }

  onExitClick()
  {
    this.router.navigate(['/dashboard']);
  }

  onClearClick()
  {
   
      this.legacyTadGroup.reset();
  }
  
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.siteListSub) {
      this.siteListSub.unsubscribe();
    }
    if (this.agencyListSub) {
      this.agencyListSub.unsubscribe();
    }
  }
}

