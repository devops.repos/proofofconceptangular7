export interface LegacyTadAgency {
  agencyNo: string;
  agencyName: string;
  agencyId :number;
 
}

export interface LegacyTadSite {
  siteNo:string;
  agencyno:string;
  siteId:number;
  siteName:string;
}

export interface legacyReportMonth {
  leagcyTadMonth: string;
  leagcyTadMonthName :string;

}

export interface legacyReprotYear {
  legacyTadYear: string;
  
}


export interface legacyReprotType {
  TADType: string;
  Description :string;
  
}

export interface LegacyTADDocument {
  agencyNo:string;
  siteNo :string;
  siteId?:number;
  reportMonth:string;
  reportYear:string;
  reportType:string;
  filenetDocid?:number;
  documentFileName:string;
  documentFilePath:string;
  userId?:number;
  
}

export interface iDocumentLink {
  linkURL: string;
}



