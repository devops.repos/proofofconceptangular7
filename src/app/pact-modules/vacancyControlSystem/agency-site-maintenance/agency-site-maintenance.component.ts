import { Component, OnInit, ViewChild } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { HttpClient } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import 'ag-grid-enterprise';
import { AgencySiteMaintenanceActionComponent } from './agency-site-maintenance-action.component';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { ActivatedRoute, Router, Event } from '@angular/router';
import { SiteAdminService } from './site-admin.service';
import { RaSiteRequestService } from '../site-request-ra/site-request-ra.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { CommonService } from '../../../services/helper-services/common.service';
import { AgencySiteData, AgencyData, siteDemographic } from "./agency-site-model"
import { Subscription,Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-agency-site-maintenance',
  templateUrl: './agency-site-maintenance.component.html',
  styleUrls: ['./agency-site-maintenance.component.scss']
})
export class AgencySiteMaintenanceComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;


  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  pagination;
  rowSelection;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;

  RA;
  isPE;
  isHP;
  isIH;
  agencyInfo: AgencyData;
  agencyList: AgencyData[];
  userData: AuthData;
  userDataSub: Subscription;
  agencyID;
  context;
  overlayLoadingTemplate: string = '';
  overlayNoRowsTemplate: string = '';
  raSiteRequestSvcSub : Subscription;


  public gridOptions: GridOptions;
  rowData: AgencySiteData[] = [];
  sites: siteDemographic[] = [];
  agencyName: string = "";;
  agencyAddress: string = "";
  activatedRouteSub: Subscription;
  searchStringCtrl = new FormControl();
  agencyGroup: FormGroup;
  filteredAgencyData: Observable<any[]>;
  siteSelectedSub: Subscription; 
  userid:number=0;

  constructor(private http: HttpClient,
    private sidenavService: NavService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private siteAdminservice: SiteAdminService,
    private userService: UserService,
    private commonService: CommonService,
    private raSiteRequestService : RaSiteRequestService
  ) {

    this.gridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;
    //this.gridOptions.api.hideOverlay();

    this.columnDefs = [
      {
        headerName: 'Site ID',
        filter: 'agTextColumnFilter',
        field: "siteID",
        hide: true,
        suppressFilter: true,
        suppressToolPanel: true
      },
      {
        headerName: 'Agency/Site#',
        field: 'agencySite',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Name',
        field: 'name',
        width: 350,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Address',
        field: 'siteAddress',
        width: 300,
        filter: 'agTextColumnFilter'
      },

      {
        headerName: 'Site Status',
        field: 'siteStatus',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Housing Program Model/Site type',
        field: 'siteType',
        width: 270,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Category',
        width: 150,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.siteCategoryType ===5  ? 'Referring Agency' : params.data.siteCategoryType ===6 ? 'Placement Agency' : 'Housing Provider'
        },
      },
      

      {
        headerName: 'Site Location',
        field: 'locationType',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 70,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'actionRenderer',
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.pagination = true;
    this.frameworkComponents = {
      actionRenderer: AgencySiteMaintenanceActionComponent
    };

  }

  ngOnInit() {
    this.agencyGroup = this.formBuilder.group({
      searchStringCtrl: ['']
    });

    this.isIH = false;
    this.isHP = false;
    this.RA= false;
    this.isPE = false;

    this.agencyInfo = {} as AgencyData;
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Sites are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Sites are available</span>';

    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        this.userid = this.userData.optionUserId;
        if (this.userData.siteCategoryType.length > 0) {
          this.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
          this.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
          this.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
          this.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
        }
        this.siteAdminservice.getAgencyList(null, null)
          .subscribe(
            res => {
              this.agencyList = res.body as AgencyData[];
              this.filteredAgencyData = this.searchStringCtrl.valueChanges.pipe(
                startWith(''),
                map(value => this._filter(value))
              );
              if (this.isHP === true || this.RA === true || this.isPE === true) {
                if (this.userData.agencyId > 0) {
                  this.gridRefresh(this.userData.agencyId);
                  this.getAgencyinfo(this.userData.agencyId);
                }
              }

              //alert(this.isIH);
              if (this.isIH === true) {
                this.getAgecnyID();
                //alert(this.agencyID);

                if (this.agencyID !== null && this.agencyID !== 0) {
                  this.gridRefresh(this.agencyID);
                  this.getAgencyinfo(this.agencyID);
                }
              }
            },
            error => console.error('Error!', error)
          );
      }
    });


  }

  getAgecnyID() {

    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const selctedagencyID = params.get('agencyID');
      if (selctedagencyID === null) {
        return;
      } else {
        const vvid = parseInt(selctedagencyID);
        if (isNaN(vvid)) {
          throw new Error('Invalid agency ID!!');
        } else if (vvid > 0 && vvid < 2147483647) {
          this.agencyID = vvid;
        }
      }
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setDomLayout('autoHeight');

    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function (column) {

      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);

      }
      //this.populateGrid();
      params.api.sizeColumnsToFit();



    });
  }


  refreshSites(agid: number) {
    this.gridRefresh(agid);
    this.getAgencyinfo(agid);
    this.agencyID=agid;
  }

  referSiteToParent(cell) {
     this.siteAdminservice.setSiteSelected(cell);
  }

  referAddPrimaryServiceAggrementToParent(cell) {

    const newitem: siteDemographic = {
      siteID: cell.siteID,
      siteCategoryType: cell.siteCategoryType,
      siteNo: null,
      agencyID: null,
      housingProgram: null,
      name: null,
      address: null,
      city: null,
      state: null,
      zip: null,
      locationType: null,
      referralAvailableType: null,
      tcoReadyType: null,
      tcoContractStartDate: null,
      taxCreditType: null,
      maxIncomeForStudio: null,
      maxIncomeForOneBR: null,
      levelOfCareType: null,
      contractType: null,
      contractStartDate: null,
      siteTrackedType: null,
      tadLiasion: null,
      sameAsSiteInterviewAddressType: null,
      interviewAddress: null,
      interviewCity: null,
      interviewState: null,
      interviewZip: null,
      interviewPhone: null,
      siteFeatures: null,
      siteTypeDesc: null,
      agencyName: null,
      agencyNo: null,
      userId: 0
    }
    this.sites.push(newitem);
    this.siteAdminservice.setSiteCreated(this.sites);
  }

  gridRefresh(agid: number) {
  if (agid) {
    this.siteAdminservice.getSites(agid.toString(),this.userid.toString())
      .subscribe(
        res => {
          if (this.isIH) {
            this.rowData = res.body as AgencySiteData[];
          }
          else {
            const data = res.body as AgencySiteData[];
            this.rowData = data.filter(d => (d.siteStatus === 'Active'));

          }
          // this.gridOptions.api.refreshView();
        },
        error => console.error('Error!', error)
      );
      }
  }

  getAgencyinfo(agid: number) {
    var itm = this.agencyList.find(x => x.agencyID === agid) as AgencyData
    if (itm) {
    if (itm !== null) {
      this.agencyAddress = itm.agencyAddress.trim() + ',' + itm.city.trim() + ' ,' + itm.state.trim() + ', ' + itm.zip.trim()
      this.agencyName = itm.name.trim();
    }
  }

  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onRowSelected(event) {
    if (event.node.selected) {
      //alert('row ' + event.node.data.siteID + ' selected = ' + event.node.selected);
      // alert(event.node);        
    }
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'AgencySiteReport-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

    console.log('AgencySiteReport-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  private _filter(value: string): AgencyData[] {
    const filterValue = value.toLowerCase();
    return this.agencyList.filter(agency => agency.name.toLowerCase().includes(filterValue) || agency.agencyNo.includes(filterValue));
  }

  ngOnDestroy = () => {
    if(this.siteSelectedSub){
     this.siteSelectedSub.unsubscribe();
    }
    if(this.raSiteRequestSvcSub) this.raSiteRequestSvcSub.unsubscribe();
  }

  editAgencydetail(agid: number) {
    
    if (agid) {
      this.siteAdminservice.setAgencySelected(agid);
      this.router.navigate(['./admin/editagencydetail']);
    }
  }  

  
  /*onBtPrint() {
   var gridApi = this.gridOptions.api;
     this.setPrinterFriendly(gridApi);
 
   setTimeout(function() {
     print();
     this.setNormal(gridApi);
   }, 2000);
 }
 
  setPrinterFriendly(api) {
   var eGridDiv = document.querySelector('#myGrid') as HTMLDivElement  ;
  eGridDiv.style.height = '';
  
   api.setDomLayout('print');
 }
 
  setNormal(api) {
   var eGridDiv = document.querySelector('#myGrid') as HTMLDivElement ;
   eGridDiv.style.width = '600px';
   eGridDiv.style.height = '200px';
 
   api.setDomLayout(null);
 }
 
 // setup the grid after the page has finished loading
 /*document.addEventListener('DOMContentLoaded', function() {
   var gridDiv = document.querySelector('#myGrid');
   new agGrid.Grid(gridDiv, gridOptions);
 });*/

  /**printPage() {
    this.onBtPrint()
   // window.print()
  }*/
}


