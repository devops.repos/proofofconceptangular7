import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencySiteMaintenanceComponent } from './agency-site-maintenance.component';

describe('AgencySiteMaintenanceComponent', () => {
  let component: AgencySiteMaintenanceComponent;
  let fixture: ComponentFixture<AgencySiteMaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencySiteMaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgencySiteMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
