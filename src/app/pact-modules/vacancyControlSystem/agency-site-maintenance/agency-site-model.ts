


export interface siteDemographic {
  siteID: number;
  siteNo: string;
  agencyID: number;
  housingProgram: string;
  name: string;
  address: string;
  city: string;
  state: string;
  zip: string;
  locationType: number;
  referralAvailableType: number;
  tcoReadyType?: number;
  tcoContractStartDate: string;
  taxCreditType?: number;
  maxIncomeForStudio: string;
  maxIncomeForOneBR: string;
  levelOfCareType: number;
  contractType?: number;
  contractStartDate: string;
  siteTrackedType: number;
  tadLiasion: number;
  sameAsSiteInterviewAddressType: number;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhone: string;
  siteFeatures: string;
  siteTypeDesc: string;
  agencyName: string;
  agencyNo: string;
  userId: number;
  siteCategoryType?: number;
  boroughType?: number;
  isActive?: boolean;
  isSiteDemographicTabComplete?: boolean;
  isSiteContactTabComplete?: boolean;
  isPrimaryServiceContractTabComplete?: boolean;
  isSiteApprovalTabComplete?: boolean;
  isUnitDetailTabComplete?: boolean;
  isDraft?: boolean;
  isCapsMandate?: boolean;
}

export interface siteRequestDemographic {
  siteID: number;
  siteNo: string;
  agencyID: number;
  housingProgram: string;
  name: string;
  address: string;
  city: string;
  state: string;
  zip: string;
  locationType: number;
  referralAvailableType: number;
  tcoReadyType: number;
  tcoContractStartDate: string;
  taxCreditType: number;
  maxIncomeForStudio: string;
  maxIncomeForOneBR: string;
  levelOfCareType: number;
  contractType: number;
  contractStartDate: string;
  siteTrackedType: number;
  tadLiasion: number;
  sameAsSiteInterviewAddressType: number;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhone: string;
  siteFeatures: string;
  siteTypeDesc: string;
  agencyAddress: string;
  agencyCity: string;
  agencyState: string;
  agencyZip: string;
  agencyName: string;
  agencyNo: string;
  userId: number;
  siteCategoryType?: number;
  boroughType?: number;
  isActive?: boolean;
  isSiteDemographicTabComplete?: boolean;
  isSiteContactTabComplete?: boolean;
  isPrimaryServiceContractTabComplete?: boolean;
  isSiteApprovalTabComplete?: boolean;
  isUnitDetailTabComplete?: boolean;
  isDraft?: boolean;

  agencyRequestID?: number;
  categoryTypeDesc: string;
  id?: number;
  siteRequestID?: number;
  siteType: string;//housing program model
  userName: string;

  email1SentDate: string;
  email2SentDate: string;
  responseReceivedDate: string;
  expectedAvailableDate: string;
  IsCapsMandate: boolean;
  siteRequestType: number;

  requestedBy?: number;
  requestedDate: string;
  reviewedBy?: number;
  reviewedDate: string;
  approvedType: number;
  addlApproverComments: string;

  CreatedBy: number;
  CreatedDate: string;
  UpdatedBy: number;
  UpdatedDate: string;
  RequestStatusType: number;
}

export interface SiteContact {
  siteContactID?: number;
  siteContactRequestID?: number;
  siteID?: number;
  siteRequestID?: number;
  optionUserID: number;
  firstName: string;
  lastName: string;
  title: string;
  email: string;
  phone: string;
  extension: string;
  alternatePhone: string;
  alternateExtension: string;
  fax: string;
  isActive: boolean;
  isSysAdmin: boolean;
  createdBy: number;
  createdDate: string;
  updatedBy: number;
  updatedDate: string;
}

export interface housingProgram {
  siteTypeCode: string;
  typeDescription: string;
}


export interface AgencySiteData {
  siteID: number;
  agencySite: string;
  siteStatus: string;
  name: string;
  siteType: string;
  locationType: string;
  siteCategoryType?: number;
  siteAddress: string;
}

export interface AgencyRequestData {
  agencyID: number;
  agencyNo: string;
  name: string;
  agencyType: string;
  agencyAddress: string;
  city: string;
  state: string;
  zip: string;
  userId?: number;
  sites: siteRequestDemographic[];
  userName: string;
}

export interface AgencyData {
  agencyID: number;
  agencyNo: string;
  name: string;
  agencyType: string;
  agencyAddress: string;
  city: string;
  state: string;
  zip: string;
  userId?: number;
  sites: siteDemographic[];
  userName: string;
  contactLastName: string;
  contactFirstName: string;
  contactTitle: string;
  phone: string;
  extension: string;
  alternatePhone: string;
  alternateExtension: string;
  fax: string,
  email: string
}

export interface UserOptions {
  RA?: boolean;
  isPE?: boolean;
  isHP?: boolean;
  isIH?: boolean;
  isDisableForHP: boolean;
  isDisableForInactiveSite: boolean;
  isdisableActiveRadiobutton: boolean;
  isActive?: boolean;
  optionUserId: number;
  isNewPage?:boolean;
}

export interface SiteAgreementPopulation {
  siteID: number;
  primaryServiceContractType: string;
  rentalSubsidies: string;
  totalUnits: number;
  siteAgreementPopulationID?: number;
  agreementPopulationID?: number;
  isActive?: boolean;
  userId?: number;
}

export interface SiteRequestAgreementPopulation {
  siteID?: number;
  siteRequestID?: number;

  siteAgreementPopulationID?: number;
  siteAgreementPopulationRequestID?: number;

  siteAgreementPopulationRequestType: number;

  agreementPopulationID?: number;
  primaryServiceAgreementPopName: string;
  rentalSubsidies: string;

  totalUnits: number;
  totalUnitsDataEntered: number;

  isActive?: boolean;
  userId?: number;
}

export interface SiteAgreementProfile {
  siteID: number;
  primaryServiceContractType: string;
  rentalSubsidies: string;
  totalUnits: number;
}

export interface SiteFeature {
  siteFeatureMappingID?: number;
  siteFeatureMappingRequestID?: number;
  siteID?: number;
  siteRequestID?: number;
  siteFeatureType: number;
  userId: number;
}


export interface siteApproval {
    siteID: number;
  siteTrackedType: number;
  tadLiasion: number;
  tcoContractStartDate: string;
  referralAvailableType: number;
  expectedAvailableDate: string;
  addlApproverComments: string;
  userId: number;
  isActive: boolean;
}

export interface siteRequestApproval {
  siteID?: number;
  siteRequestID?: number;
  siteTrackedType: number;
  tadLiasion: number;
  tcoContractStartDate: string;
  referralAvailableType: number;
  expectedAvailableDate: string;
  addlApproverComments: string;
  userId: number;
  userName: string;
  isActive: boolean;
  approvedType: number;
  approverEmail: string;
  requestorEmail: string;
  requestorFirstName: string;
  requestorLastName: string;

  requestStatusType: number;
}

export interface tADLiaison {
  lanID: string;
  userName: string;
  userId: number;
}

export class Unit {

  unitID: number;
  name: string;

  address: string;
  city: string;
  state: string;
  zip: string;

  isSideDoorAllowed: boolean;
  isVacancyMonitorRequired: boolean;
  isVerified?: boolean;

  siteAgreementPopulationSelected: number;
  siteAgreementPopulationID: number;
  primaryServiceAgreementPopName: string;

  unitTypeID: number;
  unitTypeDesc: string;
  unitTypeSelected: number

  unitStatusID: number;
  unitStatusDesc: string;
  unitStatusSelected: number;

  contractAgencyID: number;
  contractAgencyDesc: string;
  contractAgencySelected: number;

  unitFeatures: UnitFeature[];
  rentalSubsidies: RentalSubsidy[];

  unitFeaturesJson: string;
  unitFeaturesString: string;

  rentalSubsidyJson: string;
  rentalSubsidyString: string;

  offlineReasonType?: number;
  offlineOtherSpecify?: string;
  expectedAvailableDate?: Date;
  AddlComment?: string;
  isActive: boolean;

  userName: string;
  isVacancyMonitorRequiredString: string;
  isSideDoorAllowedString: string;
}

export interface RentalSubsidy {

  sub_Id: number;
  sub_desc: string;
}

export interface UnitFeature {

  f_Id: number;
  f_desc: string;

}

export interface PrimaryServiceContractAgreementPopulation {
  agreementPopulationID: number;
  primaryServiceAgreementPopName: string;
  siteAgreementPopulationID: string;
}

export interface agencySiteCreate {
  Agency: agencyAddModel,
  Sites: siteAddModel[]
}

export interface agencyAddModel {
  AgencyID: number;
  Name: string;
  AgencyType: string;
  Address: string;
  City: string;
  State: string;
  Zip: string;
  IsRA: number;
  IsHP: number;
  CreatedBy: number;
}

export interface siteAddModel {
  SiteID: number;
  Name: string;
  Address: string;
  City: string;
  State: string;
  Zip: string;
  SiteCategoryType: number;
  SiteRequestType: number;
  CreatedBy: number;
}

export interface UnitHistory {
  unitHistoryID: number;
  unitID: number;
  statusID?: number;
  statusDescription?: string;
  startDate: string;
  endDate: string;
  comments?: string;
  verifiedByUserId?: number;
  verifiedByUserName: string;
  verifiedByDate: string;

}


export interface StatusBarHeader {
  AgencyID: number;
  AgencyName: string;
  AgencyAddress: string;
  SiteID: number;
  SiteName: string;
  SiteAddress: string;
  SiteAgreementPopulationID: number;
  AgreementpopulationName: string;
  UnitID: number;
  UnitName: string;
}

export interface TransferSite {
  TransferFromAgencyID: number;
  TransferFromSiteID: number;
  IsTransferToExistingAgency: number;
  TransferToExistingAgencyID: number;
  TransferToAgencyName: string;
  TransferToAgencyAddress: string;
  TransferToAgencyCity: string;
  TransferToAgencyState: string;
  TransferToAgencyZip: string;
  TransferToAgencyType: string;
  TransferToExistingSiteID: number;
  TransferToAgencySiteName: string;
  TransferToAgencySiteAddress: string;
  TransferToAgencySiteCity: string;
  TransferToAgencySiteState: string;
  TransferToAgencySiteZip: string;
  TransferToAgencySiteBorough: number;
  TransferToAgencySiteCategoryType: number;
  TransferToAgencyContactLastName: string;
  TransferToAgencyContactFirstName: string;
  TransferToAgencyOffceTitle: string;
  TransferToAgencyEmail: string;
  TransferToAgencyOfficePhone: string;
  TransferToAgencyOfficePhoneExtn: string;
  TransferToAgencyFax: string;
  userId: number;

}

export interface siteCreatedStatus {
  siteId: number;
  siteCompleteStaus: number;
}





