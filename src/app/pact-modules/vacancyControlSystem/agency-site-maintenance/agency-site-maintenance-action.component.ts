import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from "ag-grid-angular";
import { AgencySiteData } from "./agency-site-model"
import { SiteAgreementRequestService } from '../site-agreement-request/site-agreement-request.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { isNumber } from 'util';

@Component({
  selector: "agency-site-maintenance-action",
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="agencySiteMaintenanceAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #agencySiteMaintenanceAction="matMenu">
      <button mat-menu-item routerLink="/admin/site-profile" (click)="editSiteClicked()">Edit Site Profile</button>
      <button mat-menu-item
        *ngIf="params.data.siteCategoryType === 7 && isRoleIH === true"
         (click)="addPrimaryServiceAgreement()" routerLink="/admin/createsitedetail">
        Add New Agreement
      </button>
      <button mat-menu-item
      *ngIf="params.data.siteCategoryType === 7 && isRoleHP === true"
       (click)="requestSiteAgreement()">
      Request Site Agreement
    </button>
      </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})



export class AgencySiteMaintenanceActionComponent implements OnInit, OnDestroy, ICellRendererAngularComp {
  params: any;
  public cell: any;
  private siteSelected: AgencySiteData;
  isRoleHP: boolean;
  isRoleIH: boolean; // internal CAS User 

  siteAgreementRequestSvcSub: Subscription;
  userDataSub: Subscription;
  userData: AuthData;

  constructor(private siteAgreementRequestSvc: SiteAgreementRequestService, private router: Router,
    private userService: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });
  }


  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    this.isRoleHP = this.params.context.componentParent.isHP;
    this.isRoleIH = this.params.context.componentParent.isIH; // internal CAS User 
  }

  editSiteClicked() {

    this.siteSelected =
      {
        siteID: this.cell.row.siteID,
        agencySite: this.cell.row.agencySite,
        siteStatus: this.cell.row.siteStatus,
        name: this.cell.row.name,
        siteType: this.cell.row.siteType,
        locationType: this.cell.row.locationType,
        siteCategoryType: this.cell.row.siteCategoryType,
        siteAddress: this.cell.row.siteAddress
      }
    this.params.context.componentParent.referSiteToParent(this.siteSelected);
  }

  addPrimaryServiceAgreement() {
    this.siteSelected =
      {
        siteID: this.cell.row.siteID,
        agencySite: this.cell.row.agencySite,
        siteStatus: this.cell.row.siteStatus,
        name: this.cell.row.name,
        siteType: this.cell.row.siteType,
        locationType: this.cell.row.locationType,
        siteCategoryType: this.cell.row.siteCategoryType,
        siteAddress: this.cell.row.siteAddress
      }
    this.params.context.componentParent.referAddPrimaryServiceAggrementToParent(this.siteSelected)
  }

  requestSiteAgreement() {
    let siteID = this.cell.row.siteID;
    let userID = this.userData.optionUserId;
    this.siteAgreementRequestSvcSub = this.siteAgreementRequestSvc.createSiteAgreementRequestFromSite(siteID, userID)
      .subscribe(
        data => {
          if (isNumber(data) && data > 0) {
            this.toastr.success("Successfully Saved", 'Save Success');
            this.router.navigate(['./admin/site-agreement-request'],
              { queryParams: { "siteRequestID": data } });
          } else {
            this.toastr.error("The SiteRequestID returned from server is not valid.", 'Site Request');
          }
        },
        error => { this.toastr.error("Create New Site Request for Agreement is Failed", 'Save Failed'); }
      );
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.siteAgreementRequestSvcSub) {
      this.siteAgreementRequestSvcSub.unsubscribe();
    }
  }

}