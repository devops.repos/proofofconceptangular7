import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { AgencySiteData, AgencyData, siteDemographic, SiteContact, siteApproval, Unit, SiteAgreementPopulation, TransferSite } from "./agency-site-model"

@Injectable({
  providedIn: 'root'
})
export class SiteAdminService {
  SiteAdminUrl = environment.pactApiUrl + 'SiteAdmin';
  getUnitsRequestURL = environment.pactApiUrl + 'Unit/GetUnits';
  saveUnitRequestURL = environment.pactApiUrl + 'Unit/SaveUnitDetails';
  deleteUnitRequestURL = environment.pactApiUrl + 'Unit/DeleteUnit';
  private siteSelected = new BehaviorSubject<AgencySiteData>(null);
  private siteprofilecreated = new BehaviorSubject<siteDemographic[]>(null);
  private agencySelected = new BehaviorSubject<number>(null);

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }


  getSites(value: string,userid: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetSites", { params: { agencyId: value,userId:userid }, observe: 'response' });
  }

  getAgencyList(value: string, value1: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetAgencyList", { params: { agencyId: value, sitecategorytype: value1 }, observe: 'response' });
  }

  getUserSiteInfo(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetUserSiteInfo", { params: { lanId: value }, observe: 'response' });
  }

  getSiteDemogramics(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetSiteDemographic", { params: { siteId: value }, observe: 'response' });
  }

  getSiteApprovalData(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetSiteApproval", { params: { siteId: value }, observe: 'response' });
  }

  getSiteContacts(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetSiteContacts", { params: { siteId: value }, observe: 'response' });
  }
  deleteSiteContact(siteContact: SiteContact) {
    return this.httpClient.post(this.SiteAdminUrl + "/DeleteSiteContact", JSON.stringify(siteContact), this.httpOptions);
  }

  saveSiteContact(siteContact: SiteContact) {
    return this.httpClient.post(this.SiteAdminUrl + "/SaveSiteContact", JSON.stringify(siteContact), this.httpOptions);
  }



  getHousingPrograms() {
    return this.httpClient.get(this.SiteAdminUrl + "/GetHousingPrograms", { observe: 'response' });
  }

  saveSiteDemographic(SiteDemographic: siteDemographic) {
    return this.httpClient.post(this.SiteAdminUrl + "/SaveSiteDemographicData", JSON.stringify(SiteDemographic), this.httpOptions);
  }

  saveSites(Sites: siteDemographic[]) {
    return this.httpClient.post(this.SiteAdminUrl + "/SaveSites", JSON.stringify(Sites), this.httpOptions);
  }

  getSiteAgreementPopulation(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetSiteAgreementPopulation", { params: { siteId: value }, observe: 'response' });
  }

  getTADLiaison(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetTADLiaisons", { params: { siteCategoryType: value }, observe: 'response' });
  }
  SaveSiteApproval(siteApprovaldata: siteApproval) {
    return this.httpClient.post(this.SiteAdminUrl + "/SaveSiteApproval", JSON.stringify(siteApprovaldata), this.httpOptions);
  }

  checDuplicateSite(SiteDemographic: siteDemographic) {
    return this.httpClient.post(this.SiteAdminUrl + "/CheckDuplicateSite", JSON.stringify(SiteDemographic), this.httpOptions);
  }

  setSiteSelected(data: AgencySiteData) {
    this.siteSelected= new BehaviorSubject<AgencySiteData>(null);
    this.siteSelected.next(data);
  
  }

  getSiteSelected(): Observable<AgencySiteData> {
    return this.siteSelected.asObservable();
  }

  setAgencySelected(data: number) {
    this.agencySelected= new BehaviorSubject<number>(null);
    this.agencySelected.next(data);
   
  }

  getAgencySelected(): Observable<number> {
    return this.agencySelected.asObservable();
  }
  
  

  setSiteCreated(data: siteDemographic[]) {
    this.siteprofilecreated = new BehaviorSubject<siteDemographic[]>(null);
    this.siteprofilecreated.next(data);
  }

  getSitecreated(): Observable<siteDemographic[]> {
    return this.siteprofilecreated.asObservable();
  }


  getUnitDetails(SiteID: number): Observable<any> {
    return this.httpClient.get(this.getUnitsRequestURL + '?SiteId=' + SiteID, this.httpOptions);
  }

  saveUnitDetails(unit: Unit): Observable<any> {
    return this.httpClient.post(this.saveUnitRequestURL, JSON.stringify(unit), this.httpOptions);
  }

  deleteUnit(UnitID: number, UserName: string): Observable<any> {
    let reqData = { "UnitID": UnitID, "UserName": UserName }
    return this.httpClient.post(this.deleteUnitRequestURL, reqData, this.httpOptions);
  }

  savePrimaryServiceContract(siteAgreementPopulation: SiteAgreementPopulation) {
    return this.httpClient.post(this.SiteAdminUrl + "/SavePrimaryServiceContract", JSON.stringify(siteAgreementPopulation), this.httpOptions);
  }

  deletePrimaryServiceContract(SiteConsiteAgreementPopulationtact: SiteAgreementPopulation) {
    return this.httpClient.post(this.SiteAdminUrl + "/DeletePrimaryServiceContract", JSON.stringify(SiteConsiteAgreementPopulationtact), this.httpOptions);
  }

  SaveAgencyinfo(Agencyinfo: AgencyData) {
    return this.httpClient.post(this.SiteAdminUrl + "/SaveAgencySiteDetails", JSON.stringify(Agencyinfo), this.httpOptions);
  }
  checDuplicateAgency(Agencyinfo: AgencyData) {
    return this.httpClient.post(this.SiteAdminUrl + "/CheckDuplicateAgency", JSON.stringify(Agencyinfo), this.httpOptions);
  }

  getUnitHistory(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetUnitHistory", { params: { UnitId: value }, observe: 'response' });
  }

  SaveTransferSite(transfersite: TransferSite) {
    return this.httpClient.post(this.SiteAdminUrl + "/SaveTransferSite", JSON.stringify(transfersite), this.httpOptions);
  }

  getSiteFeatures(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetSiteFeatures", { params: { siteId: value }, observe: 'response' });
  }


  getAgreementPopulationUnitTypes(value: string, value1: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetAgreementPopulationUnitTypes", { params: { agreementPopulationID: value, ContarctAgencyType: value1 }, observe: 'response' });
  }

  getAgreementRentalSubsidies(value: string) {
    return this.httpClient.get(this.SiteAdminUrl + "/GetAgreementRentalSubsidies", { params: { primaryserviceContratcID: value }, observe: 'response' });
  }




}