import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { siteDemographic, AgencyData, AgencySiteData, TransferSite, housingProgram } from '../agency-site-model';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { Router, Event } from '@angular/router';
import { SiteAdminService } from '../site-admin.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';


@Component({
  selector: 'app-transfer-site',
  templateUrl: './transfer-site.component.html',
  styleUrls: ['./transfer-site.component.scss']
})
export class TransferSiteComponent implements OnInit {

  agencyList: AgencyData[];
  transferSiteForm: FormGroup;
  agencyInfo: AgencyData
  agencyTypes: housingProgram[];
  yesNoTypes: RefGroupDetails[];
  siteCategoryTypes: RefGroupDetails[];
  boroughTypes: RefGroupDetails[];
  agencySites: AgencySiteData[] = [];
  site: siteDemographic
  userData: AuthData;
  userDataSub: Subscription;
  userid: number = 10;
  message: string;
  commonServiceSub: Subscription;
  transfersiteinfo: TransferSite;
  showTransfersiteDetail: boolean = false;
  filterAgencyList: Observable<AgencyData[]>;
  myControl = new FormControl();
  sitetransfered: AgencySiteData;
  isVerifyTransferSite: boolean = false;
  filteredAgencyData: Observable<any[]>;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private siteAdminservice: SiteAdminService,
    private userService: UserService,
    private toastr: ToastrService,
    private commonService: CommonService) { }

  ngOnInit() {
    this.transferSiteForm = this.formBuilder.group({
      transferFromAgencyCtrl: ['', [Validators.required]],
      transferFromSiteCtrl: ['', [Validators.required]],
      IsTransferToExistingAgencyCtrl: ['', [Validators.required]],
      transferToExistingAgencyIDCtrl: [''],
      agencyNameCtrl: ['', [Validators.maxLength(100)]],
      agencyTypeCtrl: ['', [Validators.required]],
      agencyaddressCtrl: ['', [Validators.maxLength(200)]],
      agencycityCtrl: ['', [Validators.maxLength(50)]],
      agencystateCtrl: ['', [Validators.maxLength(2)]],
      agencyzipCtrl: ['', [Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      siteNameCtrl: ['', [Validators.maxLength(100)]],
      addressCtrl: ['', [Validators.maxLength(200)]],
      cityCtrl: ['', [Validators.maxLength(50)]],
      stateCtrl: ['', [Validators.maxLength(2)]],
      zipCtrl: ['', [Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      hdnsiteIDCtrl: [''],
      siteCategoryTypeCtrl: ['', [Validators.required]],
      siteBoroughTypeCtrl: ['', [Validators.required]],
      isVerifyTransferSiteCtrl: [false, [Validators.required]],
      firstNameCtrl: ['', [Validators.required, Validators.maxLength(50)]],
      lastNameCtrl: ['', [Validators.required, Validators.maxLength(50)]],
      titleCtrl: ['', [Validators.maxLength(25)]], // , Validators.pattern('^[A-Za-z.]+$')
      emailCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.email]],
      officephoneCtrl: ['', [Validators.required, Validators.maxLength(10)]],
      cellphoneCtrl: [''],
      officephoneExtnCtrl: ['',],
      faxCtrl: [''],
    });
    this.clearTransferSiteInfo();
    this.getUserRolesInfo();
    this.loadRefGroupDetails();
    this.loadagencylist();
    this.loadHousingPrograms()
    this.transferSiteForm.controls.siteCategoryTypeCtrl.disable();
    //this.initFilters();
    /*  this.filterAgencyList = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );*/

  }

  filterAgencyFrom(value: string): AgencyData[] {
    return this.agencyList.filter(option => option.name.toLowerCase().indexOf(value.toLowerCase()) === 0);
  }


  initFilters() {
    this.filterAgencyList = this.transferSiteForm
      .get('transferFromAgencyCtrl')
      .valueChanges.pipe(
        startWith<string | any>(''),
        map(val => (typeof val === 'string' ? val : val.view)),
        map(name => (name ? this.filterAgencyFrom(name) : this.agencyList.slice()))
      );


  }

  getUserRolesInfo() {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {

        this.userid = this.userData.optionUserId;
      }

    }
    );
  }

 
  private _filter(value: string): AgencyData[] {
    const filterValue = value.toLowerCase();
    return this.agencyList.filter(agency => agency.name.toLowerCase().includes(filterValue) || agency.agencyNo.includes(filterValue));
  }
 
  loadRefGroupDetails = () => {
    const yesNoRefGroupId = 7; const siteCategoryRefGroupId = 2; const siteboroughRefGroupId = 6;
    this.commonServiceSub = this.commonService.getRefGroupDetails(yesNoRefGroupId + ',' + siteCategoryRefGroupId + ',' + siteboroughRefGroupId).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        //  this.agencyTypes = data.filter(d => (d.refGroupID === agencyTypeRefGroupId));
        // this.siteCategoryTypes = data.filter(d => (d.refGroupID === siteCategoryRefGroupId && (d.refGroupDetailID===5 || d.refGroupDetailID===7 || d.refGroupDetailID===6 )));
        //   this.siteCategoryTypes = data.filter(d => (d.refGroupID === siteCategoryRefGroupId));
        this.siteCategoryTypes = data.filter(d => (d.refGroupID === siteCategoryRefGroupId && (d.refGroupDetailID === 7)));
        //this.boroughTypes = data.filter(d => (d.refGroupID === siteboroughRefGroupId));
        this.boroughTypes = data.filter(d => (d.refGroupID === siteboroughRefGroupId && d.refGroupDetailID >= 27 && d.refGroupDetailID <= 31));
        this.yesNoTypes = data.filter(d => d.refGroupID === yesNoRefGroupId && (d.refGroupDetailDescription.toUpperCase() === 'YES' || d.refGroupDetailDescription.toUpperCase() === 'NO'));
      },
      error => console.error('Error!', error)
    );
  }

  loadSiteInfo(agencyid: number) {
    this.siteAdminservice.getSites(agencyid.toString(),null)
      .subscribe(
        res => {
          const data = res.body as AgencySiteData[];
          this.agencySites = data.filter(d => (d.siteStatus === 'Active' && d.siteCategoryType===7) );
        },
        error => console.error('Error!', error)
      );
  }

  refershAgencySites(id:number) {
    this.transfersiteinfo.TransferFromAgencyID = id;
    if (this.transfersiteinfo.TransferFromAgencyID != null) {
      this.loadSiteInfo(this.transfersiteinfo.TransferFromAgencyID)
    }
  }

  refershTransferAgency(id:number) {
    this.transfersiteinfo.TransferToExistingAgencyID = id;
  }
  loadagencylist() {
    this.siteAdminservice.getAgencyList(null, null)
      .subscribe(
        res => {
          this.agencyList = res.body as AgencyData[];
          this.filteredAgencyData = this.transferSiteForm.controls.transferFromAgencyCtrl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
            );
            this.filterAgencyList = this.transferSiteForm.controls.transferToExistingAgencyIDCtrl.valueChanges.pipe(
              startWith(''),
              map(value => this._filter(value))
              );
          
        },
        error => console.error('Error!', error)
      );
  }

  clearTransferSiteInfo() {

    this.sitetransfered = {
      siteID: 0,
      agencySite: '',
      siteStatus: '',
      name: '',
      siteType: '',
      locationType: '',
      siteCategoryType: 7,
      siteAddress: ''
    };
    this.agencyInfo = {
      agencyID: 0,
      agencyNo: null,
      name: null,
      agencyType: null,
      agencyAddress: null,
      city: null,
      state: null,
      zip: null,
      userId: null,
      sites: null,
      userName: null,
      contactLastName: null,
      contactFirstName: null,
      contactTitle: null,
      phone: null,
      extension: null,
      alternatePhone: null,
      alternateExtension: null,
      fax: null,
      email: null,
    };

    this.site = {
      siteID: 0,
      siteNo: null,
      agencyID: 0,
      housingProgram: null,
      name: '',
      address: '',
      city: '',
      state: '',
      zip: '',
      locationType: null,
      referralAvailableType: null,
      tcoReadyType: null,
      tcoContractStartDate: null,
      taxCreditType: null,
      maxIncomeForStudio: null,
      maxIncomeForOneBR: null,
      levelOfCareType: null,
      contractType: null,
      contractStartDate: null,
      siteTrackedType: null,
      tadLiasion: null,
      sameAsSiteInterviewAddressType: null,
      interviewAddress: null,
      interviewCity: null,
      interviewState: null,
      interviewZip: null,
      interviewPhone: null,
      siteFeatures: null,
      siteTypeDesc: null,
      agencyName: null,
      agencyNo: null,
      userId: 10,
      siteCategoryType: 7,
      boroughType: null

    }


    this.transfersiteinfo = {
      TransferFromAgencyID: null,
      TransferFromSiteID: null,
      IsTransferToExistingAgency: null,
      TransferToExistingAgencyID: null,
      TransferToAgencyName: null,
      TransferToAgencyAddress: null,
      TransferToAgencyCity: null,
      TransferToAgencyState: null,
      TransferToAgencyZip: null,
      TransferToExistingSiteID: null,
      TransferToAgencySiteName: null,
      TransferToAgencySiteAddress: null,
      TransferToAgencySiteCity: null,
      TransferToAgencySiteState: null,
      TransferToAgencySiteZip: null,
      TransferToAgencySiteBorough: null,
      TransferToAgencySiteCategoryType: null,
      TransferToAgencyContactLastName: null,
      TransferToAgencyContactFirstName: null,
      TransferToAgencyOffceTitle: null,
      TransferToAgencyEmail: null,
      TransferToAgencyOfficePhone: null,
      TransferToAgencyOfficePhoneExtn: null,
      TransferToAgencyFax: null,
      TransferToAgencyType: null,
      userId: null
    }
  }

  ValidateTransferSites = (): boolean => {
    this.transfersiteinfo.userId = this.userid;
    this.transfersiteinfo.TransferFromSiteID = this.site.siteID;
    this.transfersiteinfo.TransferToAgencySiteName = this.site.name;
    this.transfersiteinfo.TransferToAgencySiteAddress = this.site.address;
    this.transfersiteinfo.TransferToAgencySiteState = this.site.state;
    this.transfersiteinfo.TransferToAgencySiteCity = this.site.city;
    this.transfersiteinfo.TransferToAgencySiteZip = this.site.zip;
    this.transfersiteinfo.TransferToAgencySiteBorough = this.site.boroughType;
    this.transfersiteinfo.TransferToAgencySiteCategoryType = this.site.siteCategoryType;



    if (this.transfersiteinfo.IsTransferToExistingAgency != 33) {
      this.transfersiteinfo.TransferToExistingAgencyID = 0;
      this.transfersiteinfo.TransferToAgencyName = this.agencyInfo.name;
      this.transfersiteinfo.TransferToAgencyAddress = this.agencyInfo.agencyAddress;
      this.transfersiteinfo.TransferToAgencyCity = this.agencyInfo.city;
      this.transfersiteinfo.TransferToAgencyState = this.agencyInfo.state;
      this.transfersiteinfo.TransferToAgencyZip = this.agencyInfo.zip;
      this.transfersiteinfo.TransferToAgencyType = this.agencyInfo.agencyType;
      this.transfersiteinfo.TransferToAgencyContactLastName = this.agencyInfo.contactFirstName;
      this.transfersiteinfo.TransferToAgencyContactFirstName = this.agencyInfo.contactLastName;
      this.transfersiteinfo.TransferToAgencyOffceTitle = this.agencyInfo.contactTitle;
      this.transfersiteinfo.TransferToAgencyEmail = this.agencyInfo.email;
      this.transfersiteinfo.TransferToAgencyOfficePhone = this.agencyInfo.phone;
      this.transfersiteinfo.TransferToAgencyOfficePhoneExtn = this.agencyInfo.extension;
      this.transfersiteinfo.TransferToAgencyFax = this.agencyInfo.fax;

    }
    else {
      this.transfersiteinfo.TransferToExistingAgencyID = this.agencyInfo.agencyID;
    }

    if (this.transfersiteinfo.IsTransferToExistingAgency != 33) {
      if (this.agencyInfo.name == null || this.agencyInfo.name.trim() == '') {
        this.message = "please enter  Agency name "
        this.toastr.error(this.message, "Error");
        return false;
      }

      if (this.agencyInfo.agencyAddress == null || this.agencyInfo.agencyAddress.trim() == '') {
        this.message = "please enter Address"
        this.toastr.error(this.message, "Error");
        return false;
      }

      if (this.agencyInfo.city == null || this.agencyInfo.city.trim() == '') {
        this.message = "please enter city"
        this.toastr.error(this.message, "Error");
        return false;
      }

      if (this.agencyInfo.state == null || this.agencyInfo.state.trim() == '') {
        this.message = "please enter state"
        this.toastr.error(this.message, "Error");
        return false;
      }

      if (this.agencyInfo.zip == null || this.agencyInfo.zip.trim() == '') {
        this.message = "please enter Zip "
        this.toastr.error(this.message, "Error");
        return false;
      }
      if (this.agencyInfo.contactFirstName === null || this.agencyInfo.contactFirstName.trim() === '') {
        this.message = "please enter agency contact first name  "
        this.toastr.error(this.message, "Error");
        return false;
      }
      if (this.agencyInfo.contactLastName === null || this.agencyInfo.contactLastName.trim() === '') {
        this.message = "please enter agency contact last name  "
        this.toastr.error(this.message, "Error");
        return false;
      }
      if (this.agencyInfo.email === null || this.agencyInfo.email.trim() === '') {
        this.message = "please enter agency contact email  "
        this.toastr.error(this.message, "Error");
        return false;
      }
      if (this.agencyInfo.phone === null || this.agencyInfo.phone.trim() === '') {
        this.message = "please enter agency contact phone  "
        this.toastr.error(this.message, "Error");
        return false;
      }

    }


    if (this.site.name === null || this.site.name.trim() === '') {
      this.message = "please enter  site name "
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.address === null || this.site.address.trim() === '') {
      this.message = "please enter Address"
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.city === null || this.site.city.trim() === '') {
      this.message = "please enter city"
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.state === null || this.site.state.trim() === '') {
      this.message = "please enter state"
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.zip === null || this.site.zip.trim() === '') {
      this.message = "please enter Zip "
      this.toastr.error(this.message, "Error");
      return false;
    }




    if (this.isVerifyTransferSite === false) {
      this.message = "please check the Note checkbox to procced"
      this.toastr.error(this.message, "Error");
      return false;
    }



    return true;

  }

  ValidateTransferSiteSelection = (): boolean => {

    if (this.transfersiteinfo.TransferFromAgencyID == null || this.transfersiteinfo.TransferFromAgencyID == 0) {
      this.message = "please select  Transfer from Agency  "
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.transfersiteinfo.TransferFromSiteID == null || this.transfersiteinfo.TransferFromSiteID == 0) {
      this.message = "please select  Transfer from site  "
      this.toastr.error(this.message, "Error");
      return false;
    }


    if (this.transfersiteinfo.IsTransferToExistingAgency == null) {
      this.message = "please answer  question -Do you wish to copy the site profile to an existing agency ?  "
      this.toastr.error(this.message, "Error");
      return false;
    }
    else if (this.transfersiteinfo.IsTransferToExistingAgency == 33) {
      if ((this.transfersiteinfo.TransferToExistingAgencyID == null) || (this.transfersiteinfo.TransferToExistingAgencyID == this.transfersiteinfo.TransferFromAgencyID)) {
        this.message = "please Select the transfer existing agency ?  "
        this.toastr.error(this.message, "Error");
        return false;
      }
    }


    return true;

  }




  proceedTransferSiteSelection() {
    if (this.ValidateTransferSiteSelection() === true) {
      if (this.transfersiteinfo.IsTransferToExistingAgency === 33) {
        this.agencyInfo = this.agencyList.find(e => e.agencyID === this.transfersiteinfo.TransferToExistingAgencyID);
        this.disableAgencydetails()
      }
      this.siteAdminservice.getSiteDemogramics(this.transfersiteinfo.TransferFromSiteID.toString())
        .subscribe(
          res1 => {
            this.site = res1.body as siteDemographic;
            this.site.siteCategoryType = 7
            this.showTransfersiteDetail = true;
          },
          error => console.error('Error!', error)
        );

    }
  }
  disableAgencydetails() {
    this.transferSiteForm.controls.agencyNameCtrl.disable();
    this.transferSiteForm.controls.agencyTypeCtrl.disable();
    this.transferSiteForm.controls.agencyaddressCtrl.disable();
    this.transferSiteForm.controls.agencycityCtrl.disable();
    this.transferSiteForm.controls.agencystateCtrl.disable();
    this.transferSiteForm.controls.agencyzipCtrl.disable();
    this.transferSiteForm.controls.firstNameCtrl.disable()
    this.transferSiteForm.controls.lastNameCtrl.disable()
    this.transferSiteForm.controls.titleCtrl.disable()
    this.transferSiteForm.controls.emailCtrl.disable()
    this.transferSiteForm.controls.officephoneCtrl.disable()
    this.transferSiteForm.controls.officephoneExtnCtrl.disable()
    this.transferSiteForm.controls.cellphoneCtrl.disable()
    this.transferSiteForm.controls.emailCtrl.disable()

  }

  enableAgencydetails() {
    this.transferSiteForm.controls.agencyNameCtrl.enable(); 
      this.transferSiteForm.controls.agencyNameCtrl.enable();
      this.transferSiteForm.controls.agencyTypeCtrl.enable();
      this.transferSiteForm.controls.agencyaddressCtrl.enable();
      this.transferSiteForm.controls.agencycityCtrl.enable();
      this.transferSiteForm.controls.agencystateCtrl.enable();
      this.transferSiteForm.controls.agencyzipCtrl.enable();
      this.transferSiteForm.controls.firstNameCtrl.enable()
      this.transferSiteForm.controls.lastNameCtrl.enable()
      this.transferSiteForm.controls.titleCtrl.enable()
      this.transferSiteForm.controls.emailCtrl.enable()
      this.transferSiteForm.controls.officephoneCtrl.enable()
      this.transferSiteForm.controls.officephoneExtnCtrl.enable()
      this.transferSiteForm.controls.cellphoneCtrl.enable()
      this.transferSiteForm.controls.emailCtrl.enable()
  
    
  }
  disableSitedetails() {
    this.transferSiteForm.controls.siteNameCtrl.disable();
    this.transferSiteForm.controls.addressCtrl.disable();
    this.transferSiteForm.controls.cityCtrl.disable();
    this.transferSiteForm.controls.stateCtrl.disable();
    this.transferSiteForm.controls.zipCtrl.disable();
    this.transferSiteForm.controls.siteBoroughTypeCtrl.disable();
    this.transferSiteForm.controls.firstNameCtrl.disable()
   
  }
  enableSitedetails() {
    this.transferSiteForm.controls.siteNameCtrl.enable();
    this.transferSiteForm.controls.addressCtrl.enable();
    this.transferSiteForm.controls.cityCtrl.enable();
    this.transferSiteForm.controls.stateCtrl.enable();
    this.transferSiteForm.controls.zipCtrl.enable();
    this.transferSiteForm.controls.siteBoroughTypeCtrl.enable();
    this.transferSiteForm.controls.firstNameCtrl.enable()
   
  }




  SaveTranserSite() {
    console.log(this.transfersiteinfo);
    console.log(this.agencyInfo);
    if (this.transfersiteinfo.IsTransferToExistingAgency != 33) {
      this.siteAdminservice.checDuplicateAgency(this.agencyInfo)
        .subscribe(
          data => {
            var checkagency = data as boolean
            if (checkagency) {
              this.toastr.error('duplicate site', 'Adding New Agency');
            }
            else {
              this.CheckDuplicateAndSaveSite();
            }
          },
          error => {
            this.toastr.error("checkduplicate", 'Check duplicate');

          }
        );

    }

    else {
      this.CheckDuplicateAndSaveSite();
    }
  }

  CheckDuplicateAndSaveSite() {
    this.Savesite();
    /*this.siteAdminservice.checDuplicateSite(this.site)
    .subscribe(
             data => {
              
               var checksites = data as boolean
               if (checksites)
               {
                 this.toastr.error('duplicate site', 'Adding New site');
                 
                }
                else 
                { 
                  this.Savesite();
                }
             },
             error => {this.toastr.error("checkduplicate", 'Check duplicate');
             
           }
           );*/


  }

  Savesite() {
    this.siteAdminservice.SaveTransferSite(this.transfersiteinfo)
      .subscribe(
        data => {
          // var messagestring = "Transfered site to" +  this.transfersiteinfo.ag + '-' + this.agencyFrom.name
          this.toastr.success("Successfully transfered site", 'Site transfer success');
          var result = data as siteDemographic;
          this.sitetransfered.siteID = result.siteID;
          this.sitetransfered.siteCategoryType = result.siteCategoryType;
          this.siteAdminservice.setSiteSelected(this.sitetransfered);
          this.router.navigate(['./admin/site-profile']);
        },
        error => { this.toastr.error("Save Failed", 'Save Failed'); }
      );

  }





  proceed() {
    if (this.ValidateTransferSites() === true) {

      this.SaveTranserSite();
    }

  }

  //On Consent Check Change
  onIsVerifyTransferSite(event: { checked: Boolean }) {
    if (event.checked) {
      this.isVerifyTransferSite = true;
      this.disableAgencydetails();
      this.disableSitedetails();
    }
    else {
      this.isVerifyTransferSite = false;
      if (this.transfersiteinfo.IsTransferToExistingAgency !== 33)
      {
      this.enableAgencydetails();
      }
      this.enableSitedetails();
    }
  }
  onSubmit = (): void => {
    // this.markFormGroupTouched(this.contactForm);
    //if (this.contactForm.valid) {
    //this.saveDocument();
    // }
  }
  loadHousingPrograms() {
    this.siteAdminservice.getHousingPrograms()
      .subscribe(
        res => {
          this.agencyTypes = res.body as housingProgram[];
        },
        error => console.error('Error!', error)
      );
  }

}