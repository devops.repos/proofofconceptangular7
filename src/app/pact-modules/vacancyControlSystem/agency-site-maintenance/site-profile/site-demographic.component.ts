import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  OnDestroy,
  OnChanges
} from '@angular/core';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { ToastrService } from 'ngx-toastr';
import { SiteAdminService } from '../site-admin.service';
import { RefGroupDetails } from '../../../../models/refGroupDetailsDropDown.model';
import { housingProgram, siteDemographic, UserOptions } from "../agency-site-model"
import { CommonService } from '../../../../services/helper-services/common.service';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';



@Component({
  selector: 'app-site-demographic',
  templateUrl: './site-demographic.component.html',
  styleUrls: ['./site-demographic.component.scss'],
  providers: [DatePipe]
})
export class SiteDemographicComponent implements OnInit, OnDestroy, OnChanges {
  siteDemographicGroup: FormGroup;
  tCOS: RefGroupDetails[];
  taxCredits: RefGroupDetails[];
  levelOfCareTypes: RefGroupDetails[];
  siteFeatures: RefGroupDetails[];
  siteTypes: RefGroupDetails[];
  interviewLocations: RefGroupDetails[];
  housingPrograms: housingProgram[];
  submitted: boolean = false;
  message: string;
  mySubscription: any;
  @Input() siteDemographicData: siteDemographic;
  @Input() options: UserOptions;
  @Input() siteFeatureData: Array<number>;

  @Output() siteDemographicDataChanged = new EventEmitter();
  @Output() siteDemographicValueChanged = new EventEmitter();



  validationMessages = {

    siteTypeRadioCtrl: {
      'required': 'Please select the type of the site.',
      'radioGroupRequired': 'Please select the type of the site.',

    },
    tcoReadyRadioCtrl: {
      'required': 'Please select Is TCO ready .',

    },
    siteContratctedRadioCtrl: {
      'required': 'Please select Is  site contracted .',

    },

    contractStartDateCtrl: {
      'required': 'Please enter the site contracted date .',
      'dateValidator': 'site contract date is invalid'
    },
    tcocontractStartDateCtrl: {
      'required': 'Please enter the TCO contracted date .',
      'dateValidator': 'TCO Contarct date is invalid'
    },
    taxcreditedRadioCtrl: {
      'required': 'Please select Tax credited .',

    },
    levelOfCareTypeCtrl: {
      'required': 'Please select the level of care.',

    },

    maxIncomeForStudioCtrl: {
      'required': 'Please enter the max income for studio',

    },
    maxIncomeForOneBRctrl: {
      'required': 'Please max income for one Br.',

    },


    housingProgramCtrl: {
      'required': 'Please select housing program model.',

    },
    siteFeaturesCtrl: {
      'required': 'Please select site features.',

    },
    interviewLocationctrl: {
      'required': 'Please select interview loaction.',

    },

    interviewAddressCtrl: {
      'required': 'Please enter interview Address',
      'maxlength': 'Max length of Address is 200 characters.'
    },
    interviewCityCtrl: {
      'required': 'Please enter interview city.',
      'pattern': 'Please enter alphabets for city. No other characters allowed.',
      'maxlength': 'Max length of city is 50 characters.'
    },
    interviewStateCtrl: {
      'required': 'Please enter interview state. ',
      'pattern': 'Please enter alphabets for State. No other characters allowed.',
      'maxlength': 'Max length of state is 2 characters.'
    },
    interviewZipCtrl: {
      'required': 'Please enter valid ZIP code.',
      'pattern': 'Please enter valid ZIP code.',
      'maxlength': 'Please enter valid ZIP code.'
    },

    interviewPhoneCtrl: {
      'required': 'Please enter interview phone number.',
      'pattern': 'Please enter 10 digit number for phone.',
      'maxlength': 'Max length of phone is 10 characters.',
      'Mask error': 'Invalid Phone number. Please enter date 10 digit phone number',
    }

  };



  formErrors = {
    siteTypeRadioCtrl: '',
    levelOfCareTypeCtrl: '',
    housingProgramCtrl: '',
    siteFeaturesCtrl: '',
    interviewLocationctrl: '',
    interviewAddressCtrl: '',
    interviewCityCtrl: '',
    interviewStateCtrl: '',
    interviewZipCtrl: '',
    interviewPhoneCtrl: '',
    maxIncomeForStudioCtrl: '',
    maxIncomeForOneBRctrl: '',
    taxcreditedRadioCtrl: '',
    tcocontractStartDateCtrl: '',
    tcoReadyRadioCtrl: '',
    siteContratctedRadioCtrl: '',
    contractStartDateCtrl: ''

  };


  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private siteadminservice: SiteAdminService,
    private commonService: CommonService,
    private datePipe: DatePipe,
    private router: Router,

  ) {
    this.siteDemographicGroup = this.formBuilder.group({
      siteTypeRadioCtrl: ['', [CustomValidators.radioGroupRequired()]],
      //contractStartDateCtrl: ['',Validators.compose([Validators.required, this.dateValidator])],
      contractStartDateCtrl: ['', Validators.compose([Validators.required])],
      tcoReadyRadioCtrl: ['', Validators.required],
      //siteContratctedRadioCtrl:['',Validators.required],
      siteContratctedRadioCtrl: ['', Validators.required],
      tcocontractStartDateCtrl: ['', Validators.compose([Validators.required])],
      //tcocontractStartDateCtrl:['',Validators.compose([Validators.required, this.dateValidator])],
      taxcreditedRadioCtrl: ['', Validators.required],
      levelOfCareTypeCtrl: ['', Validators.required],
      housingProgramCtrl: ['', Validators.required],
      siteFeaturesCtrl: ['', Validators.required],
      interviewAddressCtrl: ['', [Validators.required, Validators.maxLength(200)]],
      interviewStateCtrl: ['', [Validators.required, Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
      interviewCityCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
      interviewZipCtrl: ['', [Validators.required, Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      interviewPhoneCtrl: ['', [ Validators.maxLength(10), Validators.pattern('[0-9]+')]],
      maxIncomeForStudioCtrl: ['', [Validators.maxLength(25), ]],
      maxIncomeForOneBRctrl: ['', [Validators.maxLength(25), ]],
      interviewLocationctrl: ['', Validators.required]
    });
    //this.siteDemographicGroup.get('interviewPhoneCtrl').setValue( this.siteDemographicData.interviewPhone);

  }

  ngOnInit() {

    this.loadRefGroupDetails();
    this.loadHousingPrograms();


    this.siteDemographicGroup.valueChanges.subscribe(data => {
      if (this.siteDemographicGroup.touched || this.siteDemographicGroup.dirty) {
        this.siteDemographicValueChanged.emit(true);
          
        //this.ValidateSiteDemographicData();
      }
    });
    this.siteFeatureData = this.convertStringToArray(this.siteDemographicData.siteFeatures);
    this.enabledisablecontrol()
    // this.siteDemographicGroup.get('interviewPhoneCtrl').setValue( this.siteDemographicData.interviewPhone);
    //console.log(this.siteDemographicData.siteFeatures);
    //console.log(this.siteDemographicGroup.get('interviewPhoneCtrl').value);

  }


  loadRefGroupDetails() {
    var value = "7,25,9,12,26";
    this.commonService.getRefGroupDetails(value)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.tCOS = data.filter(d => { return ((d.refGroupID === 7) && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34)) });
          this.taxCredits = data.filter(d => { return ((d.refGroupID === 7) && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34)) });
          this.interviewLocations = data.filter(d => { return ((d.refGroupID === 7) && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34)) });
          this.siteFeatures = data.filter(d => { return d.refGroupID === 12 });
          this.siteTypes = data.filter(d => { return d.refGroupID === 26 });
          this.levelOfCareTypes = data.filter(d => { return d.refGroupID === 25 });
        },
        error => console.error('Error!', error)
      );

  }

  loadHousingPrograms() {
    this.siteadminservice.getHousingPrograms()
      .subscribe(
        res => {
          this.housingPrograms = res.body as housingProgram[];
        },
        error => console.error('Error!', error)
      );
  }



  onSubmit = (): boolean => {
    this.markFormGroupTouched(this.siteDemographicGroup);
    this.setSiteDemographicData();
    this.ValidateSiteDemographicData();
    this.siteDemographicDataChanged.emit(this.siteDemographicData);
    var errormessage = '';
    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '') {
      this.toastr.error(errormessage, "Validation Error");
      return false;
    }
    else {
      return true;
    }
  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);

      }
    });
  }



  resetformerror() {
    this.formErrors = {
      siteTypeRadioCtrl: '',
      levelOfCareTypeCtrl: '',
      housingProgramCtrl: '',
      siteFeaturesCtrl: '',
      interviewLocationctrl: '',
      interviewAddressCtrl: '',
      interviewCityCtrl: '',
      interviewStateCtrl: '',
      interviewZipCtrl: '',
      interviewPhoneCtrl: '',
      maxIncomeForStudioCtrl: '',
      maxIncomeForOneBRctrl: '',
      taxcreditedRadioCtrl: '',
      tcocontractStartDateCtrl: '',
      tcoReadyRadioCtrl: '',
      siteContratctedRadioCtrl: '',
      contractStartDateCtrl: ''

    };
  }


  setSiteDemographicData = () => {

    if (this.siteDemographicData.locationType == 233) {

      this.siteDemographicData.contractStartDate = null;
      this.siteDemographicData.contractType = null;
      //this.siteDemographicData.tcoContractStartDate = this.datePipe.transform( this.siteDemographicData.tcoContractStartDate, 'MM/dd/yyyy');
      if (this.siteDemographicData.taxCreditType != 33) {
        this.siteDemographicData.maxIncomeForOneBR = null;
        this.siteDemographicData.maxIncomeForStudio = null;
      }

    }
    else {
      this.siteDemographicData.tcoReadyType = null;
      this.siteDemographicData.tcoContractStartDate = null;
      this.siteDemographicData.taxCreditType = null;
      this.siteDemographicData.maxIncomeForOneBR = null;
      this.siteDemographicData.maxIncomeForStudio = null
      //this.siteDemographicData.levelOfCareType= null
      //this.siteDemographicData.contractStartDate = this.datePipe.transform( this.siteDemographicData.contractStartDate, 'MM/dd/yyyy');
    }

    this.siteDemographicData.siteFeatures = this.convertArrayToString(this.siteDemographicGroup.get('siteFeaturesCtrl').value);
    this.siteFeatureData = this.siteDemographicGroup.get('siteFeaturesCtrl').value;
    this.siteDemographicData.userId = this.options.optionUserId;
    if (this.siteDemographicData.sameAsSiteInterviewAddressType == 33) {
      this.siteDemographicData.interviewAddress = null;
      this.siteDemographicData.interviewCity = null;
      this.siteDemographicData.interviewState = null;
      this.siteDemographicData.interviewZip = null;
      this.siteDemographicData.interviewPhone = null;
    }
    else {
      this.siteDemographicData.interviewPhone = this.siteDemographicGroup.get('interviewPhoneCtrl').value;
    }

  }



  convertArrayToString(s: any): string {
    if (s != null && s != '') {
      return s.join(',');

    }
    else {
      return null;
    }
  }

  convertStringToArray(s: string): Array<number> {
    if (s != null) {
      var array = s.split(',').map(Number);
      return array;
    }
    else {
      return null;
    }


  }



  ValidateSiteDemographicData() {
    //this.clearValidators();
    var messages: any;
    var key: any;
    this.resetformerror();

    if (!this.siteDemographicGroup.controls.siteTypeRadioCtrl.valid) {
      key = "siteTypeRadioCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDemographicGroup.controls.siteTypeRadioCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.siteDemographicGroup.controls.levelOfCareTypeCtrl.valid) {
      key = "levelOfCareTypeCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDemographicGroup.controls.levelOfCareTypeCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }

    if (!this.siteDemographicGroup.controls.housingProgramCtrl.valid) {
      key = "housingProgramCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDemographicGroup.controls.housingProgramCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.siteDemographicGroup.controls.siteFeaturesCtrl.valid) {
      key = "siteFeaturesCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDemographicGroup.controls.siteFeaturesCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.siteDemographicGroup.controls.interviewLocationctrl.valid) {
      key = "interviewLocationctrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDemographicGroup.controls.interviewLocationctrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }


    if (this.siteDemographicData.locationType === 233) {
      if (!this.siteDemographicGroup.controls.tcoReadyRadioCtrl.valid) {
        key = "tcoReadyRadioCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.tcoReadyRadioCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }

      if (!this.siteDemographicGroup.controls.tcocontractStartDateCtrl.valid) {
        key = "tcocontractStartDateCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.tcocontractStartDateCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      if (!this.siteDemographicGroup.controls.taxcreditedRadioCtrl.valid) {
        key = "taxcreditedRadioCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.taxcreditedRadioCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      if (this.siteDemographicData.taxCreditType === 33) {
        if (!this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.valid) {
          key = "maxIncomeForStudioCtrl";
          messages = this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + '\n';
            }
          }
        }
        if (!this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.valid) {
          key = "maxIncomeForOneBRctrl";
          messages = this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + '\n';
            }
          }
        }
      }
    }
    if (this.siteDemographicData.locationType === 234) {
      if (!this.siteDemographicGroup.controls.siteContratctedRadioCtrl.valid) {
        key = "siteContratctedRadioCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.siteContratctedRadioCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      if (!this.siteDemographicGroup.controls.contractStartDateCtrl.valid) {
        key = "contractStartDateCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.contractStartDateCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
    }


    if (this.siteDemographicData.sameAsSiteInterviewAddressType === 34) {
      if (!this.siteDemographicGroup.controls.interviewAddressCtrl.valid) {
        key = "interviewAddressCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.interviewAddressCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      if (!this.siteDemographicGroup.controls.interviewCityCtrl.valid) {
        key = "interviewCityCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.interviewCityCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      if (!this.siteDemographicGroup.controls.interviewStateCtrl.valid) {
        key = "interviewStateCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.interviewStateCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      if (!this.siteDemographicGroup.controls.interviewZipCtrl.valid) {
        key = "interviewZipCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.interviewZipCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
      if (!this.siteDemographicGroup.controls.interviewPhoneCtrl.valid) {
        key = "interviewPhoneCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.interviewPhoneCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }
    }
  }


  //Validate Date
  dateValidator(AC: AbstractControl) {
    if (AC && AC.value && !moment(AC.value, 'MM/DD/YYYY', true).isValid()) {
      return { 'dateValidator': true };
    }
    return null;
  }

  ngOnChanges() {
   
    this.siteDemographicGroup.markAsUntouched();
    this.siteDemographicGroup.markAsPristine();

    // this.populateDemographicdata()
    //console.log(this.siteDemographicData); // logs undefined
  }

  onInterviewlocationselected(event) {

    this.siteDemographicGroup.controls.interviewPhoneCtrl.setValue(' ');
    this.siteDemographicData.interviewPhone = ' '

  }

  ngOnDestroy = () => {

  }

  enabledisablecontrol() {
    if (this.options.isDisableForHP) {
      this.siteDemographicGroup.controls.siteTypeRadioCtrl.disable();
      this.siteDemographicGroup.controls.tcoReadyRadioCtrl.disable();
      this.siteDemographicGroup.controls.tcocontractStartDateCtrl.disable();
      this.siteDemographicGroup.controls.taxcreditedRadioCtrl.disable();
      this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.disable();
      this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.disable();
      this.siteDemographicGroup.controls.siteContratctedRadioCtrl.disable();
      this.siteDemographicGroup.controls.contractStartDateCtrl.disable();
      this.siteDemographicGroup.controls.levelOfCareTypeCtrl.disable();
      this.siteDemographicGroup.controls.housingProgramCtrl.disable();
      this.siteDemographicGroup.controls.siteFeaturesCtrl.disable();
      if (this.options.isDisableForInactiveSite) {
        this.siteDemographicGroup.controls.interviewLocationctrl.disable();
        this.siteDemographicGroup.controls.interviewAddressCtrl.disable();
        this.siteDemographicGroup.controls.interviewCityCtrl.disable();
        this.siteDemographicGroup.controls.interviewStateCtrl.disable();
        this.siteDemographicGroup.controls.interviewZipCtrl.disable();
        this.siteDemographicGroup.controls.interviewPhoneCtrl.disable();
      }
      else {
        this.siteDemographicGroup.controls.interviewLocationctrl.enable();
        this.siteDemographicGroup.controls.interviewAddressCtrl.enable();
        this.siteDemographicGroup.controls.interviewCityCtrl.enable();
        this.siteDemographicGroup.controls.interviewStateCtrl.enable();
        this.siteDemographicGroup.controls.interviewZipCtrl.enable();
        this.siteDemographicGroup.controls.interviewPhoneCtrl.enable();
      }

    }

    else {
      if (this.options.isDisableForInactiveSite) {
        this.siteDemographicGroup.controls.siteTypeRadioCtrl.disable();
        this.siteDemographicGroup.controls.tcoReadyRadioCtrl.disable();
        this.siteDemographicGroup.controls.tcocontractStartDateCtrl.disable();
        this.siteDemographicGroup.controls.taxcreditedRadioCtrl.disable();
        this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.disable();
        this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.disable();
        this.siteDemographicGroup.controls.siteContratctedRadioCtrl.disable();
        this.siteDemographicGroup.controls.contractStartDateCtrl.disable();
        this.siteDemographicGroup.controls.levelOfCareTypeCtrl.disable();
        this.siteDemographicGroup.controls.housingProgramCtrl.disable();
        this.siteDemographicGroup.controls.siteFeaturesCtrl.disable();

        this.siteDemographicGroup.controls.interviewLocationctrl.disable();
        this.siteDemographicGroup.controls.interviewAddressCtrl.disable();
        this.siteDemographicGroup.controls.interviewCityCtrl.disable();
        this.siteDemographicGroup.controls.interviewStateCtrl.disable();
        this.siteDemographicGroup.controls.interviewZipCtrl.disable();
        this.siteDemographicGroup.controls.interviewPhoneCtrl.disable();
      }
      else {
        this.siteDemographicGroup.controls.siteTypeRadioCtrl.enable();
        this.siteDemographicGroup.controls.tcoReadyRadioCtrl.enable();
        this.siteDemographicGroup.controls.tcocontractStartDateCtrl.enable();
        this.siteDemographicGroup.controls.taxcreditedRadioCtrl.enable();
        this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.enable();
        this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.enable();
        this.siteDemographicGroup.controls.siteContratctedRadioCtrl.enable();
        this.siteDemographicGroup.controls.contractStartDateCtrl.enable();
        this.siteDemographicGroup.controls.levelOfCareTypeCtrl.enable();
        this.siteDemographicGroup.controls.housingProgramCtrl.enable();
        this.siteDemographicGroup.controls.siteFeaturesCtrl.enable();

        this.siteDemographicGroup.controls.interviewLocationctrl.enable();
        this.siteDemographicGroup.controls.interviewAddressCtrl.enable();
        this.siteDemographicGroup.controls.interviewCityCtrl.enable();
        this.siteDemographicGroup.controls.interviewStateCtrl.enable();
        this.siteDemographicGroup.controls.interviewZipCtrl.enable();
        this.siteDemographicGroup.controls.interviewPhoneCtrl.enable();
      }
    }

  }


}
