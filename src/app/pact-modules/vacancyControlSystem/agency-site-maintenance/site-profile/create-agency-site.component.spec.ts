import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAgencySiteComponent } from './create-agency-site.component';

describe('CreateAgencySiteComponent', () => {
  let component: CreateAgencySiteComponent;
  let fixture: ComponentFixture<CreateAgencySiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAgencySiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAgencySiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
