import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SiteContact } from '../agency-site-model';

@Component({
  selector: 'app-site-more-contacts-dialog',
  templateUrl: './site-more-contacts-dialog.component.html',
  styleUrls: ['./site-more-contacts-dialog.component.scss']
})
export class SiteMoreContactsDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: SiteContact[],

    private dialogRef: MatDialogRef<SiteMoreContactsDialogComponent>
  ) { }

  //Close the dialog on close button
  CloseDialog() {
    this.dialogRef.close(true);
  }

  //On Init
  ngOnInit() { }
}
