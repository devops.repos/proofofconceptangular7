
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { SiteContact, siteDemographic } from '../agency-site-model';


@Component({
  selector: "create-site-action",
  template: `<mat-icon (click)="onDelete()" matTooltip='Delete Site' class="create-site-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .create-site-icon {
        cursor: pointer;
      }
    `
  ],
})
export class CreateSiteActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private siteContactSelected: siteDemographic;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    /*this.siteContactSelected = {
     
    siteID : this.params.data.siteID,
    ,
    };
    this.params.context.componentParent.contactViewParent(this.siteContactSelected);*/
  }

  onDelete() {
    /*this.siteContactSelected = {
      
    };
    this.params.context.componentParent.siteContactDeleteParent(this.siteContactSelected);*/
  }

  refresh(): boolean {
    return false;
  }
}
