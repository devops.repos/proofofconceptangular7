import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteProfileComponent } from './site-profile.component';

describe('SiteProfileComponent', () => {
  let component: SiteProfileComponent;
  let fixture: ComponentFixture<SiteProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
