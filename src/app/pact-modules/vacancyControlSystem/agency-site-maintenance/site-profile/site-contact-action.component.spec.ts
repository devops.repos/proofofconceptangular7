import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteContactActionComponent} from './site-contact-action.component';

describe('SiteContactAction.ComponentComponent', () => {
  let component: SiteContactActionComponent;
  let fixture: ComponentFixture<SiteContactActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteContactActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteContactActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
