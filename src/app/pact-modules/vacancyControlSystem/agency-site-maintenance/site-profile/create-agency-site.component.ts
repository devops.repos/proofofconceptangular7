import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { siteDemographic, AgencyData, housingProgram } from '../agency-site-model';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { GridOptions } from 'ag-grid-community';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { Router, Event } from '@angular/router';

import { SiteAdminService } from '../site-admin.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

import { MatTabChangeEvent } from '@angular/material';
import { isNullOrUndefined } from 'util';
import { Alert } from 'selenium-webdriver';

@Component({
    selector: 'app-create-agency-site',
    templateUrl: './create-agency-site.component.html',
    styleUrls: ['./create-agency-site.component.scss']
})
export class CreateAgencySiteComponent implements OnInit {

    agencySiteCreateForm: FormGroup;
    agencyinfo: AgencyData
    savedsites: siteDemographic[];
    sites: siteDemographic[] = [];
    agencyTypes: housingProgram[];
    yesNoTypes: RefGroupDetails[];
    siteCategoryTypes: RefGroupDetails[];
    boroughTypes: RefGroupDetails[];
    site: siteDemographic
    userData: AuthData;
    userDataSub: Subscription; f
    userid: number = 10;
    tabSiteSelectedIndex: number;
    message: string;
    commonServiceSub: Subscription;
    mainTabSelectedIndex: number = 0;
    isBackRequired: boolean = true;
    isNextRequired: boolean = true;
    isProceedRequired: boolean = true




    constructor(private sidenavService: NavService,
        private router: Router,
        private formBuilder: FormBuilder,
        private siteAdminservice: SiteAdminService,
        private userService: UserService,
        private toastr: ToastrService,
        private commonService: CommonService) {
    }

    ngOnInit() {

        this.agencySiteCreateForm = this.formBuilder.group({
            agencyNameCtrl: ['', [Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9 ]+')]],
            agencyTypeCtrl: ['', [Validators.required]],
            agencyaddressCtrl: ['', [Validators.maxLength(200)]],
            agencycityCtrl: ['', [Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
            agencystateCtrl: ['', [Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
            agencyzipCtrl: ['', [Validators.maxLength(5), Validators.pattern('[0-9]+')]],
            siteNameCtrl: ['', [Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9 ]+')]],
            addressCtrl: ['', [Validators.maxLength(200)]],
            cityCtrl: ['', [Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
            stateCtrl: ['', [Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
            zipCtrl: ['', [Validators.maxLength(5), Validators.pattern('[0-9]+')]],
            siteCategoryTypeCtrl: ['', [Validators.required]],
            siteBoroughTypeCtrl: ['', [Validators.required]],
            firstNameCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z ]+')]],
            lastNameCtrl: ['', [Validators.required, Validators.maxLength(50), , Validators.pattern('[a-zA-Z ]+')]],
            titleCtrl: ['', [Validators.maxLength(25),  Validators.pattern('[a-zA-Z. ]+')]],
            emailCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.email]],
            officephoneCtrl: ['', [Validators.required, Validators.maxLength(10)]],
            cellphoneCtrl: [''],
            officephoneExtnCtrl: ['',],
            faxCtrl: [''],
            siteTypeCtrl: [''],
            isCapsMandateRadioCtrl: [''],
            isAddressSameasAgency: ['']

        });

        this.clearAgencyInfo();
        this.clearsiteInfo();
        this.getUserRolesInfo();
        this.loadRefGroupDetails();
        this.loadHousingPrograms();

    }


    ngOnDestroy = () => {
        this.commonServiceSub.unsubscribe();
        this.userDataSub.unsubscribe();
    }

    getUserRolesInfo() {
        this.userDataSub = this.userService.getUserData().subscribe(res => {
            this.userData = res;
            if (this.userData) {

                this.userid = this.userData.optionUserId;
            }

        }
        );
    }

    loadRefGroupDetails = () => {
        const yesNoRefGroupId = 7; const siteCategoryRefGroupId = 2; const siteboroughRefGroupId = 6;
        this.commonServiceSub = this.commonService.getRefGroupDetails(yesNoRefGroupId + ',' + siteCategoryRefGroupId + ',' + siteboroughRefGroupId).subscribe(
            res => {
                const data = res.body as RefGroupDetails[];
                //this.agencyTypes = data.filter(d => (d.refGroupID === agencyTypeRefGroupId));
                this.siteCategoryTypes = data.filter(d => (d.refGroupID === siteCategoryRefGroupId && (d.refGroupDetailID === 5 || d.refGroupDetailID === 7 || d.refGroupDetailID === 6)));
                this.boroughTypes = data.filter(d => (d.refGroupID === siteboroughRefGroupId && d.refGroupDetailID >= 27 && d.refGroupDetailID <= 31));
                this.yesNoTypes = data.filter(d => d.refGroupID === yesNoRefGroupId && (d.refGroupDetailDescription.toUpperCase() === 'YES' || d.refGroupDetailDescription.toUpperCase() === 'NO'));
            },
            error => console.error('Error!', error)
        );
    }




    onSubmit = (): void => {
        // this.markFormGroupTouched(this.agencySiteCreateForm);
        // if (this.agencySiteCreateForm.valid) {
        //this.saveDocument();
        // }
    }

    private markFormGroupTouched = (formGroup: FormGroup) => {
        (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);

            }
        });
    }

    clearAgencyInfo() {
        this.agencyinfo = {
            agencyID: 0,
            agencyNo: null,
            name: null,
            agencyType: null,
            agencyAddress: null,
            city: null,
            state: null,
            zip: null,
            userId: null,
            sites: null,
            userName: null,
            contactLastName: null,
            contactFirstName: null,
            contactTitle: null,
            phone: null,
            extension: null,
            alternatePhone: null,
            alternateExtension: null,
            fax: null,
            email: null
        }
    }
    addnewSites() {
        if (this.ValidateNewsites()) {
            var findsite = this.sites.find(x => x.name.trim().toUpperCase() === this.site.name.trim().toUpperCase());
            if (findsite == null) {
                const newitem: siteDemographic = {
                    siteID: 0,
                    siteNo: null,
                    agencyID: 0,
                    housingProgram: this.site.housingProgram,
                    name: this.site.name,
                    address: this.site.address,
                    city: this.site.city,
                    state: this.site.state,
                    zip: this.site.zip,
                    locationType: null,
                    referralAvailableType: null,
                    tcoReadyType: null,
                    tcoContractStartDate: null,
                    taxCreditType: null,
                    maxIncomeForStudio: null,
                    maxIncomeForOneBR: null,
                    levelOfCareType: null,
                    contractType: null,
                    contractStartDate: null,
                    siteTrackedType: null,
                    tadLiasion: null,
                    sameAsSiteInterviewAddressType: null,
                    interviewAddress: null,
                    interviewCity: null,
                    interviewState: null,
                    interviewZip: null,
                    interviewPhone: null,
                    siteFeatures: null,
                    siteTypeDesc: null,
                    agencyName: null,
                    agencyNo: null,
                    userId: this.userid,
                    siteCategoryType: this.site.siteCategoryType,
                    boroughType: this.site.boroughType,
                    isCapsMandate: this.site.isCapsMandate

                }

                this.siteAdminservice.checDuplicateSite(newitem)
                    .subscribe(
                        data => {

                            var checksites = data as boolean
                            if (!checksites) {
                                this.sites.push(newitem);
                                this.clearsiteInfo();
                                this.agencySiteCreateForm.markAsUntouched();
                                this.toastr.success("Successfully Added", 'Site Added');
                                this.agencySiteCreateForm.controls['isAddressSameasAgency'].setValue(false);
                            }
                            else {
                                this.toastr.error('duplicate site', 'Adding New site');
                            }
                        },
                        error => { this.toastr.error("checkduplicate", 'Check duplicate'); }
                    );

            }

            else {
                this.toastr.error('duplicate site', 'Adding New site');
            }

        }
    }


    validateAgency = (): boolean => {
        if (this.agencyinfo.name === null || this.agencyinfo.name.trim() === '') {
            this.message = "Please enter  agency name "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.agencyNameCtrl.valid) {
            this.message = "Please enter only alphabet for agency name "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.agencyinfo.agencyType === null || this.agencyinfo.agencyType.trim() === '') {
            this.message = "Please select  agency type "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.agencyinfo.agencyAddress === null || this.agencyinfo.agencyAddress.trim() === '') {
            this.message = "Please enter agency address"
            this.toastr.error(this.message, "Error");
            return false;
        }


        if (this.agencyinfo.city === null || this.agencyinfo.city.trim() === '') {
            this.message = "Please enter  agency city"
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.agencycityCtrl.valid) {
            this.message = "Please enter only alphabet for agency city "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.agencyinfo.state === null || this.agencyinfo.state.trim() === '') {
            this.message = "Please enter agency state"
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.agencystateCtrl.valid) {
            this.message = "Please enter only alphabet for agency state "
            this.toastr.error(this.message, "Error");
            return false;
        }


        if (this.agencyinfo.zip === null || this.agencyinfo.zip.trim() === '') {
            this.message = "please enter agency zip "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.agencyzipCtrl.valid) {
            this.message = "Please enter valid agency ZIP code"
            this.toastr.error(this.message, "Error");
            return false;
        }
        if (this.agencyinfo.contactFirstName === null || this.agencyinfo.contactFirstName.trim() === '') {
            this.message = "please enter agency contact first name  "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.firstNameCtrl.valid) {
            this.message = "Please enter only Alphbet for first name "
            this.toastr.error(this.message, "Error");
            return false;
        }
        if (this.agencyinfo.contactLastName === null || this.agencyinfo.contactLastName.trim() === '') {
            this.message = "please enter agency contact last name  "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.lastNameCtrl.valid) {
            this.message = "Please enter only Alphbet for last name "
            this.toastr.error(this.message, "Error");
            return false;
        }
        if (this.agencyinfo.email === null || this.agencyinfo.email.trim() === '') {
            this.message = "please enter agency contact email  "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.emailCtrl.valid) {
            this.message = "Please enter valid email  "
            this.toastr.error(this.message, "Error");
            return false;
        }
        if (this.agencyinfo.phone === null || this.agencyinfo.phone.trim() === '') {
            this.message = "please enter agency contact phone  "
            this.toastr.error(this.message, "Error");
            return false;
        }

        return true;


    }

    validateSite = (): boolean => {
        if (this.site.name === null || this.site.name.trim() === '') {
            this.message = "Please enter  site name "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.siteNameCtrl.valid) {
            this.message = "Please enter only Alphabet for Site Name "
            this.toastr.error(this.message, "Error");
            return false;
        }


        if (this.site.address === null || this.site.address.trim() === '') {
            this.message = "Please enter  site Address"
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.site.city === null || this.site.city.trim() === '') {
            this.message = "Please enter  site city"
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.cityCtrl.valid) {
            this.message = "Please enter only Alphabet for city "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.site.state === null || this.site.state.trim() === '') {
            this.message = "Please enter site state"
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.stateCtrl.valid) {
            this.message = "Please enter only Alphabet for state "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.site.zip === null || this.site.zip.trim() === '') {
            this.message = "Please enter site zip "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencySiteCreateForm.controls.zipCtrl.valid) {
            this.message = "Please enter valid  site ZIP code "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.site.boroughType === null || this.site.boroughType === 0) {
            this.message = "Please select the  borough"
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.site.housingProgram === null || this.site.housingProgram === '') {
            this.message = "Please select the  Site type"
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.site.siteCategoryType === null || this.site.siteCategoryType === 0) {
            this.message = "Please select the  Site category"
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.site.siteCategoryType === 5 || this.site.siteCategoryType === 6) {
            if (this.site.isCapsMandate === null) {
                this.message = "Please select the  Caps Mandate"
                this.toastr.error(this.message, "Error");
                return false;
            }
        }



        return true;


    }


    ValidateNewsites = (): boolean => {

        if (this.validateAgency() && this.validateSite()) {
            return true
        }
        else
            return false;
    }



    clearsiteInfo() {
        this.site = {
            siteID: 0,
            siteNo: null,
            agencyID: 0,
            housingProgram: null,
            name: '',
            address: '',
            city: '',
            state: '',
            zip: '',
            locationType: null,
            referralAvailableType: null,
            tcoReadyType: null,
            tcoContractStartDate: null,
            taxCreditType: null,
            maxIncomeForStudio: null,
            maxIncomeForOneBR: null,
            levelOfCareType: null,
            contractType: null,
            contractStartDate: null,
            siteTrackedType: null,
            tadLiasion: null,
            sameAsSiteInterviewAddressType: null,
            interviewAddress: null,
            interviewCity: null,
            interviewState: null,
            interviewZip: null,
            interviewPhone: null,
            siteFeatures: null,
            siteTypeDesc: null,
            agencyName: null,
            agencyNo: null,
            userId: 10,
            siteCategoryType: null,
            boroughType: null
        }

        this.enablesiteinformation();
      //  this.agencySiteCreateForm.controls['isAddressSameasAgency'].setValue(false);
    }


    SaveAgencySites() {
        this.agencyinfo.sites = this.sites;
        this.agencyinfo.userId = this.userid;
        this.siteAdminservice.checDuplicateAgency(this.agencyinfo)
            .subscribe(
                data => {

                    var checkagency = data as boolean
                    if (!checkagency) {
                        this.agencyinfo.sites = this.sites;
                        this.agencyinfo.userId = this.userid;
                        this.Savesites();
                        //this.agencySiteCreateForm.markAsUntouched();

                        // this.toastr.success("Successfully Added", 'Site Added');
                    }
                    else {
                        this.toastr.error('duplicate Agency', 'Adding New Agency');
                    }
                },
                error => { this.toastr.error("checkduplicate", 'Check duplicate'); }
            );

    }

    Savesites() {


        this.siteAdminservice.SaveAgencyinfo(this.agencyinfo)
            .subscribe(
                data => {
                    this.toastr.success("Successfully created sites", 'Site Creation');
                    var result = data as AgencyData;
                    this.savedsites = result.sites;
                    this.siteAdminservice.setSiteCreated(this.savedsites);
                    this.router.navigate(['./admin/createsitedetail','new']);
                },
                error => { this.toastr.error("Save Failed", 'Save Failed'); }
            );

    }

    proceed() {
        if (this.sites.length > 0) {
            this.SaveAgencySites();
        }
        else {
            this.toastr.warning("There is no new site added, cannot proceed. Please click ‘Add new’ before proceeding.", 'Validation Error.');
        }
    }

    tabSiteChanged(tabsiteChangeEvent: MatTabChangeEvent): void {
        // /* Saving Data Between Tab Navigation */
        // if (this.dvGroup.touched || this.dvGroup.dirty) {
        //   this.onSave(false);
        // }
        this.tabSiteSelectedIndex = tabsiteChangeEvent.index;
        if (this.sites.length > 0) {
            const s = this.sites[this.tabSiteSelectedIndex]
            this.site = s;
        }
    }
    setSites(i: number) {
        if (this.sites.length > 0) {
            const s = this.sites[i];
            this.site = s;
            this.disablesiteinformation();
        }
    }

    enablesiteinformation() {
        this.agencySiteCreateForm.controls.siteCategoryTypeCtrl.enable();
        this.agencySiteCreateForm.controls.siteBoroughTypeCtrl.enable();
        this.agencySiteCreateForm.controls.addressCtrl.enable();
        this.agencySiteCreateForm.controls.siteNameCtrl.enable();
        this.agencySiteCreateForm.controls.cityCtrl.enable();
        this.agencySiteCreateForm.controls.stateCtrl.enable();
        this.agencySiteCreateForm.controls.zipCtrl.enable();
        this.agencySiteCreateForm.controls.siteTypeCtrl.enable();
        this.agencySiteCreateForm.controls.isCapsMandateRadioCtrl.enable();
        this.agencySiteCreateForm.controls.isAddressSameasAgency.enable();
    }


    disablesiteinformation() {
        this.agencySiteCreateForm.controls.siteCategoryTypeCtrl.disable();
        this.agencySiteCreateForm.controls.siteBoroughTypeCtrl.disable();
        this.agencySiteCreateForm.controls.addressCtrl.disable();
        this.agencySiteCreateForm.controls.siteNameCtrl.disable();
        this.agencySiteCreateForm.controls.cityCtrl.disable();
        this.agencySiteCreateForm.controls.stateCtrl.disable();
        this.agencySiteCreateForm.controls.zipCtrl.disable();
        this.agencySiteCreateForm.controls.siteTypeCtrl.disable();
        this.agencySiteCreateForm.controls.isCapsMandateRadioCtrl.disable();
        this.agencySiteCreateForm.controls.isAddressSameasAgency.disable();
    }



    removeSites() {

        if (this.sites.length > 0) {

            this.sites.splice(this.tabSiteSelectedIndex, 1);
        }
    }

    mainTabChanged(mainTabChangedEvent: MatTabChangeEvent): void {

        this.mainTabSelectedIndex = mainTabChangedEvent.index;

    }

    loadHousingPrograms() {
        this.siteAdminservice.getHousingPrograms()
            .subscribe(
                res => {
                    this.agencyTypes = res.body as housingProgram[];
                },
                error => console.error('Error!', error)
            );
    }

    isCapsMandateSelection(issiteActive: boolean) {


    }

    SubmitPage() {
        this.proceed();
    }

    // Previous button click
    exitPage() {

        this.router.navigate(['/admin/agency-site-maintenance']);

    }

    nextPage() {
        if (this.mainTabSelectedIndex < 1) {
            if (this.validateAgency()) {
                this.mainTabSelectedIndex = this.mainTabSelectedIndex + 1;
            }

        }
    }

    // Previous button click
    previousPage() {

        if (this.mainTabSelectedIndex != 0) {
            this.mainTabSelectedIndex = this.mainTabSelectedIndex - 1;
        }
    }
    onisAddressSameasAgency(event: { checked: Boolean }) {
        if (event.checked) {
            this.site.address = this.agencyinfo.agencyAddress;
            this.site.city = this.agencyinfo.city;
            this.site.state = this.agencyinfo.state;
            this.site.zip = this.agencyinfo.zip;
            this.site.housingProgram = this.agencyinfo.agencyType;

        }
        else {

        }
    }
}
