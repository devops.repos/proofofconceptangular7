import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { SiteAgreementPopulation } from "../agency-site-model"
import { SiteAdminService } from '../site-admin.service';

@Component({
  selector: 'app-site-agreement-detail',
  templateUrl: './site-agreement-detail.component.html',
  styleUrls: ['./site-agreement-detail.component.scss']
})
export class SiteAgreementDetailComponent implements OnInit, OnDestroy {
  @ViewChild('agGrid') agGrid: AgGridAngular;


  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  pagination;
  rowSelection;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;
  public gridOptions: GridOptions;
  rowData: SiteAgreementPopulation[] = [];
  overlayLoadingTemplate: string = '';
  overlayNoRowsTemplate: string = '';

  @Input() contactsiteId: number;
  constructor(
    private siteadminservice: SiteAdminService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;
    //this.gridOptions.api.hideOverlay();

    this.columnDefs = [
      {
        headerName: 'Site ID',
        filter: 'agTextColumnFilter',
        field: "agreementPopulatioId",
        hide: true
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'primaryServiceContractType',
        width: 300,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Rental Subsidies',
        field: 'rentalSubsidies',
        width: 300,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'No of Units',
        field: 'totalUnits',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Status',
        field: 'status',
        width: 150,
        filter: 'agTextColumnFilter'
      }
      
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      //filter: false
      filter: true,
      floatingFilter: true
    };
    this.rowSelection = 'single';

    this.pagination = true;
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the primary service contracts are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No  primary service contarcts are available</span>';
  }

  ngOnInit() {

    if (this.contactsiteId != null) {
      this.geSiteAgreementPopulation();
    }

  }

  geSiteAgreementPopulation = () => {
    this.siteadminservice.getSiteAgreementPopulation(this.contactsiteId.toString())
      .subscribe(
        res => {
          if (res.body) {
            this.rowData = res.body as SiteAgreementPopulation[];
          }
        },
        error => console.error('Error in retrieving the Primary Service Contract Data By Site ID...!', error)
      );

  }



  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setDomLayout('autoHeight');

    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function (column) {


      //  params.api.sizeColumnsToFit();
      //params.api.setDomLayout('autoHeight');
      //  this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the documents are loading.</span>';
      //this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Documents Available</span>';

    });
  }




  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onRowSelected(event) {
    if (event.node.selected) {
      //alert('row ' + event.node.data.siteID + ' selected = ' + event.node.selected);
      // alert(event.node);        
    }
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'SiteAgreemnetProfile-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

  }
  ngOnDestroy = () => {

  }


}
