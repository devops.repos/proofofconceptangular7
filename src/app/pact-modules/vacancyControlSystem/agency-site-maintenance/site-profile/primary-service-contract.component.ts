
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  OnDestroy
} from '@angular/core';

import 'ag-grid-enterprise';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { SiteAgreementPopulation, PrimaryServiceContractAgreementPopulation, UserOptions } from '../agency-site-model';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { GridOptions } from 'ag-grid-community';
import { SiteAdminService } from '../site-admin.service';
import { PrimaryServiceContractActionComponent } from './primary-service-contract-action.component'
import { CommonService } from '../../../../services/helper-services/common.service';

@Component({
  selector: 'app-primary-service-contract',
  templateUrl: './primary-service-contract.component.html',
  styleUrls: ['./primary-service-contract.component.scss']
})
export class PrimaryServiceContractComponent implements OnInit, OnDestroy {

  primaryServiceContractForm: FormGroup;
  siteAgrrementPopulationData: SiteAgreementPopulation[] = [];
  primaryServiceAgreementPopulationList: PrimaryServiceContractAgreementPopulation[] = [];
  siteAgrrementPopulationModel: SiteAgreementPopulation = {
    siteID: null,
    primaryServiceContractType: null,
    rentalSubsidies: null,
    totalUnits: 0,
    siteAgreementPopulationID: null,
    agreementPopulationID: null,
    isActive: true,
    userId: null
   
  };
  currentAgreementPopulationID :number = 0;
  message: string;
  @Input() primaryServicecontractsiteId: number;
  @Input() SiteAgreementId: number;
   @Input() options: UserOptions;
  @Output() unitdetailopenbtnclick = new EventEmitter();
  @Output() primaryserviceContarctDatachanged = new EventEmitter();

  validationMessages = {

  };


  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  context;
  public gridOptions: GridOptions;
  overlayLoadingTemplate: string = '';
  overlayNoRowsTemplate: string = '';
  actionLabel: string = 'Add'


  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private siteadminservice: SiteAdminService,
    private commonService: CommonService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;
    //this.gridOptions.api.hideOverlay();

    this.columnDefs = [
      {
        headerName: 'Site Aggreement Population ID',
        filter: 'agTextColumnFilter',
        field: "siteAgreementPopulationID",
        hide: true
      },
      {
        headerName: 'Site ID',
        filter: 'agTextColumnFilter',
        field: "siteID",
        hide: true
      },
      {
        headerName: 'Primary Service Contract',
        field: 'primaryServiceContractType',
        width: 250,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'No of Units',
        field: 'totalUnits',
        width: 250,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Status',
        field: 'status',
        width: 200,
        filter: 'agTextColumnFilter'
      },

      {
        headerName: 'Actions',
        field: 'action',
        width: 100,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'actionRenderer',
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.rowSelection = 'single';
    this.pagination = true;
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: PrimaryServiceContractActionComponent
    };
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the primary service contracts are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Primary service contract are available</span>';
  }



  ngOnInit() {

    this.primaryServiceContractForm = this.fb.group({
      agrrementPopulationCtrl: ['', [Validators.required]],
      totalUnitsCtrl: ['', [Validators.required]],
      hdnSiteAgreementPopulationIDCtrl: ['']
    });
    this.loadPrimaryServiceContract();
    this.getPrimartSerivceContractList();
    
    /* this.enabledisablecontrol();
     if (this.contactsiteId != null){
       this.getContactsList();
     }
     if (this.options.isIH == true){
       this.contactForm.get('sysadminNameCtrl').enable();
     }
     else {
       this.contactForm.get('sysadminNameCtrl').disable();
     }
     this.contactForm.valueChanges.subscribe(data => {
     });*/




  }




  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setDomLayout('autoHeight');

    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function (column) {

      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);
      }
    });
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }



  loadPrimaryServiceContract() {
    this.commonService.getAllPrimaryServiceAgreementPopulation()
      .subscribe(
        res => {
          this.primaryServiceAgreementPopulationList = res.body as PrimaryServiceContractAgreementPopulation[];
        },
        error => console.error('Error!', error)
      );
  }


  onSubmit = (): void => {
    this.markFormGroupTouched(this.primaryServiceContractForm);
    if (this.primaryServiceContractForm.valid) {
      //this.saveDocument();
    }
  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }


  sitePrimartSerivceContractDeleteParent = (cell: SiteAgreementPopulation) => {
    const title = 'Confirm Delete';
    const primaryMessage = '';
    const secondaryMessage = "Are you sure to delete " + cell.primaryServiceContractType + "?";
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => this.deleteSitePrimaryServiceContract(cell),
        (negativeResponse) => console.log(),
      );

  }

  getPrimartSerivceContractList = () => {
   // alert (this.SiteAgreementId);

    this.siteadminservice.getSiteAgreementPopulation(this.primaryServicecontractsiteId.toString())
      .subscribe(
        res => {
          if (res.body) {
            this.siteAgrrementPopulationData = res.body as SiteAgreementPopulation[];
            if (this.SiteAgreementId !== 0 )
            {
              this.primaryServiceContractForm.get("hdnSiteAgreementPopulationIDCtrl").setValue(this.SiteAgreementId);
                      const findPrimaryservicecontract1 = this.siteAgrrementPopulationData.find(x => ((x.siteAgreementPopulationID === this.SiteAgreementId)
            && (x.siteID === this.primaryServicecontractsiteId)));
                if (findPrimaryservicecontract1)
                {
                  this.actionLabel ="Save";
                  this.currentAgreementPopulationID = findPrimaryservicecontract1.agreementPopulationID;
                  this.primaryServiceContractForm.get("agrrementPopulationCtrl").setValue(findPrimaryservicecontract1.agreementPopulationID);
                  this.primaryServiceContractForm.get("totalUnitsCtrl").setValue(findPrimaryservicecontract1.totalUnits);
                  //this.primaryServiceContractForm.get("agrrementPopulationCtrl").disable();
                }
          
            }
          }
        },
        error => console.error('Error in retrieving the primary service contract Data By  ID...!', error)
      );
  }
  //add code for the request.



  deleteSitePrimaryServiceContract = (dataSelected: SiteAgreementPopulation) => {
    dataSelected.userId = this.options.optionUserId;

    this.siteadminservice.deletePrimaryServiceContract(dataSelected).subscribe(
      res => {
        this.message = 'Primary Service Contract deleted successfully.';
        this.toastr.success(this.message, 'Delete');
        // console.error('Success!', res);
        this.getPrimartSerivceContractList();
         this.primaryserviceContarctDatachanged.emit();
      },
      error => {
        this.message = 'primary service contract did not delete.';
        this.toastr.error(this.message, 'Delete');
        // console.error('Error!', error);
      }
    );

  }

  saveSitePrimaryServiceContratc() {
    this.setSitePrimaryServiceContractValidator();

    if (this.primaryServiceContractForm.invalid) {
      this.toastr.error('Validation failed', 'Save');
      return;
    }

    if (this.ValidatePrimaryServiceContract()) {
      //this.siteContactModel.SiteContactID= this.contactForm.get('hdnContactIDCtrl').value;
      console.log(this.siteAgrrementPopulationModel);
      if (this.primaryServicecontractsiteId != null) {
        this.siteAgrrementPopulationModel.siteID = this.primaryServicecontractsiteId;
      }

      this.siteAgrrementPopulationModel.isActive = true;
      if (this.actionLabel === "Add")
      this.siteAgrrementPopulationModel.siteAgreementPopulationID = 0;
      this.siteAgrrementPopulationModel.userId = this.options.optionUserId;

      /*if (this.siteAgrrementPopulationModel.siteAgreementPopulationID != null && this.siteAgrrementPopulationModel.siteAgreementPopulationID != 0) {
        this.siteAgrrementPopulationModel.userId = this.options.optionUserId;

      }
      else {
        this.siteAgrrementPopulationModel.siteAgreementPopulationID = 0;*
        

      }*/

      this.siteadminservice.savePrimaryServiceContract(this.siteAgrrementPopulationModel)
        .subscribe(
          data => {
            this.message = 'Primary Service Contract saved successfully.';
            this.toastr.success(this.message, 'Save');
            this.SiteAgreementId=0; 
           this.getPrimartSerivceContractList();
            this.clearValidatorsFoSitePrimaryServiceContract();
            this.clearPrimaryServiceContractFields();
            this.primaryserviceContarctDatachanged.emit();
          },
          error => {
            this.message = 'Cant ave Primary Service Contract';
            this.toastr.error(this.message, 'Save');
          }
        );
    }
    // add code for Request Save

  }


  setSitePrimaryServiceContractValidator = () => {

    this.primaryServiceContractForm.controls.agrrementPopulationCtrl.setValidators(Validators.compose([Validators.required]));
    this.primaryServiceContractForm.controls.totalUnitsCtrl.setValidators(Validators.compose([Validators.required]));
    this.primaryServiceContractForm.controls.agrrementPopulationCtrl.updateValueAndValidity();
    this.primaryServiceContractForm.controls.totalUnitsCtrl.updateValueAndValidity();

  }

  clearValidatorsFoSitePrimaryServiceContract = () => {
    this.primaryServiceContractForm.controls.agrrementPopulationCtrl.clearValidators();
    this.primaryServiceContractForm.controls.totalUnitsCtrl.clearValidators();

  }

  clearPrimaryServiceContractFields() {
   /* this.siteAgrrementPopulationModel = {
      siteID: null,
      primaryServiceContractType: null,
      rentalSubsidies: null,
      totalUnits: 0,
      siteAgreementPopulationID: null,
      agreementPopulationID: null,
      isActive: true,
      userId: null
  
    };*/
    this.primaryServiceContractForm.get('hdnSiteAgreementPopulationIDCtrl').setValue(0);
    this.primaryServiceContractForm.get('totalUnitsCtrl').setValue(0);
    this.primaryServiceContractForm.get('agrrementPopulationCtrl').setValue('');
    this.primaryServiceContractForm.get('agrrementPopulationCtrl').enable();
    this.actionLabel= "Add";
  }


  primaryServiceContractViewParent = (cell: SiteAgreementPopulation) => {

    if (cell.siteAgreementPopulationID != null) {
      this.siteAgrrementPopulationModel.siteAgreementPopulationID = cell.siteAgreementPopulationID;
    }
    if (cell.siteID != null) {
      this.siteAgrrementPopulationModel.siteID = cell.siteID;
    }
    if (cell.agreementPopulationID != null) {
      this.siteAgrrementPopulationModel.agreementPopulationID = cell.agreementPopulationID;
    }
    if (cell.totalUnits != null) {
      this.siteAgrrementPopulationModel.totalUnits = cell.totalUnits;
    }
    this.actionLabel="Save"
    //this.primaryServiceContractForm.get('agrrementPopulationCtrl').disable();

    this.unitdetailopenbtnclick.emit(cell);


  }

  ValidatePrimaryServiceContract = (): boolean => {

    if (this.siteAgrrementPopulationModel.totalUnits === null || this.siteAgrrementPopulationModel.totalUnits <= 0 || this.siteAgrrementPopulationModel.totalUnits >= 999) {
      this.message = "Total units must be greater than 0 and less than 1000"
      this.toastr.error(this.message, "Error");
      return false;
    }


    if (this.siteAgrrementPopulationModel.agreementPopulationID === null) {
      this.message = "Please select the agreement population "
      this.toastr.error(this.message, "Error");
      return false;
    }
    if (this.actionLabel === "Add") {
    const findPrimaryservicecontract = this.siteAgrrementPopulationData.find(x => ((x.agreementPopulationID === this.siteAgrrementPopulationModel.agreementPopulationID)
      && (x.siteID === this.primaryServicecontractsiteId)));
    /*console.log (findPrimaryservicecontract);
    alert(this.siteAgrrementPopulationModel.agreementPopulationID);
    alert(this.primaryServicecontractsiteId);
    alert(findPrimaryservicecontract);*/
    if (findPrimaryservicecontract) {
      this.message = "Duplicate Primary Service Agreement population "
      this.toastr.error(this.message, "Error");
      return false;
    }
  }
  else (this.actionLabel === "Save")
  {
    if (this.currentAgreementPopulationID !==0 && this.currentAgreementPopulationID !== this.siteAgrrementPopulationModel.agreementPopulationID)
    {
      const findPrimaryservicecontract = this.siteAgrrementPopulationData.find(x => ((x.agreementPopulationID === this.siteAgrrementPopulationModel.agreementPopulationID)
      && (x.siteID === this.primaryServicecontractsiteId)));
    if (findPrimaryservicecontract) {
      this.message = "Duplicate Primary Service Agreement population "
      this.toastr.error(this.message, "Error");
      return false;
    }
    }
  }



    return true;

  }



  ngOnDestroy = () => {

  }

  enabledisablecontrol() {
    if (this.options.isDisableForInactiveSite) {

      this.primaryServiceContractForm.disable();
    }
    else {
      this.primaryServiceContractForm.enable();
    }
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'PrimayServiceContract-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

    console.log('PrimayServiceContract-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }
  ngOnChanges() {

    if (this.primaryServicecontractsiteId != null) {
      this.getPrimartSerivceContractList();
    }

  }
}



