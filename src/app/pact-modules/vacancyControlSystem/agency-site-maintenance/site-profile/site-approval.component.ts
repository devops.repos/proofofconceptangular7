import {
  Component, OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  OnDestroy,
  OnChanges
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SiteAdminService } from '../site-admin.service';
import { RefGroupDetails } from '../../../../models/refGroupDetailsDropDown.model';
import { siteApproval, UserOptions, tADLiaison } from "../agency-site-model"
import { CommonService } from '../../../../services/helper-services/common.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';


@Component({
  selector: 'app-site-approval',
  templateUrl: './site-approval.component.html',
  styleUrls: ['./site-approval.component.scss'],
  providers: [DatePipe]
})
export class SiteApprovalComponent implements OnInit, OnDestroy, OnChanges {

  approvalForm: FormGroup;
  isYesNoList: RefGroupDetails[];
  tADLiaisons: tADLiaison[];

  submitted: boolean = false;
  message: string;
  @Input() siteApprovalData: siteApproval;
  @Input() options: UserOptions;
  @Input() isTCOVisible: boolean = true;
  @Output() siteApprovalChanged = new EventEmitter();
  @Output() siteApprovalValueChanged = new EventEmitter();


  validationMessages = {

    issiteTrackedRadioCtrl: {
      'required': 'Please select is site tracked?.',
      'radioGroupRequired': 'Please select is site tracked?.'

    },
    tadLiasonCtrl: {
      'required': 'Please select TAD Liaison.',
    },


    referralAvailableTypeRadioCtrl: {
      'required': 'Please Select Available for referrals .',

    },
    expectedAvailableDateCtrl: {
      'required': 'Please enter site expected Available date .',
    },
    isActiveRadioCtrl: {
      'required': 'Please selct is Active.',

    },
    additionalcommentsCtrl: {
      'required': 'Please enter comments.',

    },



  };



  formErrors = {
    issiteTrackedRadioCtrl: '',
    tadLiasonCtrl: '',
    referralAvailableTypeRadioCtrl: '',
    expectedAvailableDateCtrl: '',
    isActiveRadioCtrl: '',
    additionalcommentsCtrl: ''

  };


  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private siteadminservice: SiteAdminService,
    private commonService: CommonService,
    private datePipe: DatePipe
  ) {
    this.approvalForm = this.formBuilder.group({
      issiteTrackedRadioCtrl: ['', [CustomValidators.radioGroupRequired()]],
      tadLiasonCtrl: ['', Validators.compose([Validators.required])],
      referralAvailableTypeRadioCtrl: ['', Validators.required],
      expectedAvailableDateCtrl: ['', Validators.compose([Validators.required])],
      isActiveRadioCtrl: ['', Validators.required],
      additionalcommentsCtrl: ['', Validators.required]
    });
    // 

  }

  ngOnInit() {
    this.loadRefGroupDetails();
    this.LoadTADLiaisons();
    this.enabledisablecontrol();
    this.approvalForm.valueChanges.subscribe(data => {
      if (this.approvalForm.touched || this.approvalForm.dirty) {
        this.siteApprovalValueChanged.emit(true);
        //this.ValidateSiteDemographicData();
      }
    });

  }
 

  ngOnChanges() {
    this.approvalForm.markAsUntouched();
    this.approvalForm.markAsPristine();
    
  }
  ngOnDestroy = () => {

  }

  loadRefGroupDetails() {
    var value = "7,25,9,12,26";
    this.commonService.getRefGroupDetails(value)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.isYesNoList = data.filter(d => { return ((d.refGroupID === 7) && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34)) });
        },
        error => console.error('Error!', error)
      );

  }

  LoadTADLiaisons = () => {

    this.siteadminservice.getTADLiaison('8')
      .subscribe(
        res => {
          if (res.body) {
            this.tADLiaisons = res.body as tADLiaison[];
          }
        },
        error => console.error('Error in retrieving the TAD Laisions ...!', error)
      );
  }

  enabledisablecontrol() {
    if (this.options.isDisableForHP) {
      this.approvalForm.controls.issiteTrackedRadioCtrl.disable();
      this.approvalForm.controls.tadLiasonCtrl.disable();
      this.approvalForm.controls.referralAvailableTypeRadioCtrl.disable();
      this.approvalForm.controls.expectedAvailableDateCtrl.disable();
      //  this.approvalForm.controls.isActiveRadioCtrl.disable();
      this.approvalForm.controls.additionalcommentsCtrl.disable();
    }

    else {
      if (this.options.isDisableForInactiveSite) {
        this.approvalForm.controls.issiteTrackedRadioCtrl.disable();
        this.approvalForm.controls.tadLiasonCtrl.disable();
        this.approvalForm.controls.referralAvailableTypeRadioCtrl.disable();
        this.approvalForm.controls.expectedAvailableDateCtrl.disable();
        this.approvalForm.controls.isActiveRadioCtrl.enable();
        this.approvalForm.controls.additionalcommentsCtrl.disable();
      }
      else {
        this.approvalForm.controls.issiteTrackedRadioCtrl.enable();
        this.approvalForm.controls.tadLiasonCtrl.enable();
        this.approvalForm.controls.referralAvailableTypeRadioCtrl.enable();
        this.approvalForm.controls.expectedAvailableDateCtrl.enable();
        this.approvalForm.controls.isActiveRadioCtrl.enable();
        this.approvalForm.controls.additionalcommentsCtrl.enable();
      }
    }
    if (this.options.isdisableActiveRadiobutton) {
      this.approvalForm.controls.isActiveRadioCtrl.disable();
    }
    else {
      this.approvalForm.controls.isActiveRadioCtrl.enable();
    }
  }


  onSubmit = (): boolean => {
    this.markFormGroupTouched(this.approvalForm);
    this.ValidateSiteApprovalData();
    this.siteApprovalChanged.emit(this.siteApprovalData);
    var errormessage = '';
    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '') {
      this.toastr.error(errormessage, "Validation Error");
      return false;
    }
    else {
      return true;
    }
  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }



  resetformerror() {
    this.formErrors = {
      issiteTrackedRadioCtrl: '',
      tadLiasonCtrl: '',
      referralAvailableTypeRadioCtrl: '',
      expectedAvailableDateCtrl: '',
      isActiveRadioCtrl: '',
      additionalcommentsCtrl: ''
    };
  }


  isActiveSelection(issiteActive: boolean) {


  }

  ValidateSiteApprovalData() {
    //this.clearValidators();
    var messages: any;
    var key: any;
    this.resetformerror();
    //alert(this.approvalForm.get('isActiveRadioCtrl').value);


    if (!this.approvalForm.controls.issiteTrackedRadioCtrl.valid) {
      key = "issiteTrackedRadioCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.approvalForm.controls.issiteTrackedRadioCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (this.siteApprovalData.siteTrackedType === 33) {
      if (this.approvalForm.controls.issiteTrackedRadioCtrl.valid) {
        if (!this.approvalForm.controls.tadLiasonCtrl.valid) {
          key = "tadLiasonCtrl";
          messages = this.validationMessages[key];
          for (const errorKey in this.approvalForm.controls.tadLiasonCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + '\n';
            }
          }
        }
      }
    }
    if (!this.approvalForm.controls.referralAvailableTypeRadioCtrl.valid) {
      key = "referralAvailableTypeRadioCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.approvalForm.controls.referralAvailableTypeRadioCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }

    if (this.approvalForm.controls.referralAvailableTypeRadioCtrl.valid) {
      if (!this.approvalForm.controls.expectedAvailableDateCtrl.valid) {
        key = "expectedAvailableDateCtrl";
        messages = this.validationMessages[key];
        for (const errorKey in this.approvalForm.controls.expectedAvailableDateCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
        }
      }

    }
    if (!this.approvalForm.controls.isActiveRadioCtrl.valid) {
      key = "isActiveRadioCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.approvalForm.controls.isActiveRadioCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.approvalForm.controls.additionalcommentsCtrl.valid) {
      key = "additionalcommentsCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.approvalForm.controls.additionalcommentsCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
  }



}