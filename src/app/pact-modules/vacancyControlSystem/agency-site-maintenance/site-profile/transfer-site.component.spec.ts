import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferSiteComponent } from './transfer-site.component';

describe('TransferSiteComponent', () => {
  let component: TransferSiteComponent;
  let fixture: ComponentFixture<TransferSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
