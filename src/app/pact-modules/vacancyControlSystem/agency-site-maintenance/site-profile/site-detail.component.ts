import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  OnDestroy,
  OnChanges
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SiteAdminService } from '../site-admin.service';
import { RefGroupDetails } from '../../../../models/refGroupDetailsDropDown.model';
import { housingProgram, siteDemographic, UserOptions } from "../agency-site-model"
import { CommonService } from '../../../../services/helper-services/common.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription,Observable } from 'rxjs';
@Component({
  selector: 'app-site-detail',
  templateUrl: './site-detail.component.html',
  styleUrls: ['./site-detail.component.scss']
})
export class SiteDetailComponent implements OnInit {

  siteDetailForm: FormGroup;
  message: string;
  siteCategoryTypes: RefGroupDetails[];
  boroughTypes: RefGroupDetails[];
  agencyTypes: housingProgram[];
  tagencyid: number = 0;
  @Input() raSiteDemographicData: siteDemographic;
  @Output() raSiteDemographicDataChanged = new EventEmitter();
  @Output() siteDetailValueChanged = new EventEmitter();
  userData: AuthData;
  userDataSub: Subscription;
  RA=false;
  isPE=false;
  isHP=false;
  isIH=false;



  validationMessages = {

    siteNameCtrl: {
      'required': 'Please enter site Address',
      'maxlength': 'Max length of site name is 100 characters.'

    },
    addressCtrl: {
      'required': 'Please enter site Address',
      'maxlength': 'Max length of Address is 200 characters.'

    },
    cityCtrl: {
      'required': 'Please enter city.',
      'pattern': 'Please enter alphabets for city. No other characters allowed.',
      'maxlength': 'Max length of city is 50 characters.'

    },

    stateCtrl: {
      'required': 'Please enter state. ',
      'pattern': 'Please enter valid state.',
      'maxlength': 'Max length of state is 2 characters.'
    },
    zipCtrl: {
      'required': 'Please enter valid ZIP code.',
      'pattern': 'Please enter valid ZIP code.',
      'maxlength': 'Please enter valid ZIP code'
    },
    siteBoroughTypeCtrl: {
      'required': 'Please select borough.',

    },
    siteTypeCtrl: {
      'required': 'Please select site type.',

    },

    isCapsMandateRadioCtrl: {
      'required': 'Please select the CAPS mandate.',
      'radioGroupRequired': 'Please select the CAPS mandate.',

    },

    isActiveRadioCtrl: {
      'required': 'Please select Active yes or no',
      'radioGroupRequired': 'Please select  Active yes or no.',
    }
  };

  formErrors = {
    siteNameCtrl: '',
    addressCtrl: '',
    cityCtrl: '',
    stateCtrl: '',
    zipCtrl: '',
    siteBoroughTypeCtrl: '',
    siteTypeCtrl: '',
    isCapsMandateRadioCtrl: ''
    , isActiveRadioCtrl: ''
  };




  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private siteAdminservice: SiteAdminService,
    private toastr: ToastrService,
    private commonService: CommonService,
    private userService: UserService
    ) {
    this.siteDetailForm = this.formBuilder.group({
      siteNameCtrl: ['', [Validators.required, Validators.maxLength(100)]],
      addressCtrl: ['', [Validators.required, Validators.maxLength(200)]],
      cityCtrl: ['', [Validators.required, Validators.maxLength(50)]],
      stateCtrl: ['', [Validators.required, Validators.maxLength(2)]],
      zipCtrl: ['', [Validators.required, Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      siteBoroughTypeCtrl: ['', [Validators.required]],
      siteTypeCtrl: ['', [Validators.required]],
      isCapsMandateRadioCtrl: ['', [Validators.required]],
      isActiveRadioCtrl: ['', [Validators.required]]

    });

  }



  ngOnInit() {
    this.loadRefGroupDetails();
    this.loadHousingPrograms();
    this.siteDetailForm.valueChanges.subscribe(data => {
      if (this.siteDetailForm.touched || this.siteDetailForm.dirty) {
        this.siteDetailValueChanged.emit(true);
        //this.ValidateSiteDemographicData();
      }
    });
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        if (this.userData.siteCategoryType.length > 0) {
          this.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
          this.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
          this.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
          this.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
        }
        if (this.RA===true || this.isPE === true || this.isHP === true)
        {
          this.siteDetailForm.disable();
        }
      }
    });

  }

  ngOnChanges() {
   
    this.siteDetailForm.markAsUntouched();
    this.siteDetailForm.markAsPristine();

  }
  onSubmit = (): boolean => {
    this.markFormGroupTouched(this.siteDetailForm);
    this.setRASiteDemographicData();
    this.ValidateRASiteDemographicData();
    this.raSiteDemographicDataChanged.emit(this.raSiteDemographicData);
    var errormessage = '';
    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '') {
      this.toastr.error(errormessage, "Validation Error");
      return false;
    }
    else {
      return true;
    }
  }

  setRASiteDemographicData = () => {


  }

  ValidateRASiteDemographicData() {
    //this.clearValidators();
    var messages: any;
    var key: any;
    this.resetformerror();

    if (!this.siteDetailForm.controls.siteNameCtrl.valid) {
      key = "siteNameCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDetailForm.controls.siteNameCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.siteDetailForm.controls.addressCtrl.valid) {
      key = "addressCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDetailForm.controls.addressCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }

    if (!this.siteDetailForm.controls.cityCtrl.valid) {
      key = "cityCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDetailForm.controls.cityCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.siteDetailForm.controls.stateCtrl.valid) {
      key = "stateCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDetailForm.controls.stateCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.siteDetailForm.controls.zipCtrl.valid) {
      key = "zipCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDetailForm.controls.zipCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.siteDetailForm.controls.siteTypeCtrl.valid) {
      key = "siteTypeCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDetailForm.controls.siteTypeCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }

    if (!this.siteDetailForm.controls.isCapsMandateRadioCtrl.valid) {
      //alert(this.raSiteDemographicData.isCapsMandate);
      key = "isCapsMandateRadioCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDetailForm.controls.isCapsMandateRadioCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }
    if (!this.siteDetailForm.controls.isActiveRadioCtrl.valid) {
      key = "isActiveRadioCtrl";
      messages = this.validationMessages[key];
      for (const errorKey in this.siteDetailForm.controls.isActiveRadioCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
      }
    }

  }




  resetformerror() {
    this.formErrors = {
      siteNameCtrl: '',
      addressCtrl: '',
      cityCtrl: '',
      stateCtrl: '',
      zipCtrl: '',
      siteBoroughTypeCtrl: '',
      siteTypeCtrl: '',
      isCapsMandateRadioCtrl: '',
      isActiveRadioCtrl: ''
    };
  }





  private markFormGroupTouched = (formGroup: FormGroup) => {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  loadHousingPrograms() {
    this.siteAdminservice.getHousingPrograms()
      .subscribe(
        res => {
          this.agencyTypes = res.body as housingProgram[];
        },
        error => console.error('Error!', error)
      );
  }

  isCapsMandateSelection(issiteActive: boolean) {


  }
  loadRefGroupDetails = () => {
    const yesNoRefGroupId = 7; const siteCategoryRefGroupId = 2; const siteboroughRefGroupId = 6;
    this.commonService.getRefGroupDetails(yesNoRefGroupId + ',' + siteCategoryRefGroupId + ',' + siteboroughRefGroupId).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.siteCategoryTypes = data.filter(d => (d.refGroupID === siteCategoryRefGroupId && (d.refGroupDetailID === 5 || d.refGroupDetailID === 7 || d.refGroupDetailID === 6)));
        this.boroughTypes = data.filter(d => (d.refGroupID === siteboroughRefGroupId));

      },
      error => console.error('Error!', error)
    );
  }

  isActiveSelection(issiteActive: boolean) {


  }
  ngOnDestroy = () => {
    if(this.userDataSub){
     this.userDataSub.unsubscribe();
    }
  }

}

