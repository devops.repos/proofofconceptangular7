import { Component, OnInit, ViewChild } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, AbstractControl, FormControl, FormArray } from '@angular/forms';
import { CommonService } from '../../../../services/helper-services/common.service';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { SiteAdminService } from '../site-admin.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { AgencySiteData, AgencyData, SiteContact, siteDemographic, UserOptions, siteApproval, Unit, RentalSubsidy, UnitFeature, siteCreatedStatus } from "../agency-site-model"
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { SiteDemographicComponent } from './site-demographic.component';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { SiteDetailComponent } from './site-detail.component';
import { SiteMoreContactsDialogComponent } from './site-more-contacts-dialog.component';
import { MatDialog } from '@angular/material';
import { SiteApprovalComponent } from './site-approval.component';




@Component({
  selector: 'app-create-site-detail',
  templateUrl: './create-site-detail.component.html',
  styleUrls: ['./create-site-detail.component.scss']
})
export class CreateSiteDetailComponent implements OnInit {
  sitecreated: siteDemographic[] = [];


  siteDemographicdata: siteDemographic;
  siteStatus: siteDemographic;
  siteApprovaldata: siteApproval;
  agencyInfo: AgencyData;
  siteId: number;
  agencyId: number;
  agencyList: AgencyData[];
  siteContacts: SiteContact[];
  siteFeaturedata: Array<number>;
  allcontacts: string;
  siteagreementpopulationID: number = 0;
  primaryserviceContractType: string = '';
  unitsDataObj: Unit[];
  totalunitsbyAgereement: number = 0;
  SelectedSiteCategorytype: number = 7;
  activatedRouteSub: Subscription;

  useroptions: UserOptions = {
    RA: false,
    isPE: false,
    isHP: false,
    isIH: false,
    isDisableForHP: false,
    isDisableForInactiveSite: false,
    isdisableActiveRadiobutton: true,
    isActive: false,
    optionUserId: 0,
    isNewPage:false
  };

  tabSelectedIndex: number = 0;
  tabRASelectedIndex: number = 0;

  tabSiteSelectedIndex: number = 0;
  userData: AuthData;
  userDataSub: Subscription;
  isSiteTabVisible: boolean = true;
  unitTabVisible: boolean = false;
  submitted: boolean = false;
  isSaveRequired: boolean = true;
  siteDemographicDataTabStatus = 2;
  siteContactTabStatus = 2;
  siteDraftStatus = 2;
  siteApprovalTabStatus = 2
  sitePrimaryServiceTabStatus = 2;
  siteUnitDetailTabStatus = 2;
  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;
  primaryContact: string = '';
  hasMorecontacts: boolean = false;
  sitecreatedstatus: siteCreatedStatus[];



  @ViewChild(SiteDemographicComponent) sitedemographicComponent: SiteDemographicComponent;
  @ViewChild(SiteDetailComponent) siteDetailComponent: SiteDetailComponent;
  @ViewChild(SiteApprovalComponent) siteApprovalComponent: SiteApprovalComponent;




  constructor(
    private sidenavService: NavService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private siteadminservice: SiteAdminService,
    private commonService: CommonService,
    private userService: UserService,
    private confirmDialogService: ConfirmDialogService,
    private toastr: ToastrService,
    public dialog: MatDialog

  ) {

    this.siteDemographicdata = {} as siteDemographic;
    this.agencyInfo = {} as AgencyData;
    this.siteApprovaldata = {} as siteApproval;


  }

  ngOnInit() {
    this.populateSiteInforamtion();

  }

  getUserRolesInfo() {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        if (this.userData.siteCategoryType.length > 0) {
          this.useroptions.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
          this.useroptions.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
          this.useroptions.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
          this.useroptions.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
          this.useroptions.optionUserId = this.userData.optionUserId;
        }

        this.useroptions.isActive = true;
        if (this.useroptions.isIH == true) {
          this.useroptions.isDisableForInactiveSite = false;
          this.useroptions.isDisableForHP = false;
          this.useroptions.isdisableActiveRadiobutton = false;
        }
        else {
          this.useroptions.isDisableForInactiveSite = false;
          this.useroptions.isDisableForHP = true;
          this.useroptions.isdisableActiveRadiobutton = true;
        }
        this.useroptions.isdisableActiveRadiobutton = false;

      }
    }
    );
  }

  loadSiteContacts() {
    if (this.siteId) {
      this.siteadminservice.getSiteContacts(this.siteId.toString())
        .subscribe(
          res => {
            this.siteContacts = res.body as SiteContact[];
            if (this.siteContacts && this.siteContacts.length > 0) {
              this.primaryContact = this.siteContacts[0].lastName + ',' + this.siteContacts[0].firstName;
            }
            if (this.siteContacts && this.siteContacts.length > 1) {
              this.hasMorecontacts = true;
            }
            //this.getallcontacts();
            console.log(this.siteContacts)
          },
          error => console.error('Error!', error)
        );

    }
  }

  loadSiteDemographics() {
    this.siteadminservice.getSiteDemogramics(this.siteId.toString())
      .subscribe(
        res1 => {
          this.siteDemographicdata = res1.body as siteDemographic;
          this.siteFeaturedata = this.convertStringToArray(this.siteDemographicdata.siteFeatures)
          this.agencyId = this.siteDemographicdata.agencyID;
          //this.sitedemographicComponent.populateDemographicdata();
          this.siteadminservice.getAgencyList(this.agencyId.toString(), null)
            .subscribe(
              res2 => {
                this.agencyList = res2.body as AgencyData[];
                this.agencyInfo = this.agencyList.find(e => e.agencyID === this.agencyId);
              },
              error => console.error('Error!', error)
            );
        },
        error => console.error('Error!', error)
      );
  }

  populateSiteInforamtion() {
    this.siteadminservice.getSitecreated().subscribe(res => {
      this.sitecreated = res;
      if (this.sitecreated) {
        this.siteId = this.sitecreated[0].siteID;

        this.SelectedSiteCategorytype = this.sitecreated[0].siteCategoryType;
        if (this.sitecreated[0].name === null) {
          this.isSiteTabVisible = false;
        }
        let sstatus: siteCreatedStatus[] = [];

        this.sitecreated.forEach(element => {
          sstatus.push({
            "siteId": element.siteID,
            "siteCompleteStaus": 0
          });
        });
        this.sitecreatedstatus = sstatus;
        this.getUserRolesInfo();
        this.loadSiteDemographics();
        this.loadSiteContacts();
        this.loadSiteApproval();
        //this.GetAllUnitsBySite();

        if (this.isSiteTabVisible == false) {
          this.tabSelectedIndex = 2;

        }
        this.loadSiteStatus();
        this.getpPagePara();
      }
    });

  }





  convertStringToArray(s: string): Array<number> {
    if (s != null) {
      var array = s.split(',').map(Number);
      return array;
    }
    else {
      return null;
    }

  }

  setTabStatusColor() {
    if (this.siteStatus != null) {
      if (this.siteStatus.isSiteDemographicTabComplete != null) {
        this.siteDemographicDataTabStatus = this.siteStatus.isSiteDemographicTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isSiteContactTabComplete != null) {
        this.siteContactTabStatus = this.siteStatus.isSiteContactTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isPrimaryServiceContractTabComplete != null) {
        this.sitePrimaryServiceTabStatus = this.siteStatus.isPrimaryServiceContractTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isDraft != null) {
        this.siteDraftStatus = this.siteStatus.isDraft == true ? 0 : 1;
      }
      if (this.siteStatus.isSiteApprovalTabComplete != null) {
        this.siteApprovalTabStatus = this.siteStatus.isSiteApprovalTabComplete == true ? 1 : 0;
      }

      if (this.siteStatus.isUnitDetailTabComplete != null) {
        this.siteUnitDetailTabStatus = this.siteStatus.isUnitDetailTabComplete == true ? 1 : 0;
      }
  /*    if (this.siteStatus.siteCategoryType == 7) {
        if (this.sitecreated.length > 0 && this.sitecreatedstatus.length > 0) {
          // alert(this.sitecreated[this.tabSelectedIndex].siteID);
          if (this.sitecreated[this.tabSelectedIndex].siteID == this.siteStatus.siteID) {
            if (this.siteDemographicDataTabStatus == 1 && this.siteContactTabStatus == 1 && this.sitePrimaryServiceTabStatus == 1)
              this.sitecreatedstatus[this.tabSelectedIndex].siteCompleteStaus = 1;
          }
        }


      }
      else {
        if (this.sitecreated.length > 0 && this.sitecreatedstatus.length > 0) {
          if (this.sitecreated[this.tabSelectedIndex].siteID == this.siteStatus.siteID) {
            if (this.siteDemographicDataTabStatus == 1 && this.siteContactTabStatus == 1)
              this.sitecreatedstatus[this.tabSelectedIndex].siteCompleteStaus = 1;
          }
        }

      }*/
    }

  }


  loadSiteStatus() {
    this.siteadminservice.getSiteDemogramics(this.siteId.toString())
      .subscribe(
        res1 => {
          this.siteStatus = res1.body as siteDemographic;
          this.setTabStatusColor();
        },
        error => console.error('Error!', error)
      );
  }





  getmodifiedData(modifiedData) {
    this.siteDemographicdata = modifiedData;
  }

  OnSave(event: Event) {
    if (this.tabSelectedIndex == 0) {
      this.saveDemographicAndApprovalData();
      //this.loadSiteApproval();
    }
  }

  saveDemographicAndApprovalData() {
    var sitedemographicValidation = this.sitedemographicComponent.onSubmit();
    var siteApprovalValidation = this.siteApprovalComponent.onSubmit();
    if (sitedemographicValidation == true && siteApprovalValidation == true) {
      this.siteadminservice.saveSiteDemographic(this.siteDemographicdata)
        .subscribe(
          data => {
            this.siteadminservice.SaveSiteApproval(this.siteApprovaldata)
              .subscribe(
                data1 => {
                  this.toastr.success("Site demographic is saved Sucessfully", 'Save Success');
                  this.getUserRolesInfo();
                  this.loadSiteDemographics();
                  this.loadSiteApproval();
                  this.tabSelectedIndex = 1;
                },
                error => { this.toastr.error("Save Failed", 'Save Failed'); }
              );
          },
          error => { this.toastr.error("Save Failed", 'Save Failed'); }
        );
    }
  }

  OnRASave(event: Event) {
    /*if (this.tabSelectedIndex  == 0 ) {
      this.saveSiteDeialData();
   
      }*/

  }

  saveSiteDeialData() {
    var sitedemographicValidation = this.siteDetailComponent.onSubmit();
    if (sitedemographicValidation == true) {
      this.siteadminservice.saveSiteDemographic(this.siteDemographicdata)
        .subscribe(
          data => {
            this.toastr.success("Site demographic is saved successfully", 'Save Success');
            //    this.siteApprovalComponent.populateSiteApprovalData();
            this.loadSiteDemographics();
          },
          error => { this.toastr.error("Save Failed", 'Save Failed'); }
        );
    }
  }


  /* nextPage() {
     if (this.tabSelectedIndex < 3) {
       this.OnSave(null);
       this.tabSelectedIndex = this.tabSelectedIndex + 1;
     } else if ((this.tabSiteSelectedIndex === (this.sitecreated.length - 1)) && this.tabSelectedIndex === 3) {
       this.loadSiteStatus();
       //alert(this.siteDemographicDataTabStatus.toString() + this.siteContactTabStatus.toString() +  this.sitePrimaryServiceTabStatus.toString())
       if (this.siteDemographicDataTabStatus === 1 && this.siteContactTabStatus === 1 && this.sitePrimaryServiceTabStatus === 1 && this.siteUnitDetailTabStatus === 1) {
         // this.toastr.success("Successfully added all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')     
         // this.tabSiteSelectedIndex = this.tabSiteSelectedIndex+1;
         this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
       }
       else {
         this.tabSelectedIndex = 3;
         this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
 
       }
 
     }
     else {
       this.loadSiteStatus();
       if (this.siteDemographicDataTabStatus === 1 && this.siteContactTabStatus === 1 && this.sitePrimaryServiceTabStatus === 1 && this.siteUnitDetailTabStatus === 1) {
         this.toastr.success("Successfully added all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
         this.tabSiteSelectedIndex = this.tabSiteSelectedIndex + 1;
         this.loadSiteStatus();
       }
       else {
         this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
 
       }
 
     }
 
 
   }*/

  nextPage() {
    if (this.tabSelectedIndex < 3) {
      this.OnSave(null);
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  previousPage() {
    if (this.tabSelectedIndex !== 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }

  submitPage() {
    this.siteadminservice.getSiteDemogramics(this.siteId.toString())
      .subscribe(
        res1 => {
          this.siteStatus = res1.body as siteDemographic;
          this.siteStatus.isDraft = false;
          this.siteStatus.isActive = true;
          if (this.siteStatus.isSiteContactTabComplete === true && this.siteStatus.isPrimaryServiceContractTabComplete === true && this.siteStatus.isSiteDemographicTabComplete === true) {
            this.siteadminservice.saveSiteDemographic(this.siteStatus)
              .subscribe(
                data => {
                  this.toastr.success(this.sitecreated[this.tabSiteSelectedIndex].name + " Site  is sucessfully created", 'Save Success');
                  this.sitecreatedstatus[this.tabSiteSelectedIndex].siteCompleteStaus = 1;
                  this.loadSiteStatus();
                  
                },
                error => { this.toastr.error("Save Failed", 'Save Failed'); }
              );
          }
          else {
            this.toastr.success("Please add all information to this site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'info');
          }
        },
        error => { this.toastr.error("Save Failed", 'Save Failed'); }
      );
  }



exitPage()
{
  if (this.useroptions.isNewPage === true)
  {
  let iscomplete: boolean = true;
  this.sitecreatedstatus.forEach(scr => {
      if (scr.siteCompleteStaus === 0)
      iscomplete = false;
    });
    
  if (iscomplete === true)
    this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
  else {
    this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
  }
  }
  else
  {
    this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
  }
}

submitRAPage()
{
    this.siteadminservice.getSiteDemogramics(this.siteId.toString())
      .subscribe(
        res1 => {
          this.siteStatus = res1.body as siteDemographic;
          this.siteStatus.isDraft = false;
          this.siteStatus.isActive = true;
          if (this.siteStatus.isSiteContactTabComplete === true &&  this.siteStatus.isSiteDemographicTabComplete === true) {
            this.siteadminservice.saveSiteDemographic(this.siteStatus)
              .subscribe(
                data => {
                  this.toastr.success(this.sitecreated[this.tabSiteSelectedIndex].name + " Site is sucessfully created", 'Save Success');
                  this.sitecreatedstatus[this.tabSiteSelectedIndex].siteCompleteStaus = 1;
                  this.loadSiteStatus();
                },
                error => { this.toastr.error("Save Failed", 'Save Failed'); }
              );
          }
          else {
            this.toastr.success("Please add all information to the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'info');
          }
        },
        error => { this.toastr.error("Save Failed", 'Save Failed'); }
      );
  }


  exitRAPage()
  {
    let iscomplete: boolean = true;
    this.sitecreatedstatus.forEach(scr => {
        if (scr.siteCompleteStaus === 0)
        iscomplete = false;
      });
      
    if (iscomplete === true)
      this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
    else {
      this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
    }
   }
  

// Previous button click
/*  previousPage() {

    if (this.tabSelectedIndex !== 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    } else if (this.tabSelectedIndex === 0 && this.tabSiteSelectedIndex === 0) {

      this.loadSiteStatus();
      if (this.siteDemographicDataTabStatus === 1 && this.siteContactTabStatus === 1 && this.sitePrimaryServiceTabStatus === 1 && this.siteUnitDetailTabStatus === 1) {
        // this.toastr.success("Successfully added all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')     
        // this.tabSiteSelectedIndex = this.tabSiteSelectedIndex+1;
        this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
      }
      else {
        this.tabSelectedIndex = 0;
        this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')

      }
    }
    else {
      this.loadSiteStatus();
      if (this.siteDemographicDataTabStatus === 1 && this.siteContactTabStatus === 1 && this.sitePrimaryServiceTabStatus === 1 && this.siteUnitDetailTabStatus === 1) {
        this.toastr.success("Successfully added all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
        this.tabSiteSelectedIndex = this.tabSiteSelectedIndex - 1;
        this.tabSelectedIndex = 0;
        this.loadSiteStatus();
      }
      else {
        this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')

      }

    }
  }*/

tabChanged(tabChangeEvent: MatTabChangeEvent): void {
  // /* Saving Data Between Tab Navigation */
  // if (this.dvGroup.touched || this.dvGroup.dirty) {
  //   this.onSave(false);
  // }
  this.tabSelectedIndex = tabChangeEvent.index;
  this.loadSiteStatus();

}
tabRAChanged(tabChangeEvent: MatTabChangeEvent): void {
  // /* Saving Data Between Tab Navigation */
  // if (this.dvGroup.touched || this.dvGroup.dirty) {
  //   this.onSave(false);
  // }
  this.tabRASelectedIndex = tabChangeEvent.index;
  this.loadSiteStatus();

}

nextRAPage() {
  /*if (this.tabRASelectedIndex < 1) {
    this.tabRASelectedIndex = this.tabRASelectedIndex + 1;
  } else */
  // alert(this.tabRASelectedIndex.toString() + "," + this.tabSiteSelectedIndex.toString())
  if (this.tabRASelectedIndex === 0 && (this.tabSiteSelectedIndex === (this.sitecreated.length - 1))) {
    this.loadSiteStatus();
    if (this.siteDemographicDataTabStatus === 1 && this.siteContactTabStatus === 1) {
      this.toastr.success("Successfully added all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
      this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
    }
    else {
      this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
      this.tabSelectedIndex = 0;
    }

  }
  else {
    this.loadSiteStatus();
    if (this.siteDemographicDataTabStatus === 1 && this.siteContactTabStatus === 1) {
      this.toastr.success("Successfully added all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
      this.tabSiteSelectedIndex = this.tabSiteSelectedIndex + 1;
      this.tabSelectedIndex = 0;
      this.loadSiteStatus();
    }
    else {
      this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')

    }
    /* this.tabSiteSelectedIndex = this.tabSiteSelectedIndex+1;
    this.loadSiteStatus();*/
  }
}

// Previous button click
previousRAPage() {

  /*  if (this.tabRASelectedIndex !== 0) {
      this.tabRASelectedIndex = this.tabRASelectedIndex - 1;
    } else */
  if (this.tabRASelectedIndex === 0 && this.tabSiteSelectedIndex === 0) {
    this.loadSiteStatus();
    if (this.siteDemographicDataTabStatus === 1 && this.siteContactTabStatus === 1) {
      // this.toastr.success("Successfully added all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')     
      this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
    }
    else {
      this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')

    }

  }
  else {
    this.loadSiteStatus();
    if (this.siteDemographicDataTabStatus === 1 && this.siteContactTabStatus === 1) {
      this.toastr.success("Successfully added all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')
      this.tabSiteSelectedIndex = this.tabSiteSelectedIndex - 1;
      this.loadSiteStatus();
    }
    else {
      this.toastr.success("Please add all information for the site " + this.sitecreated[this.tabSiteSelectedIndex].name, 'Site Creation ')

    }

    this.tabSiteSelectedIndex = this.tabSiteSelectedIndex - 1;
    this.loadSiteStatus();

  }

}



tabSiteChanged(tabsiteChangeEvent: MatTabChangeEvent): void {

  this.tabSiteSelectedIndex = tabsiteChangeEvent.index;
  if(this.sitecreated.length > 0) {

  this.siteId = this.sitecreated[tabsiteChangeEvent.index].siteID;
  //alert(this.siteId)
  this.SelectedSiteCategorytype = this.sitecreated[tabsiteChangeEvent.index].siteCategoryType;
  this.getUserRolesInfo();
  this.loadSiteDemographics();
  this.loadSiteApproval();
  this.loadSiteContacts();
  this.loadSiteStatus();
  this.tabSelectedIndex = 0;
  this.unitTabVisible = false;
}
  }



getallcontacts() {
  var keys = ['firstName', 'lastName'];
  this.allcontacts = this.obsKeysToString(this.siteContacts, keys, ',');
}

obsKeysToString(o, k, sep) {
  return k.map(key => o[key]).filter(v => v).join(sep);
}


showInfo(s: number) {
  this.siteId = s;
  this.getUserRolesInfo();
  this.loadSiteDemographics();
  this.loadSiteContacts();
  this.loadSiteApproval();
}

unitbackbtnClick(t: boolean) {
  this.unitTabVisible = t;
  //alert(this.unitTabVisible);
  this.tabSelectedIndex = 2;
  this.loadSiteStatus();
}


unitDetailOpenClick(param) {
  /*alert (param.siteAgreementPopulationID);*/
  this.siteagreementpopulationID = param.siteAgreementPopulationID;
  this.primaryserviceContractType = param.primaryServiceContractType
  this.totalunitsbyAgereement = param.totalUnits
  this.GetAllUnitsBySite();
  this.tabSelectedIndex = 2;

}



GetAllUnitsBySite() {
  this.siteadminservice.getUnitDetails(this.siteId)
    .subscribe(
      res1 => {
        this.FormatUnitsDataObject(res1);
      },
      error => console.error('Error!', error)
    );
}

FormatUnitsDataObject(res) {
  let unitList = res as Unit[];
  let newUnitList: Unit[] = [];
  newUnitList = unitList.map(unit => {

    unit.unitFeatures = JSON.parse(unit.unitFeaturesJson);
    unit.unitFeaturesString = this.getFeaturesString(unit.unitFeatures);

    unit.rentalSubsidies = JSON.parse(unit.rentalSubsidyJson);
    unit.rentalSubsidyString = this.getSubsidiesString(unit.rentalSubsidies);

    return unit;
  });


  if (this.siteagreementpopulationID != 0) {
    this.unitsDataObj = newUnitList.filter(d => (d.siteAgreementPopulationID === this.siteagreementpopulationID))
  }
  else {
    this.unitsDataObj = newUnitList; // massaged object for GRID
  }
  if (this.unitsDataObj.length < this.totalunitsbyAgereement) {
    this.unitTabVisible = true;
  }
  else {
    this.toastr.info("Can't add units", 'Unit details')
  }


}

getFeaturesString(UnitFeaturesArray: UnitFeature[]) {
  let featuresString: string = null;

  if (UnitFeaturesArray) {
    UnitFeaturesArray.forEach(feature => {
      featuresString = featuresString ? (featuresString + ' ,' + feature.f_desc) : feature.f_desc;
    });
    return featuresString.substring(0, featuresString.length);
  }
  return '';
}

getSubsidiesString(rentSubsidyArray: RentalSubsidy[]) {
  let subsidiesString: string = null;

  if (rentSubsidyArray) {
    rentSubsidyArray.forEach(subsidy => {
      subsidiesString = subsidiesString ? (subsidiesString + ' ,' + subsidy.sub_desc) : subsidy.sub_desc;
    });
    return subsidiesString.substring(0, subsidiesString.length);
  }
  return '';
}


onMoreContactsClick() {
  const dialogRef = this.dialog.open(SiteMoreContactsDialogComponent, {
    width: '40%',
    panelClass: 'transparent',
    data: this.siteContacts
  });

}
loadSiteApproval() {
  this.siteadminservice.getSiteApprovalData(this.siteId.toString())
    .subscribe(
      res1 => {
        this.siteApprovaldata = res1.body as siteApproval;
      },
      error => console.error('Error!', error)
    );
}
saveApprovalData() {
  var siteApprovalValidation = this.siteApprovalComponent.onSubmit();
  if (siteApprovalValidation == true) {
    this.siteadminservice.SaveSiteApproval(this.siteApprovaldata)
      .subscribe(
        data => {
          this.toastr.success("Site approval saved successfully", 'Save Success');
          /* if  (this.siteApprovaldata.isActive == false){
             this.siteSelectedDetail.siteStatus="InActive";
           }
           else {
             this.siteSelectedDetail.siteStatus="Active";
           }*/
          this.getUserRolesInfo();
          this.loadSiteDemographics();
          //    this.sitedemographicComponent.populateDemographicdata();
        },
        error => { this.toastr.error("Save Failed", 'Save Failed'); }
      );

  }
}


getpPagePara() {

  this.activatedRouteSub = this.route.paramMap.subscribe(params => {
    const page = params.get('page');
    if (page === null) {
    this.useroptions.isNewPage=false;      
    } else {
      if (page ==='new')
      this.useroptions.isNewPage=true;       
      else
      this.useroptions.isNewPage=false; 
    }

  });
}

}



