import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryServiceContractComponent } from './primary-service-contract.component';

describe('PrimaryServiceContractComponent', () => {
  let component: PrimaryServiceContractComponent;
  let fixture: ComponentFixture<PrimaryServiceContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryServiceContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryServiceContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
