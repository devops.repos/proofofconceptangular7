import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { siteDemographic, AgencyData, housingProgram } from '../agency-site-model';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { GridOptions } from 'ag-grid-community';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { Router, Event } from '@angular/router';

import { SiteAdminService } from '../site-admin.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

import { MatTabChangeEvent } from '@angular/material';
import { isNullOrUndefined } from 'util';
import { Alert } from 'selenium-webdriver';

@Component({
    selector: 'app-agency-detail',
    templateUrl: './agency-detail.component.html',
    styleUrls: ['./agency-detail.componet.scss']
})
export class AgencyDetailComponent implements OnInit {

    agencyDetailForm: FormGroup;
    agencyID:number;
    agencyinfo: AgencyData;
    agencyTypes: housingProgram[];
    siteCategoryTypes: RefGroupDetails[];
    boroughTypes: RefGroupDetails[];
    userData: AuthData;
    userDataSub: Subscription; f
    userid: number = 10;
    message: string;
    commonServiceSub: Subscription;
   



    constructor(private sidenavService: NavService,
        private router: Router,
        private formBuilder: FormBuilder,
        private siteAdminservice: SiteAdminService,
        private userService: UserService,
        private toastr: ToastrService,
        private commonService: CommonService) {
            this.agencyinfo = {} as AgencyData;
    }

    ngOnInit() {
           
        this.agencyDetailForm = this.formBuilder.group({
            agencyNameCtrl: ['', [Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9 ]+')]],
            agencyTypeCtrl: ['', [Validators.required]],
            agencyaddressCtrl: ['', [Validators.maxLength(200)]],
            agencycityCtrl: ['', [Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
            agencystateCtrl: ['', [Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
            agencyzipCtrl: ['', [Validators.maxLength(5), Validators.pattern('[0-9]+')]],
            titleCtrl: ['', [Validators.maxLength(25),  Validators.pattern('[a-zA-Z. ]+')]],
            emailCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.email]],
            officephoneCtrl: ['', [Validators.required, Validators.maxLength(10)]],
            firstNameCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z ]+')]],
            lastNameCtrl: ['', [Validators.required, Validators.maxLength(50), , Validators.pattern('[a-zA-Z ]+')]],
            cellphoneCtrl: [''],
            officephoneExtnCtrl: ['',],
            faxCtrl: [''],
   
        });

        this.siteAdminservice.getAgencySelected().subscribe((res: number) => {
            if (res) {
              const data = res as number;
              this.agencyID = data;
            
              this.siteAdminservice.getAgencyList(this.agencyID.toString(), null)
              .subscribe(
                res => {
                  const data1 = res.body as AgencyData[];
                  if (data1.length >0)
                     this.agencyinfo = data1[0];
                     //alert(this.agencyinfo.contactFirstName);
                     console.log(this.agencyinfo);
            
          });
        }
    });

    
        this.getUserRolesInfo();
        this.loadRefGroupDetails();
        this.loadHousingPrograms();

    }


    ngOnDestroy = () => {
        this.commonServiceSub.unsubscribe();
        this.userDataSub.unsubscribe();
    }

    getUserRolesInfo() {
        this.userDataSub = this.userService.getUserData().subscribe(res => {
            this.userData = res;
            if (this.userData) {

                this.userid = this.userData.optionUserId;
            }

        }
        );
    }

    loadRefGroupDetails = () => {
        const yesNoRefGroupId = 7; const siteCategoryRefGroupId = 2; const siteboroughRefGroupId = 6;
        this.commonServiceSub = this.commonService.getRefGroupDetails(yesNoRefGroupId + ',' + siteCategoryRefGroupId + ',' + siteboroughRefGroupId).subscribe(
            res => {
                const data = res.body as RefGroupDetails[];
                //this.agencyTypes = data.filter(d => (d.refGroupID === agencyTypeRefGroupId));
                this.boroughTypes = data.filter(d => (d.refGroupID === siteboroughRefGroupId && d.refGroupDetailID >= 27 && d.refGroupDetailID <= 31));
            },
            error => console.error('Error!', error)
        );
    }




    onSubmit = (): void => {
        // this.markFormGroupTouched(this.agencyDetailForm);
        // if (this.agencyDetailForm.valid) {
        //this.saveDocument();
        // }
    }

    private markFormGroupTouched = (formGroup: FormGroup) => {
        (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);

            }
        });
    }

  
    validateAgency = (): boolean => {
        if (this.agencyinfo.name === null || this.agencyinfo.name.trim() === '') {
            this.message = "Please enter  agency name "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencyDetailForm.controls.agencyNameCtrl.valid) {
            this.message = "Please enter only alphabet for agency name "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.agencyinfo.agencyType === null || this.agencyinfo.agencyType.trim() === '') {
            this.message = "Please select  agency type "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.agencyinfo.agencyAddress === null || this.agencyinfo.agencyAddress.trim() === '') {
            this.message = "Please enter agency address"
            this.toastr.error(this.message, "Error");
            return false;
        }


        if (this.agencyinfo.city === null || this.agencyinfo.city.trim() === '') {
            this.message = "Please enter  agency city"
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencyDetailForm.controls.agencycityCtrl.valid) {
            this.message = "Please enter only alphabet for agency city "
            this.toastr.error(this.message, "Error");
            return false;
        }

        if (this.agencyinfo.state === null || this.agencyinfo.state.trim() === '') {
            this.message = "Please enter agency state"
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencyDetailForm.controls.agencystateCtrl.valid) {
            this.message = "Please enter only alphabet for agency state "
            this.toastr.error(this.message, "Error");
            return false;
        }


        if (this.agencyinfo.zip === null || this.agencyinfo.zip.trim() === '') {
            this.message = "please enter agency zip "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencyDetailForm.controls.agencyzipCtrl.valid) {
            this.message = "Please enter valid agency ZIP code"
            this.toastr.error(this.message, "Error");
            return false;
        }
        if (this.agencyinfo.contactFirstName === null || this.agencyinfo.contactFirstName.trim() === '') {
            this.message = "please enter agency contact first name  "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencyDetailForm.controls.firstNameCtrl.valid) {
            this.message = "Please enter only Alphbet for first name "
            this.toastr.error(this.message, "Error");
            return false;
        }
        if (this.agencyinfo.contactLastName === null || this.agencyinfo.contactLastName.trim() === '') {
            this.message = "please enter agency contact last name  "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencyDetailForm.controls.lastNameCtrl.valid) {
            this.message = "Please enter only Alphbet for last name "
            this.toastr.error(this.message, "Error");
            return false;
        }
        if (this.agencyinfo.email === null || this.agencyinfo.email.trim() === '') {
            this.message = "please enter agency contact email  "
            this.toastr.error(this.message, "Error");
            return false;
        }
        else if (!this.agencyDetailForm.controls.emailCtrl.valid) {
            this.message = "Please enter valid email  "
            this.toastr.error(this.message, "Error");
            return false;
        }
        if (this.agencyinfo.phone === null || this.agencyinfo.phone.trim() === '') {
            this.message = "please enter agency contact phone  "
            this.toastr.error(this.message, "Error");
            return false;
        }

        return true;


    }


    SaveAgency() {
        this.agencyinfo.userId = this.userid;
    
        this.siteAdminservice.SaveAgencyinfo(this.agencyinfo)
            .subscribe(
                data => {
                    this.toastr.success("Successfully updated agency detail", 'Agency edit');
                    var result = data as AgencyData;
                },
                error => { this.toastr.error("Save Failed", 'Save Failed'); }
            );

    }

  

    loadHousingPrograms() {
        this.siteAdminservice.getHousingPrograms()
            .subscribe(
                res => {
                    this.agencyTypes = res.body as housingProgram[];
                },
                error => console.error('Error!', error)
            );
    }
exitPage(){
    this.router.navigate(['./admin/agency-site-maintenance', this.agencyID]);
}

onSave()
{
  if ( this.validateAgency() === true)
  {
      this.SaveAgency();
  }

}
  
}
