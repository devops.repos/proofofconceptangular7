
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { SiteAgreementPopulation } from '../agency-site-model';


@Component({
  selector: "primary-service-contract-action",
  template: `<mat-icon  matTooltip='Unit Detail' (click)="onView()" class="primary-service-contract-action-icon" color="primary">dvr</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete' class="primary-service-contract-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .primary-service-contract-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class PrimaryServiceContractActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private siteagrementSeviceselected: SiteAgreementPopulation;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    this.siteagrementSeviceselected = {
    siteID : this.params.data.siteID,
    primaryServiceContractType: this.params.data.primaryServiceContractType,
    rentalSubsidies :this.params.data.rentalSubsidies,
    siteAgreementPopulationID :this.params.data.siteAgreementPopulationID,
    agreementPopulationID:this.params.data.agreementPopulationID,
    totalUnits:this.params.data.totalUnits,
    isActive :this.params.data.isActive,
    userId : this.params.data.userId
    
    };

    this.params.context.componentParent.primaryServiceContractViewParent(this.siteagrementSeviceselected);
  }

  onDelete() {
    this.siteagrementSeviceselected = {
      siteID : this.params.data.siteID,
      primaryServiceContractType: this.params.data.primaryServiceContractType,
      rentalSubsidies :this.params.data.rentalSubsidies,
      siteAgreementPopulationID :this.params.data.siteAgreementPopulationID,
      agreementPopulationID:this.params.data.agreementPopulationID,
      totalUnits:this.params.data.totalUnits,
       isActive :this.params.data.isActive,
      userId : this.params.data.userId
    };
    this.params.context.componentParent.sitePrimartSerivceContractDeleteParent(this.siteagrementSeviceselected);
  }

  refresh(): boolean {
    return false;
  }
}
