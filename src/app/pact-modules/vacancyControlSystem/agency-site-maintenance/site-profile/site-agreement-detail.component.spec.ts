import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteAgreementDetailComponent } from './site-agreement-detail.component';

describe('SiteAgreementDetailComponent', () => {
  let component: SiteAgreementDetailComponent;
  let fixture: ComponentFixture<SiteAgreementDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteAgreementDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteAgreementDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
