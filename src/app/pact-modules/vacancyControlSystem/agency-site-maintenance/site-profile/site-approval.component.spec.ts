import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteApprovalComponent } from './site-approval.component';

describe('SiteApprovalComponent', () => {
  let component: SiteApprovalComponent;
  let fixture: ComponentFixture<SiteApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
