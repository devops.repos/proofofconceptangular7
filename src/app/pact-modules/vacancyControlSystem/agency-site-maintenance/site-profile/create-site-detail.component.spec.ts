import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSiteDetailComponent } from './create-site-detail.component';

describe('CreateSiteDetailComponent', () => {
  let component: CreateSiteDetailComponent;
  let fixture: ComponentFixture<CreateSiteDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSiteDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSiteDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
