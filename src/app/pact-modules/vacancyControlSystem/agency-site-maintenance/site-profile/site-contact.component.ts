import { FormGroup, FormBuilder, Validators,FormArray } from '@angular/forms';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  OnDestroy
} from '@angular/core';

import 'ag-grid-enterprise';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { SiteContact,UserOptions } from '../agency-site-model';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { GridOptions } from 'ag-grid-community';
import { SiteAdminService } from '../site-admin.service';
import {SiteContactActionComponent} from './site-contact-action.component'

@Component({
  selector: 'app-site-contact',
  templateUrl: './site-contact.component.html',
  styleUrls: ['./site-contact.component.scss']
})
export class SiteContactComponent implements OnInit, OnDestroy {
  
  contactForm: FormGroup;
  SiteContactsData:SiteContact[]=[];
  siteContactModel: SiteContact = {
    siteContactID : null,
    siteRequestID: null,
    siteID : null,
    optionUserID: null,
    firstName :null,
    lastName :null,
    title:null,
    email :null,
    phone :null,
    extension :null,
    alternatePhone :null,
    alternateExtension :null,
    fax :null,
    isSysAdmin:null,
    isActive :null,
    createdBy : null,
    createdDate :null,
    updatedBy : null,
    updatedDate :null,
  };

  message: string;
  @Input() contactsiteId: number;
  @Input() options:UserOptions;
  @Input() contactType:number;
  @Output() siteContactDataChanged = new EventEmitter();
  validationMessages = {
    firstNameCtrl: {
      'required': 'Please enter first name.',
      'pattern' : 'please enter valid first name'
    },
    lastNameCtrl: {
        'required': 'Please enter last name?.',
        'pattern' : 'please enter valid last name'
      },
    
      emailCtrl: {
        'required': 'Please enter email .',
        'pattern' : 'please enter valid email',
        'email' : 'please enter valid email.'
       
      },
      officephoneCtrl : {
        'required': 'Please enter office Phone.',
        'pattern' : 'please enter valid phone.'
      },
      
  
  };
  
  
  
  formErrors = {
    firstNameCtrl: '',
    lastNameCtrl: '',
    emailCtrl: '',
    officephoneCtrl:'',
    
  };
  

  
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  context;
  public gridOptions: GridOptions;
  overlayLoadingTemplate: string='';
  overlayNoRowsTemplate: string='';

  
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private confirmDialogService:ConfirmDialogService,
    private siteadminservice : SiteAdminService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
      sideBar: {
       toolPanels: [
               {
                   id: 'columns',
                   labelDefault: 'Columns',
                   labelKey: 'columns',
                   iconKey: 'columns',
                   toolPanel: 'agColumnsToolPanel',
                   toolPanelParams: {
                       suppressValues: true,
                       suppressPivots: true,
                       suppressPivotMode: true,
                       suppressRowGroups: false
                   }
               },
               {
                   id: 'filters',
                   labelDefault: 'Filters',
                   labelKey: 'filters',
                   iconKey: 'filter',
                   toolPanel: 'agFiltersToolPanel',
               }
           ],
           defaultToolPanel: ''
       }
    } as GridOptions;
    //this.gridOptions.api.hideOverlay();
   
    this.columnDefs = [
     {
       headerName: 'Site Contatct ID',
      filter: 'agTextColumnFilter',
      field : "siteContactID",
      hide : true
     },
     {
      headerName: 'Site ID',
     filter: 'agTextColumnFilter',
     field : "siteID",
     hide : true
    },
     {
       headerName: 'First Name',
       field: 'firstName',
        width: 250,
      filter: 'agTextColumnFilter'
     },
     {
       headerName: 'Last Name',
       field: 'lastName',
       width: 250,
       filter: 'agTextColumnFilter'
     },
     {
      headerName: 'Office Title',
      field: 'title',
      width: 100,
      filter: 'agTextColumnFilter'
    },
     {
       headerName: 'Email',
       field: 'email',
       width: 150,
       filter: 'agTextColumnFilter'
     },
     {
       headerName: 'Office Phone',
       field: 'phone',
       width: 150,
       filter: 'agTextColumnFilter'
     },
     {
      headerName: 'Extension',
      field: 'extension',
      width: 50,
      filter: 'agTextColumnFilter'
    },
     {
       headerName: 'Alternate Phone',
       field: 'alternatePhone',
       width: 150,
       filter: 'agTextColumnFilter'
     },
     {
      headerName: 'Extension',
      field: 'alternateExtension',
      width: 50,
      filter: 'agTextColumnFilter'
    },
     {
      headerName: 'Fax',
      field: 'fax',
      width: 150,
      filter: 'agTextColumnFilter'
    },
   
    {
      headerName: 'Sys Admin',
      //field: 'isSysAdmin',
      width: 100,
      filter: 'agTextColumnFilter',
      valueGetter: function(params) {
        if (params.data.isSysAdmin){
          return 'Yes';
        }
        else {
        return '';
        }
      }
    },
     {
         headerName: 'Actions',
         field: 'action',
         width: 100,
         filter: false,
         sortable: false,
         resizable: false,
         pinned: 'left',
         suppressMenu: true,
         cellRenderer: 'actionRenderer',
     }
    ];
   
    this.defaultColDef = {
     sortable: true,
     resizable: true,
    // filter: false
     filter: true,
     floatingFilter: true
    };
     this.rowSelection = 'single';
     this.pagination = true;
     this.context = { componentParent: this };
     this.frameworkComponents = {
      actionRenderer: SiteContactActionComponent
    };
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the site contacts are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Site contact are available</span>';
  }

  

  ngOnInit() {

    this.contactForm = this.fb.group({
    firstNameCtrl: ['', [Validators.required]],
    lastNameCtrl: ['', [Validators.required]],
    titleCtrl: [''],
    emailCtrl: ['', [Validators.required,  Validators.email]],
    officephoneCtrl: ['',[Validators.required]],
    cellphoneCtrl: [''],
    officephoneExtnCtrl: ['',],
    cellphoneExtnCtrl: [''],
    faxCtrl : [''],
    sysadminNameCtrl: [''],
    hdnContactIDCtrl: [''],
    });
    this.enabledisablecontrol();
    if (this.contactsiteId != null){
      this.getContactsList();
    }
    if (this.options.isIH === true){
      this.contactForm.get('sysadminNameCtrl').enable();
    }
    else {
      this.contactForm.get('sysadminNameCtrl').disable();
    }
    this.contactForm.valueChanges.subscribe(data => {
    });
    

    
    
  }


  

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setDomLayout('autoHeight');

    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {

      if (column.colId !== 'action' ) {
          allColumnIds.push(column.colId);
      }
    });
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }


 
  

  onSubmit = (): void => {
    this.markFormGroupTouched(this.contactForm);
    if (this.contactForm.valid) {
      //this.saveDocument();
    }
  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

 
  siteContactDeleteParent = (cell: SiteContact) => {
    const title = 'Confirm Delete';
    const primaryMessage = '';
    const secondaryMessage = "Are you sure  to delete  contact -" + cell.firstName + "," + cell.lastName +" ?";
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => this.deleteSiteContact(cell),
        (negativeResponse) => console.log(),
      );

  }

  getContactsList = () => {
    if (this.contactType == 1)
    {
    this.siteadminservice.getSiteContacts(this.contactsiteId.toString())
      .subscribe(
        res => {
          if (res.body) {
            this.SiteContactsData = res.body as SiteContact[];
          }
        },
        error => console.error('Error in retrieving the Contact Data By Site ID...!', error)
      );
     }
     //add code for the request.

  }

  deleteSiteContact = (dataSelected: SiteContact) => {
    dataSelected.updatedBy = this.options.optionUserId;
    if (this.contactType == 1)
    {
    this.siteadminservice.deleteSiteContact(dataSelected).subscribe(
      res => {
        this.message = 'Contact deleted successfully.';
        this.toastr.success(this.message, 'Delete');
        // console.error('Success!', res);
        this.getContactsList();
        this.clearContactFields();
        this.siteContactDataChanged.emit();
      },
      error => {
        this.message = 'Contact did not delete.';
        this.toastr.error(this.message, 'Delete');
        // console.error('Error!', error);
      }
    );
    }
    // add delete code for  request
  }

  saveSiteContact() {
    this.setSiteContactValidator();
    if (this.ValidateSiteContact() === false) {
        // this.toastr.error('Validation failed', 'Save');
      return;
    }
    if (this.ValidateContact()) 
    {
    //this.siteContactModel.SiteContactID= this.contactForm.get('hdnContactIDCtrl').value;
    if (this.contactsiteId != null){
        this.siteContactModel.siteID = this.contactsiteId;
    }
    
    this.siteContactModel.isActive = true;
    if (this.siteContactModel.siteContactID != null && this.siteContactModel.siteContactID != 0 )
    {
      this.siteContactModel.updatedBy = this.options.optionUserId;
      this.siteContactModel.createdBy = null
    }
    else
    {
      this.siteContactModel.siteContactID =0;
      this.siteContactModel.createdBy = this.options.optionUserId;
      this.siteContactModel.updatedBy= null;
    }
    this.siteContactModel.isActive = true;
    if (this.contactType == 1)
    {
    this.siteadminservice.saveSiteContact(this.siteContactModel)
      .subscribe(
        data => {
          this.message = 'Contact saved successfully.';
          this.toastr.success(this.message, 'Save');
          this.getContactsList();
          this.clearValidatorsForSiteContact();
          this.clearContactFields();
          this.siteContactDataChanged.emit();
        }, 
        error => {
          this.message = 'Contact did not Save.';
          this.toastr.error(this.message, 'Save');
        }
      );
      }
    }
    // add code for Request Save

  }


  setSiteContactValidator = () => {

    this.contactForm.controls.firstNameCtrl.setValidators(Validators.compose([Validators.required,  Validators.pattern('[a-zA-Z]+')]));
    this.contactForm.controls.lastNameCtrl.setValidators(Validators.compose([Validators.required,  Validators.pattern('[a-zA-Z]+')]));
    this.contactForm.controls.titleCtrl.setValidators(Validators.compose([  Validators.pattern('[a-zA-Z]+')]));
    this.contactForm.controls.emailCtrl.setValidators(Validators.compose([Validators.required,  Validators.email]));
    this.contactForm.controls.officephoneCtrl.setValidators(Validators.compose([Validators.required ]));
    this.contactForm.controls.firstNameCtrl.updateValueAndValidity();
    this.contactForm.controls.lastNameCtrl.updateValueAndValidity();
    this.contactForm.controls.titleCtrl.updateValueAndValidity();
    this.contactForm.controls.emailCtrl.updateValueAndValidity();
    this.contactForm.controls.officephoneCtrl.updateValueAndValidity();
    this.contactForm.controls.cellphoneCtrl.updateValueAndValidity();
    this.contactForm.controls.faxCtrl.updateValueAndValidity();
    
  }

  clearValidatorsForSiteContact = () => {
    this.contactForm.controls.firstNameCtrl.clearValidators();
    this.contactForm.controls.lastNameCtrl.clearValidators();
    this.contactForm.controls.titleCtrl.clearValidators();
    this.contactForm.controls.emailCtrl.clearValidators();
    this.contactForm.controls.officephoneCtrl.clearValidators();
    this.contactForm.controls.cellphoneCtrl.clearValidators();
    this.contactForm.controls.faxCtrl.clearValidators();

  }

  clearContactFields() {
    this.contactForm.get('hdnContactIDCtrl').setValue(0);
    this.contactForm.get('firstNameCtrl').setValue('');
     this.contactForm.get('lastNameCtrl').setValue('');
     this.contactForm.get('titleCtrl').setValue('');
     this.contactForm.get('emailCtrl').setValue('');
    this.contactForm.get('officephoneCtrl').setValue('');
     this.contactForm.get('cellphoneCtrl').setValue('');
     this.contactForm.get('officephoneExtnCtrl').setValue('');
     this.contactForm.get('cellphoneExtnCtrl').setValue('');
     this.contactForm.get('faxCtrl').setValue('');
     this.contactForm.get('sysadminNameCtrl').setValue(false);
     //alert(this.contactForm.get('sysadminNameCtrl').value);
  }


  contactViewParent = (cell: SiteContact) => {
    // console.log(JSON.stringify(cell));

    if (cell.siteContactID != null) {
      this.siteContactModel.siteContactID = cell.siteContactID;
    }
    if (cell.siteID != null) {
      this.siteContactModel.siteID = cell.siteID;
    }
    if (cell.firstName != null) {
      this.siteContactModel.firstName = cell.firstName;
    }
    if (cell.lastName != null) {
      this.siteContactModel.lastName = cell.lastName;
    }
    if (cell.title != null) {
      this.siteContactModel.title = cell.title;
    }
    if (cell.email != null) {
      this.siteContactModel.email = cell.email;
    }
    if (cell.phone != null) {
      this.siteContactModel.phone = cell.phone;
    }
    if (cell.extension != null) {
      this.siteContactModel.extension = cell.extension;
    }
      else{
        this.siteContactModel.extension = null;
      }

    
    if (cell.alternatePhone != null) {
      this.siteContactModel.alternatePhone = cell.alternatePhone;
    }
    else
    {
      this.siteContactModel.alternatePhone=null;

    }
    if (cell.alternateExtension != null) {
      this.siteContactModel.alternateExtension = cell.alternateExtension;
      }
      else  {
        this.siteContactModel.alternateExtension = null;
      }

    if (cell.fax != null) {
      this.siteContactModel.fax = cell.fax;
    }
    if (cell.isActive != null) {
      this.siteContactModel.isActive = cell.isActive;
    }
    if (cell.isSysAdmin != null) {
      this.siteContactModel.isSysAdmin = cell.isSysAdmin;
    }
    else{
      this.siteContactModel.isSysAdmin = false;
      }
      

  }

  ValidateContact = (): boolean => {
    if (this.options.isNewPage === false) {
      this.siteContactModel.isSysAdmin= false;
    }

    if (this.siteContactModel.siteContactID === null || this.siteContactModel.siteContactID === 0 )
    {
     if (this.SiteContactsData.length > 4)
      {
       this.message = "Only 5 contacts are allowed"
       this.toastr.error(this.message,"Error");
        return false;
     }
    }



    if (this.SiteContactsData.length > 0)
    {
      if (this.siteContactModel.siteContactID != null && this.siteContactModel.siteContactID != 0 )
      {
        if (this.SiteContactsData.find(x => ( (x.siteContactID !=this.siteContactModel.siteContactID) 
          && (
          (x.firstName === this.siteContactModel.firstName.trim()  && x.lastName === this.siteContactModel.lastName.trim()) 
        && (x.email === this.siteContactModel.email)
        && (x.phone === this.siteContactModel.phone)
         ))) != null)
        {
          this.message = "Duplicate contacts are not allowed.";
          this.toastr.error(this.message,"Error");
          return false;
        }
      }
      else

      if (this.SiteContactsData.find(x => (  
                          (x.firstName === this.siteContactModel.firstName.trim()  && x.lastName === this.siteContactModel.lastName.trim()) 
                          && (x.email === this.siteContactModel.email)
                          && (x.phone === this.siteContactModel.phone) )) != null )
      {
        this.message = "Duplicate contacts are not allowed.";
        this.toastr.error(this.message,"Error");
        return false;
      }
    }

    if (this.SiteContactsData.length > 0)
    {
      if (this.siteContactModel.isSysAdmin === true)
      {
        if (this.checksysAdminExits(true))
        {
          this.message = "Only one SysAdmin is allowed.";
          this.toastr.error(this.message,"Error", );
          return false;
        
        }
      }
    }    
    return true;
   
  }

  checksysAdminExits(role: boolean):boolean {
    if (this.siteContactModel.siteContactID != null && this.siteContactModel.siteContactID != 0 )
    {
      return this.SiteContactsData.some(r => r.isSysAdmin === role && r.siteContactID !=this.siteContactModel.siteContactID  );
    }
    else
    {
      return this.SiteContactsData.some(r => r.isSysAdmin === role);
      
    }
  }


ngOnDestroy = () => {

}

enabledisablecontrol()
{
  if (this.options.isDisableForInactiveSite){

    this.contactForm.disable();
  }  
  else
  {
    this.contactForm.enable();
  }
}

resetformerror()
{
  this.formErrors = {
    firstNameCtrl: '',
    lastNameCtrl: '',
    emailCtrl: '',
    officephoneCtrl:'',
  };
}

ValidateSiteContact= (): boolean =>
    {
    var messages : any ;
    var key : any;
    this.resetformerror();

    if (!this.contactForm.controls.firstNameCtrl.valid) {
        key ="firstNameCtrl";
        messages= this.validationMessages[key];
        for (const errorKey in this.contactForm.controls.firstNameCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
      } 
    }
    if (!this.contactForm.controls.lastNameCtrl.valid) {
      key ="lastNameCtrl";
      messages= this.validationMessages[key];
      for (const errorKey in this.contactForm.controls.lastNameCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] += messages[errorKey] + '\n';
        }
    } 
  }
  if (!this.contactForm.controls.emailCtrl.valid) {
    key ="emailCtrl";
    messages= this.validationMessages[key];
    for (const errorKey in this.contactForm.controls.emailCtrl.errors) {
      if (errorKey) {
        this.formErrors[key] += messages[errorKey] + '\n';
      }
  } 
}
  if (!this.contactForm.controls.officephoneCtrl.valid) {
    key ="officephoneCtrl";
   messages= this.validationMessages[key];
  for (const errorKey in this.contactForm.controls.officephoneCtrl.errors) {
    if (errorKey) {
      this.formErrors[key] += messages[errorKey] + '\n';
    }
  } 
}
  var errormessage = '';
      for (const errorkey in this.formErrors) {
        if (errorkey) {
          errormessage += this.formErrors[errorkey];
        }
      }
      if (errormessage != '') 
      {
        this.toastr.error(errormessage,"Validation Error", );
        return false;
      }
        else
        {
          return true; 
        }
 
    }
    ngOnChanges() {

      if (this.contactsiteId != null){
        this.getContactsList();
      }
     
  
    }
}
