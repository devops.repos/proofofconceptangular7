import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { siteDemographic, AgencyData, housingProgram } from '../agency-site-model';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { GridOptions } from 'ag-grid-community';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { Router, Event } from '@angular/router';

import { SiteAdminService } from '../site-admin.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { CommonService } from '../../../../services/helper-services/common.service';
import { FormGroup, FormBuilder, FormControl,Validators, FormArray } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { Subscription,Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-create-site',
  templateUrl: './create-site.component.html',
  styleUrls: ['./create-site.component.scss']
})
export class CreateSiteComponent implements OnInit {
  siteCreateForm: FormGroup;
  agencyList: AgencyData[];
  savedsites: siteDemographic[];
  sites: siteDemographic[] = [];
  site: siteDemographic
  userData: AuthData;
  userDataSub: Subscription;
  userid: number = 10;
  tabSiteSelectedIndex: number;
  message: string;
  commonServiceSub: Subscription;
  siteCategoryTypes: RefGroupDetails[];
  boroughTypes: RefGroupDetails[];
  agencyTypes: housingProgram[];
  tagencyid: number = 0;
  filteredAgencyData: Observable<any[]>;




  constructor(private sidenavService: NavService,
    private router: Router,
    private formBuilder: FormBuilder,
    private siteAdminservice: SiteAdminService,
    private userService: UserService,
    private toastr: ToastrService,
    private commonService: CommonService) {
  }

  ngOnInit() {


    this.siteCreateForm = this.formBuilder.group({
      siteNameCtrl: ['', [Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9 ]+')]],
      addressCtrl: ['', [Validators.maxLength(200)]],
      cityCtrl: ['', [Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
      stateCtrl: ['', [Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
      zipCtrl: ['', [Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      agencyCtrl: ['', [Validators.required]],
      hdnsiteIDCtrl: [''],
      siteCategoryTypeCtrl: ['', [Validators.required]],
      siteBoroughTypeCtrl: ['', [Validators.required]],
      siteTypeCtrl: ['', [Validators.required]],
      isCapsMandateRadioCtrl: [''],
      isAddressSameasAgency: ['']
    });
    this.clearsiteInfo();

    this.loadagencylist();
    this.getUserRolesInfo();
    this.loadRefGroupDetails();
    this.loadHousingPrograms();


  }

  ngOnDestroy = () => {
    this.commonServiceSub.unsubscribe();
    this.userDataSub.unsubscribe();
  }

  loadagencylist() {
    this.siteAdminservice.getAgencyList(null, null)
      .subscribe(
        res => {
          this.agencyList = res.body as AgencyData[];
          this.filteredAgencyData = this.siteCreateForm.controls.agencyCtrl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
            );
        },
        error => console.error('Error!', error)
      );
  }

  loadRefGroupDetails = () => {
    const yesNoRefGroupId = 7; const siteCategoryRefGroupId = 2; const siteboroughRefGroupId = 6;
    this.commonServiceSub = this.commonService.getRefGroupDetails(yesNoRefGroupId + ',' + siteCategoryRefGroupId + ',' + siteboroughRefGroupId).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.siteCategoryTypes = data.filter(d => (d.refGroupID === siteCategoryRefGroupId && (d.refGroupDetailID === 5 || d.refGroupDetailID === 7 || d.refGroupDetailID === 6)));
        //this.boroughTypes = data.filter(d => (d.refGroupID === siteboroughRefGroupId));
        this.boroughTypes = data.filter(d => (d.refGroupID === siteboroughRefGroupId && d.refGroupDetailID >= 27 && d.refGroupDetailID <= 31));
      },
      error => console.error('Error!', error)
    );
  }


  getUserRolesInfo() {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {

        this.userid = this.userData.optionUserId;
      }

    }
    );
  }





  onSubmit = (): void => {
    // this.markFormGroupTouched(this.siteCreateForm);
    // if (this.siteCreateForm.valid) {
    //this.saveDocument();
    // }
  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);

      }
    });
  }
  addnewSites() {
    if (this.ValidateNewsites()) {
      var findsite = this.sites.find(x => x.name.trim().toUpperCase() == this.site.name.trim().toUpperCase());
      this.tagencyid = this.site.agencyID;
      if (findsite == null) {
        const newitem: siteDemographic = {
          siteID: 0,
          siteNo: null,
          agencyID: this.site.agencyID,
          housingProgram: this.site.housingProgram,
          name: this.site.name,
          address: this.site.address,
          city: this.site.city,
          state: this.site.state,
          zip: this.site.zip,
          locationType: null,
          referralAvailableType: null,
          tcoReadyType: null,
          tcoContractStartDate: null,
          taxCreditType: null,
          maxIncomeForStudio: null,
          maxIncomeForOneBR: null,
          levelOfCareType: null,
          contractType: null,
          contractStartDate: null,
          siteTrackedType: null,
          tadLiasion: null,
          sameAsSiteInterviewAddressType: null,
          interviewAddress: null,
          interviewCity: null,
          interviewState: null,
          interviewZip: null,
          interviewPhone: null,
          siteFeatures: null,
          siteTypeDesc: null,
          agencyName: null,
          agencyNo: null,
          userId: this.userid,
          siteCategoryType: this.site.siteCategoryType,
          boroughType: this.site.boroughType,
          isCapsMandate: this.site.isCapsMandate
        }

        this.siteAdminservice.checDuplicateSite(newitem)
          .subscribe(
            data => {

              var checksites = data as boolean
              if (!checksites) {
                this.sites.push(newitem);
                this.clearsiteInfo();
                this.site.agencyID = this.tagencyid;
                this.toastr.success("Successfully Added", 'Site Added');
                this.siteCreateForm.get('agencyCtrl').disable();
                this.siteCreateForm.markAsUntouched();
                this.siteCreateForm.controls['isAddressSameasAgency'].setValue(false);
                
              }
              else {
                this.toastr.error('duplicate site', 'Adding New site');
              }
            },
            error => { this.toastr.error("checkduplicate", 'Check duplicate'); }
          );

      }

      else {
        this.toastr.error('duplicate site', 'Adding New site');
      }

    }
  }


  ValidateNewsites = (): boolean => {

    if (this.site.agencyID == null || this.site.agencyID == 0) {
      this.message = "please select the Agency"
      this.toastr.error(this.message, "Error");
      return false;
    }
    if (this.site.name === null || this.site.name.trim() === '') {
      this.message = "Please enter  site name "
      this.toastr.error(this.message, "Error");
      return false;
    }
    else if (!this.siteCreateForm.controls.siteNameCtrl.valid) {
      this.message = "Please enter only Alphabet for Site Name "
      this.toastr.error(this.message, "Error");
      return false;
    }


    if (this.site.address === null || this.site.address.trim() === '') {
      this.message = "Please enter  site Address"
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.city === null || this.site.city.trim() === '') {
      this.message = "Please enter  site city"
      this.toastr.error(this.message, "Error");
      return false;
    }
    else if (!this.siteCreateForm.controls.cityCtrl.valid) {
      this.message = "Please enter only Alphabet for city "
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.state === null || this.site.state.trim() === '') {
      this.message = "Please enter site state"
      this.toastr.error(this.message, "Error");
      return false;
    }
    else if (!this.siteCreateForm.controls.stateCtrl.valid) {
      this.message = "Please enter only Alphabet for state "
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.zip === null || this.site.zip.trim() === '') {
      this.message = "Please enter site zip "
      this.toastr.error(this.message, "Error");
      return false;
    }
    else if (!this.siteCreateForm.controls.zipCtrl.valid) {
      this.message = "Please enter valid ZIP code"
      this.toastr.error(this.message, "Error");
      return false;
    }


    if (this.site.boroughType === null || this.site.boroughType === 0) {
      this.message = "Please select the  borough"
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.housingProgram === null || this.site.housingProgram === '') {
      this.message = "Please select the  Site type"
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.siteCategoryType === null || this.site.siteCategoryType === 0) {
      this.message = "Please select the  Site category"
      this.toastr.error(this.message, "Error");
      return false;
    }

    if (this.site.siteCategoryType === 5 || this.site.siteCategoryType === 6) {
      if (this.site.isCapsMandate === null) {
        this.message = "Please select the  Caps Mandate"
        this.toastr.error(this.message, "Error");
        return false;
      }
    }

    return true;

  }


  enablesiteinformation() {
    this.siteCreateForm.controls.siteCategoryTypeCtrl.enable();
    this.siteCreateForm.controls.siteBoroughTypeCtrl.enable();
    this.siteCreateForm.controls.addressCtrl.enable();
    this.siteCreateForm.controls.siteNameCtrl.enable();
    this.siteCreateForm.controls.cityCtrl.enable();
    this.siteCreateForm.controls.stateCtrl.enable();
    this.siteCreateForm.controls.zipCtrl.enable();
    this.siteCreateForm.controls.siteTypeCtrl.enable();
    this.siteCreateForm.controls.isCapsMandateRadioCtrl.enable();
    this.siteCreateForm.controls.isAddressSameasAgency.enable();
  }


  disablesiteinformation() {
    this.siteCreateForm.controls.siteCategoryTypeCtrl.disable();
    this.siteCreateForm.controls.siteBoroughTypeCtrl.disable();
    this.siteCreateForm.controls.siteNameCtrl.disable();
    this.siteCreateForm.controls.cityCtrl.disable();
    this.siteCreateForm.controls.addressCtrl.disable();
    this.siteCreateForm.controls.stateCtrl.disable();
    this.siteCreateForm.controls.zipCtrl.disable();
    this.siteCreateForm.controls.siteTypeCtrl.disable();
    this.siteCreateForm.controls.isCapsMandateRadioCtrl.disable();
    this.siteCreateForm.controls.isAddressSameasAgency.disable();
  }





  clearsiteInfo() {


    this.site = {
      siteID: 0,
      siteNo: null,
      agencyID: 0,
      housingProgram: null,
      name: '',
      address: '',
      city: '',
      state: '',
      zip: '',
      locationType: null,
      referralAvailableType: null,
      tcoReadyType: null,
      tcoContractStartDate: null,
      taxCreditType: null,
      maxIncomeForStudio: null,
      maxIncomeForOneBR: null,
      levelOfCareType: null,
      contractType: null,
      contractStartDate: null,
      siteTrackedType: null,
      tadLiasion: null,
      sameAsSiteInterviewAddressType: null,
      interviewAddress: null,
      interviewCity: null,
      interviewState: null,
      interviewZip: null,
      interviewPhone: null,
      siteFeatures: null,
      siteTypeDesc: null,
      agencyName: null,
      agencyNo: null,
      userId: 10,
      siteCategoryType: null,
      boroughType: null
    }
    //this.siteCreateForm.controls['isAddressSameasAgency'].setValue(false);
    // this.siteCreateForm.markAsUntouched(); 
    //this.enablesiteinformation();

  }

  clearsite() {
    this.clearsiteInfo();
    this.site.agencyID = this.tagencyid;
    this.enablesiteinformation();
  }

  Savesites() {
    this.siteAdminservice.saveSites(this.sites)
      .subscribe(
        data => {
          this.toastr.success("Site created successfully", 'Site creation');
          this.savedsites = data as siteDemographic[];
          console.log(this.savedsites);
          this.siteAdminservice.setSiteCreated(this.savedsites);
          this.router.navigate(['./admin/createsitedetail','new']);
        },
        error => { this.toastr.error("Save Failed", 'Save Failed'); }
      );

  }

  proceed() {
    if (this.sites.length > 0) {
      this.Savesites();
    }
    else {
      this.toastr.warning("There is no new site added, cannot proceed. Please click ‘Add new’ before proceeding.", 'Validation Error.');
    }

  }

  tabSiteChanged(tabsiteChangeEvent: MatTabChangeEvent): void {
    // /* Saving Data Between Tab Navigation */
    // if (this.dvGroup.touched || this.dvGroup.dirty) {
    //   this.onSave(false);
    // }
    this.tabSiteSelectedIndex = tabsiteChangeEvent.index;
    if (this.sites.length > 0) {
      const s = this.sites[this.tabSiteSelectedIndex]
      this.site = s;
    }
  }

  removeSites() {

    if (this.sites.length > 0) {

      this.sites.splice(this.tabSiteSelectedIndex, 1);
    }
  }

  loadHousingPrograms() {
    this.siteAdminservice.getHousingPrograms()
      .subscribe(
        res => {
          this.agencyTypes = res.body as housingProgram[];
        },
        error => console.error('Error!', error)
      );
  }

  isCapsMandateSelection(issiteActive: boolean) {


  }

  SubmitPage() {
    this.proceed();
  }

  // Previous button click
  exitPage() {

    this.router.navigate(['/admin/agency-site-maintenance']);

  }
  onisAddressSameasAgency(event: { checked: Boolean }) {
    if (event.checked) {
      this.getAgencyinfo(this.site.agencyID)

    }
    else {

    }
  }
  getAgencyinfo(agid: number) {
    if (this.site.agencyID !== null) {
      var itm = this.agencyList.find(x => x.agencyID === agid) as AgencyData

      if (itm !== null) {
        this.site.address = itm.agencyAddress.trim();
        this.site.city = itm.city;
        this.site.state = itm.state;
        this.site.zip = itm.zip;
        this.site.housingProgram = itm.agencyType;

      }
    }

  }
  setSites(i: number) {
    if (this.sites.length > 0) {
      const s = this.sites[i];
      this.site = s;
      this.disablesiteinformation();
    }
  }
  private _filter(value: string): AgencyData[] {
    const filterValue = value.toLowerCase();
    return this.agencyList.filter(agency => agency.name.toLowerCase().includes(filterValue) || agency.agencyNo.includes(filterValue));
  }
  refreshAgency(id: number)
  {
    this.site.agencyID= id;
  }

}
