import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, AbstractControl, FormControl, FormArray } from '@angular/forms';
import { CommonService } from '../../../../services/helper-services/common.service';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { SiteAdminService } from '../site-admin.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';
import { AgencySiteData, AgencyData, housingProgram, SiteContact, siteDemographic, UserOptions, siteApproval } from "../agency-site-model"
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { SiteDemographicComponent } from './site-demographic.component';
import { SiteApprovalComponent } from './site-approval.component';
import { SiteDetailComponent } from './site-detail.component';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { SiteMoreContactsDialogComponent } from './site-more-contacts-dialog.component';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-site-profile',
  templateUrl: './site-profile.component.html',
  styleUrls: ['./site-profile.component.scss'],
})
export class SiteProfileComponent implements OnInit {
  siteSelectedDetail: AgencySiteData;
  siteDemographicdata: siteDemographic;
  siteRADemographicdata: siteDemographic
  siteApprovaldata: siteApproval;
  agencyInfo: AgencyData;
  siteId: number;
  agencyId: number;
  agencyList: AgencyData[];
  siteContacts: SiteContact[];
  siteFeaturedata: Array<number>;
  allcontacts: string;
  siteStatus: siteDemographic;
  siteSelectedSub: Subscription;

  useroptions: UserOptions = {
    RA: false,
    isPE: false,
    isHP: false,
    isIH: false,
    isDisableForHP: false,
    isDisableForInactiveSite: false,
    isdisableActiveRadiobutton: false,
    isActive: false,
    optionUserId: 0,
    isNewPage: false,
  }

  tabSelectedIndex: number = 0;
  tabRASelectedIndex: number = 0;
  userData: AuthData;
  userDataSub: Subscription;
  isDisableField;
  submitted: boolean = false;
  isSaveRequired: boolean = true;
  primaryContact: string = '';
  hasMorecontacts: boolean = false;

  siteDemographicDataTabStatus = 2;
  siteContactTabStatus = 2;
  siteDraftStatus = 2;
  siteApprovalTabStatus = 2
  sitePrimaryServiceTabStatus = 2;
  siteUnitDetailTabStatus = 2;
  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;
  SelectetdSiteCategorytype: number = 5;
  siteDemographicvaluechanged: boolean = false;
  siteApprovalValueChanged: boolean = false;
  siteDetailvalueChanged: boolean = false;


  @ViewChild(SiteDemographicComponent) sitedemographicComponent: SiteDemographicComponent;
  @ViewChild(SiteApprovalComponent) siteApprovalComponent: SiteApprovalComponent;
  @ViewChild(SiteDetailComponent) siteDetailComponent: SiteDetailComponent;




  constructor(
    private sidenavService: NavService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private siteadminservice: SiteAdminService,
    private commonService: CommonService,
    private userService: UserService,
    private confirmDialogService: ConfirmDialogService,
    private toastr: ToastrService,
    public dialog: MatDialog

  ) {

    this.siteDemographicdata = {} as siteDemographic;
    this.siteRADemographicdata = {} as siteDemographic;
    this.siteApprovaldata = {} as siteApproval;
    this.agencyInfo = {} as AgencyData;

  }

  ngOnInit() {
      this.populateSiteInforamtion();

      this.activatedRoute.queryParams.subscribe(params => {
        this.tabSelectedIndex = Number(params.tabSelectedIndex) > 0 ? Number(params.tabSelectedIndex) : 0;
      });
  }

  getUserRolesInfo() {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        if (this.userData.siteCategoryType.length > 0) {
          this.useroptions.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
          this.useroptions.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
          this.useroptions.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
          this.useroptions.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
          this.useroptions.optionUserId = this.userData.optionUserId;
        }
        if (this.siteSelectedDetail.siteStatus == "InActive") {
          this.useroptions.isActive = false;
          this.useroptions.isDisableForInactiveSite = true;
          this.useroptions.isDisableForHP = true;
          if (this.useroptions.isIH == true) {
            this.useroptions.isdisableActiveRadiobutton = false;
          }
          else {
            this.useroptions.isdisableActiveRadiobutton = true;
          }
        }
        else {
          this.useroptions.isActive = true;
          if (this.useroptions.isIH == true) {
            this.useroptions.isDisableForInactiveSite = false;
            this.useroptions.isDisableForHP = false;
            this.useroptions.isdisableActiveRadiobutton = false;
          }
          else {
            this.useroptions.isDisableForInactiveSite = false;
            this.useroptions.isDisableForHP = true;
            this.useroptions.isdisableActiveRadiobutton = true;
          }
        }

      }
    }
    );
  }

  loadSiteContacts() {
    if (this.siteId) {
      this.siteadminservice.getSiteContacts(this.siteId.toString())
        .subscribe(
          res => {
            this.siteContacts = res.body as SiteContact[];
            if (this.siteContacts && this.siteContacts.length > 0) {
              this.primaryContact = this.siteContacts[0].lastName + ',' + this.siteContacts[0].firstName;
            }
            if (this.siteContacts && this.siteContacts.length > 1) {
              this.hasMorecontacts = true;
            }
            //this.getallcontacts();
          },
          error => console.error('Error!', error)
        );

    }
  }
  loadSiteApproval() {
    this.siteadminservice.getSiteApprovalData(this.siteId.toString())
      .subscribe(
        res1 => {
          this.siteApprovaldata = res1.body as siteApproval;
        },
        error => console.error('Error!', error)
      );
  }

  loadSiteDemographics() {
    this.siteadminservice.getSiteDemogramics(this.siteId.toString())
      .subscribe(
        res1 => {
          this.siteDemographicdata = res1.body as siteDemographic;
          this.siteRADemographicdata = res1.body as siteDemographic;
          this.siteFeaturedata = this.convertStringToArray(this.siteDemographicdata.siteFeatures)
          this.agencyId = this.siteDemographicdata.agencyID;
          // alert(' i am here');
          //alert(this.agencyId);
          this.siteadminservice.getAgencyList(this.agencyId.toString(), null)
            .subscribe(
              res2 => {
                this.agencyList = res2.body as AgencyData[];
                this.agencyInfo = this.agencyList.find(e => e.agencyID === this.agencyId);
              },
              error => console.error('Error!', error)
            );
        },
        error => console.error('Error!', error)
      );
  }

  populateSiteInforamtion() {
    this.siteSelectedSub = this.siteadminservice.getSiteSelected().subscribe(res => {
      this.siteSelectedDetail = res;
      if (this.siteSelectedDetail) {
        this.siteId = this.siteSelectedDetail.siteID;
        this.SelectetdSiteCategorytype = this.siteSelectedDetail.siteCategoryType;
        // alert(this.siteSelectedDetail.siteCategoryType);
        // alert(this.SelectetdSiteCategorytype);
        this.getUserRolesInfo();
        this.loadSiteDemographics();
        this.loadSiteContacts();
        this.loadSiteApproval();
        this.loadSiteStatus();
      }
      this.siteadminservice.setSiteSelected(null); // reset the value back to NULL to CLEAR
    });
  }


  loadSiteStatus() {
    this.siteadminservice.getSiteDemogramics(this.siteId.toString())
      .subscribe(
        res1 => {
          this.siteStatus = res1.body as siteDemographic;
          this.setTabStatusColor();
        },
        error => console.error('Error!', error)
      );
  }







  convertStringToArray(s: string): Array<number> {
    if (s != null) {
      var array = s.split(',').map(Number);
      return array;
    }
    else {
      return null;
    }

  }





  getmodifiedData(modifiedData) {
    this.siteDemographicdata = modifiedData;
  }

  getmodifiedRAData(modifiedData) {
    this.siteRADemographicdata = modifiedData;
  }


  OnSave(event: Event) {
    if (this.tabSelectedIndex == 0) {
      //alert(this.siteDemographicvaluechanged);
      if (this.siteDemographicvaluechanged === true) {
        this.saveDemographicData();
        this.loadSiteApproval();
      }
      else {
        this.toastr.info("There are no changes in siteDemographic to save", 'Save Info');
      }
    }
    if (this.tabSelectedIndex == 4) {
      // alert(this.siteApprovalValueChanged);
      if (this.siteApprovalValueChanged === true) {
        this.saveApprovalData();
        this.loadSiteDemographics();
      }
      else {
        this.toastr.info("There are no changes in approval to save", 'Save Info');
      }
    }
  }




  saveDemographicData() {
    var sitedemographicValidation = this.sitedemographicComponent.onSubmit();
    if (sitedemographicValidation == true) {
      this.siteadminservice.saveSiteDemographic(this.siteDemographicdata)
        .subscribe(
          data => {
            this.toastr.success("Site demographic is successfully saved", 'Save Success');
            //    this.siteApprovalComponent.populateSiteApprovalData();
            this.loadSiteApproval();
            this.loadSiteDemographics();
            this.siteDemographicvaluechanged = false;
            this.siteApprovalValueChanged = false;
            this.tabSelectedIndex = 1;
          },
          error => { this.toastr.error("Save Failed", 'Save Failed'); }
        );
    }
    else {
      this.tabSelectedIndex = 0;
    }
  }

  OnRASave(event: Event) {
    if (this.tabSelectedIndex == 0) {
      // alert(this.siteDetailvalueChanged);
      if (this.siteDetailvalueChanged === true) {
        this.saveSiteDetialData();
      }
      else {
        this.toastr.info("There are no changes in site information to save", 'Save Info');
      }

    }

  }

  saveSiteDetialData() {
    var sitedemographicValidation = this.siteDetailComponent.onSubmit();
    if (sitedemographicValidation == true) {
      this.siteadminservice.saveSiteDemographic(this.siteRADemographicdata)
        .subscribe(
          data => {
            this.toastr.success("site demographic is successfully saved", 'Save Success');
            //    this.siteApprovalComponent.populateSiteApprovalData();
            //this.loadSiteApproval();
            this.loadSiteDemographics();
            this.siteDetailvalueChanged = false;
            this.tabRASelectedIndex = 1;
          },
          error => { this.toastr.error("Save Failed", 'Save Failed'); }
        );
    }
    else {
      this.tabRASelectedIndex = 0;
    }
  }
  saveApprovalData() {
    var siteApprovalValidation = this.siteApprovalComponent.onSubmit();
    if (siteApprovalValidation == true) {
      this.siteadminservice.SaveSiteApproval(this.siteApprovaldata)
        .subscribe(
          data => {
            this.toastr.success("Site approval is successfully saved", 'Save Success');
            if (this.siteApprovaldata.isActive == false) {
              this.siteSelectedDetail.siteStatus = "InActive";
            }
            else {
              this.siteSelectedDetail.siteStatus = "Active";
            }
            this.getUserRolesInfo();
            this.loadSiteDemographics();
            this.siteDemographicvaluechanged = false;
            this.siteApprovalValueChanged = false;
            //    this.sitedemographicComponent.populateDemographicdata();
          },
          error => { this.toastr.error("Save Failed", 'Save Failed'); }
        );

    }
    else {
      this.tabSelectedIndex = 4;
    }
  }

  nextPage() {
    if (this.tabSelectedIndex < 4) {
      if (this.tabSelectedIndex === 0 && this.siteDemographicvaluechanged === true) {
        this.OnSave(null);
      }
      else
      {
        if (this.useroptions.isHP === true)
        {
          if (this.tabSelectedIndex === 3 && this.siteUnitDetailTabStatus !== 1 )
          {
            this.toastr.warning("Unit details are mandatory, please complete the unit roster to proceed.", "Validation Error Message.");
          }
        }
        this.tabSelectedIndex = this.tabSelectedIndex + 1;
      }
    }
  }

  // Previous button click
  previousPage() {

    if (this.tabSelectedIndex !== 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }
  exitPage() {
    this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
  }

  nextRAPage() {
    if (this.tabRASelectedIndex < 1) {
      if (this.tabRASelectedIndex === 0 && this.siteDetailvalueChanged === true) {
        this.OnRASave(null);
      }
      else
        this.tabRASelectedIndex = this.tabRASelectedIndex + 1;
    }
  }


  // Previous button click
  previousRAPage() {

    if (this.tabRASelectedIndex !== 0) {
      this.tabRASelectedIndex = this.tabRASelectedIndex - 1;
    }

  }
  exitRAPage() {
    this.router.navigate(['./admin/agency-site-maintenance', this.agencyId]);
  }


  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    // /* Saving Data Between Tab Navigation */
    // if (this.dvGroup.touched || this.dvGroup.dirty) {
    //   this.onSave(false);
    // }
    this.tabSelectedIndex = tabChangeEvent.index;
    this.loadSiteStatus();
  }

  tabRAChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.tabRASelectedIndex = tabChangeEvent.index;
    this.loadSiteStatus();

  }


  getallcontacts() {
    var keys = ['firstName', 'lastName'];
    this.allcontacts = this.obsKeysToString(this.siteContacts, keys, ',');
  }

  obsKeysToString(o, k, sep) {
    return k.map(key => o[key]).filter(v => v).join(sep);
  }


  setTabStatusColor() {
    if (this.siteStatus != null) {
      if (this.siteStatus.isSiteDemographicTabComplete != null) {
        this.siteDemographicDataTabStatus = this.siteStatus.isSiteDemographicTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isSiteContactTabComplete != null) {
        this.siteContactTabStatus = this.siteStatus.isSiteContactTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isPrimaryServiceContractTabComplete != null) {
        this.sitePrimaryServiceTabStatus = this.siteStatus.isPrimaryServiceContractTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isDraft != null) {
        this.siteDraftStatus = this.siteStatus.isDraft == true ? 0 : 1;
      }
      if (this.siteStatus.isSiteApprovalTabComplete != null) {
        this.siteApprovalTabStatus = this.siteStatus.isSiteApprovalTabComplete == true ? 1 : 0;
      }

      if (this.siteStatus.isUnitDetailTabComplete != null) {
        this.siteUnitDetailTabStatus = this.siteStatus.isUnitDetailTabComplete == true ? 1 : 0;
      }

    }

  }

  onMoreContactsClick() {
    const dialogRef = this.dialog.open(SiteMoreContactsDialogComponent, {
      width: '40%',
      panelClass: 'transparent',
      data: this.siteContacts
    });

  }

  checkIsSiteDataChanged(s: Event) {
    this.siteDemographicvaluechanged = true;


  }

  checkIsRASiteDataChanged(s: Event) {
    this.siteDetailvalueChanged = true;
  }
  checkIsApprovalDataChanged(s: Event) {
    this.siteApprovalValueChanged = true;
  }

  ngOnDestroy = () => {
    if (this.siteSelectedSub) {
      this.siteSelectedSub.unsubscribe();
    }
  }


}

