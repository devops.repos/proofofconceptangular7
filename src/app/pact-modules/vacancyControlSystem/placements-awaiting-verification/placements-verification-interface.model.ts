export interface IVCSPlacementVerification {
  pending: number;
  verificationOverdue: number;
  moveIns: number;
  moveOuts: number;
  coCMoveIns: number;
  placementVerificationList: IVCSPlacementVerificationList[];
}

export interface IVCSPlacementVerificationList {
  hpAgencyID: number;
  hpAgencyNo: string;
  hpSiteID: number;
  hpSiteNo: string;
  hpAgencyName: string;
  hpSiteName: string;
  vcsid: string;
  clientID: number;
  clientSourceType: number;
  referralDate: string;
  pactApplicationID: number;
  tenantFirstName: string;
  tenantLastName: string;
  coCType: number;
  coCTypeDescription: string;
  vcsPlacementMoveInID: number;
  vcsPlacementMoveOutID: number;
  vcsPlacementVerificationID: number;
  verificationAssignedToID: number;
  verificationAssignedTo: string;
  moveInDate: string;
  moveOutDate: string;
  verificationStatusType: number;
  verificationStatusDescription: string;
  verificationReasonType: number;
  verificationReasonDescription: string;
  submittedID: number;
  submittedBy: string;
  submittedDate: string;
  verifiedID: number;
  verifiedBy: string;
  verifiedDate: string;
  isPlacementAutoVerified: boolean;
  applicableTab: number;
}

export interface IVCSClientPlacementsHistory {
  clientID: number;
  vcsid: number;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  gender: string;
  hasPriorPlacements: boolean;
  hasActiveApproval: boolean;
  clientPlacementHistoryList: IVCSClientPlacementsHistoryList[];
}

export interface IVCSClientPlacementsHistoryList {
  hpAgencyID: number;
  hpAgencyNo: string;
  hpSiteID: number;
  hpSiteNo: string;
  hpAgencyName: string;
  hpSiteName: string;
  siteTrackedType: number;
  raAgencyID: number;
  raAgencyNo: string;
  raSiteID: number;
  raSiteNo: string;
  raAgencyName: string;
  raSiteName: string;
  vcsid: string;
  clientID: number;
  clientSourceType: number;
  referralDate: string;
  tenantFirstName: string;
  tenantLastName: string;
  svaPrioritization: string;
  serviceNeeds: string;
  eligibility: string;
  vcsPlacementMoveInID: number;
  vcsPlacementMoveOutID: number;
  vcsPlacementVerificationID: number;
  unitID: number;
  unitName: string;
  primaryFundingSource: string;
  verificationAssignedToID: number;
  verificationAssignedTo: string;
  moveInDate: string;
  moveOutDate: string;
  moveOutReasonType: number;
  moveOutReasonDescription: string;
  moveOutLocationType: number;
  moveOutLocationDescription: string;
  placementNo: number;
  placementType: number;
  placementTypeDesription: string;
  moveOutType: number;
  moveOutTypeDescription: string;
  moveToAgencyID: number;
  moveToAgencyNo: string;
  moveToAgencyName: string;
  moveToAgencyOtherSpecify: string;
  verificationStatusType: number;
  verificationStatusDescription: string;
  verificationReasonType: number;
  verificationReasonDescription: string;
  submittedID: number;
  submittedBy: string;
  submittedDate: string;
  originalSourceType: number;
  originalSourceDescription: string;
  originalSourceEnteredID: number;
  originalSourceEnteredBy: string;
  originalSourceEnteredDate: string;
  sourceType: number;
  sourceDescription: string;
  sourceEnteredID: number;
  sourceEnteredBy: string;
  sourceEnteredDate: string;
  verificationSourceType: number;
  verificationSourceDescription: string;
  verificationSourceVerifiedID: number;
  verificationSourceVerifiedBy: string;
  verificationSourceVerifiedDate: string;
  verificationUpdatedID: number;
  verificationUpdatedBy: string;
  verificationUpdatedDate: string;
  moveInComments: string;
  moveOutComments: string;
  placementVerificationComments: string;
  actionID: number;
  isPlacementAutoVerified: boolean;
  isVacancyMonitorRequired: boolean;
  vcuAdminVerificationStatus: number;
  isDeleted: boolean;
  reviewerDateVerified: string;
}
export interface IVCSPlacementClientSelected {
  clientID: number;
  clientSourceType: number;
  isPendingVerificationRequired: boolean;
}

export interface IVCSUpdatePlacementVerification {
  placementOutcomeType: number;
  placementOutcomeID: number;
  placementVerificationType: number;
  placementVerificationID: number;
  placementType: number;
  siteID: number;
  originalSourceType: number;
  moveOutSourceType : number;
  verificationSourceType: number;
  verificationStatusType: number;
  placementOutcomeDate: string;
  placementOutcomeComments: string;
  placementVerificationComments: string;
  moveOutLocationType: number;
  moveOutReasonType: number;
  moveOutType: number;
  userID: number;
  reviewerDateVerified: string;
}

export interface IVCSPlacementVerificationOutput {
  vcsPlacementMoveInID: number;
  vcsPlacementVerificationID: number;
  moveInDate: string;
  placementNo: number;
  isFollowUpRequired: boolean;
  hasActivePlacement: boolean;
  wasActivePlacement: boolean;
}

export interface IVCSHpAgenciesForPlacementVerification {
  agencyID: number;
  agencyNo: string;
  agencyName: string;
  siteTrackedType: number;
}

export interface IVCSHpSitesForPlacementVerification {
  siteID: number;
  siteNo: string;
  agencyID: number;
  siteName: string;
  siteTrackedType: number;
}

export interface IVCSPlacementHistoryClientSearch {
  clientID?: number;
  clientSourceType?: number;
  clientSourceDescription?: string;
  vcsid?: string;
  firstName?: string;
  lastName?: string;
  ssn?: string;
  dob?: string;
  genderType?: number;
  gender?: string;
  cin?: number;
  referralDateFrom?: string;
  referralDateTo?: string;
}

export interface IVCSDeletePlacement {
  vcsPlacementVerificationID: number;
  comments: string;
}

export interface IVCSClientInfo {
  pactClientID: number;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
  referralDate: string;
  pactApplicationID: number;
  referringSiteID: number;
  referringSiteNo: string;
  referringSiteName: string;
  referringAgencyID: number;
  referringAgencyNo: string;
  referringAgencyName: string;
  capsClientNo: number;
  createdBy: string;
  createdDate: string;
  approvalExpiryDate: string;
}

export interface IVCSClientData {
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
  pactClientId: number;
  referralDate: string;
};

export interface IVCSClientDocumentsData {
  agencyNumber: string;
  siteNumber: string;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
  pactClientId: number;
  approvalExpiryDate: string;
  pactApplicationId: number;
};

export interface verificationInput {
  ApplicableTab: number;
}
