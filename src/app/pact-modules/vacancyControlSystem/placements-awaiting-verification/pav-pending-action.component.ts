import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IVCSPlacementVerificationList } from './placements-verification-interface.model';

@Component({
  selector: 'app-pav-pending-action',
  template: `
    <mat-icon
      class="pav-pending-icon"
      color="warn"
      [matMenuTriggerFor]="pavPendingAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #pavPendingAction="matMenu">
      <button mat-menu-item (click)="onVerifyClick()">
        <mat-icon color="primary">assignment</mat-icon>Verify
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .pav-pending-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class PAVPendingActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  pavSelected: IVCSPlacementVerificationList;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onVerifyClick() {
    this.pavSelected = this.params.data;
    this.params.context.componentParent.onActionClickParent(this.pavSelected);
   }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
  }
}
