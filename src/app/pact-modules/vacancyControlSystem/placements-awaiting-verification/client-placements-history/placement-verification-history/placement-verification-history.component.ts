import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { GridOptions } from 'ag-grid-community';
import { IVCSClientPlacementsHistoryList, IVCSPlacementClientSelected } from '../../placements-verification-interface.model';
import { Subscription } from 'rxjs';
import { PlacementsVerificationService } from '../../placements-verification.service';
import { Router } from '@angular/router';
import { PageSize } from 'src/app/models/pact-enums.enum';
import { CommonService } from 'src/app/services/helper-services/common.service';

@Component({
  selector: 'app-placement-verification-history',
  templateUrl: './placement-verification-history.component.html',
  styleUrls: ['./placement-verification-history.component.scss']
})
export class PlacementVerificationHistoryComponent implements OnInit, OnDestroy {

  placementClientSelected: IVCSPlacementClientSelected = {
    clientID: 0,
    clientSourceType: 0,
    isPendingVerificationRequired: false
  }

  pvhGridApi;
  pvhGridColumnApi;
  pvhColumnDefs;
  rowClassRules;
  defaultColDef;
  autoGroupColumnDef;
  frameworkComponents;
  context;


  public pvhGridOptions: GridOptions;
  pvhRowData: IVCSClientPlacementsHistoryList[] = [];

  pageSizes = PageSize;

  placementVerificationSelectedSub: Subscription;
  pvhRowDataSub: Subscription;

  constructor(
    private placementsVerificationService: PlacementsVerificationService,
    private dialogRef: MatDialogRef<PlacementVerificationHistoryComponent>,
    private router: Router,
    private commonService: CommonService
  ) {
    this.pvhGridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.pvhColumnDefs = [
      {
        headerName: 'Client#-Referral Date',
        field: 'clientNoReferralDate',
        rowGroup: true,
        hide: true,
        valueGetter(params) {
          if(params.data.clientSourceType == 573) {
            return 'Supportive Housing Client#-Referral Date: ' + params.data.clientID + ' - ' + params.data.referralDate;
          } else if(params.data.clientSourceType == 574) {
            return 'Community Housing Client#-Referral Date: ' + params.data.clientID + ' - ' + params.data.referralDate;
          }
        }
      },
      {
        headerName: 'Plac#',
        field: 'placementNo',
        width: 120,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Type',
        field: 'placementTypeDesription',
        width: 160,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Agency',
        field: 'agency',
        width: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.hpAgencyNo && params.data.hpAgencyName) {
            return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Site',
        field: 'site',
        width: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.hpSiteNo && params.data.hpSiteName) {
            return params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
          } else {
            return '';
          }
        },
      },
      {
        headerName: 'Move-In',
        field: 'moveInDate',
        width: 130,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Move-Out',
        field: 'moveOutDate',
        width: 140,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Reason Moved ',
        field: 'moveOutReasonDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Location Moved To',
        field: 'moveOutLocationDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Verification Source',
        field: 'verificationSourceDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Reviewed By',
        field: 'verificationUpdatedBy',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Reviewed Date',
        field: 'verificationUpdatedDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Reviewer Comments',
        field: 'placementVerificationComments',
        filter: 'agTextColumnFilter'
      },
    ];
    this.defaultColDef = {
      flex: 1,
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.rowClassRules = {
      'grid-gray-row': 'data.isDeleted == true',
    };
    this.context = { componentParent: this };

    this.frameworkComponents = {
    };
  }

  ngOnInit() {
    /* Getting the selected Placement Verification */
    this.placementVerificationSelectedSub = this.placementsVerificationService.getPlacementClientSelected().subscribe((res: IVCSPlacementClientSelected) => {
      if (res) {
        this.placementClientSelected = res;
      } else {
        this.router.navigate(['/vcs/placements-awaiting-verification']);
      }

    });
  }

    //On Page Size Changed
    onPageSizeChanged(selectedPageSize: any) {
      this.pvhGridOptions.api.paginationSetPageSize(Number(selectedPageSize));
      this.pvhGridOptions.api.refreshHeader();
    }

  onPVHGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.pvhGridApi = params.api;
    this.pvhGridColumnApi = params.columnApi;

    // const allColumnIds = [];
    // this.pvhGridColumnApi.getAllColumns().forEach(column => {
    //   if (column.colId !== 'action') {
    //     if (column.colId !== 'clientNoReferralDate') {
    //       allColumnIds.push(column.colId);
    //     }
    //   }
    // });
    // this.pvhGridColumnApi.autoSizeColumns(allColumnIds);
    this.pvhGridApi.sizeColumnsToFit();
    // params.api.expandAll();

    if (this.placementClientSelected.clientID > 0) {
      this.pvhRowDataSub = this.placementsVerificationService.getPlacementVerificationHistoryData(this.placementClientSelected).subscribe((res: IVCSClientPlacementsHistoryList[]) => {
        if (res) {
          this.pvhRowData = res;
          // console.log(this.pvhRowData);
        }
      });
    }
  }

  ClosePlacementVerificationHistoryDialog() {
    this.dialogRef.close();
  }

  refreshAgGrid() {
    this.pvhGridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'PlacementVerificationHistoryReport-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.pvhGridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.placementVerificationSelectedSub) {
      this.placementVerificationSelectedSub.unsubscribe();
    }
    if (this.pvhRowDataSub) {
      this.pvhRowDataSub.unsubscribe();
    }
  }

}
