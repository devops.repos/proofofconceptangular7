import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { Subscription } from 'rxjs';
import { PlacementsVerificationService } from '../placements-verification.service';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-vcu-info-icon',
  template: `
    <span *ngIf="!isDeletePlacement">{{params.value}}</span>
    <span *ngIf="isDeletePlacement" class="v-info-icon">
        <mat-icon class="vcu-info-icon" matTooltip="{{tooltipMessage}}" matTooltipClass="vcs-info-tooltip">info</mat-icon>{{params.value}}
    </span>
  `,
  styles: [`
    .v-info-icon {
      cursor: pointer;
    }
  `]
})
export class VCUInfoIconComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  tooltipMessage: string = '';
  isDeletePlacement = false;
  isVCU: string = '';
  inVCUQueue: string = '';

  isDeletePlacementSub: Subscription;
  constructor(
    private placementVerificationService: PlacementsVerificationService
  ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    this.params.data.isVacancyMonitorRequired ? this.isVCU = 'Yes' : this.isVCU = 'No';
    this.params.data.vcuAdminVerificationStatus == 606 ? this.inVCUQueue = 'Pending'
    : this.params.data.vcuAdminVerificationStatus == 607 ? this.inVCUQueue = 'Offline Verified'
    : this.params.data.vcuAdminVerificationStatus == 608 ? this.inVCUQueue = 'Online Verified'
    : this.inVCUQueue = 'n/a';
    this.tooltipMessage = `VCU Unit: ` + this.isVCU + `\nVCU Monitor Queue Status: ` + this.inVCUQueue;


    /* Get IsDeletePlacement flag to know if the workflow is for Delete Placement */
    this.isDeletePlacementSub = this.placementVerificationService.getIsDeletePlacement().subscribe(flag => {
      if (flag) {
        this.isDeletePlacement = flag;
      }
    });
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.isDeletePlacementSub) {
      this.isDeletePlacementSub.unsubscribe();
    }
  }
}
