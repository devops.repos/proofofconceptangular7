import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IVCSClientPlacementsHistoryList } from '../placements-verification-interface.model';
import { PlacementsVerificationService } from '../placements-verification.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cph-action',
  template: `
    <mat-icon
      class="cph-icon"
      color="warn"
      [matMenuTriggerFor]="cphAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #cphAction="matMenu">
      <button mat-menu-item *ngIf="params.data.actionID == 1" (click)="onActionButtonClick()">
        <mat-icon color="primary">assignment</mat-icon>Verify
      </button>
      <button mat-menu-item *ngIf="params.data.actionID == 2 && !isDeletePlacement" (click)="onActionButtonClick()">
        <mat-icon color="primary">assignment</mat-icon>Make Correction
      </button>
      <button mat-menu-item *ngIf="params.data.actionID == 2 && isDeletePlacement" (click)="onDeletePlacementClick()">
        <mat-icon color="primary">assignment</mat-icon>Delete Placement
      </button>
      <button mat-menu-item *ngIf="params.data.actionID == 3" (click)="onActionButtonClick()">
        <mat-icon color="primary">assignment</mat-icon>View
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .cph-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class CPHActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  cphSelected: IVCSClientPlacementsHistoryList;
  isDeletePlacement = false;

  isDeletePlacementSub: Subscription;

  constructor(
    private placementVerificationService: PlacementsVerificationService
  ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    /* Get IsDeletePlacement flag to know if the workflow is for Delete Placement */
    this.isDeletePlacementSub = this.placementVerificationService.getIsDeletePlacement().subscribe(flag => {
      if (flag) {
        this.isDeletePlacement = flag;
      }
    });
  }

  onActionButtonClick() {
    this.cphSelected = this.params.data;
    this.params.context.componentParent.onActionButtonClickParent(this.cphSelected);
  }

  onDeletePlacementClick() {
    this.cphSelected = this.params.data;
    this.params.context.componentParent.onDeletePlacementClickParent(this.cphSelected);
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.isDeletePlacementSub) {
      this.isDeletePlacementSub.unsubscribe();
    }
  }
}
