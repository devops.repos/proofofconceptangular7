import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { IVCSClientPlacementsHistory, IVCSClientPlacementsHistoryList, IVCSUpdatePlacementVerification, IVCSPlacementVerificationOutput, IVCSHpAgenciesForPlacementVerification, IVCSHpSitesForPlacementVerification, IVCSPlacementClientSelected, IVCSDeletePlacement } from '../placements-verification-interface.model';
import { GridOptions } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { PlacementsVerificationService } from '../placements-verification.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CPHActionComponent } from './cph-action.component';
import { MatTabChangeEvent, MatDialog } from '@angular/material';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { IVCSMoveInValidator } from '../../tenant-roster/tenant-roster-interface.model';
import { TenantRosterService } from '../../tenant-roster/tenant-roster.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { DatePipe } from '@angular/common';
import { ReferralRosterService } from '../../referral-roster/referral-roster.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { PlacementVerificationHistoryComponent } from './placement-verification-history/placement-verification-history.component';
import { TadService } from '../../tad-submission/tad.service';
import { DeletePlacementDialogComponent } from './delete-placement-dialog/delete-placement-dialog.component';
import { VCUInfoIconComponent } from './vcu-info-icon.component';

@Component({
  selector: 'app-client-placements-history',
  templateUrl: './client-placements-history.component.html',
  styleUrls: ['./client-placements-history.component.scss'],
  providers: [DatePipe]
})
export class ClientPlacementsHistoryComponent implements OnInit, OnDestroy {
  @ViewChild('clientPlacementsHistoryAgGrid') clientPlacementsHistoryAgGrid: AgGridAngular;

  currentUser: AuthData;
  refData: RefGroupDetails[];
  placementPanelExpanded = true;
  moveInPanelExpanded = true;
  moveOutPanelExpanded = true;
  reviewerPanelExpanded = false;

  placementClientSelected: IVCSPlacementClientSelected = {
    clientID: 0,
    clientSourceType: 0,
    isPendingVerificationRequired: false
  }
  pactClientID = 0;
  vcsid = 0;
  firstName = '';
  lastName = '';
  ssn = '';
  dob = '';
  gender = '';
  hasPriorPlacements = false;
  hasActiveApproval = false;
  selectedTab = 0;
  placementVerificationGroup: FormGroup;

  clientPlacementsGridApi;
  clientPlacementsGridColumnApi;
  clientPlacementsHistoryColumnDefs;
  rowClassRules;
  defaultColDef;
  frameworkComponents;
  context;

  public cpGridOptions: GridOptions;
  clientPlacementsHistoryRowData: IVCSClientPlacementsHistoryList[] = [];

  cphSelected: IVCSClientPlacementsHistoryList = {
    hpAgencyID: 0,
    hpAgencyNo: '',
    hpSiteID: 0,
    hpSiteNo: '',
    hpAgencyName: '',
    hpSiteName: '',
    siteTrackedType: 0,
    raAgencyID: 0,
    raAgencyNo: '',
    raSiteID: 0,
    raSiteNo: '',
    raAgencyName: '',
    raSiteName: '',
    vcsid: '',
    clientID: 0,
    clientSourceType: 0,
    referralDate: '',
    tenantFirstName: '',
    tenantLastName: '',
    svaPrioritization: '',
    serviceNeeds: '',
    eligibility: '',
    vcsPlacementMoveInID: 0,
    vcsPlacementMoveOutID: 0,
    vcsPlacementVerificationID: 0,
    unitID: 0,
    unitName: '',
    primaryFundingSource: '',
    verificationAssignedToID: 0,
    verificationAssignedTo: '',
    moveInDate: '',
    moveOutDate: '',
    moveOutReasonType: 0,
    moveOutReasonDescription: '',
    moveOutLocationType: 0,
    moveOutLocationDescription: '',
    placementNo: 0,
    placementType: 0,
    placementTypeDesription: '',
    moveOutType: 0,
    moveOutTypeDescription: '',
    moveToAgencyID: 0,
    moveToAgencyNo: '',
    moveToAgencyName: '',
    moveToAgencyOtherSpecify: '',
    verificationStatusType: 0,
    verificationStatusDescription: '',
    verificationReasonType: 0,
    verificationReasonDescription: '',
    submittedID: 0,
    submittedBy: '',
    submittedDate: '',
    originalSourceType: 0,
    originalSourceDescription: '',
    originalSourceEnteredID: 0,
    originalSourceEnteredBy: '',
    originalSourceEnteredDate: '',
    sourceType: 0,
    sourceDescription: '',
    sourceEnteredID: 0,
    sourceEnteredBy: '',
    sourceEnteredDate: '',
    verificationSourceType: 0,
    verificationSourceDescription: '',
    verificationSourceVerifiedID: 0,
    verificationSourceVerifiedBy: '',
    verificationSourceVerifiedDate: '',
    verificationUpdatedID: 0,
    verificationUpdatedBy: '',
    verificationUpdatedDate: '',
    moveInComments: '',
    moveOutComments: '',
    placementVerificationComments: '',
    actionID: 0,
    isPlacementAutoVerified: false,
    isVacancyMonitorRequired: false,
    vcuAdminVerificationStatus: 0,
    isDeleted: false,
    reviewerDateVerified: ''
  }
  isPlacementTabOpen = false;
  placementTabNumber = 0;
  hpAgencies: IVCSHpAgenciesForPlacementVerification[] = [];
  AllHpAgencies: IVCSHpAgenciesForPlacementVerification[] = [];
  hpSiteList: IVCSHpSitesForPlacementVerification[] = [];
  isReviewerCommentMandatory = false;
  moveInDateChanged = false;
  moveOutDateChanged = false;
  reasonMovedChanged = false;
  locationMoveToChanged = false;
  case3Flag = false;
  wasFormReset = false;
  isFollowUpMoveIn = false;
  followUpMoveInDate = '';
  // containsProviderAgency = false;

  moveOutType: RefGroupDetails[];
  moveOutLocationType: RefGroupDetails[];
  moveOutReasonType: RefGroupDetails[];
  placementType: RefGroupDetails[];
  originalSourceType: RefGroupDetails[];
  verificationSourceType: RefGroupDetails[];

  hpAgToolTipMsg = '';
  hpStToolTipMsg = '';
  reasonMovedToolTipMsg = '';
  locationMovedToolTipMsg = '';
  agencyMovedToolTipMsg = '';
  primaryServiceContractToolTipMsg = '';

  isTadVerificationWorkflow = false;
  isClientPlacements = false;
  isDeletePlacement = false;

  currentUserSub: Subscription;
  placementVerificationSelectedSub: Subscription;
  clientPlacementsHistoryRowDataSub: Subscription;
  commonServiceSub: Subscription;
  // raHpAgenciesSub: Subscription;
  agencyNameCtrlSub: Subscription;
  siteNameCtrlSub: Subscription;
  hpAgenciesSub: Subscription;
  hpSiteListSub: Subscription;
  placementTypeCtrlSub: Subscription;
  moveOutTypeCtrlSub: Subscription;
  reasonMovedCtrlSub: Subscription;
  locationMoveToCtrlSub: Subscription;
  // isPlacementTabOpenSub: Subscription;
  // placementTabNumberSub: Subscription;
  // cphSelectedSub: Subscription;
  moveInValidatorSub: Subscription;
  istadVerificationWorkflowSub: Subscription;
  routeSub: Subscription;
  isClientPlacementsSub: Subscription;
  isDeletePlacementSub: Subscription;

  constructor(
    private userService: UserService,
    private placementsVerificationService: PlacementsVerificationService,
    private toastr: ToastrService,
    private router: Router,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private tenantRosterService: TenantRosterService,
    private confirmDialogService: ConfirmDialogService,
    private datePipe: DatePipe,
    private referralRosterService: ReferralRosterService,
    private dialog: MatDialog,
    private tadService: TadService,

  ) {
    this.placementVerificationGroup = this.formBuilder.group({
      placementNoCtrl: ['0'],
      placementTypeCtrl: [-1, Validators.required],
      moveInDateCtrl: ['', Validators.required],
      agencyNameCtrl: [-1, Validators.required],
      siteNameCtrl: [-1, Validators.required],
      unitNoCtrl: [''],
      primaryServiceContractTypeCtrl: [''],
      moveInCommentsCtrl: [''],
      moveOutDateCtrl: ['', Validators.required],
      reasonMovedCtrl: [-1, Validators.required],
      locationMoveToCtrl: [-1, Validators.required],
      moveOutTypeCtrl: [-1, Validators.required],
      agencyMovedToCtrl: ['', Validators.required],
      agencyMovedToOtherSpecifyCtrl: ['', Validators.required],
      moveOutCommentsCtrl: [''],
      originalSourceCtrl: [724, Validators.required],
      sourceCtrl: [724, Validators.required],
      verificationSourceCtrl: [724, Validators.required],
      osDateEnteredCtrl: [],
      osEnteredByCtrl: [],
      sDateEnteredCtrl: [],
      sEnteredByCtrl: [],
      dateVerifiedCtrl: [],
      reviewerDateVerifiedCtrl:[],
      verifiedByCtrl: [],
      reviewerCommentsCtrl: ['', Validators.required]

    });
    this.cpGridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.clientPlacementsHistoryColumnDefs = [
      {
        headerName: 'Client#-Referral Date',
        field: 'clientNoReferralDate',
        rowGroup: true,
        hide: true,
        valueGetter(params) {
          if(params.data.clientSourceType == 573) {
            return 'Supportive Housing Client#-Referral Date: ' + params.data.clientID + ' - ' + params.data.referralDate;
          } else if(params.data.clientSourceType == 574) {
            return 'Community Housing Client#-Referral Date: ' + params.data.clientID + ' - ' + params.data.referralDate;
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 100,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'cpActionRenderer'
      },
      {
        headerName: 'Plac#',
        field: 'placementNo',
        width: 120,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Type',
        field: 'placementTypeDesription',
        width: 160,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Agency',
        field: 'agency',
        width: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.hpAgencyNo && params.data.hpAgencyName) {
            return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Site',
        field: 'site',
        width: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.hpSiteNo && params.data.hpSiteName) {
            return params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
          } else {
            return '';
          }
        },
        cellRenderer: 'infoIconRenderer'
        // cellRendererSelector(params) {
        //   const vcuInfoIcon = {
        //       component: 'infoIconRenderer'
        //   };
        //   if (this.isDeletePlacement) {
        //       return vcuInfoIcon;
        //   } else {
        //       return null;
        //   }
        // }
      },
      {
        headerName: 'Move-In',
        field: 'moveInDate',
        width: 130,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Move-Out',
        field: 'moveOutDate',
        width: 140,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Reason Moved ',
        field: 'moveOutReasonDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Location Moved To',
        field: 'moveOutLocationDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Updated By',
        field: 'verificationUpdatedBy',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Updated Date',
        field: 'verificationUpdatedDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
    ];
    this.defaultColDef = {
      flex: 1,
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.rowClassRules = {
      'verify-row-bg': 'data.actionID == 1 || (data.actionID == 2 && (data.verificationStatusType == 520 || data.verificationStatusType == 758) && (data.hpSiteID <= 0 || data.hpSiteID == null))',
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      cpActionRenderer: CPHActionComponent,
      infoIconRenderer: VCUInfoIconComponent
    };
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '80,81,82,84,90';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        this.refData = res.body as RefGroupDetails[];
        this.moveOutType = this.refData.filter(d => d.refGroupID === 80);
        this.moveOutLocationType = this.refData.filter(d => d.refGroupID === 81);
        this.moveOutReasonType = this.refData.filter(d => d.refGroupID === 82);
        this.placementType = this.refData.filter(d => d.refGroupID === 84);
        this.originalSourceType = this.refData.filter(d => d.refGroupID === 90);

      },
      error => {
        throw new Error(error.message);
      }
    );

    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
      }
    });
    /* Getting the selected Placement Verification */
    this.placementVerificationSelectedSub = this.placementsVerificationService.getPlacementClientSelected().subscribe((res: IVCSPlacementClientSelected) => {
      if (res) {
        this.placementClientSelected = res;
      } else {
        this.router.navigate(['/vcs/placements-awaiting-verification']);
      }

    });

    /** Get all the active untracked HP Agencies List for the ReferralReceivedFrom dropdown */
    this.hpAgenciesSub = this.placementsVerificationService.GetHpAgenciesForPlacementVerification().subscribe((agencies: IVCSHpAgenciesForPlacementVerification[]) => {
      if (agencies) {
        this.hpAgencies = agencies;
        this.AllHpAgencies = agencies;
      }
    });

    /* Get IsClientPlacements flag to know if the workflow is for ClientPlacements from Dashboard to redirect bck to Dashboard on exit */
    this.isClientPlacementsSub = this.placementsVerificationService.getIsClientPlacements().subscribe(flag => {
      if (flag) {
        this.isClientPlacements = flag;
      }
    });
    /* Get IsDeletePlacement flag to know if the workflow is for Delete Placement */
    this.isDeletePlacementSub = this.placementsVerificationService.getIsDeletePlacement().subscribe(flag => {
      if (flag) {
        this.isDeletePlacement = flag;
      }
    });

    // this.placementTypeCtrlSub = this.placementVerificationGroup.get('placementTypeCtrl').valueChanges.subscribe(res => {
    //   if (res == 662 && (this.cphSelected.verificationStatusType == 758 || this.cphSelected.verificationStatusType == 520)) {
    //     /* PlacementType is Continuous(662) and selected record is FollowUp MoveIn(758) or MoveInVerified (520) */
    //     const hpAg = this.AllHpAgencies;
    //     this.hpAgencies = [];
    //     this.hpSiteList = [];
    //     if (this.cphSelected.actionID !== 3) {
    //       this.placementVerificationGroup.get('agencyNameCtrl').setValue(0);
    //       this.placementVerificationGroup.get('siteNameCtrl').setValue(0);
    //       this.hpAgToolTipMsg = '';
    //       this.hpStToolTipMsg = '';
    //     }
    //     if (this.cphSelected.hpAgencyID > 0) {
    //       /* If AllHpAgencies doesn't contain providerAgency, we need to append it */
    //       let containsHpAgencySelected = this.AllHpAgencies.filter(d => d.agencyID === this.cphSelected.hpAgencyID);
    //       if (containsHpAgencySelected.length <= 0) {
    //         /* HpAgencies list is of non-tracked Agencies, meaning this.cphSelected.hpAgencyID is tracked Agency,
    //         Append this this.cphSelected.hpAgencyID with HpAgencies (non-tracked) */

    //         const addAgency: IVCSHpAgenciesForPlacementVerification = {
    //           agencyID: this.cphSelected.hpAgencyID,
    //           agencyNo: this.cphSelected.hpAgencyNo,
    //           agencyName: this.cphSelected.hpAgencyName,
    //           siteTrackedType: 33
    //         }
    //         this.hpAgencies.push(addAgency);
    //       }
    //     }
    //     hpAg.forEach(ag => {
    //       this.hpAgencies.push(ag);
    //     });
    //     if (this.isFollowUpMoveIn && this.followUpMoveInDate !== '') {
    //       /* append all the MoveIn Pending Verification record's HpAgencies whose MoveInDate = FollowUp-MoveInDate */
    //       this.appendPendingMoveInAgencies(this.hpAgencies, this.followUpMoveInDate);
    //       hpAg.forEach(ag => {
    //         this.hpAgencies.push(ag);
    //       });
    //     }
    //   } else if (res == 663 && (this.cphSelected.verificationStatusType == 758 || this.cphSelected.verificationStatusType == 520)) {
    //     /* PlacementType is Dis-Continuous(663) and selected record is FollowUp MoveIn(758) or MoveInVerified (520) */
    //     this.hpAgencies = [];
    //     const Agencies = this.AllHpAgencies.filter(d => (d.siteTrackedType == 34 || d.siteTrackedType == null));
    //     this.hpAgencies = Agencies;
    //     this.hpSiteList = [];
    //     // this.containsProviderAgency = true; // to remove the ProviderAgency/site from the list for Discontinue case
    //     if (this.cphSelected.actionID !== 3) {
    //       this.placementVerificationGroup.get('agencyNameCtrl').setValue(0);
    //       this.placementVerificationGroup.get('siteNameCtrl').setValue(0);
    //       this.hpAgToolTipMsg = '';
    //       this.hpStToolTipMsg = '';
    //     }
    //   }
    // });

    this.agencyNameCtrlSub = this.placementVerificationGroup.get('agencyNameCtrl').valueChanges.subscribe(res => {
      if (res > 0) {
        this.hpSiteList = [];
        this.hpAgencies.forEach(ag => {
          if (ag.agencyID == res) {
            this.hpAgToolTipMsg = ag.agencyNo + ' - ' + ag.agencyName;
          }
        });
        // var pendingHpAgencies = this.hpAgencies.filter(d => d.siteTrackedType == 33); // List of Agencies
        this.placementVerificationGroup.get('siteNameCtrl').setValue(0);
        this.hpStToolTipMsg = '';
        // console.log('agencyIDSelected res: ', res)
        this.hpSiteListSub = this.placementsVerificationService.GetHpSitesForPlacementVerification(res).subscribe((st: IVCSHpSitesForPlacementVerification[]) => {
          if (st) {
            if (this.placementVerificationGroup.get('placementTypeCtrl').value == 663) {
              this.hpSiteList = st.filter(d => (d.siteTrackedType == 34 || d.siteTrackedType == null));
              this.placementVerificationGroup.get('siteNameCtrl').setValue(this.cphSelected.hpSiteID);
            } else {
              var hpSt = st;
              this.hpSiteList = [];
              if (this.cphSelected.hpSiteID > 0) {
                /* If HpSiteList doesn't contain providerAgency, we need to append it */
                let containsHpSiteSelected = st.filter(d => d.siteID === this.cphSelected.hpSiteID);
                if (containsHpSiteSelected.length <= 0) {
                  /* HpSiteList list is of non-tracked Sites, meaning this.cphSelected.hpSiteID is tracked Site,
                  Append this this.cphSelected.hpSiteID with HpSiteList (non-tracked) */
                  const addSite: IVCSHpSitesForPlacementVerification = {
                    siteID: this.cphSelected.hpSiteID,
                    siteNo: this.cphSelected.hpSiteNo,
                    agencyID: this.cphSelected.hpAgencyID,
                    siteName: this.cphSelected.hpSiteName,
                    siteTrackedType: 33
                  }
                  if (this.cphSelected.hpAgencyID == res) {
                    this.hpSiteList.push(addSite);
                  }
                }
              }
              hpSt.forEach(s => {
                this.hpSiteList.push(s);
              });
              if (this.isFollowUpMoveIn && this.followUpMoveInDate !== '') {
                /* Checking if the Pending MoveIn Verification Agency is selected and
                If so, checking if the Pending MoveIn Verification Site is listed in hpSiteList
                If not append it */
                this.appendPendingMoveInSites(this.hpSiteList, res, this.followUpMoveInDate);
                st.forEach(s => {
                  this.hpSiteList.push(s);
                });
              } else if (this.cphSelected.siteTrackedType == 33 || this.cphSelected.actionID == 3) {
                this.placementVerificationGroup.get('siteNameCtrl').setValue(this.cphSelected.hpSiteID);
              }
            }
          } else {
            if (this.isFollowUpMoveIn && this.followUpMoveInDate !== '') {
              this.appendPendingMoveInSites(this.hpSiteList, res, this.followUpMoveInDate);
            }
          }
        });
      } else {
        this.hpSiteList = [];
        this.placementVerificationGroup.get('siteNameCtrl').setValue(0);
        this.hpStToolTipMsg = '';
      }
    });

    this.siteNameCtrlSub = this.placementVerificationGroup.get('siteNameCtrl').valueChanges.subscribe(res => {
      if (res > 0) {
        this.hpSiteList.forEach(st => {
          if (st.siteID == res) {
            this.hpStToolTipMsg = st.siteNo + ' - ' + st.siteName;
          }
        })
      }
    });

    /* Pop Up Dialog if user selects different moveOutType, ReasonMoved, LocationMoveTo from what the Provider has selected */
    this.reasonMovedCtrlSub = this.placementVerificationGroup.get('reasonMovedCtrl').valueChanges.subscribe(res => {
      if (res > 0 && !this.wasFormReset) {
        if (this.cphSelected.moveOutReasonType !== res) {
          const title = 'Verify';
          const primaryMessage = `You are updating the Reason Moved from what the provider has originally submitted.`;
          const secondaryMessage = `Do you want to continue?`;
          const confirmButtonName = 'OK';
          const dismissButtonName = 'Cancel';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              // Make reviewer comment mandatory
              this.isReviewerCommentMandatory = true;
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue('');
              /* For ToolTip */
              this.moveOutReasonType.forEach(r => {
                if (r.refGroupDetailID == res) {
                  this.reasonMovedToolTipMsg = r.refGroupDetailDescription;
                }
              });

              if (res == 639) {
                // 639 = deceased
                this.reasonMovedChanged = true;
                this.placementVerificationGroup.get('locationMoveToCtrl').setValue(634);  // 634 = Deceased
                /* For ToolTip */
                this.moveOutLocationType.forEach(l => {
                  if (l.refGroupDetailID == 634) {
                    this.locationMovedToolTipMsg = l.refGroupDetailDescription;
                  }
                });
                this.placementVerificationGroup.get('locationMoveToCtrl').disable();
                this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(617);   // 616 = planned, 617 = unplanned
                this.placementVerificationGroup.get('moveOutTypeCtrl').disable();
                this.placementVerificationGroup.get('agencyMovedToCtrl').setValue('');
                /* For ToolTip */
                this.agencyMovedToolTipMsg = '';
                this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue('');
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
              } else if (res == 643) {
                // 643 = HOSPITALIZED
                this.reasonMovedChanged = true;
                this.placementVerificationGroup.get('locationMoveToCtrl').setValue(624);  // 624 = HOSPITAL
                /* For ToolTip */
                this.moveOutLocationType.forEach(l => {
                  if (l.refGroupDetailID == 624) {
                    this.locationMovedToolTipMsg = l.refGroupDetailDescription;
                  }
                })
                this.placementVerificationGroup.get('locationMoveToCtrl').disable();
                this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(617);   // 616 = planned, 617 = unplanned
                this.placementVerificationGroup.get('moveOutTypeCtrl').disable();
                this.placementVerificationGroup.get('agencyMovedToCtrl').setValue('');
                /* For ToolTip */
                this.agencyMovedToolTipMsg = '';
                this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue('');
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
              } else {
                this.reasonMovedChanged = false;
                this.placementVerificationGroup.get('locationMoveToCtrl').setValue(this.cphSelected.moveOutLocationType);
                /* For ToolTip */
                this.moveOutLocationType.forEach(l => {
                  if (l.refGroupDetailID == this.cphSelected.moveOutLocationType) {
                    this.locationMovedToolTipMsg = l.refGroupDetailDescription;
                  }
                });
                this.placementVerificationGroup.get('locationMoveToCtrl').enable();
                this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(this.cphSelected.moveOutType);
                this.placementVerificationGroup.get('moveOutTypeCtrl').enable();
                this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
                /* For ToolTip */
                this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
                this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify);
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
              }
            },
            negativeResponse => {
              // reset the moveInDate back
              this.reasonMovedChanged = false;
              this.isReviewerCommentMandatory = false;
              this.placementVerificationGroup.get('reasonMovedCtrl').setValue(this.cphSelected.moveOutReasonType);
              /* For ToolTip */
              this.moveOutReasonType.forEach(r => {
                if (r.refGroupDetailID == this.cphSelected.moveOutReasonType) {
                  this.reasonMovedToolTipMsg = r.refGroupDetailDescription;
                }
              });
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue(this.cphSelected.placementVerificationComments);
              this.placementVerificationGroup.get('locationMoveToCtrl').setValue(this.cphSelected.moveOutLocationType);
              /* For ToolTip */
              this.moveOutLocationType.forEach(l => {
                if (l.refGroupDetailID == this.cphSelected.moveOutLocationType) {
                  this.locationMovedToolTipMsg = l.refGroupDetailDescription;
                }
              });
              this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(this.cphSelected.moveOutType);
              this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
              /* For ToolTip */
              this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
              this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
              this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify);
              this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
            }
          );
        } else {
          this.placementVerificationGroup.get('locationMoveToCtrl').setValue(this.cphSelected.moveOutLocationType);
          /* For ToolTip */
          this.moveOutLocationType.forEach(l => {
            if (l.refGroupDetailID == this.cphSelected.moveOutLocationType) {
              this.locationMovedToolTipMsg = l.refGroupDetailDescription;
            }
          });
          this.placementVerificationGroup.get('locationMoveToCtrl').enable();
          this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(this.cphSelected.moveOutType);
          this.placementVerificationGroup.get('moveOutTypeCtrl').enable();
          this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
          /* For ToolTip */
          this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
          this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
          this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify);
          this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
        }
      }
    });
    this.locationMoveToCtrlSub = this.placementVerificationGroup.get('locationMoveToCtrl').valueChanges.subscribe(res => {
      if (res > 0 && !this.wasFormReset && !this.reasonMovedChanged) {
        if (this.cphSelected.moveOutLocationType !== res) {
          const title = 'Verify';
          const primaryMessage = `You are updating the Location Moved To from what the provider has originally submitted.`;
          const secondaryMessage = `Do you want to continue?`;
          const confirmButtonName = 'OK';
          const dismissButtonName = 'Cancel';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              // Make reviewer comment mandatory
              this.isReviewerCommentMandatory = true;
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue('');

              /* For ToolTip */
              this.moveOutLocationType.forEach(l => {
                if (l.refGroupDetailID == res) {
                  this.locationMovedToolTipMsg = l.refGroupDetailDescription;
                }
              });

              if (res == 619 || res == 623 || res == 625 || res == 626 || res == 628 || res == 631 || res == 632 || res == 636) {
                this.locationMoveToChanged = true;
                this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(617); // 616 = planned, 617 = unplanned
                this.placementVerificationGroup.get('moveOutTypeCtrl').disable();
              } else if (res == 622) {
                // 622 = SUPPORTED HOUSING
                this.locationMoveToChanged = true;
                this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(616);
                this.placementVerificationGroup.get('moveOutTypeCtrl').disable();
                this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
                /* For ToolTip */
                this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
                this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify);
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
              } else {
                this.locationMoveToChanged = false;
                this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(this.cphSelected.moveOutType);
                this.placementVerificationGroup.get('moveOutTypeCtrl').enable();
                this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
                /* For ToolTip */
                this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
                this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify);
                this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
              }
            },
            negativeResponse => {
              // reset the moveInDate back
              this.locationMoveToChanged = false;
              this.isReviewerCommentMandatory = false;
              this.placementVerificationGroup.get('locationMoveToCtrl').setValue(this.cphSelected.moveOutLocationType);
              /* For ToolTip */
              this.moveOutLocationType.forEach(l => {
                if (l.refGroupDetailID == this.cphSelected.moveOutLocationType) {
                  this.locationMovedToolTipMsg = l.refGroupDetailDescription;
                }
              });
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue(this.cphSelected.placementVerificationComments);
              this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(this.cphSelected.moveOutType);
              this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
              /* For ToolTip */
              this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
              this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
              this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify);
              this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
            }
          );
        } else {
          /* For ToolTip */
          this.moveOutLocationType.forEach(l => {
            if (l.refGroupDetailID == this.cphSelected.moveOutLocationType) {
              this.locationMovedToolTipMsg = l.refGroupDetailDescription;
            }
          });
          this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(this.cphSelected.moveOutType);
          this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
          /* For ToolTip */
          this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
          this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
          this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify);
          this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
        }
      }
    });
    this.moveOutTypeCtrlSub = this.placementVerificationGroup.get('moveOutTypeCtrl').valueChanges.subscribe(res => {
      if (res > 0 && !this.wasFormReset && !this.reasonMovedChanged && !this.locationMoveToChanged) {
        if (this.cphSelected.moveOutType !== res) {
          const title = 'Verify';
          const primaryMessage = `You are updating the moveout type from what the provider has originally submitted.`;
          const secondaryMessage = `Do you want to continue?`;
          const confirmButtonName = 'OK';
          const dismissButtonName = 'Cancel';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              // Make reviewer comment mandatory
              this.isReviewerCommentMandatory = true;
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue('');
            },
            negativeResponse => {
              // reset the moveInDate back
              this.isReviewerCommentMandatory = false;
              this.placementVerificationGroup.get('moveInDateCtrl').setValue(this.cphSelected.moveOutType);
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue(this.cphSelected.placementVerificationComments);
            }
          );
        }
      }
    });

    /* Check if the workFlow is coming from TAD Verification, if so, on submit or exit, redirect back to TAD Verification workflow */
    this.istadVerificationWorkflowSub = this.tadService.getIsTadVerificationWorkflow().subscribe(flag => {
      if (flag) {
        this.isTadVerificationWorkflow = flag;
      }
    });
    // this.routeSub = this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     /* Reset the isDeletePlacement Flag */
    //     this.placementsVerificationService.setIsDeletePlacement(false);
    //   };
    // });
  }

  // @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
  //   /* Reset the isDeletePlacement Flag */
  //   this.placementsVerificationService.setIsDeletePlacement(false);
  // };


  onPlacementExpansionPanelToggle() {
    this.placementPanelExpanded = !this.placementPanelExpanded;
  }
  onMoveInExpansionPanelToggle() {
    this.moveInPanelExpanded = !this.moveInPanelExpanded;
  }
  onMoveOutExpansionPanelToggle() {
    this.moveOutPanelExpanded = !this.moveOutPanelExpanded;
  }
  onReviewerExpansionPanelToggle() {
    this.reviewerPanelExpanded = !this.reviewerPanelExpanded;
  }

  onClientPlacementsHistoryGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.clientPlacementsGridApi = params.api;
    this.clientPlacementsGridColumnApi = params.columnApi;

    // const allColumnIds = [];
    // this.clientPlacementsGridColumnApi.getAllColumns().forEach(column => {
    //   if (column.colId !== 'action') {
    //     if (column.colId !== 'clientNoReferralDate') {
    //       allColumnIds.push(column.colId);
    //     }
    //   }
    // });
    // this.clientPlacementsGridColumnApi.autoSizeColumns(allColumnIds);
    this.clientPlacementsGridApi.sizeColumnsToFit();
    // params.api.expandAll();
    this.getGridData();

  }

  getGridData() {
    if (this.placementClientSelected.clientID > 0) {
      this.clientPlacementsHistoryRowDataSub = this.placementsVerificationService.getClientPlacementsHistoryData(this.placementClientSelected).subscribe((res: IVCSClientPlacementsHistory) => {
        if (res) {
          // console.log('ClientPlacementsHistory: ', res);
          this.pactClientID = res.clientID;
          this.vcsid = res.vcsid;
          this.firstName = res.firstName;
          this.lastName = res.lastName;
          this.ssn = res.ssn;
          this.dob = res.dob;
          this.gender = res.gender;
          this.hasPriorPlacements = res.hasPriorPlacements;
          this.hasActiveApproval = res.hasActiveApproval;
          this.clientPlacementsHistoryRowData = res.clientPlacementHistoryList;
        }
      });
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;
    if (this.placementClientSelected.clientID > 0 && this.selectedTab == 0) {
      this.getGridData();
    }
  }

  onPlacementTypeChanged(event) {
    const res = event.value;
    if (res == 662 && (this.cphSelected.verificationStatusType == 758 || this.cphSelected.verificationStatusType == 520)) {
      /* PlacementType is Continuous(662) and selected record is FollowUp MoveIn(758) or MoveInVerified (520) */
      const hpAg = this.AllHpAgencies;
      this.hpAgencies = [];
      this.hpSiteList = [];
      if (this.cphSelected.actionID !== 3) {
        this.placementVerificationGroup.get('agencyNameCtrl').setValue(0);
        this.placementVerificationGroup.get('siteNameCtrl').setValue(0);
        this.hpAgToolTipMsg = '';
        this.hpStToolTipMsg = '';
      }
      if (this.cphSelected.hpAgencyID > 0) {
        /* If AllHpAgencies doesn't contain providerAgency, we need to append it */
        let containsHpAgencySelected = this.AllHpAgencies.filter(d => d.agencyID === this.cphSelected.hpAgencyID);
        if (containsHpAgencySelected.length <= 0) {
          /* HpAgencies list is of non-tracked Agencies, meaning this.cphSelected.hpAgencyID is tracked Agency,
          Append this this.cphSelected.hpAgencyID with HpAgencies (non-tracked) */

          const addAgency: IVCSHpAgenciesForPlacementVerification = {
            agencyID: this.cphSelected.hpAgencyID,
            agencyNo: this.cphSelected.hpAgencyNo,
            agencyName: this.cphSelected.hpAgencyName,
            siteTrackedType: 33
          }
          this.hpAgencies.push(addAgency);
        }
      }
      hpAg.forEach(ag => {
        this.hpAgencies.push(ag);
      });
      if (this.isFollowUpMoveIn && this.followUpMoveInDate !== '') {
        /* append all the MoveIn Pending Verification record's HpAgencies whose MoveInDate = FollowUp-MoveInDate */
        this.appendPendingMoveInAgencies(this.hpAgencies, this.followUpMoveInDate);
        hpAg.forEach(ag => {
          this.hpAgencies.push(ag);
        });
      }
    } else if (res == 663 && (this.cphSelected.verificationStatusType == 758 || this.cphSelected.verificationStatusType == 520)) {
      /* PlacementType is Dis-Continuous(663) and selected record is FollowUp MoveIn(758) or MoveInVerified (520) */
      this.hpAgencies = [];
      const Agencies = this.AllHpAgencies.filter(d => (d.siteTrackedType == 34 || d.siteTrackedType == null));
      this.hpAgencies = Agencies;
      this.hpSiteList = [];
      // this.containsProviderAgency = true; // to remove the ProviderAgency/site from the list for Discontinue case
      if (this.cphSelected.actionID !== 3) {
        this.placementVerificationGroup.get('agencyNameCtrl').setValue(0);
        this.placementVerificationGroup.get('siteNameCtrl').setValue(0);
        this.hpAgToolTipMsg = '';
        this.hpStToolTipMsg = '';
      }
    }
  }

  onActionButtonClickParent(cphSelected: IVCSClientPlacementsHistoryList) {
    if (this.isPlacementTabOpen && !this.case3Flag) {
      const title = 'Verify';
      const primaryMessage = `Please close Placement form [ Placement #` + this.placementTabNumber + `] before opening another placement form`;
      const secondaryMessage = ``;
      const confirmButtonName = 'OK';
      const dismissButtonName = '';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => { },
        negativeResponse => { }
      );
    } else {
      /* No previous placement Form opened */
      this.isPlacementTabOpen = true;
      this.placementTabNumber = cphSelected.placementNo || 1;
      this.cphSelected = cphSelected;
      /* Reset the ToolTip */
      this.hpAgToolTipMsg = '';
      this.hpStToolTipMsg = '';
      this.reasonMovedToolTipMsg = '';
      this.locationMovedToolTipMsg = '';
      this.agencyMovedToolTipMsg = '';
      this.primaryServiceContractToolTipMsg = '';
      // this.placementsVerificationService.setClientPlacementsHistorySelected(cphSelected);
      // this.placementsVerificationService.setClientPlacementNumber(this.placementTabNumber);
      // this.placementsVerificationService.setIsClientPlacementFormTabOpen(true);

      if (this.cphSelected.placementType == 662 || this.cphSelected.placementType == 663 || (this.cphSelected.placementType == null && this.cphSelected.verificationStatusType == 758)) {
        /* If PlacementType is not N -New (661) only filter 662 and 663 in dropdown */
        this.placementType = this.refData.filter(d => d.refGroupID === 84 && d.refGroupDetailID !== 661);
      } else if (this.cphSelected.placementType == 661 || (this.cphSelected.placementType == null && this.cphSelected.verificationStatusType !== 758)) {
        /* If PlacementType is N -New (661) only filter 661 in dropdown */
        this.placementType = this.refData.filter(d => d.refGroupID == 84 && d.refGroupDetailID == 661);
      }
      this.selectedTab = 1;
      /* enable (reset) all the editable formControl */
      this.placementVerificationGroup.enable();

      if (this.cphSelected.verificationStatusType == 758) {
        this.placementVerificationGroup.get('moveInDateCtrl').disable();
      } else if (this.cphSelected.actionID == 2 && this.cphSelected.placementType == 661 && this.cphSelected.verificationStatusType == 520) {
        this.placementVerificationGroup.get('moveInDateCtrl').enable();
      }


      if (this.cphSelected.placementType == 661 || (this.cphSelected.placementType == null && this.cphSelected.verificationStatusType !== 758)) {
        this.placementVerificationGroup.get('placementTypeCtrl').setValue(661);
        this.placementVerificationGroup.get('placementTypeCtrl').disable();
      } else if (this.cphSelected.placementType == 662 || this.cphSelected.placementType == 663 || (this.cphSelected.placementType == null && this.cphSelected.verificationStatusType == 758)) {
        this.placementVerificationGroup.get('placementTypeCtrl').setValue(this.cphSelected.placementType);
        this.placementVerificationGroup.get('placementTypeCtrl').enable();
      }
      // else if (this.cphSelected.actionID == 3 && this.cphSelected.verificationStatusType == 523) {
      //   this.placementVerificationGroup.get('placementTypeCtrl').setValue(this.cphSelected.placementType);
      // }
      /* To Add the Tracked Agency of the Selected Placement */
      if (this.cphSelected.siteTrackedType == 33) {
        if (this.cphSelected.hpAgencyID > 0 && this.cphSelected.hpSiteID > 0) {
          var hpAg = this.AllHpAgencies;
          this.hpAgencies = [];
          const addAgency: IVCSHpAgenciesForPlacementVerification = {
            agencyID: this.cphSelected.hpAgencyID,
            agencyNo: this.cphSelected.hpAgencyNo,
            agencyName: this.cphSelected.hpAgencyName,
            siteTrackedType: 33
          }
          this.hpAgencies.push(addAgency);
          hpAg.forEach(ag => {
            this.hpAgencies.push(ag);
          });
        }
      }
      // console.log('cphSelected: ', cphSelected);
      /* ReadOnly Form fields -- making it disabled */
      this.placementVerificationGroup.get('placementNoCtrl').setValue(this.placementTabNumber);
      this.placementVerificationGroup.get('placementNoCtrl').disable();
      this.placementVerificationGroup.get('unitNoCtrl').setValue(this.cphSelected.unitName || 'n/a');
      this.placementVerificationGroup.get('unitNoCtrl').disable();
      this.placementVerificationGroup.get('primaryServiceContractTypeCtrl').setValue(this.cphSelected.primaryFundingSource || 'n/a');
      /* For Tooltip */
      this.primaryServiceContractToolTipMsg = this.cphSelected.primaryFundingSource;
      this.placementVerificationGroup.get('primaryServiceContractTypeCtrl').disable();
      this.placementVerificationGroup.get('moveInCommentsCtrl').setValue(this.cphSelected.moveInComments);
      this.placementVerificationGroup.get('moveInCommentsCtrl').disable();
      this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
      /* For ToolTip */
      this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
      this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
      this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify);
      this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
      this.placementVerificationGroup.get('moveOutCommentsCtrl').setValue(this.cphSelected.moveOutComments);
      this.placementVerificationGroup.get('moveOutCommentsCtrl').disable();
      this.placementVerificationGroup.get('osDateEnteredCtrl').setValue(this.cphSelected.originalSourceEnteredDate ? this.cphSelected.originalSourceEnteredDate : (this.cphSelected.verificationStatusType == 519 || this.cphSelected.verificationStatusType == 758) ? this.datePipe.transform(new Date(), 'MM/dd/yyyy') : '');
      this.placementVerificationGroup.get('osDateEnteredCtrl').disable();
      this.placementVerificationGroup.get('osEnteredByCtrl').setValue(this.cphSelected.originalSourceEnteredBy ? this.cphSelected.originalSourceEnteredBy : (this.cphSelected.verificationStatusType == 519 || this.cphSelected.verificationStatusType == 758) ? (this.currentUser.firstName + ' ' + this.currentUser.lastName) : '');
      this.placementVerificationGroup.get('osEnteredByCtrl').disable();
      this.placementVerificationGroup.get('sDateEnteredCtrl').setValue(this.cphSelected.sourceEnteredDate ? this.cphSelected.sourceEnteredDate : this.cphSelected.verificationStatusType == 522 ? this.datePipe.transform(new Date(), 'MM/dd/yyyy') : '');
      this.placementVerificationGroup.get('sDateEnteredCtrl').disable();
      this.placementVerificationGroup.get('sEnteredByCtrl').setValue(this.cphSelected.sourceEnteredBy ? this.cphSelected.sourceEnteredBy : this.cphSelected.verificationStatusType == 522 ? (this.currentUser.firstName + ' ' + this.currentUser.lastName) : '');
      this.placementVerificationGroup.get('sEnteredByCtrl').disable();
      this.placementVerificationGroup.get('dateVerifiedCtrl').setValue(this.cphSelected.verificationSourceVerifiedDate ? this.cphSelected.verificationSourceVerifiedDate : this.datePipe.transform(new Date(), 'MM/dd/yyyy'));
      this.placementVerificationGroup.get('dateVerifiedCtrl').disable();
      this.placementVerificationGroup.get('reviewerDateVerifiedCtrl').setValue(this.cphSelected.reviewerDateVerified ? new Date(this.cphSelected.reviewerDateVerified) : new Date());
      this.placementVerificationGroup.get('verifiedByCtrl').setValue(this.cphSelected.verificationSourceVerifiedBy ? this.cphSelected.verificationSourceVerifiedBy : (this.currentUser.firstName + ' ' + this.currentUser.lastName));
      this.placementVerificationGroup.get('verifiedByCtrl').disable();

      /* Pre populating all the form fields */
      this.placementVerificationGroup.get('moveInDateCtrl').setValue(this.cphSelected.moveInDate ? new Date(this.cphSelected.moveInDate) : '');
      this.placementVerificationGroup.get('agencyNameCtrl').setValue(this.cphSelected.hpAgencyID);
      this.placementVerificationGroup.get('siteNameCtrl').setValue(this.cphSelected.hpSiteID);
      /* For ToolTip */
      this.hpAgToolTipMsg = this.cphSelected.hpAgencyNo + ' - ' + this.cphSelected.hpAgencyName;
      this.hpStToolTipMsg = this.cphSelected.hpSiteNo + ' - ' + this.cphSelected.hpSiteName;
      this.placementVerificationGroup.get('moveOutDateCtrl').setValue(this.cphSelected.moveOutDate ? new Date(this.cphSelected.moveOutDate) : '');
      this.placementVerificationGroup.get('reasonMovedCtrl').setValue(this.cphSelected.moveOutReasonType || -1);
      /* For ToolTip */
      this.moveOutReasonType.forEach(r => {
        if (r.refGroupDetailID == this.cphSelected.moveOutReasonType) {
          this.reasonMovedToolTipMsg = r.refGroupDetailDescription;
        }
      });
      this.placementVerificationGroup.get('locationMoveToCtrl').setValue(this.cphSelected.moveOutLocationType || -1);
      /* For ToolTip */
      this.moveOutLocationType.forEach(l => {
        if (l.refGroupDetailID == this.cphSelected.moveOutLocationType) {
          this.locationMovedToolTipMsg = l.refGroupDetailDescription;
        }
      });
      this.placementVerificationGroup.get('moveOutTypeCtrl').setValue(this.cphSelected.moveOutType || -1);
      this.placementVerificationGroup.get('agencyMovedToCtrl').setValue(this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '');
      /* For ToolTip */
      this.agencyMovedToolTipMsg = this.cphSelected.moveToAgencyID ? (this.cphSelected.moveToAgencyNo + ' - ' + this.cphSelected.moveToAgencyName) : '';
      this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').setValue(this.cphSelected.moveToAgencyOtherSpecify || '');
      this.placementVerificationGroup.get('originalSourceCtrl').setValue(this.cphSelected.originalSourceType || 724);
      this.placementVerificationGroup.get('sourceCtrl').setValue(this.cphSelected.sourceType || 724);
      this.placementVerificationGroup.get('verificationSourceCtrl').setValue(this.cphSelected.verificationSourceType || 724);
      this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue(this.cphSelected.placementVerificationComments);

      if (this.cphSelected.actionID == 1) {
        /* VERIFY button clicked */
        if (!this.cphSelected.moveOutDate) {
          /* MoveIn Verify */
          this.placementVerificationGroup.get('moveInDateCtrl').disable();
          this.placementVerificationGroup.get('agencyNameCtrl').disable();
          this.placementVerificationGroup.get('siteNameCtrl').disable();

          this.placementVerificationGroup.get('osDateEnteredCtrl').setValue(this.datePipe.transform(new Date(), 'MM/dd/yyyy'));
          this.placementVerificationGroup.get('osEnteredByCtrl').setValue(this.currentUser.firstName + ' ' + this.currentUser.lastName);
          this.placementVerificationGroup.get('dateVerifiedCtrl').setValue(this.datePipe.transform(new Date(), 'MM/dd/yyyy'));
          this.placementVerificationGroup.get('reviewerDateVerifiedCtrl').setValue(new Date());
          this.placementVerificationGroup.get('verifiedByCtrl').setValue(this.currentUser.firstName + ' ' + this.currentUser.lastName);


          /* disable all the moveout form */
          this.placementVerificationGroup.get('moveOutDateCtrl').disable();
          this.placementVerificationGroup.get('reasonMovedCtrl').disable();
          this.placementVerificationGroup.get('locationMoveToCtrl').disable();
          this.placementVerificationGroup.get('agencyMovedToCtrl').disable();
          this.placementVerificationGroup.get('agencyMovedToOtherSpecifyCtrl').disable();
          this.placementVerificationGroup.get('moveOutTypeCtrl').disable();
          /* disabel the source control */
          this.placementVerificationGroup.get('sourceCtrl').setValue('');
          this.placementVerificationGroup.get('sourceCtrl').disable();

        } else if (this.cphSelected.moveOutDate) {
          /* MoveOut Verify */
          this.placementVerificationGroup.get('moveOutDateCtrl').disable();

          this.placementVerificationGroup.get('sDateEnteredCtrl').setValue(this.datePipe.transform(new Date(), 'MM/dd/yyyy'));
          this.placementVerificationGroup.get('sEnteredByCtrl').setValue(this.currentUser.firstName + ' ' + this.currentUser.lastName);
          this.placementVerificationGroup.get('dateVerifiedCtrl').setValue(this.datePipe.transform(new Date(), 'MM/dd/yyyy'));
          this.placementVerificationGroup.get('reviewerDateVerifiedCtrl').setValue(new Date());
          this.placementVerificationGroup.get('verifiedByCtrl').setValue(this.currentUser.firstName + ' ' + this.currentUser.lastName);

          /* Disable all the MoveIn Form */
          this.placementVerificationGroup.get('moveInDateCtrl').disable();
          this.placementVerificationGroup.get('agencyNameCtrl').disable();
          this.placementVerificationGroup.get('siteNameCtrl').disable();
          /* disable the Original source Control*/
          // this.placementVerificationGroup.get('originalSourceCtrl').setValue('');
          this.placementVerificationGroup.get('originalSourceCtrl').disable();

        }
      } else if (this.cphSelected.actionID == 2) {
        /* UPDATE button clicked */
        if (!this.cphSelected.moveOutDate) {
          /* MoveIn Update */
          if ((this.cphSelected.placementType == 661 || this.cphSelected.placementType == null) && this.cphSelected.verificationStatusType !== 758) {
            // if not 758 = Move-In:FollowUpVerified
            this.placementVerificationGroup.get('agencyNameCtrl').disable();
            this.placementVerificationGroup.get('siteNameCtrl').disable();
          }

          if (this.cphSelected.placementType !== 661 && this.cphSelected.verificationStatusType == 520) {
            this.placementVerificationGroup.get('moveInDateCtrl').disable();
          }

          /* disable all the moveout form */
          this.placementVerificationGroup.get('moveOutDateCtrl').disable();
          this.placementVerificationGroup.get('reasonMovedCtrl').disable();
          this.placementVerificationGroup.get('locationMoveToCtrl').disable();
          this.placementVerificationGroup.get('moveOutTypeCtrl').disable();
          /* disabel the source control */
          this.placementVerificationGroup.get('sourceCtrl').setValue('');
          this.placementVerificationGroup.get('sourceCtrl').disable();

        } else if (this.cphSelected.moveOutDate) {
          /* MoveOut Update */

          /* Disable all the MoveIn Form */
          this.placementVerificationGroup.get('moveInDateCtrl').disable();
          this.placementVerificationGroup.get('agencyNameCtrl').disable();
          this.placementVerificationGroup.get('siteNameCtrl').disable();

          /* disable the Original source Control*/
          // this.placementVerificationGroup.get('originalSourceCtrl').setValue('');
          this.placementVerificationGroup.get('originalSourceCtrl').disable();
        }
      } else if (this.cphSelected.actionID == 3) {
        /* VIEW button clicked */
        // this.placementVerificationGroup.get('placementTypeCtrl').enable();
        // this.placementVerificationGroup.get('placementTypeCtrl').setValue(this.cphSelected.placementType);
        this.placementVerificationGroup.disable();
      }
    }
  }

  onDeletePlacementClickParent(cphSelectedForDel: IVCSClientPlacementsHistoryList) {
    if (this.hasPriorPlacements) {
      const title = 'Verify';
      const primaryMessage = `The selected placement for client # ` + this.pactClientID + ` for Referral date ` + cphSelectedForDel.referralDate + ` will be removed and the prior placement made current.`;
      const secondaryMessage = `Please Click OK to confirm or Cancel.`;
      const confirmButtonName = 'OK';
      const dismissButtonName = 'Cancel';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => {
          /* checkVCURequirement */
          this.checkVCURequirement(cphSelectedForDel);
        },
        negativeResponse => { }
      );
    } else if (!this.hasPriorPlacements && this.hasActiveApproval) {
      const title = 'Verify';
      const primaryMessage = cphSelectedForDel.clientSourceType == 573 ?
        (`The selected placement for client # ` + this.pactClientID + ` for Referral date ` + cphSelectedForDel.referralDate + ` will be removed and the client will be placed back on the Clients Awaiting Placement list on the next business day.`)
        : cphSelectedForDel.clientSourceType == 574 ?
          (`The selected placement for client # ` + this.pactClientID + ` for Referral date ` + cphSelectedForDel.referralDate + ` will be removed.`) : '';
      const secondaryMessage = `Please Click OK to confirm or Cancel.`;
      const confirmButtonName = 'OK';
      const dismissButtonName = 'Cancel';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => {
          /* checkVCURequirement */
          this.checkVCURequirement(cphSelectedForDel);
        },
        negativeResponse => { }
      );
    } else if (!this.hasPriorPlacements && !this.hasActiveApproval) {
      const title = 'Verify';
      const primaryMessage = cphSelectedForDel.clientSourceType == 573 ?
        (`The selected placement for client client # ` + this.pactClientID + ` for Referral date ` + cphSelectedForDel.referralDate + ` will be removed. The client will not be placed back on the Clients Awaiting Placement list because they no longer have an active referral.`)
        : cphSelectedForDel.clientSourceType == 574 ?
          (`The selected placement for client client # ` + this.pactClientID + ` for Referral date ` + cphSelectedForDel.referralDate + ` will be removed.`) : '';
      const secondaryMessage = `Please Click OK to confirm or Cancel.`;
      const confirmButtonName = 'OK';
      const dismissButtonName = 'Cancel';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => {
          /* checkVCURequirement */
          this.checkVCURequirement(cphSelectedForDel);
        },
        negativeResponse => { }
      );
    }
  }

  checkVCURequirement(cphSelectedForDel: IVCSClientPlacementsHistoryList) {
    if (cphSelectedForDel.isVacancyMonitorRequired) {
      const title = 'Verify';
      const primaryMessage = `Deleting this placement will notify VCU unit that this unit is no longer online.`;
      const secondaryMessage = ``;
      const confirmButtonName = 'OK';
      const dismissButtonName = 'Cancel';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => {
          /* Pop Up Dialog for Delete Placement */
          this.deletePlacement(cphSelectedForDel);
        },
        negativeResponse => { }
      );
    } else {
      /* Pop Up Dialog for Delete Placement */
      this.deletePlacement(cphSelectedForDel);
    }
  }

  deletePlacement(cphSelectedForDel: IVCSClientPlacementsHistoryList) {
    /* Pop Up Dialog for Delete Placement */
    const dialogRef = this.dialog.open(DeletePlacementDialogComponent, {
      width: '40%',
      panelClass: 'transparent',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((comment: string) => {
      if (comment) {
        /** Delete Placement */
        const input: IVCSDeletePlacement = {
          vcsPlacementVerificationID: cphSelectedForDel.vcsPlacementVerificationID,
          comments: comment
        }
        this.placementsVerificationService.deleteVCSPlacement(input).subscribe((res: IVCSDeletePlacement[]) => {
          if (res[0].vcsPlacementVerificationID > 0) {
            this.toastr.success('Placement delete Successful.');
            this.getGridData();
          } else {
            this.toastr.error('Something went wrong.');
          }
        });
      }
    });
  }

  onMoveInDateSelected() {
    if (this.cphSelected.moveInDate && this.cphSelected.actionID == 2 && !this.cphSelected.moveOutDate && !this.wasFormReset) {
      /* MoveIn Update */
      const moveInDate = new Date(this.cphSelected.moveInDate);
      if (this.datePipe.transform(this.placementVerificationGroup.get('moveInDateCtrl').value, 'MM/dd/yyyy')) {
        const moveInDatePicked = new Date(this.placementVerificationGroup.get('moveInDateCtrl').value);
        if (moveInDate !== moveInDatePicked) {
          const title = 'Verify';
          const primaryMessage = `you are updating the moveIn date from what the provider has originally submitted.`;
          const secondaryMessage = `Do you want to continue?`;
          const confirmButtonName = 'OK';
          const dismissButtonName = 'Cancel';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              // Make reviewer comment mandatory
              this.isReviewerCommentMandatory = true;
              this.moveInDateChanged = true;
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue('');
            },
            negativeResponse => {
              // reset the moveInDate back
              this.isReviewerCommentMandatory = false;
              this.moveInDateChanged = false;
              this.placementVerificationGroup.get('moveInDateCtrl').setValue(moveInDate);
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue(this.cphSelected.placementVerificationComments);
            }
          );
        }
      }
    }
  }

  onMoveOutDateSelected() {
    if (this.cphSelected.actionID == 2 && this.cphSelected.moveOutDate && !this.wasFormReset) {
      /* MoveOut Update */
      const moveOutDate = new Date(this.cphSelected.moveOutDate);
      if (this.datePipe.transform(this.placementVerificationGroup.get('moveOutDateCtrl').value, 'MM/dd/yyyy')) {
        const moveOutDatePicked = new Date(this.placementVerificationGroup.get('moveOutDateCtrl').value);
        if (moveOutDate !== moveOutDatePicked) {
          const title = 'Verify';
          const primaryMessage = `you are updating the moveout date from what the provider has originally submitted.`;
          const secondaryMessage = `Do you want to continue?`;
          const confirmButtonName = 'OK';
          const dismissButtonName = 'Cancel';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              // Make reviewer comment mandatory
              this.isReviewerCommentMandatory = true;
              this.moveOutDateChanged = true;
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue('');
            },
            negativeResponse => {
              // reset the moveOutDate back
              this.isReviewerCommentMandatory = false;
              this.moveOutDateChanged = false;
              this.placementVerificationGroup.get('moveOutDateCtrl').setValue(moveOutDate);
              this.placementVerificationGroup.get('reviewerCommentsCtrl').setValue(this.cphSelected.placementVerificationComments);
            }
          );
        }
      }

    }
  }

  onAcceptRejectUpdateVerification(acceptRejectUpdateStatus: number) {
    // acceptRejectUpdateStatus -- 1 = Accept Placement,  2 = Reject Placement, 3 = Update Placement
    const reviewerComments = this.placementVerificationGroup.get('reviewerCommentsCtrl').value;
    if (this.isReviewerCommentMandatory && reviewerComments == '') {
      this.reviewerPanelExpanded = true;
      this.toastr.error('Please enter some review comments in Verification Details section');
    } else {
      // Accept the placement
      this.case3Flag = false;  // resetting the flag incase if it is coming back after case3 - moveOut verified: moveIn followUp
      this.isFollowUpMoveIn = false; // resetting the flag incase if it is coming back after case3 - moveOut verified: moveIn followUp
      this.followUpMoveInDate = '';
      const pvData: IVCSUpdatePlacementVerification = {
        placementOutcomeType: null,    // MoveIn - 1, MoveOut - 2 required
        placementOutcomeID: null,   // required  -- MoveIn Or MoveOut ID based on the above @PlacementOutcomeType
        placementVerificationType: null,    // required -- 1 - Verify MoveIn, 2 - Update Verified Move-In, 3 - Verify MoveOut, 4 - Update Verified Move-Out
        placementVerificationID: this.cphSelected.vcsPlacementVerificationID,   // required
        placementType: null,
        siteID: null,
        originalSourceType: null,   // required
        moveOutSourceType: null,
        verificationSourceType: null,   // required
        verificationStatusType: null,   // required based on cases (e.g if this is MoveIn pending verification pass it as MoveIn verified id)
        placementOutcomeDate: null,  // -- MoveIn Or MoveOut Date based on the above @PlacementOutcomeType
        placementOutcomeComments: null,  // -- MoveIn or MoveOut comment
        placementVerificationComments: null,  // -- Reviewer comment
        moveOutLocationType: null,
        moveOutReasonType: null,
        moveOutType: null,
        userID: null,
        reviewerDateVerified: this.placementVerificationGroup.get('reviewerDateVerifiedCtrl').value
      }
      if (this.cphSelected.verificationStatusType == 519) {
        // Case 1 - MoveIn pending Verification
        // const placementType = this.placementVerificationGroup.get('placementTypeCtrl').value;
        const originalSrcType = this.placementVerificationGroup.get('originalSourceCtrl').value;
        const verificationSrcType = this.placementVerificationGroup.get('verificationSourceCtrl').value;

        pvData.placementOutcomeType = 1;    // MoveIn - 1, MoveOut - 2
        pvData.placementOutcomeID = this.cphSelected.vcsPlacementMoveInID;
        pvData.placementVerificationType = 1; // required -- 1 - Verify MoveIn, 2 - Update Verified Move-In, 3 - Verify MoveOut, 4 - Update Verified Move-Out
        // pvData.placementType = placementType;
        pvData.originalSourceType = originalSrcType;
        pvData.verificationSourceType = verificationSrcType;
        pvData.verificationStatusType = acceptRejectUpdateStatus == 1 ? 520 : 521; // 520 = MoveIn Verified, 521 = MoveIn Rejected
        pvData.placementVerificationComments = reviewerComments;

        if (originalSrcType <= 0) {
          this.toastr.error('Please select the Original Source Type in Verification Details section.');
        } else if (verificationSrcType <= 0) {
          this.toastr.error('Please select the Verification Source Type in Verification Details section.');
        } else {
          // save the placement Verification
          this.savePlacementVerification(pvData, 1, pvData.verificationStatusType, acceptRejectUpdateStatus);
        }
      } else if (this.cphSelected.verificationStatusType == 520 || this.cphSelected.verificationStatusType == 758) {
        // Case 2 - verified MoveIn Update Verification
        const sysDate = new Date();
        const placementType = this.placementVerificationGroup.get('placementTypeCtrl').value || 0;
        const moveInDate = this.placementVerificationGroup.get('moveInDateCtrl').value ? new Date(this.placementVerificationGroup.get('moveInDateCtrl').value) : this.placementVerificationGroup.get('moveInDateCtrl').value;
        const siteid = this.placementVerificationGroup.get('siteNameCtrl').value || 0;
        const originalSrcType = this.placementVerificationGroup.get('originalSourceCtrl').value;
        const verificationSrcType = this.placementVerificationGroup.get('verificationSourceCtrl').value;

        pvData.placementOutcomeType = 1;    // MoveIn - 1, MoveOut - 2
        pvData.placementOutcomeID = this.cphSelected.vcsPlacementMoveInID;
        pvData.placementVerificationType = 2; // required -- 1 - Verify MoveIn, 2 - Update Verified Move-In, 3 - Verify MoveOut, 4 - Update Verified Move-Out
        pvData.siteID = siteid;
        pvData.placementType = placementType;
        pvData.originalSourceType = originalSrcType;
        pvData.placementOutcomeDate = this.moveInDateChanged ? moveInDate : null;
        pvData.verificationSourceType = verificationSrcType;
        // pvData.verificationStatusType = acceptRejectUpdateStatus == 1 ? 520 : 521; // 520 = MoveIn Verified, 521 = MoveIn Rejected
        pvData.placementVerificationComments = reviewerComments;

        if ((placementType <= 0 || placementType == null) && this.cphSelected.verificationStatusType == 758) {
          this.toastr.error('Please select the PlacementType in Placement Details section.');
        } else if (!moveInDate) {
          this.toastr.error('Please select the MoveInDate');
        } if (moveInDate > sysDate) {
          this.toastr.error('The Move-In Date should be on or before ' + (sysDate.getMonth() + 1) + '/' + sysDate.getDate() + '/' + sysDate.getFullYear());
        } else if ((siteid <= 0) && this.cphSelected.verificationStatusType == 758) {
          this.toastr.error('Please select the Agency/Site in MoveIn Details section');
        } else if (originalSrcType <= 0) {
          this.toastr.error('Please select the Original Source Type in Verification Details section.');
        } else if (verificationSrcType <= 0) {
          this.toastr.error('Please select the Verification Source Type in Verification Details section.');
        } else if (this.cphSelected.placementType == 661 && this.cphSelected.verificationStatusType == 520) {
          /* Unit#, MoveIn-Date validation (Occupancy Validation, Offline Period Validation, Existing Tenant Verification)
            Only for Verified MoveIn Update, (not for 758- Move-In:FollowUpVerified) */
          const validatorInput: IVCSMoveInValidator = {
            unitID: this.cphSelected.unitID,
            moveInDate: moveInDate,
            clientID: this.cphSelected.clientID,
            clientSourceType: this.cphSelected.clientSourceType,
          }
          this.moveInValidatorSub = this.tenantRosterService.getVCSMoveInValidators(validatorInput).subscribe((val: IVCSMoveInValidator) => {
            if (val) {
              // console.log('move-In Validator: ',val);
              // if (this.referralType == 526) {
              //   savePlcmtOutcome.isEligibilityCriteriaMatched = val.isEligibilityCriteriaMatched;
              //   savePlcmtOutcome.isApprovalActive = val.isApprovalActive;
              // }
              // console.log('moveInValidator: ', val);
              if (!val.wasUnitOffline && (!val.isUnitAvailable || val.wasUnitOccupied)) {
                /* Occupancy Validation
                 Unit was not Offline but another Client was residing in the selected Unit during this Move-In Date selected */
                const title = 'Verify';
                const primaryMessage = `Our records show that another tenant was living in this unit on this move-In date, please review your records and select a different move-in date or revise the prior tenant housing record.`;
                const secondaryMessage = ``;
                const confirmButtonName = 'OK';
                const dismissButtonName = '';
                this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                  positiveResponse => { },
                  negativeResponse => { }
                );
              } else if (val.wasUnitOffline) {
                /* Offline Period Validation
                  Unit was Offline during the selected Move-In Date */
                const title = 'Verify';
                const primaryMessage = `Our records show that this unit was offline during period.`;
                const secondaryMessage = `Do you still want to continue?`;
                const confirmButtonName = 'Yes';
                const dismissButtonName = 'No';
                this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                  positiveResponse => {
                    if (reviewerComments === '') {
                      this.toastr.error('Please enter some review comments in Verification Details section');
                    } else {
                      // save the placement Verification
                      this.savePlacementVerification(pvData, 2, pvData.verificationStatusType, acceptRejectUpdateStatus);
                    }
                  },
                  negativeResponse => {
                    const title = 'Verify';
                    const primaryMessage = `Please select a different Move-In Date.`;
                    const secondaryMessage = ``;
                    const confirmButtonName = 'OK';
                    const dismissButtonName = '';
                    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                      positiveResponse => { },
                      negativeResponse => { }
                    );
                  }
                );
              } else if (val.wasClientResiding) {
                /* Existing Tenant Verification
                Client is currently residing in a unit or was residing in a unit during the selected Move-In Date */
                const title = 'Verify';
                const primaryMessage = `This tenant is listed as residing in ` + val.wasResidingUnit + ` during this period. Please review your records and select a different move-in date or revise the prior housing record`;
                const secondaryMessage = ``;
                const confirmButtonName = 'OK';
                const dismissButtonName = '';
                this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                  positiveResponse => { },
                  negativeResponse => { }
                );
              } else if (val.isClientResiding) {
                /* Existing Tenant Verification
                Client is currently residing in a unit or was residing in a unit during the selected Move-In Date */
                const title = 'Verify';
                const primaryMessage = `This tenant is currently residing in ` + val.currentlyResidingUnit;
                const secondaryMessage = ``;
                const confirmButtonName = 'OK';
                const dismissButtonName = '';
                this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                  positiveResponse => { },
                  negativeResponse => { }
                );
              } else {
                // save the placement Verification
                this.savePlacementVerification(pvData, 2, pvData.verificationStatusType, acceptRejectUpdateStatus);
              }
            }
          });
        } else {
          // save the placement Verification
          this.savePlacementVerification(pvData, 2, pvData.verificationStatusType, acceptRejectUpdateStatus);
        }
      } else if (this.cphSelected.verificationStatusType == 522) {
        // Case 3 - MoveOut Pending Verification

        // const placementType = this.placementVerificationGroup.get('placementTypeCtrl').value;
        // const siteid = this.placementVerificationGroup.get('siteNameCtrl').value;
        // const originalSrcType = this.placementVerificationGroup.get('originalSourceCtrl').value;
        const reasonMoved = this.placementVerificationGroup.get('reasonMovedCtrl').value;
        const locationMovedTo = this.placementVerificationGroup.get('locationMoveToCtrl').value;
        const moveOutType = this.placementVerificationGroup.get('moveOutTypeCtrl').value;
        const srcType = this.placementVerificationGroup.get('sourceCtrl').value;
        const verificationSrcType = this.placementVerificationGroup.get('verificationSourceCtrl').value;

        pvData.placementOutcomeType = 2;    // MoveIn - 1, MoveOut - 2
        pvData.placementOutcomeID = this.cphSelected.vcsPlacementMoveOutID;
        pvData.placementVerificationType = 3; // required -- 1 - Verify MoveIn, 2 - Update Verified Move-In, 3 - Verify MoveOut, 4 - Update Verified Move-Out
        // pvData.placementType = placementType;
        // pvData.originalSourceType = originalSrcType;
        pvData.moveOutLocationType = locationMovedTo;
        pvData.moveOutReasonType = reasonMoved;
        pvData.moveOutType = moveOutType;
        pvData.moveOutSourceType = srcType;
        pvData.verificationSourceType = verificationSrcType;
        pvData.verificationStatusType = acceptRejectUpdateStatus == 1 ? 523 : 524; // 523 = MoveOut Verified, 524 = MoveOut Rejected
        pvData.placementVerificationComments = reviewerComments;

        // if (placementType <= 0) {
        //   this.toastr.error('Please select the PlacementType in Placement Details section.');
        // } else
        if (reasonMoved <= 0) {
          this.toastr.error('Please select Reason Moved To in MoveOut Details section');
        } else if (locationMovedTo <= 0) {
          this.toastr.error('Please select Location Moved To in MoveOut Details section');
        } else if (moveOutType <= 0) {
          this.toastr.error('Please select Move Out Type in MoveOut Details section');
        } else if (srcType <= 0) {
          this.toastr.error('Please select the Source Type in Verification Details section.');
        } else if (verificationSrcType <= 0) {
          this.toastr.error('Please select the Verification Source Type in Verification Details section.');
        } else {
          // save the placement Verification
          this.savePlacementVerification(pvData, 3, pvData.verificationStatusType, acceptRejectUpdateStatus);
        }
      } else if (this.cphSelected.verificationStatusType == 523) {
        // Case 4 - Verified MoveOut Update Verification

        // const placementType = this.placementVerificationGroup.get('placementTypeCtrl').value;
        // const siteid = this.placementVerificationGroup.get('siteNameCtrl').value;
        // const originalSrcType = this.placementVerificationGroup.get('originalSourceCtrl').value;
        const moveOutDate = this.placementVerificationGroup.get('moveOutDateCtrl').value;
        const reasonMoved = this.placementVerificationGroup.get('reasonMovedCtrl').value;
        const locationMovedTo = this.placementVerificationGroup.get('locationMoveToCtrl').value;
        const moveOutType = this.placementVerificationGroup.get('moveOutTypeCtrl').value;
        const srcType = this.placementVerificationGroup.get('sourceCtrl').value;
        const verificationSrcType = this.placementVerificationGroup.get('verificationSourceCtrl').value;

        pvData.placementOutcomeType = 2;    // MoveIn - 1, MoveOut - 2
        pvData.placementOutcomeID = this.cphSelected.vcsPlacementMoveOutID;
        pvData.placementVerificationType = 3; // required -- 1 - Verify MoveIn, 2 - Update Verified Move-In, 3 - Verify MoveOut, 4 - Update Verified Move-Out
        // pvData.placementType = placementType;
        // pvData.originalSourceType = originalSrcType;
        pvData.placementOutcomeDate = this.moveOutDateChanged ? moveOutDate : null;
        pvData.moveOutLocationType = locationMovedTo;
        pvData.moveOutReasonType = reasonMoved;
        pvData.moveOutType = moveOutType;
        pvData.moveOutSourceType = srcType;
        pvData.verificationSourceType = verificationSrcType;
        // pvData.verificationStatusType = acceptRejectUpdateStatus == 1 ? 523 : 524; // 523 = MoveOut Verified, 524 = MoveOut Rejected
        pvData.placementVerificationComments = reviewerComments;

        // if (placementType <= 0) {
        //   this.toastr.error('Please select the PlacementType in Placement Details section.');
        // } else
        if (!moveOutDate) {
          this.toastr.error('Please select the MoveOut Date in MoveOut Details section');
        } else if (reasonMoved <= 0) {
          this.toastr.error('Please select Reason Moved To in MoveOut Details section');
        } else if (locationMovedTo <= 0) {
          this.toastr.error('Please select Location Moved To in MoveOut Details section');
        } else if (moveOutType <= 0) {
          this.toastr.error('Please select Move Out Type in MoveOut Details section');
        } else if (srcType <= 0) {
          this.toastr.error('Please select the Source Type in Verification Details section.');
        } else if (verificationSrcType <= 0) {
          this.toastr.error('Please select the Verification Source Type in Verification Details section.');
        } else {
          // save the placement Verification
          this.savePlacementVerification(pvData, 3, pvData.verificationStatusType, acceptRejectUpdateStatus);
        }
      }
    }
  }

  savePlacementVerification(inputData: IVCSUpdatePlacementVerification, caseNo: number, vStatus: number, acceptRejectUpdateStatus: number) {
    // caseNo -- all the cases for Verification
    // acceptRejectUpdateStatus -- 1 = accept, 2 = reject, 3 = update
    const title = 'Verify';
    const primaryMessage = acceptRejectUpdateStatus == 1 ? `Are you sure you want to Accept this Placement?` : acceptRejectUpdateStatus == 2 ? `Are you sure you want to Reject this Placement?` : `Are you sure you want to Update this Placement?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        // console.log('savePlacement Input Data: ', inputData);
        // console.log('caseNo: ', caseNo);
        // console.log('vStatus: ', vStatus);
        // console.log('acceptRejectUpdateStatus: ', acceptRejectUpdateStatus);
        // console.log('cphSelected: ',this.cphSelected);
        this.placementsVerificationService.savePlacementVerification(inputData).subscribe((res: IVCSPlacementVerificationOutput) => {
          // console.log('save placement response: ', res);
          if (res.vcsPlacementVerificationID > 0) {
            if (caseNo == 1 && vStatus == 520) {
              this.toastr.success('MoveIn Placement has been accepted');
            } else if (caseNo == 1 && vStatus == 521) {
              this.toastr.success('MoveIn Placement has been Rejected');
            } else if (caseNo == 2) {
              this.toastr.success('MoveIn Placement has been updated');
            } else if (caseNo == 3 && vStatus == 523 && res.isFollowUpRequired) {
              // this.toastr.success('MoveOut Placement has been accepted');
              const title = 'Verify';
              const primaryMessage = `Please complete Pending Action for this client`;
              const secondaryMessage = ``;
              const confirmButtonName = 'OK';
              const dismissButtonName = '';
              this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                positiveResponse => {
                  this.case3Flag = true;
                  this.isFollowUpMoveIn = true;
                  this.followUpMoveInDate = res.moveInDate;
                  this.wasFormReset = true;
                  this.isReviewerCommentMandatory = false;
                  this.placementVerificationGroup.reset();
                  /* append all the MoveIn Pending Verification record's HpAgencies whose MoveInDate = FollowUp-MoveInDate */
                  this.appendPendingMoveInAgencies(this.AllHpAgencies, res.moveInDate);
                  this.AllHpAgencies.forEach(ag => {
                    this.hpAgencies.push(ag);
                  });
                  /* Create the FollowUpMoveIn Object */
                  var sysDate = new Date();
                  this.placementVerificationGroup.get('reviewerDateVerifiedCtrl').setValue(sysDate);
                  var rvd = this.placementVerificationGroup.get('reviewerDateVerifiedCtrl').value;
                  var rvd2 = (rvd.getMonth() + 1) + '/' + rvd.getDate() + '/' + rvd.getFullYear();
                  const followUpMoveIn: IVCSClientPlacementsHistoryList = {
                    hpAgencyID: null,
                    hpAgencyNo: null,
                    hpSiteID: null,
                    hpSiteNo: null,
                    hpAgencyName: null,
                    hpSiteName: null,
                    siteTrackedType: null,
                    raAgencyID: 0,
                    raAgencyNo: '',
                    raSiteID: 0,
                    raSiteNo: '',
                    raAgencyName: '',
                    raSiteName: '',
                    vcsid: this.cphSelected.vcsid,
                    clientID: this.cphSelected.clientID,
                    clientSourceType: this.cphSelected.clientSourceType,
                    referralDate: this.cphSelected.referralDate,
                    tenantFirstName: this.cphSelected.tenantFirstName,
                    tenantLastName: this.cphSelected.tenantLastName,
                    svaPrioritization: '',
                    serviceNeeds: '',
                    eligibility: '',
                    vcsPlacementMoveInID: res.vcsPlacementMoveInID,
                    vcsPlacementMoveOutID: 0,
                    vcsPlacementVerificationID: res.vcsPlacementVerificationID,
                    unitID: null,
                    unitName: null,
                    primaryFundingSource: null,
                    verificationAssignedToID: 0,
                    verificationAssignedTo: '',
                    moveInDate: res.moveInDate,
                    moveOutDate: '',
                    moveOutReasonType: 0,
                    moveOutReasonDescription: '',
                    moveOutLocationType: 0,
                    moveOutLocationDescription: '',
                    placementNo: res.placementNo,
                    placementType: null,
                    placementTypeDesription: '',
                    moveOutType: 0,
                    moveOutTypeDescription: '',
                    moveToAgencyID: 0,
                    moveToAgencyNo: '',
                    moveToAgencyName: '',
                    moveToAgencyOtherSpecify: '',
                    verificationStatusType: 758, // Move-In:FollowUp:Verified
                    verificationStatusDescription: '',
                    verificationReasonType: 0,
                    verificationReasonDescription: '',
                    submittedID: 0,
                    submittedBy: '',
                    submittedDate: '',
                    originalSourceType: 0,
                    originalSourceDescription: '',
                    originalSourceEnteredID: 0,
                    originalSourceEnteredBy: '',
                    originalSourceEnteredDate: '',
                    sourceType: 0,
                    sourceDescription: '',
                    sourceEnteredID: 0,
                    sourceEnteredBy: '',
                    sourceEnteredDate: '',
                    verificationSourceType: 0,
                    verificationSourceDescription: '',
                    verificationSourceVerifiedID: 0,
                    verificationSourceVerifiedBy: '',
                    verificationSourceVerifiedDate: '',
                    verificationUpdatedID: 0,
                    verificationUpdatedBy: '',
                    verificationUpdatedDate: '',
                    moveInComments: null,
                    moveOutComments: '',
                    placementVerificationComments: null,
                    actionID: 2,
                    isPlacementAutoVerified: false,
                    isVacancyMonitorRequired: false,
                    vcuAdminVerificationStatus: null,
                    isDeleted: false,
                    reviewerDateVerified: rvd2
                  }
                  // this.isPlacementTabOpen = false;
                  // this.placementTabNumber = 1;
                  // this.selectedTab = 0;
                  // this.placementVerificationGroup.get('moveInDateCtrl').disable();
                  // this.placementType =  this.refData.filter(d => d.refGroupID === 84 && d.refGroupDetailID !== 661);
                  setTimeout(() => {
                    this.onActionButtonClickParent(followUpMoveIn);
                  }, 1000);
                },
                negativeResponse => { }
              );
            } else if (caseNo == 3 && vStatus == 524) {
              this.toastr.success('MoveOut Placement has been rejected');
            }
            if (!(caseNo == 3 && vStatus == 523 && res.isFollowUpRequired)) {
              // /* this values are being consumed in sidenav to display the placement related navigation, so resetting before leaving */
              // this.placementsVerificationService.setClientPlacementsHistorySelected(null);
              // this.placementsVerificationService.setIsClientPlacementFormTabOpen(false);
              // this.placementsVerificationService.setClientPlacementNumber(0);

              // this.router.navigate(['/vcs/placements-awaiting-verification']);
              this.isPlacementTabOpen = false;
              this.placementTabNumber = 0;
              this.selectedTab = 0;
              this.cphSelected = null;
              this.isFollowUpMoveIn = false;
              this.followUpMoveInDate = '';
              this.placementVerificationGroup.reset();
            }
          } else if (res.hasActivePlacement == true || res.wasActivePlacement == true) {
            this.toastr.error('Can\'t be verified, Client already have a active Placement for the selected Date.');
          } else {
            this.toastr.error('there was some unknown error');
          }
        });
      },
      negativeResponse => { }
    );

  }

  appendPendingMoveInAgencies(preExistingAgencies: IVCSHpAgenciesForPlacementVerification[], followUpMoveInDate: string) {
    const hpAg = preExistingAgencies;
    this.hpAgencies = [];
    if (this.clientPlacementsHistoryRowData.length > 0) {
      const pendingMoveIns = this.clientPlacementsHistoryRowData.filter(d => d.verificationStatusType == 519 && d.moveInDate == followUpMoveInDate);
      // const pendingMoveIns = this.clientPlacementsHistoryRowData.filter(d => d.verificationStatusType == 519);
      if (pendingMoveIns.length > 0) {
        pendingMoveIns.forEach(pm => {
          var contains = false;
          hpAg.forEach(ag => {
            if (pm.hpAgencyID == ag.agencyID) {
              contains = true;
            }
          });
          if (!contains) {
            const agency: IVCSHpAgenciesForPlacementVerification = {
              agencyID: pm.hpAgencyID,
              agencyNo: pm.hpAgencyNo,
              agencyName: pm.hpAgencyName,
              siteTrackedType: 33
            }
            this.hpAgencies.push(agency);
          }
        });
      }
    }
  }

  appendPendingMoveInSites(preExistingSites: IVCSHpSitesForPlacementVerification[], selectedAgencyID: number, followUpMoveInDate: string) {
    /* Checking if the Pending MoveIn Verification Agency is selected and
            If so, checking if the Pending MoveIn Verification Site is listed in hpSiteList
            If not append it */
    const hpSt = preExistingSites;
    this.hpSiteList = [];
    if (this.clientPlacementsHistoryRowData.length > 0) {
      const pendingMoveIns = this.clientPlacementsHistoryRowData.filter(d => d.verificationStatusType == 519 && d.moveInDate == followUpMoveInDate);
      // const pendingMoveIns = this.clientPlacementsHistoryRowData.filter(d => d.verificationStatusType == 519);
      if (hpSt.length > 0) {
        pendingMoveIns.forEach(pm => {
          if (pm.hpAgencyID == selectedAgencyID) {
            var contains = false;
            hpSt.forEach(st => {
              if (st.agencyID == selectedAgencyID && st.siteID == pm.hpSiteID) {
                contains = true;
              }
            });
            if (!contains) {
              const site: IVCSHpSitesForPlacementVerification = {
                siteID: pm.hpSiteID,
                siteNo: pm.hpSiteNo,
                agencyID: pm.hpAgencyID,
                siteName: pm.hpSiteName,
                siteTrackedType: 33
              }
              this.hpSiteList.push(site);
            }
          }
        });
      } else {
        pendingMoveIns.forEach(pm => {
          if (pm.hpAgencyID == selectedAgencyID) {
            const site: IVCSHpSitesForPlacementVerification = {
              siteID: pm.hpSiteID,
              siteNo: pm.hpSiteNo,
              agencyID: pm.hpAgencyID,
              siteName: pm.hpSiteName,
              siteTrackedType: 33
            }
            this.hpSiteList.push(site);
          }
        })
      }
    }
  }

  onPlacementFormExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.isPlacementTabOpen = false;
        this.placementTabNumber = 0;
        this.selectedTab = 0;
        this.cphSelected = null;
        // this.wasFormReset = true;
        this.placementVerificationGroup.reset();
        // /* this values are being consumed in sidenav to display the placement related navigation, so resetting before leaving */
        // this.placementsVerificationService.setClientPlacementsHistorySelected(null);
        // this.placementsVerificationService.setIsClientPlacementFormTabOpen(false);
        // this.placementsVerificationService.setClientPlacementNumber(0);
      },
      negativeResponse => { }
    );
  }
  onPlacementHistoryExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        if (this.isTadVerificationWorkflow) {
          this.router.navigate(['/vcs/tad-verification']);
        } else if (this.isClientPlacements) {
          this.placementsVerificationService.setIsClientPlacements(false);
          this.router.navigate(['/dashboard']);
        } else if (this.isDeletePlacement) {
          this.placementsVerificationService.setIsDeletePlacement(false);
          this.router.navigate(['/dashboard']);
        } else {
          this.router.navigate(['/vcs/placements-awaiting-verification']);
        }
      },
      negativeResponse => { }
    );
  }

  refreshAgGrid() {
    this.cpGridOptions.api.setFilterModel(null);
  }

  onVerificicationHistoryClick() {
    this.dialog.open(PlacementVerificationHistoryComponent, {
      width: '80%',
      maxHeight: '80%',
      disableClose: true
    });
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'ClientPlacementsHistoryReport-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.clientPlacementsGridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.placementVerificationSelectedSub) {
      this.placementVerificationSelectedSub.unsubscribe();
    }
    if (this.clientPlacementsHistoryRowDataSub) {
      this.clientPlacementsHistoryRowDataSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    // if (this.raHpAgenciesSub) {
    //   this.raHpAgenciesSub.unsubscribe();
    // }
    if (this.agencyNameCtrlSub) {
      this.agencyNameCtrlSub.unsubscribe();
    }
    if (this.siteNameCtrlSub) {
      this.siteNameCtrlSub.unsubscribe();
    }
    if (this.hpAgenciesSub) {
      this.hpAgenciesSub.unsubscribe();
    }
    if (this.hpSiteListSub) {
      this.hpSiteListSub.unsubscribe();
    }
    if (this.placementTypeCtrlSub) {
      this.placementTypeCtrlSub.unsubscribe();
    }
    if (this.moveOutTypeCtrlSub) {
      this.moveOutTypeCtrlSub.unsubscribe();
    }
    if (this.reasonMovedCtrlSub) {
      this.reasonMovedCtrlSub.unsubscribe();
    }
    if (this.locationMoveToCtrlSub) {
      this.locationMoveToCtrlSub.unsubscribe();
    }
    // if (this.isPlacementTabOpenSub) {
    //   this.isPlacementTabOpenSub.unsubscribe();
    // }
    // if (this.placementTabNumberSub) {
    //   this.placementTabNumberSub.unsubscribe();
    // }
    // if (this.cphSelectedSub) {
    //   this.cphSelectedSub.unsubscribe();
    // }
    if (this.moveInValidatorSub) {
      this.moveInValidatorSub.unsubscribe();
    }
    if (this.istadVerificationWorkflowSub) {
      this.istadVerificationWorkflowSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.isClientPlacementsSub) {
      this.isClientPlacementsSub.unsubscribe();
    }
    if (this.isDeletePlacementSub) {
      this.isDeletePlacementSub.unsubscribe();
    }
  }
}

