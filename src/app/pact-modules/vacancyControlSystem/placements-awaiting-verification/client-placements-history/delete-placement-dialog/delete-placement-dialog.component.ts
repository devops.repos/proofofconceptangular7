import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-placement-dialog',
  templateUrl: './delete-placement-dialog.component.html',
  styleUrls: ['./delete-placement-dialog.component.scss']
})
export class DeletePlacementDialogComponent implements OnInit, OnDestroy {

  deleteFormGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DeletePlacementDialogComponent>,
    private router: Router,
  ) {
    this.deleteFormGroup = this.formBuilder.group({
      commentCtrl: ['', Validators.required]
    });
  }

  ngOnInit() { }

  onOKClick() {
    const comment = this.deleteFormGroup.get('commentCtrl').value
    this.dialogRef.close(comment);
  }

  onCancelClick(): void {
    this.dialogRef.close(null);
  }

  ngOnDestroy() {
  }
}

