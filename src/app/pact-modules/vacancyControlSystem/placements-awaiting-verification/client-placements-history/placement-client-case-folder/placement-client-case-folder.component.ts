import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { PlacementsVerificationService } from '../../placements-verification.service';
import { IVCSPlacementClientSelected, IVCSClientInfo, IVCSClientData, IVCSClientDocumentsData } from '../../placements-verification-interface.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-placement-client-case-folder',
  templateUrl: './placement-client-case-folder.component.html',
  styleUrls: ['./placement-client-case-folder.component.scss']
})
export class PlacementClientCaseFolderComponent implements OnInit, OnDestroy {

  selectedTabIndex = 0;
  currentUser: AuthData;
  placementClientSelected: IVCSPlacementClientSelected = {
    clientID: 0,
    clientSourceType: 0,
    isPendingVerificationRequired: false
  }

  clientInfo: IVCSClientInfo;

  clientData: IVCSClientData  = {
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null,
    pactClientId: null,
    referralDate: null
  };
  clientDocumentsData: IVCSClientDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null
  }

  currentUserSub: Subscription;
  placementVerificationSelectedSub: Subscription;
  clientInfoSub: Subscription;

  constructor(
    private userService: UserService,
    private placementsVerificationService: PlacementsVerificationService,
    private router: Router,
  ) { }

  ngOnInit() {
    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
      }
    });

    /* Getting the selected Placement Verification */
    this.placementVerificationSelectedSub = this.placementsVerificationService.getPlacementClientSelected().subscribe((res: IVCSPlacementClientSelected) => {
      if (res) {
        this.placementClientSelected = res;
        // console.log('placementClientSelected: ', this.placementClientSelected);
        if (this.placementClientSelected.clientID > 0) {
          this.clientInfoSub = this.placementsVerificationService.getVCSClientInfoForClientCaseFolder(this.placementClientSelected.clientID).subscribe((client: IVCSClientInfo) => {
            if (client) {
              this.clientInfo = client;
              this.clientData.firstName = client.firstName;
              this.clientData.lastName = client.lastName;
              this.clientData.ssn = client.ssn;
              this.clientData.dob = client.dob;
              this.clientData.genderType = client.genderType;
              this.clientData.genderTypeDescription = client.genderTypeDescription;
              this.clientData.cin = client.cin;
              this.clientData.pactClientId = client.pactClientID;
              this.clientData.referralDate = client.referralDate;

              // console.log('ClientData: ', this.clientData);

              this.clientDocumentsData.firstName = client.firstName;
              this.clientDocumentsData.lastName = client.lastName;
              this.clientDocumentsData.ssn = client.ssn;
              this.clientDocumentsData.dob = client.dob;
              this.clientDocumentsData.genderType = client.genderType;
              this.clientDocumentsData.genderTypeDescription = client.genderTypeDescription;
              this.clientDocumentsData.pactClientId = client.pactClientID;
              this.clientDocumentsData.pactApplicationId = client.pactApplicationID;
              this.clientDocumentsData.siteNumber = client.referringSiteNo;
              this.clientDocumentsData.agencyNumber = client.referringAgencyNo;
              this.clientDocumentsData.approvalExpiryDate = client.approvalExpiryDate;
              this.clientDocumentsData.cin = client.cin;

              // console.log('clientDocumentsData: ', this.clientDocumentsData);
            }
          })
        }
        // console.log('placementClientSelected: ', this.placementClientSelected);
      } else {
        this.router.navigate(['/vcs/placements-awaiting-verification']);
      }
    });
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTabIndex = tabChangeEvent.index;
  }

  previousPage() {
    if (this.selectedTabIndex != 0) {
      this.selectedTabIndex = this.selectedTabIndex - 1;
    }
  }

  nextPage() {
    if (this.selectedTabIndex != 5) {
      this.selectedTabIndex = this.selectedTabIndex + 1;
    }
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.placementVerificationSelectedSub) {
      this.placementVerificationSelectedSub.unsubscribe();
    }
    if (this.clientInfoSub) {
      this.clientInfoSub.unsubscribe();
    }
  }

}
