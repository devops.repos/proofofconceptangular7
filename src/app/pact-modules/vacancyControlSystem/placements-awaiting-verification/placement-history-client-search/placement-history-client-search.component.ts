import { Component, OnInit, OnDestroy } from '@angular/core';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { GridOptions } from 'ag-grid-community';
import { IVCSPlacementHistoryClientSearch, IVCSPlacementClientSelected } from '../placements-verification-interface.model';
import { CSRActionComponent } from './csr-action.component';
import { PlacementsVerificationService } from '../placements-verification.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-placement-history-client-search',
  templateUrl: './placement-history-client-search.component.html',
  styleUrls: ['./placement-history-client-search.component.scss'],
  providers: [DatePipe]
})
export class PlacementHistoryClientSearchComponent implements OnInit, OnDestroy {

  genders: RefGroupDetails[];
  sourceSystems: RefGroupDetails[];
  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  clientSearchGroup: FormGroup;

  csrGridApi;
  csrColumnApi;
  csrColumnDefs;
  defaultColDef;
  frameworkComponents;
  context;

  public csrGridOptions: GridOptions;
  csrRowData: IVCSPlacementHistoryClientSearch[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private placementsVerificationService: PlacementsVerificationService,
    private datePipe: DatePipe,
    private router: Router,
    private toastrService: ToastrService,
    private confirmDialogService: ConfirmDialogService,
  ) {
    this.clientSearchGroup = this.formBuilder.group({
      firstNameCtrl: [''],
      lastNameCtrl: [''],
      ssnCtrl: [''],
      cinCtrl: [''],
      dobCtrl: [''],
      genderCtrl: [0],
      sourceSystemCtrl: [0],
      hraIDCtrl: [''],
      referralDateFromCtrl: [''],
      referralDateToCtrl: [''],
    });
    this.csrGridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.csrColumnDefs = [
      {
        headerName: 'Source System',
        field: 'clientSourceDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'HRA ID',
        field: 'clientID',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Client Name (L,F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.lastName && params.data.firstName) {
            return params.data.lastName + ' ' + params.data.firstName;
          } else if (params.data.lastName) {
            return params.data.lastName;
          } else if (params.data.firstName) {
            return params.data.firstName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'SSN',
        field: 'ssn',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.ssn) {
            return 'XXX-XX-' + params.data.ssn.substr(5, 4);
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'DOB',
        field: 'dob',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Gender',
        field: 'gender',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'csrActionRenderer'
      },
    ];
    this.defaultColDef = {
      flex: 1,
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      csrActionRenderer: CSRActionComponent
    };
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '4,71';
    this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        // console.log(data);
        this.genders = data.filter(d => d.refGroupID === 4);
        this.sourceSystems = data.filter(d => d.refGroupID === 71 && d.refGroupDetailID !== 575); //hiding 575= Master Leasing for this release
      },
      error => {
        throw new Error(error.message);
      }
    );
  }

  onCSRGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.csrGridApi = params.api;
    this.csrColumnApi = params.columnApi;

    // const allColumnIds = [];
    // this.csrColumnApi.getAllColumns().forEach(column => {
    //   if (column.colId !== 'action') {
    //     allColumnIds.push(column.colId);
    //   }
    // });
    // this.csrColumnApi.autoSizeColumns(allColumnIds);
    this.csrGridApi.sizeColumnsToFit();
    // params.api.expandAll();

  }

  onActionButtonClickParent(csrSelected: IVCSPlacementHistoryClientSearch) {
    // console.log(csrSelected);
    const input: IVCSPlacementClientSelected = {
      clientID: csrSelected.clientID,
      clientSourceType: csrSelected.clientSourceType,
      isPendingVerificationRequired: false
    }
    this.placementsVerificationService.setPlacementClientSelected(input);
    this.router.navigate(['/vcs/client-placements-history']);

  }

  onClearClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to clear the Form?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        /* Clear the Form */
        this.clientSearchGroup.reset();
        this.clientSearchGroup.get('genderCtrl').setValue(0);
        this.clientSearchGroup.get('sourceSystemCtrl').setValue(0);
        this.csrRowData = [];
      },
      negativeResponse => { }
    );
  }

  onSearchClick() {
    this.csrRowData = [];
    const firstName = this.clientSearchGroup.get('firstNameCtrl').value || null;
    const lastName = this.clientSearchGroup.get('lastNameCtrl').value || null;
    const ssn = this.clientSearchGroup.get('ssnCtrl').value || null;
    const cin = this.clientSearchGroup.get('cinCtrl').value || null;
    const dob = this.clientSearchGroup.get('dobCtrl').value || null;
    const gender = this.clientSearchGroup.get('genderCtrl').value > 0 ? this.clientSearchGroup.get('genderCtrl').value : null;
    const sourceSystem = this.clientSearchGroup.get('sourceSystemCtrl').value > 0 ? this.clientSearchGroup.get('sourceSystemCtrl').value : null;
    const hraID = this.clientSearchGroup.get('hraIDCtrl').value || null;
    const referralDateFrom = this.clientSearchGroup.get('referralDateFromCtrl').value || null;
    const referralDateTo = this.clientSearchGroup.get('referralDateToCtrl').value || null;

    let length = 0;

    if (firstName) { length++; }
    if (lastName) { length++; }
    if (ssn) { length++; }
    if (cin) { length++; }
    if (dob) { length++; }
    if (gender > 0) { length++; }
    if (sourceSystem > 0) { length++; }
    if (hraID) { length++; }
    if (referralDateFrom) { length++; }
    if (referralDateTo) { length++; }

    if (length <= 0) {
      const title = 'Verify';
      const primaryMessage = `Please enter at least one search criteria`;
      const secondaryMessage = ``;
      const confirmButtonName = 'OK';
      const dismissButtonName = '';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => { },
        negativeResponse => { }
      );
    } else {
      const clientSearchInput: IVCSPlacementHistoryClientSearch = {
        clientID: hraID,
        clientSourceType: sourceSystem,
        firstName: firstName,
        lastName: lastName,
        ssn: ssn,
        dob: dob,
        genderType: gender,
        cin: cin,
        referralDateFrom: referralDateFrom,
        referralDateTo: referralDateTo
      }
      // console.log('clientSearchInput: ', clientSearchInput);
      this.clientSearch(clientSearchInput);
    }
  }

  clientSearch(clientSearchInput: IVCSPlacementHistoryClientSearch) {
    this.placementsVerificationService.getPlacementMatchedClient(clientSearchInput).subscribe((res: IVCSPlacementHistoryClientSearch[]) => {
      if (res.length > 0) {
        this.csrRowData = res;
        // console.log(this.csrRowData);
      } else {
        this.toastrService.warning('No results found');
      }
    });
  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.router.navigate(['/dashboard']);
      },
      negativeResponse => { }
    );
  }

  refreshAgGrid() {
    this.csrGridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'ClientPlacementsSearchResultReport-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.csrGridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
  }

}
