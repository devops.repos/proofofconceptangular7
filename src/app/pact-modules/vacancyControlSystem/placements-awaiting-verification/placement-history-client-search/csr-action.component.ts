import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IVCSPlacementHistoryClientSearch } from '../placements-verification-interface.model';

@Component({
  selector: 'app-csr-action',
  template: `
    <mat-icon
      class="csr-icon"
      color="warn"
      [matMenuTriggerFor]="csrAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #csrAction="matMenu">
      <button mat-menu-item (click)="onActionButtonClick()">
        <mat-icon color="primary">assignment</mat-icon>View
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .csr-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class CSRActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  csrSelected: IVCSPlacementHistoryClientSearch;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onActionButtonClick() {
    this.csrSelected = this.params.data;
    this.params.context.componentParent.onActionButtonClickParent(this.csrSelected);
   }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
  }
}
