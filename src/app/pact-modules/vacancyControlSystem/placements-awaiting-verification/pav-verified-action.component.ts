import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IVCSPlacementVerificationList } from './placements-verification-interface.model';

@Component({
  selector: 'app-pav-verified-action',
  template: `
    <mat-icon
      class="pav-verified-icon"
      color="warn"
      [matMenuTriggerFor]="pavVerifiedAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #pavVerifiedAction="matMenu">
      <button mat-menu-item (click)="onViewClick()">
        <mat-icon color="primary">assignment</mat-icon>View
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .pav-verified-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class PAVVerifiedActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  pavSelected: IVCSPlacementVerificationList;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onViewClick() {
    this.pavSelected = this.params.data;
    this.params.context.componentParent.onActionClickParent(this.pavSelected);
   }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
  }
}
