import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { IVCSPlacementVerification, IVCSPlacementVerificationList, IVCSPlacementClientSelected, verificationInput } from './placements-verification-interface.model';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { PlacementsVerificationService } from './placements-verification.service';
import { PAVPendingActionComponent } from './pav-pending-action.component';
import { PAVVerifiedActionComponent } from './pav-verified-action.component';
import { MatTabChangeEvent } from '@angular/material';
import { CommonService } from 'src/app/services/helper-services/common.service';

@Component({
  selector: 'app-placements-awaiting-verification',
  templateUrl: './placements-awaiting-verification.component.html',
  styleUrls: ['./placements-awaiting-verification.component.scss']
})
export class PlacementsAwaitingVerificationComponent implements OnInit, OnDestroy {
  @ViewChild('pendingAgGrid') pendingAgGrid: AgGridAngular;
  @ViewChild('verifiedAgGrid') verifiedAgGrid: AgGridAngular;

  pending = 0;
  verificationOverdue = 0;
  moveIns = 0;
  moveOuts = 0;
  cocMoveIns = 0;
  flagClicked = '';

  selectedTab = 0;

  pendingGridApi;
  pendingGridColumnApi;
  pendingColumnDefs;
  verifiedGridApi;
  verifiedGridColumnApi;
  verifiedColumnDefs;

  defaultColDef;
  frameworkComponents;
  context;

  public pendingGridOptions: GridOptions;
  public verifiedGridOptions: GridOptions;
  placementsVerificationData: IVCSPlacementVerification;
  pendingRowData: IVCSPlacementVerificationList[] ;
  verifiedRowData: IVCSPlacementVerificationList[];


  placementsVerificationDataSub: Subscription;

  constructor(
    private placementsVerificationService: PlacementsVerificationService,
    private commonService: CommonService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.pendingGridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.verifiedGridOptions = {
      rowHeight: 35
    } as GridOptions;

    this.pendingColumnDefs = [
      {
        headerName: 'Housing Agency / Site Name',
        field: 'agencySiteName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
        }
      },
      {
        headerName: 'Tenant Name (L,F)',
        field: 'tenantName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantLastName + ', ' + params.data.tenantFirstName;
          } else if (params.data.tenantFirstName && !params.data.tenantLastName) {
            return params.data.tenantLastName;
          } else if (!params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantFirstName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Client#-Referral Date',
        field: 'clientNoReferralDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.clientID + ' - ' + params.data.referralDate;
        }
      },
      {
        headerName: 'Move-In Date',
        field: 'moveInDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },

      {
        headerName: 'Move-Out Date',
        field: 'moveOutDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Verification Reason',
        field: 'verificationReasonDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Status',
        field: 'verificationStatusDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Submitted By-Date',
        field: 'submittedByDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.submittedBy + ' - ' + params.data.submittedDate;
        }
      },
      {
        headerName: 'Assigned-To',
        field: 'verificationAssignedTo',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'pendingActionRenderer'
      }
    ];
    this.verifiedColumnDefs = [
      {
        headerName: 'Housing Agency / Site Name',
        field: 'agencySiteName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
        }
      },
      {
        headerName: 'Tenant Name (L,F)',
        field: 'tenantName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantLastName + ', ' + params.data.tenantFirstName;
          } else if (params.data.tenantFirstName && !params.data.tenantLastName) {
            return params.data.tenantLastName;
          } else if (!params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantFirstName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Client#-Referral Date',
        field: 'clientNoReferralDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.clientID + ' - ' + params.data.referralDate;
        }
      },
      {
        headerName: 'Move-In Date',
        field: 'moveInDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },

      {
        headerName: 'Move-Out Date',
        field: 'moveOutDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Verification Reason',
        field: 'verificationReasonDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Status',
        field: 'verificationStatusDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Submitted By-Date',
        field: 'submittedByDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.submittedBy + ' - ' + params.data.submittedDate;
        }
      },
      {
        headerName: 'Verified By-Date',
        field: 'verifiedByDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.verifiedBy + ' - ' + params.data.verifiedDate;
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'verifiedActionRenderer'
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      pendingActionRenderer: PAVPendingActionComponent,
      verifiedActionRenderer: PAVVerifiedActionComponent
    };
  }

  ngOnInit() {
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    // console.log('tab changed');
    this.selectedTab = tabChangeEvent.index;
    // this.placementsVerificationDataSub = this.placementsVerificationService.getPlacementsVerificationData().subscribe((res: IVCSPlacementVerification) => {
    //     this.placementsVerificationData = res;
    //     this.pending = res.pending;
    //     this.verificationOverdue = res.verificationOverdue;
    //     this.moveIns = res.moveIns;
    //     this.moveOuts = res.moveOuts;
    //     this.cocMoveIns = res.coCMoveIns;
    //     this.pendingRowData = [];
    //     this.verifiedRowData = [];
    //     res.placementVerificationList.forEach(pv => {
    //       if (pv.applicableTab == 1) {
    //         this.pendingRowData.push(pv);
    //       }
    //       if (pv.applicableTab == 2) {
    //         this.verifiedRowData.push(pv);
    //       }
    //     });
    // });
    this.loadData();
  }

  onPendingGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.pendingGridApi = params.api;
    this.pendingGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.pendingGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);
      }
    });
    this.pendingGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
    this.loadData();
  }

  onVerifiedGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.verifiedGridApi = params.api;
    this.verifiedGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.verifiedGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);
      }
    });
    this.verifiedGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
    this.loadData();
  }

  loadData() {
    var input: verificationInput = {
      ApplicableTab: this.selectedTab == 0 ? 1 : 2
    }
    this.placementsVerificationDataSub = this.placementsVerificationService.getPlacementsVerificationData(input).subscribe((res: IVCSPlacementVerification) => {
      this.placementsVerificationData = res;
      this.pending = res.pending;
      this.verificationOverdue = res.verificationOverdue;
      this.moveIns = res.moveIns;
      this.moveOuts = res.moveOuts;
      this.cocMoveIns = res.coCMoveIns;
      // this.pendingRowData = [];
      // this.verifiedRowData = [];
      // res.placementVerificationList.forEach(pv => {
      //   if (pv.applicableTab == 1) {
      //     this.pendingRowData.push(pv);
      //   }
      //   if (pv.applicableTab == 2) {
      //     this.verifiedRowData.push(pv);
      //   }
      // });

      if (this.selectedTab == 0) {
        this.pendingRowData = res.placementVerificationList;
      } else if (this.selectedTab == 1) {
        this.verifiedRowData = res.placementVerificationList;
      }
    });
  }

  onActionClickParent(pavSelected: IVCSPlacementVerificationList) {
    const input: IVCSPlacementClientSelected = {
      clientID: pavSelected.clientID,
      clientSourceType: pavSelected.clientSourceType,
      isPendingVerificationRequired: true
    }
    this.placementsVerificationService.setPlacementClientSelected(input);
    this.router.navigate(['/vcs/client-placements-history']);
  }

  refreshAgGrid(val: string) {
    this.flagClicked = '';
    if (val === 'p') {
      this.pendingGridOptions.api.setFilterModel(null);
    } else if (val === 'v') {
      this.verifiedGridOptions.api.setFilterModel(null);
    }
  }

  onBtExport(val: string) {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: (val === 'p' ? 'PendingPlacementVerificationReport-' : val === 'v' ? 'VerifiedPlacementVerificationReport-' : 'PlacementVerificationReport') + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    if (val === 'p') {
      this.pendingGridApi.exportDataAsExcel(params);
    } else if (val === 'v') {
      this.verifiedGridApi.exportDataAsExcel(params);
    }
  }

  ngOnDestroy() {
    if (this.placementsVerificationDataSub) {
      this.placementsVerificationDataSub.unsubscribe();
    }
  }
}
