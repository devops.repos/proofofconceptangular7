import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { IVCSUpdatePlacementVerification, IVCSClientPlacementsHistoryList, IVCSPlacementClientSelected, IVCSPlacementHistoryClientSearch, IVCSDeletePlacement, verificationInput } from './placements-verification-interface.model';

@Injectable({
  providedIn: 'root'
})
export class PlacementsVerificationService {
  private apiURL = environment.pactApiUrl + 'VCSPlacementVerification';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private placementClientSelected = new BehaviorSubject<IVCSPlacementClientSelected>(null);
  private clientPlacementsHistorySelected = new BehaviorSubject<IVCSClientPlacementsHistoryList>(null);
  private currentClientPlacementsHistoryTabIndex = new BehaviorSubject<number>(0);
  private isClientPlacementFormTabOpen = new BehaviorSubject<boolean>(false);
  private clientPlacementNumber = new BehaviorSubject<number>(0);

  private isClientPlacements = new BehaviorSubject<boolean>(false);
  private isDeletePlacement = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) { }

  //#region Getting the PlacementsVerificationData from api
  getPlacementsVerificationData(tabNo : verificationInput) {
    // return this.http.get(this.apiURL + '/GetPlacementVerification');
    return this.http.post(this.apiURL + '/GetPlacementVerification', JSON.stringify(tabNo), this.httpOptions);
  }
  //#endregion

  //#region Get and Set Placement Client Selected in the grid
  setPlacementClientSelected(pav: IVCSPlacementClientSelected) {
    this.placementClientSelected.next(pav);
  }

  getPlacementClientSelected() {
    return this.placementClientSelected.asObservable();
  }
  //#endregion

  //#region Getting the ClientPlacementsHistoryData from api
  getClientPlacementsHistoryData(input: IVCSPlacementClientSelected) {
    return this.http.post(this.apiURL + '/GetClientPlacementHistory', JSON.stringify(input), this.httpOptions);
  }
  //#endregion

  //#region Getting the PlacementVerificationHistoryData from api
  getPlacementVerificationHistoryData(input: IVCSPlacementClientSelected) {
    return this.http.post(this.apiURL + '/GetClientVerificationHistory', JSON.stringify(input), this.httpOptions);
  }
  //#endregion

  // //#region Get and Set client Placements History Selected in the grid
  // setClientPlacementsHistorySelected(cph: IVCSClientPlacementsHistoryList) {
  //   this.clientPlacementsHistorySelected.next(cph);
  // }

  // getClientPlacementsHistorySelected() {
  //   return this.clientPlacementsHistorySelected.asObservable();
  // }
  // //#endregion

  // //#region Get and Set Current client Placement Form tab Open/Close status
  // setIsClientPlacementFormTabOpen(stat: boolean) {
  //   this.isClientPlacementFormTabOpen.next(stat);
  // }

  // getIsClientPlacementFormTabOpen() {
  //   return this.isClientPlacementFormTabOpen.asObservable();
  // }
  // //#endregion

  // //#region Get and Set Current client Placement Number from the selected CientPlacementsHistory record
  // setClientPlacementNumber(no: number) {
  //   this.clientPlacementNumber.next(no);
  // }
  // getClientPlacementNumber() {
  //   return this.clientPlacementNumber.asObservable();
  // }
  // //#endregion

  // //#region Get and Set Current client Placements History tab index
  // setCurrentClientPlacementsHistoryTabIndex(index: number) {
  //   this.currentClientPlacementsHistoryTabIndex.next(index);
  // }

  // getCurrentClientPlacementsHistoryTabIndex() {
  //   return this.currentClientPlacementsHistoryTabIndex.asObservable();
  // }
  // //#endregion

  //#region Get the List of Agency and Site
  GetHpAgenciesForPlacementVerification() {
    return this.http.get(this.apiURL + '/GetHpAgenciesForPlacementVerification');
  }
  GetHpSitesForPlacementVerification(agencyID: number) {
    return this.http.post(this.apiURL + '/GetHpSitesForPlacementVerification', JSON.stringify(agencyID), this.httpOptions);
  }
  //#endregion

  savePlacementVerification(input: IVCSUpdatePlacementVerification) {
    return this.http.post(this.apiURL + '/UpdatePlacementVerification', JSON.stringify(input), this.httpOptions);
  }

  getPlacementMatchedClient(input: IVCSPlacementHistoryClientSearch) {
    return this.http.post(this.apiURL + '/GetPlacementMatchedClient', JSON.stringify(input), this.httpOptions);
  }

  //#region Get, Set, Delete Placement
  setIsClientPlacements(flag: boolean) {
    this.isClientPlacements.next(flag);
  }
  getIsClientPlacements() {
    return this.isClientPlacements.asObservable();
  }
  //#endregion

  //#region Get, Set, Delete Placement
  setIsDeletePlacement(flag: boolean) {
    this.isDeletePlacement.next(flag);
  }
  getIsDeletePlacement() {
    return this.isDeletePlacement.asObservable();
  }
  deleteVCSPlacement(input: IVCSDeletePlacement) {
    return this.http.post(this.apiURL + '/DeletePlacement', JSON.stringify(input), this.httpOptions);
  }
  //#endregion

  getVCSClientInfoForClientCaseFolder(pactClientID: number) {
    return this.http.post(this.apiURL + '/GetVCSClientInfo', JSON.stringify(pactClientID), this.httpOptions);
  }

}
