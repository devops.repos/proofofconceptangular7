import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { GridOptions } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { TADService } from '../tad/tad.service';
import { TadData, TadSite } from '../tad/tad.model';
import { PTadActionComponent } from './ptad-action.component';
import { VTadActionComponent } from './vtad-action.component';
import { HousingLiasionListComponent } from './housing-liasion-list.component';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { TadService } from '../tad-submission/tad.service';
import { Router } from '@angular/router';
import { PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { CommonService } from 'src/app/services/helper-services/common.service';

@Component({
  selector: 'app-tad-awaiting-verification',
  templateUrl: './tad-awaiting-verification.component.html',
  styleUrls: ['./tad-awaiting-verification.component.scss']
})
export class TadAwaitingVerificationComponent implements OnInit, OnDestroy {

  selectedTab = 0;

  nsGridApi;
  nsGridColumnApi;
  nsColumnDefs;
  nsOverlayNoRowsTemplate;
  nsOverlayLoadingTemplate;
  pGridApi;
  pGridColumnApi;
  pColumnDefs;
  pOverlayNoRowsTemplate;
  pOverlayLoadingTemplate;
  vGridApi;
  vGridColumnApi;
  vColumnDefs;
  vOverlayNoRowsTemplate;
  vOverlayLoadingTemplate;

  defaultColDef;
  frameworkComponents;
  context;


  public nsGridOptions: GridOptions;
  public pGridOptions: GridOptions;
  public vGridOptions: GridOptions;
  nsRowData = []; //: IVCSTenantRosterList[] = [];
  pRowData = []; //: IVCSTenantRosterList[] = [];
  vRowData = [];

  selectedSiteID: number = 0;
  selectedAgencyID: number = 0;
  TAD: TadData;
  TADList: TadSite[];


  reportParams: PACTReportUrlParams;

  getTADListbySiteSub: Subscription;

  constructor(
    private tadService: TADService,
    private confirmDialogService: ConfirmDialogService,
    private tadSubmissionService: TadService,
    private router: Router,
    private commonService: CommonService
  ) {
    this.nsOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Not Submitted TAD list is loading.</span>';
    this.nsOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Not-Submitted TADs Available</span>';
    this.pOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Pending TAD list is loading.</span>';
    this.pOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Pending TADs Available</span>';
    this.vOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Verified TAD list is loading.</span>';
    this.vOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Verified TADs Available</span>';
    this.nsGridOptions = {
      rowHeight: 35,
      headerHeight: 40,
      skipHeaderOnAutoSize: true,
    } as GridOptions;
    this.pGridOptions = {
      rowHeight: 35,
      headerHeight: 40,
      skipHeaderOnAutoSize: true,
    } as GridOptions;
    this.vGridOptions = {
      rowHeight: 35,
      headerHeight: 40,
      skipHeaderOnAutoSize: true,
    } as GridOptions;

    this.nsColumnDefs = [
      {
        headerName: 'Housing Agency Name',
        field: 'hpAgencyName',
        minWidth: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.agencyNo && params.data.agencyName) {
            return params.data.agencyNo + ' - ' + params.data.agencyName;
          } else if (params.data.agencyNo) {
            return params.data.agencyNo;
          } else if (params.data.agencyName) {
            return params.data.agencyName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Housing Site Name',
        field: 'hpSiteName',
        minWidth: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.siteNo && params.data.siteName) {
            return params.data.siteNo + ' - ' + params.data.siteName;
          } else if (params.data.siteNo) {
            return params.data.siteNo;
          } else if (params.data.siteName) {
            return params.data.siteName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Reporting Month',
        field: 'tadMonthName',
        minWidth: 65,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Reporting Year',
        field: 'tadYear',
        minWidth: 65,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Agency Liaison Details',
        field: 'agencyLiaisonDetails',
        minWidth: 140,
        filter: 'agTextColumnFilter',
        cellRenderer: 'housingLiasionRenderer'
      },
      {
        headerName: 'HRA Liaison (L, F)',
        field: 'hraLiaison',
        minWidth: 120,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.tadLiasionFirstName && params.data.tadLiasionLastName) {
            return params.data.tadLiasionLastName + ' ' + params.data.tadLiasionFirstName;
          } else if (params.data.tadLiasionFirstName) {
            return params.data.tadLiasionLastName;
          } else if (params.data.tadLiasionLastName) {
            return params.data.tadLiasionFirstName;
          } else {
            return '';
          }
        }
      },
    ];
    this.pColumnDefs = [
      {
        headerName: 'Housing Agency Name',
        field: 'hpAgencyName',
        minWidth: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.agencyNo && params.data.agencyName) {
            return params.data.agencyNo + ' - ' + params.data.agencyName;
          } else if (params.data.agencyNo) {
            return params.data.agencyNo;
          } else if (params.data.agencyName) {
            return params.data.agencyName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Housing Site Name',
        field: 'hpSiteName',
        minWidth: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.siteNo && params.data.siteName) {
            return params.data.siteNo + ' - ' + params.data.siteName;
          } else if (params.data.siteNo) {
            return params.data.siteNo;
          } else if (params.data.siteName) {
            return params.data.siteName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Reporting Month',
        field: 'tadMonthName',
        minWidth: 65,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Reporting Year',
        field: 'tadYear',
        minWidth: 65,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Submitted By-Date',
        field: 'submittedByDate',
        minWidth: 140,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.submittedBy && params.data.submittedDate) {
            return params.data.submittedBy + ' - ' + params.data.submittedDate;
          } else if (params.data.submittedBy) {
            return params.data.submittedBy;
          } else if (params.data.submittedDate) {
            return params.data.submittedDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Liaison',
        field: 'liaison',
        minWidth: 100,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.tadLiasionFirstName && params.data.tadLiasionLastName) {
            return params.data.tadLiasionLastName + ' ' + params.data.tadLiasionFirstName;
          } else if (params.data.tadLiasionFirstName) {
            return params.data.tadLiasionLastName;
          } else if (params.data.tadLiasionLastName) {
            return params.data.tadLiasionFirstName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'pActionRenderer'
      }
    ];
    this.vColumnDefs = [
      {
        headerName: 'Housing Agency Name',
        field: 'hpAgencyName',
        minWidth: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.agencyNo && params.data.agencyName) {
            return params.data.agencyNo + ' - ' + params.data.agencyName;
          } else if (params.data.agencyNo) {
            return params.data.agencyNo;
          } else if (params.data.agencyName) {
            return params.data.agencyName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Housing Site Name',
        field: 'hpSiteName',
        minWidth: 260,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.siteNo && params.data.siteName) {
            return params.data.siteNo + ' - ' + params.data.siteName;
          } else if (params.data.siteNo) {
            return params.data.siteNo;
          } else if (params.data.siteName) {
            return params.data.siteName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Reporting Month',
        field: 'tadMonthName',
        minWidth: 65,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Reporting Year',
        field: 'tadYear',
        minWidth: 65,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Submitted By-Date',
        field: 'submittedByDate',
        minWidth: 140,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.submittedBy && params.data.submittedDate) {
            return params.data.submittedBy + ' - ' + params.data.submittedDate;
          } else if (params.data.submittedBy) {
            return params.data.submittedBy;
          } else if (params.data.submittedDate) {
            return params.data.submittedDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Verified By-Date',
        field: 'verifiedByDate',
        minWidth: 140,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.verifiedBy && params.data.verifiedDate) {
            return params.data.verifiedBy + ' - ' + params.data.verifiedDate;
          } else if (params.data.verifiedBy) {
            return params.data.verifiedBy;
          } else if (params.data.verifiedDate) {
            return params.data.verifiedDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'vActionRenderer'
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      // filter: true,
      floatingFilter: true,
      cellStyle: { 'white-space': 'normal' },
      autoHeight: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      pActionRenderer: PTadActionComponent,
      vActionRenderer: VTadActionComponent,
      housingLiasionRenderer: HousingLiasionListComponent
    };
  }

  ngOnInit() {
    this.getTADListbySiteSub = this.tadService.getTADListbySite(this.selectedAgencyID.toString(), this.selectedSiteID.toString(), '2').subscribe((res: TadData) => {
      if (res) {
        this.TAD = res;
        this.TADList = this.TAD.tadList;
        this.nsRowData = this.TADList.filter(d => { return d.tadStatusType == 808 || d.tadStatusType == 809 || d.tadStatusType == 810 })
        this.pRowData = this.TADList.filter(d => { return d.tadStatusType == 811 });
        this.vRowData = this.TADList.filter(d => { return d.tadStatusType == 812 });
        // if (this.nsRowData.length > 0) {
        //   setTimeout(() => { this.nsGridApi.sizeColumnsToFit(); }, 100);
        // }
        // if (this.pRowData.length > 0) {
        //   setTimeout(() => { this.pGridApi.sizeColumnsToFit(); }, 100);
        // }
        // if (this.vRowData.length > 0) {
        //   setTimeout(() => { this.vGridApi.sizeColumnsToFit(); }, 100);
        // }
      }
    });
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;
    this.getTADListbySiteSub = this.tadService.getTADListbySite(this.selectedAgencyID.toString(), this.selectedSiteID.toString(), '2').subscribe((res: TadData) => {
      if (res) {
        this.TAD = res;
        this.TADList = this.TAD.tadList;
        this.nsRowData = this.TADList.filter(d => { return d.tadStatusType == 808 || d.tadStatusType == 809 || d.tadStatusType == 810 })
        this.pRowData = this.TADList.filter(d => { return d.tadStatusType == 811 });
        this.vRowData = this.TADList.filter(d => { return d.tadStatusType == 812 });
        // if (this.nsRowData.length > 0) {
        //   setTimeout(() => { this.nsGridApi.sizeColumnsToFit(); }, 100);
        // }
        // if (this.pRowData.length > 0) {
        //   setTimeout(() => { this.pGridApi.sizeColumnsToFit(); }, 100);
        // }
        // if (this.vRowData.length > 0) {
        //   setTimeout(() => { this.vGridApi.sizeColumnsToFit(); }, 100);
        // }
      }
    });
  }

  onNSGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.nsGridApi = params.api;
    this.nsGridColumnApi = params.columnApi;
    // setTimeout(() => { this.nsGridApi.sizeColumnsToFit(); }, 50);
    const allColumnIds = [];
    this.nsGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action' && column.colId !== 'siteName') {
        allColumnIds.push(column.colId);
      }
    });
    this.nsGridColumnApi.autoSizeColumns(allColumnIds, true);
    this.nsGridApi.sizeColumnsToFit(allColumnIds);
  }
  onPGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.pGridApi = params.api;
    this.pGridColumnApi = params.columnApi;
    // setTimeout(() => { this.pGridApi.sizeColumnsToFit(); }, 50);
    const allColumnIds = [];
    this.pGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action' && column.colId !== 'siteName') {
        allColumnIds.push(column.colId);
      }
    });
    this.pGridColumnApi.autoSizeColumns(allColumnIds, true);
    this.pGridApi.sizeColumnsToFit(allColumnIds);
  }
  onVGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.vGridApi = params.api;
    this.vGridColumnApi = params.columnApi;
    // setTimeout(() => { this.vGridApi.sizeColumnsToFit(); }, 50);
    const allColumnIds = [];
    this.vGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action' && column.colId !== 'siteName') {
        allColumnIds.push(column.colId);
      }
    });
    this.vGridColumnApi.autoSizeColumns(allColumnIds, true);
    this.vGridApi.sizeColumnsToFit(allColumnIds);
  }

  onVerifyTADClickParent(cell: TadSite) {
    var selectedTadDate = new Date(cell.tadYear, cell.tadMonth - 1, 1);
    var prevTadNotVerifiedDates = [];
    var selectedSiteTads: TadSite[];

    selectedSiteTads = this.pRowData.filter(d => { return d.siteID == cell.siteID });
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    selectedSiteTads.forEach(d => {
      var pendingTadDate = new Date(d.tadYear, d.tadMonth - 1, 1);
      if (pendingTadDate < selectedTadDate) {
        prevTadNotVerifiedDates.push(pendingTadDate);
      }
    });

    if (prevTadNotVerifiedDates.length > 0) {
      const minDate = new Date(Math.min(...prevTadNotVerifiedDates));
      const title = 'Warning';
      const primaryMessage = '';
      const secondaryMessage = "Please Verify  the TAD for " + monthNames[minDate.getMonth()] + ' ' + minDate.getFullYear() + " before Verifying this TAD.";
      const confirmButtonName = 'OK';
      const dismissButtonName = '';

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          (positiveResponse) => { },
          (negativeResponse) => { },
        );
    }
    else {
      this.tadSubmissionService.setTadVSelectedHpSiteID(cell);
      // this.tadSubmissionService.setIsTadWorkflow(false);
      this.router.navigate(['/vcs/tad-verification']);
    }
  }

  onSelectedVerified(cell: TadSite) {
    this.reportParams = { reportParameterID: cell.verifiedTADReportID, reportName: 'VerifiedTADReport', "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.commonService.OpenWindow(URL.createObjectURL(data));
            this.commonService.setIsOverlay(false);
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }
  onSelectedTransmit(cell: TadSite) {
    this.reportParams = { reportParameterID: cell.transmittedTADReportID, reportName: 'TransmittedTADReport', "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.commonService.OpenWindow(URL.createObjectURL(data));
            this.commonService.setIsOverlay(false);
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }

  refreshAgGrid(val: string) {
    if (val === 'ns') {
      this.nsGridOptions.api.setFilterModel(null);
    } else if (val === 'p') {
      this.pGridOptions.api.setFilterModel(null);
    } else if (val === 'v') {
      this.vGridOptions.api.setFilterModel(null);
    }
  }

  onBtExport(val: string) {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: (val === 'ns' ? 'NotSubmittedTADReport-' : val === 'p' ? 'PendingTADReport-' : 'VerifiedTADReport') + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    if (val === 'ns') {
      this.nsGridApi.exportDataAsExcel(params);
    } else if (val === 'p') {
      this.pGridApi.exportDataAsExcel(params);
    } else if (val === 'v') {
      this.vGridApi.exportDataAsExcel(params);
    }
  }

  ngOnDestroy() {
    if (this.getTADListbySiteSub) {
      this.getTADListbySiteSub.unsubscribe();
    }
  }

}
