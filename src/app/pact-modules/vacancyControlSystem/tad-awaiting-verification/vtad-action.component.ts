import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-vtad-action',
  template: `
    <mat-icon
      class="vtad-icon"
      color="warn"
      [matMenuTriggerFor]="vtadAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #vtadAction="matMenu">
      <button mat-menu-item
        (click)="onTransmittedTADClick()">
        <mat-icon color="primary">assignment</mat-icon>Transmitted TAD
      </button>
      <button mat-menu-item
        (click)="onHRAVerifiedTADClick()">
        <mat-icon color="primary">assignment</mat-icon>HRA Verified TAD
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .vtad-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class VTadActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onTransmittedTADClick() {
    this.params.context.componentParent.onSelectedTransmit(this.params.data);
  }
  onHRAVerifiedTADClick() {
    this.params.context.componentParent.onSelectedVerified(this.params.data);
  }

  // onAssignTenantToUnitClick() {
  //   this.trSelected = this.params.data;
  //   this.params.context.componentParent.onAssignTenantToUnitClickParent(this.trSelected);
  //  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() { }
}
