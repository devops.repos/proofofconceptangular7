import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-ptad-action',
  template: `
    <mat-icon
      class="ptad-icon"
      color="warn"
      [matMenuTriggerFor]="ptadAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #ptadAction="matMenu">
      <button mat-menu-item
        (click)="onVerifyTADClick()">
        <mat-icon color="primary">assignment</mat-icon>Verify TAD
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .ptad-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class PTadActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onVerifyTADClick() {
    this.params.context.componentParent.onVerifyTADClickParent(this.params.data);
   }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() { }
}
