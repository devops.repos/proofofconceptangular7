import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-housing-liasion',
  template: `
    <span *ngIf="params.data.housingLiasionList.length > 0">
      <span *ngFor="let liasion of params.data.housingLiasionList; index as i; last as isLast">
        <span class="housing-liasions" matTooltip="{{'name: ' + liasion.lastName + ' ' + liasion.firstName + '\n phone: ' + liasion.phone + '\n Email: ' + liasion.email }}" matTooltipClass="vcs-info-tooltip">{{liasion.lastName + ' ' + liasion.firstName}}
          <span *ngIf="params.data.housingLiasionList.length > 0 && !isLast">,&nbsp;</span>
        <span>
      </span>
    </span>
  `,
  styles: [
    `
      .housing-liasions {
        cursor: pointer;
      }
      .housing-liasions:hover {
        color: blue;
      }
    `
  ]
})
export class HousingLiasionListComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() { }
}
