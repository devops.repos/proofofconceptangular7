import { Component, OnInit, OnDestroy } from '@angular/core';
import { TadSite } from '../../tad/tad.model';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import { IVCSTadSummary, IVCSTadSummaryList, IVCSTadDetailsList } from '../../tad-submission/tad-interface.model';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { TadService } from '../../tad-submission/tad.service';
import { ToastrService } from 'ngx-toastr';
import { TadOfflineInfoIconComponent } from '../../tad-submission/tad-hp/offline-info-icon.component';
import { TadMoveOutInfoIconComponent } from '../../tad-submission/tad-hp/moveOut-info-icon.component';
import { TadDiscrepancyIconComponent } from '../../tad-submission/tad-hp/discrepancy-icon.component';
import { TadVerificationActionComponent } from './tad-verification-action.component';
import { IVCSPlacementClientSelected } from '../../placements-awaiting-verification/placements-verification-interface.model';
import { PlacementsVerificationService } from '../../placements-awaiting-verification/placements-verification.service';
import { CommonService } from 'src/app/services/helper-services/common.service';

@Component({
  selector: 'app-tad-verification',
  templateUrl: './tad-verification.component.html',
  styleUrls: ['./tad-verification.component.scss']
})
export class TadVerificationComponent implements OnInit, OnDestroy {

  selectedHpSite: TadSite;

  sGridApi;
  sGridColumnApi;
  sColumnDefs;
  tadGridApi;
  tadGridColumnApi;
  tadColumnDefs;

  sdefaultColDef;
  defaultColDef;
  frameworkComponents;
  context;

  public sGridOptions: GridOptions;
  public tadGridOptions: GridOptions;
  tadSummaryData: IVCSTadSummary;
  sRowData: IVCSTadSummaryList[] = [];
  tadRowData: IVCSTadDetailsList[] = [];

  pendingPlacement: IVCSTadDetailsList[] = [];
  readyForVerification = false;

  tadSelectedHpSiteIDSub: Subscription;

  constructor(
    private router: Router,
    private confirmDialogService: ConfirmDialogService,
    private tadService: TadService,
    private toastr: ToastrService,
    private placementsVerificationService: PlacementsVerificationService,
    private commonService: CommonService
  ) {
    this.sGridOptions = {
      rowHeight: 35,
    } as GridOptions;
    this.tadGridOptions = {
      rowHeight: 35,
      headerHeight: 50,
      skipHeaderOnAutoSize: true,
      getRowStyle(params) {
        if (params.data.verificationStatusType == 519 || params.data.verificationStatusType == 522) {
          return { background: 'sandybrown' };
        }
        if (params.data.unitStatusType == 473) {
          return { background: '#E5E8E8', color: 'rgba(0, 0, 0, 0.38)' };
        }
      }
    } as GridOptions;
    this.sColumnDefs = [
      {
        headerName: 'Primary Service Contract',
        field: 'primaryFundingSource',
        minWidth: 400,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Total Units',
        field: 'units',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Occupied',
        field: 'unitsOccupied',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Discrepancies',
        field: 'discrepancies',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Offline',
        field: 'unitsOffline',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Vacant',
        field: 'unitsVacant',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Move-Ins',
        field: 'moveIns',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Move-Outs',
        field: 'moveOuts',
        minWidth: 100,
        filter: 'agNumberColumnFilter'
      },

    ];
    this.tadColumnDefs = [
      {
        headerName: 'Unit#',
        field: 'unitName',
        width: 80,
        suppressSizeToFit: true,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Primary Service Contract',
        field: 'primaryFundingSource',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Rental Subsidies',
        field: 'secondaryFundingSource',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Client# - ReferralDate',
        field: 'clientNoReferralDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.clientID && params.data.referralDate) {
            return params.data.clientID + ' - ' + params.data.referralDate;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Tenant Name (L,F)',
        field: 'tenantName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.tenantFirstName && params.data.tenantLastName && params.data.footNotes) {
            return params.data.tenantLastName + ', ' + params.data.tenantFirstName + ' ' + params.data.footNotes;
          } else if (params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantLastName + ', ' + params.data.tenantFirstName;
          } else if (params.data.tenantFirstName && !params.data.tenantLastName) {
            return params.data.tenantLastName + ' ' + params.data.footNotes;
          } else if (!params.data.tenantFirstName && params.data.tenantLastName) {
            return params.data.tenantFirstName + ' ' + params.data.footNotes;
          } else {
            return '';
          }
        },
        // cellRendererSelector(params) {
        //   const discrepancyIcon = {
        //     component: 'discrepancyIconRenderer'
        //   };
        //   if (params.data.discrepancyType > 0 || params.data.priorityPlacementType > 0) {
        //     return discrepancyIcon;
        //   } else {
        //     return null;
        //   }
        // }
      },
      {
        headerName: 'Referring Agency/Site',
        field: 'raAgencySiteName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.raAgencyID && params.data.raSiteID) {
            return params.data.raAgencyNo + ' - ' + params.data.raAgencyName + ' / ' + params.data.raSiteNo + ' - ' + params.data.raSiteName;
          } else if (params.data.raAgencyID) {
            return params.data.raAgencyNo + ' - ' + params.data.raAgencyName;
          } else if (params.data.raSiteID) {
            return params.data.raSiteNo + ' - ' + params.data.raSiteName;
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Tenant Eligibility',
        field: 'tenantEligibility',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Move-In Date',
        field: 'moveInDate',
        width: 90,
        suppressSizeToFit: true,
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Move-Out Date',
        field: 'moveOutDate',
        width: 120,
        suppressSizeToFit: true,
        filter: 'agDateColumnFilter',
        cellRendererSelector(params) {
          const infoIcon = {
            component: 'moveOutInfoIconRenderer'
          };
          if (params.data.moveOutDate) {
            return infoIcon;
          } else {
            return null;
          }
        },
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      // {
      //   headerName: 'Unit Status',
      //   field: 'unitStatusTypeDescription',
      //   width: 120,
      //   suppressSizeToFit: true,
      //   filter: 'agTextColumnFilter',
      //   cellRendererSelector(params) {
      //     const infoIcon = {
      //       component: 'offlineInfoIconRenderer'
      //     };
      //     if (params.data.unitStatusType == 473) {
      //       return infoIcon;
      //     } else {
      //       return null;
      //     }
      //   }
      // },
      {
        headerName: 'Verification Status',
        field: 'verificationStatusDescription',
        width: 180,
        suppressSizeToFit: true,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRendererSelector(params) {
          const actionButton = {
            component: 'vActionRenderer'
          };
          if (params.data.verificationStatusType == 519 || params.data.verificationStatusType == 522) {
            return actionButton;
          } else {
            return 'n/a';
          }
        }
      }
    ];

    this.sdefaultColDef = {
      sortable: true,
      resizable: true,
    };
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      floatingFilter: true,
      cellStyle: { 'white-space': 'normal' },
      autoHeight: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      offlineInfoIconRenderer: TadOfflineInfoIconComponent,
      moveOutInfoIconRenderer: TadMoveOutInfoIconComponent,
      discrepancyIconRenderer: TadDiscrepancyIconComponent,
      vActionRenderer: TadVerificationActionComponent
    };
  }

  ngOnInit() {
    /* Set IsTadWorkFlow flag to true */
    //  this.tadService.setIsTadWorkflow(false);
    this.tadService.setIsTadVerificationWorkflow(true);

    /* Get the HpSiteID selected in TAD Awaiting Verification page */
    this.tadSelectedHpSiteIDSub = this.tadService.getTadVSelectedHpSiteID().subscribe((site: TadSite) => {
      if (site.siteID > 0) {
        this.selectedHpSite = site;

        /* Get the TAD summary */
        const tadSummaryInput: IVCSTadSummary = {
          vcstadid: this.selectedHpSite.vcsTadID,
          actionID: 1   // 1 = Get, 2 = submit
        }
        this.tadService.getAndSaveTadSummary(tadSummaryInput).subscribe((sum: IVCSTadSummary) => {
          if (sum.vcstadid > 0) {
            this.tadSummaryData = sum;
            // console.log(this.tadSummaryData);
            if (sum.tadSummary.length > 0) {
              this.sRowData = sum.tadSummary;
              setTimeout(() => { this.sGridApi.sizeColumnsToFit(); }, 50);
            }
            if (sum.tadDetails.length > 0) {
              this.tadRowData = sum.tadDetails;
              this.pendingPlacement = this.tadRowData.filter(d => (d.verificationStatusType == 519 || d.verificationStatusType == 522));
              if (this.pendingPlacement.length > 0) {
                this.readyForVerification = false;
              } else {
                this.readyForVerification = true;
              }
              setTimeout(() => { this.tadGridApi.sizeColumnsToFit(); }, 50);
            }
          }
        });
      } else {
        this.router.navigate(['/vcs/tad-awaiting-verification']);
      }
    });
  }

  onSGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.sGridApi = params.api;
    this.sGridColumnApi = params.columnApi;
  }
  onTadGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.tadGridApi = params.api;
    this.tadGridColumnApi = params.columnApi;
  }
  refreshAgGrid(val: string) {
    this.tadGridOptions.api.setFilterModel(null);
  }
  onBtExport(val: string) {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'TADVerificationReport-' + date + '-' + time
    };
    this.tadGridApi.exportDataAsExcel(params);
  }

  onVerifyPlacementClickParent(cell: IVCSTadDetailsList) {
    // alert(cell);
    const input: IVCSPlacementClientSelected = {
      clientID: cell.clientID,
      clientSourceType: cell.clientSourceType,
      isPendingVerificationRequired: true
    }
    this.placementsVerificationService.setPlacementClientSelected(input);
    this.router.navigate(['/vcs/client-placements-history']);
  }

  onExitClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        this.tadService.setIsTadVerificationWorkflow(false);
        this.router.navigate(['/vcs/tad-awaiting-verification']);
      },
      negativeResponse => { }
    );
  }

  VerifyTadReport() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to Verify the TAD? `;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        /* Ready for TAD Verification */
        const tadSummaryInput: IVCSTadSummary = {
          vcstadid: this.selectedHpSite.vcsTadID,
          actionID: 3   // 1 = Get, 2 = submit, 3 = Verify
        }
        this.tadService.getAndSaveTadSummary(tadSummaryInput).subscribe((sum: IVCSTadSummary) => {
          if (sum.vcstadid > 0) {
            this.toastr.success('TAD has been Verified.');
            this.router.navigate(['/vcs/tad-awaiting-verification']);
          }
        });
      },
      negativeResponse => { }
    );

  }

  ngOnDestroy() {
    if (this.tadSelectedHpSiteIDSub) {
      this.tadSelectedHpSiteIDSub.unsubscribe();
    }
  }
}
