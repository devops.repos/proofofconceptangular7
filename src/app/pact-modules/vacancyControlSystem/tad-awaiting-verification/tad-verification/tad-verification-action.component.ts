import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-ptad-action',
  template: `
    <mat-icon
      class="tad-v-icon"
      color="warn"
      [matMenuTriggerFor]="tadVAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #tadVAction="matMenu">
      <button mat-menu-item
        (click)="onVerifyPlacementClick()">
        <mat-icon color="primary">assignment</mat-icon>Verify Placement
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .tad-v-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class TadVerificationActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onVerifyPlacementClick() {
    this.params.context.componentParent.onVerifyPlacementClickParent(this.params.data);
   }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() { }
}
