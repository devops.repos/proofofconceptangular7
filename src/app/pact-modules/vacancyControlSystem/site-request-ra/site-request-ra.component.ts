import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import {  FormGroup, FormBuilder } from '@angular/forms';
import { CommonService } from './../../../services/helper-services/common.service';
import { RaSiteRequestService } from './site-request-ra.service';
import { housingProgram, UserOptions } from "../agency-site-maintenance/agency-site-model";
import {SiteRequestEventService} from '../site-request/site-request-event.service';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { SiteInfoReviewComponent } from '../agency-site-request-review-ra/site-info-review.component';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent, SELECT_PANEL_INDENT_PADDING_X } from '@angular/material';
import { RaSiteRequestModel } from "./site-request-ra.model";
import { RefGroupDetails } from './../../../models/refGroupDetailsDropDown.model';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import { ConfirmDialogService } from  '../../../shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-site-request-ra',
  templateUrl: './site-request-ra.component.html',
  styleUrls: ['./site-request-ra.component.scss'],
})
export class RequestRaSiteComponent implements OnInit, OnDestroy {
 
  requestStatusTypes: RefGroupDetails[];
  housingProgramData:housingProgram[];
  siteRequestID: number;

  raSiteRequestModel : RaSiteRequestModel;
  agencyFullAddress : string = '';
  siteFullAddress : string = '';
  
  useroptions: UserOptions ={
    RA : false,
    isPE : false,
    isHP : false,
    isIH : false,
    isDisableForHP:false,
    isDisableForInactiveSite:false,
    isdisableActiveRadiobutton:false,
    isActive  : false,
    optionUserId:0,
    isNewPage:false
  };
 
  tabSelectedIndex: number = 0;
  userData: AuthData;
  showValidationFlag : boolean = false;

  userDataSub: Subscription;
  siteDemographicSub: Subscription;
  evtSvcSub3: Subscription;
  siteRequestSvcSub1: Subscription;
  siteAdminSvcSub1: Subscription;
  agencySiteReviewSaveSub: Subscription;

  requestSiteInfoFormIsValid : boolean = false;
  requestSiteContactsIsValid : boolean = false;

  requestReviewRaFormGroup: FormGroup;

  @ViewChild(SiteInfoReviewComponent) siteInfoReviewComponent : SiteInfoReviewComponent;

  
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private raSiteRequestService : RaSiteRequestService,
    private siteRequestEventSvc : SiteRequestEventService,
    private siteAdminSvc : SiteAdminService,
    private userService :UserService,
    private commonService : CommonService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private confirmDialogService: ConfirmDialogService,
  ) {

    this.housingProgramData = [];

    this.raSiteRequestModel = {
        agencyID: 0,
        agencyName : "",
        agencyAddress : "",
        agencyCity  : "",
        agencyState : "",
        agencyZip : "",
    
        siteRequestID : 0,
        siteName : "",
        siteType  : "",
        siteAddress : "",
        siteCity  : "",
        siteState : "",
        siteZip : "",
    
        userId : this.useroptions.optionUserId

        }
        
    }

  ngOnInit() {

    this.getUserRolesInfo();
  
   this.requestReviewRaFormGroup = this.formBuilder.group({

    });
    let that = this;
    this.activatedRoute.paramMap.subscribe(params => {
        that.siteRequestID = Number(params.get('siteReqID'));
    });

    this.evtSvcSub3 = this.siteRequestEventSvc.subscribeSiteRequestSiteContacts().subscribe(res => {
      if(res && res.has(that.siteRequestID)) { 
          that.requestSiteContactsIsValid = res.get(that.siteRequestID); 
      }
      else
        that.requestSiteContactsIsValid = false;
    });

      this.loadHousingPrograms();

      setTimeout(() => {
          that.loadAgencySiteData();
      }, 1000);
      
  }

  loadAgencySiteData()
   {
     let that = this;
      this.siteDemographicSub = this.raSiteRequestService.getRaSiteRequest(that.siteRequestID)
        .subscribe(
          res => {
              that.raSiteRequestModel = res as RaSiteRequestModel;
              if(that.raSiteRequestModel){
                if(!that.raSiteRequestModel.siteAddress)  that.raSiteRequestModel.siteAddress = '';
                if(!that.raSiteRequestModel.siteCity)  that.raSiteRequestModel.siteCity = '';
                if(!that.raSiteRequestModel.siteState)  that.raSiteRequestModel.siteState = '';
                if(!that.raSiteRequestModel.siteZip)  that.raSiteRequestModel.siteZip = '';

                that.agencyFullAddress = that.raSiteRequestModel.agencyAddress + ', ' + that.raSiteRequestModel.agencyCity+ ', ' + that.raSiteRequestModel.agencyState + ', ' + that.raSiteRequestModel.agencyZip;
                that.siteFullAddress = that.raSiteRequestModel.siteAddress + ', ' + that.raSiteRequestModel.siteCity+ ', ' + that.raSiteRequestModel.siteState + ', ' + that.raSiteRequestModel.siteZip;
              }
          },
          error => console.error('Error!', error)
        );
  }

  get getSitesAddress(): string {
    return this.raSiteRequestModel.siteAddress + ', ' + this.raSiteRequestModel.siteCity+ ', ' + this.raSiteRequestModel.siteState + ', ' + this.raSiteRequestModel.siteZip;;
  }

   loadHousingPrograms()
   {
    this.siteAdminSvcSub1 = this.siteAdminSvc.getHousingPrograms()
      .subscribe(
        res => {
          this.housingProgramData = res.body as housingProgram[];
        },
        error => console.error('Error!', error)
      );
   }

    Save()
     {
       this.onSubmit();
       let that = this;
        if(this.validateForm()){
          this.raSiteRequestModel.userId = this.useroptions.optionUserId;

          this.agencySiteReviewSaveSub = this.raSiteRequestService.saveRaSiteRequest(this.raSiteRequestModel).subscribe(
                data => {
                  that.showPopupEmailSentOnSubmit('Initiating a New Site request process.');
                },
                error => {
                  this.toastr.warning('Error while Saving !!', '');
                }
              );
        }

     }

    validateForm() : boolean{

      if(this.siteInfoReviewComponent && this.siteInfoReviewComponent.siteInfoReviewFormGroup.invalid  && this.requestSiteInfoFormIsValid == false){ // viewing siteInfoReviewComponent
          this.toastr.warning("Please enter the data in required fields in Site Information Tab.", "Validation Failed.");
          return false;
      } else if(this.siteInfoReviewComponent && this.siteInfoReviewComponent.siteContactRequestComponent && this.requestSiteContactsIsValid == false ){
          return false;
      } else if (this.siteInfoReviewComponent && this.siteInfoReviewComponent.duplicateSiteExists === true) {
          this.toastr.warning("Please fix the duplicate Site Name.", "Validation Failed.");
          return false;
      }
      return true;
    }

    onSubmit(){

      if(this.siteInfoReviewComponent) this.siteInfoReviewComponent.onSubmit();

    }

    tabChanged(tabChangeEvent: MatTabChangeEvent) {

      // if(this.showValidationFlag){
      //   this.validateForm();
      // }
      // this.showValidationFlag = true; // reset
    }

    getUserRolesInfo()
    {
        this.userDataSub = this.userService.getUserData().subscribe(res => {
          this.userData = res;
          if(this.userData) {
            if(this.userData.siteCategoryType.length > 0) {
              this.useroptions.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
              this.useroptions.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
              this.useroptions.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
              this.useroptions.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
              this.useroptions.optionUserId = this.userData.optionUserId;  
            }
            // if (this.siteSelectedDetail.siteStatus == "InActive")
            // {
            //   this.useroptions.isActive = false;
            //   this.useroptions.isDisableForInactiveSite=true;
            //   this.useroptions.isDisableForHP = true;
            //   if (this.useroptions.isIH == true) {
            //   this.useroptions.isdisableActiveRadiobutton=false;
            //   }
            //   else { 
            //     this.useroptions.isdisableActiveRadiobutton=true;
            //   }
            // }
            // else
            // {
              this.useroptions.isActive = true;
              if (this.useroptions.isIH == true) {
                this.useroptions.isDisableForInactiveSite=false;
                this.useroptions.isDisableForHP = false;
                this.useroptions.isdisableActiveRadiobutton=false;
              }
              else{
                this.useroptions.isDisableForInactiveSite=false;
                this.useroptions.isDisableForHP = true;
                this.useroptions.isdisableActiveRadiobutton=true;
              }
            // }

            }  
          }
        );
    }

    showPopupEmailSentOnSubmit(msg : string) {
      let that = this;
      const title = 'Confirmation';
      const primaryMessage = '';
      const secondaryMessage = msg; //`An email has been sent to the user to request a new agency/site.`;
      const confirmButtonName = 'Ok';
      const dismissButtonName = null;

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          (positiveResponse) => {
              that.toastr.success('Successfully Saved !!', '');
              that.router.navigate(['/admin/agency-site-maintenance']);
          },
          (negativeResponse) => console.log(),
        );
    }

    ngOnDestroy() {

      if(this.siteDemographicSub) this.siteDemographicSub.unsubscribe();
      if(this.userDataSub) this.userDataSub.unsubscribe();
      if(this.siteAdminSvcSub1) this.siteAdminSvcSub1.unsubscribe();
      if(this.agencySiteReviewSaveSub) this.agencySiteReviewSaveSub.unsubscribe();
      if(this.evtSvcSub3) this.evtSvcSub3.unsubscribe();
      if(this.siteRequestSvcSub1) this.siteRequestSvcSub1.unsubscribe();
      
    }

}