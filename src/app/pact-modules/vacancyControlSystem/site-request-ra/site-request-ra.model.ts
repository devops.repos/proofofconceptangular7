export interface RaSiteRequestModel {

    agencyID: number;
    agencyName : string;
    agencyAddress : string;
    agencyCity : string;
    agencyState : string;
    agencyZip : string;
 
    siteRequestID?: number;
    siteName : string;
    siteType : string;
    siteAddress : string;
    siteCity : string;
    siteState : string;
    siteZip : string;

    userId: number;
     
   }
 