import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { RaSiteRequestModel } from "./site-request-ra.model";
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class RaSiteRequestService {

  private getRaSiteRequestURL = environment.pactApiUrl + 'Site/GetRaSiteRequest';
  private newRaSiteRequestURL = environment.pactApiUrl + 'Site/NewRaSiteRequest';
  private saveRaSiteRequestURL = environment.pactApiUrl + 'Site/SaveRaSiteRequest';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }

  saveRaSiteRequest(raSiteRequestModel : RaSiteRequestModel) : Observable<any>
  {
    return this.httpClient.post(this.saveRaSiteRequestURL, JSON.stringify(raSiteRequestModel), this.httpOptions);
  }

  NewRaSiteRequest(agencyID : number, userID : number): Observable<any>  {
    return this.httpClient.post(this.newRaSiteRequestURL, JSON.stringify({ "AgencyID" : agencyID, "UserID" : userID}), this.httpOptions);
  }

  getRaSiteRequest(siteRequestId : number): Observable<any> {
    return this.httpClient.post(this.getRaSiteRequestURL, JSON.stringify(siteRequestId), this.httpOptions);
  }

}