import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { IV2CReferralHandshake, IVacancyListing, IVCSUpdatedInterviewAddress } from '../v2c-interface.model';
import { IVCSSiteCalendarEvents, IVCSAgencySiteInfoForCalendar } from 'src/app/shared/calendar/pact-calendar-interface.model';
import { V2cService } from '../v2c.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PactCalendarService } from 'src/app/shared/calendar/pact-calendar.service';
import { C2vInterviewDatePickerComponent } from '../../client-awaiting-placement/c2v-scheduler/c2v-interview-date-picker.component';
import { C2vInterviewTimePickerComponent } from '../../client-awaiting-placement/c2v-scheduler/c2v-interview-time-picker.component';
import { C2vSchedulerActionComponent } from '../../client-awaiting-placement/c2v-scheduler/c2v-scheduler-action.component';
import { VCSAvailableInterviewTimeSlotInput, VCSAvailableInterviewTimeSlot } from '../../client-awaiting-placement/c2v-interfaces.model';
import { C2vService } from '../../client-awaiting-placement/c2v.service';
import { Subscription } from 'rxjs';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

export interface IC2VSchedulerData {
  site: string;
  interviewDate: string;
  interviewTime: string;
}

@Component({
  selector: 'app-v2c-scheduler',
  templateUrl: './v2c-scheduler.component.html',
  styleUrls: ['./v2c-scheduler.component.scss']
})
export class V2cSchedulerComponent implements OnInit, OnDestroy {

  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  frameworkComponents;

  context;

  public gridOptions: GridOptions;
  rowData: IV2CReferralHandshake[];

  vacancyListingSelected: IVacancyListing;
  hpSiteID = 0;
  agencySiteInfo: IVCSAgencySiteInfoForCalendar;
  v2cEvents: IVCSSiteCalendarEvents[];


  vacancyListingSelectedSub: Subscription;
  rowDataSub: Subscription;
  availableInterviewTimeSlotSub: Subscription;

  constructor(
    private c2vService: C2vService,
    private v2cService: V2cService,
    private toastr: ToastrService,
    private router: Router,
    private pactCalendarService: PactCalendarService,
    private confirmDialogService: ConfirmDialogService
  ) {
    this.gridOptions = {
      rowHeight: 36,
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Client selected for Referral (L, F)',
        field: 'client',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName;
        }
      },
      {
        headerName: 'Interview Date',
        field: 'changedInterviewDate',
        width: 190,
        headerTooltip: 'Select the Interview Date for each site below',
        cellRenderer: 'interviewDatePickerRenderer',
      },
      {
        headerName: 'InterviewTime',
        field: 'changedInterviewTime',
        width: 210,
        headerTooltip: 'Select the Interview Time from the dropdown',
        cellRenderer: 'interviewTimePickerRenderer',
      },
      {
        headerName: 'Interview Address',
        field: 'intrvwAddress',
        valueGetter(params) {
          if (params.data.interviewAddress && params.data.interviewCity && params.data.interviewState && params.data.interviewZip) {
            return params.data.interviewAddress + ', ' + params.data.interviewCity + ', ' + params.data.interviewState + ' ' + params.data.interviewZip
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Site Phone #',
        field: 'interviewPhoneNo',
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];
    this.defaultColDef = {
      // sortable: true,
      // resizable: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      interviewDatePickerRenderer: C2vInterviewDatePickerComponent,
      interviewTimePickerRenderer: C2vInterviewTimePickerComponent,
      actionRenderer: C2vSchedulerActionComponent
    };
  }

  ngOnInit() {
    /** Get the selected Site for referral */
    this.vacancyListingSelectedSub = this.v2cService.getVacancyListingSelected().subscribe((res: IVacancyListing) => {
      if (res) {
        this.vacancyListingSelected = res;
        this.hpSiteID = res.hpSiteID;
        this.agencySiteInfo = {
          agencyID: this.vacancyListingSelected.hpAgencyID,
          agencyNo: this.vacancyListingSelected.hpAgencyNo.toString(),
          agencyName: this.vacancyListingSelected.hpAgencyName,
          siteID: this.vacancyListingSelected.hpSiteID,
          siteNo: this.vacancyListingSelected.hpSiteNo,
          siteName: this.vacancyListingSelected.hpSiteName
        }
      } else {
        /* If user refresh(reload) the page the Site selected data will be empty, so redirect them back to vacancy-listing Page */
        this.router.navigate(['/vcs/vacancy-listing']);
      }
    });
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    /** Getting the data from the C2VService for the list of site selected */
    this.rowDataSub = this.v2cService.getReferralClientSelectedList().subscribe((res: IV2CReferralHandshake[]) => {
      if (res) {
        this.rowData = res;
        if (this.vacancyListingSelected) {
          /* this value is being assigned for edit functionality of editable calender events
          to check if the calendar event is already in the ag grid (new interview being scheduled) or not */
          this.rowData.forEach(r => {
            // below field is for calendar event
            r.siteAgreementPopulationID = this.vacancyListingSelected.siteAgreementPopulationID;
          });
        }
      }
      // console.log('ag-grid data : ', this.rowData);
    });

  }

  selectedInterviewDateInParentPage(cell) {
    // alert('Site Info with InterviewDateTime : ' + JSON.stringify(cell));
    // console.log(cell);
    if (cell.changedInterviewDate) {
      cell.interviewDate = cell.changedInterviewDate;
      const sDate = (cell.changedInterviewDate.getMonth() + 1) + '/' + cell.changedInterviewDate.getDate() + '/' + cell.changedInterviewDate.getFullYear();
      const inputValue: VCSAvailableInterviewTimeSlotInput = {
        HPSiteID: this.hpSiteID,
        InterviewDate: sDate
      };
      /** Getting the available Interview Time slot from database for the select HPSite */
      this.availableInterviewTimeSlotSub = this.c2vService.GetAvailableInterviewTimeSlot(inputValue).subscribe((res: VCSAvailableInterviewTimeSlot[]) => {
        if (res.length > 0) {
          // this.interviewTimes.push(res);
          this.rowData.forEach(val => {
            val.availableInterviewTimeSlot = res;
          });
          const prms = {
            force: true,
            columns: ['interviewTime']
          };
          this.gridApi.refreshCells(prms);
        }
      });
    }
  }

  selectedInterviewTimeInParentPage(cell) {
    // console.log(cell);
    cell.interviewTime = cell.changedInterviewTime;
  }

  onUpdateInterviewAddressClickParentPage(cell: IV2CReferralHandshake) {
    this.rowData.forEach((r: IV2CReferralHandshake) => {
      if (r.pactApplicationID == cell.pactApplicationID) {
        r.interviewAddress = cell.interviewAddress;
        r.interviewCity = cell.interviewCity;
        r.interviewState = cell.interviewState;
        r.interviewZip = cell.interviewZip;
        r.interviewPhoneNo = cell.interviewPhoneNo;
      }
      const prms = {
        force: true,
        columns: ['intrvwAddress', 'interviewPhoneNo']
      };
      this.gridApi.refreshCells(prms);
    });
    // console.log('After update this.rowDate: ', this.rowData);
  }

  vcsReferralInterviewToBeEdited(intrvwToBeEdited: IVCSSiteCalendarEvents) {
    // console.log('Calendar Event to be Edited: ', intrvwToBeEdited);
    const dt = intrvwToBeEdited.interviewDate.split('/');
    const yr = +dt[2];
    const mn = +dt[0];
    const dy = +dt[1];
    const intrvwDate = new Date(yr, mn - 1, dy);
    const toEdit: IV2CReferralHandshake = {
      vcsReferralID: intrvwToBeEdited.vcsReferralID,
      pactApplicationID: intrvwToBeEdited.pactApplicationID,
      referringAgencyID: intrvwToBeEdited.referringAgencyID,
      referringAgencyNo: intrvwToBeEdited.referringAgencyNo,
      referringSiteID: intrvwToBeEdited.referringSiteID,
      referringSiteNo: intrvwToBeEdited.referringSiteNo,
      pactClientID: intrvwToBeEdited.clientID,
      firstName: intrvwToBeEdited.firstName,
      lastName: intrvwToBeEdited.lastName,
      dateOfBirth: intrvwToBeEdited.dateOfBirth,
      referralDate: intrvwToBeEdited.referralDate,
      ssn: intrvwToBeEdited.ssn,
      agencyName: intrvwToBeEdited.hpAgencyName,
      siteName: intrvwToBeEdited.hpSiteName,
      clientEligibleFor: intrvwToBeEdited.clientEligibleFor,
      svaPrioritization: intrvwToBeEdited.svaPrioritization,
      serviceNeeds: intrvwToBeEdited.serviceNeeds,
      approvalFromDate: intrvwToBeEdited.approvalFromDate,
      approvalToDate: intrvwToBeEdited.approvalToDate,
      placementCriteria: intrvwToBeEdited.placementCriteria,
      isReferralPackageReady: intrvwToBeEdited.isReferralPackageReady,
      pendingReferrals: intrvwToBeEdited.pendingReferrals,
      isHUD: intrvwToBeEdited.isHUD,
      isReferralHold: intrvwToBeEdited.isReferralHold,
      matchPercentage: intrvwToBeEdited.matchPercentage,
      assignUnitType: [],
      selectedUnitType: intrvwToBeEdited.assignUnitType,
      assignUnitTypeDescription: intrvwToBeEdited.assignUnitTypeDescription,
      interviewDate: intrvwDate,
      interviewTime: intrvwToBeEdited.interviewTime,
      interviewAddress: intrvwToBeEdited.interviewAddress,
      interviewCity: intrvwToBeEdited.interviewCity,
      interviewState: intrvwToBeEdited.interviewState,
      interviewZip: intrvwToBeEdited.interviewZip,
      interviewPhoneNo: intrvwToBeEdited.interviewPhoneNo,
      changedInterviewDate: intrvwDate,
      changedInterviewTime: intrvwToBeEdited.interviewTime,
      placementMatchCriteria: null,
      availableInterviewTimeSlot: null,
      siteAgreementPopulationID: intrvwToBeEdited.siteAgreementPopulationID
    };
    let duplicate = 0;
    this.rowData.forEach((rec: IV2CReferralHandshake) => {
      if (rec.pactApplicationID == toEdit.pactApplicationID && rec.siteAgreementPopulationID == toEdit.siteAgreementPopulationID) {
        duplicate++;
      }
    });
    // console.log('duplicate : ', duplicate);
    if (duplicate == 0) {
      // console.log('data to be appended to handshake: ', toEdit);
      this.rowData.push(toEdit);
      this.gridApi.updateRowData({ add: [toEdit] });
      this.gridApi.refreshCells();
    } else {
      this.toastr.error('Interview is already Listed in the Grid above', 'Duplicate record!!!');
    }
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onClear() {
    const title = 'Verify';
    const primaryMessage = `Do you want to clear all the interview information entered?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        const arr = [];
        this.agGrid.api.forEachNode((rowNode, index) => {
          rowNode.data.interviewDate = null;
          rowNode.data.interviewTime = null;
          rowNode.data.changedInterviewDate = null;
          rowNode.data.changedInterviewTime = null;
          arr.push(rowNode.data);
        });
        this.rowData = arr;
      },
      negativeResponse => { }
    );
  }

  onNext() {
    const sysDate = new Date();
    const currentYear = sysDate.getFullYear();
    const currentDay = sysDate.getDate();
    const currentMonth = sysDate.getMonth();
    const fDate = new Date(currentYear, (currentMonth + 6), currentDay, sysDate.getHours(), sysDate.getMinutes(), sysDate.getSeconds(), sysDate.getMilliseconds());
    let inValidcount = 0;
    let partialSelection = 0;
    var sameDateTimeCounter = 0;
    var siteNo = '';
    var siteName = '';
    let interviewScheduleCounter = 0;
    /** Validating the Date and Time field */
    this.agGrid.api.forEachNode((rowNode, index) => {
      const intDate = rowNode.data.interviewDate;
      const intTime = rowNode.data.interviewTime;
      const d = new Date(intDate);
      var sDate;
      if (intTime) {
        const splt = intTime.split(':');
        let hrs = +splt[0];
        const mns = +splt[1].substr(0, 2);
        const AMPM = splt[1].substr(2, 2);
        if (AMPM == 'PM' && hrs < 12) { hrs = hrs + 12; }
        if (AMPM == 'AM' && hrs == 12) { hrs = hrs - 12; }

        sDate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), hrs, mns);
      } else {
        sDate = d;
      }

      if ((intDate && !intTime) || (!intDate && intTime )) {
        this.toastr.error('Please enter both date and time for Interview', 'InterviewDate/Time partially field');
        inValidcount++;
        partialSelection++;
      } else if (intDate && intTime) {
        if (sDate < sysDate) {
          this.toastr.error('Interview Date cannot be the Past Date.');
          inValidcount++;
        } else if (sDate > fDate) {
          this.toastr.error('Select Interview Date within 6 months period.');
          inValidcount++;
        } else {
          this.agGrid.api.forEachNode((row, i) => {
            if (row.data.interviewDate && row.data.interviewTime) {
              if (index !== i && (rowNode.data.hpSiteID == row.data.hpSiteID) && (new Date(rowNode.data.interviewDate).getTime() == new Date(row.data.interviewDate).getTime()) && (rowNode.data.interviewTime == row.data.interviewTime)) {
                /* Same interview Date and Time selected for same HPSiteID */
                inValidcount++;
                sameDateTimeCounter++;
                siteNo = row.data.hpSiteNo;
                siteName = row.data.hpSiteName;
              }
            }
            interviewScheduleCounter++;
          });
        }
      }
      if (rowNode.lastChild) {
        if (interviewScheduleCounter == 0 && partialSelection == 0) {
          const title = 'Verify';
          const primaryMessage = `Do you want to proceed without scheduling the interview for one or more site(s)?`;
          const secondaryMessage = ``;
          const confirmButtonName = 'OK';
          const dismissButtonName = 'Cancel';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              if (sameDateTimeCounter > 0) {
                this.toastr.error('Same Interview Date and Time selected for Site \"' + siteNo + ' - ' + siteName + '\". Please select different interviewTime.');
              } else if (inValidcount == 0) {
                var updatedInterviewAddress: IVCSUpdatedInterviewAddress[] = [];
                this.rowData.forEach(r => {
                  var updatedIntAdd: IVCSUpdatedInterviewAddress = {
                    pactApplicationID: r.pactApplicationID,
                    interviewAddress: r.interviewAddress,
                    interviewCity: r.interviewCity,
                    interviewState: r.interviewState,
                    interviewZip: r.interviewZip,
                    interviewPhoneNo: r.interviewPhoneNo
                  }
                  updatedInterviewAddress.push(updatedIntAdd);
                });
                if (updatedInterviewAddress.length > 0) {
                  // console.log('Next click rowData: ', this.rowData);
                  this.v2cService.setV2CUpdatedInterviewAddress(updatedInterviewAddress);
                  this.router.navigate(['/vcs/v2c-referral-handshake-success']);
                } else {
                  this.router.navigate(['/vcs/v2c-referral-handshake-success']);
                }
              }
            },
            negativeResponse => { }
          );
        } else if (inValidcount == 0) {
          var updatedInterviewAddress: IVCSUpdatedInterviewAddress[] = [];
          this.rowData.forEach(r => {
            var updatedIntAdd: IVCSUpdatedInterviewAddress = {
              pactApplicationID: r.pactApplicationID,
              interviewAddress: r.interviewAddress,
              interviewCity: r.interviewCity,
              interviewState: r.interviewState,
              interviewZip: r.interviewZip,
              interviewPhoneNo: r.interviewPhoneNo
            }
            updatedInterviewAddress.push(updatedIntAdd);
          });
          if (updatedInterviewAddress.length > 0) {
            // console.log('Next click rowData: ', this.rowData);
            this.v2cService.setV2CUpdatedInterviewAddress(updatedInterviewAddress);
            this.router.navigate(['/vcs/v2c-referral-handshake-success']);
          } else {
            this.router.navigate(['/vcs/v2c-referral-handshake-success']);
          }
        }
      }
    });
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'v2cScheduler-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

    // console.log('c2vScheduler-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.vacancyListingSelectedSub) {
      this.vacancyListingSelectedSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.availableInterviewTimeSlotSub) {
      this.availableInterviewTimeSlotSub.unsubscribe();
    }
  }

}
