import { Component} from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { V2cInterviewAddressDialogComponent } from './v2c-interview-address-dialog/v2c-interview-address-dialog.component';
import { MatDialog } from '@angular/material';
import { IVCSInterviewAddress } from '../../client-awaiting-placement/c2v-interfaces.model';


@Component({
  selector: 'app-v2c-scheduler-action',
  template: `
    <mat-icon
      class="v2c-scheduler-action-icon"
      color="warn"
      [matMenuTriggerFor]="v2cSchedulerAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #v2cSchedulerAction="matMenu">
      <button mat-menu-item (click)="onUpdateInterviewAddressClick()"><mat-icon color="primary">assignment</mat-icon>Update Interview Address</button>
      <!-- <button mat-menu-item routerLink="/vcs/v2c-scheduler"><mat-icon color="primary">assignment</mat-icon>View</button> -->
    </mat-menu>
  `,
  styles: [
    `
      .v2c-scheduler-action-icon {
        cursor: pointer;
      }
    `
  ]
})
export class V2cSchedulerActionComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onUpdateInterviewAddressClick() {
    const title = 'Verify';
    const primaryMessage = `You are about to change the Interview Address for the selected Referral. `;
    const secondaryMessage = `Are you sure you want to continue?`;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        const dialogRef = this.dialog.open(V2cInterviewAddressDialogComponent, {
          width: '40%',
          panelClass: 'transparent',
          data: this.params.data,
          disableClose: true
        });

        dialogRef.afterClosed().subscribe((updatedIntAddress: IVCSInterviewAddress) => {
          if (updatedIntAddress) {
            console.log('updatedIntAddress: ', updatedIntAddress);
            this.params.data.interviewAddress = updatedIntAddress.interviewAddress;
            this.params.data.interviewCity = updatedIntAddress.interviewCity;
            this.params.data.interviewState = updatedIntAddress.interviewState;
            this.params.data.interviewZip = updatedIntAddress.interviewZip;
            this.params.data.interviewPhoneNo = updatedIntAddress.interviewPhoneNo;
            this.params.context.componentParent.onUpdateInterviewAddressClickParentPage(this.params.data);
          }
        });
      },
      negativeResponse => { }
    );
  }

  refresh(): boolean {
    return false;
  }
}
