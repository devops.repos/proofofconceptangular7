import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { IVCSInterviewAddress } from '../../../client-awaiting-placement/c2v-interfaces.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IStates, States } from 'src/app/models/pact-enums.enum';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IV2CReferralHandshake } from '../../v2c-interface.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

@Component({
  selector: 'app-v2c-interview-address-dialog',
  templateUrl: './v2c-interview-address-dialog.component.html',
  styleUrls: ['./v2c-interview-address-dialog.component.scss']
})
export class V2cInterviewAddressDialogComponent implements OnInit, OnDestroy {

  IntAddressFormGroup: FormGroup;

  states: IStates[] = States;

  commonServiceSub: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<V2cInterviewAddressDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: IV2CReferralHandshake,
    private commonService: CommonService
  ) {
    this.IntAddressFormGroup = this.formBuilder.group({
      intAddressCtrl: ['', Validators.compose([Validators.required])],
      intCityCtrl: ['', Validators.compose([Validators.required])],
      // intStateCtrl: ['', Validators.compose([Validators.required, CustomValidators.dropdownRequired()])],
      intStateCtrl: ['', Validators.compose([Validators.required, Validators.maxLength(2)])],
      intZipCtrl: ['', Validators.compose([Validators.required])],
      intPhoneCtrl: ['']
    });
  }

  ngOnInit() {
    // // Get Refgroup Details
    // const refGroupList = '62';
    // this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
    //   res => {
    //     const data = res.body as RefGroupDetails[];
    //     this.withdrawnReason = data.filter(d => d.refGroupID === 62);
    //   },
    //   error => {
    //     throw new Error(error.message);
    //   }
    // );
    this.IntAddressFormGroup.get('intAddressCtrl').setValue(this.data.interviewAddress.trim());
    this.IntAddressFormGroup.get('intCityCtrl').setValue(this.data.interviewCity.trim());
    this.IntAddressFormGroup.get('intStateCtrl').setValue(this.data.interviewState.trim());
    this.IntAddressFormGroup.get('intZipCtrl').setValue(this.data.interviewZip.trim());
    this.IntAddressFormGroup.get('intPhoneCtrl').setValue(this.data.interviewPhoneNo.trim());
  }

  onUpdateClick() {
    const value: IVCSInterviewAddress = {
      interviewAddress: this.IntAddressFormGroup.get('intAddressCtrl').value,
      interviewCity: this.IntAddressFormGroup.get('intCityCtrl').value,
      interviewState: this.IntAddressFormGroup.get('intStateCtrl').value,
      interviewZip: this.IntAddressFormGroup.get('intZipCtrl').value,
      interviewPhoneNo: this.IntAddressFormGroup.get('intPhoneCtrl').value
    };
    // alert('Reason: ' + value.withdrawnReason + '   comment: ' + value.withdrawnComment);
    this.dialogRef.close(value);
  }

  onCancelClick(): void {
    this.dialogRef.close(null);
  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
  }
}
