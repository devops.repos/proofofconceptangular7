import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { V2cSchedulerComponent } from './v2c-scheduler.component';

describe('V2cSchedulerComponent', () => {
  let component: V2cSchedulerComponent;
  let fixture: ComponentFixture<V2cSchedulerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ V2cSchedulerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(V2cSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
