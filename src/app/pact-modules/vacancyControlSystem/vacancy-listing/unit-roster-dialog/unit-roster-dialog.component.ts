import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VCSUnitRosterDialogData, IVCSOnlineUnitRoster } from '../v2c-interface.model';
import { GridOptions } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { V2cService } from '../v2c.service';

@Component({
  selector: 'app-unit-roster-dialog',
  templateUrl: './unit-roster-dialog.component.html',
  styleUrls: ['./unit-roster-dialog.component.scss']
})
export class UnitRosterDialogComponent implements OnInit, OnDestroy {

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  frameworkComponents;

  public gridOptions: GridOptions;
  totalUnit = 0;
  rowData: IVCSOnlineUnitRoster[];

  rowDataSub: Subscription;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: VCSUnitRosterDialogData,
    private dialogRef: MatDialogRef<UnitRosterDialogComponent>,
    private v2cService: V2cService
  ) {
    this.gridOptions = {
      rowHeight: 35,
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Unit Name',
        field: 'unitName',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Unit Features',
        field: 'unitFeatures',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Rental Subsidies',
        field: 'rentalSubsidies',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Unit Status',
        field: 'unitStatus',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Contracting Agency',
        field: 'contractingAgencyTypeDescription',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Verified By',
        field: 'vcuVerifiedBy',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Verified Date',
        field: 'vcuVerifiedDate',
        filter: 'agTextColumnFilter',
      },

    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.frameworkComponents = { };
  }

  // On Init
  ngOnInit() { }

  onGridReady(params) {
    // params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    /** Getting the data from the C2VService for the list of site selected */
    this.rowDataSub = this.v2cService.getVCSOnlineUnitRoster(this.data.siteAgreemenetPopulationID).subscribe((res: IVCSOnlineUnitRoster[]) => {
      if (res.length > 0) {
        this.rowData = res;
        this.totalUnit = res.length
        setTimeout(() => {
          params.api.sizeColumnsToFit();
        }, 500);
      }
    });
  }

  // Close the dialog on close button
  CloseDialog() {
    this.dialogRef.close(true);
  }

  ngOnDestroy() {
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
  }
}

