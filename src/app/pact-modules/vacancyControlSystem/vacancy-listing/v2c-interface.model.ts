import {
  VCSPlacementMatchCriteria,
  VCSAvailableInterviewTimeSlot, VCSUnitType
} from '../client-awaiting-placement/c2v-interfaces.model';

export interface IVacancyListingAllData {
  nynY1n2: number;
  nynY3: number;
  nyC1515: number;
  coC: number;
  offlineUnits: number;
  vacancyList: IVacancyListing[];
}

export interface IVacancyListing {
  hpAgencyID: number;
  hpAgencyNo: number;
  hpSiteID: number;
  hpSiteNo: string;
  hpAgencyName: string;
  hpSiteName: string;
  primaryServiceContractID: number;
  primaryServiceContractName: string;
  agreementPopulationID: number;
  populationName: string;
  siteAgreementPopulationID: number;
  agreementTypeDescription: string;
  boroughType: number;
  boroughTypeDescription: string;
  siteType: number;
  siteTypeDescription: string;
  siteFeatures: string;
  siteLocationType: number;
  siteLocationTypeDescription: string;
  rentalSubsidies: string;
  siteAddress: string;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhoneNo: string;
  unitsOccupied: number;
  unitsAvailable: number;
  unitsOffline: number;
  totalUnits: number;
  pendingReferrals: number;
  primarySiteContact: string;
  siteContacts: IMoreContacts[];
}

export interface IMoreContacts {
  contactName: string;
  phoneNumber: string;
}

export interface IV2CReferralHandshake {
  vcsClientAwaitingPlacementID?: number;
  vcsReferralID?: number;
  pactApplicationID: number;
  referringAgencyID: number;
  referringAgencyNo: string;
  referringSiteID: number;
  referringSiteNo: string;
  pactClientID: number;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  referralDate: string;
  ssn: string;
  agencyName: string;
  siteName: string;
  clientEligibleFor: string;
  svaPrioritization: string;
  serviceNeeds: string;
  approvalFromDate: string;
  approvalToDate: string;
  placementCriteria: string;
  siteType?: number;
  siteTypeDescription?: string;
  gender?: string;
  ethnicity?: string;
  age?: number;
  borough?: string;
  applicationStatus?: string;
  isReferralPackageReady: number;
  pendingReferrals: number;
  isHUD: boolean;
  isReferralHold: number;
  isActive?: boolean;
  createdBy?: number;
  createdDate?: string;
  updatedBy?: number;
  updatedDate?: string;
  matchPercentage: number;
  placementMatchCriteria: VCSPlacementMatchCriteria[];
  assignUnitType?: VCSUnitType[];
  selectedUnitType: number;
  assignUnitTypeDescription: string;
  interviewDate: any;
  interviewTime: string;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhoneNo: string;
  changedInterviewDate: any;
  changedInterviewTime: string;
  availableInterviewTimeSlot: VCSAvailableInterviewTimeSlot[];
  siteAgreementPopulationID?: number; // this field is added only for calender Edit functionality, to check if the interview scheduled is existing in the ag-grid (new interview being scheduled)
}

export interface VCSUnitRosterDialogData {
  selectedHpSite: number;
  siteAgreemenetPopulationID: number;
}

export interface IVCSOnlineUnitRoster {
  unitName: string;
  primaryServiceContractID: number;
  primaryServiceContractName: string;
  agreementPopulationID: number;
  populationName: string;
  siteAgreementPopulationID: number;
  agreementTypeDescription: string;
  unitFeatures: string;
  rentalSubsidies: string;
  unitType: number;
  unitTypeDescription: string;
  unitStatus: string;
  contractingAgencyTypeDescription: string;
  vcuVerifiedBy: string;
  vcuVerifiedDate: string;
}

export interface IVCSUpdatedInterviewAddress {
  pactApplicationID: number;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhoneNo: string;
}
