import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IVacancyListing } from './v2c-interface.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-vl-action',
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="vlAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #vlAction="matMenu">
      <button mat-menu-item (click)="onVacancySelected()">Refer Clients </button>
      <button mat-menu-item (click)="onUnitRosterClick()">View Unit Roster</button>
    </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class VLActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private vlSelected: IVacancyListing;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onVacancySelected() {
    this.vlSelected = this.params.data;
    this.params.context.componentParent.selectedSiteCatchedInParentPage(this.vlSelected);
  }

  onUnitRosterClick() {
    this.params.context.componentParent.onUnitRosterClickParentPage(this.params.data);
  }

  refresh(): boolean {
    return false;
  }
}
