import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { IV2CReferralHandshake, IVacancyListing, IVCSUpdatedInterviewAddress } from '../v2c-interface.model';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { V2cService } from '../v2c.service';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { PactCalendarService } from 'src/app/shared/calendar/pact-calendar.service';
import { ReferralRosterService } from '../../referral-roster/referral-roster.service';
import { SSNTooltipComponent } from '../../client-awaiting-placement/ssn-tooltip.component';
import { IVCSSiteCalendarEvents } from 'src/app/shared/calendar/pact-calendar-interface.model';
import { VCSReferralData } from '../../client-awaiting-placement/c2v-interfaces.model';
import { C2vService } from '../../client-awaiting-placement/c2v.service';
import { ReferralType, ReferralStatusType, CoCType } from 'src/app/models/pact-enums.enum';
import { Subscription } from 'rxjs';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-v2c-referral-handshake-success',
  templateUrl: './v2c-referral-handshake-success.component.html',
  styleUrls: ['./v2c-referral-handshake-success.component.scss']
})
export class V2cReferralHandshakeSuccessComponent implements OnInit, OnDestroy {

  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  frameworkComponents;
  public gridOptions: GridOptions;
  rowData: IV2CReferralHandshake[];

  makeCoCReferral = false;

  referralType: RefGroupDetails[];
  unitType: RefGroupDetails[];
  referralStatusType: RefGroupDetails[];
  cocType: RefGroupDetails[];
  withdrawnReasonType: RefGroupDetails[];

  vacancyListingSelected: IVacancyListing;
  v2cEvents: IVCSSiteCalendarEvents[];

  commonServiceSub: Subscription;
  vacancyListingSelectedSub: Subscription;
  rowDataSub: Subscription;
  v2cEventSub: Subscription;
  availableInterviewTimeSlotSub: Subscription;
  saveVCSReferralSub: Subscription;
  makeCoCReferralSub: Subscription;

  constructor(
    private c2vService: C2vService,
    private v2cService: V2cService,
    private router: Router,
    private commonService: CommonService,
    private toastr: ToastrService,
    private pactCalendarService: PactCalendarService,
    private referralRosterService: ReferralRosterService,
    private confirmDialogService: ConfirmDialogService
  ) {
    this.gridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Client# - Ref Date',
        field: 'clientNoRefDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.pactClientID + ' - ' + params.data.referralDate;
        },
      },
      {
        headerName: 'Client Name',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.firstName + ' ' + params.data.lastName;
        }
      },
      // {
      //   headerName: 'SSN',
      //   field: 'ssn',
      //   filter: 'agTextColumnFilter',
      //   cellRenderer: 'ssnTooltip'
      // },
      {
        headerName: 'Referring Agency/site#',
        field: 'referringAgencySiteNo',
        // width: 80,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.agencyName !== '' && params.data.siteName !== '') {
            return params.data.referringAgencyNo + ' - ' + params.data.agencyName + ' / ' + params.data.referringSiteNo + ' - ' + params.data.siteName;
          } else if (params.data.agencyName !== '' && params.data.siteName === '') {
            return params.data.referringAgencyNo + ' - ' + params.data.agencyName;
          } else if (params.data.agencyName === '' && params.data.siteName !== '') {
            return params.data.referringSiteNo + ' - ' + params.data.siteName;
          }
        }
      },
      // {
      //   headerName: 'Referring Agency/site#',
      //   field: 'referringAgencySiteNo',
      //   // width: 80,
      //   filter: 'agTextColumnFilter',
      //   valueGetter(params) {
      //     return params.data.referringAgencyNo + ' - ' + params.data.agencyName + ' / ' + params.data.referringSiteNo + ' - ' + params.data.siteName;
      //   }
      // },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        width: 300,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Approval Expiry Date',
        field: 'approvalToDate',
        filter: 'agDateColumnFilter',
        headerTooltip: 'The date till when referral can be approved'
      },
      {
        headerName: 'Placements Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referral Package Ready',
        field: 'isReferralPackageReady',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.isReferralPackageReady === true) {
            return 'Y';
          } else {
            return 'N';
          }
        }
      },
      {
        headerName: 'PendingReferrals',
        field: 'pendingReferrals',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Assign Unit Type',
        field: 'assignUnitTypeDescription',
        width: 210
      },
      {
        headerName: 'Interview Date',
        field: 'interviewDate',
        valueGetter: params => {
          if (params.data.interviewDate) {
            return (
              (params.data.interviewDate.getMonth() + 1) +
              '/' +
              params.data.interviewDate.getDate() +
              '/' +
              params.data.interviewDate.getFullYear()
            );
          }
        }
      },
      {
        headerName: 'Interview Time',
        field: 'interviewTime'
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.frameworkComponents = {
      ssnTooltip: SSNTooltipComponent,
    };
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '29,59,60,61';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.unitType = data.filter(d => d.refGroupID === 29);
        this.referralStatusType = data.filter(d => d.refGroupID === 59);
        this.referralType = data.filter(d => d.refGroupID === 60);
        this.cocType = data.filter(d => d.refGroupID === 61);
      },
      error => {
        throw new Error(error.message);
      }
    );

    /** Get the selected Site for referral */
    this.vacancyListingSelectedSub = this.v2cService.getVacancyListingSelected().subscribe(res => {
      if (res) {
        this.vacancyListingSelected = res;
      } else {
        /* If user refresh(reload) the page the Site selected data will be empty, so redirect them back to vacancy-listing Page */
        this.router.navigate(['/vcs/vacancy-listing']);
      }
    });

    /* Get the makeCoCReferral */
    this.makeCoCReferralSub = this.v2cService.getMakeCoCReferral().subscribe(res => {
      if (res == true) {
        // console.log('makeCoCReferral');
        this.makeCoCReferral = true;
      }
    });
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'assignUnitTypeDescription') {
        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    // params.api.expandAll();

    /** Getting the data from the V2CService for the list of Client selected */
    this.rowDataSub = this.v2cService.getReferralClientSelectedList().subscribe((res: IV2CReferralHandshake[]) => {
      this.rowData = res;
      if (this.vacancyListingSelected) {
        this.v2cService.getV2CUpdatedInterviewAddress().subscribe((i: IVCSUpdatedInterviewAddress[]) => {
          if (i) {
            // console.log('From Service: ', i);
            this.rowData.forEach((r: IV2CReferralHandshake) => {
              var data = i.filter(d => d.pactApplicationID == r.pactApplicationID);
              if (data.length > 0) {
                // console.log('data');
                data.forEach(ii => {
                  r.interviewAddress = ii.interviewAddress;
                  r.interviewCity = ii.interviewCity;
                  r.interviewState = ii.interviewState;
                  r.interviewZip = ii.interviewZip
                  r.interviewPhoneNo = ii.interviewPhoneNo;
                });
              } else {
                r.interviewAddress = this.vacancyListingSelected.interviewAddress;
                r.interviewCity = this.vacancyListingSelected.interviewCity;
                r.interviewState = this.vacancyListingSelected.interviewState;
                r.interviewZip = this.vacancyListingSelected.interviewZip
                r.interviewPhoneNo = this.vacancyListingSelected.interviewPhoneNo;
              }
            });
          } else {
            this.rowData.forEach(r => {
              r.interviewAddress = this.vacancyListingSelected.interviewAddress;
              r.interviewCity = this.vacancyListingSelected.interviewCity;
              r.interviewState = this.vacancyListingSelected.interviewState;
              r.interviewZip = this.vacancyListingSelected.interviewZip
              r.interviewPhoneNo = this.vacancyListingSelected.interviewPhoneNo;
            });
          }
        });
        // console.log('V2ChandshakeScreen rowData: ', this.rowData);
      }
    });
  }

  onSave() {
    const vcsReferralList: VCSReferralData[] = [];
    // console.log('ClientSelected : ', this.clientSelected);
    if (this.vacancyListingSelected) {
      const hpAgencyId = this.vacancyListingSelected.hpAgencyID;
      const hpSiteId = this.vacancyListingSelected.hpSiteID;
      const siteAgreementPopulationId = this.vacancyListingSelected.siteAgreementPopulationID;
      // iterate through every node in the grid
      this.agGrid.api.forEachNode((rowNode, index) => {
        // console.log('HPSite data: ', rowNode.data);
        let year;
        let month;
        let day;
        let interviewDate;
        if (rowNode.data.interviewDate) {
          const date = rowNode.data.interviewDate;
          year = date.getFullYear();
          month = date.getMonth() + 1;
          day = date.getDate();
          interviewDate = year + '-' + month + '-' + day;
        }
        const referralObj: VCSReferralData = {
          vcsReferralID: rowNode.data.vcsReferralID,
          pactApplicationID: rowNode.data.pactApplicationID,
          vcsID: null,
          clientID: rowNode.data.pactClientID,
          referralDate: rowNode.data.referralDate,
          clientSourceType: 573,
          referringAgencyID: rowNode.data.referringAgencyID,
          referringSiteID: rowNode.data.referringSiteID,
          hpAgencyID: hpAgencyId,
          hpSiteID: hpSiteId,
          siteAgreementPopulationID: siteAgreementPopulationId,
          referralGroupGUID: null,
          referralType: ReferralType.Regular,
          referralReceivedType: null,
          referralReceivedDate: null,
          referralReceivedOtherSpecify: null,
          isScheduleMandatory: 1,
          unitType: rowNode.data.selectedUnitType,
          matchPercent: rowNode.data.matchPercentage,
          referralStatusType: ReferralStatusType.Draft,
          cocType: (rowNode.data.isHUD == true && this.makeCoCReferral == true) ? CoCType.HPDCoC : null,
          withdrawnReasonType: null,
          referralClosedComment: null,
          interviewDate: interviewDate || null,
          interviewTime: rowNode.data.interviewTime || null,
          interviewAddress: rowNode.data.interviewAddress,
          interviewCity: rowNode.data.interviewCity,
          interviewState: rowNode.data.interviewState,
          interviewZip: rowNode.data.interviewZip,
          interviewPhoneNo: rowNode.data.interviewPhoneNo,
          placementMatchCriteria: rowNode.data.placementMatchCriteria,
          createdBy: null,
          updatedBy: null
        };
        vcsReferralList.push(referralObj);

        if (rowNode.lastChild) {
          if (vcsReferralList) {
            // console.log('vcsReferralList : ', vcsReferralList);
            this.saveVCSReferralSub = this.c2vService.saveVCSReferral(vcsReferralList).subscribe(res => {
              if (res >= 0) {
                this.toastr.success('Client Referral Saved as DRAFT.', 'Referral Successful !');
                // this.referralRosterService.setCurrentReferralRosterTabIndex(0);
                this.v2cService.setV2CUpdatedInterviewAddress([]);  //reset the flag to empty
                this.referralRosterService.setCurrentRRTabIndex(0);
                /* Pre Populating the RR Agency/Site dropdown */
                this.referralRosterService.setRRAgencySelected(hpAgencyId);
                this.referralRosterService.setRRSiteSelected(hpSiteId);
                this.referralRosterService.setRRAgencyDropdownTypeSelected(2);

                /* Reset VacancyListing Filters, pageNumber, rowsCounter */
                this.v2cService.setVacancyListingFilterModel(null);
                this.v2cService.setVacancyListingPageNumber(0);
                this.v2cService.setVacancyListingNoOfRowCount(10);

                /* Reset the User Selected Filters, PageNumber and RowsCounter of V2C Handshake screen */
                this.v2cService.setV2CHandShakeFilterModel(null);
                this.v2cService.setV2CHandShakePageNumber(0);
                this.v2cService.setV2CHandShakeNoOfRowCount(10);

                this.router.navigate(['/vcs/referral-roster']);
              }
            });
          }
        }
      });
    }
  }

  onTransmit() {
    const vcsReferralList: VCSReferralData[] = [];
    // console.log('ClientSelected : ', this.clientSelected);
    if (this.vacancyListingSelected) {
      const hpAgencyId = this.vacancyListingSelected.hpAgencyID;
      const hpSiteId = this.vacancyListingSelected.hpSiteID;
      const siteAgreementPopulationId = this.vacancyListingSelected.siteAgreementPopulationID;
      let interviewScheduleCounter = 0;
      // iterate through every node in the grid
      this.agGrid.api.forEachNode((rowNode, index) => {
        // console.log('HPSite data: ', rowNode.data);
        let year;
        let month;
        let day;
        let interviewDate;
        if (rowNode.data.interviewDate) {
          const date = rowNode.data.interviewDate;
          year = date.getFullYear();
          month = date.getMonth() + 1;
          day = date.getDate();
          interviewDate = year + '-' + month + '-' + day;
        }
        const referralObj: VCSReferralData = {
          vcsReferralID: rowNode.data.vcsReferralID,
          pactApplicationID: rowNode.data.pactApplicationID,
          vcsID: null,
          clientID: rowNode.data.pactClientID,
          referralDate: rowNode.data.referralDate,
          clientSourceType: 573,
          referringAgencyID: rowNode.data.referringAgencyID,
          referringSiteID: rowNode.data.referringSiteID,
          hpAgencyID: hpAgencyId,
          hpSiteID: hpSiteId,
          siteAgreementPopulationID: siteAgreementPopulationId,
          referralGroupGUID: null,
          referralType: ReferralType.Regular,
          referralReceivedType: null,
          referralReceivedDate: null,
          referralReceivedOtherSpecify: null,
          isScheduleMandatory: 1,
          unitType: rowNode.data.selectedUnitType,
          matchPercent: rowNode.data.matchPercentage,
          referralStatusType: ReferralStatusType.Pending,
          cocType: (rowNode.data.isHUD == true && this.makeCoCReferral == true) ? CoCType.HPDCoC : null,
          withdrawnReasonType: null,
          referralClosedComment: null,
          interviewDate: interviewDate || null,
          interviewTime: rowNode.data.interviewTime || null,
          interviewAddress: rowNode.data.interviewAddress,
          interviewCity: rowNode.data.interviewCity,
          interviewState: rowNode.data.interviewState,
          interviewZip: rowNode.data.interviewZip,
          interviewPhoneNo: rowNode.data.interviewPhoneNo,
          placementMatchCriteria: rowNode.data.placementMatchCriteria,
          createdBy: null,
          updatedBy: null
        };
        vcsReferralList.push(referralObj);

        if (rowNode.data.interviewDate && rowNode.data.interviewTime) {
          interviewScheduleCounter++;
        }

        if (rowNode.lastChild) {
          if (interviewScheduleCounter !== this.rowData.length) {
            const title = 'Verify';
            const primaryMessage = `You are Transmitting the Referral with out Interview Schedule.`;
            const secondaryMessage = `Do you want to continue`;
            const confirmButtonName = 'OK';
            const dismissButtonName = 'Cancel';
            this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
              positiveResponse => {
                this.saveReferral(vcsReferralList);
              },
              negativeResponse => { }
            );
          } else {
            this.saveReferral(vcsReferralList);
          }
        }
      });
    }
  }

  saveReferral(vcsReferralList: VCSReferralData[]) {
    if (vcsReferralList.length > 0) {
      const title = 'Verify';
      const primaryMessage = `Are you sure you want to transmit? `;
      const secondaryMessage = ``;
      const confirmButtonName = 'OK';
      const dismissButtonName = 'Cancel';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => {
          this.saveVCSReferralSub = this.c2vService.saveVCSReferral(vcsReferralList).subscribe(res => {
            if (res >= 0) {
              this.toastr.success('Client Referral Transmitted.', 'Referral Successful !');
              // this.referralRosterService.setCurrentReferralRosterTabIndex(1);
              this.v2cService.setV2CUpdatedInterviewAddress([]);  //reset the flag to empty
              this.referralRosterService.setCurrentRRTabIndex(1);

               /* Pre Populating the RR Agency/Site dropdown */
               this.referralRosterService.setRRAgencySelected(this.vacancyListingSelected.hpAgencyID);
               this.referralRosterService.setRRSiteSelected(this.vacancyListingSelected.hpSiteID);
               this.referralRosterService.setRRAgencyDropdownTypeSelected(2);

              /* Reset VacancyListing Filters, pageNumber, rowsCounter */
              this.v2cService.setVacancyListingFilterModel(null);
              this.v2cService.setVacancyListingPageNumber(0);
              this.v2cService.setVacancyListingNoOfRowCount(10);

              /* Reset the User Selected Filters, PageNumber and RowsCounter of V2C Handshake screen */
              this.v2cService.setV2CHandShakeFilterModel(null);
              this.v2cService.setV2CHandShakePageNumber(0);
              this.v2cService.setV2CHandShakeNoOfRowCount(10);

              this.router.navigate(['/vcs/referral-roster']);
            }
          });
        },
        negativeResponse => { }
      );

    }
  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.vacancyListingSelectedSub) {
      this.vacancyListingSelectedSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.v2cEventSub) {
      this.v2cEventSub.unsubscribe();
    }
    if (this.availableInterviewTimeSlotSub) {
      this.availableInterviewTimeSlotSub.unsubscribe();
    }
    if (this.saveVCSReferralSub) {
      this.saveVCSReferralSub.unsubscribe();
    }
  }
}
