import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { IVacancyListing, IV2CReferralHandshake } from '../v2c-interface.model';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions, RowNode } from 'ag-grid-community';
import { V2cService } from '../v2c.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { SSNTooltipComponent } from '../../client-awaiting-placement/ssn-tooltip.component';
import { AssignUnitTypeComponent } from '../../client-awaiting-placement/c2v-referral-handshake/assign-unit-type.component';
import { MatchPercentageTooltipComponent } from '../../client-awaiting-placement/c2v-referral-handshake/match-percentage-tooltip.component';
import { Subscription } from 'rxjs';
import { PageSize } from 'src/app/models/pact-enums.enum';

@Component({
  selector: 'app-v2c-referral-handshake',
  templateUrl: './v2c-referral-handshake.component.html',
  styleUrls: ['./v2c-referral-handshake.component.scss']
})
export class V2cReferralHandshakeComponent implements OnInit, OnDestroy {

  vacancyListingSelected: IVacancyListing = null; // Site Selected from VacancyListing Page

  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  rowSelection;
  context: any;
  frameworkComponents;
  savedFilterModel: any;
  pageNumber: number;
  noOfRowCounter: number;
  public gridOptions: GridOptions;
  rowData: IV2CReferralHandshake[];

  unitType: RefGroupDetails[];

  pageSizes = PageSize;

  commonServiceSub: Subscription;
  vacancyListingSelectedSub: Subscription;
  rowDataSub: Subscription;
  savedFilterModelSub: Subscription;
  pageNumberSub: Subscription;
  noOfRowCounterSub: Subscription;

  constructor(
    private v2cService: V2cService,
    private toastrService: ToastrService,
    private router: Router,
    private commonService: CommonService,
    private confirmDialogService: ConfirmDialogService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Client# - Ref Date',
        field: 'clientNoRefDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.pactClientID + ' - ' + params.data.referralDate;
        },
      },
      {
        headerName: 'Client Name',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.firstName + ' ' + params.data.lastName;
        }
      },
      // {
      //   headerName: 'SSN',
      //   field: 'ssn',
      //   filter: 'agTextColumnFilter',
      //   cellRenderer: 'ssnTooltip'
      // },
      // {
      //   headerName: 'Referring Agency/site',
      //   field: 'referringAgencySite',
      //   // width: 80,
      //   filter: 'agTextColumnFilter',
      //   valueGetter(params) {
      //     if (params.data.agencyName !== '' && params.data.siteName !== '') {
      //       return params.data.agencyName + '/' + params.data.siteName;
      //     } else if (
      //       params.data.agencyName !== '' &&
      //       params.data.siteName === ''
      //     ) {
      //       return params.data.agencyName;
      //     } else if (
      //       params.data.agencyName === '' &&
      //       params.data.siteName !== ''
      //     ) {
      //       return params.data.siteName;
      //     }
      //   }
      // },
      {
        headerName: 'Referring Agency/site#',
        field: 'referringAgencySiteNo',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.referringAgencyNo + ' - ' + params.data.agencyName + ' / ' + params.data.referringSiteNo + ' - ' + params.data.siteName;
        }
      },
      {
        headerName: 'Assign Unit Type',
        field: 'selectedUnitType',
        width: 225,
        headerTooltip: 'Select the unit type from the dropdown',
        cellRenderer: 'dropdownRenderer',

      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        width: 300,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Approval Expiry Date',
        field: 'approvalToDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
        headerTooltip: 'The date till when referral can be approved'
      },
      {
        headerName: 'Placements Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referral Package Ready',
        field: 'isReferralPackageReady',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.isReferralPackageReady === true) {
            return 'Y';
          } else {
            return 'N';
          }
        }
      },
      {
        headerName: 'PendingReferrals',
        field: 'pendingReferrals',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Match Percentage',
        field: 'matchPercentage',
        filter: 'agNumberColumnFilter',
        cellRenderer: 'matchPercentageTooltip',
        // valueGetter(params) {
        //   return params.data.matchPercentage + '%';
        // },
        // cellStyle: {cursor: 'pointer'},
        // tooltipField: 'matchPercentage',
        // // tooltipComponentParams: { color: '#ececec' },
        // tooltipComponent: 'matchPercentageTooltip'
      },
      {
        headerName: 'Site Type',
        field: 'siteTypeDescription',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Gender',
        field: 'gender',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Ethnicity',
        field: 'ethnicity',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Age',
        field: 'age',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Borough',
        field: 'borough',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Application Status',
        field: 'applicationStatus',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Select',
        field: 'select',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        checkboxSelection: true
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.rowSelection = 'multiple';
    this.context = { componentParent: this };
    this.frameworkComponents = {
      ssnTooltip: SSNTooltipComponent,
      dropdownRenderer: AssignUnitTypeComponent,
      matchPercentageTooltip: MatchPercentageTooltipComponent
    };
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '29';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.unitType = data.filter(d => d.refGroupID === 29);
      },
      error => {
        throw new Error(error.message);
      }
    );

    /** Get the selected Site for referral */
    this.vacancyListingSelectedSub = this.v2cService.getVacancyListingSelected().subscribe(res => {
      this.vacancyListingSelected = res;
      // console.log('vacancyListingSelected: ', this.vacancyListingSelected);
    });
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {

      if (column.colId !== 'select') {
        if (column.colId !== 'selectedUnitType') {
          // console.log('column.colID: ' + column.colId);
          allColumnIds.push(column.colId);
        }
      }

    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    // params.api.expandAll();

    /** Getting the C2VHandshake data from api */
    if (this.vacancyListingSelected) {
      this.rowDataSub = this.v2cService.getV2CHandshakeData(this.vacancyListingSelected.siteAgreementPopulationID).subscribe((res: IV2CReferralHandshake[]) => {
        this.rowData = res;
        // console.log('C2VHandshake data: ', this.rowData);
      });
    }
  }

  onRowSelected(event) {
    if (event.node.selected) {
      // console.log('row ' + event.node.data.hpAgencyName + ' selected = ' + event.node.selected);
      // console.log('row index : ', event.node.index);
    }
    if (this.vacancyListingSelected.unitsAvailable > 0) {
      if (this.agGrid.api.getSelectedNodes().length > (3 * this.vacancyListingSelected.unitsAvailable)) {
        // alert('more than 3 selected');
        const title = 'Alert';
        const primaryMessage = `You can select Clients up to three times the count of available units.`;
        const secondaryMessage = ``;
        const confirmButtonName = 'OK';
        const dismissButtonName = '';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => { },
          negativeResponse => { }
        );
        event.node.setSelected(false);
      }
    }
  }

  getSelectedRows() {
    let formValid = false;
    if (this.agGrid.api.getSelectedNodes().length > 0) {
      const selectedNodes = this.agGrid.api.getSelectedNodes();
      const selectedData = selectedNodes.map(node => node.data);
      const referralClientSelectedList: IV2CReferralHandshake[] = [];
      let cocReferralClientSelectedList = 0;
      selectedData.map((node: IV2CReferralHandshake) => {
        /* Below Code is when UnitType is Mandatory (Please uncomment this section when UnitType is mandatory)*/
        // if (node.selectedUnitType == 0 || node.selectedUnitType == null) {
        //   formValid = false;
        // } else {
        //   for (const val of this.unitType) {
        //     if (val.refGroupDetailID == node.selectedUnitType) {
        //       node.assignUnitTypeDescription = val.refGroupDetailDescription;
        //     }
        //   }
        //   if (node.isHUD == true) {
        //     // 9 = HUD CoC Service Funding
        //     cocReferralClientSelectedList++;
        //   }
        //   formValid = true;
        //   referralClientSelectedList.push(node);
        // }

        /* Below Code is when UnitType is not Mandatory (Please comment this section when UnitType is mandatory)*/
        if (node.selectedUnitType == 0 || node.selectedUnitType == null) {
          formValid = false;
        }
        if (node.selectedUnitType > 0) {
          formValid = true;
          for (const val of this.unitType) {
            if (val.refGroupDetailID == node.selectedUnitType) {
              node.assignUnitTypeDescription = val.refGroupDetailDescription;
            }
          }
        }
        if (node.isHUD == true) {
          // 9 = HUD CoC Service Funding
          cocReferralClientSelectedList++;
        }

        referralClientSelectedList.push(node);

      });

      /* Unit Type updated as Non Mandatory (Temporary code - later this field will be mandatory, then uncomment below section code and comment this section code) */
      if (!formValid) {
        const title = 'Verify';
        const primaryMessage = `UnitType not selected for some of the selected Clients.`;
        const secondaryMessage = `Are you sure you want to continue?`;
        const confirmButtonName = 'Yes';
        const dismissButtonName = 'No';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => {
            if (cocReferralClientSelectedList > 0 && this.vacancyListingSelected.primaryServiceContractID == 9) {
              const title = 'Verify';
              const primaryMessage = `Some of the sites selected are CoC, `;
              const secondaryMessage = `do you intend to make CoC Referral for this client.`;
              const confirmButtonName = 'Yes';
              const dismissButtonName = 'No';
              this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                positiveResponse => {
                  this.v2cService.setMakeCoCReferral(true);
                  this.saveReferralClientSelectedList(referralClientSelectedList);
                },
                negativeResponse => {
                  this.v2cService.setMakeCoCReferral(false);
                  this.saveReferralClientSelectedList(referralClientSelectedList);
                }
              );
            } else {
              this.v2cService.setMakeCoCReferral(false);
              this.saveReferralClientSelectedList(referralClientSelectedList);
            }
          },
          negativeResponse => { }
        );
      } else {
        if (cocReferralClientSelectedList > 0 && this.vacancyListingSelected.primaryServiceContractID == 9) {
          const title = 'Verify';
          const primaryMessage = `Some of the sites selected are CoC, `;
          const secondaryMessage = `do you intend to make CoC Referral for this client.`;
          const confirmButtonName = 'Yes';
          const dismissButtonName = 'No';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              this.v2cService.setMakeCoCReferral(true);
              this.saveReferralClientSelectedList(referralClientSelectedList);
            },
            negativeResponse => {
              this.v2cService.setMakeCoCReferral(false);
              this.saveReferralClientSelectedList(referralClientSelectedList);
            }
          );
        } else {
          this.v2cService.setMakeCoCReferral(false);
          this.saveReferralClientSelectedList(referralClientSelectedList);
        }
      }

      /* Below Code is when UnitType is Mandatory (Please comment above code and uncomment this section when UnitType is mandatory)*/
      // if (formValid) {
      //   if (cocReferralClientSelectedList > 0 && this.vacancyListingSelected.primaryServiceContractID == 9) {
      //     const title = 'Verify';
      //     const primaryMessage = `Some of the sites selected are CoC, `;
      //     const secondaryMessage = `do you intend to make CoC Referral for this client.`;
      //     const confirmButtonName = 'Yes';
      //     const dismissButtonName = 'No';
      //     this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      //       positiveResponse => {
      //         this.v2cService.setMakeCoCReferral(true);
      //         this.saveReferralClientSelectedList(referralClientSelectedList);
      //       },
      //       negativeResponse => {
      //         this.v2cService.setMakeCoCReferral(false);
      //         this.saveReferralClientSelectedList(referralClientSelectedList);
      //        }
      //     );
      //   } else {
      //     this.v2cService.setMakeCoCReferral(false);
      //     this.saveReferralClientSelectedList(referralClientSelectedList);
      //   }
      // } else {
      //   this.toastrService.error('Please select the unit type for the row selected', 'UnitType Missing');
      // }
    } else {
      this.toastrService.error('Please select at least one client to proceed');
    }
  }

  onFirstDataRendered(params) {
    this.savedFilterModelSub = this.v2cService.getV2CHandShakeFilterModel().subscribe(res => {
      if (res) {
        this.savedFilterModel = res;
        params.api.setFilterModel(this.savedFilterModel);
        params.api.onFilterChanged();
      }
    });

    this.pageNumberSub = this.v2cService.getV2CHandShakePageNumber().subscribe(pageNo => {
      if (pageNo) {
        this.pageNumber = pageNo;
        params.api.paginationGoToPage(this.pageNumber);
      }
    });
    this.noOfRowCounterSub = this.v2cService.getV2CHandShakeNoOfRowCount().subscribe(rowCount => {
      if (rowCount) {
        this.noOfRowCounter = rowCount;
        params.api.paginationSetPageSize(this.noOfRowCounter);
        params.api.refreshHeader();
      }
    });
  }

  onUnitTypeSelected(unitType: any, rowNodes: RowNode) {
    rowNodes.setSelected(unitType > 0);
    rowNodes.setDataValue('selectedUnitType', unitType);
    // console.log('onUnitTypeSelected - ', unitType, rowNodes);
  }

  //On Page Size Changed
  onPageSizeChanged() {
    // this.noOfRowCounter = noOfRowCount;
    this.gridOptions.api.paginationSetPageSize(Number(this.noOfRowCounter));
    this.gridOptions.api.refreshHeader();
  }

  saveReferralClientSelectedList(referralClientSelectedList: IV2CReferralHandshake[]) {
    this.v2cService.setReferralClientSelectedList(referralClientSelectedList);

    this.savedFilterModel = this.gridApi.getFilterModel();
    this.v2cService.setV2CHandShakeFilterModel(this.savedFilterModel);
    this.pageNumber = this.gridApi.paginationGetCurrentPage();
    this.v2cService.setV2CHandShakePageNumber(this.pageNumber);
    this.v2cService.setV2CHandShakeNoOfRowCount(this.noOfRowCounter);

    this.router.navigate(['/vcs/v2c-referral-handshake-success']);
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
    this.gridOptions.api.paginationGoToPage(0);
    this.gridOptions.api.paginationSetPageSize(10);
    this.gridOptions.api.refreshHeader();
    this.v2cService.setV2CHandShakeFilterModel(null);
    this.v2cService.setV2CHandShakePageNumber(0);
    this.v2cService.setV2CHandShakeNoOfRowCount(10);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'V2CReferralHandshake-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

    // console.log('clientReferralHandshake-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.vacancyListingSelectedSub) {
      this.vacancyListingSelectedSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.savedFilterModelSub) {
      this.savedFilterModelSub.unsubscribe();
    }
    if (this.pageNumberSub) {
      this.pageNumberSub.unsubscribe();
    }
    if (this.noOfRowCounterSub) {
      this.noOfRowCounterSub.unsubscribe();
    }
  }
}
