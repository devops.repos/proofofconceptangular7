import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IVacancyListing, IV2CReferralHandshake, IVCSUpdatedInterviewAddress } from './v2c-interface.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class V2cService {
  private apiURL = environment.pactApiUrl + 'VCSReferral';

  private vacancyListingSelected = new BehaviorSubject<IVacancyListing>(null);

  private makeCoCReferral = new BehaviorSubject<boolean>(false);
  private v2cUpdatedInterviewAddress = new BehaviorSubject<IVCSUpdatedInterviewAddress[]>(null);
  private referralClientSelectedList = new BehaviorSubject<IV2CReferralHandshake[]>(null);

  private vFilterModel = new BehaviorSubject<any>(null);
  private vPageNumber = new BehaviorSubject<number>(0);
  private vNoOfRowCount = new BehaviorSubject<number>(10);

  private v2cHandShakeFilterModel = new BehaviorSubject<any>(null);
  private v2cHandShakePageNumber = new BehaviorSubject<number>(0);
  private v2cHandShakeNoOfRowCount = new BehaviorSubject<number>(10);

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  constructor(private http: HttpClient) {}

  //#region Getting the GetVacancyListing from api
  getVacancyListing() {
    /** Call the API to get the VCSVacancyListing data here */
    return this.http.get(this.apiURL + '/GetVacancyListing');
  }
  //#endregion

  //#region Get and Set for Site selected from the VacancyListing pool
  setVacancyListingSelected(data: IVacancyListing) {
    this.vacancyListingSelected.next(data);
  }

  getVacancyListingSelected(): Observable<IVacancyListing> {
    return this.vacancyListingSelected.asObservable();
  }
  //#endregion

  //#region Get and Set for makeCoCReferral flag
  setMakeCoCReferral(input: boolean) {
    this.makeCoCReferral.next(input);
    // console.log('referralSiteSelectedList', list);
  }

  getMakeCoCReferral(): Observable<boolean> {
    return this.makeCoCReferral.asObservable();
  }
//#endregion

  //#region Getting the List of Site after Matchlogic in backend for the given PactApplicationID
  getV2CHandshakeData(siteAgreementPopulationID: number) {
    return this.http.post(this.apiURL + '/GetV2CHandShake', siteAgreementPopulationID, this.httpOptions);
    // return this.http.get('assets/data/v2c-handshake-data.json');
  }
  //#endregion

  //#region Get and Set for List of Client selected from V2C handshake screen for the select Site for making a V2C referral
  setReferralClientSelectedList(list: IV2CReferralHandshake[]) {
    this.referralClientSelectedList.next(list);
    // console.log('referralClientSelectedList', list);
  }

  getReferralClientSelectedList(): Observable<IV2CReferralHandshake[]> {
    return this.referralClientSelectedList.asObservable();
  }
  //#endregion

   //#region Get and Set Ag-Grid Filter Model for Pages
   setVacancyListingFilterModel(filter: any) {
    this.vFilterModel.next(filter);
  }
  getVacancyListingFilterModel() {
    return this.vFilterModel.asObservable();
  }
  setV2CHandShakeFilterModel(filter: any) {
    this.v2cHandShakeFilterModel.next(filter);
  }
  getV2CHandShakeFilterModel() {
    return this.v2cHandShakeFilterModel.asObservable();
  }
  //#endregion

  //#region Get and Set User selected Ag-Grid PageNumber
  setVacancyListingPageNumber(pageNumber: number) {
    this.vPageNumber.next(pageNumber);
  }
  getVacancyListingPageNumber() {
    return this.vPageNumber.asObservable();
  }
  setV2CHandShakePageNumber(pageNumber: number) {
    this.v2cHandShakePageNumber.next(pageNumber);
  }
  getV2CHandShakePageNumber() {
    return this.v2cHandShakePageNumber.asObservable();
  }
  //#endregion

  //#region Get and Set User selected Ag-Grid NoOfRowCount
  setVacancyListingNoOfRowCount(rowCount: number) {
    this.vNoOfRowCount.next(rowCount);
  }
  getVacancyListingNoOfRowCount() {
    return this.vNoOfRowCount.asObservable();
  }
  setV2CHandShakeNoOfRowCount(rowCount: number) {
    this.v2cHandShakeNoOfRowCount.next(rowCount);
  }
  getV2CHandShakeNoOfRowCount() {
    return this.v2cHandShakeNoOfRowCount.asObservable();
  }
  //#endregion

  getVCSOnlineUnitRoster(siteAgrId: number) {
    return this.http.post(this.apiURL + '/GetVCSOnlineUnitRosterBySiteAgreementPopulationID', JSON.stringify(siteAgrId), this.httpOptions);
  }

  //#region Get and Set V2C Updated Interview Address
  /* this logic is to hold the Updated Interview Address,
  so it's easy to cache it while redirecting to and forth between
  V2C handshake screen and V2C scheduler pages */
  getV2CUpdatedInterviewAddress() {
   return this.v2cUpdatedInterviewAddress.asObservable();
  }

  setV2CUpdatedInterviewAddress(address: IVCSUpdatedInterviewAddress[]) {
    this.v2cUpdatedInterviewAddress.next(address);
  }
}
