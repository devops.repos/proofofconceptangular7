import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { Subscription } from 'rxjs';
import { GridOptions } from 'ag-grid-community';
import { IVacancyListingAllData, IVacancyListing, VCSUnitRosterDialogData } from './v2c-interface.model';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { V2cService } from './v2c.service';
import { VLActionComponent } from './vl-action.component';
import { MatDialog } from '@angular/material';
import { UnitRosterDialogComponent } from './unit-roster-dialog/unit-roster-dialog.component';
import { Router } from '@angular/router';
import { PageSize } from 'src/app/models/pact-enums.enum';

@Component({
  selector: 'app-vacancy-listing',
  templateUrl: './vacancy-listing.component.html',
  styleUrls: ['./vacancy-listing.component.scss']
})
export class VacancyListingComponent implements OnInit, OnDestroy {
  @ViewChild('agGrid') agGrid: AgGridAngular;

  widthSideBar: number;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  rowSelection;
  frameworkComponents;
  context;
  savedFilterModel: any;
  pageNumber: number;
  noOfRowCounter: number;

  public gridOptions: GridOptions;
  rowData: IVacancyListing[];
  numberOfVL = 0;
  nynY1n2 = 0;
  nynY3 = 0;
  nyC1515 = 0;
  coC = 0;
  offlineUnits = 0;
  flagClicked = '';

  pageSizes = PageSize;

  widthSideBarSub: Subscription;
  vacancyListingSub: Subscription;
  savedFilterModelSub: Subscription;
  pageNumberSub: Subscription;
  noOfRowCounterSub: Subscription;

  constructor(
    private navService: NavService,
    private v2cService: V2cService,
    private dialog: MatDialog,
    private router: Router
  ) {
    this.gridOptions = {
      rowHeight: 35,
      // skipHeaderOnAutoSize: true,
      // getRowStyle: (params) => {
      //   if (params.node.data.isReferralHold) {
      //     return {background: '#FFB74D'};
      //   }
      // }
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Housing Provider Agency/Site #',
        field: 'hpAgencySiteNo',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.hpAgencyNo + ' / ' + params.data.hpSiteNo;
        },
        hide: true,
      },
      {
        headerName: 'Housing Provider Agency/Site Name',
        field: 'hpAgencySiteName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.hpAgencyName !== '' && params.data.hpSiteName !== '') {
            return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
          } else if (params.data.hpAgencyName !== '' && params.data.hpSiteName === '') {
            return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName;
          } else if (params.data.hpAgencyName === '' && params.data.hpSiteName !== '') {
            return params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
          } else {
            return '';
          }
        }
      },
      // {
      //   headerName: 'Primary Funding Source',
      //   field: 'primaryServiceContractName',
      //   filter: 'agTextColumnFilter'
      // },
      // {
      //   headerName: 'Population Name',
      //   field: 'populationName',
      //   filter: 'agTextColumnFilter'
      // },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Rental Subsidies',
        field: 'rentalSubsidies',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Location Type',
        field: 'siteLocationTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Address',
        field: 'siteAddress',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Units Occupied',
        field: 'unitsOccupied',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Units Available',
        field: 'unitsAvailable',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Units Offline',
        field: 'unitsOffline',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Pending Referrals',
        field: 'pendingReferrals',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'primaryServiceContractID',
        field: 'primaryServiceContractID',
        hide: true,
        filter: 'agSetColumnFilter'
      },
      {
        headerName: 'Site Type',
        field: 'siteTypeDescription',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Borough',
        field: 'boroughTypeDescription',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Site Features',
        field: 'siteFeatures',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: VLActionComponent
    };
  }

  ngOnInit() {
    /** Getting the width of the sidenav to reflect the changes to nav toggle */
    this.widthSideBarSub = this.navService.getWidthSideBar().subscribe(res => {
      this.widthSideBar = res;
    });

    /* Reset the User Selected Filters, PageNumber and RowsCounter of V2C Handshake screen */
    this.v2cService.setV2CHandShakeFilterModel(null);
    this.v2cService.setV2CHandShakePageNumber(0);
    this.v2cService.setV2CHandShakeNoOfRowCount(10);
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    /** API call to get the grid data */
    this.vacancyListingSub = this.v2cService.getVacancyListing().subscribe((res: IVacancyListingAllData) => {
      this.nynY1n2 = res.nynY1n2;
      this.nynY3 = res.nynY3;
      this.nyC1515 = res.nyC1515;
      this.coC = res.coC;
      this.offlineUnits = res.offlineUnits;
      this.rowData = res.vacancyList;
      this.numberOfVL = this.rowData.length;
      // console.log('vacancyListing: ', res);
    });
  }

  onFirstDataRendered(params) {
    this.savedFilterModelSub = this.v2cService.getVacancyListingFilterModel().subscribe(res => {
      if (res) {
        this.savedFilterModel = res;
        params.api.setFilterModel(this.savedFilterModel);
        params.api.onFilterChanged();
      }
    });

    this.pageNumberSub = this.v2cService.getVacancyListingPageNumber().subscribe(pageNo => {
      if (pageNo) {
        this.pageNumber = pageNo;
        params.api.paginationGoToPage(this.pageNumber);
      }
    });
    this.noOfRowCounterSub = this.v2cService.getVacancyListingNoOfRowCount().subscribe(rowCount => {
      if (rowCount) {
        this.noOfRowCounter = rowCount;
        params.api.paginationSetPageSize(this.noOfRowCounter);
        params.api.refreshHeader();
      }
    });
  }

  onFilterChanged(event) {
    if (event.afterFloatingFilter) {
      this.flagClicked = '';
    }
  }

  //On Page Size Changed
  onPageSizeChanged() {
    // this.noOfRowCounter = noOfRowCount;
    this.gridOptions.api.paginationSetPageSize(this.noOfRowCounter);
    this.gridOptions.api.refreshHeader();
  }

  selectedSiteCatchedInParentPage(cell: IVacancyListing) {
    // alert('Refer Client Parent Component Method: ' + JSON.stringify(cell));
    this.v2cService.setVacancyListingSelected(cell);

    this.savedFilterModel = this.gridApi.getFilterModel();
    this.v2cService.setVacancyListingFilterModel(this.savedFilterModel);
    this.pageNumber = this.gridApi.paginationGetCurrentPage();
    this.v2cService.setVacancyListingPageNumber(this.pageNumber);
    this.v2cService.setVacancyListingNoOfRowCount(this.noOfRowCounter);

    this.router.navigate(['/vcs/v2c-referral-handshake']);
  }

  onUnitRosterClickParentPage(cell: IVacancyListing) {
    this.v2cService.setVacancyListingSelected(cell);
    const input: VCSUnitRosterDialogData = {
      selectedHpSite: cell.hpSiteID,
      siteAgreemenetPopulationID: cell.siteAgreementPopulationID
    }
    this.dialog.open(UnitRosterDialogComponent, {
      width: '98%',
      disableClose: true,
      autoFocus: false,
      data: input
    });
  }

  selectCounterFilter(fieldValue: string, value: number) {
    if (value === 3) {
      this.flagClicked = 'List of Sites with Primary Funding Source as NYNY III';
    } else if (value === 4) {
      this.flagClicked = 'List of Sites with Primary Funding Source as NYC 15/15';
    } else if (value === 9) {
      this.flagClicked = 'List of Sites with Primary Funding Source as CoC';
    } else if (value === 0) {
      this.flagClicked = 'List of Sites with Primary Funding Source as NY/NY I & NY/NY II';
    }
    this.gridOptions.api.setFilterModel(null);
    const instance = this.gridApi.getFilterInstance(fieldValue);
    instance.selectNothing(fieldValue);
    if (value === 0) {
      /* 1=NY/NY I, 2=NY/NY II, 18=NY/NY I & II  */
      instance.selectValue(1);
      instance.selectValue(2);
      instance.selectValue(18);
    } else {
      instance.selectValue(value);
    }
    instance.applyModel();
    this.gridApi.onFilterChanged();
  }

  selectNothing(fieldValue?: string) {
    const instance = this.gridApi.getFilterInstance(fieldValue);
    instance.selectNothing();
    instance.applyModel();
    this.gridApi.onFilterChanged();
  }

  selectOfflineUnitsFilter() {
    this.flagClicked = 'List of Sites with some Offline Units';
    const instance = this.gridApi.getFilterInstance('unitsOffline');
    this.gridOptions.api.setFilterModel(null);
    instance.setModel({
      type: 'greaterThan',
      filter: 0,
      filterTo: null,
    });
    this.gridApi.onFilterChanged();
  }

  refreshAgGrid() {
    this.flagClicked = '';
    this.gridOptions.api.setFilterModel(null);
    this.gridOptions.api.paginationGoToPage(0);
    this.gridOptions.api.paginationSetPageSize(10);
    this.gridOptions.api.refreshHeader();
    this.v2cService.setVacancyListingFilterModel(null);
    this.v2cService.setVacancyListingPageNumber(0);
    this.v2cService.setVacancyListingNoOfRowCount(10);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'VacancyListing-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };

    // console.log('VacancyListing-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.widthSideBarSub) {
      this.widthSideBarSub.unsubscribe();
    }
    if (this.vacancyListingSub) {
      this.vacancyListingSub.unsubscribe();
    }
    if (this.savedFilterModelSub) {
      this.savedFilterModelSub.unsubscribe();
    }
    if (this.pageNumberSub) {
      this.pageNumberSub.unsubscribe();
    }
    if (this.noOfRowCounterSub) {
      this.noOfRowCounterSub.unsubscribe();
    }
  }
}
