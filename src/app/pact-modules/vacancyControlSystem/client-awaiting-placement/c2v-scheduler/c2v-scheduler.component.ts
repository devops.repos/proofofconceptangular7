import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { C2vSchedulerActionComponent } from './c2v-scheduler-action.component';
import { C2vInterviewDatePickerComponent } from './c2v-interview-date-picker.component';
import { C2vInterviewTimePickerComponent } from './c2v-interview-time-picker.component';
import { IC2VReferralHandshakeData, VCSAvailableInterviewTimeSlotInput, VCSAvailableInterviewTimeSlot, IClientAwaitingPlacementData } from '../c2v-interfaces.model';
import { C2vService } from '../c2v.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { IVCSAgencySiteInfoForCalendar } from 'src/app/shared/calendar/pact-calendar-interface.model';
import { Subscription, BehaviorSubject } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-c2v-scheduler',
  templateUrl: './c2v-scheduler.component.html',
  styleUrls: ['./c2v-scheduler.component.scss']
})
export class C2vSchedulerComponent implements OnInit, OnDestroy {

  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  frameworkComponents;
  rowSelection;
  context;

  public gridOptions: GridOptions;
  rowData: IC2VReferralHandshakeData[];

  clientSelected: IClientAwaitingPlacementData;
  pactApplicationID = 0;
  hpSiteID = 0;
  hpSiteIDforCalendar: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  agencySiteInfo: IVCSAgencySiteInfoForCalendar;

  private currentUser: AuthData;

  clientSelectedSub: Subscription;
  rowDataSub: Subscription;
  currentUserSub: Subscription;
  availableInterviewTimeSlotSub: Subscription;

  constructor(
    private c2vService: C2vService,
    private toastr: ToastrService,
    private router: Router,
    private userService: UserService,
    private confirmDialogService: ConfirmDialogService
  ) {
    this.gridOptions = {
      rowHeight: 36,
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Site selected for Referral',
        field: 'site',
        valueGetter(params) {
          if (params.data.hpAgencyName !== '' && params.data.hpSiteName !== '') {
            return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
          } else if (
            params.data.hpAgencyName !== '' &&
            params.data.hpSiteName == ''
          ) {
            return params.data.hpAgencyName;
          } else if (
            params.data.hpAgencyName == '' &&
            params.data.hpSiteName !== ''
          ) {
            return params.data.hpSiteName;
          }
        }
      },
      {
        headerName: 'Interview Date',
        field: 'changedInterviewDate',
        width: 190,
        headerTooltip: 'Select the Interview Date for each site below',
        cellRenderer: 'interviewDatePickerRenderer',
      },
      {
        headerName: 'InterviewTime',
        field: 'changedInterviewTime',
        width: 210,
        headerTooltip: 'Select the Interview Time from the dropdown',
        cellRenderer: 'interviewTimePickerRenderer',
      },
      {
        headerName: 'Interview Address',
        field: 'intrvwAddress',
        valueGetter(params) {
          if (params.data.interviewAddress && params.data.interviewCity && params.data.interviewState && params.data.interviewZip) {
            return params.data.interviewAddress + ', ' + params.data.interviewCity + ', ' + params.data.interviewState + ' ' + params.data.interviewZip
          } else {
            return '';
          }
        }
      },
      {
        headerName: 'Site Phone #',
        field: 'interviewPhoneNo',
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];
    this.defaultColDef = {
      // sortable: true,
      // resizable: true
    };
    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.frameworkComponents = {
      interviewDatePickerRenderer: C2vInterviewDatePickerComponent,
      interviewTimePickerRenderer: C2vInterviewTimePickerComponent,
      actionRenderer: C2vSchedulerActionComponent
    };
  }

  ngOnInit() {
    /** Get the selected Client for referral */
    this.clientSelectedSub = this.c2vService.getClientAwaitingPlacementSelected().subscribe(res => {
      if (res) {
        this.clientSelected = res;
        this.pactApplicationID = res.pactApplicationID;
      } else {
        /* If user refresh(reload) the page the client selected data will be empty, so redirect them back to ClientAwaitingPlacement Page */
        /** Getting the currently active user info */
        this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
          if (userdata) {
            this.currentUser = userdata;
            /** Checking the User siteCategoryType either PE or HP */
            this.currentUser.siteCategoryType.forEach(ct => {
              if (ct.siteCategory !== UserSiteType.SH_HP) {
                this.router.navigate(['/vcs/client-awaiting-placement']);
              } else if (ct.siteCategory === UserSiteType.SH_HP) {
                this.router.navigate(['/vcs/referral-roster']);
              }
            });
          }
        });
      }
    });
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    /** Getting the data from the C2VService for the list of site selected */
    this.rowDataSub = this.c2vService.getReferralSiteSelectedList().subscribe((res: IC2VReferralHandshakeData[]) => {
      if (res) {
        this.rowData = res;
        // console.log('this.rowData: ', this.rowData);
      }
    });

  }

  onRowSelected(event) {
    if (event.node.selected) {
      this.hpSiteID = event.node.data.hpSiteID;
      this.agencySiteInfo = {
        agencyID: event.node.data.hpAgencyID,
        agencyNo: event.node.data.hpAgencyNo,
        agencyName: event.node.data.hpAgencyName,
        siteID: event.node.data.hpSiteID,
        siteNo: event.node.data.hpSiteNo,
        siteName: event.node.data.hpSiteName
      }
      this.hpSiteIDforCalendar.next(this.hpSiteID);
    }
  }

  selectedInterviewDateInParentPage(cell) {
    // alert('Site Info with InterviewDateTime : ' + JSON.stringify(cell));
    // console.log(cell);
    if (cell.changedInterviewDate) {
      cell.interviewDate = cell.changedInterviewDate;
      const sDate = (cell.changedInterviewDate.getMonth() + 1) + '/' + cell.changedInterviewDate.getDate() + '/' + cell.changedInterviewDate.getFullYear();
      const inputValue: VCSAvailableInterviewTimeSlotInput = {
        HPSiteID: cell.hpSiteID,
        InterviewDate: sDate
      };
      /** Getting the available Interview Time slot from database for the select HPSite */
      this.availableInterviewTimeSlotSub = this.c2vService.GetAvailableInterviewTimeSlot(inputValue).subscribe((res: VCSAvailableInterviewTimeSlot[]) => {
        if (res.length > 0) {
          // this.interviewTimes.push(res);
          this.rowData.forEach(val => {
            if (val.hpSiteID == cell.hpSiteID) {
              val.availableInterviewTimeSlot = res;
            }
          });
          // console.log('after getting avaliable TimeSlot : ', cell);
          const prms = {
            force: true,
            columns: ['interviewTime']
          };
          this.gridApi.refreshCells(prms);
        }
      });
    }
  }

  selectedInterviewTimeInParentPage(cell) {
    // console.log(cell);
    cell.interviewTime = cell.changedInterviewTime;
  }

  onUpdateInterviewAddressClickParentPage(cell: IC2VReferralHandshakeData) {
    this.rowData.forEach(r => {
      if (r.siteAgreementPopulationID == cell.siteAgreementPopulationID) {
        r.interviewAddress = cell.interviewAddress;
        r.interviewCity = cell.interviewCity;
        r.interviewState = cell.interviewState;
        r.interviewZip = cell.interviewZip;
        r.interviewPhoneNo = cell.interviewPhoneNo;
      }
      const prms = {
        force: true,
        columns: ['intrvwAddress', 'interviewPhoneNo']
      };
      this.gridApi.refreshCells(prms);
    });
    // console.log('After update this.rowDate: ', this.rowData);
  }

  // vcsReferralInterviewToBeEdited(intrvwToBeEdited: IVCSSiteCalendarEvents) {
  //   // console.log('Calendar Event to be Edited: ', intrvwToBeEdited);
  //   const dt = intrvwToBeEdited.interviewDate.split('-');
  //   const yr = +dt[0];
  //   const mn = +dt[1];
  //   const dy = +dt[2];
  //   const intrvwDate = new Date(yr, mn - 1, dy);
  //   const toEdit: IC2VReferralHandshakeData = {
  //     hpAgencyID: intrvwToBeEdited.hpAgencyID,
  //     hpSiteID: intrvwToBeEdited.hpSiteID,
  //     hpAgencyNo: +intrvwToBeEdited.hpAgencyNo,
  //     hpSiteNo: intrvwToBeEdited.hpSiteNo,
  //     hpAgencyName: intrvwToBeEdited.hpAgencyName,
  //     hpSiteName: intrvwToBeEdited.hpSiteName,
  //     primaryServiceContractID: intrvwToBeEdited.primaryServiceContractID,
  //     primaryServiceContractName: intrvwToBeEdited.primaryServiceContractName,
  //     agreementPopulationID: intrvwToBeEdited.agreementPopulationID,
  //     populationName: intrvwToBeEdited.populationName,
  //     siteAgreementPopulationID: intrvwToBeEdited.siteAgreementPopulationID,
  //     agreementTypeDescription: intrvwToBeEdited.agreementTypeDescription,
  //     siteLocationType: intrvwToBeEdited.siteLocationType,
  //     siteLocationTypeDescription: intrvwToBeEdited.siteLocationTypeDescription,
  //     siteAddress: intrvwToBeEdited.siteAddress,
  //     unitsOccupied: intrvwToBeEdited.unitsOccupied,
  //     unitsAvailable: intrvwToBeEdited.unitsAvailable,
  //     totalUnits: intrvwToBeEdited.totalUnits,
  //     pendingReferrals: intrvwToBeEdited.pendingReferrals,
  //     assignUnitType: intrvwToBeEdited.assignUnitType,
  //     assignUnitTypeDescription: intrvwToBeEdited.assignUnitTypeDescription,
  //     matchPercentage: intrvwToBeEdited.matchPercentage,
  //     interviewDate: intrvwDate,
  //     interviewTime: intrvwToBeEdited.interviewTime,
  //     changedInterviewDate: intrvwDate,
  //     changedInterviewTime: intrvwToBeEdited.interviewTime,
  //     placementMatchCriteria: null,
  //     availableInterviewTimeSlot: null
  //   };
  //   let duplicate = 0;
  //   this.rowData.forEach((rec: IC2VReferralHandshakeData) => {
  //     if (rec.siteAgreementPopulationID == toEdit.siteAgreementPopulationID) {
  //       duplicate++;
  //     }
  //   });
  //   // console.log('duplicate : ', duplicate);
  //   if (duplicate == 0) {
  //     // console.log('data to be appended to handshake: ', toEdit);
  //     this.rowData.push(toEdit);
  //     this.gridApi.updateRowData({ add: [toEdit] });
  //     this.gridApi.refreshCells();
  //   } else {
  //     this.toastr.error('Interview is already Listed in the Grid above', 'Duplicate record!!!');
  //   }
  // }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onClear() {
    const title = 'Verify';
    const primaryMessage = `Do you want to clear all the interview information entered?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        const arr = [];
        this.agGrid.api.forEachNode((rowNode, index) => {
          rowNode.data.interviewDate = null;
          rowNode.data.interviewTime = null;
          rowNode.data.changedInterviewDate = null;
          rowNode.data.changedInterviewTime = null;
          arr.push(rowNode.data);
        });
        this.rowData = arr;
      },
      negativeResponse => { }
    );
  }

  onNext() {
    const sysDate = new Date();
    const currentYear = sysDate.getFullYear();
    const currentDay = sysDate.getDate();
    const currentMonth = sysDate.getMonth();
    const fDate = new Date(currentYear, (currentMonth + 6), currentDay, sysDate.getHours(), sysDate.getMinutes(), sysDate.getSeconds(), sysDate.getMilliseconds());
    let inValidcount = 0;
    let partialSelection = 0;
    var sameDateTimeCounter = 0;
    var siteNo = '';
    var siteName = '';
    let interviewScheduleCounter = 0;
    /** Validating the Date and Time field */
    this.agGrid.api.forEachNode((rowNode, index) => {
      // console.log('index: ', index);
      // console.log('rowNode.data: ', rowNode.data);
      const intDate = rowNode.data.interviewDate;
      const intTime = rowNode.data.interviewTime;
      const d = new Date(intDate);
      var sDate;
      if (intTime) {
        const splt = intTime.split(':');
        let hrs = +splt[0];
        const mns = +splt[1].substr(0, 2);
        const AMPM = splt[1].substr(2, 2);
        if (AMPM == 'PM' && hrs < 12) { hrs = hrs + 12; }
        if (AMPM == 'AM' && hrs == 12) { hrs = hrs - 12; }

        sDate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), hrs, mns);
      } else {
        sDate = d;
      }

      if ((intDate && !intTime) || (!intDate && intTime)) {
        this.toastr.error('Please enter both date and time for Interview', 'InterviewDate/Time partially field');
        inValidcount++;
        partialSelection++;
      } else if (intDate && intTime) {
        if (sDate < sysDate) {
          this.toastr.error('Interview Date cannot be the Past Date.');
          inValidcount++;
        } else if (sDate > fDate) {
          this.toastr.error('Select Interview Date within 6 months period.');
          inValidcount++;
        } else {
          this.agGrid.api.forEachNode((row, i) => {
            if (row.data.interviewDate && row.data.interviewTime) {
              if (index !== i && (rowNode.data.hpSiteID == row.data.hpSiteID) &&
                  (new Date(rowNode.data.interviewDate).getTime() == new Date(row.data.interviewDate).getTime()) &&
                  (rowNode.data.interviewTime == row.data.interviewTime)
                 ) {
                /* Same interview Date and Time selected for same HPSiteID */
                inValidcount++;
                sameDateTimeCounter++;
                siteNo = row.data.hpSiteNo;
                siteName = row.data.hpSiteName;
              }
            }
            interviewScheduleCounter++;
          });
        }
      }
      if (rowNode.lastChild) {
        if (interviewScheduleCounter == 0 && partialSelection == 0) {
          const title = 'Verify';
          const primaryMessage = `Do you want to proceed without scheduling the interview for one or more site(s)?`;
          const secondaryMessage = ``;
          const confirmButtonName = 'OK';
          const dismissButtonName = 'Cancel';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              if (sameDateTimeCounter > 0) {
                this.toastr.error('Same Interview Date and Time selected for Site \"' + siteNo + ' - ' + siteName + '\". Please select different interviewTime.');
              } else if (inValidcount == 0) {
                this.router.navigate(['/vcs/c2v-referral-handshake-success']);
              }
            },
            negativeResponse => { }
          );
        } else if (inValidcount == 0) {
          this.router.navigate(['/vcs/c2v-referral-handshake-success']);
        }
      }
      // if (inValidcount == 0 && rowNode.lastChild) {
      //   this.router.navigate(['/vcs/c2v-referral-handshake-success']);
      // }
    });
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'c2vScheduler-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

    // console.log('c2vScheduler-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.clientSelectedSub) {
      this.clientSelectedSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.availableInterviewTimeSlotSub) {
      this.availableInterviewTimeSlotSub.unsubscribe();
    }
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
  }

}
