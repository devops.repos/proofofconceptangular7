import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { IVCSInterviewAddress, IC2VReferralHandshakeData } from '../../c2v-interfaces.model';
import { IStates, States } from 'src/app/models/pact-enums.enum';

@Component({
  selector: 'app-c2v-interview-address-dialog',
  templateUrl: './c2v-interview-address-dialog.component.html',
  styleUrls: ['./c2v-interview-address-dialog.component.scss']
})
export class C2vInterviewAddressDialogComponent implements OnInit, OnDestroy {

  IntAddressFormGroup: FormGroup;

  states: IStates[] = States;

  commonServiceSub: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<C2vInterviewAddressDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: IC2VReferralHandshakeData,
    private router: Router,
    private commonService: CommonService
  ) {
    this.IntAddressFormGroup = this.formBuilder.group({
      intAddressCtrl: ['', Validators.compose([Validators.required])],
      intCityCtrl: ['', Validators.compose([Validators.required])],
      // intStateCtrl: ['', Validators.compose([Validators.required, CustomValidators.dropdownRequired()])],
      intStateCtrl: ['', Validators.compose([Validators.required, Validators.maxLength(2)])],
      intZipCtrl: ['', Validators.compose([Validators.required])],
      intPhoneCtrl: ['']
    });
  }

  ngOnInit() {
    // // Get Refgroup Details
    // const refGroupList = '62';
    // this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
    //   res => {
    //     const data = res.body as RefGroupDetails[];
    //     this.withdrawnReason = data.filter(d => d.refGroupID === 62);
    //   },
    //   error => {
    //     throw new Error(error.message);
    //   }
    // );
    if (this.data.interviewAddress) {
      this.IntAddressFormGroup.get('intAddressCtrl').setValue(this.data.interviewAddress.trim());
    }
    if (this.data.interviewCity) {
      this.IntAddressFormGroup.get('intCityCtrl').setValue(this.data.interviewCity.trim());
    }
    if (this.data.interviewState) {
      this.IntAddressFormGroup.get('intStateCtrl').setValue(this.data.interviewState.trim());
    }
    if (this.data.interviewZip) {
      this.IntAddressFormGroup.get('intZipCtrl').setValue(this.data.interviewZip.trim());
    }
    if (this.data.interviewPhoneNo) {
      this.IntAddressFormGroup.get('intPhoneCtrl').setValue(this.data.interviewPhoneNo.trim());
    }
  }

  onUpdateClick() {
    const value: IVCSInterviewAddress = {
      interviewAddress: this.IntAddressFormGroup.get('intAddressCtrl').value,
      interviewCity: this.IntAddressFormGroup.get('intCityCtrl').value,
      interviewState: this.IntAddressFormGroup.get('intStateCtrl').value,
      interviewZip: this.IntAddressFormGroup.get('intZipCtrl').value,
      interviewPhoneNo: this.IntAddressFormGroup.get('intPhoneCtrl').value
    };
    // alert('Reason: ' + value.withdrawnReason + '   comment: ' + value.withdrawnComment);
    this.dialogRef.close(value);
  }

  onCancelClick(): void {
    this.dialogRef.close(null);
  }

  ngOnDestroy() {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
  }
}
