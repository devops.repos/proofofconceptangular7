import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { C2vSchedulerComponent } from './c2v-scheduler.component';

describe('C2vSchedulerComponent', () => {
  let component: C2vSchedulerComponent;
  let fixture: ComponentFixture<C2vSchedulerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ C2vSchedulerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(C2vSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
