import { Component} from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { C2vInterviewAddressDialogComponent } from './c2v-interview-address-dialog/c2v-interview-address-dialog.component';
import { MatDialog } from '@angular/material';
import { IVCSInterviewAddress } from '../c2v-interfaces.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-c2v-scheduler-action',
  template: `
    <mat-icon
      *ngIf="params.data.interviewDate && params.data.interviewTime"
      class="c2v-scheduler-action-icon"
      color="warn"
      [matMenuTriggerFor]="c2vSchedulerAction"
    >
      more_vert
    </mat-icon>
    <mat-icon
      *ngIf="!(params.data.interviewDate && params.data.interviewTime)"
      class="c2v-scheduler-action-icon"
      color="warn"
      (click)="onActionClick()"
    >
      more_vert
    </mat-icon>
    <mat-menu #c2vSchedulerAction="matMenu">
      <button mat-menu-item (click)="onUpdateInterviewAddressClick()"><mat-icon color="primary">assignment</mat-icon>Update Interview Address</button>
      <!-- <button mat-menu-item routerLink="/vcs/c2v-scheduler"><mat-icon color="primary">assignment</mat-icon>View</button> -->
    </mat-menu>
  `,
  styles: [
    `
      .c2v-scheduler-action-icon {
        cursor: pointer;
      }
    `
  ]
})
export class C2vSchedulerActionComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog,
    private toastr: ToastrService
  ) { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onUpdateInterviewAddressClick() {
    const title = 'Verify';
    const primaryMessage = `You are about to change the Interview Address for the selected Referral. `;
    const secondaryMessage = `Are you sure you want to continue?`;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        const dialogRef = this.dialog.open(C2vInterviewAddressDialogComponent, {
          width: '40%',
          panelClass: 'transparent',
          data: this.params.data,
          disableClose: true
        });

        dialogRef.afterClosed().subscribe((updatedIntAddress: IVCSInterviewAddress) => {
          if (updatedIntAddress) {
            console.log('updatedIntAddress: ', updatedIntAddress);
            this.params.data.interviewAddress = updatedIntAddress.interviewAddress;
            this.params.data.interviewCity = updatedIntAddress.interviewCity;
            this.params.data.interviewState = updatedIntAddress.interviewState;
            this.params.data.interviewZip = updatedIntAddress.interviewZip;
            this.params.data.interviewPhoneNo = updatedIntAddress.interviewPhoneNo;
            this.params.context.componentParent.onUpdateInterviewAddressClickParentPage(this.params.data);
          }
        });
      },
      negativeResponse => { }
    );
  }

  onActionClick() {
    this.toastr.warning('Please select InterviewDate and Interview Time before trying to update the Interview Address');
  }

  refresh(): boolean {
    return false;
  }
}
