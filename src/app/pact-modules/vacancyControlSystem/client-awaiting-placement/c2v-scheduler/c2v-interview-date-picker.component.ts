import {Component, ViewChild, Input} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import { MatDatepickerInputEvent, MatDatepicker } from '@angular/material/datepicker';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: 'app-c2v-interview-date-picker',
    template: `
    <form [formGroup]="interviewDateGroup">
        <mat-form-field appearance="outline">
            <input matInput
                [matDatepicker]="interviewDatePicker"
                [min]="minDate"
                [max]="maxDate"
                formControlName="interviewDateCtrl"
                (click) = "onDateClick()"
                (dateChange)="onInterviewDateSelected()"
                dateOnly
                placeholder="MM/DD/YYYY"
            >
            <mat-datepicker-toggle matSuffix [for]="interviewDatePicker"></mat-datepicker-toggle>
            <mat-datepicker #interviewDatePicker></mat-datepicker>
        </mat-form-field>
    </form>
    `,
    providers: [DatePipe]
})
export class C2vInterviewDatePickerComponent implements ICellRendererAngularComp {
  @ViewChild(MatDatepicker) datepicker;
    params: any;
    public cell: any;
    date = new FormControl();

    interviewDateGroup: FormGroup;

    minDate: Date;
    maxDate: Date;

    constructor(
        private formBuilder: FormBuilder,
        private datePipe: DatePipe,
    ) {
        this.interviewDateGroup = this.formBuilder.group({
            interviewDateCtrl: [''],
          });
        // Set the minimum and maximum date range to be selectable in date picker.
        const currentYear = new Date().getFullYear();
        const currentDay = new Date().getDate();
        const currentMonth = new Date().getMonth();
        this.minDate = new Date(currentYear, currentMonth, currentDay);
        this.maxDate = new Date(currentYear, currentMonth + 6, currentDay);
    }

    agInit(params: any): void {
        this.params = params;
        // this.cell = {row: params.value, col: params.colDef.headerName};
        this.cell = {row: params.node.data, col: params.colDef.headerName};
        if (this.params.value) {
            // console.log('there is date value set already : ', this.params.value);
            // this.date.setValue(this.params.value);
            this.interviewDateGroup.get('interviewDateCtrl').setValue(this.params.value);
            // console.log('pre datepicker: ', this.params.value);
            this.cell.row.changedInterviewDate = this.params.value;
            this.params.context.componentParent.selectedInterviewDateInParentPage(this.cell.row);
        } else {
            this.date.setValue(null);
            this.cell.row.changedInterviewDate = null;
            this.params.context.componentParent.selectedInterviewDateInParentPage(this.cell.row);
        }
    }

    onDateClick() {
      this.datepicker.open();
    }

    onInterviewDateSelected() {
        if (this.datePipe.transform(this.interviewDateGroup.get('interviewDateCtrl').value, 'MM/dd/yyyy')) {
            const datePicked = new Date(this.interviewDateGroup.get('interviewDateCtrl').value);
            if (datePicked) {
                this.cell.row.changedInterviewDate = datePicked;
                this.params.context.componentParent.selectedInterviewDateInParentPage(this.cell.row);
            } else {
                this.cell.row.changedInterviewDate = null;
                this.params.context.componentParent.selectedInterviewDateInParentPage(this.cell.row);
            }
        } else {
            this.cell.row.changedInterviewDate = null;
            this.params.context.componentParent.selectedInterviewDateInParentPage(this.cell.row);
        }
      }

    // addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    //     // console.log(`${type}: ${event.value}`);
    //     if (event.value !== null) {
    //         const selectedDate = new Date(event.value);
    //         // console.log('selectedDate: ', selectedDate);
    //         // this.cell.row.interviewDate = selectedDate.getFullYear() + '-' + (selectedDate.getMonth() + 1 ) + '-' + selectedDate.getDate();
    //         this.cell.row.changedInterviewDate = selectedDate;
    //         // alert('Selected Date from DatePicker inside ag-grid : ' + JSON.stringify(this.cell.row));
    //         this.params.context.componentParent.selectedInterviewDateInParentPage(this.cell.row);
    //     } else {
    //         this.cell.row.changedInterviewDate = null;
    //         this.params.context.componentParent.selectedInterviewDateInParentPage(this.cell.row);
    //     }

    //   }

    refresh(): boolean {
        return true;
    }
}
