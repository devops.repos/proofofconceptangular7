import { Component} from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MasterDropdown } from 'src/app/models/masterDropdown.module';
import { VCSAvailableInterviewTimeSlot, VCSAvailableInterviewTimeSlotInput } from '../c2v-interfaces.model';
import { C2vService } from '../c2v.service';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-c2v-interview-time-picker',
  template: `
    <mat-form-field appearance="outline">
      <mat-select [(ngModel)]="selectedInterviewTime">
        <mat-option (click)="onInterviewTimeSelected()" [value]="0">Select One</mat-option>
        <mat-option *ngIf="params.node.data.interviewTime" (click)="onInterviewTimeSelected()" [value]="params.node.data.interviewTime">{{params.node.data.interviewTime}}</mat-option>
        <mat-option (click)="onInterviewTimeSelected()" *ngFor="let interviewTime of params.node.data.availableInterviewTimeSlot" [value]="interviewTime.availableTimeSlot">
          {{ interviewTime.availableTimeSlot }}
        </mat-option>
      </mat-select>
    </mat-form-field>
  `
})
export class C2vInterviewTimePickerComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;

  selectedInterviewTime;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    /** If we already selected Interview Date/Time and came back to change the selected Date/Time,
     * we have to populate the previously selected Time
     */
    if (this.params.value) {
      // console.log('pre time : ', this.params.value);
      this.selectedInterviewTime = this.params.value;
    } else {
      this.selectedInterviewTime = 0;
    }
  }

  onInterviewTimeSelected(): void {
    // console.log('selectedInterviewTime : ', this.selectedInterviewTime);
    if (this.selectedInterviewTime !== 0) {

      this.cell.row.changedInterviewTime = this.selectedInterviewTime;
      // alert('Selected Time from DROPDOWN : ' + JSON.stringify(this.cell.row));
      this.params.context.componentParent.selectedInterviewTimeInParentPage(this.cell.row);
    } else {
      this.cell.row.changedInterviewTime = null;
      // alert('Selected Time from DROPDOWN : ' + JSON.stringify(this.cell.row));
      this.params.context.componentParent.selectedInterviewTimeInParentPage(this.cell.row);
    }

  }

  refresh(): boolean {
    return true;
  }
}
