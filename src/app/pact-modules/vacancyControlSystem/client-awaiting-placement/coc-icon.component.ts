import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-coc-icon',
  template: `
    <p class="cap-coc-icon">
      <span matBadge="CoC" matBadgeOverlap="false">{{ params.value }}</span>
    </p>
  `,
  styles: [``]
})
export class CoCIconComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }
}
