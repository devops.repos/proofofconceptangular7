import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { C2vService } from './c2v.service';
import { IClientAwaitingPlacementData } from './c2v-interfaces.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-cap-action',
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="capAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #capAction="matMenu">
      <button mat-menu-item (click)="onReferralPackageSelected()">Referral Package</button>
      <button mat-menu-item (click)="onReferClientSelected()">Refer Client </button>
    </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class CapActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private capSelected: IClientAwaitingPlacementData;

  constructor(private c2vService: C2vService) {}

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onReferralPackageSelected(){
    this.capSelected = this.params.data;
    this.params.context.componentParent.selectedClientCatchedInParentPage(this.capSelected, '/vcs/referral-package/homeless-information');
  }

  onReferClientSelected() {
    this.capSelected = this.params.data;
    this.params.context.componentParent.selectedClientCatchedInParentPage(this.capSelected, '/vcs/c2v-referral-handshake');
  }

  refresh(): boolean {
    return false;
  }
}
