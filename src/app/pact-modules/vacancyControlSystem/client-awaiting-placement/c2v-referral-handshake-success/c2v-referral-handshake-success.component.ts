import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { C2vService } from '../c2v.service';
import {
  IC2VReferralHandshakeData,
  IClientAwaitingPlacementData,
  VCSReferralData,
} from '../c2v-interfaces.model';
import { Router } from '@angular/router';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import {
  ReferralType,
  ReferralStatusType,
  CoCType,
  UserSiteType
} from 'src/app/models/pact-enums.enum';
import { ToastrService } from 'ngx-toastr';
import { PactCalendarService } from 'src/app/shared/calendar/pact-calendar.service';
import { ReferralRosterService } from '../../referral-roster/referral-roster.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { TadService } from '../../tad-submission/tad.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-c2v-referral-handshake-success',
  templateUrl: './c2v-referral-handshake-success.component.html',
  styleUrls: ['./c2v-referral-handshake-success.component.scss']
})
export class C2VReferralHandshakeSuccessComponent implements OnInit, OnDestroy {
  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  frameworkComponents;
  public gridOptions: GridOptions;
  rowData: IC2VReferralHandshakeData[];

  referralType: RefGroupDetails[];
  unitType: RefGroupDetails[];
  referralStatusType: RefGroupDetails[];
  cocType: RefGroupDetails[];
  withdrawnReasonType: RefGroupDetails[];

  clientSelected: IClientAwaitingPlacementData;

  makeCoCReferral = false;

  private currentUser: AuthData;
  is_SH_PE = false;
  is_SH_HP = false;

  isTadWorkflow = false;
  isRoutedFromRR = false;
  currentRRTabNo = 0;

  commonServiceSub: Subscription;
  clientSelectedSub: Subscription;
  currentUserSub: Subscription;
  rowDataSub: Subscription;
  c2vEventSub: Subscription;
  availableInterviewTimeSlotSub: Subscription;
  saveVCSReferralSub: Subscription;
  makeCoCReferralSub: Subscription;
  istadWorkflowSub: Subscription;
  isRoutedFromRRSub: Subscription;
  currentRRTabNoSub: Subscription;

  constructor(
    private c2vService: C2vService,
    private router: Router,
    private commonService: CommonService,
    private toastr: ToastrService,
    private pactCalendarService: PactCalendarService,
    private referralRosterService: ReferralRosterService,
    private userService: UserService,
    private tadService: TadService,
    private confirmDialogService: ConfirmDialogService
  ) {
    this.gridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Housing Provider Agency/Site#',
        // set the column to use text filter
        filter: 'agTextColumnFilter',
        // pass in additional parameters to the text filter
        // filterParams: {
        //   clearButton: true,
        //   applyButton: true
        //   // debounceMs: 200
        // },
        valueGetter(params) {
          return params.data.hpAgencyNo + ' / ' + params.data.hpSiteNo;
        }
      },
      {
        headerName: 'Housing Provider Agency/Site Name',
        // field: 'clientName',
        // width: 80,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (
            params.data.hpAgencyName !== '' &&
            params.data.hpSiteName !== ''
          ) {
            return params.data.hpAgencyName + '/' + params.data.hpSiteName;
          } else if (
            params.data.hpAgencyName !== '' &&
            params.data.hpSiteName == ''
          ) {
            return params.data.hpAgencyName;
          } else if (
            params.data.hpAgencyName == '' &&
            params.data.hpSiteName !== ''
          ) {
            return params.data.hpSiteName;
          }
        }
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        // width: 80,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Location',
        field: 'siteLocationTypeDescription',
        // width: 80,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Address',
        field: 'siteAddress',
        // width: 80,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Total Units',
        field: 'totalUnits',
        // width: 300,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Units Available',
        field: 'unitsAvailable',
        // width: 80,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Pending Referrals',
        field: 'pendingReferrals',
        // width: 80,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Assign Unit Type',
        field: 'assignUnitTypeDescription',
        width: 210
      },
      {
        headerName: 'Interview Date',
        field: 'interviewDate',
        filter: 'agDateColumnFilter',
        valueGetter: params => {
          if (params.data.interviewDate) {
            return (
              (params.data.interviewDate.getMonth() + 1) +
              '/' +
              params.data.interviewDate.getDate() +
              '/' +
              params.data.interviewDate.getFullYear()
            );
          }
        },
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Interview Time',
        field: 'interviewTime'
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.frameworkComponents = {};
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '29,59,60,61';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.unitType = data.filter(d => d.refGroupID === 29);
        this.referralStatusType = data.filter(d => d.refGroupID === 59);
        this.referralType = data.filter(d => d.refGroupID === 60);
        this.cocType = data.filter(d => d.refGroupID === 61);
      },
      error => {
        throw new Error(error.message);
      }
    );

    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
        /** Checking the User siteCategoryType either PE or HP */
        this.currentUser.siteCategoryType.forEach(ct => {
          if (ct.siteCategory === UserSiteType.SH_PE) {
            this.is_SH_PE = true;
          } else if (ct.siteCategory === UserSiteType.SH_HP) {
            this.is_SH_HP = true;
          }
        });
      }
    });

    /** Get the selected Client for referral */
    this.clientSelectedSub = this.c2vService.getClientAwaitingPlacementSelected().subscribe(res => {
      if (res) {
        this.clientSelected = res;
      } else {
        /* If user refresh(reload) the page the client selected data will be empty, so redirect them back to ClientAwaitingPlacement Page */
        if (this.is_SH_HP) {
          this.router.navigate(['/vcs/referral-roster']);
        } else {
          this.router.navigate(['/vcs/client-awaiting-placement']);
        }
      }
    });

    /* Get the makeCoCReferral */
    this.makeCoCReferralSub = this.c2vService.getMakeCoCReferral().subscribe(res => {
      if (res == true) {
        // console.log('makeCoCReferral');
        this.makeCoCReferral = true;
      }
    });

    /* Check if the redirection is coming from Referral Roster or not */
    this.isRoutedFromRRSub = this.referralRosterService.getisRoutedFromRR().subscribe((flag: boolean) => {
      if (flag) {
        this.isRoutedFromRR = flag;
        /* Get the Current Tab No of Referral Roster */
        this.currentRRTabNoSub = this.referralRosterService.getCurrentRRTabNo().subscribe((no: number) => {
          /* 1=Draft, 2=Transmitted, 3=Pending, 4=Completed Tab */
          if (no > 0) {
            this.currentRRTabNo = no;
          } else {
            this.currentRRTabNo = 0;
          }
        });
      } else {
        this.isRoutedFromRR = false;
      }
    });


    /* Check if the workFlow is coming from TAD, if so, on submit or exit, redirect back to TAD workflow */
    this.istadWorkflowSub = this.tadService.getIsTadWorkflow().subscribe(flag => {
      if (flag) {
        this.isTadWorkflow = flag;
      }
    });
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'agreementType') {
        if (column.colId !== 'assignUnitTypeDescription') {
          // console.log('column.colID: ' + column.colId);
          allColumnIds.push(column.colId);
        }
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    // params.api.expandAll();

    /** Getting the data from the C2VService for the list of site selected */
    this.rowDataSub = this.c2vService.getReferralSiteSelectedList().subscribe((res: IC2VReferralHandshakeData[]) => {
      this.rowData = res;
    });
  }

  onSave() {
    const vcsReferralList: VCSReferralData[] = [];
    // console.log('ClientSelected : ', this.clientSelected);
    if (this.clientSelected) {
      const pactApplicationId = this.clientSelected.pactApplicationID;
      const clientId = this.clientSelected.pactClientID;
      const referringAgencyId = this.clientSelected.referringAgencyID;
      const referringSiteId = this.clientSelected.referringSiteID;
      // iterate through every node in the grid
      this.agGrid.api.forEachNode((rowNode, index) => {
        // console.log('HPSite data: ', rowNode.data);
        let year;
        let month;
        let day;
        let interviewDate;
        if (rowNode.data.interviewDate) {
          const date = rowNode.data.interviewDate;
          year = date.getFullYear();
          month = date.getMonth() + 1;
          day = date.getDate();
          interviewDate = year + '-' + month + '-' + day;
        }
        const referralObj: VCSReferralData = {
          vcsReferralID: null,
          pactApplicationID: pactApplicationId,
          vcsID: null,
          clientID: clientId,
          referralDate: this.clientSelected.referralDate,
          clientSourceType: 573,
          referringAgencyID: referringAgencyId,
          referringSiteID: referringSiteId,
          hpAgencyID: rowNode.data.hpAgencyID,
          hpSiteID: rowNode.data.hpSiteID,
          siteAgreementPopulationID: rowNode.data.siteAgreementPopulationID,
          referralGroupGUID: null,
          referralType: ReferralType.Regular,
          referralReceivedType: null,
          referralReceivedDate: null,
          referralReceivedOtherSpecify: null,
          isScheduleMandatory: 1,
          unitType: rowNode.data.selectedUnitType,
          matchPercent: rowNode.data.matchPercentage,
          referralStatusType: ReferralStatusType.Draft,
          cocType: (rowNode.data.primaryServiceContractID == 9 && this.makeCoCReferral == true) ? CoCType.HPDCoC : null,
          withdrawnReasonType: null,
          referralClosedComment: null,
          interviewDate: interviewDate || null,
          interviewTime: rowNode.data.interviewTime || null,
          interviewAddress: rowNode.data.interviewAddress,
          interviewCity: rowNode.data.interviewCity,
          interviewState: rowNode.data.interviewState,
          interviewZip: rowNode.data.interviewZip,
          interviewPhoneNo: rowNode.data.interviewPhoneNo,
          placementMatchCriteria: rowNode.data.placementMatchCriteria,
          createdBy: null,
          updatedBy: null
        };
        vcsReferralList.push(referralObj);

        if (rowNode.lastChild) {
          if (vcsReferralList) {
            // console.log('vcsReferralList : ', vcsReferralList);
            this.saveVCSReferralSub = this.c2vService.saveVCSReferral(vcsReferralList).subscribe(res => {
              if (res >= 0) {
                if (this.is_SH_HP || this.isRoutedFromRR) {
                  this.toastr.success('Interview was scheduled successfully!');
                } else {
                  this.toastr.success('Client Referral Saved as DRAFT.', 'Referral Successful !');
                }

                if (!this.is_SH_HP && !this.isTadWorkflow) {
                  this.referralRosterService.setRRAgencySelected(referringAgencyId);
                  this.referralRosterService.setRRSiteSelected(referringSiteId);
                  this.referralRosterService.setRRAgencyDropdownTypeSelected(1);
                }
                this.referralRosterService.setCurrentRRTabIndex(0);

                /* Reset CLAP Filters, pageNumber, rowsCounter */
                this.c2vService.setCLAPFilterModel(null);
                this.c2vService.setCLAPPageNumber(0);
                this.c2vService.setCLAPNoOfRowCount(10);

                /* Reset the User Selected Filters, PageNumber and RowsCounter of C2V Handshake screen */
                this.c2vService.setC2VHandShakeFilterModel(null);
                this.c2vService.setC2VHandShakePageNumber(0);
                this.c2vService.setC2VHandShakeNoOfRowCount(10);

                this.router.navigate(['/vcs/referral-roster']);

              }
            });
          }
        }
      });
    }
  }

  onTransmit() {
    const vcsReferralList: VCSReferralData[] = [];
    // console.log('ClientSelected : ', this.clientSelected);
    if (this.clientSelected) {
      const pactApplicationId = this.clientSelected.pactApplicationID;
      const clientId = this.clientSelected.pactClientID;
      const referringAgencyId = this.clientSelected.referringAgencyID;
      const referringSiteId = this.clientSelected.referringSiteID;
      let interviewScheduleCounter = 0;
      // iterate through every node in the grid
      this.agGrid.api.forEachNode((rowNode, index) => {
        // console.log('HPSite data: ', rowNode.data);
        let year;
        let month;
        let day;
        let interviewDate;
        if (rowNode.data.interviewDate) {
          const date = rowNode.data.interviewDate;
          year = date.getFullYear();
          month = date.getMonth() + 1;
          day = date.getDate();
          interviewDate = year + '-' + month + '-' + day;
        }
        const referralObj: VCSReferralData = {
          vcsReferralID: null,
          pactApplicationID: pactApplicationId,
          vcsID: null,
          clientID: clientId,
          referralDate: this.clientSelected.referralDate,
          clientSourceType: 573,
          referringAgencyID: referringAgencyId,
          referringSiteID: referringSiteId,
          hpAgencyID: rowNode.data.hpAgencyID,
          hpSiteID: rowNode.data.hpSiteID,
          siteAgreementPopulationID: rowNode.data.siteAgreementPopulationID,
          referralGroupGUID: null,
          referralType: ReferralType.Regular,
          referralReceivedType: null,
          referralReceivedDate: null,
          referralReceivedOtherSpecify: null,
          isScheduleMandatory: 1,
          unitType: rowNode.data.selectedUnitType,
          matchPercent: rowNode.data.matchPercentage,
          referralStatusType: ReferralStatusType.Pending,
          cocType: (rowNode.data.primaryServiceContractID == 9 && this.makeCoCReferral == true) ? CoCType.HPDCoC : null,
          withdrawnReasonType: null,
          referralClosedComment: null,
          interviewDate: interviewDate || null,
          interviewTime: rowNode.data.interviewTime || null,
          interviewAddress: rowNode.data.interviewAddress,
          interviewCity: rowNode.data.interviewCity,
          interviewState: rowNode.data.interviewState,
          interviewZip: rowNode.data.interviewZip,
          interviewPhoneNo: rowNode.data.interviewPhoneNo,
          placementMatchCriteria: rowNode.data.placementMatchCriteria,
          createdBy: null,
          updatedBy: null
        };
        vcsReferralList.push(referralObj);

        if (rowNode.data.interviewDate && rowNode.data.interviewTime) {
          interviewScheduleCounter++;
        }

        if (rowNode.lastChild) {
          if (interviewScheduleCounter !== this.rowData.length) {
            const title = 'Verify';
            const primaryMessage = `You are Transmitting the Referral with out Interview Schedule.`;
            const secondaryMessage = `Do you want to continue`;
            const confirmButtonName = 'OK';
            const dismissButtonName = 'Cancel';
            this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
              positiveResponse => {
                this.saveReferral(vcsReferralList);
              },
              negativeResponse => { }
            );
          } else {
            this.saveReferral(vcsReferralList);
          }
        }
      });
    }
  }

  saveReferral(vcsReferralList: VCSReferralData[]) {
    if (vcsReferralList.length > 0) {
      // console.log('vcsReferralList : ', vcsReferralList);
      const title = 'Verify';
      const primaryMessage = (this.is_SH_HP || this.isRoutedFromRR) ? `Are you sure you want to update the interview Date and Time? `:`Are you sure you want to transmit? `;
      const secondaryMessage = ``;
      const confirmButtonName = 'OK';
      const dismissButtonName = 'Cancel';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => {
          this.saveVCSReferralSub = this.c2vService.saveVCSReferral(vcsReferralList).subscribe(res => {
            if (res >= 0) {
              if (this.is_SH_HP || this.isRoutedFromRR) {
                this.toastr.success('Interview was scheduled successfully!');
              } else {
                this.toastr.success('Client Referral Transmitted.', 'Referral Successful !');
              }

              /* Reset CLAP Filters, pageNumber, rowsCounter */
              this.c2vService.setCLAPFilterModel(null);
              this.c2vService.setCLAPPageNumber(0);
              this.c2vService.setCLAPNoOfRowCount(10);

              /* Reset the User Selected Filters, PageNumber and RowsCounter of C2V Handshake screen */
              this.c2vService.setC2VHandShakeFilterModel(null);
              this.c2vService.setC2VHandShakePageNumber(0);
              this.c2vService.setC2VHandShakeNoOfRowCount(10);

              if (this.isTadWorkflow) {
                this.router.navigate(['/vcs/tad/tad-submission']);
              } else {
                if (!this.is_SH_HP) {
                  this.referralRosterService.setRRAgencySelected(this.clientSelected.referringAgencyID);
                  this.referralRosterService.setRRSiteSelected(this.clientSelected.referringSiteID);
                  this.referralRosterService.setRRAgencyDropdownTypeSelected(1);
                  this.referralRosterService.setCurrentRRTabIndex(1);
                } else if (this.is_SH_HP) {
                  this.referralRosterService.setCurrentRRTabIndex(0);
                }
                this.router.navigate(['/vcs/referral-roster']);
              }
              // else {
              //   this.router.navigate(['/vcs/client-awaiting-placement']);
              // }
            }
          });
        },
        negativeResponse => { }
      );

    }
  }

  onExit() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to exit?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        if (this.isTadWorkflow) {
          this.tadService.setIsTadWorkflow(false);
          this.router.navigate(['/vcs/tad/tad-submission']);
        } else {
          this.referralRosterService.setisRoutedFromRR(false);
          this.referralRosterService.setCurrentRRTabNo(0);
          this.router.navigate(['/vcs/referral-roster']);
        }
      },
      negativeResponse => { }
    );
  }

  ngOnDestroy(): void {
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.clientSelectedSub) {
      this.clientSelectedSub.unsubscribe();
    }
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.c2vEventSub) {
      this.c2vEventSub.unsubscribe();
    }
    if (this.availableInterviewTimeSlotSub) {
      this.availableInterviewTimeSlotSub.unsubscribe();
    }
    if (this.saveVCSReferralSub) {
      this.saveVCSReferralSub.unsubscribe();
    }
    if (this.makeCoCReferralSub) {
      this.makeCoCReferralSub.unsubscribe();
    }
    if (this.istadWorkflowSub) {
      this.istadWorkflowSub.unsubscribe();
    }
    if (this.isRoutedFromRRSub) {
      this.isRoutedFromRRSub.unsubscribe();
    }
    if (this.currentRRTabNoSub) {
      this.currentRRTabNoSub.unsubscribe();
    }
  }
}
