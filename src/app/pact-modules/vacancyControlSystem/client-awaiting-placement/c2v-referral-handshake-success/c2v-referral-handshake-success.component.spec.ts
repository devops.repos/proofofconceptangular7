import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { C2VReferralHandshakeSuccessComponent } from './c2v-referral-handshake-success.component';

describe('C2VReferralHandshakeSuccessComponent', () => {
  let component: C2VReferralHandshakeSuccessComponent;
  let fixture: ComponentFixture<C2VReferralHandshakeSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ C2VReferralHandshakeSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(C2VReferralHandshakeSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
