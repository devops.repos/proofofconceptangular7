import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-info-icon',
  template: `
    <span class="tr-info-icon">
        {{params.value}}<mat-icon class="cap-info-icon" matTooltip="{{tooltipMessage}}" matTooltipClass="vcs-info-tooltip">info</mat-icon>
    </span>
  `,
  styles: [`
  `]
})
export class InfoIconComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;
  tooltipMessage: string;

  referralHoldReasons: RefGroupDetails[];
  referralHoldReason = '';

  constructor(
    private commonService: CommonService
  ) { }

  agInit(params: any): void {
    this.params = params;
    // console.log(this.params);
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    // Get Refgroup Details
    const refGroupList = '58';
    this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        this.tooltipMessage = '';
        const data = res.body as RefGroupDetails[];
        // console.log(data);
        this.referralHoldReasons = data.filter(d => d.refGroupID === 58);
        this.referralHoldReasons.forEach(reason => {
          if (reason.refGroupDetailID == this.params.data.clientNotReadyReason) {
            this.referralHoldReason = reason.refGroupDetailDescription;
            // console.log(this.referralHoldReason);
            this.tooltipMessage = `Updated By: ` + params.data.clientPackageUpdatedBy +
                              `\nUpdated Date: ` + params.data.clientPackageUpdatedDate +
                              `\nReason: ` + this.referralHoldReason;
            var msg = (params.data.clientNotReadyReason == 510) ?
                      `\nComments: ` + params.data.clientNotReadyOtherSpecify + `\nAddl Comments: ` + params.data.clientNotReadyAddlComments
                      : `\nAddl Comments: ` + params.data.clientNotReadyAddlComments;
            this.tooltipMessage += msg;
          }
        });
      },
      error => {
        throw new Error(error.message);
      }
    );
  }

  refresh(): boolean {
    return false;
  }
}
