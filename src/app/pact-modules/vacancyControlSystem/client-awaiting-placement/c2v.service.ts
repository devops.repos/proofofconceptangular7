import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IClientAwaitingPlacementData, IC2VReferralHandshakeData, VCSReferralData, VCSAvailableInterviewTimeSlotInput } from './c2v-interfaces.model';

@Injectable({
  providedIn: 'root'
})
export class C2vService {
  private apiURL = environment.pactApiUrl + 'VCSReferral';

  private clientAwaitingPlacementSelected = new BehaviorSubject<IClientAwaitingPlacementData>(null);
  private referralSiteSelectedList = new BehaviorSubject<IC2VReferralHandshakeData[]>(null);
  private makeCoCReferral = new BehaviorSubject<boolean>(false);

  private clapFilterModel = new BehaviorSubject<any>(null);
  private clapPageNumber = new BehaviorSubject<number>(0);
  private clapNoOfRowCount = new BehaviorSubject<number>(10);

  private c2vHandShakeFilterModel = new BehaviorSubject<any>(null);
  private c2vHandShakePageNumber = new BehaviorSubject<number>(0);
  private c2vHandShakeNoOfRowCount = new BehaviorSubject<number>(10);

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(
    private http: HttpClient
  ) { }

  //#region Getting the ClientAwaitingPlacementList and stat counterFlags for banner from api
  getClientAwaitingPlacementList() {
    /** Call the API to get the VCSClientAwaitingPlacementList data here */
    return this.http.get(this.apiURL + '/GetVCSClientAwaitingPlacementList');
  }
  getClientAwaitingPlacementByPactApplicationID(pactApplicationID: number) {
    /** Call the API to get the VCSClientAwaitingPlacementList data here */
    return this.http.post(this.apiURL + '/GetVCSClientAwaitingPlacementByPactApplicationID', pactApplicationID, this.httpOptions);
  }

  getClientAwaitingPlacementCounterFlags() {
    return this.http.get(this.apiURL + '/GetVCSClientAwaitingPlacementCounterFlags');
  }
  //#endregion

  //#region Get and Set for client selected from the ClientAwaitingPlacement pool
  setClientAwaitingPlacementSelected(data: IClientAwaitingPlacementData) {
    this.clientAwaitingPlacementSelected.next(data);
    // console.log(' setting clientAwaitingPlacementSelected : ', data);
  }

  getClientAwaitingPlacementSelected(): Observable<IClientAwaitingPlacementData> {
    return this.clientAwaitingPlacementSelected.asObservable();
  }
  //#endregion

  //#region Get and Set Ag-Grid Filter Model for Pages
  setCLAPFilterModel(filter: any) {
    this.clapFilterModel.next(filter);
  }
  getCLAPFilterModel() {
    return this.clapFilterModel.asObservable();
  }
  setC2VHandShakeFilterModel(filter: any) {
    this.c2vHandShakeFilterModel.next(filter);
  }
  getC2VHandShakeFilterModel() {
    return this.c2vHandShakeFilterModel.asObservable();
  }
  //#endregion

  //#region Get and Set User selected Ag-Grid PageNumber
  setCLAPPageNumber(pageNumber: number) {
    this.clapPageNumber.next(pageNumber);
  }
  getCLAPPageNumber() {
    return this.clapPageNumber.asObservable();
  }
  setC2VHandShakePageNumber(pageNumber: number) {
    this.c2vHandShakePageNumber.next(pageNumber);
  }
  getC2VHandShakePageNumber() {
    return this.c2vHandShakePageNumber.asObservable();
  }
  //#endregion

  //#region Get and Set User selected Ag-Grid NoOfRowCount
  setCLAPNoOfRowCount(rowCount: number) {
    this.clapNoOfRowCount.next(rowCount);
  }
  getCLAPNoOfRowCount() {
    return this.clapNoOfRowCount.asObservable();
  }
  setC2VHandShakeNoOfRowCount(rowCount: number) {
    this.c2vHandShakeNoOfRowCount.next(rowCount);
  }
  getC2VHandShakeNoOfRowCount() {
    return this.c2vHandShakeNoOfRowCount.asObservable();
  }
  //#endregion

  //#region Getting the List of Site after Matchlogic in backend for the given PactApplicationID
  getC2VHandshakeData(appID: number) {
    return this.http.post(this.apiURL + '/GetC2VHandShake', appID, this.httpOptions);
  }
  //#endregion

  //#region Get and Set for List of Site selected from C2V handshake screen for the select client for making a C2V referral
  setReferralSiteSelectedList(list: IC2VReferralHandshakeData[]) {
    this.referralSiteSelectedList.next(list);
    // console.log('referralSiteSelectedList', list);
  }

  getReferralSiteSelectedList(): Observable<IC2VReferralHandshakeData[]> {
    return this.referralSiteSelectedList.asObservable();
  }
  //#endregion

  //#region Get and Set for makeCoCReferral flag
  setMakeCoCReferral(input: boolean) {
    this.makeCoCReferral.next(input);
    // console.log('referralSiteSelectedList', list);
  }

  getMakeCoCReferral(): Observable<boolean> {
    return this.makeCoCReferral.asObservable();
  }
  //#endregion

  //#region Save Referral
  saveVCSReferral(referralList: VCSReferralData[]) {
    return this.http.post(this.apiURL + '/SaveVCSReferral', JSON.stringify(referralList), this.httpOptions);
  }
  //#endregion

  //#region Get the Available InterviewTime Slot for the Given HPSite for the given Date
  GetAvailableInterviewTimeSlot(inputValue: VCSAvailableInterviewTimeSlotInput) {
    return this.http.post(this.apiURL + '/GetVCSAvailableInterviewTimeSlot', JSON.stringify(inputValue), this.httpOptions);
  }
  //#endregion

}
