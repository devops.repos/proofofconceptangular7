import { Component, ViewEncapsulation } from '@angular/core';
import { ITooltipAngularComp, ICellRendererAngularComp } from 'ag-grid-angular';
// import {ITooltipAngularComp} from '@ag-grid-community/angular';

@Component({
  selector: 'app-tooltip-component',
  template: `
        <span class="ssn-toottip" matTooltip="{{params.data.ssn }}" matTooltipClass="vcs-info-tooltip">{{params.value}}<span>
  `,
  styles: [
    ` .ssn-toottip {
        cursor: pointer;
      }
      .ssn-toottip:hover {
        color: blue;
      }
    `
  ]
})
export class SSNTooltipComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    // console.log(this.params);
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }
}
