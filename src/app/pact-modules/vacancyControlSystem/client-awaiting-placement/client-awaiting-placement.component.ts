import { Component, OnInit, ViewChild, OnDestroy, ViewEncapsulation } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { CapActionComponent } from './cap-action.component';
import { Subscription } from 'rxjs';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { C2vService } from './c2v.service';
import { SSNTooltipComponent } from './ssn-tooltip.component';
import { CoCIconComponent } from './coc-icon.component';
import { InfoIconComponent } from './info-icon.component';
import { IClientAwaitingPlacementCounterFlags, IClientAwaitingPlacementData } from './c2v-interfaces.model';
import { PageSize } from 'src/app/models/pact-enums.enum';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';

@Component({
  selector: 'app-client-awaiting-placement',
  templateUrl: './client-awaiting-placement.component.html',
  styleUrls: ['./client-awaiting-placement.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClientAwaitingPlacementComponent implements OnInit, OnDestroy {
  clientAwaitingPlacementCounterFlags: IClientAwaitingPlacementCounterFlags = {
    numberOfHUDandSVAHigh: 0,
    numberOfNoPendingReferrals: 0,
    numberOfReferralHold: 0,
    numberOfCoCEligible: 0,
    numberOfApprovalExpiryInAWeek: 0
  };
  numberOfCAP = 0;
  flagClicked = '';

  @ViewChild('agGrid') agGrid: AgGridAngular;

  widthSideBar: number;
  clapFilterGroup: FormGroup;
  clapFiltersInitialValues;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  rowSelection;
  frameworkComponents;
  context;
  CLAPSavedFilterModel: any;
  pageNumber: number;
  noOfRowCounter: number;

  public gridOptions: GridOptions;
  rowData: IClientAwaitingPlacementData[];

  pageSizes = PageSize;

  genderTypes: RefGroupDetails[];
  boroTypes: RefGroupDetails[];
  responseItems: RefGroupDetails[];
  responseItemsYN: RefGroupDetails[];
  housingTypes: RefGroupDetails[];

  widthSideBarSub: Subscription;
  capCounterFlagSub: Subscription;
  rowDataSub: Subscription;
  CLAPSavedFilterModelSub: Subscription;
  pageNumberSub: Subscription;
  noOfRowCounterSub: Subscription;
  commonServiceSub: Subscription;

  constructor(
    private navService: NavService,
    private c2vService: C2vService,
    private commonService: CommonService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.clapFilterGroup = this.fb.group({
      referredToHpCtrl: [0],
      ageFromCtrl: [''],
      ageToCtrl: [{ value: '', disabled: true }],
      boroPref1Ctrl: [0],
      boroPref2Ctrl: [0],
      hudCtrl: [0],
      genderCtrl: [0],
      documentCompletedCtrl: [0],
      prioritizationCtrl: [0],
      serviceNeedsCtrl: [0],
      veteranCtrl: [0],
      dvCtrl: [0],
      householdCompositionCtrl: [''],
      wheelChairAccessibleCtrl: [0],
      mobilityImpairmentCtrl: [0],
      housingTypeCtrl: [0],
      substanceUseCtrl: [0],

    });
    this.clapFiltersInitialValues = this.clapFilterGroup.value;
    this.gridOptions = {
      rowHeight: 35,
      // headerHeight: 50,
      // skipHeaderOnAutoSize: true,
      getRowStyle: (params) => {
        if (params.node.data.isClientReferralReady == 34) {
          return { background: 'sandybrown' };
        }
      },
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'HRA Client# - Ref Date',
        field: 'clientNoRefDate',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.pactClientID + ' - ' + params.data.referralDate;
        },
      },
      {
        headerName: 'Client Name (L, F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName;
        },
        cellRendererSelector(params) {
          const cocIcon = {
            component: 'cocIconRenderer'
          };
          if (params.data.isHUD === true) {
            return cocIcon;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'SSN',
        field: 'socialSecurityNumber',
        filter: 'agTextColumnFilter',
        // cellRenderer: 'ssnToolTipRenderer',
        valueGetter(params) {
          if (params.data.ssn) {
            return params.data.ssn.substr(5, 4);
          } else {
            return '';
          }
        },
        cellRendererSelector(params) {
          const ssnToolTip = {
            component: 'ssnToolTipRenderer'
          };
          if (params.data.ssn) {
            return ssnToolTip;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Referring Agency/site',
        field: 'referringAgencySite',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.agencyName !== '' && params.data.siteName !== '') {
            return params.data.referringAgencyNo + ' - ' + params.data.agencyName + ' / ' + params.data.referringSiteNo + ' - ' + params.data.siteName;
          } else if (params.data.agencyName !== '' && params.data.siteName === '') {
            return params.data.referringAgencyNo + ' - ' + params.data.agencyName;
          } else if (params.data.agencyName === '' && params.data.siteName !== '') {
            return params.data.referringSiteNo + ' - ' + params.data.siteName;
          }
        }
      },
      {
        headerName: 'Eligibility',
        field: 'clientEligibleFor',
        width: 300,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Prioritization',
        field: 'svaPrioritization',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Service Needs',
        field: 'serviceNeeds',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Approval Expiry Date',
        field: 'approvalToDate',
        filter: 'agDateColumnFilter',
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
        headerTooltip: 'The date till when referral can be approved'
      },
      {
        headerName: 'Placements Criteria',
        field: 'placementCriteria',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Client Documentation Complete',
        field: 'isReferralPackageReady',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.isPackageReferralReady === 33) {
            return 'Y';
          } else {
            return 'N';
          }
        },
        cellRendererSelector(params) {
          const infoIcon = {
            component: 'infoIconRenderer'
          };
          if (params.data.isClientReferralReady === 34) {
            return infoIcon;
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'PendingReferrals',
        field: 'pendingReferrals',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Site Type',
        field: 'siteType',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Ethnicity',
        field: 'ethnicity',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Application Type',
        field: 'applicationStatus',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'HUD Chronic & SVA high',
        field: 'isHUDandSVAHigh',
        hide: true,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.isHUD === true && params.data.svaPrioritization !== null) {
            if ((params.data.svaPrioritization).toLowerCase() === 'sva-high' || (params.data.svaPrioritization).toLowerCase() === 'sva - high' || (params.data.svaPrioritization).toLowerCase() === 'high') {
              return 'Y';
            } else {
              return 'N';
            }
          } else {
            return 'N';
          }
        },
      },
      {
        headerName: 'Clients with no Pending Referrals',
        field: 'pendingReferrals1',
        hide: true,
        valueGetter(params) {
          return params.data.pendingReferrals;
        },
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Referral Hold clients',
        field: 'isReferralHold',
        hide: true,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.isClientReferralReady === 34) {
            // 34 = No
            return 'Y';
          } else {
            return 'N';
          }
        },
      },
      {
        headerName: 'Age',
        field: 'age',
        filter: 'agNumberColumnFilter',
        hide: true,
      },
      {
        headerName: 'Boro Preference1',
        field: 'boroPref1',
        hide: true,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Boro Preference2',
        field: 'boroPref2',
        hide: true,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'CoC eligible',
        field: 'isHUD',
        hide: true,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.isHUD === true) {
            return 'Y';
          } else {
            return 'N';
          }
        },
      },
      {
        headerName: 'Gender',
        field: 'gender',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Veteran',
        field: 'veteranType',
        hide: true,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.veteranType === 33) {
            return 'Y';
          } else {
            return 'N';
          }
        },
      },
      {
        headerName: 'Domestic Violence',
        field: 'domesticViolenceType',
        hide: true,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.domesticViolenceType === 33) {
            return 'Y';
          } else {
            return 'N';
          }
        },
      },
      {
        headerName: 'Household Composition',
        field: 'houseHoldComposition',
        hide: true,
        filter: 'agNumberColumnFilter',
      },
      {
        headerName: 'Housing Type',
        field: 'housingTypeDescription',
        hide: true,
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Mobility Impairment',
        field: 'mobilityImpairment',
        hide: true,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.mobilityImpairment === 33) {
            return 'Y';
          } else if (params.data.mobilityImpairment === 34) {
            return 'N';
          } else if (params.data.mobilityImpairment === 498) {
            return 'No Preference'
          } else {
            return '';
          }
        },
      },
      {
        headerName: 'WheelChair Accessible',
        field: 'wheelchairAccessible',
        hide: true,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.wheelchairAccessible === 33) {
            return 'Y';
          } else if (params.data.wheelchairAccessible === 34) {
            return 'N';
          } else if (params.data.mobilityImpairment === 498) {
            return 'No Preference'
          } else {
            return '';
          }
        },
      },
      {
        headerName: 'Substance Use',
        field: 'substanceUse',
        hide: true,
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.substanceUse === true) {
            return 'Y';
          } else {
            return 'N';
          }
        },
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'actionRenderer',
        // cellRendererSelector(params) {
        //   const actionButton = {
        //     component: 'actionRenderer'
        //   };
        //   if (!params.data.isReferralHold) {
        //     return actionButton;
        //   } else {
        //     return null;
        //   }
        // }
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: CapActionComponent,
      // ssnTooltip: SSNTooltipComponent,
      ssnToolTipRenderer: SSNTooltipComponent,
      cocIconRenderer: CoCIconComponent,
      infoIconRenderer: InfoIconComponent
    };
  }

  ngOnInit() {
    // Getting RefGroupItems
    const refGroupList = '4,6,7,105';
    this.commonServiceSub = this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.genderTypes = data.filter(d => d.refGroupID === 4);
        this.boroTypes = data.filter(d => d.refGroupID === 6);
        this.responseItems = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34 || d.refGroupDetailID === 498));
        this.responseItemsYN = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
        this.housingTypes = data.filter(d => d.refGroupID === 105);
      },
      error => {
        throw new Error(error.message);
      }
    );
    /** Getting the width of the sidenav to reflect the changes to nav toggle */
    this.widthSideBarSub = this.navService.getWidthSideBar().subscribe(res => {
      this.widthSideBar = res;
    });

    /** Getting the counter flags for ClientAwaitingPlacement banner  */
    this.capCounterFlagSub = this.c2vService.getClientAwaitingPlacementCounterFlags().subscribe((flags: IClientAwaitingPlacementCounterFlags) => {
      this.clientAwaitingPlacementCounterFlags = flags;
    });

    /* Reset the User Selected Filters, PageNumber and RowsCounter of C2V Handshake screen */
    this.c2vService.setC2VHandShakeFilterModel(null);
    this.c2vService.setC2VHandShakePageNumber(0);
    this.c2vService.setC2VHandShakeNoOfRowCount(10);

    // ########################## Advanced Filters For CLAP ################################################
    this.clapFilterGroup.get('referredToHpCtrl').valueChanges.subscribe(res => {
      if (res >= 0) {
        const filterComponent = this.gridApi.getFilterInstance('pendingReferrals');
        if (res === 33) {
          filterComponent.setModel({
            filterType: 'number',
            type: 'greaterThan',
            filter: 0
          })
        } else if (res === 34) {
          filterComponent.setModel({
            filterType: 'number',
            type: 'lessThanOrEqual',
            filter: 0
          })
        } else {
          filterComponent.setModel({
            filterType: 'number',
            type: 'empty',
            filter: ''
          })
        }
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('ageFromCtrl').valueChanges.subscribe(res => {
      if (res) {
        this.clapFilterGroup.get('ageToCtrl').enable();
      } else {
        this.clapFilterGroup.get('ageToCtrl').setValue('');
        this.clapFilterGroup.get('ageToCtrl').disable();
      }
    });
    this.clapFilterGroup.get('ageToCtrl').valueChanges.subscribe(res => {
      if (res) {
        const filterComponent = this.gridApi.getFilterInstance('age');
        filterComponent.setModel({
          filterType: 'number',
          type: 'inRange',
          filter: this.clapFilterGroup.get('ageFromCtrl').value,
          filterTo: this.clapFilterGroup.get('ageToCtrl').value,
          inRangeInclusive: true
        });
        this.gridApi.onFilterChanged();
      } else {
        const filterComponent = this.gridApi.getFilterInstance('age');
        filterComponent.setModel({
          filterType: 'number',
          type: 'empty',
          filter: ''
        });
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('boroPref1Ctrl').valueChanges.subscribe(res => {
      if (res && (res != '')) {
        const filterComponent = this.gridApi.getFilterInstance('boroPref1');
        if (res != 0) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: this.clapFilterGroup.get('boroPref1Ctrl').value
          });
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          })
        }
        this.gridApi.onFilterChanged();
      } else if (res == 0) {
        const filterComponent = this.gridApi.getFilterInstance('boroPref1');
        filterComponent.setModel({
          filterType: 'text',
          type: 'empty',
          filter: ''
        });
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('boroPref2Ctrl').valueChanges.subscribe(res => {
      if (res && (res != '')) {
        const filterComponent = this.gridApi.getFilterInstance('boroPref2');
        if (res != 0) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: this.clapFilterGroup.get('boroPref2Ctrl').value
          });
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          });
        }
        this.gridApi.onFilterChanged();
      } else if (res == 0) {
        const filterComponent = this.gridApi.getFilterInstance('boroPref2');
        filterComponent.setModel({
          filterType: 'text',
          type: 'empty',
          filter: ''
        });
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('hudCtrl').valueChanges.subscribe(res => {
      if (res >= 0) {
        const filterComponent = this.gridApi.getFilterInstance('isHUD');
        if (res === 33) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'Y'
          })
        } else if (res === 34) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'N'
          })
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          })
        }
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('genderCtrl').valueChanges.subscribe(res => {
      if (res && (res != '')) {
        const filterComponent = this.gridApi.getFilterInstance('gender');
        if (res != 0) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: this.clapFilterGroup.get('genderCtrl').value
          });
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          });
        }
        this.gridApi.onFilterChanged();
      } else if (res == 0) {
        const filterComponent = this.gridApi.getFilterInstance('gender');
        filterComponent.setModel({
          filterType: 'text',
          type: 'empty',
          filter: ''
        });
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('veteranCtrl').valueChanges.subscribe(res => {
      if (res >= 0) {
        const filterComponent = this.gridApi.getFilterInstance('veteranType');
        if (res === 33) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'Y'
          })
        } else if (res === 34) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'N'
          })
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          })
        }
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('dvCtrl').valueChanges.subscribe(res => {
      if (res >= 0) {
        const filterComponent = this.gridApi.getFilterInstance('domesticViolenceType');
        if (res === 33) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'Y'
          })
        } else if (res === 34) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'N'
          })
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          })
        }
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('householdCompositionCtrl').valueChanges.subscribe(res => {
      if (res >= 0) {
        const filterComponent = this.gridApi.getFilterInstance('houseHoldComposition');
        filterComponent.setModel({
          filterType: 'number',
          type: 'equals',
          filter: this.clapFilterGroup.get('householdCompositionCtrl').value
        });
        this.gridApi.onFilterChanged();
      } else {
        const filterComponent = this.gridApi.getFilterInstance('houseHoldComposition');
        filterComponent.setModel({
          filterType: 'text',
          type: 'empty',
          filter: ''
        });
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('housingTypeCtrl').valueChanges.subscribe(res => {
      if (res && (res != '')) {
        const filterComponent = this.gridApi.getFilterInstance('housingTypeDescription');
        if (res != 0) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: this.clapFilterGroup.get('housingTypeCtrl').value
          });
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          });
        }
        this.gridApi.onFilterChanged();
      } else if (res == 0) {
        const filterComponent = this.gridApi.getFilterInstance('housingTypeDescription');
        filterComponent.setModel({
          filterType: 'text',
          type: 'empty',
          filter: ''
        });
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('mobilityImpairmentCtrl').valueChanges.subscribe(res => {
      if (res >= 0) {
        const filterComponent = this.gridApi.getFilterInstance('mobilityImpairment');
        if (res === 33) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'Y'
          })
        } else if (res === 34) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'N'
          })
        } else if (res === 498) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'No Preference'
          })
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          })
        }
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('wheelChairAccessibleCtrl').valueChanges.subscribe(res => {
      if (res >= 0) {
        const filterComponent = this.gridApi.getFilterInstance('wheelchairAccessible');
        if (res === 33) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'Y'
          })
        } else if (res === 34) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'N'
          })
        } else if (res === 498) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'No Preference'
          })
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          })
        }
        this.gridApi.onFilterChanged();
      }
    });
    this.clapFilterGroup.get('substanceUseCtrl').valueChanges.subscribe(res => {
      if (res >= 0) {
        const filterComponent = this.gridApi.getFilterInstance('substanceUse');
        if (res === 33) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'Y'
          })
        } else if (res === 34) {
          filterComponent.setModel({
            filterType: 'text',
            type: 'equals',
            filter: 'N'
          })
        } else {
          filterComponent.setModel({
            filterType: 'text',
            type: 'empty',
            filter: ''
          })
        }
        this.gridApi.onFilterChanged();
      }
    });
    // ########################## END Advanced Filters For CLAP ################################################
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'clientEligibleFor') {
          allColumnIds.push(column.colId);
        }
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    /** API call to get the grid data */
    this.rowDataSub = this.c2vService.getClientAwaitingPlacementList().subscribe((res: IClientAwaitingPlacementData[]) => {
      if (res) {
        this.rowData = res;
        this.numberOfCAP = this.rowData.length;
        // console.log('rowData: ', this.rowData);
        // const prms = {
        //   force: true,
        //   columns: ['ssn']
        // };
        // this.gridApi.refreshCells(prms);
      }
    });
  }

  onFirstDataRendered(params) {
    this.CLAPSavedFilterModelSub = this.c2vService.getCLAPFilterModel().subscribe(res => {
      if (res) {
        this.CLAPSavedFilterModel = res;
        params.api.setFilterModel(this.CLAPSavedFilterModel);
        params.api.onFilterChanged();
      }
    });

    this.pageNumberSub = this.c2vService.getCLAPPageNumber().subscribe(pageNo => {
      if (pageNo) {
        this.pageNumber = pageNo;
        params.api.paginationGoToPage(this.pageNumber);
      }
    });
    this.noOfRowCounterSub = this.c2vService.getCLAPNoOfRowCount().subscribe(rowCount => {
      if (rowCount) {
        this.noOfRowCounter = rowCount;
        params.api.paginationSetPageSize(this.noOfRowCounter);
        params.api.refreshHeader();
      }
    });
    this.c2vService.getClientAwaitingPlacementSelected().subscribe(res => {
      if (res) {
        if (this.gridOptions.api) {
          this.gridOptions.api.forEachNode(node => node.data.vcsClientAwaitingPlacementID == res.vcsClientAwaitingPlacementID ? node.setSelected(true) : 0);
        }
      }
    })
  }

  onFilterChanged(event) {
    if (event.afterFloatingFilter) {
      this.flagClicked = '';
    }
  }

  //On Page Size Changed
  onPageSizeChanged() {
    // this.noOfRowCounter = noOfRowCount;
    this.gridOptions.api.paginationSetPageSize(Number(this.noOfRowCounter));
    this.gridOptions.api.refreshHeader();
  }

  selectedClientCatchedInParentPage(capSelected: IClientAwaitingPlacementData, route: string) {
    // alert("Refer Client Parent Component Method: " + JSON.stringify(cell));
    this.c2vService.setClientAwaitingPlacementSelected(capSelected);

    this.CLAPSavedFilterModel = this.gridApi.getFilterModel();
    this.c2vService.setCLAPFilterModel(this.CLAPSavedFilterModel);
    this.pageNumber = this.gridApi.paginationGetCurrentPage();
    this.c2vService.setCLAPPageNumber(this.pageNumber);
    this.c2vService.setCLAPNoOfRowCount(this.noOfRowCounter);

    this.router.navigate([route]);
  }

  selectCounterFilter(value: string, count?: number) {
    if (value === 'isHUDandSVAHigh') {
      this.flagClicked = 'HUD Chronic & SVA High';
    } else if (value === 'pendingReferrals1') {
      this.flagClicked = 'no pending referrals';
    } else if (value === 'isReferralHold') {
      this.flagClicked = 'Referral hold clients';
    } else if (value === 'isHUD') {
      this.flagClicked = 'CoC eligible';
    }
    this.gridOptions.api.setFilterModel(null);
    const instance = this.gridApi.getFilterInstance(value);
    // instance.selectNothing(value);
    // if (count == 0) {
    //   instance.suppressSelectAll = true;
    // } else {
    if (value === 'pendingReferrals1') {
      // instance.selectValue('0');
      instance.setModel({
        filterType: 'number',
        type: 'equals',
        filter: 0
      })
    } else {
      // instance.selectValue('Y');
      instance.setModel({
        filterType: 'text',
        type: 'equals',
        filter: 'Y'
      })
    }
    // }
    // instance.applyModel();
    this.gridApi.onFilterChanged();

  }

  selectNothing(value?: string) {
    const instance = this.gridApi.getFilterInstance(value);
    instance.selectNothing();
    instance.applyModel();
    this.gridApi.onFilterChanged();
  }

  selectExpiryDateInWeekFilter() {
    this.flagClicked = 'Approval Expiry Date in a week';
    const dateFilterComponent = this.gridApi.getFilterInstance('approvalToDate');
    this.gridOptions.api.setFilterModel(null);

    const d = new Date();
    const day1 = d.getDate();
    const month1 = d.getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12
    const year1 = d.getFullYear();

    d.setDate(d.getDate() + 7);   // adding 7 days to current date

    const day2 = d.getDate();
    const month2 = d.getMonth() + 1;
    const year2 = d.getFullYear();

    dateFilterComponent.setModel({
      type: 'inRange',
      dateFrom: year1 + '-' + month1 + '-' + (day1 - 1),
      dateTo: year2 + '-' + month2 + '-' + (day2 + 1)
    });
    this.gridApi.onFilterChanged();
    this.gridApi.refreshHeader();
  }



  refreshAgGrid() {
    this.flagClicked = '';
    this.gridOptions.api.setFilterModel(null);
    this.gridOptions.api.paginationGoToPage(0);
    this.gridOptions.api.paginationSetPageSize(10);
    this.gridOptions.api.refreshHeader();
    this.c2vService.setCLAPFilterModel(null);
    this.c2vService.setCLAPPageNumber(0);
    this.c2vService.setCLAPNoOfRowCount(10);
    this.clapFilterGroup.reset(this.clapFiltersInitialValues);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'clientAwaitingPlacementList-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };

    // console.log('clientAwaitingPlacementList-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.widthSideBarSub) {
      this.widthSideBarSub.unsubscribe();
    }
    if (this.capCounterFlagSub) {
      this.capCounterFlagSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.CLAPSavedFilterModelSub) {
      this.CLAPSavedFilterModelSub.unsubscribe();
    }
    if (this.pageNumberSub) {
      this.pageNumberSub.unsubscribe();
    }
    if (this.noOfRowCounterSub) {
      this.noOfRowCounterSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
  }
}
