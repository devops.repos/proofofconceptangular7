export interface IClientAwaitingPlacementData {
  vcsClientAwaitingPlacementID: number;
  pactApplicationID: number;
  referringAgencyID: number;
  referringAgencyNo: string;
  referringSiteID: number;
  referringSiteNo: string;
  pactClientID: number;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  referralDate: string;
  ssn: string;
  agencyName: string;
  siteName: string;
  clientEligibleFor: string;
  svaPrioritization: string;
  serviceNeeds: string;
  approvalFromDate: string;
  approvalToDate: string;
  placementCriteria: string;
  siteType: string;
  gender: string;
  ethnicity: string;
  age: number;
  borough: string;
  applicationStatus: string;
  isPackageReferralReady: number;
  packageReferralReadyComment: string;
  isClientReferralReady: number;
  clientReferralReadyComment: string;
  clientNotReadyReason: number;
  clientNotReadyOtherSpecify: string;
  clientNotReadyAddlComments: string;
  clientPackageUpdatedBy: string;
  clientPackageUpdatedDate: string;
  pendingReferrals: number;
  isHUD: boolean;
  isActive: boolean;
  createdBy: number;
  createdDate: string;
  updatedBy: number;
  updatedDate: string;
  veteranType?: number;
  domesticViolenceType?: number;
  housingTypeDescription?: string;
  houseHoldComposition?: number;
  substanceUse?: boolean;
  boroPref1?: string;
  boroPref2?: string;
  mobilityImpairment?: number;
  wheelchairAccessible?: number;
}

export interface IClientAwaitingPlacementCounterFlags {
  numberOfHUDandSVAHigh: number;
  numberOfNoPendingReferrals: number;
  numberOfReferralHold: number;
  numberOfCoCEligible: number;
  numberOfApprovalExpiryInAWeek: number;
}

export class IC2VReferralHandshakeData {
  hpAgencyID: number;
  hpSiteID: number;
  hpAgencyNo: number;
  hpSiteNo: string;
  hpAgencyName: string;
  hpSiteName: string;
  primaryServiceContractID: number;
  primaryServiceContractName: string;
  agreementPopulationID: number;
  populationName: string;
  siteAgreementPopulationID: number;
  agreementTypeDescription: string;
  boroughType?: number;
  boroughTypeDescription?: string;
  siteType?: number;
  siteTypeDescription?: string;
  siteFeatures?: string;
  siteLocationType: number;
  siteLocationTypeDescription: string;
  siteAddress: string;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhoneNo: string;
  unitsOccupied: number;
  unitsAvailable: number;
  totalUnits: number;
  pendingReferrals: number;
  assignUnitType?: VCSUnitType[];
  selectedUnitType: number;
  assignUnitTypeDescription: string;
  matchPercentage: number;
  placementMatchCriteria: VCSPlacementMatchCriteria[];
  interviewDate: any;
  interviewTime: string;
  changedInterviewDate: any;
  changedInterviewTime: string;
  availableInterviewTimeSlot: VCSAvailableInterviewTimeSlot[];
}

export class VCSUnitType {
  unitType: number;
  unitTypeDescription: string;
}

export interface VCSReferralData {
  vcsReferralID: number;
  pactApplicationID: number;
  vcsID: string;
  clientID: number;
  referralDate: string;
  clientSourceType: number;
  referringAgencyID: number;
  referringSiteID: number;
  hpAgencyID: number;
  hpSiteID: number;
  siteAgreementPopulationID: number;
  referralGroupGUID: string;
  referralType: number;
  referralReceivedType: number;
  referralReceivedDate: string;
  referralReceivedOtherSpecify: string;
  isScheduleMandatory: number;
  unitType: number;
  matchPercent: number;
  referralStatusType: number;
  cocType: number;
  withdrawnReasonType: number;
  referralClosedComment: string;
  interviewDate: string;
  interviewTime: string;
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhoneNo: string;
  placementMatchCriteria: VCSPlacementMatchCriteria[];
  createdBy: string;
  updatedBy: string;
}

export interface VCSAvailableInterviewTimeSlotInput {
  HPSiteID: number;
  InterviewDate: string;
}

export interface VCSAvailableInterviewTimeSlot {
  availableTimeSlot: string;
}

export interface VCSPlacementMatchCriteria {
  criteriaID: number;
  criteriaDescription: string;
}

export interface IVCSInterviewAddress {
  interviewAddress: string;
  interviewCity: string;
  interviewState: string;
  interviewZip: string;
  interviewPhoneNo: string;
}
