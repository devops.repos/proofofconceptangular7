import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAwaitingPlacementComponent } from './client-awaiting-placement.component';

describe('ClientAwaitingPlacementComponent', () => {
  let component: ClientAwaitingPlacementComponent;
  let fixture: ComponentFixture<ClientAwaitingPlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAwaitingPlacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAwaitingPlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
