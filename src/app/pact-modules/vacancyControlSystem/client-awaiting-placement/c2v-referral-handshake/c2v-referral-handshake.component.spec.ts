import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { C2VReferralHandshakeComponent } from './c2v-referral-handshake.component';

describe('C2VReferralHandshakeComponent', () => {
  let component: C2VReferralHandshakeComponent;
  let fixture: ComponentFixture<C2VReferralHandshakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ C2VReferralHandshakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(C2VReferralHandshakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
