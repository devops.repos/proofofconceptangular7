import { Component, ViewEncapsulation } from '@angular/core';
import { ITooltipAngularComp, ICellRendererAngularComp } from 'ag-grid-angular';
// import {ITooltipAngularComp} from '@ag-grid-community/angular';

@Component({
  selector: 'app-match-percentage-tooltip-component',
  template: `
    <!-- <div class="custom-tooltip" [style.background-color]="data.color">
      <p>
        <span *ngIf="data.placementMatchCriteria">
            <p><u>List of Matched Criteria are:</u></p>
            <li *ngFor="let criteria of data.placementMatchCriteria">
                {{criteria.criteriaDescription}}
            </li>
        </span>
      </p>
    </div> -->
    <span class="match-percentage" matTooltip="{{tooltipMessage}}" matTooltipClass="vcs-info-tooltip">{{params.data.matchPercentage + '%'}}<span>
  `,
  styles: [
    `
      .match-percentage {
        cursor: pointer;
      }
      .match-percentage:hover {
        color: blue;
      }
      /* :host {
        position: absolute;
        border: 1px solid cornflowerblue;
        overflow: hidden;
        pointer-events: none;
        transition: opacity 1s;
      }

      :host.ag-tooltip-hiding {
        opacity: 0;
      }

      .custom-tooltip p {
        margin: 0px;
        padding: 5px;
        white-space: nowrap;
      }

      .custom-tooltip p:first-of-type {
        font-weight: bold;
      } */
    `
  ]
})
export class MatchPercentageTooltipComponent implements ICellRendererAngularComp {
  params: any;
  public cell: any;
  tooltipMessage = '';

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };

    this.tooltipMessage = 'List of Matched Criteria are:\n';
    var index = 0;
    if (params.data.placementMatchCriteria.length > 0) {
      params.data.placementMatchCriteria.forEach(criteria => {
        index++;
        this.tooltipMessage += index + '. ' + criteria.criteriaDescription + '\n';
      });
    }
  }

  refresh(): boolean {
    return false;
  }
}
// export class MatchPercentageTooltipComponent implements ITooltipAngularComp {
//   params: any;
//   data: any;

//   agInit(params): void {
//     this.params = params;
//     this.data = params.api.getDisplayedRowAtIndex(params.rowIndex).data;
//     this.data.color = this.params.color || 'white';
//     console.log('params: ', this.params);
//     console.log('data: ', this.data);
//   }
// }
