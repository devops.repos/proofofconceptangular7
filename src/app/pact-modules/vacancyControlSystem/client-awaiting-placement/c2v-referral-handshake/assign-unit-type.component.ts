import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import { MasterDropdown } from 'src/app/models/masterDropdown.module';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: 'app-assign-unit-type',
    template: `
    <mat-form-field appearance="outline">
        <mat-select [(ngModel)]="selectedValue" placeholder="Select">
        <!-- <mat-option [value]="0">Select One</mat-option> -->
        <mat-option (click)="onUnitTypeSelected()" *ngFor="let unitType of params.data.assignUnitType" [value]="unitType.unitType">
            {{unitType.unitTypeDescription}}
        </mat-option>
        </mat-select>
    </mat-form-field>
    `
})
export class AssignUnitTypeComponent implements ICellRendererAngularComp {
    params: any;
    public cell: any;

    selectedValue = 0;

    agInit(params: any): void {
        this.params = params;
        // console.log(this.params);
        // console.log(this.params.data);
        // this.cell = {row: params.value, col: params.colDef.headerName};
        this.cell = {row: params.node.data, col: params.colDef.headerName};
        // this.cell.row.assignUnitType = this.selectedValue;
        this.selectedValue = params.data.selectedUnitType ? parseInt(params.data.selectedUnitType) : 0;
    }

    onUnitTypeSelected(): void {
        // this.cell.row.assignUnitType = this.selectedValue;
        //this.params.data.selectedUnitType = this.selectedValue;
        // console.log('selectedValue: ',this.selectedValue);
        this.params.context.componentParent.onUnitTypeSelected(this.selectedValue,this.params.node);
      }

    refresh(): boolean {
        return false;
    }
}
