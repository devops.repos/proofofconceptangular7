

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridOptions, RowNode } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { AssignUnitTypeComponent } from './assign-unit-type.component';
import { C2vService } from '../c2v.service';
import { IClientAwaitingPlacementData, IC2VReferralHandshakeData } from '../c2v-interfaces.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { MatchPercentageTooltipComponent } from './match-percentage-tooltip.component';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { Subscription } from 'rxjs';
import { PageSize } from 'src/app/models/pact-enums.enum';

@Component({
  selector: 'app-c2v-referral-handshake',
  templateUrl: './c2v-referral-handshake.component.html',
  styleUrls: ['./c2v-referral-handshake.component.scss']
})
export class C2VReferralHandshakeComponent implements OnInit, OnDestroy {

  clientSelected: IClientAwaitingPlacementData = null; // Client Selected from ClientAwaitingPlacement Page
  clientSelectedSub: Subscription;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  pageSizes = PageSize;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  rowSelection;
  context: any;
  frameworkComponents;
  savedFilterModel: any;
  pageNumber: number;
  noOfRowCounter: number;

  public gridOptions: GridOptions;
  rowData: IC2VReferralHandshakeData[];

  unitType: RefGroupDetails[];

  rowDataSub: Subscription;
  commonServiceSub: Subscription;
  savedFilterModelSub: Subscription;
  pageNumberSub: Subscription;
  noOfRowCounterSub: Subscription;

  constructor(
    private c2vService: C2vService,
    private toastrService: ToastrService,
    private router: Router,
    private commonService: CommonService,
    private confirmDialogService: ConfirmDialogService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Housing Provider Agency/Site#',
        filter: 'agTextColumnFilter',
        width: 100,
        suppressSizeToFit: true,
        valueGetter(params) {
          return params.data.hpAgencyNo + ' / ' + params.data.hpSiteNo;
        }
      },
      {
        headerName: 'Housing Provider Agency/Site',
        field: 'HPAgencySiteName',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          if (params.data.hpAgencyName !== '' && params.data.hpSiteName !== '') {
            return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName + ' / ' + params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
          } else if (params.data.hpAgencyName !== '' && params.data.hpSiteName == '') {
            return params.data.hpAgencyNo + ' - ' + params.data.hpAgencyName;
          } else if (params.data.hpAgencyName == '' && params.data.hpSiteName !== '') {
            return params.data.hpSiteNo + ' - ' + params.data.hpSiteName;
          }
        }
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'agreementTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Location',
        field: 'siteLocationTypeDescription',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Assign Unit Type',
        field: 'selectedUnitType',
        width: 225,
        headerTooltip: 'Select the unit type from the dropdown',
        cellRenderer: 'dropdownRenderer',

      },
      {
        headerName: 'Site Address',
        field: 'siteAddress',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Units Occupied',
        field: 'unitsOccupied',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Units Available',
        field: 'unitsAvailable',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Pending Referrals',
        field: 'pendingReferrals',
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Match Percentage',
        field: 'matchPercentage',
        filter: 'agNumberColumnFilter',
        cellRenderer: 'matchPercentageTooltip',
        // valueGetter(params) {
        //   return params.data.matchPercentage + '%';
        // },
        // cellStyle: {cursor: 'pointer'},
        // tooltipField: 'matchPercentage',
        // // tooltipComponentParams: { color: '#ececec' },
        // tooltipComponent: 'matchPercentageTooltip'
      },
      {
        headerName: 'Site Type',
        field: 'siteTypeDescription',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Borough',
        field: 'boroughTypeDescription',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Site Features',
        field: 'siteFeatures',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Select',
        field: 'select',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        checkboxSelection: true
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };
    this.rowSelection = 'multiple';
    this.context = { componentParent: this };
    this.frameworkComponents = {
      dropdownRenderer: AssignUnitTypeComponent,
      matchPercentageTooltip: MatchPercentageTooltipComponent
    };
  }

  ngOnInit() {
    // Get Refgroup Details
    const refGroupList = '29';
    this.commonService.getRefGroupDetails(refGroupList).subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.unitType = data.filter(d => d.refGroupID === 29);
      },
      error => {
        throw new Error(error.message);
      }
    );

    /** Get the selected Client for referral */
    this.c2vService.getClientAwaitingPlacementSelected().subscribe(res => {
      this.clientSelected = res;
      // console.log('this.clientSelected: ', this.clientSelected);
    });
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {

      if (column.colId !== 'select') {
        if (column.colId !== 'selectedUnitType') {
          // console.log('column.colID: ' + column.colId);
          allColumnIds.push(column.colId);
        }
      }

    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    // params.api.expandAll();

    /** Getting the C2VHandshake data from api */
    if (this.clientSelected) {
      this.c2vService.getC2VHandshakeData(this.clientSelected.pactApplicationID).subscribe((res: IC2VReferralHandshakeData[]) => {
        this.rowData = res;
        // console.log('C2VHandshake data: ', this.rowData);
      });
    }
  }

  onRowSelected(event) {
    if(!event.node.selected && event.node.data.selectedUnitType > 0) {
      event.node.setDataValue('selectedUnitType', 0)
    }
    if (this.agGrid.api.getSelectedNodes().length > 3) {
      // alert('more than 3 selected');
      const title = 'Alert';
      const primaryMessage = `You can make referrals to only 3 Site at a time.`;
      const secondaryMessage = ``;
      const confirmButtonName = 'OK';
      const dismissButtonName = '';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        positiveResponse => { },
        negativeResponse => { }
      );
      event.node.setSelected(false);
      event.node.setDataValue('selectedUnitType', 0);
    }
  }

  getSelectedRows() {
    if (this.agGrid.api.getSelectedNodes().length > 0) {
      const selectedNodes = this.agGrid.api.getSelectedNodes();
      const selectedData = selectedNodes.map(node => node.data);
      const referralSiteSelectedList: IC2VReferralHandshakeData[] = [];
      let cocReferralSiteSelectedList = 0;
      let formValid = false;
      selectedData.map((node: IC2VReferralHandshakeData) => {
        /* Below Code is when UnitType is Mandatory (Please uncomment this section when UnitType is mandatory)*/
        // if (node.selectedUnitType == 0 || node.selectedUnitType == null) {
        //   formValid = false;
        // } else {
        //   for (const val of this.unitType) {
        //     if (val.refGroupDetailID == node.selectedUnitType) {
        //       node.assignUnitTypeDescription = val.refGroupDetailDescription;
        //     }
        //   }
        //   if (node.primaryServiceContractID == 9) {
        //     // 9 = HUD CoC Service Funding
        //     cocReferralSiteSelectedList++;
        //   }
        //   formValid = true;
        //   referralSiteSelectedList.push(node);
        // }

        /* Below Code is when UnitType is not Mandatory (Please comment this section when UnitType is mandatory)*/
        if (node.selectedUnitType == 0 || node.selectedUnitType == null) {
          formValid = false;
        }
        if (node.selectedUnitType > 0) {
          formValid = true;
          for (const val of this.unitType) {
            if (val.refGroupDetailID == node.selectedUnitType) {
              node.assignUnitTypeDescription = val.refGroupDetailDescription;
            }
          }
        }
        if (node.primaryServiceContractID == 9) {
          // 9 = HUD CoC Service Funding
          cocReferralSiteSelectedList++;
        }

        referralSiteSelectedList.push(node);
      });
      /* Unit Type updated as Non Mandatory (Temporary code - later this field will be mandatory, then uncomment below section code and comment this section code) */
      if (!formValid) {
        const title = 'Verify';
        const primaryMessage = `UnitType not selected for some of the selected HP Sites.`;
        const secondaryMessage = `Are you sure you want to continue?`;
        const confirmButtonName = 'Yes';
        const dismissButtonName = 'No';
        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          positiveResponse => {
            if (cocReferralSiteSelectedList > 0 && this.clientSelected.isHUD == true) {
              const title = 'Verify';
              const primaryMessage = `Some of the sites selected are CoC, `;
              const secondaryMessage = `do you intend to make CoC Referral for this client.`;
              const confirmButtonName = 'Yes';
              const dismissButtonName = 'No';
              this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                positiveResponse => {
                  this.c2vService.setMakeCoCReferral(true);
                  this.saveReferralSiteSelectedList(referralSiteSelectedList);
                },
                negativeResponse => {
                  this.c2vService.setMakeCoCReferral(false);
                  this.saveReferralSiteSelectedList(referralSiteSelectedList);
                }
              );
            } else {
              this.c2vService.setMakeCoCReferral(false);
              this.saveReferralSiteSelectedList(referralSiteSelectedList);
            }
          },
          negativeResponse => { }
        );
      } else {
        if (cocReferralSiteSelectedList > 0 && this.clientSelected.isHUD == true) {
          const title = 'Verify';
          const primaryMessage = `Some of the sites selected are CoC, `;
          const secondaryMessage = `do you intend to make CoC Referral for this client.`;
          const confirmButtonName = 'Yes';
          const dismissButtonName = 'No';
          this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
            positiveResponse => {
              this.c2vService.setMakeCoCReferral(true);
              this.saveReferralSiteSelectedList(referralSiteSelectedList);
            },
            negativeResponse => {
              this.c2vService.setMakeCoCReferral(false);
              this.saveReferralSiteSelectedList(referralSiteSelectedList);
            }
          );
        } else {
          this.c2vService.setMakeCoCReferral(false);
          this.saveReferralSiteSelectedList(referralSiteSelectedList);
        }
      }

      /* Below Code is when UnitType is Mandatory (Please comment above code and uncomment this section when UnitType is mandatory)*/
      // if (formValid) {
      //   if (cocReferralSiteSelectedList > 0 && this.clientSelected.isHUD == true) {
      //     const title = 'Verify';
      //     const primaryMessage = `Some of the sites selected are CoC, `;
      //     const secondaryMessage = `do you intend to make CoC Referral for this client.`;
      //     const confirmButtonName = 'Yes';
      //     const dismissButtonName = 'No';
      //     this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      //       positiveResponse => {
      //         this.c2vService.setMakeCoCReferral(true);
      //         this.saveReferralSiteSelectedList(referralSiteSelectedList);
      //       },
      //       negativeResponse => {
      //         this.c2vService.setMakeCoCReferral(false);
      //         this.saveReferralSiteSelectedList(referralSiteSelectedList);
      //       }
      //     );
      //   } else {
      //     this.c2vService.setMakeCoCReferral(false);
      //     this.saveReferralSiteSelectedList(referralSiteSelectedList);
      //   }
      // } else {
      //   this.toastrService.error('Please select the unit type for the row selected', 'UnitType Missing');
      // }
    } else {
      this.toastrService.error('Please select at least one Site to proceed');
    }
  }

  onFirstDataRendered(params) {
    this.savedFilterModelSub = this.c2vService.getC2VHandShakeFilterModel().subscribe(res => {
      if (res) {
        this.savedFilterModel = res;
        params.api.setFilterModel(this.savedFilterModel);
        params.api.onFilterChanged();
      }
    });

    this.pageNumberSub = this.c2vService.getC2VHandShakePageNumber().subscribe(pageNo => {
      if (pageNo) {
        this.pageNumber = pageNo;
        params.api.paginationGoToPage(this.pageNumber);
      }
    });
    this.noOfRowCounterSub = this.c2vService.getC2VHandShakeNoOfRowCount().subscribe(rowCount => {
      if (rowCount) {
        this.noOfRowCounter = rowCount;
        params.api.paginationSetPageSize(this.noOfRowCounter);
        params.api.refreshHeader();
      }
    });
  }

  onUnitTypeSelected(unitType: any, rowNodes: RowNode) {
    rowNodes.setSelected(unitType > 0);
    rowNodes.setDataValue('selectedUnitType', unitType);
    // console.log('onUnitTypeSelected - ', unitType, rowNodes);
  }

  //On Page Size Changed
  onPageSizeChanged() {
    // this.noOfRowCounter = noOfRowCount;
    this.gridOptions.api.paginationSetPageSize(Number(this.noOfRowCounter));
    this.gridOptions.api.refreshHeader();
  }

  saveReferralSiteSelectedList(referralSiteSelectedList: IC2VReferralHandshakeData[]) {
    this.c2vService.setReferralSiteSelectedList(referralSiteSelectedList);

    this.savedFilterModel = this.gridApi.getFilterModel();
    this.c2vService.setC2VHandShakeFilterModel(this.savedFilterModel);
    this.pageNumber = this.gridApi.paginationGetCurrentPage();
    this.c2vService.setC2VHandShakePageNumber(this.pageNumber);
    this.c2vService.setC2VHandShakeNoOfRowCount(this.noOfRowCounter);

    this.router.navigate(['/vcs/c2v-referral-handshake-success']);
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
    this.gridOptions.api.paginationGoToPage(0);
    this.gridOptions.api.paginationSetPageSize(10);
    this.gridOptions.api.refreshHeader();
    this.c2vService.setC2VHandShakeFilterModel(null);
    this.c2vService.setC2VHandShakePageNumber(0);
    this.c2vService.setC2VHandShakeNoOfRowCount(10);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'clientReferralHandshake-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

    // console.log('clientReferralHandshake-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.clientSelectedSub) {
      this.clientSelectedSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.savedFilterModelSub) {
      this.savedFilterModelSub.unsubscribe();
    }
    if (this.pageNumberSub) {
      this.pageNumberSub.unsubscribe();
    }
    if (this.noOfRowCounterSub) {
      this.noOfRowCounterSub.unsubscribe();
    }
  }
}


