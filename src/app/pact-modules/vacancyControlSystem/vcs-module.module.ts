import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../shared/material/material.module';
import { ErrorsModule } from '../../shared/errors-handling/errors.module';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { TextMaskModule } from 'angular2-text-mask';

import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';

import { VcsRoutingModule } from './vcs-routing.module';
import { ContentBannerModule } from '../../shared/content-banner/content-banner.module';
import { PactCalendarModule } from '../../shared/calendar/pact-calendar.module';
import { ConfirmDialogModule } from 'src/app/shared/confirm-dialog/confirm-dialog.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { DetClientBannerModule } from '../../shared/determination/client-banner/det-client-banner.module';

import { CustomDateComponent } from './custom-date-component.component';

import { ClientAwaitingPlacementComponent } from './client-awaiting-placement/client-awaiting-placement.component';
import { CapActionComponent } from './client-awaiting-placement/cap-action.component';
import { C2VReferralHandshakeComponent } from './client-awaiting-placement/c2v-referral-handshake/c2v-referral-handshake.component';
import { AssignUnitTypeComponent } from './client-awaiting-placement/c2v-referral-handshake/assign-unit-type.component';
import { MatchPercentageTooltipComponent } from './client-awaiting-placement/c2v-referral-handshake/match-percentage-tooltip.component';
import { C2VReferralHandshakeSuccessComponent } from './client-awaiting-placement/c2v-referral-handshake-success/c2v-referral-handshake-success.component';
import { C2vSchedulerComponent } from './client-awaiting-placement/c2v-scheduler/c2v-scheduler.component';
import { C2vSchedulerActionComponent } from './client-awaiting-placement/c2v-scheduler/c2v-scheduler-action.component';
import { C2vInterviewTimePickerComponent } from './client-awaiting-placement/c2v-scheduler/c2v-interview-time-picker.component';
import { C2vInterviewDatePickerComponent } from './client-awaiting-placement/c2v-scheduler/c2v-interview-date-picker.component';
import { C2vInterviewAddressDialogComponent } from './client-awaiting-placement/c2v-scheduler/c2v-interview-address-dialog/c2v-interview-address-dialog.component';

import { VacancyListingComponent } from './vacancy-listing/vacancy-listing.component';
import { V2CAgencySiteBannerModule } from 'src/app/shared/v2c-agency-site-banner/v2c-agency-site-banner.module';
import { V2cReferralHandshakeComponent } from './vacancy-listing/v2c-referral-handshake/v2c-referral-handshake.component';
import { V2cReferralHandshakeSuccessComponent } from './vacancy-listing/v2c-referral-handshake-success/v2c-referral-handshake-success.component';
import { V2cSchedulerComponent } from './vacancy-listing/v2c-scheduler/v2c-scheduler.component';
import { V2cSchedulerActionComponent } from './vacancy-listing/v2c-scheduler/v2c-scheduler-action.component';
import { V2cInterviewAddressDialogComponent } from './vacancy-listing/v2c-scheduler/v2c-interview-address-dialog/v2c-interview-address-dialog.component';

import { ReferralRosterComponent } from './referral-roster/referral-roster.component';
import { RRDraftActionComponent } from './referral-roster/rr-draft-action.component';
import { RRTransmittedActionComponent } from './referral-roster/rr-transmitted-action.component';
import { RRPendingActionComponent } from './referral-roster/rr-pending-action.component';
import { RRCompletedActionComponent } from './referral-roster/rr-completed-action.component';
import { RRInfoIconComponent } from './referral-roster/rr-info-icon.component';
import { WithdrawnDialogComponent } from './referral-roster/withdrawn-dialog/withdrawn-dialog.component';
import { RrApplicationPackageDialogComponent } from './referral-roster/rr-application-package-dialog/rr-application-package-dialog.component';
import { HousingApplicationSupportingDocumentsModule } from 'src/app/shared/housing-application-supporting-documents/housing-application-supporting-documents.module';
import { InterviewOutcomeComponent } from './referral-roster/interview-outcome/interview-outcome.component';
import { PlacementOutcomeModule } from 'src/app/shared/placement-outcome/placement-outcome.module';

import { TenantRosterComponent } from './tenant-roster/tenant-roster.component';
import { TRMyTenantsActionComponent } from './tenant-roster/tr-my-tenants-action.component';
import { TrInfoIconComponent } from './tenant-roster/tr-info-icon.component';
import { SideDoorClientSearchComponent } from './tenant-roster/side-door-client-search/side-door-client-search.component';
import { SideDoorClientSearchResultComponent } from './tenant-roster/side-door-client-search-result/side-door-client-search-result.component';
import { SideDoorMoveInComponent } from './tenant-roster/side-door-move-in/side-door-move-in.component';
import { AssignTenantToUnitComponent } from './tenant-roster/assign-tenant-to-unit/assign-tenant-to-unit.component';
import { UpdateTenantProfileComponent } from './tenant-roster/update-tenant-profile/update-tenant-profile.component';

import { PlacementsAwaitingVerificationComponent } from './placements-awaiting-verification/placements-awaiting-verification.component';
import { PAVPendingActionComponent } from './placements-awaiting-verification/pav-pending-action.component';
import { PAVVerifiedActionComponent } from './placements-awaiting-verification/pav-verified-action.component';
import { ClientPlacementsHistoryComponent } from './placements-awaiting-verification/client-placements-history/client-placements-history.component';
import { CPHActionComponent } from './placements-awaiting-verification/client-placements-history/cph-action.component';
import { VCUInfoIconComponent } from './placements-awaiting-verification/client-placements-history/vcu-info-icon.component';
import { PlacementClientCaseFolderComponent } from './placements-awaiting-verification/client-placements-history/placement-client-case-folder/placement-client-case-folder.component';
import { PlacementVerificationHistoryComponent } from './placements-awaiting-verification/client-placements-history/placement-verification-history/placement-verification-history.component';
import { PlacementHistoryClientSearchComponent } from './placements-awaiting-verification/placement-history-client-search/placement-history-client-search.component';
import { CSRActionComponent } from './placements-awaiting-verification/placement-history-client-search/csr-action.component';
import { DeletePlacementDialogComponent } from './placements-awaiting-verification/client-placements-history/delete-placement-dialog/delete-placement-dialog.component';

import { TadComponent } from './tad/tad.component';
import { TadActionComponent } from './tad/tad-action.component';
import { TadNoSubmitActionComponent } from './tad/tad-nosubmit-action.component';
import { TadSubmissionComponent } from './tad-submission/tad-submission.component';
import { TadUnitRosterComponent } from './tad-submission/tad-unit-roster/tad-unit-roster.component';
import { TadReferralRosterComponent } from './tad-submission/tad-referral-roster/tad-referral-roster.component';
import { TadTenantRosterComponent } from './tad-submission/tad-tenant-roster/tad-tenant-roster.component';
import { TadHPComponent } from './tad-submission/tad-hp/tad-hp.component';
import { TadOfflineInfoIconComponent } from './tad-submission/tad-hp/offline-info-icon.component';
import { TadMoveOutInfoIconComponent } from './tad-submission/tad-hp/moveOut-info-icon.component';
import { TadDiscrepancyIconComponent } from './tad-submission/tad-hp/discrepancy-icon.component';
import { TadAwaitingVerificationComponent } from './tad-awaiting-verification/tad-awaiting-verification.component';
import { PTadActionComponent } from './tad-awaiting-verification/ptad-action.component';
import { VTadActionComponent } from './tad-awaiting-verification/vtad-action.component';
import { HousingLiasionListComponent } from './tad-awaiting-verification/housing-liasion-list.component';
import { TadVerificationComponent } from './tad-awaiting-verification/tad-verification/tad-verification.component';
import { TadVerificationActionComponent } from './tad-awaiting-verification/tad-verification/tad-verification-action.component';

import { UnitRosterComponent } from './unit-roster/unit-roster.component';
import { SchedularComponent } from './schedular/schedular.component';

import {CreateSiteRequestComponent} from './site-request/create-site-request.component';
import {CreateSiteRequestDetailComponent} from './site-request/create-site-request-detail.component';
import {SiteContactRequestComponent} from './site-request/site-contact-request.component';
import {SiteContactRequestActionComponent} from './site-request/site-contact-request-action.component';
import {PrimaryServiceContractRequestComponent} from './site-request/primary-service-contract-request.component';
import {PrimaryServiceContractRequestActionComponent} from './site-request/primary-service-contract-request-action.component';
import {UnitDetailsRequestGridComponent} from './site-request/unit-details-request-grid.component';
import {UnitDetailsRequestActionComponent} from './site-request/unit-details-request-action.component';

import {SiteAgreementRequestComponent} from './site-agreement-request/site-agreement-request.component';
import {SiteContactRequestViewComponent} from './site-agreement-request/site-contact-request-view.component';

import { RequestRaSiteComponent } from './site-request-ra/site-request-ra.component';

import { NewAgencySiteRequestComponent } from './agency-site-request/new-agency-site-request.component';
import {NewAgencySiteDocumentComponent} from './agency-site-document/new-agency-site-document.component';
import {AgencySiteRequestQueueComponent} from './agency-site-request-queue/agency-site-request-queue.component';
import {RequestQueueFileUploadDialogComponent} from './agency-site-request-queue/request-queue-dialog/request-queue-file-upload-dialog.component';

import {ReviewedAgencySiteActionComponent} from './agency-site-request-queue/request-queue-action/reviewed-agency-site-action.component';
import {RequestSentAgencySiteActionComponent} from './agency-site-request-queue/request-queue-action/request-sent-agency-site-action.component';
import {PendingRequestAgencySiteActionComponent} from './agency-site-request-queue/request-queue-action/pending-requests-agency-site-action.component';
import {PendingApprovalAgencySiteActionComponent} from './agency-site-request-queue/request-queue-action/pending-approval-agency-site-action.component';

import {AgencyInfoReviewComponent} from './agency-site-request-review-ra/agency-info-review.component';
import {ApprovalInfoReviewComponent} from './agency-site-request-review-ra/approval-info-review.component';
import {RequestReviewRaComponent} from './agency-site-request-review-ra/request-review-ra.component';
import {SiteInfoReviewComponent} from './agency-site-request-review-ra/site-info-review.component';

import { AgencySiteMaintenanceComponent } from './agency-site-maintenance/agency-site-maintenance.component';
import { AgencySiteMaintenanceActionComponent } from './agency-site-maintenance/agency-site-maintenance-action.component';

import { SiteProfileComponent } from './agency-site-maintenance/site-profile/site-profile.component';
import { SiteDemographicComponent } from './agency-site-maintenance/site-profile/site-demographic.component';
import { SiteContactComponent } from './agency-site-maintenance/site-profile/site-contact.component';
import { SiteContactActionComponent } from './agency-site-maintenance/site-profile/site-contact-action.component';
import { SiteAgreementDetailComponent } from './agency-site-maintenance/site-profile/site-agreement-detail.component';

import {SiteProfileRequestComponent} from './agency-site-request-review/site-profile.component';
import {SiteAgreementDetailRequestComponent} from './agency-site-request-review/site-agreement-detail.component';
import {SiteApprovalRequestComponent} from './agency-site-request-review/site-approval.component';
import {SiteDemographicRequestComponent} from './agency-site-request-review/site-demographic.component';

import { HomelessInformationComponent } from './referral-package/homeless-information/homeless-information.component';
import { ReferralReadyTrackingComponent } from './referral-package/referral-ready-tracking/referral-ready-tracking.component';
import { ReferralHousingPreferenceComponent } from './referral-package/referral-housing-preference/referral-housing-preference.component';
import { ReferralAndPlacementSummaryComponent } from './referral-package/referral-and-placement-summary/referral-and-placement-summary.component';

import { CocReferralQueueComponent } from './coc-referral-queue/coc-referral-queue.component';
import { CoCActionComponent } from './coc-referral-queue/coc-action.component';
import { CocReferralDetailsComponent } from './coc-referral-queue/coc-referral-details/coc-referral-details.component';
import { MonitorVacanciesComponent } from './monitor-vacancies/monitor-vacancies.component';
import { NavFooterModule } from 'src/app/shared/nav-footer/nav-footer.module';
import { ReferralPackageClientBannerModule } from 'src/app/shared/referral-package-client-banner/referral-package-client-banner.module';
import { SharedADLModule } from 'src/app/shared/shared-adl/shared-adl.module';
import { HomelessHistoryModule } from 'src/app/shared/homeless-history/homeless-history.module';
import { RecommendedServicesModule } from 'src/app/shared/recommended-services/recommended-services.module';
import { ApplicantPreferencesModule } from 'src/app/shared/applicant-preferences/applicant-preferences.module';

import { NgxMaskModule } from 'ngx-mask';
import { SiteApprovalComponent } from './agency-site-maintenance/site-profile/site-approval.component';
import { SSNTooltipComponent } from './client-awaiting-placement/ssn-tooltip.component';
import { CoCIconComponent } from './client-awaiting-placement/coc-icon.component';
import { InfoIconComponent } from './client-awaiting-placement/info-icon.component';

import { PrimaryServiceContractComponent } from './agency-site-maintenance/site-profile/primary-service-contract.component';
import { CreateAgencySiteComponent } from './agency-site-maintenance/site-profile/create-agency-site.component';
import { CreateSiteComponent } from './agency-site-maintenance/site-profile/create-site.component';
import { CreateSiteActionComponent } from './agency-site-maintenance/site-profile/create-site-action.component';
import {CreateSiteDetailComponent} from './agency-site-maintenance/site-profile/create-site-detail.component';
import { PrimaryServiceContractActionComponent } from './agency-site-maintenance/site-profile/primary-service-contract-action.component';
import { VLActionComponent } from './vacancy-listing/vl-action.component';

import { ContactsActionComponent } from './referral-package/homeless-information/homeless-information-contacts-action.component';
import { TransferSiteComponent } from './agency-site-maintenance/site-profile/transfer-site.component';
import { MVVerifyActionComponent } from './monitor-vacancies/mv-verify-action.component';
import { VerifyVacancyComponent } from './monitor-vacancies/verify-vacancy.component';
import { DocumentModule } from 'src/app/shared/document/document.module';
import { AddCocReferralComponent } from './coc-referral-queue/add-coc-referral/add-coc-referral.component';

import {AddPrimaryServiceContractComponent} from './primary-service-agreement-pop-config/new-primary-service-contract/add-primary-service-contract.component';
import {AdditionalInfoPrimaryServiceContractComponent} from './primary-service-agreement-pop-config/new-primary-service-contract/additional-information.component';
import {ContractInfoPrimaryServiceContractComponent} from './primary-service-agreement-pop-config/new-primary-service-contract/contract-information.component';

import {PrimaryServiceAgreePopMainGridComponent} from './primary-service-agreement-pop-config/MainGrid/primary-service-agreement-pop-mainGrid.component';
import {PrimaryServiceAgreementConfigurationActionComponent} from './primary-service-agreement-pop-config/MainGrid/primary-service-agreement-pop-action.component';
import { PlacementAgencyGridActionComponent } from './primary-service-agreement-pop-config/new-primary-service-contract/action-components/placement-agency-grid-action.component';
import { PopulationUnitTypeGridActionComponent } from './primary-service-agreement-pop-config/new-primary-service-contract/action-components/population-unitType-grid-action.component';
import { PopulationsGridActionComponent } from './primary-service-agreement-pop-config/new-primary-service-contract/action-components/populations-grid-action.component';
import { MoveOutTenantComponent } from './tenant-roster/move-out-tenant/move-out-tenant.component';
import { UserAdministrationComponent } from './user-administration/user-administration.component';
import { UserActionComponent } from './user-administration/user-action.component';
import { UserSiteContactActionComponent } from './user-administration/user-profile/site-contact-action.component';
import { AssignSupervisorComponent } from './user-administration/user-profile/assign-supervisor-action.component';
import { SiteDetailComponent } from './agency-site-maintenance/site-profile/site-detail.component';
import {SharedUnitDetailsModule} from 'src/app/shared/unit-details/shared-unit-details.module';
import { UnitRosterDialogComponent } from './vacancy-listing/unit-roster-dialog/unit-roster-dialog.component';
import {SiteMoreContactsDialogComponent} from './agency-site-maintenance/site-profile/site-more-contacts-dialog.component';
import { AdlReviewModule } from 'src/app/shared/adl-review/adl-review.module';
import { UserProfileComponent } from './user-administration/user-profile/user-profile.component';

import { AdminClientSearchComponent } from './Administration/admin-client-search/admin-client-search.component';
import { AdminToolsComponent } from './Administration/admin-tools/admin-tools.component';
import { CSActionComponent } from './Administration/admin-client-search/cs-action.component';
import { DeleteReferralComponent } from './Administration/admin-tools/delete-referral.component';
import {ExtendDeterminationComponent} from './Administration/admin-tools/extend-determination.component';
import { LiftSignoffComponent } from './Administration/admin-tools/lift-signoff.component';
import { MoveReferralComponent } from './Administration/admin-tools/move-referral.component';
import { AssignTadLiaisonComponent } from './assign-tad-liaison/assign-tad-liaison.component';
import { CaseClientDocumentsModule } from 'src/app/shared/client-case-folder/case-client-documents/case-client-documents.module';
import { CaseNotesModule } from 'src/app/shared/client-case-folder/case-notes/case-notes.module';
import { CoordinatedAssessmentSurveyModule } from 'src/app/shared/client-case-folder/coordinated-assessment-survey/coordinated-assessment-survey.module';
import { PreviousApplicationsDeterminationsModule } from 'src/app/shared/client-case-folder/previous-applications-determinations/previous-applications-determinations.module';
import { VcsReferralsPlacementsModule } from 'src/app/shared/client-case-folder/vcs-referrals-placements/vcs-referrals-placements.module';
import { VpsReferralsModule } from 'src/app/shared/client-case-folder/vps-referrals/vps-referrals.module';
import { AgencyDetailComponent } from './agency-site-maintenance/site-profile/agency-detail.component';
import { ReferralHoldModule } from 'src/app/shared/referral-hold/referral-hold.module';
import { AdminReferralHoldComponent } from './Administration/admin-tools/admin-referral-hold/admin-referral-hold.component';
import { UpdateHoldActionComponent } from './Administration/admin-tools/admin-referral-hold/update-hold-action.component';
import { UpdateClientComponent } from './Administration/admin-tools/update-client.component';
import { TutorialModule } from 'src/app/shared/tutorial/tutorial.module';
import { AdminClientCaseFolderComponent } from './Administration/admin-tools/admin-client-case-folder/admin-client-case-folder.component';
import { LegacyTadComponent } from './legacyTAD/legacytad.component'

@NgModule({
  declarations: [
    CustomDateComponent,
    ClientAwaitingPlacementComponent,
    CapActionComponent,
    SSNTooltipComponent,
    CoCIconComponent,
    InfoIconComponent,
    C2VReferralHandshakeComponent,
    AssignUnitTypeComponent,
    MatchPercentageTooltipComponent,

    C2VReferralHandshakeSuccessComponent,
    C2vSchedulerComponent,
    C2vSchedulerActionComponent,
    C2vInterviewDatePickerComponent,
    C2vInterviewTimePickerComponent,
    C2vInterviewAddressDialogComponent,

    VacancyListingComponent,
    VLActionComponent,
    UnitRosterDialogComponent,
    V2cReferralHandshakeComponent,
    V2cReferralHandshakeSuccessComponent,
    V2cSchedulerComponent,
    V2cSchedulerActionComponent,
    V2cInterviewAddressDialogComponent,

    ReferralRosterComponent,
    RRDraftActionComponent,
    RRTransmittedActionComponent,
    RRPendingActionComponent,
    RRCompletedActionComponent,
    RRInfoIconComponent,
    WithdrawnDialogComponent,
    RrApplicationPackageDialogComponent,
    InterviewOutcomeComponent,

    TenantRosterComponent,
    TRMyTenantsActionComponent,
    TrInfoIconComponent,
    AssignTenantToUnitComponent,
    UpdateTenantProfileComponent,
    SideDoorClientSearchComponent,
    SideDoorClientSearchResultComponent,
    SideDoorMoveInComponent,

    PlacementsAwaitingVerificationComponent,
    PAVPendingActionComponent,
    PAVVerifiedActionComponent,
    ClientPlacementsHistoryComponent,
    CPHActionComponent,
    VCUInfoIconComponent,
    PlacementClientCaseFolderComponent,
    PlacementVerificationHistoryComponent,
    PlacementHistoryClientSearchComponent,
    CSRActionComponent,
    DeletePlacementDialogComponent,

    TadComponent,
    TadActionComponent,
    TadNoSubmitActionComponent,
    TadSubmissionComponent,
    TadUnitRosterComponent,
    TadReferralRosterComponent,
    TadTenantRosterComponent,
    TadHPComponent,
    TadAwaitingVerificationComponent,
    TadOfflineInfoIconComponent,
    TadMoveOutInfoIconComponent,
    TadDiscrepancyIconComponent,
    PTadActionComponent,
    VTadActionComponent,
    HousingLiasionListComponent,
    TadVerificationComponent,
    TadVerificationActionComponent,

    UnitRosterComponent,
    SchedularComponent,

    SiteAgreementRequestComponent, SiteContactRequestViewComponent,

    RequestRaSiteComponent,

    CreateSiteRequestComponent,CreateSiteRequestDetailComponent,
    SiteContactRequestComponent,SiteContactRequestActionComponent,
    PrimaryServiceContractRequestComponent, PrimaryServiceContractRequestActionComponent,
    UnitDetailsRequestGridComponent, UnitDetailsRequestActionComponent,

    NewAgencySiteRequestComponent,
    NewAgencySiteDocumentComponent,
    AgencySiteRequestQueueComponent,
    RequestQueueFileUploadDialogComponent,

    ReviewedAgencySiteActionComponent,
    RequestSentAgencySiteActionComponent,
    PendingRequestAgencySiteActionComponent,
    PendingApprovalAgencySiteActionComponent,

    AgencyInfoReviewComponent,
    ApprovalInfoReviewComponent,
    RequestReviewRaComponent,
    SiteInfoReviewComponent,

    AgencySiteMaintenanceComponent,
    AgencySiteMaintenanceActionComponent,
    SiteProfileComponent,
    SiteDemographicComponent,
    SiteContactComponent,
    SiteContactActionComponent,
    SiteAgreementDetailComponent,

    SiteProfileRequestComponent, SiteAgreementDetailRequestComponent, SiteApprovalRequestComponent, SiteDemographicRequestComponent,
    HomelessInformationComponent,
    ReferralReadyTrackingComponent,
    ReferralHousingPreferenceComponent,
    ReferralAndPlacementSummaryComponent,
    CocReferralQueueComponent,
    CoCActionComponent,
    CocReferralDetailsComponent,
    MonitorVacanciesComponent,
    SiteApprovalComponent,
    PrimaryServiceContractComponent,
    CreateAgencySiteComponent,
    CreateSiteComponent,
    CreateSiteActionComponent,
    CreateSiteDetailComponent,
    PrimaryServiceContractActionComponent,
    ContactsActionComponent,
     TransferSiteComponent,
     MVVerifyActionComponent,
     VerifyVacancyComponent,

     AddPrimaryServiceContractComponent, ContractInfoPrimaryServiceContractComponent, AdditionalInfoPrimaryServiceContractComponent,
     PlacementAgencyGridActionComponent, PopulationUnitTypeGridActionComponent, PopulationsGridActionComponent,
     PrimaryServiceAgreePopMainGridComponent, PrimaryServiceAgreementConfigurationActionComponent,
     AddCocReferralComponent,
     MoveOutTenantComponent,
     UserAdministrationComponent,
     UserActionComponent,
     UserSiteContactActionComponent,
     AssignSupervisorComponent,
     SiteDetailComponent,
     SiteMoreContactsDialogComponent,
     UserProfileComponent,

     AdminClientSearchComponent,
     CSActionComponent,
     AdminToolsComponent,
     DeleteReferralComponent,
     ExtendDeterminationComponent,
     LiftSignoffComponent,
     MoveReferralComponent,
     UpdateClientComponent,
     AssignTadLiaisonComponent,
     AgencyDetailComponent,
     AdminReferralHoldComponent,
     UpdateHoldActionComponent,
     AdminClientCaseFolderComponent,
     LegacyTadComponent
  ],
  imports: [
    DetClientBannerModule,
    CommonModule,
    RouterModule,
    ContentBannerModule,
    ReferralPackageClientBannerModule,
    V2CAgencySiteBannerModule,
    HousingApplicationSupportingDocumentsModule,
    NavFooterModule,
    PactCalendarModule,
    VcsRoutingModule,
    SharedADLModule,
    DocumentModule,
    FormsModule,
    TextMaskModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    MaterialModule,
    DirectivesModule,
    PipesModule,
    ErrorsModule,
    NgxHmCarouselModule,
    ConfirmDialogModule,
    HomelessHistoryModule,
    RecommendedServicesModule,
    ApplicantPreferencesModule,
    PlacementOutcomeModule,
    SharedUnitDetailsModule,
    AdlReviewModule,
    CaseClientDocumentsModule,
    CaseNotesModule,
    CoordinatedAssessmentSurveyModule,
    PreviousApplicationsDeterminationsModule,
    VcsReferralsPlacementsModule,
    VpsReferralsModule,
    ReferralHoldModule,
    TutorialModule,
    NgxMaskModule.forRoot(),
    AgGridModule.withComponents([
      CustomDateComponent,
      CapActionComponent,
      SSNTooltipComponent,
      CoCIconComponent,
      InfoIconComponent,
      AssignUnitTypeComponent,
      MatchPercentageTooltipComponent,
      C2vSchedulerActionComponent,
      C2vInterviewDatePickerComponent,
      C2vInterviewTimePickerComponent,
      V2cSchedulerActionComponent,
      VLActionComponent,
      RRDraftActionComponent,
      RRTransmittedActionComponent,
      RRPendingActionComponent,
      RRCompletedActionComponent,
      RRInfoIconComponent,
      TRMyTenantsActionComponent,
      TrInfoIconComponent,
      PAVPendingActionComponent,
      PAVVerifiedActionComponent,
      CPHActionComponent,
      VCUInfoIconComponent,
      CSRActionComponent,
      TadActionComponent,
      TadOfflineInfoIconComponent,
      TadMoveOutInfoIconComponent,
      TadDiscrepancyIconComponent,
      TadNoSubmitActionComponent,
      PTadActionComponent,
      VTadActionComponent,
      TadVerificationActionComponent,
      HousingLiasionListComponent,
      AgencySiteMaintenanceActionComponent,
      SiteContactActionComponent,SiteContactRequestActionComponent,PrimaryServiceContractRequestActionComponent,
      UnitDetailsRequestActionComponent,
      CreateSiteActionComponent,
      PrimaryServiceContractActionComponent,
      ContactsActionComponent,
      ReviewedAgencySiteActionComponent,
      RequestSentAgencySiteActionComponent,
      PendingRequestAgencySiteActionComponent,
      PendingApprovalAgencySiteActionComponent,
      CoCActionComponent,
      MVVerifyActionComponent,
      PrimaryServiceAgreementConfigurationActionComponent,
      PlacementAgencyGridActionComponent, PopulationUnitTypeGridActionComponent, PopulationsGridActionComponent,
      UserActionComponent,
      UserSiteContactActionComponent,
      AssignSupervisorComponent,
      CSActionComponent,
      UpdateHoldActionComponent
    ])
  ],
  entryComponents: [
    UnitRosterDialogComponent,
    WithdrawnDialogComponent,
    RrApplicationPackageDialogComponent,
    RequestQueueFileUploadDialogComponent,
    AssignTenantToUnitComponent,
    PlacementVerificationHistoryComponent,
    DeletePlacementDialogComponent,
    SiteMoreContactsDialogComponent,
    C2vInterviewAddressDialogComponent,
    V2cInterviewAddressDialogComponent
  ],
})
export class VcsModule { }


