import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { IVCSAgencyList, IVCSSiteList, IVCSSiteInput, IVCSAgencyInput, IVCSRaHpAgenciesSites, IVCSRaHpAgencyList, IVCSRaHpSiteList } from '../referral-roster/referral-roster-interface.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { TenantRosterService } from '../tenant-roster/tenant-roster.service';
import { ReferralRosterService } from '../referral-roster/referral-roster.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { Subscription, BehaviorSubject } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { IVCSAgencySiteInfoForCalendar } from 'src/app/shared/calendar/pact-calendar-interface.model';
import { SchedularService } from './schedular.service';

@Component({
  selector: 'app-schedular',
  templateUrl: './schedular.component.html',
  styleUrls: ['./schedular.component.scss']
})
export class SchedularComponent implements OnInit, OnDestroy {
  currentUser: AuthData;
  is_CAS = false;
  is_SH_HP = false;
  is_SH_PE = false;

  agencySiteInfo: IVCSAgencySiteInfoForCalendar;
  hpSiteIDforCalendar: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  selectedAgencyID = 0;
  selectedSiteID = 0;
  agencyList: IVCSAgencyList[] = [];
  siteList: IVCSSiteList[] = [];
  onGoClicked = false;

  raHpAgenciesSites: IVCSRaHpAgenciesSites;
  raHpAgencies: IVCSRaHpAgencyList[];
  raHpSites: IVCSRaHpSiteList[];
  savedAgencyID: number;
  savedSiteID: number;

  currentUserSub: Subscription;
  agencyListSub: Subscription;
  siteListSub: Subscription;
  tenantRosterDataSub: Subscription;
  assignTenantToUnitSub: Subscription;
  selectedAgencySub: Subscription;
  selectedSiteSub: Subscription;
  agencySiteBySiteIDSub: Subscription;

  constructor(
    private userService: UserService,
    private tenantRosterService: TenantRosterService,
    private referralRosterService: ReferralRosterService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private schedularService: SchedularService,
    private commonService: CommonService
  ) {
    /* Getting all the RA/HP Agencies and Sites before Page Load */
    this.route.data.subscribe((res) => {
      // console.log('route data: ', res.rahpAgenciesSites);
      if (res.rahpAgenciesSites.raHpSites) {
        this.raHpAgenciesSites = res.rahpAgenciesSites;
        this.raHpAgencies = res.rahpAgenciesSites.raHpAgencies;
        this.raHpSites = res.rahpAgenciesSites.raHpSites;
        console.log('raHpAgenciesSites: ', this.raHpAgenciesSites);
      }
    });
  }

  ngOnInit() {
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
        if (this.currentUser.siteCategoryType.length > 0) {
          /** Checking the User siteCategoryType either PE or HP */
          var shhp = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_HP);
          var shpe = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.SH_PE);
          var cas = this.currentUser.siteCategoryType.filter(d => d.siteCategory == UserSiteType.CAS);

          if (shhp.length > 0) {
            /* HP User */
            this.is_SH_HP = true;
            this.selectedAgencyID = this.currentUser.agencyId;
            /** Get the HPSite List */
            this.getSiteSelected();
          } else {
            /* PE or CAS User */
            if (cas.length > 0) {
              this.is_CAS = true;
            } else if (shpe.length > 0) {
              this.is_SH_PE = true;
            }
            /** Get the HP Agency List */
            if (this.raHpAgencies.length > 0) {
              this.agencyList = this.raHpAgencies;
              // this.selectedAgencyID = this.agencyList[0].agencyID;
              // this.onAgencySelected();
            }
          }
        }
      }
    });
  }

  onAgencySelected() {
    if (this.selectedAgencyID > 0) {
      this.onGoClicked = false;
      // console.log('onAgencySelected');
      this.getSiteSelected();
    }
  }

  getSiteSelected() {
    this.onGoClicked = false;
    this.selectedSiteID = 0;
    if (this.raHpSites.length > 0) {
      this.siteList = this.raHpSites.filter(d => d.agencyID == this.selectedAgencyID);
      // console.log('getSiteSelected2 RA/HP Site');
      // if (this.siteList.length > 0) {
      //   /** Selected Default HP Site */
      //   this.selectedSiteID = this.siteList[0].siteID;
      //     // console.log('getSiteSelected13');
      //     this.onGo();
      // }
    }
  }

  onGo() {
    if (this.selectedAgencyID > 0 && this.selectedSiteID > 0) {
      this.onGoClicked = true;
      /* get the calendar for selected AgencyID and SiteID  */
      this.agencySiteBySiteIDSub = this.schedularService.getAgencySiteBySiteID(this.selectedSiteID).subscribe((a: IVCSAgencySiteInfoForCalendar) => {
        if (a) {
          this.agencySiteInfo = {
            agencyID: a.agencyID,
            agencyNo: a.agencyNo,
            agencyName: a.agencyName,
            siteID: a.siteID,
            siteNo: a.siteNo,
            siteName: a.siteName
          };
          this.hpSiteIDforCalendar.next(this.selectedSiteID);
        } else {
          this.toastr.error('Could not fetch the selected Site, Please try again.');
        }
      })
      // alert('selectedAgencyID: '+this.selectedAgencyID + '    selectedSiteID: ' + this.selectedSiteID);
    } else {
      this.toastr.error('Please select both Agency and Site.');
    }
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.agencyListSub) {
      this.agencyListSub.unsubscribe();
    }
    if (this.siteListSub) {
      this.siteListSub.unsubscribe();
    }
    if (this.tenantRosterDataSub) {
      this.tenantRosterDataSub.unsubscribe();
    }
    if (this.assignTenantToUnitSub) {
      this.assignTenantToUnitSub.unsubscribe();
    }
    if (this.selectedAgencySub) {
      this.selectedAgencySub.unsubscribe();
    }
    if (this.selectedSiteSub) {
      this.selectedSiteSub.unsubscribe();
    }
    if (this.agencySiteBySiteIDSub) {
      this.agencySiteBySiteIDSub.unsubscribe();
    }
  }

}
