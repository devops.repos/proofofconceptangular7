import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SchedularService {
  private apiURL = environment.pactApiUrl + 'VCSReferral';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  getAgencySiteBySiteID(siteId: number) {
    return this.http.post(this.apiURL + '/GetVCSAgencySiteBySiteID', siteId, this.httpOptions);
  }
}
