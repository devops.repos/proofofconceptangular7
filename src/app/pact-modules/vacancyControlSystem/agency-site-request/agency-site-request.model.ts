import { siteRequestDemographic, siteRequestApproval, UnitFeature, RentalSubsidy } from "../agency-site-maintenance/agency-site-model";

export interface AgencySiteRequest {
    Agency : AgencyRequestModel,
    Site: SiteRequestModel //[]

    UserId: number;
    UserName: string;
    UserEmail: string;
  }

  export interface AgencyRequestModel {
    AgencyRequestID: number;
    Name: string;
    AgencyType: string;
    Address: string;
    City: string;
    State: string;
    Zip: string;
    IsRA : number;
    IsHP : number;

    RequestorLastName: string;
    RequestorFirstName: string;
    RequestorTitle: string;
    RequestorPhone: string;
    RequestorEmail: string;

    CreatedBy: number;

    UserId: number;
    UserName: string;

    ContactFirstName: string;
    ContactLastName: string;
    ContactTitle: string;
    ContactEmail: string;
    ContactCellPhone: string;
    ContactOfficePhone: string;
    ContactOfficeExtension: string;
    ContactFax: string;
}

export interface SiteRequestModel {
    SiteRequestID: number;
    Name: string;
    Address: string;
    City: string;
    State: string;
    Zip: string;
    SiteCategoryType : number;
    SiteRequestType : number;
    CreatedBy: number;
    RequestStatusType: number;

    UserId: number;
    UserName: string;
}

export interface AgencySiteRequestReviewModel{
  siteRequestDemographic: siteRequestDemographic;
  siteRequestApproval: siteRequestApproval;
  siteRequestType : number;
}

export class UnitRequest {

  unitRequestID: number;
  siteRequestID: number;
  siteAgreementPopulationRequestID: number;

  agreementPopulationID: number;
  primaryServiceAgreementPopName: string;

  name: string;
  address: string;
  city: string;
  state: string;
  zip: string;

  siteAgreementPopulationSelected: number;
  siteAgreementPopulationID: number;

  isSideDoorAllowedString: string;
  isVacancyMonitorRequiredString: string;

  isSideDoorAllowed: boolean;
  isVacancyMonitorRequired: boolean;
  isVerified ?:boolean;

  unitType: number;
  unitTypeDesc: string;
  unitTypeSelected: number

  unitStatusType: number;
  unitStatusDesc: string;
  unitStatusSelected: number;

  contractingAgencyType: number;
  contractAgencyDesc: string;
  contractAgencySelected: number;

  unitFeatures: UnitFeature[];
  rentalSubsidies: RentalSubsidy[];

  unitFeaturesJson: string;
  unitFeaturesString: string;

  rentalSubsidyJson: string;
  rentalSubsidyString: string;

  isActive: boolean;
  createdBy : number;
  createdDate :string;
  updatedBy : number;
  updatedDate :string;

  userName: string;
}

export interface AgencySiteRejectModel{
  agencyName: string;
  siteName: string;
  requestorEmail : string;
  requestorFirstName : string;
  requestorLastName : string;
  errorMessages : string[];
  userId : number;
  userName : string;
  userEmail : string;
}