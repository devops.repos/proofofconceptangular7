import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { NewAgencySiteService } from './new-agency-site.service';
import { ConfirmDialogService } from  '../../../shared/confirm-dialog/confirm-dialog.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import {AgencySiteRequest, AgencyRequestModel, SiteRequestModel, AgencySiteRejectModel} from './agency-site-request.model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { ToastrService } from 'ngx-toastr';
import { MatRadioChange } from '@angular/material';
import { Router } from '@angular/router';
import { SiteAdminService } from 'src/app/pact-modules/vacancyControlSystem/agency-site-maintenance/site-admin.service';
import { siteDemographic,AgencyData,housingProgram } from 'src/app/pact-modules/vacancyControlSystem/agency-site-maintenance/agency-site-model';
import { MatTabChangeEvent } from '@angular/material';

@Component({
  selector: 'app-agency-site-request',
  templateUrl: './new-agency-site-request.component.html',
  styleUrls: ['./agency-site-request.component.scss']
})
export class NewAgencySiteRequestComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private userService: UserService,
    private commonService: CommonService, private siteAdminservice : SiteAdminService,
    private formBuilder: FormBuilder, private confirmDialogService: ConfirmDialogService,
    private newAgencySiteService : NewAgencySiteService) { }

    readonly validationMssg = {
      firstNameCtrl: {
        'required': 'Please enter Requester First Name.',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 50 characters.'
      },
      lastNameCtrl: {
        'required': 'Please enter Requester Last Name.',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 50 characters.'
      },
      titleCtrl: {
        'required': 'Please enter Requester Title.',
        'pattern': 'Please enter alphabets ending with character \'.\'',
        'maxlength' : 'Max length of text is 30 characters.'
      },
      phoneCtrl: {
        'required': 'Please enter Requester Phone.',
        'pattern': 'Please enter 10 digit number.',
        'maxlength' : 'Max length of text is 10 characters.'
      },
      emailCtrl: {
        'required': 'Please enter Requester Email.',
        'email': 'Invalid Requester Email string pattern.',
        'maxlength' : 'Max length of text is 50 characters.'
      },
      surveyApplicationCtrl: {
        'required': 'Please select Transmit Application Data.'
      },
      referralPlacementCtrl: {
        'required': 'Please select Access Housing Placement Data.'
      },
      // agency controls
      agencyNameCtrl: {
        'required': 'Please enter Name.',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 100 characters.'
      },
      agencyTypeCtrl: {
        'required': 'Please select Agency Type.'
      },
      agencyAddressCtrl: {
        'required': 'Please enter Street.',
        'maxlength' : 'Max length of text is 200 characters.'
      },
      agencyCityCtrl: {
        'required': 'Please enter City.',
        'pattern': 'Please enter alphabets. space character allowed.',
        'maxlength' : 'Max length of text is 50 characters.'
      },
      agencyStateCtrl: {
        'required': 'Please enter State.',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 2 characters.'
      },
      agencyZipCodeCtrl: {
        'required': 'Please enter Zip code.',
        'pattern': 'Please enter 5 digit number.',
        'maxlength' : 'Max length of text is 5 characters.'
      },
      contactFirstCtrl: {
        'required': 'Please enter First Name',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 50 characters.'
      },
      contactLastCtrl: {
        'required': 'Please enter Last Name',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 50 characters.'
      },
      contactTitleCtrl: {
        'required': 'Please enter Contact Title.',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 10 characters.'
      },
      contactEmailCtrl: {
        'required': 'Please enter Contact Email.',
        'email': 'Invalid Contact Email string pattern.',
        'maxlength' : 'Max length of text is 50 characters.'
      },
      contactCellCtrl: {
        'required': 'Please enter Contact Cell.',
        'pattern': 'Please enter 10 digit number.',
        'maxlength' : 'Max length of text is 10 characters.'
      },
      contactFaxCtrl: {
        'required': 'Please enter Contact Fax.',
        'pattern': 'Please enter 10 digit number.',
        'maxlength' : 'Max length of text is 10 characters.'
      },
      contactPhoneCtrl: {
        'required': 'Please enter Contact Phone.',
        'pattern': 'Please enter 10 digit number.',
        'maxlength' : 'Max length of text is 10 characters.'
      },
      contactExtnCtrl: {
        'required': 'Please enter Extension.',
        'pattern': 'Please enter 4 digit number.',
        'maxlength' : 'Max length of text is 4 characters.'
      },
      // site controls
      isHPSiteCtrl: {
        'required': 'Please choose site type.',
      },
      siteNameCtrl: {
        'required': 'Please enter Name.',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 100 characters.'
      },
      siteAddressCtrl: {
        'required': 'Please enter Street.',
        'maxlength': 'Max length of text is 200 characters.'
      },
      siteCityCtrl: {
        'required': 'Please enter City.',
        'pattern': 'Please enter alphabets. space character allowed.',
        'maxlength' : 'Max length of text is 50 characters.'
      },
      siteStateCtrl: {
        'required': 'Please enter State.',
        'pattern': 'Please enter alphabets. No other characters allowed.',
        'maxlength' : 'Max length of text is 2 characters.'
      },
      siteZipCodeCtrl: {
        'required': 'Please enter Zip code.',
        'pattern': 'Please enter 5 digit number.',
        'maxlength' : 'Max length of text is 5 characters.'
      }
    };
  
    tabSelectedIndex: number = 0;
    submitted : boolean = false; 
    sitesFormArray: FormArray;
    requestAgencySiteFormGroup: FormGroup;
    siteRequestTypes: RefGroupDetails[];
    siteCategoryTypes: RefGroupDetails[];
    agencyTypes: housingProgram[];
    yesNoTypes: RefGroupDetails[];
    requestStatusTypes: RefGroupDetails[];
    preSelectedAgencyType: number = 0;
    preSelectedIsHPSite: number = 0;
    preSelectedTransmitData: number = 0;
    preSelectedPlacementData: number = 0;

    isHPSiteCtrlDisabled: boolean = false;
    isHPSiteCtrlSelectedLabel: string = '';
    isHPSiteCtrlValue : number = 0;

    duplicateSiteExists : boolean = true;
    duplicateAgencyExists : boolean = true;

    userData: AuthData;
    userDataSub: Subscription;
    commonServiceSub: Subscription;
    agencySiteServiceSub1: Subscription;
    agencySiteServiceSub2: Subscription;
    agencySiteServiceSub3: Subscription;
    siteAdminServiceDuplicateAgencySub: Subscription;
    siteAdminServiceDuplicateSiteSub: Subscription;

    surveyApplicationModel: string;
    housingReferralPlacementModel: string;
  
    // convenience getter for easy access to form fields
    get f() { return this.requestAgencySiteFormGroup.controls; }

  ngOnInit() {

    this.requestAgencySiteFormGroup = this.formBuilder.group({

      firstNameCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
      lastNameCtrl:['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
      titleCtrl: ['', [Validators.maxLength(25)]], // , Validators.pattern('^[A-Za-z.]+$')
      phoneCtrl:['', [Validators.required, Validators.maxLength(10), Validators.pattern('[0-9]+')]],
      emailCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.email]],
      surveyApplicationCtrl: [this.preSelectedTransmitData, [CustomValidators.radioGroupRequired()]],
      referralPlacementCtrl: [this.preSelectedPlacementData, [CustomValidators.radioGroupRequired()]],

      agencyNameCtrl: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9 ]+')]],
      agencyTypeCtrl: [this.preSelectedAgencyType, [CustomValidators.dropdownRequired()]],
      agencyAddressCtrl:['', [Validators.required, Validators.maxLength(200)]],
      agencyCityCtrl:['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z][a-zA-Z ]*')]],
      agencyStateCtrl:['', [Validators.required, Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
      agencyZipCodeCtrl:['', [Validators.required, Validators.maxLength(5), Validators.pattern('[0-9]+')]],

      contactFirstCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
      contactLastCtrl:['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
      contactTitleCtrl: ['', [Validators.maxLength(25)]], // , Validators.pattern('^[A-Za-z.]+$')
      contactEmailCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.email]],
      contactCellCtrl:['', [Validators.maxLength(10), Validators.pattern('[0-9]+')]],
      contactPhoneCtrl:['', [Validators.required, Validators.maxLength(10), Validators.pattern('[0-9]+')]],
      contactExtnCtrl:['', [Validators.maxLength(4), Validators.pattern('[0-9]+')]],
      contactFaxCtrl:['', [Validators.maxLength(10), Validators.pattern('[0-9]+')]],

      sitesFormArray: this.formBuilder.array([ this.createSite() ])

    });

    this.loadRefGroupDetails();
    this.loadHousingPrograms();

    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

  }

  get allSitesCtrlArry(): FormArray {
    return this.requestAgencySiteFormGroup.get('sitesFormArray') as FormArray;
  }

  siteCtrlsArryAt(index) : any {
    return (<FormArray>this.requestAgencySiteFormGroup.get('sitesFormArray')).at(index);
  }

  createSite(): FormGroup{
    return this.formBuilder.group({
        siteNameCtrl: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('[a-zA-Z0-9 ]+')]],
        siteAddressCtrl: ['', [Validators.required, Validators.maxLength(200)]],
        siteCityCtrl: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('[a-zA-Z][a-zA-Z ]*')]],
        siteStateCtrl: ['', [Validators.required, Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
        siteZipCodeCtrl: ['', [Validators.required, Validators.maxLength(5), Validators.pattern('[0-9]+')]],
        isHPSiteCtrl : [this.preSelectedIsHPSite, []] // NO CustomValidators.radioGroupRequired() as it fails required field validation
    });

  }

  AddNewSite(): void {
    this.sitesFormArray = this.requestAgencySiteFormGroup.get('sitesFormArray') as FormArray;
    this.sitesFormArray.push(this.createSite());

  }

  deleteSite(index){

    const title = 'Confirm Delete';
    const primaryMessage = 'This Site data will be deleted.';
    const secondaryMessage = 'Are you sure you delete this Site ?';
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    if(index != 0){
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
        (positiveResponse) => (<FormArray>this.requestAgencySiteFormGroup.get('sitesFormArray')).removeAt(index),
        (negativeResponse) => console.log(),
        );       
    }

  }

  nextTab() : boolean {
    if(this.requestAgencySiteFormGroup.invalid)
      this.requestAgencySiteFormGroup.updateValueAndValidity();

    if (this.requestAgencySiteFormGroup.get('lastNameCtrl').invalid || this.requestAgencySiteFormGroup.get('firstNameCtrl').invalid 
        || this.requestAgencySiteFormGroup.get('phoneCtrl').invalid || this.requestAgencySiteFormGroup.get('emailCtrl').invalid
        || this.requestAgencySiteFormGroup.get('surveyApplicationCtrl').invalid || this.requestAgencySiteFormGroup.get('referralPlacementCtrl').invalid) {
      
          this.toastr.warning('Please enter valid data in the Requestor Details Tab.', ''); 
          return false; // stop here if form is invalid
    }
    if (this.tabSelectedIndex == 0){
        this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }

    return true;
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    if(tabChangeEvent.index == 1){
      if(this.nextTab() == false)
      this.tabSelectedIndex = this.tabSelectedIndex - 1; // go back
    }

  }

  //Previous button click
  previousTab() {

    if (this.tabSelectedIndex > 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }

  }

  resetForm() {
    this.requestAgencySiteFormGroup.reset();
  }

  onSubmitHousingAgency() {
    let that = this;
   this.submitted = true;
    if (this.requestAgencySiteFormGroup.invalid) {
        this.toastr.warning('Please enter all the required fields.', '');
        return; // stop here if form is invalid
    }
    this.duplicateAgencyExists = true; this.duplicateSiteExists = true;
    setTimeout(() => {
      this.checkDuplicateAgency(null); // AGENCY CHECK
    }, 500);
    setTimeout(() => {
      this.checkDuplicateSite(null); // SITE CHECK
    }, 1000);

    setTimeout(() => {
      
        if(this.duplicateAgencyExists == false && this.duplicateSiteExists == false){

          let saveForm = this.fillAgencyRequestModelFromForm();
          if(saveForm.Site.SiteCategoryType && saveForm.Site.SiteCategoryType > 0){
              this.agencySiteServiceSub2 = this.newAgencySiteService.saveNewAgencySiteRequest(saveForm).subscribe(
                data => {
                  that.showPopupEmailSentOnSubmit('An email has been sent to the requester for initiating new agency/site request process.');
                },
                error => {
                  this.toastr.warning('Error while Saving !!', '');
                }
              );
          } else{
            this.toastr.warning('Please select the Field (Is this a Housing Provider Site)', '');
          }

        }

      }, 2500);

  }

  onSubmitReferralAgency(){
    this.submitted = true;
    // let RequestorLastName = this.requestAgencySiteFormGroup.get('lastNameCtrl').value;
    // let RequestorFirstName = this.requestAgencySiteFormGroup.get('firstNameCtrl').value;
    // //RequestorTitle: this.requestAgencySiteFormGroup.get('titleCtrl').value;
    // let RequestorPhone = this.requestAgencySiteFormGroup.get('phoneCtrl').value;
    // let RequestorEmail = this.requestAgencySiteFormGroup.get('emailCtrl').value;
    let that = this;
    if (this.requestAgencySiteFormGroup.get('lastNameCtrl').valid && this.requestAgencySiteFormGroup.get('firstNameCtrl').valid 
          && this.requestAgencySiteFormGroup.get('phoneCtrl').valid && this.requestAgencySiteFormGroup.get('emailCtrl').valid) {

            let saveForm = this.fillAgencyRequestModelFromForm();
            saveForm.Agency.AgencyType = null;
            this.agencySiteServiceSub1 = this.newAgencySiteService.saveNewAgencySiteRequest(saveForm).subscribe(
              data => {
                that.showPopupEmailSentOnSubmit('An email has been sent to the requester for initiating new agency/site request process.');
              },
              error => {
                that.toastr.warning('Error while Saving !!', '');
              }
            );
      }
  }

  showPopupEmailSentOnSubmit(msg : string) {
      let that = this;
      const title = 'Email Confirmation';
      const primaryMessage = '';
      const secondaryMessage = msg; //`An email has been sent to the user to request a new agency/site.`;
      const confirmButtonName = 'Ok';
      const dismissButtonName = null;

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          (positiveResponse) => {
              that.toastr.success('Successfully Saved', '');
              that.router.navigate(['/dashboard']);
          },
          (negativeResponse) => console.log(),
        );
  }

  loadRefGroupDetails() {
    const yesNoRefGroupId = 7; const siteCategoryRefGroupId = 2; const siteRequestTypeId = 30; const requestStatusTypeId = 70;
    this.commonServiceSub = this.commonService.getRefGroupDetails(yesNoRefGroupId + ',' + siteCategoryRefGroupId 
                              + ',' + siteRequestTypeId+ ',' + requestStatusTypeId).subscribe(
      res => {
          const data = res.body as RefGroupDetails[];
          this.yesNoTypes = data.filter(d => d.refGroupID === yesNoRefGroupId && (d.refGroupDetailDescription.toUpperCase() === 'YES' || d.refGroupDetailDescription.toUpperCase() === 'NO'));
          this.siteCategoryTypes = data.filter(d => (d.refGroupID === siteCategoryRefGroupId));
          this.siteRequestTypes = data.filter(d => (d.refGroupID === siteRequestTypeId));
          this.requestStatusTypes = data.filter(d => (d.refGroupID === requestStatusTypeId));
      },
      error => console.error('Error!', error)
    );
  }

  loadHousingPrograms()
  {
   this.siteAdminservice.getHousingPrograms()
   .subscribe(
     res => {
       this.agencyTypes= res.body as housingProgram[];
     },
     error => console.error('Error!', error)
   );     
  }

  fillAgencyRequestModelFromForm() : AgencySiteRequest {
    let newAgencySiteString = "New Agency/Site";
    let t = this.siteRequestTypes.find(it => it.refGroupDetailDescription.toUpperCase() === newAgencySiteString.toUpperCase());
    let requestTypeId =  t.refGroupDetailID;

      const agencyModel : AgencyRequestModel = {
        AgencyRequestID: 0,
        Name: this.requestAgencySiteFormGroup.get('agencyNameCtrl').value,
        AgencyType: this.requestAgencySiteFormGroup.get('agencyTypeCtrl').value,
        Address: this.requestAgencySiteFormGroup.get('agencyAddressCtrl').value,
        City: this.requestAgencySiteFormGroup.get('agencyCityCtrl').value,
        State: this.requestAgencySiteFormGroup.get('agencyStateCtrl').value,
        Zip: this.requestAgencySiteFormGroup.get('agencyZipCodeCtrl').value,
        IsRA : this.requestAgencySiteFormGroup.get('surveyApplicationCtrl').value,
        IsHP : this.requestAgencySiteFormGroup.get('referralPlacementCtrl').value,

        RequestorLastName: this.requestAgencySiteFormGroup.get('lastNameCtrl').value,
        RequestorFirstName: this.requestAgencySiteFormGroup.get('firstNameCtrl').value,
        RequestorTitle: this.requestAgencySiteFormGroup.get('titleCtrl').value,
        RequestorPhone: this.requestAgencySiteFormGroup.get('phoneCtrl').value,
        RequestorEmail: this.requestAgencySiteFormGroup.get('emailCtrl').value,

        CreatedBy: this.userData.optionUserId,
        UserId: this.userData.optionUserId,
        UserName: this.userData.lanId,

        ContactFirstName: this.requestAgencySiteFormGroup.get('contactFirstCtrl').value,
        ContactLastName: this.requestAgencySiteFormGroup.get('contactLastCtrl').value,
        ContactTitle: this.requestAgencySiteFormGroup.get('contactTitleCtrl').value,
        ContactEmail: this.requestAgencySiteFormGroup.get('contactEmailCtrl').value,
        ContactCellPhone: this.requestAgencySiteFormGroup.get('contactCellCtrl').value,

        ContactOfficePhone: this.requestAgencySiteFormGroup.get('contactPhoneCtrl').value,
        ContactOfficeExtension: this.requestAgencySiteFormGroup.get('contactExtnCtrl').value,
        ContactFax: this.requestAgencySiteFormGroup.get('contactFaxCtrl').value,
      }

      const agencySiteRequestModel : AgencySiteRequest = {
        Agency : agencyModel,
        Site : null, // assigned below
        UserId: this.userData.optionUserId,
        UserName: this.userData.lanId,
        UserEmail: this.userData.email
      }

      for(let i = 0; i < this.allSitesCtrlArry.controls.length; i++) { 
        let formGrpCtrl = this.allSitesCtrlArry.controls[i] as FormGroup;

        const siteModel : SiteRequestModel = {
          SiteRequestID : 0,
          Name : formGrpCtrl.controls['siteNameCtrl'].value,
          Address : formGrpCtrl.controls['siteAddressCtrl'].value,
          City : formGrpCtrl.controls['siteCityCtrl'].value,
          State : formGrpCtrl.controls['siteStateCtrl'].value,
          Zip : formGrpCtrl.controls['siteZipCodeCtrl'].value,
          SiteCategoryType : this.getSiteCategoryTypeValue(),
          SiteRequestType: requestTypeId,
          CreatedBy: this.userData.optionUserId,
          UserId: this.userData.optionUserId,
          UserName: this.userData.lanId,
          RequestStatusType: this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Pending Request').refGroupDetailID 
        }
        // agencySiteRequestModel.Sites.push(siteModel); // mutiple sites
        agencySiteRequestModel.Site = siteModel; // only one site
      }
      
      return agencySiteRequestModel;
  }

  getSiteCategoryTypeValue(): number {
    let yes = this.yesNoTypes.find(it => it.refGroupDetailDescription.toUpperCase() === 'YES');
    let no = this.yesNoTypes.find(it => it.refGroupDetailDescription.toUpperCase() === 'NO');

    let IsRA_agency : number = Number(this.requestAgencySiteFormGroup.get('surveyApplicationCtrl').value);
    let IsHP_agency : number = Number(this.requestAgencySiteFormGroup.get('referralPlacementCtrl').value);
    
    if (this.isHPSiteCtrlValue == yes.refGroupDetailID && IsHP_agency == 33 && IsRA_agency == 33){
      let t = this.siteCategoryTypes.find(it => it.refGroupDetailDescription.toUpperCase() === "SH-HP"); // Housing Provider
      return t.refGroupDetailID;
    }
    else if (this.isHPSiteCtrlValue == yes.refGroupDetailID && IsHP_agency == 33  && IsRA_agency == 34){
      let t = this.siteCategoryTypes.find(it => it.refGroupDetailDescription.toUpperCase() === "SH-HP"); // Housing Provider
      return t.refGroupDetailID;
    }
    else if (this.isHPSiteCtrlValue == no.refGroupDetailID && IsHP_agency == 33 && IsRA_agency == 33){
      let t = this.siteCategoryTypes.find(it => it.refGroupDetailDescription.toUpperCase() === "SH-RA"); // Referring Agency
        return t.refGroupDetailID;
    }
    else if (this.isHPSiteCtrlValue == no.refGroupDetailID && IsHP_agency == 33  && IsRA_agency == 34) {
      let t = this.siteCategoryTypes.find(it => it.refGroupDetailDescription.toUpperCase() === "SH-PE"); // Placement Entity
      return t.refGroupDetailID;
    }
    else if (this.isHPSiteCtrlValue == no.refGroupDetailID && IsHP_agency == 34 && IsRA_agency == 33){
      let t = this.siteCategoryTypes.find(it => it.refGroupDetailDescription.toUpperCase() === "SH-RA"); // Referring Agency
        return t.refGroupDetailID;
    }
  }

  isHPSiteCtrlClick(evnt: MatRadioChange){
    if(this.isHPSiteCtrlDisabled) {
      this.toastr.info('Disabled. Cannot be changed. !!', '');
      return false;
    }
    else return true;
  }

radioChange(evnt: MatRadioChange) {

  this.isHPSiteCtrlSelectedLabel = '';
  //this.enableRadioGroupValidator(); //enable validators

  for(let i = 0; i < this.allSitesCtrlArry.controls.length; i++) { 
    let formGrpCtrl = this.allSitesCtrlArry.controls[i] as FormGroup;
    formGrpCtrl.controls['isHPSiteCtrl'].reset();
  }
 
    if (evnt.source.name === 'surveyApplicationCtrl') {
        if(evnt.value == '33' && this.requestAgencySiteFormGroup.get('referralPlacementCtrl').value == '33'){
          this.isHPSiteCtrlDisabled = false;
        }
        else if(evnt.value == '34' && this.requestAgencySiteFormGroup.get('referralPlacementCtrl').value == '34'){
          this.isHPSiteCtrlDisabled = true; // also disable validator
          //this.disableRadioGroupValidator();
        }
        else {
          this.isHPSiteCtrlDisabled = true;
          if(evnt.value == '33' && this.requestAgencySiteFormGroup.get('referralPlacementCtrl').value == '34'){
            this.isHPSiteCtrlSelectedLabel = "No";
            this.isHPSiteCtrlValue = 34;
          }
          if(evnt.value == '34' && this.requestAgencySiteFormGroup.get('referralPlacementCtrl').value == '33'){
            this.isHPSiteCtrlSelectedLabel = "Yes";
            this.isHPSiteCtrlValue = 33;
          }
        }
    }
    else if (evnt.source.name === 'referralPlacementCtrl') {
      if(evnt.value == '33' && this.requestAgencySiteFormGroup.get('surveyApplicationCtrl').value == '33'){
        this.isHPSiteCtrlDisabled = false;
      }
      else if(evnt.value == '34' && this.requestAgencySiteFormGroup.get('surveyApplicationCtrl').value == '34')
      {
        this.isHPSiteCtrlDisabled = true; // also disable validator
        //this.disableRadioGroupValidator();
      }
      else {
        this.isHPSiteCtrlDisabled = true;
        if(evnt.value == '33' && this.requestAgencySiteFormGroup.get('surveyApplicationCtrl').value == '34'){
          this.isHPSiteCtrlSelectedLabel = "Yes";
          this.isHPSiteCtrlValue = 33;
        }
        if(evnt.value == '34' && this.requestAgencySiteFormGroup.get('surveyApplicationCtrl').value == '33'){
          this.isHPSiteCtrlSelectedLabel = "No";
          this.isHPSiteCtrlValue = 34;
        }
      }
    }

    // console.log('surveyApplicationModel = ' + this.surveyApplicationModel + " , housingReferralPlacementModel = " + this.housingReferralPlacementModel);

}

disableRadioGroupValidator(){
  for(let i = 0; i < this.allSitesCtrlArry.controls.length; i++) { 
    let formGrpCtrl = this.allSitesCtrlArry.controls[i] as FormGroup;
    formGrpCtrl.controls['isHPSiteCtrl'].disable();
  }
}

enableRadioGroupValidator(){
  for(let i = 0; i < this.allSitesCtrlArry.controls.length; i++) { 
    let formGrpCtrl = this.allSitesCtrlArry.controls[i] as FormGroup;
    formGrpCtrl.controls['isHPSiteCtrl'].enable();
  }
}

checkDuplicateSite(event1: any){
    let that = this;
    let formGrpCtrl = this.allSitesCtrlArry.controls[0] as FormGroup;
    let _name : string = formGrpCtrl.controls['siteNameCtrl'].value;
    if(_name === "" || _name.trim().length === 0){
      formGrpCtrl.controls['siteNameCtrl'].setValue("");
      return;
    }

  const  newSite: siteDemographic = {
          siteID:0,
          siteNo:null,
          agencyID:0,
          housingProgram:null,
          name:formGrpCtrl.controls['siteNameCtrl'].value,
          address:formGrpCtrl.controls['siteAddressCtrl'].value,
          city:formGrpCtrl.controls['siteCityCtrl'].value,
          state:formGrpCtrl.controls['siteStateCtrl'].value,
          zip:formGrpCtrl.controls['siteZipCodeCtrl'].value,
          locationType:null,
          referralAvailableType:null,
          tcoReadyType:null,
          tcoContractStartDate:null,
          taxCreditType:null,
          maxIncomeForStudio:null,
          maxIncomeForOneBR:null,
          levelOfCareType:null,
          contractType:null,
          contractStartDate:null,
          siteTrackedType:null,
          tadLiasion:null,
          sameAsSiteInterviewAddressType:null,
          interviewAddress:null,
          interviewCity:null,
          interviewState:null,
          interviewZip:null,
          interviewPhone:null,
          siteFeatures:null,
          siteTypeDesc:null,
          agencyName:null,
          agencyNo:null,
          userId:this.userData.optionUserId,
          siteCategoryType:null,
          boroughType:null
  }
  this.siteAdminServiceDuplicateSiteSub = this.siteAdminservice.checDuplicateSite(newSite)
  .subscribe(
           data => {
             let checksites = data as boolean;
             if (!checksites)
             {
                that.duplicateSiteExists = false;
                that.toastr.success("No Duplicate Site Exists.", '');
             }
             else
             {
                that.duplicateSiteExists = true;
                that.toastr.error('Duplicate Site Exists', '');
             }
           },
           error => {that.toastr.error("checkDuplicateSite", 'Check duplicate Site');}
         );
  }

checkDuplicateAgency(event1: any){
    let that = this;
    let _name : string = this.requestAgencySiteFormGroup.get('agencyNameCtrl').value;
    if(_name === "" || _name.trim().length === 0){
      this.requestAgencySiteFormGroup.get('agencyNameCtrl').setValue("");
      return;
    }
      let agencyinfo: AgencyData={   
            agencyID:0,
            agencyNo:null,
            name:this.requestAgencySiteFormGroup.get('agencyNameCtrl').value,
            agencyType:this.requestAgencySiteFormGroup.get('agencyTypeCtrl').value,
            agencyAddress:this.requestAgencySiteFormGroup.get('agencyAddressCtrl').value,
            city:this.requestAgencySiteFormGroup.get('agencyCityCtrl').value,
            state:this.requestAgencySiteFormGroup.get('agencyStateCtrl').value,
            zip:this.requestAgencySiteFormGroup.get('agencyZipCodeCtrl').value,
            userId: this.userData.optionUserId,
            sites:null,
            userName:this.userData.lanId,
            contactLastName:null,
            contactFirstName:null,
            contactTitle:null,
            phone:null,
            extension:null,
            alternatePhone :null,
            alternateExtension:null,
            fax:null,
            email:null
        }

      this.siteAdminServiceDuplicateAgencySub = this.siteAdminservice.checDuplicateAgency(agencyinfo)
          .subscribe(
                    data => {
                      let checkagency = data as boolean
                      if (!checkagency)
                      {
                        that.duplicateAgencyExists = false;
                        that.toastr.success("No Duplicate Agency Exists.", '');
                      }
                      else
                      {
                        that.duplicateAgencyExists = true;
                        that.toastr.error('Duplicate Agency Exists.', '');
                      }
                    },
                    error => {that.toastr.error("checkDuplicateAgency", 'Duplicate Agency Exists');}
                  );
  }

  onReject(){
    let that = this;
    let formGrpCtrl = this.allSitesCtrlArry.controls[0] as FormGroup;

      if (this.requestAgencySiteFormGroup.get('lastNameCtrl').invalid && this.requestAgencySiteFormGroup.get('firstNameCtrl').invalid 
            && this.requestAgencySiteFormGroup.get('emailCtrl').invalid) {
            
              this.toastr.warning("Please enter Requestor's First Name, Last Name, and Email fields.", "");
            return; // stop here if form is invalid
      }
      if (this.requestAgencySiteFormGroup.get('agencyNameCtrl').invalid && this.requestAgencySiteFormGroup.get('agencyAddressCtrl').invalid 
            && this.requestAgencySiteFormGroup.get('agencyCityCtrl').invalid && this.requestAgencySiteFormGroup.get('agencyStateCtrl').invalid
            && this.requestAgencySiteFormGroup.get('agencyZipCodeCtrl').invalid) {
                
              this.toastr.warning("Please enter Agency Name, Address, City, State, Zip Code.", "");
                return; // stop here if form is invalid
      }
      if (formGrpCtrl.controls['siteNameCtrl'].invalid && formGrpCtrl.controls['siteAddressCtrl'].invalid 
          && formGrpCtrl.controls['siteCityCtrl'].invalid && formGrpCtrl.controls['siteStateCtrl'].invalid
          && formGrpCtrl.controls['siteZipCodeCtrl'].invalid) {
          
          this.toastr.warning("Please enter Site Name, Address, City, State, Zip Code.", "");
          return; // stop here if form is invalid
      }

      setTimeout(() => {
          that.checkDuplicateAgency(null); // AGENCY CHECK
      }, 500);
      setTimeout(() => {
          that.checkDuplicateSite(null); // SITE CHECK
      }, 1000);


      let requestorEmail = this.requestAgencySiteFormGroup.get('emailCtrl').value;

      setTimeout(() => {
              let errorMessages : string[] = [];
              if(that.duplicateAgencyExists)
                  errorMessages.push('Duplicate Agency found at the Address with give name = ' + that.requestAgencySiteFormGroup.get('agencyNameCtrl').value);
              if(that.duplicateSiteExists)
                  errorMessages.push('Duplicate Site found at the Address with give name = ' + formGrpCtrl.controls['siteNameCtrl'].value);
              if(that.duplicateAgencyExists == false && that.duplicateSiteExists == false)
              return; // EITHER TRUE CONTINUE

              if(errorMessages.length > 0){
                    let agencySiteRejectModel : AgencySiteRejectModel = {
                        "agencyName": that.requestAgencySiteFormGroup.get('agencyNameCtrl').value,
                        "siteName": formGrpCtrl.controls['siteNameCtrl'].value,
                        "requestorEmail" : requestorEmail,
                        "requestorFirstName" : that.requestAgencySiteFormGroup.get('firstNameCtrl').value,
                        "requestorLastName" : that.requestAgencySiteFormGroup.get('lastNameCtrl').value,
                        "errorMessages" : errorMessages,
                        "userId" : that.userData.optionUserId,
                        "userName" : that.userData.lanId,
                        "userEmail" : that.userData.email
                    }
                    that.agencySiteServiceSub3 = that.newAgencySiteService.rejectAgencySiteRequest(agencySiteRejectModel).subscribe(
                      res => {  // Email has to be sent out in any case.
                          // if(res && res.result)
                          //     that.toastr.warning(res.result, '');
                          // else
                            that.showPopupEmailSentOnSubmit('An email has been sent to the requester with Agency contact or Site system administrator contact information.');
                      },
                      error => {
                          that.toastr.warning('Error while Saving !!', '');
                      }
                    );
              } else {
                  that.toastr.info("No Duplicate Agency/Site found.", '');
              }
      }, 2000);

  }

  get disableRejectButton() : boolean { 
      if(this.duplicateAgencyExists === false && this.duplicateSiteExists === false)
          return true; // [disabled]
      else 
          return false;
  }

  ngOnDestroy() {
    if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
    if(this.userDataSub) this.userDataSub.unsubscribe();
    if(this.agencySiteServiceSub1) this.agencySiteServiceSub1.unsubscribe();
    if(this.agencySiteServiceSub2) this.agencySiteServiceSub2.unsubscribe();
    if(this.agencySiteServiceSub3) this.agencySiteServiceSub3.unsubscribe();
    if(this.siteAdminServiceDuplicateAgencySub) this.siteAdminServiceDuplicateAgencySub.unsubscribe();
    if(this.siteAdminServiceDuplicateSiteSub) this.siteAdminServiceDuplicateSiteSub.unsubscribe();
  }

}