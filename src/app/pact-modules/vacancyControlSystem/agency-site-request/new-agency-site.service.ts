import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { AgencySiteRequest, AgencySiteRejectModel } from './agency-site-request.model'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewAgencySiteService {
  saveAgencySiteRequestURL = environment.pactApiUrl + 'Agency/CreateAgencyAndSite';
  rejectAgencySiteRequestURL = environment.pactApiUrl + 'Agency/RejectAgencyAndSite';
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }


  saveNewAgencySiteRequest(agencySiteRequestModel : AgencySiteRequest) : Observable<any>
  {
    return this.httpClient.post(this.saveAgencySiteRequestURL, JSON.stringify(agencySiteRequestModel), this.httpOptions);
  }

  rejectAgencySiteRequest(agencySiteRejectModel : AgencySiteRejectModel) : Observable<any>
  {
    return this.httpClient.post(this.rejectAgencySiteRequestURL, JSON.stringify(agencySiteRejectModel), this.httpOptions);
  }

}