

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { CustomDateComponent } from '../custom-date-component.component';

import {  IMonitorVacancy, IMonitorVacancyData} from './monitor-vacancies-model';
import { MonitorVacanciesService } from './monitor-vacancies.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { UserSiteType, ReferralStatusType } from 'src/app/models/pact-enums.enum';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { MVVerifyActionComponent } from './mv-verify-action.component';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-monitor-vacancies',
  templateUrl: './monitor-vacancies.component.html',
  styleUrls: ['./monitor-vacancies.component.scss'],
  providers: [DatePipe]
})

export class MonitorVacanciesComponent implements OnInit, OnDestroy {
  currentUser: AuthData;
  is_SH_PE = false;
  is_SH_HP = false;
  is_CAS = false;


  @ViewChild('pendingAgGrid') pendingAgGrid: AgGridAngular;
  @ViewChild('offlineVerifiedAgGrid') offlineVerifiedAgGrid: AgGridAngular;
  @ViewChild('onlineVerifiedAgGrid') onlineVerifiedAgGrid: AgGridAngular;
  agencyID: number= 0;
  SiteID :number= 0;
  vacancyVerficatioID : number=0;

  pending :number= 0;
  offlineVerified :number = 0;
  onlineVerified:number = 0;
  overdue:number = 0;
  flagClicked = '';


  selectedTab: number=0;

  pendingGridApi;
  pendingGridColumnApi;
  pendingColumnDefs;
  offlineVerifiedGridApi;
  offlineVerifiedGridColumnApi;
  offlineVerifiedColumnDefs;
  onlineVerifiedGridApi;
  onlineVerifiedGridColumnApi;
  onlineVerifiedColumnDefs;

  defaultColDef;
  frameworkComponents;
  context;

  public pendinggridOptions: GridOptions;
  public offlineVerifiedgridOptions: GridOptions;
  public onlineVerifiedgridOptions: GridOptions;
  monitorVacanciesData: IMonitorVacancyData;
  pendingRowData: IMonitorVacancy[] = [];
  offlineVerifiedRowData: IMonitorVacancy[] = [];
  onlineVerifiedRowData: IMonitorVacancy[] = [];


  currentUserSub: Subscription;
  selectedTabSub: Subscription;



  constructor(
    private userService: UserService,
    private monitorVacanciesService: MonitorVacanciesService,
    private toastr: ToastrService,
    private router: Router,
    private datePipe: DatePipe
  ) {
    this.pendinggridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
                {
                    id: 'columns',
                    labelDefault: 'Columns',
                    labelKey: 'columns',
                    iconKey: 'columns',
                    toolPanel: 'agColumnsToolPanel',
                    toolPanelParams: {
                        suppressValues: true,
                        suppressPivots: true,
                        suppressPivotMode: true,
                        suppressRowGroups: false
                    }
                },
                {
                    id: 'filters',
                    labelDefault: 'Filters',
                    labelKey: 'filters',
                    iconKey: 'filter',
                    toolPanel: 'agFiltersToolPanel',
                }
            ],
            defaultToolPanel: ''
        },

      getRowStyle: (params) => {
        if (params.node.data.isOverdue) {
          return {background: '#FFB74D'};
        }
      }
    } as GridOptions;
    this.offlineVerifiedgridOptions = {
      rowHeight: 35,
      getRowStyle: (params) => {
        if (params.node.data.isOverdue) {
          return {background: '#FFB74D'};
        }
      },
      sideBar: {
        toolPanels: [
                {
                    id: 'columns',
                    labelDefault: 'Columns',
                    labelKey: 'columns',
                    iconKey: 'columns',
                    toolPanel: 'agColumnsToolPanel',
                    toolPanelParams: {
                        suppressValues: true,
                        suppressPivots: true,
                        suppressPivotMode: true,
                        suppressRowGroups: false
                    }
                },
                {
                    id: 'filters',
                    labelDefault: 'Filters',
                    labelKey: 'filters',
                    iconKey: 'filter',
                    toolPanel: 'agFiltersToolPanel',
                }
            ],
            defaultToolPanel: ''
        }

    } as GridOptions;
    this.onlineVerifiedgridOptions = {
      rowHeight: 35,
      sideBar: {
        toolPanels: [
                {
                    id: 'columns',
                    labelDefault: 'Columns',
                    labelKey: 'columns',
                    iconKey: 'columns',
                    toolPanel: 'agColumnsToolPanel',
                    toolPanelParams: {
                        suppressValues: true,
                        suppressPivots: true,
                        suppressPivotMode: true,
                        suppressRowGroups: false
                    }
                },
                {
                    id: 'filters',
                    labelDefault: 'Filters',
                    labelKey: 'filters',
                    iconKey: 'filter',
                    toolPanel: 'agFiltersToolPanel',
                }
            ],
            defaultToolPanel: ''
        }

    } as GridOptions;

    this.pendingColumnDefs = [
      {
          headerName: 'Vacancy Verification ID',
         field : "vacancyVerificationID",
         hide : true
        },
        {
        headerName: 'Housing Provider Agency/Site ',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.agencyNo + ' - ' + params.data.siteNo;
        }
      }
      ,
      {
        headerName: 'Housing Provider Agency/Site Name',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.agencyName + '/' + params.data.siteName;
        }
      },
      {
        headerName: 'Site Type',
        field: 'siteType',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Name',
        field: 'unitName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Features',
        field: 'unitFeatures',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Is Contracted',
        filter: 'agTextColumnFilter',
        field: 'iscontracted',

      },
      {
        headerName: 'Primary Service Contract',
        field: 'primaryserviceContarctName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Population',
        field: 'populationName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Rental Subsidies',
        filter: 'agTextColumnFilter',
        field: 'rentalSubsidies'
       },
      {
        headerName: 'Unit Status',
        field: 'preModifiedUnitStatusDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Created Date',
        field: 'createdDate',
        filter: 'agTextColumnFilter'
       //valueGetter(params) {
         // return this.datePipe.transform(params.data.createdDate, 'MM/dd/yyyy');
        //}
      },
     

      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'pendingActionRenderer'
      }
    ];

    this.offlineVerifiedColumnDefs = [
      {
        headerName: 'Vacancy Verification ID',
       filter: 'agTextColumnFilter',
       field : "vacancyVerificationID",
       hide : true
      },
      {
      headerName: 'Housing Provider Agency/Site ',
      filter: 'agTextColumnFilter',
      valueGetter(params) {
        return params.data.agencyNo + ' - ' + params.data.siteNo;
      }
     }
    ,
      {
      headerName: 'Housing Provider Agency/Site Name',
      filter: 'agTextColumnFilter',
      valueGetter(params) {
        return params.data.agencyName + '/' + params.data.siteName;
      }
    },
    {
      headerName: 'Site Type',
      field: 'siteType',
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Unit Name',
      field: 'unitName',
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Unit Type',
      field: 'unitTypeDescription',
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Unit Features',
      field: 'unitFeatures',
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Is Contracted',
      filter: 'agTextColumnFilter',
      field: 'iscontracted',

    },
    {
      headerName: 'Primary Service Contract',
      field: 'primaryserviceContarctName',
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Population',
      field: 'populationName',
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Rental Subsidies',
      filter: 'agTextColumnFilter',
      field: 'rentalSubsidies'
     },

    {
      headerName: 'Offline Reason',
      filter: 'agTextColumnFilter',
      field: 'offlineReasonTypeDescription'
     },

     {
      headerName: 'Expected Available Date',
     field: 'expectedAvailableDateString',
      filter: 'agTextColumnFilter'


    },
    {
      headerName: 'Unit Status',
      field: 'modifiedUnitStatusDescription',
      filter: 'agTextColumnFilter'
    },
     {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'offlineVerifiedActionRenderer'
      }
    ];

    this.onlineVerifiedColumnDefs = [
      {
        headerName: 'Housing Provider Agency/Site ',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.agencyNo + ' - ' + params.data.siteNo;
        }
      },
      {
        headerName: 'Housing Provider Agency/Site Name',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.agencyName + '/' + params.data.siteName;
        }
      },
      {
        headerName: 'Site Type',
        field: 'siteType',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Name',
        field: 'unitName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitTypeDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Features',
        field: 'unitFeatures',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Is Contracted',
        filter: 'agTextColumnFilter',
        field: 'iscontracted',

      },
      {
        headerName: 'Primary Service Contract',
        field: 'primaryserviceContarctName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Population',
        field: 'populationName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Rental Subsidies',
        filter: 'agTextColumnFilter',
        field: 'rentalSubsidies'
       },
       /*{
        headerName: 'Offline Reason',
        filter: 'agTextColumnFilter',
        field: 'offlineReasonTypeDescription'
       },


       {
        headerName: 'Expected Available Date',
        field: 'expectedAvailableDateString',
        filter: 'agTextColumnFilter'
        },*/

        {
          headerName: 'Verified by',
          field: 'verifiedByUserName',
          filter: 'agTextColumnFilter'
          },
        {
          headerName: 'Verified Date',
          field: 'verifiedDate',
          filter: 'agTextColumnFilter'
          },
      {
        headerName: 'Unit Status',
        field: 'modifiedUnitStatusDescription',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'onlineVerifiedActionRenderer'
      }
    ];



    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
      agDateInput: CustomDateComponent,
      pendingActionRenderer: MVVerifyActionComponent,
      offlineVerifiedActionRenderer: MVVerifyActionComponent,
      onlineVerifiedActionRenderer: MVVerifyActionComponent
    };
  }

  ngOnInit() {
    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
        this.agencyID = userdata.agencyId;
        /** Checking the User siteCategoryType either PE or HP */
        this.currentUser.siteCategoryType.forEach(ct => {
            if (ct.siteCategory === UserSiteType.SH_PE) {
              this.is_SH_PE = true;
              }
            else if (ct.siteCategory === UserSiteType.SH_HP) {
              this.is_SH_HP = true;
            } else if (ct.siteCategory === UserSiteType.CAS) {
              this.is_CAS = true;
            }
         });
          setTimeout(() => {
            if (this.agencyID > 0 ) {
              this.onGo();
            }
          }, 1000);

      }
    });


  }


  onGo() {
    if (this.agencyID > 0 ) {
      this.monitorVacanciesService.getMonitorVacancytabSelected().subscribe(res => {
        if (res) {
         this.selectedTab = res;
        }
        else{
        this.selectedTab = 0;
        }
       // this.vacancyVerificationID= 12;*/
      this.monitorVacanciesService.getVacancyMonitorList(this.agencyID.toString(),"0",this.selectedTab.toString())
      .subscribe
      ( res => {
        this.monitorVacanciesData = res.body as IMonitorVacancyData;
        this.pending = this.monitorVacanciesData.pending;
        this.overdue = this.monitorVacanciesData.overdue;
        this.offlineVerified = this.monitorVacanciesData.offlineVerified;
        this.onlineVerified=this.monitorVacanciesData.onlineVerified;
          if (this.selectedTab === 0) {
            this.pendingRowData = this.monitorVacanciesData.vacancyVerficationList;
          } else if (this.selectedTab === 1 ) {
            this.offlineVerifiedRowData = this.monitorVacanciesData.vacancyVerficationList;
          } else if (this.selectedTab === 2) {
            this.onlineVerifiedRowData = this.monitorVacanciesData.vacancyVerficationList;
          }
          console.log(this.monitorVacanciesData);
          console.log( this.pendingRowData );

         });
    });
  }
}

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
   this.selectedTab = tabChangeEvent.index;
    // alert('selectedTabIndex: ' + this.selectedTab);
    if (this.agencyID >0 ) {
    this.monitorVacanciesService.getVacancyMonitorList(this.agencyID.toString(),this.selectedTab.toString(),"0")
    .subscribe
    ( res => {
      this.monitorVacanciesData = res.body as IMonitorVacancyData;
      this.pending = this.monitorVacanciesData.pending;
      this.overdue = this.monitorVacanciesData.overdue;
      this.offlineVerified = this.monitorVacanciesData.offlineVerified;
      this.onlineVerified=this.monitorVacanciesData.onlineVerified;
        if (this.selectedTab === 0) {
          this.pendingRowData = this.monitorVacanciesData.vacancyVerficationList;
        } else if (this.selectedTab === 1 ) {
          this.offlineVerifiedRowData = this.monitorVacanciesData.vacancyVerficationList;
        } else if (this.selectedTab === 2) {
          this.onlineVerifiedRowData = this.monitorVacanciesData.vacancyVerficationList;
        }
        console.log(this.monitorVacanciesData);
        console.log(this.monitorVacanciesData.vacancyVerficationList);
        console.log( this.pendingRowData );

       });

    }
  }


  onPendingGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.pendingGridApi = params.api;
    this.pendingGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.pendingGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
          allColumnIds.push(column.colId);
        }
    });
    this.pendingGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
  }
  onOfflineVerifiedGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.offlineVerifiedGridApi = params.api;
    this.offlineVerifiedGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.offlineVerifiedGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);

      }
    });
    this.offlineVerifiedGridColumnApi.autoSizeColumns(allColumnIds);
    // params.api.expandAll();
  }
  onOnlineVerificationGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.onlineVerifiedGridApi = params.api;
    this.onlineVerifiedGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.onlineVerifiedGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
          allColumnIds.push(column.colId);
        }
    });
    this.onlineVerifiedGridColumnApi.autoSizeColumns(allColumnIds);
  }






  refreshAgGrid(val: string) {
    this.flagClicked = '';
    if (val === 'p' || val === 'isPending' ) {
      this.pendinggridOptions.api.setFilterModel(null);
    } else if (val === 'f') {
      this.offlineVerifiedgridOptions.api.setFilterModel(null);
    } else if (val === 'o') {
      this.onlineVerifiedgridOptions.api.setFilterModel(null);
    }
    /*else if (val === 'isOverdue') {
    this.pendinggridOptions.api.setFilterModel(null);
    const instance = this.pendingGridApi.getFilterInstance(val);
    instance.selectNothing(val);
      instance.selectValue('1');
    instance.applyModel();
    this.pendingGridApi.onFilterChanged();
  }
  
  else if (val === 'isOffline') {
    this.pendinggridOptions.api.setFilterModel(null);
    const instance = this.pendingGridApi.getFilterInstance("preModifiedUnitStatusDescription");
    instance.selectNothing(val);
      instance.selectValue('Offline');
    instance.applyModel();
    this.pendingGridApi.onFilterChanged();
  }
  else if (val === 'isOnline') {
    this.pendinggridOptions.api.setFilterModel(null);
    const instance = this.pendingGridApi.getFilterInstance("preModifiedUnitStatusDescription");
    instance.selectNothing(val);
      instance.selectValue('Online');
    instance.applyModel();
    this.pendingGridApi.onFilterChanged();
  }*/
}

  onBtExport(val: string) {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: (val === 'p' ?  'pendingMonitorVacanciesReport-' : val === 'f' ? 'offlineVerifiedMonitorVacanciesReport-' : val === 'o' ? 'onlineVerifiedMonitorVacanciesReport-' : 'onlineVerifiedMonitorVacanciesReport')  + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    if (val === 'p') {
      this.pendingGridApi.exportDataAsExcel(params);
    } else if (val === 'f') {
      this.offlineVerifiedGridApi.exportDataAsExcel(params);
    } else if (val === 'o') {
      this.onlineVerifiedGridApi.exportDataAsExcel(params);
    }
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.selectedTabSub) {
      this.selectedTabSub.unsubscribe();
    }

  /*  if (this.referralRosterDataSub) {
      this.referralRosterDataSub.unsubscribe();
    }
    if (this.saveVCSReferralSub) {
      this.saveVCSReferralSub.unsubscribe();
    }*/
  }
  onVerifyVacancyClickParent(mvSelected) {
    //this.monitorVacanciesService.setMonitorVacancySelected(mvSelected.vacancyVerificationID);
   
    this.monitorVacanciesService.setMonitorVacancytabSelected(this.selectedTab);
    this.router.navigate(['./vcs/monitor-vacancies/verify-vacancy', mvSelected.vacancyVerificationID]);

  }
  selectCounterFilter(value: string) {
    if (value === 'isPending') {
      this.flagClicked = 'Pending';
      this.pendingRowData = this.monitorVacanciesData.vacancyVerficationList;
    } else if (value === 'isOverdue') {
      this.flagClicked = 'Overdue for Review';
      this.pendingRowData = this.monitorVacanciesData.vacancyVerficationList.filter(x => x.isOverdue === true);
    } else if (value === 'isOffline') {
      this.flagClicked = 'Offline Verification';
      this.pendingRowData = this.monitorVacanciesData.vacancyVerficationList.filter(x => x.preModifiedUnitStatus === 473);
    } else if (value === 'isOnline') {
      this.flagClicked = 'Online Verification';
      this.pendingRowData = this.monitorVacanciesData.vacancyVerficationList.filter(x => x.preModifiedUnitStatus === 472);
    }

    
   // this.refreshAgGrid(value);
  }
}

