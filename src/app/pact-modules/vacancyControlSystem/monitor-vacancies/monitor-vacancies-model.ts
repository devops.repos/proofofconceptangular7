




export interface IMonitorVacancyData
{
    pending: number;
    offlineVerified: number;
    onlineVerified: number;
    overdue: number;
    vacancyVerficationList: IMonitorVacancy[];
  //  vvHistoryList:IVVerificationHistory[]
}

export interface IVVerificationHistory
{
    addlComment: string;
    verifiedby: string;
    verifiedDate: string;

}


export interface IMonitorVacancy
{
    vacancyVerificationID:number;
    unitID :number;
    preModifiedUnitStatus ?:number;
    preModifiedUnitStatusDescription :string;
    modifiedUnitStatus ?:number;
    modifiedUnitStatusDescription :string;
    vcuadminVerificationStatus ?:number;
    vcuadminVerificationStatusDescription :string;
    addlComment :string;
    offlineReasonType ?:number;
    offlineReasonTypeDescription:string;
    offlineOtherSpecify :string;
    expectedAvailableDate ?:Date;
    expectedAvailableDateString :string;
    verifiedBy ?:number;
    verifiedByUserName :string;
    verifiedDate :string;
    isActive ?:boolean;
    createdBy ?:number;
    createdDate :string;
    updatedBy ?:number;
    updatedDate :string;
    agencyName :string;
    agencyNo :string;
    agencyID ?:number;
    siteNo :string;
    siteName :string;
    siteID:number;
    primaryserviceContarctName :string;
    agreementPopulationID ?:number;
    populationName :string;
    siteType :string;
    contractType :string;
    siteAgreementPopulationID ?:number;
    unitName :string;
    unitType :number;
    unitTypeDescription :string;
    isVacancyMonitorRequired ?:boolean;
    unitStatusType:number;
    unitStatusTypeDescription:string;
    unitFeatures :string;
    rentalSubsidies :string;
    siteAddress:string;
    unitAddress:string;
     unitfeaturesIds :string;
     sameAsSiteInterviewAddressType ?:number;
     interviewAddress :string;
     interviewCity :string;
     interviewState :string;
     interviewZip :string;
     interviewPhone :string;
     isOverdue ?:boolean;
     iscontracted:string;
}


export interface verifyVacancyDetail
{
    vacancyVerificationID:number;
    unitID :number;
    preModifiedUnitStatus ?:number;
    modifiedUnitStatus ?:number;
    vcuadminVerificationStatus ?:number;
    addlComment :string;
    offlineReasonType ?:number;
    offlineOtherSpecify :string;
    expectedAvailableDate ?:Date;
    userId ?:number;
    siteID:number;
    unitType :number;
    unitStatusType:number;
    unitfeaturesIds :string;
     sameAsSiteInterviewAddressType ?:number;
     interviewAddress :string;
     interviewCity :string;
     interviewState :string;
     interviewZip :string;
     interviewPhone :string;
}


export interface UnitOfflineReason
{
    unitOfflineReasonID :number;
    unitOfflineResonDescription :string;
    timeFrame ?:number;
  
}




