import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IMonitorVacancy } from './monitor-vacancies-model';

import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MonitorVacanciesService } from './monitor-vacancies.service';

import { Subscription } from 'rxjs';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-mv-verify-action',
  template: `
    <mat-icon
      class="mv-verify-icon"
      color="warn"
      [matMenuTriggerFor]="mvVerifyAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #mvVerifyAction="matMenu">
      <button mat-menu-item (click)="onVerifyVacancyClick()"  >
        <mat-icon color="primary">assignment</mat-icon>Verify Vacancy
      </button>
     
    </mat-menu>
  `,
  styles: [
    `
      .mv-verify-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class MVVerifyActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  mvSelected: IMonitorVacancy;

 
  constructor(
    private mvService: MonitorVacanciesService,
    private router: Router,
    private dialog: MatDialog,
  ) {}

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

 
  
  onVerifyVacancyClick() {
    this.mvSelected = this.params.data;
    // alert(JSON.stringify(this.capSelected));
      // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
    this.params.context.componentParent.onVerifyVacancyClickParent(this.mvSelected);
  }

  
  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
  }
}
