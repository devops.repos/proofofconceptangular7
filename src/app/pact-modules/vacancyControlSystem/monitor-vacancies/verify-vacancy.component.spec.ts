import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyVacancyComponent } from './verify-vacancy.component';

describe('VerifyVacancyComponent', () => {
  let component: VerifyVacancyComponent;
  let fixture: ComponentFixture<VerifyVacancyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyVacancyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyVacancyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
