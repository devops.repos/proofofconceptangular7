import { Component, OnInit } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';

import { ActivatedRoute, Router } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, AbstractControl, FormControl, FormArray } from '@angular/forms';
import { CommonService } from '../../../services/helper-services/common.service';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { MonitorVacanciesService } from './monitor-vacancies.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ToastrService } from 'ngx-toastr';
import {  IMonitorVacancy,IMonitorVacancyData,IVVerificationHistory,verifyVacancyDetail,UnitOfflineReason} from './monitor-vacancies-model';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import { RefGroupDetails } from '../../../models/refGroupDetailsDropDown.model';
import { AuthData } from 'src/app/models/auth-data.model';

import {
  UserAgencyType,
  UserRole,
  UserSiteType
} from '../../../models/pact-enums.enum';


@Component({
  selector: 'app-verify-vacancy',
  templateUrl: './verify-vacancy.component.html',
  styleUrls: ['./verify-vacancy.component.scss']
})
export class VerifyVacancyComponent implements OnInit {
  currentUser: AuthData;
  is_SH_PE = false;
  is_SH_HP = false;
  vefiyVacancyData : IMonitorVacancyData;
  verifyVacanyUnitdata : IMonitorVacancy;
   vacancyVerificationID :number=0;
   interviewLocations: RefGroupDetails[];
   unitFeatures: RefGroupDetails[] = [];
   unitTypes: RefGroupDetails[] = [];
   unitStatuses: RefGroupDetails[] = [];
   unitStatusesfiltered:RefGroupDetails[] = [];
   unitOfflineReasons:UnitOfflineReason[]=[];
   unitVerificationStatuses:RefGroupDetails[] = [];
   VerifyVacancyform: FormGroup;
   unitFeatureData:Array<number>;
   unitverifiedData:verifyVacancyDetail;
   currentUserSub: Subscription;
   activatedRouteSub: Subscription;
   savedisabled :boolean = false;
   varificationexistingstaus :number =0;

 
   validationMessages = {
       unitExpectedAvailableDateCtrl: {
        'required': 'Please enter the expected available date .',
        'dateValidator':'Contract date is invalid'
      },
    interviewLocationctrl: {
      'required': 'Please select interview loaction.',
     
    },

    interviewAddressCtrl: {
      'required': 'Please enter interview Address',
      'maxlength': 'Max length of text is 200 characters.'
    },
    interviewCityCtrl: {
      'required': 'Please enter interview city.',
      'pattern': 'Please enter alphabets. No other characters allowed.',
      'maxlength' : 'Max length of text is 50 characters.'
    },
    interviewStateCtrl: {
      'required': 'Please enter interview state. ',
      'pattern': 'Please enter alphabets. No other characters allowed.',
      'maxlength' : 'Max length of text is 2 characters.'
    },
    interviewZipCtrl: {
      'required': 'Please enter interview Zip code.',
      'pattern': 'Please enter 5 digit number.',
      'maxlength' : 'Max length of text is 5 characters.'
    },

    interviewPhoneCtrl:{
      'required': 'Please enter interview phone number.',
        'pattern': 'Please enter 10 digit number.',
      'maxlength' : 'Max length of text is 10 characters.',
      'Mask error': 'Invalid Phone number. Please enter date 10 digit phone number',
    },
    unitTypeCtrl: {
      'required': 'Please select unit Type',
     
    },
    unitStatusCtrl: {
      'required': 'Please select unit status',
    },
    unitOfflineReasonCtrl: {
      'required': 'Please select unit offline reason ',
    },
    unitVerificationStatusCtrl:{
      'required': 'Please select unit verification status',
    }
    



  };
  
   formErrors = {
    
    interviewLocationctrl:'',
    interviewAddressCtrl:'',
    interviewCityCtrl:'',
    interviewStateCtrl:'',
    interviewZipCtrl:'',
    interviewPhoneCtrl:'',
    unitExpectedAvailableDateCtrl:'',
    unitOfflineReasonCtrl:'',
    unitTypeCtrl:'',
    unitStatusCtrl:'',
    unitVerificationStatusCtrl:''
  };
  historyGridApi;
  historyGridColumnApi;
  historyColumnDefs;
  
  
  defaultColDef;
  frameworkComponents;
  context;

  public historygridOptions: GridOptions;
  historyRowData: IVVerificationHistory[] = [];
  historydataAvailable : boolean = false;
   
  constructor(
    private sidenavService: NavService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private monitorVacanciesService : MonitorVacanciesService,
    private commonService : CommonService,
    private userService :UserService,
    private confirmDialogService:ConfirmDialogService,
    private toastr: ToastrService,
    private siteAdminService: SiteAdminService
   
  ) {
    
    this.verifyVacanyUnitdata ={} as IMonitorVacancy;

    this.historygridOptions = {
      rowHeight: 35
    } as GridOptions;
   
    this.historyColumnDefs = [
      
        {
        headerName: 'Additional comments ',
        field: 'addlComment',
        filter: 'agTextColumnFilter'
      }
      ,
      {
        headerName: 'Verified By ',
        field: 'verifiedby',
        filter: 'agTextColumnFilter'
      }
      ,
      {
        headerName: 'Verified Date ',
        field: 'verifiedDate',
        filter: 'agTextColumnFilter'
      }
      
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true
      //filter: true
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
     
    };
    
  
  }

  onHistoryGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.historyGridApi = params.api;
    this.historyGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.historyGridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
          allColumnIds.push(column.colId);
        }
    });
    this.historyGridColumnApi.autoSizeColumns(allColumnIds);
    this.historyGridApi.sizeColumnsToFit();
    // params.api.expandAll();
  }
  
  ngOnInit() {
      /* unitFeaturesCtrl: ['', []],
      unitFeaturesListCtrl: ['', []],*/

      this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
        if (userdata) {
          this.currentUser = userdata;
         
        }
      });

      this.activatedRouteSub = this.route.paramMap.subscribe(params => {
        const selectedvacancyVerificationID = params.get('vacancyVerificationID');
        if (selectedvacancyVerificationID === null) {
          return;
        } else {
          const vvid = parseInt(selectedvacancyVerificationID);
          if (isNaN(vvid)) {
            throw new Error('Invalid Verification ID!!');
          } else if (vvid > 0 && vvid < 2147483647) {
            this.vacancyVerificationID = vvid;
          }
        }
      });
     
      //alert(this.vacancyVerificationID);
     
   
     this.VerifyVacancyform = this.formBuilder.group({
        unitTypeCtrl: ['', [Validators.required]],
        unitStatusCtrl: ['', [Validators.required]],
        interviewLocationctrl:['',[]],
        unitFeaturesCtrl: ['', []],
        interviewAddressCtrl:['', [Validators.required,Validators.maxLength(200)]],
        interviewStateCtrl:['', [Validators.required,Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
        interviewCityCtrl:['', [Validators.required,Validators.maxLength(50), Validators.pattern('[a-zA-Z0-9 ]+')]],
        interviewZipCtrl:['', [ Validators.required,Validators.maxLength(5), Validators.pattern('[0-9]+')]],
        interviewPhoneCtrl:['',[ Validators.maxLength(10), Validators.pattern('[0-9]+')]],
        unitVerificationStatusCtrl: ['',[]],
        unitOfflineReasonCtrl:['',[Validators.required]],
        unitExpectedAvailableDateCtrl: ['', [Validators.required]],
        unitaddlComment: ['', []]
      });

      this.VerifyVacancyform.valueChanges.subscribe(data => {
        if (this.VerifyVacancyform.touched || this.VerifyVacancyform.dirty) {
        }
      });

   // this.loadRefGroupDetails();
    this.loadUnitOfflineReason();
    this.populateVacanyInforamtion();
      
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
  }

  loadRefGroupDetails( ){
var value = "7,14,52,29,78";


  this.commonService.getRefGroupDetails(value)
            .subscribe(
              res => {
                const data = res.body as RefGroupDetails[];
                this.interviewLocations = data.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
                this.unitFeatures = data.filter(d => {return (d.refGroupID === 14)}) ;
                //this.unitTypes = data.filter(d => {return (d.refGroupID === 29)}) ;
                this.unitStatuses = data.filter(d => {return (d.refGroupID === 52)}) ;
                this.unitVerificationStatuses=  data.filter(d => {return (d.refGroupID === 78)}) ;
               
                this.varificationexistingstaus = this.verifyVacanyUnitdata.vcuadminVerificationStatus;
                if (this.verifyVacanyUnitdata.vcuadminVerificationStatus === 607)  
                    {
                      this.unitStatusesfiltered = this.unitStatuses.filter(d => {return (d.refGroupDetailID === 472 || d.refGroupDetailID === 473 )}) ;
                }
                else{
                       this.unitStatusesfiltered = this.unitStatuses.filter(d => {return (d.refGroupDetailID === 472 || d.refGroupDetailID === 473 )}) ;
                }
                if (this.verifyVacanyUnitdata.vcuadminVerificationStatus === 607)  {
                  //this.verifyVacanyUnitdata.modifiedUnitStatus = 472;
                 // this.verifyVacanyUnitdata.modifiedUnitStatus= this.verifyVacanyUnitdata.preModifiedUnitStatus;
                }
                else if (this.verifyVacanyUnitdata.vcuadminVerificationStatus === 606 || this.verifyVacanyUnitdata.vcuadminVerificationStatus === null) {
                   this.verifyVacanyUnitdata.modifiedUnitStatus= this.verifyVacanyUnitdata.preModifiedUnitStatus;
                
                  }
                  else if (this.verifyVacanyUnitdata.vcuadminVerificationStatus === 608 ) {
                    this.unitVerificationStatuses=  data.filter(d => {return (d.refGroupID === 78 && (d.refGroupDetailID === 606 || d.refGroupDetailID === 608) )}) ;
                   }
            
              },
              error => console.error('Error!', error)
            );

   }




   loadUnitTypes()
   {
     if (this.verifyVacanyUnitdata != null) {
       var ctype ="0";
       if (this.verifyVacanyUnitdata.iscontracted==='Yes'){
        ctype="488";
       }
        this.siteAdminService.getAgreementPopulationUnitTypes(this.verifyVacanyUnitdata.siteAgreementPopulationID.toString(),ctype)
    
        .subscribe(
      res => {
        this.unitTypes = res.body as RefGroupDetails[];
        var itm = this.unitTypes.find(x => x.refGroupDetailID === this.verifyVacanyUnitdata.unitType) as RefGroupDetails;
        if (!itm)
        {
          this.verifyVacanyUnitdata.unitType= null;
        }
        },
        error => console.error('Error!', error)
    );
    }
   }

   

   loadUnitFeatures()
   {
     if (this.verifyVacanyUnitdata.siteID != null) {
    this.siteAdminService.getSiteFeatures(this.verifyVacanyUnitdata.siteID.toString())
    .subscribe(
      res => {
        this.unitFeatures = res.body as RefGroupDetails[];
      },
      error => console.error('Error!', error)
    );

     }
   }

   loadUnitOfflineReason()
   {
    this.monitorVacanciesService.getUnitOfflineReason()
    .subscribe(
      res => {
        const data = res.body  as UnitOfflineReason[];
        this.unitOfflineReasons = data.filter(d => {return (d.unitOfflineReasonID >= 1 && d.unitOfflineReasonID <= 6 )}) ;
        },
        error => console.error('Error!', error) 
    );
    
   }



  populateVacanyInforamtion()
  {
 /* this.monitorVacanciesService.getMonitorVacancySelected().subscribe(res => {
     if (res) {
     this.vacancyVerificationID = res;
    // this.vacancyVerificationID= 12;*/
     if (this.vacancyVerificationID > 0)  {
     this.monitorVacanciesService.getVacancyMonitorList('0','0',this.vacancyVerificationID.toString())
     .subscribe
     ( res1 => {
      this.vefiyVacancyData= res1.body as IMonitorVacancyData;
      this.verifyVacanyUnitdata = this.vefiyVacancyData.vacancyVerficationList[0] ;
      //this.verifyVacanyHistoryData= this.vefiyVacancyData.vvHistoryList;
     // this.loadUnitFeatures();
      this.loadUnitTypes();
      this.loadRefGroupDetails();
    
     if   (this.verifyVacanyUnitdata.vcuadminVerificationStatus === 608 )
     {
       //alert(this.currentUser.roleId);
        if (this.currentUser.roleId === UserRole.SYS_ADMIN )
        {
          this.savedisabled = false;
          this.VerifyVacancyform.get('unitTypeCtrl').disable();
          this.VerifyVacancyform.get('unitStatusCtrl').disable();
          this.VerifyVacancyform.get('interviewLocationctrl').disable();
          this.VerifyVacancyform.get('unitFeaturesCtrl').disable();
          this.VerifyVacancyform.get('interviewAddressCtrl').disable();
          this.VerifyVacancyform.get('interviewStateCtrl').disable();
          this.VerifyVacancyform.get('interviewCityCtrl').disable();
          this.VerifyVacancyform.get('interviewZipCtrl').disable();
          this.VerifyVacancyform.get('interviewPhoneCtrl').disable();
           this.VerifyVacancyform.get('unitOfflineReasonCtrl').disable();
          this.VerifyVacancyform.get('unitVerificationStatusCtrl').enable();
          this.VerifyVacancyform.get('unitaddlComment').enable();
          this.VerifyVacancyform.get('unitExpectedAvailableDateCtrl').disable();
        }
          else
          {
          this.savedisabled = true;
          this.VerifyVacancyform.get('unitTypeCtrl').disable();
          this.VerifyVacancyform.get('unitStatusCtrl').disable();
          this.VerifyVacancyform.get('interviewLocationctrl').disable();
          this.VerifyVacancyform.get('unitFeaturesCtrl').disable();
          this.VerifyVacancyform.get('interviewStateCtrl').disable();
          this.VerifyVacancyform.get('interviewCityCtrl').disable();
          this.VerifyVacancyform.get('interviewZipCtrl').disable();
          this.VerifyVacancyform.get('interviewPhoneCtrl').disable();
           this.VerifyVacancyform.get('unitOfflineReasonCtrl').disable();
          this.VerifyVacancyform.get('unitVerificationStatusCtrl').disable();
          this.VerifyVacancyform.get('unitExpectedAvailableDateCtrl').disable();
          this.VerifyVacancyform.get('unitaddlComment').disable();
          this.VerifyVacancyform.get('interviewAddressCtrl').disable();
          }
     } 
      /*alert (this.savedisabled);
      alert(this.verifyVacanyUnitdata.vcuadminVerificationStatus);*/

      this.unitFeatureData = this.convertStringToArray(this.verifyVacanyUnitdata.unitfeaturesIds);
     // alert(this.unitFeatureData );
      //alert(this.verifyVacanyUnitdata.unitfeaturesIds);
     this.loadHistoryDetail()

      
     });
    }
  }
/* });
 }*/

 loadHistoryDetail()
 {
  this.monitorVacanciesService.getVacancyVeriificationHist(this.verifyVacanyUnitdata.unitID.toString())
  .subscribe(
    res => {
      this.historyRowData = res.body as IVVerificationHistory[];
      if (this.historyRowData.length >0 ) {
        this.historydataAvailable= true;
      }
      },
      error => console.error('Error!', error)
  );
  
 }

 SaveVV()
 {

  this.savedisabled= true;
 this.unitverifiedData={    
    vacancyVerificationID:this.verifyVacanyUnitdata.vacancyVerificationID,
  unitID :this.verifyVacanyUnitdata.unitID,
  preModifiedUnitStatus :this.verifyVacanyUnitdata.preModifiedUnitStatus,
  modifiedUnitStatus :this.verifyVacanyUnitdata.modifiedUnitStatus,
  vcuadminVerificationStatus :this.verifyVacanyUnitdata.vcuadminVerificationStatus,
  addlComment :this.verifyVacanyUnitdata.addlComment,
  offlineReasonType :this.verifyVacanyUnitdata.offlineReasonType,
  offlineOtherSpecify :this.verifyVacanyUnitdata.offlineOtherSpecify,
  expectedAvailableDate :this.verifyVacanyUnitdata.expectedAvailableDate,
  userId :this.currentUser.optionUserId,
  siteID:this.verifyVacanyUnitdata.siteID,
  unitType :this.verifyVacanyUnitdata.unitType,
  unitStatusType:this.verifyVacanyUnitdata.unitStatusType,
  unitfeaturesIds :this.verifyVacanyUnitdata.unitfeaturesIds,
   sameAsSiteInterviewAddressType :this.verifyVacanyUnitdata.sameAsSiteInterviewAddressType,
   interviewAddress :this.verifyVacanyUnitdata.interviewAddress,
   interviewCity :this.verifyVacanyUnitdata.interviewCity,
   interviewState :this.verifyVacanyUnitdata.interviewState,
   interviewZip :this.verifyVacanyUnitdata.interviewZip,
   interviewPhone :this.verifyVacanyUnitdata.interviewPhone

}
if (this.unitverifiedData.sameAsSiteInterviewAddressType === 33)
{
    this.unitverifiedData.interviewAddress = null;
    this.unitverifiedData.interviewCity = null;
    this.unitverifiedData.interviewState = null;
    this.unitverifiedData.interviewZip = null;
    this.unitverifiedData.interviewPhone = null;
}  
else
{
 // alert(this.VerifyVacancyform.get('interviewPhoneCtrl').value);
  console.log(this.unitverifiedData);
  this.unitverifiedData.interviewPhone= this.VerifyVacancyform.get('interviewPhoneCtrl').value;
}
if (this.unitverifiedData.sameAsSiteInterviewAddressType === 0)
{
    this.unitverifiedData.interviewAddress = null;
    this.unitverifiedData.interviewCity = null;
    this.unitverifiedData.interviewState = null;
    this.unitverifiedData.interviewZip = null;
    this.unitverifiedData.interviewPhone = null;
    this.unitverifiedData.sameAsSiteInterviewAddressType = 33
}  
//alert(this.VerifyVacancyform.get('unitFeaturesCtrl').value);
this.unitverifiedData.unitfeaturesIds =     this.convertArrayToString(this.VerifyVacancyform.get('unitFeaturesCtrl').value);
if (this.ValidateVacancyverification() === true) {
      this.monitorVacanciesService.SaveVerifyVacancy(this.unitverifiedData)
                        .subscribe(
                                  data => {
                                    this.toastr.success("Successfully Saved", 'Save Success');
                                    this.router.navigate(['/vcs/monitor-vacancies']);
                                    },
                                  error => {this.toastr.error("Save Failed", 'Save Failed');
                                  this.savedisabled= false;
                                }
                                );
              
          }
          this.savedisabled= false;
  }

  
 

 convertStringToArray(s:string) : Array<number> {
  if (s != null)  {
    if (s==='') { return null;}
    var array = s.split(',').map(Number);
    return array;
   }
  else  {
    return null;
  }

}


convertArrayToString(s:any) : string {
  if (s != null && s !='' )
  {
    return s.join(',');
  
  }
  else
  {
    return null;
  }
}


resetformerror()
{
  this.formErrors = {
    interviewLocationctrl:'',
    interviewAddressCtrl:'',
    interviewCityCtrl:'',
    interviewStateCtrl:'',
    interviewZipCtrl:'',
    interviewPhoneCtrl:'',
    unitExpectedAvailableDateCtrl:'',
    unitOfflineReasonCtrl:'',
    unitTypeCtrl:'',
    unitStatusCtrl:'',
    unitVerificationStatusCtrl:''

  };
}



ValidateVacancyverification= (): boolean => {
    //this.clearValidators();
    var messages : any ;
    var key : any;
    this.resetformerror();
if (this.varificationexistingstaus === 608) {
  if (this.VerifyVacancyform.get('unitVerificationStatusCtrl').value === 608 )
  {
    messages = " Please select verification status pending or offline"
    key ="unitVerificationStatusCtrl";
    this.formErrors[key] +=  messages + '\n';
  }
}
else
{

      if (!this.VerifyVacancyform.controls.interviewLocationctrl.valid ){
              key ="interviewLocationctrl";
              messages= this.validationMessages[key];
              for (const errorKey in this.VerifyVacancyform.controls.interviewLocationctrl.errors) {
                if (errorKey) {
                  this.formErrors[key] +=messages[errorKey] + '\n';
                }
            } 
          }


    
    if (this.verifyVacanyUnitdata.sameAsSiteInterviewAddressType === 34) {
      if (!this.VerifyVacancyform.controls.interviewAddressCtrl.valid )
        {
          key ="interviewAddressCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.VerifyVacancyform.controls.interviewAddressCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=  messages[errorKey] + '\n';
            }
        } 
      }
        if (!this.VerifyVacancyform.controls.interviewStateCtrl.valid ){
            key ="interviewStateCtrl";
            messages= this.validationMessages[key];
            for (const errorKey in this.VerifyVacancyform.controls.interviewStateCtrl.errors) {
              if (errorKey) {
                this.formErrors[key] += messages[errorKey] + '\n';
              }
           } 
        }
          if (!this.VerifyVacancyform.controls.interviewZipCtrl.valid){
              key ="interviewZipCtrl";
              messages= this.validationMessages[key];
              for (const errorKey in this.VerifyVacancyform.controls.interviewZipCtrl.errors) {
                if (errorKey) {
                  this.formErrors[key] +=messages[errorKey] + '\n';
                }
            } 
          }
            if (!this.VerifyVacancyform.controls.interviewPhoneCtrl.valid) {
                key ="interviewPhoneCtrl";
                messages= this.validationMessages[key];
                for (const errorKey in this.VerifyVacancyform.controls.interviewPhoneCtrl.errors) {
                  if (errorKey) {
                    this.formErrors[key] +=  messages[errorKey] + '\n';
                  }
              } 
            }
     
    }
    if (!this.VerifyVacancyform.controls.unitStatusCtrl.valid ){
      key ="unitStatusCtrl";
      messages= this.validationMessages[key];
      for (const errorKey in this.VerifyVacancyform.controls.interviewLocationctrl.errors) {
        if (errorKey) {
          this.formErrors[key] +=messages[errorKey] + '\n';
        }
    } 
  }
 
  if (!this.VerifyVacancyform.controls.unitTypeCtrl.valid ){
    key ="unitTypeCtrl";
    messages= this.validationMessages[key];
    for (const errorKey in this.VerifyVacancyform.controls.unitTypeCtrl.errors) {
      if (errorKey) {
        this.formErrors[key] +=messages[errorKey] + '\n';
      }
  } 
}


    
    if (this.verifyVacanyUnitdata.modifiedUnitStatus === 473   ) {

      if (!this.VerifyVacancyform.controls.unitOfflineReasonCtrl.valid ) {
        key ="unitOfflineReasonCtrl";
        messages= this.validationMessages[key];
      for (const errorKey in this.VerifyVacancyform.controls.unitOfflineReasonCtrl.errors) {
       if (errorKey) {
        this.formErrors[key] +=  messages[errorKey] + '\n';
       }
      } 
   }
      
      if (!this.VerifyVacancyform.controls.unitExpectedAvailableDateCtrl.valid ) {
        key ="unitExpectedAvailableDateCtrl";
        messages= this.validationMessages[key];
      for (const errorKey in this.VerifyVacancyform.controls.unitExpectedAvailableDateCtrl.errors) {
       if (errorKey) {
        this.formErrors[key] +=  messages[errorKey] + '\n';
       }
      } 
   }
   else {
         var exerror ;
          exerror= this.ValidateExpectedAvailabledate();
        if (exerror !=='') {
          key ="unitExpectedAvailableDateCtrl";
          this.formErrors[key] +=  exerror + '\n';
        }
      }    
  }
}
  var errormessage = '';
      for (const errorkey in this.formErrors) {
        if (errorkey) {
          errormessage += this.formErrors[errorkey];
        }
      }
      if (errormessage != '') 
      {
        this.toastr.error(errormessage,"Validation Error", );
        this.savedisabled= false;
        return false;
      }
        else
        {
          return true; 
        }

}


  ValidateExpectedAvailabledate =(): string => {
    var ermessage='';
    var itm =  this.unitOfflineReasons.find(x => x.unitOfflineReasonID === this.verifyVacanyUnitdata.offlineReasonType) as UnitOfflineReason;
    if (itm != null)
    {
      var timeframe = itm.timeFrame;
      var expecteddate = new Date();
     // alert(timeframe);
      if (timeframe > 0) {
        /* alert(timeframe);
        alert(expecteddate);*/
       expecteddate.setDate(expecteddate.getDate() + timeframe);
         /* alert(expecteddate);
          alert(this.verifyVacanyUnitdata.expectedAvailableDate);*/
      
         if (expecteddate < this.verifyVacanyUnitdata.expectedAvailableDate)
         {
             ermessage ='Expected available days should be within: ' + timeframe.toString() + "days";
          }
         /*alert(ermessage);*/
       }
    }
    return ermessage;
  }
 
  exitClick()
  {
   
    if (this.VerifyVacancyform.touched || this.VerifyVacancyform.dirty)
     {
    
    const title = 'Confirm exit';
    const primaryMessage = '';
    const secondaryMessage = "Values are changed. do you need to exit without saving ? ";
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => {this.router.navigate(['/vcs/monitor-vacancies']);},
        (negativeResponse) => console.log(),
      );
    }
    else
    {
      this.router.navigate(['/vcs/monitor-vacancies']);
    }
} 

}
