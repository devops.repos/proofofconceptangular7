import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { IMonitorVacancy,verifyVacancyDetail } from "./monitor-vacancies-model"

@Injectable({
  providedIn: 'root'
})
export class MonitorVacanciesService {
  MonitorVacanciesUrl = environment.pactApiUrl + 'VacancyMonitor';
  
  
  private mvSelected = new BehaviorSubject<number>(0);
  private mvtabSelected = new BehaviorSubject<number>(null);
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }
 
 
  getVacancyMonitorList(AgId : string,statusID :string,vvID)
  {
    return this.httpClient.get(this.MonitorVacanciesUrl+"/GetVacancyMonitorList", {params : { agencyId : AgId,verificationStatusID:statusID,VacancyVerficatioID:vvID }, observe : 'response' });
  }

  getUnitOfflineReason()
  {
    return this.httpClient.get(this.MonitorVacanciesUrl+"/GetUnitOfflineReason",  {observe : 'response' });
  }
  
 

  setMonitorVacancySelected(mvID: number) {
    this.mvSelected.next(mvID);
  }

  getMonitorVacancySelected() {
    return this.mvSelected.asObservable();
  }

  setMonitorVacancytabSelected(mvID: number) {
    this.mvtabSelected= new BehaviorSubject<number>(null);
    this.mvtabSelected.next(mvID);
  }

  getMonitorVacancytabSelected() {
    return this.mvtabSelected.asObservable();
  }


  SaveVerifyVacancy(vacancydetail : verifyVacancyDetail)
  {
  return this.httpClient.post(this.MonitorVacanciesUrl+"/SaveVacancyVerificationData", JSON.stringify(vacancydetail), this.httpOptions);
  }

  getVacancyVeriificationHist(unitid : string,)
  {
    return this.httpClient.get(this.MonitorVacanciesUrl+"/GetVacancyVeriificationHist", {params : { unitID : unitid }, observe : 'response' });
  }

  
}