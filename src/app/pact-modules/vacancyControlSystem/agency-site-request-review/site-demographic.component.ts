import { FormGroup, FormBuilder, Validators,AbstractControl } from '@angular/forms';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,AfterViewInit,
  OnDestroy,
  OnChanges
} from '@angular/core';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { ToastrService } from 'ngx-toastr';
import { RefGroupDetails } from './../../../models/refGroupDetailsDropDown.model';
import {  housingProgram,siteRequestDemographic, UserOptions, SiteFeature } from "../agency-site-maintenance/agency-site-model"
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { CommonService } from '../../../services/helper-services/common.service';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import { Subscription } from 'rxjs';
import { AgencySiteRequestReviewService } from '../agency-site-request-review/agency-site-request-review.service';
import {SiteRequestService} from '../site-request/site-request.service';
import {SiteRequestEventService} from '../site-request/site-request-event.service';
import { MatRadioChange } from '@angular/material';
import { ConfirmDialogService } from  '../../../shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-site-demographic-request',
  templateUrl: './site-demographic.component.html',
  styleUrls: ['./site-demographic.component.scss'],
  providers: [DatePipe]
})
export class SiteDemographicRequestComponent implements OnInit, OnDestroy  {

  tCOS : RefGroupDetails[];
  taxCredits : RefGroupDetails[];
  levelOfCareTypes : RefGroupDetails[];
  siteFeatures:RefGroupDetails[];
  siteTypes:RefGroupDetails[];
  interviewLocations:RefGroupDetails[];
  housingPrograms: housingProgram[];

  currentSiteLocationType: number;
  siteAdminSvcSub: Subscription;
  commonServiceSub: Subscription;
  agnySiteReqReviewSvcSub5: Subscription;
  siteReqSvcSub2: Subscription;
  siteReqSvcSub3: Subscription;

  siteDemographicGroup: FormGroup;

  @Input() siteDemographicData: siteRequestDemographic;
  @Input() refGroupDetailsData : RefGroupDetails[];
  @Input() options:UserOptions;
  @Input() siteRequestID: number;
  @Input() requestTypeID: number;
  
  @Input() siteFeatureData:Array<number>;

  @Input() housingProgramData:housingProgram[];
  @Output() siteDemographicDataChanged = new EventEmitter();
  
    
  validationMessages = {
  
    siteTypeRadioCtrl: {
      'required': 'Please select the type of the site.',
      'radioGroupRequired':'Please select the type of the site.',
      
    },
    tcoReadyRadioCtrl: {
        'required': 'Please select Tco ready.',
        
      },
      siteContratctedRadioCtrl: {
        'required': 'Please select Is contracted .',
        
      },

      contractStartDateCtrl: {
        'required': 'Please enter the site contracted date .',
        'dateValidator':'Contract date is invalid'
      },
      tcocontractStartDateCtrl : {
        'required': 'Please enter the site contracted date .',
        'dateValidator':'TCO Contract date is invalid'
      },
      taxcreditedRadioCtrl: {
        'required': 'Please select Tax credited.',
        
      },
      levelOfCareTypeCtrl: {
        'required': 'Please select the level of care.',
       
      },

      maxIncomeForStudioCtrl: {
      'required': 'Please enter the max income for studio',
     
    },
    maxIncomeForOneBRctrl: {
        'required': 'Please max income for one Br.',
       
      },
      

    housingProgramCtrl: {
      'required': 'Please select housing program model.',
     
    },
    siteFeaturesCtrl: {
      'required': 'Please select site features.',
     
    },
    interviewLocationctrl: {
      'required': 'Please select interview loaction.',
     
    },

    interviewAddressCtrl: {
      'required': 'Please enter interview Address',
      'maxlength': 'Max length of text is 200 characters.'
    },
    interviewCityCtrl: {
      'required': 'Please enter interview city.',
      'pattern': 'Please enter alphabets. No other characters allowed.',
      'maxlength' : 'Max length of text is 50 characters.'
    },
    interviewStateCtrl: {
      'required': 'Please enter interview state. ',
      'pattern': 'Please enter alphabets. No other characters allowed.',
      'maxlength' : 'Max length of text is 2 characters.'
    },
    interviewZipCtrl: {
      'required': 'Please enter interview Zip code.',
      'pattern': 'Please enter 5 digit number.',
      'maxlength' : 'Max length of text is 5 characters.'
    },

    interviewPhoneCtrl:{
      'required': 'Please enter interview phone number.',
        'pattern': 'Please enter 10 digit number.',
      'maxlength' : 'Max length of text is 10 characters.',
      'Mask error': 'Invalid Phone number. Please enter date 10 digit phone number',
    }

  };
  
  formErrors = {
    siteTypeRadioCtrl: '',
    levelOfCareTypeCtrl: '',
    housingProgramCtrl: '',
    siteFeaturesCtrl:'',
    interviewLocationctrl:'',
    interviewAddressCtrl:'',
    interviewCityCtrl:'',
    interviewStateCtrl:'',
    interviewZipCtrl:'',
    interviewPhoneCtrl:'',
    maxIncomeForStudioCtrl:'',
    maxIncomeForOneBRctrl:'',
    taxcreditedRadioCtrl:'',
    tcocontractStartDateCtrl:'',
    tcoReadyRadioCtrl:'',
    siteContratctedRadioCtrl:'',
    contractStartDateCtrl:''
  };

  
  constructor(
    private demographicFormBuilder: FormBuilder,
    private toastr: ToastrService,
    private datePipe: DatePipe, private confirmDialogService: ConfirmDialogService,
    private router: Router,private siteAdminSvc : SiteAdminService,
    private commonService : CommonService, private siteRequestSvc : SiteRequestService,
    private agnySiteReqReviewSvc: AgencySiteRequestReviewService, private siteRequestEventSvc : SiteRequestEventService,
  ) {
    
  }

  ngOnInit() {
      let that = this;

    this.siteDemographicGroup = this.demographicFormBuilder.group({
      siteTypeRadioCtrl :[this.siteDemographicData.locationType,[CustomValidators.radioGroupRequired()]],
      //contractStartDateCtrl: ['',Validators.compose([Validators.required, this.dateValidator])],
      contractStartDateCtrl: ['',[Validators.required]],
      tcoReadyRadioCtrl:[this.siteDemographicData.tcoReadyType, [Validators.required]],
      //siteContratctedRadioCtrl:['',Validators.required],
      siteContratctedRadioCtrl:[this.siteDemographicData.contractType, [Validators.required]],
      tcocontractStartDateCtrl:['',Validators.compose([Validators.required])],
      //tcocontractStartDateCtrl:['',Validators.compose([Validators.required, this.dateValidator])],
      taxcreditedRadioCtrl:[this.siteDemographicData.taxCreditType, [Validators.required]],
      levelOfCareTypeCtrl:[this.siteDemographicData.levelOfCareType,Validators.required],
      housingProgramCtrl:[this.siteDemographicData.housingProgram,Validators.required],
      siteFeaturesCtrl: ['',Validators.required],
      interviewAddressCtrl:['', [Validators.required,Validators.maxLength(200)]],
      interviewStateCtrl:['', [Validators.required,Validators.maxLength(2), Validators.pattern('[a-zA-Z]+')]],
      interviewCityCtrl:['', [Validators.required,Validators.maxLength(50), Validators.pattern('[a-zA-Z][a-zA-Z ]*')]],
      interviewZipCtrl:['', [ Validators.required,Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      interviewPhoneCtrl:['',[ Validators.required,Validators.maxLength(10), Validators.pattern('[0-9]+')]],
      maxIncomeForStudioCtrl:['', [ Validators.maxLength(25), ]],
      maxIncomeForOneBRctrl:['', [ Validators.maxLength(25), ]],
      interviewLocationctrl:['',Validators.required]
      });
  
    setTimeout(() => {
      that.loadRefGroupDetails();
      that.loadHousingPrograms();
      that.loadSiteFeatures();

      that.currentSiteLocationType = that.siteDemographicData.locationType;
      that.siteRequestEventSvc.emitSiteRequestLocationType(that.siteRequestID, that.currentSiteLocationType); // initial location type

    }, 1500);

  //  this.siteDemographicGroup.valueChanges.subscribe(data => {
  //     if (this.siteDemographicGroup.touched || this.siteDemographicGroup.dirty){
  //       //this.ValidateSiteDemographicData();
  //     }
  //   });
    this.enabledisablecontrol();
  }
 

loadRefGroupDetails( )
{
  let value = "7,12,25,26";
  if(this.refGroupDetailsData){
    this.tCOS = this.refGroupDetailsData.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
    this.taxCredits = this.refGroupDetailsData.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
    this.interviewLocations = this.refGroupDetailsData.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
    this.siteFeatures = this.refGroupDetailsData.filter(d => {return d.refGroupID === 12 });
    this.siteTypes = this.refGroupDetailsData.filter(d => {return d.refGroupID === 26 });
    this.levelOfCareTypes = this.refGroupDetailsData.filter(d => {return d.refGroupID === 25 });
  }
  else
    this.commonServiceSub = this.commonService.getRefGroupDetails(value)
              .subscribe(
                res => {
                  const data = res.body as RefGroupDetails[];
                  this.tCOS = data.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
                  this.taxCredits = data.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
                  this.interviewLocations = data.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
                  this.siteFeatures = data.filter(d => {return d.refGroupID === 12 });
                  this.siteTypes = data.filter(d => {return d.refGroupID === 26 });
                  this.levelOfCareTypes = data.filter(d => {return d.refGroupID === 25 });
                },
                error => console.error('Error!', error)
              );
}

  loadHousingPrograms()
   {
    if(this.housingProgramData)
      this.housingPrograms = this.housingProgramData;
    else
      this.siteAdminSvcSub = this.siteAdminSvc.getHousingPrograms()
        .subscribe(
          res => {
            this.housingPrograms= res.body as housingProgram[];
          },
          error => console.error('Error!', error)
        ); 
   }

   loadSiteFeatures()
   {
     let that = this;
     if (this.siteRequestID > 0) {
      this.agnySiteReqReviewSvcSub5 = this.agnySiteReqReviewSvc.getSiteRequestFeatures(this.siteRequestID)
        .subscribe(
          res => {
            let existArray = res as SiteFeature[];
            if(existArray && existArray.length > 0){
              that.siteFeatureData = [];
              that.siteFeatureData = existArray.map(it => {return it.siteFeatureType});
            }
          },
          error => console.error('Error!', error)
        );     
     }
  }

   siteFeaturesOnChange($event){
    this.saveSiteFeatures();
   }

   saveSiteFeatures(){
    if(this.requestTypeID && this.requestTypeID.toString() == "322") return; // RETURN // new primary service contract type

    let milliSecs : number = 0; let that = this;
    if(this.siteFeatureData && this.siteFeatureData.length > 0)  {
      this.siteReqSvcSub2 = this.siteRequestSvc.deleteAllSiteFeatures(this.siteRequestID)
      .subscribe(
        data => {
                  if(that.siteFeatureData && that.siteFeatureData.length > 0)
                      that.siteFeatureData.forEach(featureID => {
                        milliSecs = milliSecs + 500;
                            setTimeout(() => {
                              that.saveSiteFeature(featureID);
                            }, milliSecs);
                      });
              },
          error => {this.toastr.error("deleteAllSiteFeatures Failed", 'Delete Failed');}
        );
    }
  }
  
  saveSiteFeature(featureID){
      let feature : SiteFeature = {
        siteFeatureMappingID : null,
        siteFeatureMappingRequestID : 0, // new
        siteID : null,
        siteRequestID : this.siteRequestID,
        siteFeatureType: featureID,
        userId: this.options.optionUserId
      }
      this.siteReqSvcSub3 = this.siteRequestSvc.saveSiteRequestFeature(feature)
      .subscribe(
                data => {
                  feature.siteFeatureMappingRequestID = data;
                },
                  error => {this.toastr.error("Save Failed", 'Save Failed');}
        );
  }

  onSubmit() {
    this.markFormGroupTouched(this.siteDemographicGroup);
    this.setSiteDemographicData();
     this.ValidateSiteDemographicData();
      this.siteDemographicDataChanged.emit(this.siteDemographicData);
      let errormessage = '';
      for (const errorkey in this.formErrors) {
        if (errorkey) {
          errormessage += this.formErrors[errorkey];
        }
      }
      if (errormessage != '') 
      {
        this.toastr.warning(errormessage,"Validation Failed.");
        this.siteRequestEventSvc.emitSiteRequestSiteDemographics(this.siteRequestID, false);
      }
      else
      {
        // this.toastr.info("No validation Errors on Site Demographics Tab.", "Validation passed");
         this.siteRequestEventSvc.emitSiteRequestSiteDemographics(this.siteRequestID, true);
      }
  }

  private markFormGroupTouched(formGroup: FormGroup)  {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }


  resetformerror()
  {
    this.formErrors = {
      siteTypeRadioCtrl: '',
      levelOfCareTypeCtrl: '',
      housingProgramCtrl: '',
      siteFeaturesCtrl:'',
      interviewLocationctrl:'',
      interviewAddressCtrl:'',
      interviewCityCtrl:'',
      interviewStateCtrl:'',
      interviewZipCtrl:'',
      interviewPhoneCtrl:'',
      maxIncomeForStudioCtrl:'',
      maxIncomeForOneBRctrl:'',
      taxcreditedRadioCtrl:'',
      tcocontractStartDateCtrl:'',
      tcoReadyRadioCtrl:'',
      siteContratctedRadioCtrl:'',
      contractStartDateCtrl:''
          
    };
  }
  
  setSiteDemographicData() {

    if (this.siteDemographicData.locationType == 233)
      {
       
        this.siteDemographicData.contractStartDate = null;
        this.siteDemographicData.contractType = null;
        //this.siteDemographicData.tcoContractStartDate = this.datePipe.transform( this.siteDemographicData.tcoContractStartDate, 'MM/dd/yyyy');
        if (this.siteDemographicData.taxCreditType!= 33)
        {
          this.siteDemographicData.maxIncomeForOneBR= null;
          this.siteDemographicData.maxIncomeForStudio= null;
        }
           
      }
      else
      {
        this.siteDemographicData.tcoReadyType = null;
        this.siteDemographicData.tcoContractStartDate = null;
        this.siteDemographicData.taxCreditType = null;
        this.siteDemographicData.maxIncomeForOneBR = null;
        this.siteDemographicData.maxIncomeForStudio = null;
        //this.siteDemographicData.levelOfCareType= null
        //this.siteDemographicData.contractStartDate = this.datePipe.transform( this.siteDemographicData.contractStartDate, 'MM/dd/yyyy');
      }
        
    this.siteDemographicData.siteFeatures =this.convertArrayToString(this.siteDemographicGroup.get('siteFeaturesCtrl').value);
    this.siteFeatureData = this.siteDemographicGroup.get('siteFeaturesCtrl').value;
    this.siteDemographicData.userId = this.options.optionUserId;
    if (this.siteDemographicData.sameAsSiteInterviewAddressType == 33)
    {
        this.siteDemographicData.interviewAddress = null;
        this.siteDemographicData.interviewCity = null;
        this.siteDemographicData.interviewState = null;
        this.siteDemographicData.interviewZip = null;
        this.siteDemographicData.interviewPhone = null;
    }  
    else
    {
      this.siteDemographicData.interviewPhone= this.siteDemographicGroup.get('interviewPhoneCtrl').value;
    }

  }

  convertArrayToString(s:any) : string {
    if (s != null && s !='' )
    {
      return s.join(',');
    
    }
    else
    {
      return null;
    }
  }

  convertStringToArray(s:string) : Array<number> {
    if (s != null)
    {
    var array = s.split(',').map(Number);
    return array;
    }
    else
    {
      return null;
    }

   
  }

  ValidateSiteDemographicData()
    {
    //this.clearValidators();
    let messages : any ;
    let key : any;
    this.resetformerror();

    if (!this.siteDemographicGroup.controls.siteTypeRadioCtrl.valid) {
        key ="siteTypeRadioCtrl";
        messages= this.validationMessages[key];
        for (const errorKey in this.siteDemographicGroup.controls.siteTypeRadioCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + '\n';
          }
      } 
    }
    if (!this.siteDemographicGroup.controls.levelOfCareTypeCtrl.valid )  {
      key ="levelOfCareTypeCtrl";
      messages= this.validationMessages[key];
      for (const errorKey in this.siteDemographicGroup.controls.levelOfCareTypeCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] +=  messages[errorKey] + '\n';
        }
      } 
    }

      if (!this.siteDemographicGroup.controls.housingProgramCtrl.valid ){
          key ="housingProgramCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.housingProgramCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=messages[errorKey] + '\n';
            }
          } 
      }
        if (!this.siteDemographicGroup.controls.siteFeaturesCtrl.valid ){
            key ="siteFeaturesCtrl";
            messages= this.validationMessages[key];
            for (const errorKey in this.siteDemographicGroup.controls.siteFeaturesCtrl.errors) {
              if (errorKey) {
                this.formErrors[key] += messages[errorKey] + '\n';
              }
          } 
        }
        if (!this.siteDemographicGroup.controls.interviewLocationctrl.valid ){
              key ="interviewLocationctrl";
              messages= this.validationMessages[key];
              for (const errorKey in this.siteDemographicGroup.controls.interviewLocationctrl.errors) {
                if (errorKey) {
                  this.formErrors[key] +=messages[errorKey] + '\n';
                }
            } 
          }
    
  
    if (this.siteDemographicData.locationType== 233) {
      if (!this.siteDemographicGroup.controls.tcoReadyRadioCtrl.valid){
          key ="tcoReadyRadioCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.tcoReadyRadioCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + '\n';
            }
          } 
      }

      if (!this.siteDemographicGroup.controls.tcocontractStartDateCtrl.valid ) {
          key ="tcocontractStartDateCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.tcocontractStartDateCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=  messages[errorKey] + '\n';
            }
          } 
      }
      if (!this.siteDemographicGroup.controls.taxcreditedRadioCtrl.valid )
        {
          key ="taxcreditedRadioCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.taxcreditedRadioCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=  messages[errorKey] + '\n';
            }
        } 
      }
      if (!this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.valid ){
          key ="maxIncomeForStudioCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=  messages[errorKey] +'\n';
            }
        } 
      }
      if (!this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.valid ){
          key ="maxIncomeForOneBRctrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=  messages[errorKey] + '\n';
            }
        } 
      }
    }
    if (this.siteDemographicData.locationType== 234)
    {
      if (!this.siteDemographicGroup.controls.siteContratctedRadioCtrl.valid ) {
          key ="siteContratctedRadioCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.siteContratctedRadioCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=  messages[errorKey] + '\n';
            }
          } 
      }
      if (!this.siteDemographicGroup.controls.contractStartDateCtrl.valid){
          key ="contractStartDateCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.contractStartDateCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + '\n';
            }
          } 
      }
    }

    
    if (this.siteDemographicData.sameAsSiteInterviewAddressType == 34) {
      if (!this.siteDemographicGroup.controls.interviewAddressCtrl.valid )
        {
          key ="interviewAddressCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.siteDemographicGroup.controls.interviewAddressCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=  messages[errorKey] + '\n';
            }
        } 
      }
        if (!this.siteDemographicGroup.controls.interviewStateCtrl.valid ){
            key ="interviewStateCtrl";
            messages= this.validationMessages[key];
            for (const errorKey in this.siteDemographicGroup.controls.interviewStateCtrl.errors) {
              if (errorKey) {
                this.formErrors[key] += messages[errorKey] + '\n';
              }
           } 
        }
          if (!this.siteDemographicGroup.controls.interviewZipCtrl.valid){
              key ="interviewZipCtrl";
              messages= this.validationMessages[key];
              for (const errorKey in this.siteDemographicGroup.controls.interviewZipCtrl.errors) {
                if (errorKey) {
                  this.formErrors[key] +=messages[errorKey] + '\n';
                }
            } 
          }
            if (!this.siteDemographicGroup.controls.interviewPhoneCtrl.valid) {
                key ="interviewPhoneCtrl";
                messages= this.validationMessages[key];
                for (const errorKey in this.siteDemographicGroup.controls.interviewPhoneCtrl.errors) {
                  if (errorKey) {
                    this.formErrors[key] +=  messages[errorKey] + '\n';
                  }
              } 
            }
    }
  }


   //Validate Date
   dateValidator(AC: AbstractControl) {
    if (AC && AC.value && !moment(AC.value, 'MM/DD/YYYY', true).isValid()) {
      return { 'dateValidator': true };
    }
   return null;
  }

  onInterviewlocationselected(event) {
      this.siteDemographicGroup.controls.interviewPhoneCtrl.setValue(' ');
      this.siteDemographicData.interviewPhone = ' ';
  }

  ngOnDestroy() {
    if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
    if(this.siteAdminSvcSub) this.siteAdminSvcSub.unsubscribe();
    if(this.agnySiteReqReviewSvcSub5) this.agnySiteReqReviewSvcSub5.unsubscribe();
    if(this.siteReqSvcSub2) this.siteReqSvcSub2.unsubscribe();
    if(this.siteReqSvcSub3) this.siteReqSvcSub3.unsubscribe();
  }

  enabledisablecontrol()
{
  if (this.options.isDisableForHP){
    this.siteDemographicGroup.controls.siteTypeRadioCtrl.disable();
    this.siteDemographicGroup.controls.tcoReadyRadioCtrl.disable();
    this.siteDemographicGroup.controls.tcocontractStartDateCtrl.disable();
    this.siteDemographicGroup.controls.taxcreditedRadioCtrl.disable();
    this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.disable();
    this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.disable();
    this.siteDemographicGroup.controls.siteContratctedRadioCtrl.disable();
    this.siteDemographicGroup.controls.contractStartDateCtrl.disable();
    this.siteDemographicGroup.controls.levelOfCareTypeCtrl.disable();
    this.siteDemographicGroup.controls.housingProgramCtrl.disable();
    this.siteDemographicGroup.controls.siteFeaturesCtrl.disable();
    if (this.options.isDisableForInactiveSite){
      this.siteDemographicGroup.controls.interviewLocationctrl.disable();
      this.siteDemographicGroup.controls.interviewAddressCtrl.disable();
      this.siteDemographicGroup.controls.interviewCityCtrl.disable();
      this.siteDemographicGroup.controls.interviewStateCtrl.disable();
      this.siteDemographicGroup.controls.interviewZipCtrl.disable();
      this.siteDemographicGroup.controls.interviewPhoneCtrl.disable();
    }
      else
      {
        this.siteDemographicGroup.controls.interviewLocationctrl.enable();
        this.siteDemographicGroup.controls.interviewAddressCtrl.enable();
        this.siteDemographicGroup.controls.interviewCityCtrl.enable();
        this.siteDemographicGroup.controls.interviewStateCtrl.enable();
        this.siteDemographicGroup.controls.interviewZipCtrl.enable();
        this.siteDemographicGroup.controls.interviewPhoneCtrl.enable();
      }

  }

  else {
      if (this.options.isDisableForInactiveSite){
        this.siteDemographicGroup.controls.siteTypeRadioCtrl.disable();
        this.siteDemographicGroup.controls.tcoReadyRadioCtrl.disable();
         this.siteDemographicGroup.controls.tcocontractStartDateCtrl.disable();
        this.siteDemographicGroup.controls.taxcreditedRadioCtrl.disable();
        this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.disable();
        this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.disable();
        this.siteDemographicGroup.controls.siteContratctedRadioCtrl.disable();
        this.siteDemographicGroup.controls.contractStartDateCtrl.disable();
        this.siteDemographicGroup.controls.levelOfCareTypeCtrl.disable();
        this.siteDemographicGroup.controls.housingProgramCtrl.disable();
        this.siteDemographicGroup.controls.siteFeaturesCtrl.disable();
       
        this.siteDemographicGroup.controls.interviewLocationctrl.disable();
        this.siteDemographicGroup.controls.interviewAddressCtrl.disable();
        this.siteDemographicGroup.controls.interviewCityCtrl.disable();
        this.siteDemographicGroup.controls.interviewStateCtrl.disable();
        this.siteDemographicGroup.controls.interviewZipCtrl.disable();
        this.siteDemographicGroup.controls.interviewPhoneCtrl.disable();
      }
      else{
        this.siteDemographicGroup.controls.siteTypeRadioCtrl.enable();
        this.siteDemographicGroup.controls.tcoReadyRadioCtrl.enable();
         this.siteDemographicGroup.controls.tcocontractStartDateCtrl.enable();
        this.siteDemographicGroup.controls.taxcreditedRadioCtrl.enable();
        this.siteDemographicGroup.controls.maxIncomeForStudioCtrl.enable();
        this.siteDemographicGroup.controls.maxIncomeForOneBRctrl.enable();
        this.siteDemographicGroup.controls.siteContratctedRadioCtrl.enable();
        this.siteDemographicGroup.controls.contractStartDateCtrl.enable();
        this.siteDemographicGroup.controls.levelOfCareTypeCtrl.enable();
        this.siteDemographicGroup.controls.housingProgramCtrl.enable();
        this.siteDemographicGroup.controls.siteFeaturesCtrl.enable();
       
        this.siteDemographicGroup.controls.interviewLocationctrl.enable();
        this.siteDemographicGroup.controls.interviewAddressCtrl.enable();
        this.siteDemographicGroup.controls.interviewCityCtrl.enable();
        this.siteDemographicGroup.controls.interviewStateCtrl.enable();
        this.siteDemographicGroup.controls.interviewZipCtrl.enable();
        this.siteDemographicGroup.controls.interviewPhoneCtrl.enable();
    }
  }
 
}

  radioChangeLocationType(event: MatRadioChange){
    let that = this;
    const title = 'Confirm Change';
    const primaryMessage = 'This Location Type of Site will be changed.';
    const secondaryMessage = 'Are you sure to change Location Type of this Site ?';
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    if(that.currentSiteLocationType && that.currentSiteLocationType > 0){

        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
              (positiveResponse) => { 
                    that.currentSiteLocationType = event.value;
                    that.siteRequestEventSvc.emitSiteRequestLocationType(that.siteRequestID, that.currentSiteLocationType);
                    that.resetLocationTypeDependentValues();
              }, // reset latest value 
              (negativeResponse) => { 
                    that.siteDemographicData.locationType = that.currentSiteLocationType;
              }, // reset back to previous value
        );
    }
    else {
        that.currentSiteLocationType = event.value;
        that.siteRequestEventSvc.emitSiteRequestLocationType(that.siteRequestID, that.currentSiteLocationType);
        that.resetLocationTypeDependentValues();
    }

  }

  resetLocationTypeDependentValues(){
      if(this.siteDemographicData.locationType == 233){
          this.siteDemographicData.contractType = null;
          this.siteDemographicData.contractStartDate = null;
      } else if(this.siteDemographicData.locationType == 234){
          this.siteDemographicData.tcoReadyType = null;
          this.siteDemographicData.tcoContractStartDate = null;
          this.siteDemographicData.taxCreditType = null;
          this.siteDemographicData.maxIncomeForStudio = null;
          this.siteDemographicData.maxIncomeForOneBR = null;
      }
  }

}