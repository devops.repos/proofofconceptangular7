import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import {AgencySiteRequestReviewModel} from '../agency-site-request/agency-site-request.model';
import { RaAgencySiteRequestModel } from "../agency-site-request-review-ra/ra-agency-site-request.model"

@Injectable({
  providedIn: 'root'
})

export class AgencySiteRequestReviewService {
  
  private getRaAgencySiteRequestURL = environment.pactApiUrl + 'Agency/GetRaAgencySiteRequest';
  private getSiteAgreementRequestURL = environment.pactApiUrl + 'Site/GetSiteRequestAgreementProfile';
  private getDemographicsRequestURL = environment.pactApiUrl + 'Site/GetSiteRequestDemographics';
  private getSiteApprovalRequestURL = environment.pactApiUrl + 'Site/GetSiteRequestApproval';
  private saveReviewedAgencySiteRequestURL = environment.pactApiUrl + 'Agency/SaveReviewedAgencySiteRequest';
  private saveReviewedRaAgencySiteRequestURL = environment.pactApiUrl + 'Agency/SaveReviewedRaAgencySiteRequest';
  private getSiteRequestContactsURL = environment.pactApiUrl + 'Site/GetSiteRequestContacts';
  private getSiteRequestUnitsURL = environment.pactApiUrl + 'Site/GetSiteRequestUnits';
  private getSiteRequestFeaturesURL = environment.pactApiUrl + 'Site/GetSiteRequestFeatures';
  private saveReviewedSiteAgreePopRequestURL = environment.pactApiUrl + 'Site/SaveReviewedSiteAgreementPopulationRequest';
  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }

  getSiteAgreementProfileRequest(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.getSiteAgreementRequestURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  getSiteRequestContacts(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.getSiteRequestContactsURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  getSiteRequestUnits(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.getSiteRequestUnitsURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  getSiteRequestFeatures(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.getSiteRequestFeaturesURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  getSiteRequestDemographics(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.getDemographicsRequestURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  getSiteApprovalData(siteRequestID : number): Observable<any>  {
    return this.httpClient.post(this.getSiteApprovalRequestURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  saveReviewedAgencySiteRequest(agencySiteRequestReviewModel: AgencySiteRequestReviewModel): Observable<any>  {
    return this.httpClient.post(this.saveReviewedAgencySiteRequestURL, JSON.stringify(agencySiteRequestReviewModel), this.httpOptions);
  }

  saveReviewedSiteAgreementPopulationRequest(siteRequestID : number, approvedType: number, userID : number): Observable<any>  {
    return this.httpClient.post(this.saveReviewedSiteAgreePopRequestURL, JSON.stringify({ "SiteRequestID" : siteRequestID, "ApprovedType" : approvedType, "UserId" : userID}), this.httpOptions);
  }

  getRaAgencySiteRequest(agencyRequestId : number, siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.getRaAgencySiteRequestURL, JSON.stringify({ "AgencyRequestId" : agencyRequestId, "SiteRequestID" : siteRequestID}), this.httpOptions);
  }

  saveReviewedRaAgencySiteRequest(agencySiteRequestReviewModel: RaAgencySiteRequestModel): Observable<any>  {
    return this.httpClient.post(this.saveReviewedRaAgencySiteRequestURL, JSON.stringify(agencySiteRequestReviewModel), this.httpOptions);
  }

}