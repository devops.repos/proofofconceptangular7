import { Component, OnInit,ViewChild, Input, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { SiteRequestAgreementPopulation } from "../agency-site-maintenance/agency-site-model"
import { AgencySiteRequestReviewService } from "./agency-site-request-review.service"

@Component({
  selector: 'app-site-agreement-detail-request',
  templateUrl: './site-agreement-detail.component.html',
  styleUrls: ['./site-agreement-detail.component.scss']
})
export class SiteAgreementDetailRequestComponent implements OnInit,OnDestroy {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  
   
  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  pagination;
  rowSelection;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;
  public gridOptions: GridOptions;

  overlayLoadingTemplate: string='';
  overlayNoRowsTemplate: string='';

  @Input() siteAgreePopData: SiteRequestAgreementPopulation[];

  constructor(
    private siteRequestSvc : AgencySiteRequestReviewService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
      sideBar: {
       toolPanels: [
               {
                   id: 'columns',
                   labelDefault: 'Columns',
                   labelKey: 'columns',
                   iconKey: 'columns',
                   toolPanel: 'agColumnsToolPanel',
                   toolPanelParams: {
                       suppressValues: true,
                       suppressPivots: true,
                       suppressPivotMode: true,
                       suppressRowGroups: false
                   }
               },
               {
                   id: 'filters',
                   labelDefault: 'Filters',
                   labelKey: 'filters',
                   iconKey: 'filter',
                   toolPanel: 'agFiltersToolPanel',
               }
           ],
           defaultToolPanel: ''
       }
    } as GridOptions;
    //this.gridOptions.api.hideOverlay();
   
    this.columnDefs = [
     {
        headerName: 'Site Agreement Population Request ID',
        filter: 'agTextColumnFilter',
        field : "siteAgreementPopulationRequestID",
        hide : true
     },
     {
        headerName: 'Primary Service Contract Type',
        field: 'primaryServiceAgreementPopName',
        width: 200,
        filter: 'agTextColumnFilter'
     },
     {
       headerName: 'Rental Subsidies',
       field: 'rentalSubsidies',
       width: 400,
       filter: 'agTextColumnFilter',
     //  hide : true
     },
     {
       headerName: 'Number of Units',
       field: 'totalUnits',
       width: 150,
       filter: 'agTextColumnFilter'
     }
    ];
   
    this.defaultColDef = {
     sortable: true,
     resizable: true,
     filter: false
    };
     this.rowSelection = 'single';
    
     this.pagination = true;
     this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the primary service contracts are loading.</span>';
     this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No  primary service contracts are available</span>';
   }

  ngOnInit() {
     
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {


    //  params.api.sizeColumnsToFit();
    //params.api.setDomLayout('autoHeight');
  //  this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the documents are loading.</span>';
    //this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Documents Available</span>';

    });
  }
  
  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onRowSelected(event) {
    if(event.node.selected) {
      //alert('row ' + event.node.data.siteID + ' selected = ' + event.node.selected);
     // alert(event.node);        
    }
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'SiteAgreemnetProfile-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

  }

  ngOnDestroy() {

  }
 

}
