import { Component, OnInit,
  Output,
  EventEmitter,
  Input,AfterViewInit,
  OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import { RefGroupDetails } from './../../../models/refGroupDetailsDropDown.model';
import {  siteRequestApproval, UserOptions,tADLiaison } from "../agency-site-maintenance/agency-site-model"
import { CommonService } from './../../../services/helper-services/common.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-site-approval-request',
  templateUrl: './site-approval.component.html',
  styleUrls: ['./site-approval.component.scss'],
  providers: [DatePipe]
})
export class SiteApprovalRequestComponent implements OnInit, OnDestroy {

    isYesNoList : RefGroupDetails[];

    commonServiceSub : Subscription;
    siteAdminSvcSub : Subscription;

    submitted : boolean = false;
    message: string;

    approvalFormGroup: FormGroup;

  @Input() tadLiaisons :tADLiaison[];
  @Input() refGroupDetailsData : RefGroupDetails[];
  @Input() siteApprovalData: siteRequestApproval;
  @Input() options:UserOptions;
  @Input() requestTypeID: number;

  @Output() siteApprovalChanged = new EventEmitter();


   validationMessages = {
      issiteTrackedRadioCtrl: {
        'required': 'Please select is site tracked.',
      },
      tadLiasonCtrl: {
        'required': 'Please select TADLiason.',
      },
      referralAvailableTypeRadioCtrl: {
        'required': 'Please select Available for Refererral.',
      },
      expectedAvailableDateCtrl : {
        'required': 'Please enter the site expected Available date.',
      },
      isActiveRadioCtrl: {
        'required': 'Please select is Active.',
      },
      additionalcommentsCtrl: {
        'required': 'Please enter additional comments.',
      },
  };

  formErrors = {
    issiteTrackedRadioCtrl: '',
    tadLiasonCtrl: '',
    referralAvailableTypeRadioCtrl:'',
    expectedAvailableDateCtrl:'',
    isActiveRadioCtrl:'',
    additionalcommentsCtrl:''
  };

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private siteAdminSvc : SiteAdminService,
    private commonService : CommonService,
    private datePipe: DatePipe
  ) {
    this.approvalFormGroup = this.formBuilder.group({
      issiteTrackedRadioCtrl :['',[CustomValidators.radioGroupRequired()]],
      tadLiasonCtrl: ['',Validators.compose([Validators.required])],
      referralAvailableTypeRadioCtrl:['',Validators.required],
      expectedAvailableDateCtrl:['',Validators.compose([Validators.required])],
      isActiveRadioCtrl:['',Validators.required],
      additionalcommentsCtrl:['',Validators.required]
      });

  }

  ngOnInit() {
    this.loadRefGroupDetails();
   // this.LoadTADLiaisons();
   if(this.requestTypeID === 322) // 322 = New Primary Service Contract Type
      this.options.isDisableForInactiveSite = true; // disable ALL controls
    this.enabledisablecontrol();
  }

 ngOnDestroy() {
    if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
    if(this.siteAdminSvcSub) this.siteAdminSvcSub.unsubscribe();
  }

    loadRefGroupDetails( )
    {
      /*  var value = "7,25,9,12,26";
      this.commonServiceSub = this.commonService.getRefGroupDetails(value)
                .subscribe(
                  res => {
                    const data = res.body as RefGroupDetails[];
                    this.isYesNoList = data.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
                  },
                  error => console.error('Error!', error)
                ); */
        if(this.refGroupDetailsData){
          this.isYesNoList = this.refGroupDetailsData.filter(d => {return ((d.refGroupID === 7) && (d.refGroupDetailID===33 || d.refGroupDetailID===34))});
        }
    }

/*    LoadTADLiaisons() {

    this.siteAdminSvcSub = this.siteAdminSvc.getTADLiaison('8')
      .subscribe(
        res => {
          if (res.body) {
            this.tadLiaisons = res.body as tADLiaison[];
          }
        },
        error => console.error('Error in retrieving the TAD Laisions ...!', error)
      );
   } */

enabledisablecontrol()
{
  if(this.siteApprovalData.siteTrackedType == 34){
    this.approvalFormGroup.controls.tadLiasonCtrl.disable();
  }
  else{
    this.approvalFormGroup.controls.tadLiasonCtrl.enable();
  }
  if (this.options.isDisableForHP === true){
    this.approvalFormGroup.controls.issiteTrackedRadioCtrl.disable();
    this.approvalFormGroup.controls.tadLiasonCtrl.disable();
    this.approvalFormGroup.controls.referralAvailableTypeRadioCtrl.disable();
    this.approvalFormGroup.controls.expectedAvailableDateCtrl.disable();
  //  this.approvalFormGroup.controls.isActiveRadioCtrl.disable();
    this.approvalFormGroup.controls.additionalcommentsCtrl.disable();
  }
  else {
      if (this.options.isDisableForInactiveSite === true){ // isDisableForInactiveSite = true... for requestTypeID === 322
        this.approvalFormGroup.controls.issiteTrackedRadioCtrl.disable();
        this.approvalFormGroup.controls.tadLiasonCtrl.disable();
        this.approvalFormGroup.controls.referralAvailableTypeRadioCtrl.disable();
        this.approvalFormGroup.controls.expectedAvailableDateCtrl.disable();
        this.approvalFormGroup.controls.isActiveRadioCtrl.disable();
        // this.approvalFormGroup.controls.additionalcommentsCtrl.disable(); // always enter approver comments.
      }
      else{
      this.approvalFormGroup.controls.issiteTrackedRadioCtrl.enable();
      this.approvalFormGroup.controls.tadLiasonCtrl.enable();
      this.approvalFormGroup.controls.referralAvailableTypeRadioCtrl.enable();
      this.approvalFormGroup.controls.expectedAvailableDateCtrl.enable();
      this.approvalFormGroup.controls.isActiveRadioCtrl.enable();
      this.approvalFormGroup.controls.additionalcommentsCtrl.enable();
    }
  }
}

onSubmit(): boolean {
  this.markFormGroupTouched(this.approvalFormGroup);
 this.ValidateSiteApprovalData();
    this.siteApprovalChanged.emit(this.siteApprovalData);
    var errormessage = '';
    for (const errorkey in this.formErrors) {
      if (errorkey) {
        errormessage += this.formErrors[errorkey];
      }
    }
    if (errormessage != '')
    {
      this.toastr.error(errormessage,"Validation Error", );
      return false;
    }
    else
    {
     // this.toastr.info("No validation Errors on Approval Tab.", "Validation passed");
      return true;
    }
}

private markFormGroupTouched = (formGroup: FormGroup) => {
  (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
    control.markAsTouched();
    if (control.controls) {
      this.markFormGroupTouched(control);
    }
  });
}

resetformerror()
{
  this.formErrors = {
    issiteTrackedRadioCtrl: '',
    tadLiasonCtrl: '',
    referralAvailableTypeRadioCtrl:'',
    expectedAvailableDateCtrl:'',
    isActiveRadioCtrl:'',
    additionalcommentsCtrl:''
  };
}

isActiveSelection(issiteActive: boolean)
{


}

ValidateSiteApprovalData()
    {
    //this.clearValidators();
    var messages : any ;
    var key : any;
    this.resetformerror();
    //alert(this.approvalFormGroup.get('isActiveRadioCtrl').value);

    if (!this.approvalFormGroup.controls.issiteTrackedRadioCtrl.valid) {
        key ="issiteTrackedRadioCtrl";
        messages= this.validationMessages.issiteTrackedRadioCtrl;
        for (const errorKey in this.approvalFormGroup.controls.issiteTrackedRadioCtrl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages.required + '\n';
          }
      }
    }
    if (!this.approvalFormGroup.controls.referralAvailableTypeRadioCtrl.valid )  {
      key ="referralAvailableTypeRadioCtrl";
      messages= this.validationMessages[key];
      for (const errorKey in this.approvalFormGroup.controls.referralAvailableTypeRadioCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] +=  messages[errorKey] + '\n';
        }
      }
    }
    if (!this.approvalFormGroup.controls.tadLiasonCtrl.valid )  {
      key ="tadLiasonCtrl";
      messages= this.validationMessages[key];
      for (const errorKey in this.approvalFormGroup.controls.tadLiasonCtrl.errors) {
        if (errorKey) {
          this.formErrors[key] +=  messages[errorKey] + '\n';
        }
      }
    }
      if (!this.approvalFormGroup.controls.expectedAvailableDateCtrl.valid ){
          key ="expectedAvailableDateCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.approvalFormGroup.controls.expectedAvailableDateCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=messages[errorKey] + '\n';
            }
          }
      }
        if (!this.approvalFormGroup.controls.isActiveRadioCtrl.valid ){
            key ="isActiveRadioCtrl";
            messages= this.validationMessages[key];
            for (const errorKey in this.approvalFormGroup.controls.isActiveRadioCtrl.errors) {
              if (errorKey) {
                this.formErrors[key] += messages[errorKey] + '\n';
              }
          }
        }
        if (!this.approvalFormGroup.controls.additionalcommentsCtrl.valid ){
          key ="additionalcommentsCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.approvalFormGroup.controls.additionalcommentsCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + '\n';
            }
        }
      }
  }



}
