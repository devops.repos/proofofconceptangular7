import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import {  FormGroup, FormBuilder } from '@angular/forms';
import { CommonService } from './../../../services/helper-services/common.service';
import { AgencySiteRequestReviewService } from './agency-site-request-review.service';
import { AgencySiteData, housingProgram,tADLiaison,siteRequestDemographic, UserOptions,siteRequestApproval,RentalSubsidy,UnitFeature } from "../agency-site-maintenance/agency-site-model"
import {AgencySiteRequestReviewModel} from '../agency-site-request/agency-site-request.model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { SiteDemographicRequestComponent } from './site-demographic.component';
import { SiteApprovalRequestComponent } from './site-approval.component';
import {SiteAgreementDetailRequestComponent } from './site-agreement-detail.component';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { SiteRequestAgreementPopulation } from "../agency-site-maintenance/agency-site-model"
import { RefGroupDetails } from './../../../models/refGroupDetailsDropDown.model';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import {SiteRequestService} from '../site-request/site-request.service';
import {PrimaryServiceContractRequestComponent} from '../site-request/primary-service-contract-request.component';
import {UnitDetailsRequestGridComponent} from '../site-request/unit-details-request-grid.component';
import {UnitRequest} from '../agency-site-request/agency-site-request.model';
import {SiteRequestAgreementPopulationData} from '../site-request/site-request.model';
import {SiteRequestEventService} from '../site-request/site-request-event.service';

@Component({
  selector: 'app-site-profile-request',
  templateUrl: './site-profile.component.html',
  styleUrls: ['./site-profile.component.scss'],
})
export class SiteProfileRequestComponent implements OnInit, OnDestroy {
  siteSelectedDetail:  AgencySiteData;
  siteDemographicdata : siteRequestDemographic;
  siteStatus : siteRequestDemographic;
  siteApprovalData :siteRequestApproval;
  siteAgreePopData: SiteRequestAgreementPopulation[];
  refGroupDetailsData : RefGroupDetails[];
  requestStatusTypes: RefGroupDetails[];
  housingProgramData:housingProgram[];
  tadLiaisons :tADLiaison[];
  siteRequestID: number;
  agencyRequestId: number;
  requestTypeID: number;
  siteFeaturedata :Array<number>;

  unitsDataObj: UnitRequest[];
  unitTabVisible:boolean =false;
  siteAgreePopRequestID:number =0;
  totalUnitsByAgreement:number =0;
  primaryserviceContractType:string='';
  pageNameString : string = '';
  
  useroptions: UserOptions ={
    RA : false,
    isPE : false,
    isHP : false,
    isIH : false,
    isDisableForHP:false,
    isDisableForInactiveSite:false,
    isdisableActiveRadiobutton:false,
    isActive  : false,
    optionUserId:0,
    isNewPage:false
};
 
  tabSelectedIndex: number = 0;
  userData: AuthData;

  userDataSub: Subscription;
  siteDemographicSub: Subscription;
  siteApprovalSub: Subscription;
  siteAgreementSub: Subscription;
  commonServiceSub: Subscription;
  evtSvcSub1: Subscription;
  evtSvcSub2: Subscription;
  evtSvcSub3: Subscription;
  evtSvcSub4: Subscription;
  evtSvcSub5: Subscription;
  siteRequestSvcSub1: Subscription;
  siteAdminSvcSub1: Subscription;
  siteAdminSvcSub2: Subscription;
  agencySiteReviewSaveSub: Subscription;
  agencySiteReviewSaveSub2: Subscription;
  agnySiteReqReviewSvcSub1: Subscription;
  agnySiteReqReviewSvcSub2: Subscription;

  // isDemoFormComplete:boolean = false;
  isApprovalFormComplete:boolean = false;
  siteRequestSiteDemographicsIsValid :boolean= false;
  siteRequestPrimaryServiceAgreementIsValid :boolean= false;
  showValidationFlag : boolean = true;

  siteProfileFormGroup: FormGroup;

  @ViewChild(SiteDemographicRequestComponent) siteDemographicComponent: SiteDemographicRequestComponent;
  @ViewChild(SiteAgreementDetailRequestComponent) siteAgreementComponent: SiteAgreementDetailRequestComponent;
  @ViewChild(PrimaryServiceContractRequestComponent) primaryServiceContractRequestComp : PrimaryServiceContractRequestComponent;
  @ViewChild(UnitDetailsRequestGridComponent) unitDetailsRequestGridComp : UnitDetailsRequestGridComponent;
  @ViewChild(SiteApprovalRequestComponent) siteApprovalComponent: SiteApprovalRequestComponent;

  siteRequestAgreementPopulationData : SiteRequestAgreementPopulationData[];
  
constructor(
  private router: Router,
  private formBuilder: FormBuilder,
  private agencySiteReviewSvc : AgencySiteRequestReviewService,
  private commonService : CommonService,
  private siteAdminSvc : SiteAdminService,
  private userService :UserService,
  private siteRequestSvc : SiteRequestService,
  private toastr: ToastrService, private siteRequestEventSvc : SiteRequestEventService,
  private activatedRoute: ActivatedRoute,
) {
  
  this.siteDemographicdata ={} as siteRequestDemographic;
  this.siteApprovalData={} as siteRequestApproval;
  this.siteAgreePopData = [];
  this.refGroupDetailsData = [];
  this.housingProgramData = [];
  this.tadLiaisons = [];
  this.requestStatusTypes = [];
  }

  ngOnInit() {
  
   this.siteProfileFormGroup = this.formBuilder.group({

    });
    let that = this;
    this.activatedRoute.paramMap.subscribe(params => {
        that.siteRequestID = Number(params.get('siteReqID'));
        that.agencyRequestId = Number(params.get('agencyReqID'));
        that.requestTypeID = Number(params.get('requestTypeID'));
    });
    if(this.requestTypeID === 320){
      this.pageNameString = 'New Agency/Site Approval';
    } else if(this.requestTypeID === 321){
      this.pageNameString = 'New Site Approval';
    } else if(this.requestTypeID === 322){
      this.pageNameString = 'New Site Agreement Approval';
    } 

    this.evtSvcSub2 = this.siteRequestEventSvc.subscribeSiteRequestSiteDemographics().subscribe(res => {
      if(res && res.has(this.siteRequestID)) { // siteRequestID..!!!! has??
        that.siteRequestSiteDemographicsIsValid = res.get(that.siteRequestID); // siteRequestID..!!!
      }
      else
        that.siteRequestSiteDemographicsIsValid = false;
    });
    this.evtSvcSub3 = this.siteRequestEventSvc.subscribeSiteRequestPrimaryServiceAgreement().subscribe(res => {
      if(res && res.has(this.siteRequestID)) { // siteRequestID..!!!! has??
        that.siteRequestPrimaryServiceAgreementIsValid = res.get(that.siteRequestID); // siteRequestID..!!!
      }
      else
        that.siteRequestPrimaryServiceAgreementIsValid = false;
    });
    this.evtSvcSub4 = this.siteRequestEventSvc.subscribeSiteRequestAgreementPopulationData().subscribe(res => {
        if(res && res.has(that.siteRequestID)) {
          that.siteRequestAgreementPopulationData = res.get(that.siteRequestID);
        }
    });

    this.loadRefGroupDetails();
    this.getUserRolesInfo();
    this.populateSiteInformation();
    this.loadHousingPrograms();
    this.LoadTADLiaisons();
  }

  getUserRolesInfo()
  {
      this.userDataSub = this.userService.getUserData().subscribe(res => {
        this.userData = res;
        if(this.userData) {
          if(this.userData.siteCategoryType.length > 0) {
            this.useroptions.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
            this.useroptions.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
            this.useroptions.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
            this.useroptions.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
            this.useroptions.optionUserId = this.userData.optionUserId;  
          }
          // if (this.siteSelectedDetail.siteStatus == "InActive")
          // {
          //   this.useroptions.isActive = false;
          //   this.useroptions.isDisableForInactiveSite=true;
          //   this.useroptions.isDisableForHP = true;
          //   if (this.useroptions.isIH == true) {
          //   this.useroptions.isdisableActiveRadiobutton=false;
          //   }
          //   else { 
          //     this.useroptions.isdisableActiveRadiobutton=true;
          //   }
          // }
          // else
          // {
            this.useroptions.isActive = true;
            if (this.useroptions.isIH == true) {
              this.useroptions.isDisableForInactiveSite=false;
              this.useroptions.isDisableForHP = false;
              this.useroptions.isdisableActiveRadiobutton=false;
            }
            else{
              this.useroptions.isDisableForInactiveSite=false;
              this.useroptions.isDisableForHP = true;
              this.useroptions.isdisableActiveRadiobutton=true;
            }
          // }

          }  
        }
      );
  }
 
  loadSiteApproval()
  {
      this.siteApprovalSub = this.agencySiteReviewSvc.getSiteApprovalData(this.siteRequestID)
          .subscribe(
            res1 => {
                    this.siteApprovalData = res1 as siteRequestApproval;
            },
            error => console.error('Error!', error)
          );
  }

  loadSiteDemographics()
   {
     let that = this;
      this.siteDemographicSub = this.agencySiteReviewSvc.getSiteRequestDemographics(that.siteRequestID)
        .subscribe(
          res1 => {
            that.siteDemographicdata = res1 as siteRequestDemographic;
            that.siteFeaturedata = this.convertStringToArray(that.siteDemographicdata.siteFeatures)
            // this.agencyRequestId=this.siteDemographicdata.agencyID;
          },
          error => console.error('Error!', error)
        );
  }

  loadSiteAgreementPopulation() {
    this.siteAgreementSub = this.agencySiteReviewSvc.getSiteAgreementProfileRequest(this.siteRequestID)
      .subscribe(
        res => {
          if (res) {
            this.siteAgreePopData = res as SiteRequestAgreementPopulation[];
          }
        },
        error => console.error('Error in retrieving the Primary Service Contract Data By Site ID...!', error)
      );
  }
   
   populateSiteInformation()
   {
      this.loadSiteDemographics();
      this.loadSiteApproval();
      this.loadSiteAgreementPopulation();
   }

  loadRefGroupDetails()
  {
    let value = "7,25,9,12,26,70";
    this.commonServiceSub = this.commonService.getRefGroupDetails(value)
            .subscribe(
              res => {
                const data = res.body as RefGroupDetails[];
                this.refGroupDetailsData = data;
                this.requestStatusTypes = data.filter(d => (d.refGroupID === 70));
              },
              error => console.error('Error while fetching getRefGroupDetails data.', error)
            );
  }

   loadHousingPrograms()
   {
    this.siteAdminSvcSub1 = this.siteAdminSvc.getHousingPrograms()
      .subscribe(
        res => {
          this.housingProgramData = res.body as housingProgram[];
        },
        error => console.error('Error!', error)
      );
   }

   LoadTADLiaisons() {
    this.siteAdminSvcSub2 =this.siteAdminSvc.getTADLiaison('8')
      .subscribe(
        res => {
          if (res.body) {
            this.tadLiaisons = res.body as tADLiaison[];
          }
        },
        error => console.error('Error in retrieving the TAD Laisions ...!', error)
      );
   }

convertStringToArray(s:string) : Array<number> {
  if (s != null)
  {
  var array = s.split(',').map(Number);
  return array;
  }
  else
  {
    return null;
  }

}

getmodifiedData(modifiedData)
{
  this.siteDemographicdata = modifiedData;
}

Save(approvedType: boolean)
{
  this.siteApprovalData.approvedType = ((approvedType === true) ? 
        this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Approved').refGroupDetailID 
      : this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Disapproved').refGroupDetailID);
  this.siteApprovalData.requestStatusType = ((approvedType === true) ? 
        this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Approved').refGroupDetailID 
      : this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Disapproved').refGroupDetailID);

  this.siteApprovalData.approverEmail = this.userData.email;
  this.siteApprovalData.userName = this.userData.firstName + ' ' + this.userData.lastName;
  this.siteApprovalData.userId = this.userData.optionUserId;

  this.siteDemographicdata.tadLiasion = this.siteApprovalData.tadLiasion;
  this.siteDemographicdata.userId = this.userData.optionUserId;
  this.siteDemographicdata.userName = this.userData.firstName + ' ' + this.userData.lastName;
  this.siteDemographicdata.siteRequestType = this.requestTypeID;

    let agencySiteRequestReviewModel: AgencySiteRequestReviewModel = {
      "siteRequestDemographic": this.siteDemographicdata,
      "siteRequestApproval": this.siteApprovalData,
      "siteRequestType" : this.requestTypeID
    }
    this.agencySiteReviewSaveSub = this.agencySiteReviewSvc.saveReviewedAgencySiteRequest(agencySiteRequestReviewModel).subscribe(
      data => {
        this.toastr.success('Successfully Saved !!', '');
        this.router.navigate(['/dashboard']);
      },
      error => {
        this.toastr.warning('Error while Saving !!', '');
      }
    );
}

approveClicked(event : Event){
  let that = this;
  this.validateForm();
  setTimeout(() => {

      if(that.IsFormDataValidInTabs()){
        let approvedType = this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Approved').refGroupDetailID;
        if(that.requestTypeID === 322){
            that.agencySiteReviewSaveSub2 = that.agencySiteReviewSvc.saveReviewedSiteAgreementPopulationRequest(this.siteRequestID, approvedType , this.userData.optionUserId).subscribe(
              data => {
                that.toastr.success('Successfully Saved !!', '');
                that.router.navigate(['/dashboard']);
              },
              error => {
                that.toastr.warning('Error while Saving !!', '');
              }
            );
        } else {
            that.Save(true);
        }
      } else {
          that.toastr.warning("Please check all the Tabs, and fill required fields.", "Validation Failed.");
      }
    }, 1200);

}

disApproveClicked(event : Event){
  let approvedType = this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Disapproved').refGroupDetailID;
  if(this.siteApprovalData && this.siteApprovalData.addlApproverComments && this.siteApprovalData.addlApproverComments.trim().length > 0){

      if(this.requestTypeID === 322){
        this.agencySiteReviewSaveSub2 = this.agencySiteReviewSvc.saveReviewedSiteAgreementPopulationRequest(this.siteRequestID, approvedType , this.userData.optionUserId).subscribe(
              data => {
                this.toastr.success('Successfully Saved !!', '');
                this.router.navigate(['/dashboard']);
                },
                error => {
                  this.toastr.warning('Error while Saving !!', '');
                }
              );
          } else {
                  this.Save(false);
          }
      } else{
          this.toastr.warning("Please enter additional comments to Disapprove the Request", "Validation Failed.");
      }
}

nextPage() {
  if(this.tabSelectedIndex != 2) // avoid calling twice in tabChanged. on TabIndex=2
    this.validateForm();
    this.showValidationFlag = false;

    if(this.requestTypeID === 322){
          if (this.tabSelectedIndex < 2) {
            this.tabSelectedIndex = this.tabSelectedIndex + 1;
          }
    } else {
          if (this.tabSelectedIndex == 0) {
            this.saveDemographicData();
          }
          if (this.tabSelectedIndex < 3) {
            this.tabSelectedIndex = this.tabSelectedIndex + 1;
          }
    }
}

// Previous button click
previousPage() {
  this.validateForm();
  this.showValidationFlag = false;

  if (this.tabSelectedIndex > 0) {
    this.tabSelectedIndex = this.tabSelectedIndex - 1;
  }
}

tabChanged(tabChangeEvent: MatTabChangeEvent): void {
  if (this.tabSelectedIndex == 0 && this.requestTypeID !== 322) {
    this.saveDemographicData();
  }
  this.tabSelectedIndex = tabChangeEvent.index;
  if(this.tabSelectedIndex == 1){
     this.loadSiteAgreementPopulation();
  }

  if(this.showValidationFlag){
    this.validateForm();
  }
  this.showValidationFlag = true; // reset

}

obsKeysToString(o, k, sep) {
  return k.map(key => o[key]).filter(v => v).join(sep);
}

saveDemographicData(){
  if(this.siteDemographicComponent)
      this.siteDemographicComponent.onSubmit();

  if (this.siteRequestSiteDemographicsIsValid)  {
    this.siteRequestSvcSub1 = this.siteRequestSvc.saveSiteRequestDemographics(this.siteDemographicdata)
                      .subscribe(
                        data => {
                          this.toastr.success("Successfully Saved", 'Save Success');
                          this.loadSiteDemographics();
                      },
                        error => {this.toastr.error("Save Failed", 'Save Failed');}
                      );
    }
}

  unitCloseClick(t:boolean)
  {
    if(this.requestTypeID === 322){
      this.tabSelectedIndex = 1;
      this.unitTabVisible= t;
    } else {
        this.unitTabVisible= t;
        this.tabSelectedIndex = 2;
        //  this.loadSiteStatus();
    }
  }

  validateForm(){
    let that = this;
    
      setTimeout(() => {
        if(that.siteDemographicComponent)
            that.siteDemographicComponent.onSubmit();
      }, 250);
      setTimeout(() => {
        if(that.siteApprovalComponent)
            that.isApprovalFormComplete = that.siteApprovalComponent.onSubmit();
      }, 500);
      setTimeout(() => {
        if(that.unitDetailsRequestGridComp)
            that.unitDetailsRequestGridComp.onSubmit();
      }, 750);
      setTimeout(() => {
        if(that.primaryServiceContractRequestComp)
            that.primaryServiceContractRequestComp.onSubmit();
      }, 1000);

      if(this.requestTypeID === 322){
          if(this.tabSelectedIndex == 1)
            this.showValidationMessage();
      } else {
          if(this.tabSelectedIndex == 2)
              this.showValidationMessage();
      }
  }

  showValidationMessage(){
    if(this.useroptions && this.useroptions.isIH === false){
      if((this.siteRequestAgreementPopulationData && this.siteRequestAgreementPopulationData.length == 0) || this.validateUnitRecords() == false){
        this.toastr.warning("Please check the Unit Roster", "Validation Failed.");
        return;
      }
    }

  }

  validateUnitRecords() : boolean{
    let siteRequestAgreementPopulationUnitsIsValid : boolean = true;
    if(this.useroptions && this.useroptions.isIH === true){
      return siteRequestAgreementPopulationUnitsIsValid; // RELAXATION rule for Internal CAS user.
    }
    if(this.siteRequestAgreementPopulationData == null || this.siteRequestAgreementPopulationData == undefined){
      return false;
    }
    let _siteRequestAgreementPopulationData = this.siteRequestAgreementPopulationData;
    if(this.siteRequestAgreementPopulationData && this.siteRequestAgreementPopulationData.length > 0){
      _siteRequestAgreementPopulationData = this.siteRequestAgreementPopulationData.filter(it => it.siteRequestID == this.siteRequestID);
    }

    if(_siteRequestAgreementPopulationData && _siteRequestAgreementPopulationData.length > 0){
      for(let item of _siteRequestAgreementPopulationData){
        if(item && item.totalUnits > item.totalUnitsDataEntered) {
          siteRequestAgreementPopulationUnitsIsValid = false;
          break;
        }
      }
    } else{
      siteRequestAgreementPopulationUnitsIsValid = false;
    }
    
    return siteRequestAgreementPopulationUnitsIsValid;
  }

  unitDetailOpenClick(param : SiteRequestAgreementPopulation)
  {
    /*alert (param.siteAgreementPopulationID);*/
    this.siteAgreePopRequestID = param.siteAgreementPopulationRequestID;
    this.primaryserviceContractType=param.primaryServiceAgreementPopName;
    this.totalUnitsByAgreement = param.totalUnits
    this.GetAllUnitsBySite();

    if(this.requestTypeID === 322){
      this.tabSelectedIndex = 1;
    } else {
      this.tabSelectedIndex = 2;
    }
    this.unitTabVisible = true;
  }

  // loadSiteStatus()
  // {
  //   this.agnySiteReqReviewSvcSub1 = this.agencySiteReviewSvc.getSiteRequestDemographics(this.siteRequestID)
  //   .subscribe(
  //     res1 => {
  //       this.siteStatus = res1 as siteRequestDemographic;
  //       this.setTabStatusColor();
  //       },
  //     error => console.error('Error!', error)
  //   );
  // }

  GetAllUnitsBySite()
   {
    this.agnySiteReqReviewSvcSub2 = this.agencySiteReviewSvc.getSiteRequestUnits(this.siteRequestID)
    .subscribe(
      res1 => {
        this.FormatUnitsDataObject(res1);  
        },
      error => console.error('Error!', error)
    );
   }   

/*   setTabStatusColor()
  {
    if (this.siteStatus != null) {
      if (this.siteStatus.isSiteDemographicTabComplete != null) {
        this.siteDemographicDataTabStatus =
          this.siteStatus.isSiteDemographicTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isSiteContactTabComplete != null) {
        this.siteContactTabStatus =
          this.siteStatus.isSiteContactTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isPrimaryServiceContractTabComplete != null) {
        this.sitePrimaryServiceTabStatus =
          this.siteStatus.isPrimaryServiceContractTabComplete == true ? 1 : 0;
      }
      if (this.siteStatus.isDraft != null) {
        this.siteDraftStatus = this.siteStatus.isDraft == true ? 0 : 1;
      }
      if (this.siteStatus.isSiteApprovalTabComplete != null) {
        this.siteApprovalTabStatus =
          this.siteStatus.isSiteApprovalTabComplete == true ? 1 : 0;
      }

      if (this.siteStatus.isUnitDetailTabComplete != null) {
        this.siteUnitDetailTabStatus =
          this.siteStatus.isUnitDetailTabComplete == true ? 1 : 0;
      }
    }
  } */
   
  FormatUnitsDataObject(res){
    let unitList = res as UnitRequest[];
    let newUnitList : UnitRequest[] = [];
    newUnitList = unitList.map(unit => {
      
      unit.unitFeatures = JSON.parse(unit.unitFeaturesJson);
      unit.unitFeaturesString = this.getFeaturesString(unit.unitFeatures);

      unit.rentalSubsidies = JSON.parse(unit.rentalSubsidyJson);
      unit.rentalSubsidyString = this.getSubsidiesString(unit.rentalSubsidies);
      
      return unit;
    });
  
    if (this.siteAgreePopRequestID !=0 ) {
      this.unitsDataObj = newUnitList.filter(d => (d.siteAgreementPopulationID=== this.siteAgreePopRequestID))
    }
      else {
      this.unitsDataObj = newUnitList; // massaged object for GRID
      }
      if  (this.unitsDataObj.length < this.totalUnitsByAgreement ) {
        this.unitTabVisible= true;
       }    
       else {   
       this.toastr.info("Can't add units", 'Unit details')
       }
  }

  getFeaturesString(UnitFeaturesArray : UnitFeature[]){
    let featuresString : string = null;

    if(UnitFeaturesArray) {
      UnitFeaturesArray.forEach(feature => {
        featuresString = featuresString ? (featuresString + ' ,' + feature.f_desc) : feature.f_desc;
      });
      return featuresString.substring(0, featuresString.length);
    }
    return ''; 
  }

  getSubsidiesString(rentSubsidyArray : RentalSubsidy[]){
    let subsidiesString : string = null;

    if(rentSubsidyArray) {
      rentSubsidyArray.forEach(subsidy => {
        subsidiesString = subsidiesString ? (subsidiesString + ' ,' + subsidy.sub_desc) : subsidy.sub_desc;
      });
      return subsidiesString.substring(0, subsidiesString.length);
    }
    return ''; 
  }

  IsFormDataValidInTabs() : boolean { // get

    let siteRequestAgreementPopulationUnitsIsValid : boolean = this.validateUnitRecords();
    
    if(this.requestTypeID === 322){
      if(this.siteRequestPrimaryServiceAgreementIsValid === true && siteRequestAgreementPopulationUnitsIsValid === true && this.isApprovalFormComplete === true){
        return true; // this.siteRequestSiteContactsIsValid && this.siteRequestSiteDemographicsIsValid
      }
      else
        return false;
    } else {
        if(this.siteRequestSiteDemographicsIsValid === true && this.siteRequestPrimaryServiceAgreementIsValid === true && siteRequestAgreementPopulationUnitsIsValid === true && this.isApprovalFormComplete === true){
          return true; // this.siteRequestSiteContactsIsValid && 
        }
        else
          return false;
    }
  }

  ngOnDestroy() {
    if(this.siteApprovalSub) this.siteApprovalSub.unsubscribe();
    if(this.siteDemographicSub) this.siteDemographicSub.unsubscribe();
    if(this.siteAgreementSub) this.siteAgreementSub.unsubscribe();
    if(this.userDataSub) this.userDataSub.unsubscribe();
    if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
    if(this.siteAdminSvcSub1) this.siteAdminSvcSub1.unsubscribe();
    if(this.siteAdminSvcSub2) this.siteAdminSvcSub2.unsubscribe();
    if(this.agencySiteReviewSaveSub) this.agencySiteReviewSaveSub.unsubscribe();
    if(this.agencySiteReviewSaveSub2) this.agencySiteReviewSaveSub2.unsubscribe();
    if(this.agnySiteReqReviewSvcSub1) this.agnySiteReqReviewSvcSub1.unsubscribe();
    if(this.agnySiteReqReviewSvcSub2) this.agnySiteReqReviewSvcSub2.unsubscribe();
    if(this.evtSvcSub1) this.evtSvcSub1.unsubscribe();
    if(this.evtSvcSub2) this.evtSvcSub2.unsubscribe();
    if(this.evtSvcSub3) this.evtSvcSub3.unsubscribe();
    if(this.evtSvcSub4) this.evtSvcSub4.unsubscribe();
    if(this.evtSvcSub5) this.evtSvcSub5.unsubscribe();
    if(this.siteRequestSvcSub1) this.siteRequestSvcSub1.unsubscribe();
    
  }

}