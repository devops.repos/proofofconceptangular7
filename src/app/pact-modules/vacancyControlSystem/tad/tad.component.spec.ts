import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TadComponent } from './tad.component';

describe('TadComponent', () => {
  let component: TadComponent;
  let fixture: ComponentFixture<TadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
