import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { TadActionComponent } from './tad-action.component';
import { TadNoSubmitActionComponent } from './tad-nosubmit-action.component';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { VCSCoCSiteList } from '../coc-referral-queue/coc-referral-queue.model';
import { CoCService } from '../coc-referral-queue/coc-referral-queue.service';
import { TadData, TadSite } from './tad.model'
import { CommonService } from '../../../services/helper-services/common.service';
import { UserSiteType } from 'src/app/models/pact-enums.enum';
import { TADService } from './tad.service';
import { ToastrService } from "ngx-toastr";
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { TadService } from '../tad-submission/tad.service';
import { Router } from '@angular/router';
import { PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { IVCSHpAgenciesWithTrackedSites  } from 'src/app/pact-modules/vacancyControlSystem/tenant-roster/tenant-roster-interface.model';
import { TenantRosterService } from 'src/app/pact-modules/vacancyControlSystem/tenant-roster/tenant-roster.service';


@Component({
  selector: 'app-tad',
  templateUrl: './tad.component.html',
  styleUrls: ['./tad.component.scss']
})
export class TadComponent implements OnInit, OnDestroy {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('agGrid1') agGrid1: AgGridAngular;


  userDataSub: Subscription;
  siteListSub: Subscription;
  agencyListSub: Subscription;

  siteList: VCSCoCSiteList[];
  agencyList: IVCSHpAgenciesWithTrackedSites[];
  //hpAgenciesWithTrackedSites: IVCSHpAgenciesWithTrackedSites[];

  selectedSiteID: number = 0;
  selectedAgencyID: number = 0;
  TAD: TadData;
  TADList: TadSite[];

  is_SH_HP = false;
  is_CAS = false;
  canSubmit = false;

  gridApi;
  gridColumnApi;
  gridApi1;
  gridColumnApi1;

  columnDefs;
  transmitColumnDefs;
  //columnDefs1;
  defaultColDef;
  noSubmitColDef;
  pagination;
  context;
  autoGroupColumnDef;
  isRowSelectable;
  frameworkComponents;
  frameworkComponents1;

  overlayNoRowsTemplate;
  overlayLoadingTemplate;

  userData: AuthData;

  public gridOptions: GridOptions;
  public gridOptions1: GridOptions;
  // rowData: GridData[];
  pendingTAD: TadSite[] = [];
  transmittedTAD: TadSite[] = [];

  reportParams: PACTReportUrlParams;

  constructor(
    private userService: UserService,
    private cocService: CoCService,
    private commonService: CommonService,
    private tadService: TADService,
    private tadSubmissionService: TadService,
    private router: Router,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private tenantRosterService: TenantRosterService,
  ) {

    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the TAD list is loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No TADs Available</span>';
    this.gridOptions = {
      rowHeight: 35,
    } as GridOptions;
    this.gridOptions1 = {
      rowHeight: 35,
    } as GridOptions;

    this.columnDefs = [
      {
        headerName: 'Site No/Site Name',
        colId: 'siteName',
        width: 300,
        filter: 'agTextColumnFilter',
        suppressSizeToFit: true,
        valueGetter: function (params) {
          return params.data.siteNo + '/' + params.data.siteName;
        }
      },
      {
        headerName: 'Reporting Month',
        filter: 'agTextColumnFilter',
        field: 'tadMonthName',
      },
      {
        headerName: 'Reporting Year',
        field: 'tadYear',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Updated By',
        filter: 'agTextColumnFilter',
        valueGetter: function (params) {
          return params.data.updatedBy ? params.data.updatedBy:"";
        }
      },
      {
        headerName: 'Updated Date',
        filter: 'agDateColumnFilter',
        valueGetter: function (params) {
          return params.data.updatedDate?params.data.updatedDate:"";
        },
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Status',
        field: 'tadStatusName',
        filter: 'agTextColumnFilter',
        cellStyle: function (params) {
          if (params.data.tadStatusType == 809) {
            //mark overdue cells as red
            return { color: 'red' };
          } else if (params.data.tadStatusType == 810 || params.data.tadStatusType == 811) {
            //mark overdue cells as orange
            return { color: '#FF9900' };
          } else if (params.data.tadStatusType == 812) {
            //mark overdue cells as green
            return { color: 'green' };
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];

    this.noSubmitColDef = [
      {
        headerName: 'Site No/Site Name',
        colId: 'siteName',
        width: 300,
        filter: 'agTextColumnFilter',
        suppressSizeToFit: true,
        valueGetter: function (params) {
          return params.data.siteNo + '/' + params.data.siteName;
        }
      },
      {
        headerName: 'Reporting Month',
        filter: 'agTextColumnFilter',
        field: 'tadMonthName',
      },
      {
        headerName: 'Reporting Year',
        field: 'tadYear',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Updated By',
        filter: 'agTextColumnFilter',
        valueGetter: function (params) {
          return params.data.updatedBy?params.data.updatedBy:"";
        }
      },
      {
        headerName: 'Updated Date',
        filter: 'agDateColumnFilter',
        valueGetter: function (params) {
          return params.data.updatedDate?params.data.updatedDate:"";
        },
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Status',
        field: 'tadStatusName',
        filter: 'agTextColumnFilter',
        cellStyle: function (params) {
          if (params.data.tadStatusType == 809) {
            //mark overdue cells as red
            return { color: 'red' };
          } else if (params.data.tadStatusType == 810 || params.data.tadStatusType == 811) {
            //mark overdue cells as orange
            return { color: '#FF9900' };
          } else if (params.data.tadStatusType == 812) {
            //mark overdue cells as green
            return { color: 'green' };
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        //cellRenderer: 'noSubmit',
        cellRenderer(params) {
          const actionButton = {
            component: 'noSubmit'
          };
          if (params.data.tadStatusType == 811 || params.data.tadStatusType == 812) {
            return actionButton;
          } else {
            return null;
          }
        }

      }
    ];

    this.transmitColumnDefs = [
      {
        headerName: 'Site No/Site Name',
        colId: 'siteName',
        width: 300,
        filter: 'agTextColumnFilter',
        suppressSizeToFit: true,
        valueGetter: function (params) {
          return params.data.siteNo + '/' + params.data.siteName;
        }
      },
      {
        headerName: 'Reporting Month',
        filter: 'agTextColumnFilter',
        field: 'tadMonthName',
      },
      {
        headerName: 'Reporting Year',
        field: 'tadYear',
        filter: 'agTextColumnFilter'
      },
      // {
      //   headerName: 'Updated By',
      //   filter: 'agTextColumnFilter',
      //   valueGetter: function (params) {
      //     return params.data.updatedBy ? params.data.updatedBy:"";
      //   }
      // },
      // {
      //   headerName: 'Updated Date',
      //   filter: 'agDateColumnFilter',
      //   valueGetter: function (params) {
      //     return params.data.updatedDate?params.data.updatedDate:"";
      //   },
      //   filterParams: {
      //     // filterOptions: ['equals','inRange'],
      //     comparator(filterLocalDateAtMidnight, cellValue) {
      //       const dateAsString = cellValue;
      //       if (dateAsString == null) { return -1; }
      //       const dateParts = dateAsString.split('/');
      //       const day = Number(dateParts[1]);
      //       const month = Number(dateParts[0]) - 1;
      //       const year = Number(dateParts[2]);
      //       // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
      //       const cellDate = new Date(year, month, day);
      //       if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
      //         return 0;
      //       }
      //       if (cellDate < filterLocalDateAtMidnight) {
      //         return -1;
      //       }
      //       if (cellDate > filterLocalDateAtMidnight) {
      //         return 1;
      //       }
      //     },
      //     browserDatePicker: true
      //   },
      // },
      {
        headerName: 'Submitted By',
        filter: 'agTextColumnFilter',
        valueGetter: function (params) {
          return params.data.submittedBy ? params.data.submittedBy:"";
        }
      },
      {
        headerName: 'Submitted Date',
        filter: 'agDateColumnFilter',
        valueGetter: function (params) {
          return params.data.submittedDate ? params.data.submittedDate:"";
        },
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      {
        headerName: 'Status',
        field: 'tadStatusName',
        filter: 'agTextColumnFilter',
        cellStyle: function (params) {
          if (params.data.tadStatusType == 809) {
            //mark overdue cells as red
            return { color: 'red' };
          } else if (params.data.tadStatusType == 810 || params.data.tadStatusType == 811) {
            //mark overdue cells as orange
            return { color: '#FF9900' };
          } else if (params.data.tadStatusType == 812) {
            //mark overdue cells as green
            return { color: 'green' };
          } else {
            return null;
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];


    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };

    this.context = { componentParent: this };
    this.pagination = true;
    this.frameworkComponents = {
      actionRenderer: TadActionComponent,
      noSubmit: TadNoSubmitActionComponent
    };
  }

  ngOnInit() {
    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.userData = userdata;
        // console.log(this.userData);
        // console.log(this.commonService._doesValueExistInJson(this.userData.userFunctions, 37));
        if (this.userData.siteCategoryType.length > 0) {
          this.is_SH_HP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.SH_HP);
          this.is_CAS = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, UserSiteType.CAS);
          // this.is_CAS = true;
        }

        if (this.is_SH_HP) {
          this.canSubmit = this.commonService._doesValueExistInJson(this.userData.userFunctions, 37);
          this.selectedAgencyID = this.userData.agencyId;
          this.siteListSub = this.cocService.getUserSite(this.userData.optionUserId).subscribe(res => {
            if (res) {
              this.siteList = res as VCSCoCSiteList[];
              this.siteList = this.siteList.filter( res => { return res.siteCategoryType == 7 && res.siteTrackedType == 33})
            }
          });
        }

        if (this.is_CAS) {
          this.canSubmit = true;
          this.agencyListSub = this.tenantRosterService.getVCSHpAgenciesWithTrackedSites().subscribe(res => {
            if (res) {
              this.agencyList = res as IVCSHpAgenciesWithTrackedSites[];
            }
          });
        }
      }
    });
    setTimeout(() => {
      if (this.selectedAgencyID > 0) {
        this.onGo();
      }
    }, 1000);
  }

  onGo() {
    if (this.selectedAgencyID > 0) {
      this.tadService.getTADListbySite(this.selectedAgencyID.toString(), this.selectedSiteID.toString(), '1').subscribe(res => {
        if (res) {

          this.TAD = res as TadData;
          this.TADList = this.TAD.tadList;

          this.pendingTAD = this.TADList.filter(d => { return d.tadStatusType == 808 || d.tadStatusType == 809 || d.tadStatusType == 810 })
          this.transmittedTAD = this.TADList.filter(d => { return d.tadStatusType == 811 || d.tadStatusType == 812 })
          this.transmittedTAD = this.transmittedTAD.sort((a, b) => b.submittedDate.localeCompare(a.submittedDate));
        }
      });
    }
    else {
      this.toastr.error("Please select Agency to proceed.", "");
    }
  }

  onAgencySelected() {
    if (this.selectedAgencyID != 0) {
      this.siteListSub = this.cocService.getCoCSiteList(this.selectedAgencyID).subscribe(res => {
        if (res) {
          this.siteList = res as VCSCoCSiteList[];
          this.siteList = this.siteList.filter( res => { return res.siteCategoryType == 7 && res.siteTrackedType == 33})
        }
      });
    }
    else {
      this.siteList = [];
    }
    this.selectedSiteID = 0;
  }

  onSelectedSubmit(cell: TadSite) {
    var selectedTadDate = new Date(cell.tadYear, cell.tadMonth - 1, 1);
    var prevTadNotSubmittedDates = [];
    var selectedSiteTads: TadSite[];

    selectedSiteTads = this.pendingTAD.filter(d => { return d.siteID == cell.siteID });
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    selectedSiteTads.forEach(d => {
      var pendingTadDate = new Date(d.tadYear, d.tadMonth - 1, 1);
      if (pendingTadDate < selectedTadDate) {
        prevTadNotSubmittedDates.push(pendingTadDate);
      }
    });

    if (prevTadNotSubmittedDates.length > 0) {
      const minDate = new Date(Math.min(...prevTadNotSubmittedDates));
      const title = 'Warning';
      const primaryMessage = '';
      const secondaryMessage = "Please submit the TAD for " + monthNames[minDate.getMonth()] + ' ' + minDate.getFullYear() + " before submitting this TAD.";
      const confirmButtonName = 'OK';
      const dismissButtonName = '';

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          (positiveResponse) => { },
          (negativeResponse) => {},
        );
    }
    else {
      this.tadSubmissionService.setTadSelectedHpSiteID(cell);
      this.router.navigate(['/vcs/tad/tad-submission']);
    }
  }

  onSelectedVerified(cell: TadSite) {
    this.reportParams = { reportParameterID: cell.verifiedTADReportID, reportName: 'VerifiedTADReport', "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.commonService.OpenWindow(URL.createObjectURL(data));
            this.commonService.setIsOverlay(false);
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }
  onSelectedTransmit(cell: TadSite) {
    this.reportParams = { reportParameterID: cell.transmittedTADReportID, reportName: 'TransmittedTADReport', "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.commonService.OpenWindow(URL.createObjectURL(data));
            this.commonService.setIsOverlay(false);
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
  onFirstDataRendered1(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    /*  if(!this.canSubmit){
       this.gridColumnApi.setColumnVisible('action', false)
     } */

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action' && column.colId !== 'siteName') {
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, true);
    this.gridApi.sizeColumnsToFit(allColumnIds);

  }

  onGridReady1(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action' && column.colId !== 'siteName') {
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, true);
    this.gridApi.sizeColumnsToFit(allColumnIds);

  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'referralReport-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };

    // console.log('referralReport-' + date + '-' + time);
    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.siteListSub) {
      this.siteListSub.unsubscribe();
    }
    if (this.agencyListSub) {
      this.agencyListSub.unsubscribe();
    }
  }
}

