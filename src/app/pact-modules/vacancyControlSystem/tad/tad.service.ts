import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
  })

  export class TADService {

    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    constructor(private httpClient: HttpClient) { }

     getTADListbySite(aID: string, sID: string, ttype: string) {
      const getUrl = environment.pactApiUrl + 'VCSTAD/GetTADListbySite/';
      return this.httpClient.get(getUrl, { params: { agencyID: aID, siteID: sID, tadType: ttype }});
    }


    /* saveUserData(user: UserData){
      const postUrl = environment.pactApiUrl + 'UserAdministration/SaveUserData/';
      return this.httpClient.post(postUrl, user, this.httpOptions);
    } */
  }
