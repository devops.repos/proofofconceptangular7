import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';


@Component({
  selector: 'ag-tad-nosubmit-action',
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="userActionNoSubmit"
    >
      more_vert
    </mat-icon>
    <mat-menu #userActionNoSubmit="matMenu">
      
      <button mat-menu-item (click)="onVerifiedTAD()" >HRA Verified TAD </button>
      <button mat-menu-item (click)="onTransmittedTAD()" >Transmitted TAD </button>
    </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})

export class TadNoSubmitActionComponent {
  params: any;
  public cell: any;

  constructor() {}


  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onVerifiedTAD() {
    this.params.context.componentParent.onSelectedVerified(this.params.data);
  }

  onTransmittedTAD() {
    this.params.context.componentParent.onSelectedTransmit(this.params.data);
  }
}



