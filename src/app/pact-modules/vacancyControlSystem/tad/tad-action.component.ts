import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';


@Component({
  selector: 'ag-tad-action',
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="userAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #userAction="matMenu">
      <button mat-menu-item (click)="onSubmit()" *ngIf="params.data.tadStatusType == 808 || params.data.tadStatusType == 809 || params.data.tadStatusType == 810">Submit TAD </button>
      <button mat-menu-item (click)="onVerifiedTAD()" *ngIf="params.data.tadStatusType == 812">HRA Verified TAD </button>
      <button mat-menu-item (click)="onTransmittedTAD()" *ngIf="params.data.tadStatusType == 811 || params.data.tadStatusType == 812">Transmitted TAD </button>
    </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})

export class TadActionComponent {
  params: any;
  public cell: any;

  constructor() {}


  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onSubmit() {
    this.params.context.componentParent.onSelectedSubmit(this.params.data);
  }

  onVerifiedTAD() {
    this.params.context.componentParent.onSelectedVerified(this.params.data);
  }

  onTransmittedTAD() {
    this.params.context.componentParent.onSelectedTransmit(this.params.data);
  }
}



