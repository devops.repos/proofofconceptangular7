export interface TadData {
  totalUnits: number;
  occupied: number;
  online: number;
  offline: number;
  moveIn: number;
  moveOut: number;
  notSubmittedOverdue: number;
  inProgress: number;
  pending: number;
  verificationOverdue: number;
  sysVerified: number;
  verified: number;
  tadList: TadSite[];
}

export interface TadSite {
  vcsTadID: number;
  agencyID: number;
  agencyNo: string;
  agencyName: string;
  siteID: number;
  siteNo: string;
  siteName: string;
  tadMonth: number;
  tadMonthName: string;
  tadYear: number;
  tadStatusType: number;
  tadStatusName: string;
  tadLiasion: number;
  tadLiasionFirstName: string;
  tadLiasionLastName: string;
  tadLiasionPhone: string;
  tadLiasionEmail: string;
  isUnitRosterComplete: boolean;
  isReferralRosterComplete: boolean;
  isTenantRosterComplete: boolean;
  createdBy: string;
  createdByID: number;
  createdDate: string;
  updatedBy: string;
  updatedByID: number;
  updatedDate: string;
  submittedBy: string;
  submittedByID: number;
  submittedDate: string;
  verifiedBy: string;
  verifiedByID: number;
  verifiedDate: string;
  transmittedTADReportID: string;
  verifiedTADReportID: string;
  housingLiasionList?: TADHousingLiasion[];
}

export interface TADHousingLiasion {
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  siteID: number;
}

