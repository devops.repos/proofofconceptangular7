import { Component, OnInit,ViewChild, OnDestroy, } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { CommonService } from '../../../services/helper-services/common.service';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import { AgencyData,SiteContact,siteRequestDemographic, UserOptions,siteApproval,RentalSubsidy,UnitFeature } from '../agency-site-maintenance/agency-site-model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { AgencySiteRequestReviewService } from '../agency-site-request-review/agency-site-request-review.service';
import { SiteDemographicRequestComponent } from '../agency-site-request-review/site-demographic.component';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import {UnitDetailsRequestGridComponent} from '../site-request/unit-details-request-grid.component';
import {SiteRequestService} from '../site-request/site-request.service';
import {UnitRequest} from '../agency-site-request/agency-site-request.model';
import {SiteRequestAgreementPopulation} from '../agency-site-maintenance/agency-site-model';
import {PrimaryServiceContractRequestComponent} from '../site-request/primary-service-contract-request.component';
import {SiteRequestAgreementPopulationData} from '../site-request/site-request.model';
import {SiteRequestEventService} from '../site-request/site-request-event.service';


@Component({
  selector: 'app-site-agreement-request',
  templateUrl: './site-agreement-request.component.html',
  styleUrls: ['./site-agreement-request.component.scss']
})
export class SiteAgreementRequestComponent implements OnInit, OnDestroy {
  sitecreated : siteRequestDemographic[]=[];
  isSaveBtnRequired : boolean = false;
  isNextBtnRequired : boolean = true;
  isPrevBtnRequired : boolean = false;
 
  siteDemographicdata : siteRequestDemographic;
  siteStatus : siteRequestDemographic;
  siteApprovaldata :siteApproval;
  agencyInfo:AgencyData;
  siteRequestID: number;
  agencyId:number;
  agencyList:AgencyData[];
  siteContacts:SiteContact[];
  siteFeatureData :Array<number>;
  allcontacts :string;
  siteAgreePopRequestID:number =0;
  primaryserviceContractType:string='';
  unitsDataObj: UnitRequest[];
  totalUnitsByAgreement:number =0;
  
  useroptions: UserOptions ={
  RA : false,
    isPE : false,
    isHP : false,
    isIH : false,
    isDisableForHP:false,
    isDisableForInactiveSite:false,
    isdisableActiveRadiobutton:false,
    isActive  : false,
    optionUserId:0,
    isNewPage:false
};
 
  tabSelectedIndex: number = 0;
  tabSiteSelectedIndex:number=0;
  userData: AuthData;

  userDataSub: Subscription;
  agnySiteReqReviewSvcSub1: Subscription;
  agnySiteReqReviewSvcSub2: Subscription;
  agnySiteReqReviewSvcSub3: Subscription;
  agnySiteReqReviewSvcSub4: Subscription;
  evtSvcSub4: Subscription;
  evtSvcSub5: Subscription;
  evtSvcSub6: Subscription;

  siteReqSvcSub1: Subscription;
  siteReqSvcSub2: Subscription;
  siteReqSvcSub3: Subscription;
  siteAdminSvcSub: Subscription;

  isSiteTabVisible :boolean= true ;
  unitTabVisible:boolean =false;
  submitted : boolean = false; 
  isSaveRequired:boolean= true;
  siteDemographicDataTabStatus = 2;
  siteContactTabStatus = 2;
  siteDraftStatus = 2;
  siteApprovalTabStatus = 2
  sitePrimaryServiceTabStatus = 2;
  siteUnitDetailTabStatus = 2;
  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;
  siteRequestPrimaryServiceAgreementIsValid :boolean= false ;

  siteRequestAgreementPopulationData : SiteRequestAgreementPopulationData[];

  @ViewChild(SiteDemographicRequestComponent) sitedemographicComponent: SiteDemographicRequestComponent;
  @ViewChild(PrimaryServiceContractRequestComponent) primaryServiceContractRequestComp : PrimaryServiceContractRequestComponent;
  @ViewChild(UnitDetailsRequestGridComponent) unitDetailsRequestGridComp : UnitDetailsRequestGridComponent;
  
constructor(
  private agnySiteReqReviewSvc: AgencySiteRequestReviewService,
  private router: Router,
  private siteAdminSvc : SiteAdminService, private activatedRoute: ActivatedRoute,
  private commonService : CommonService, private siteRequestSvc : SiteRequestService,
  private userService :UserService, private siteRequestEventSvc : SiteRequestEventService,
  private toastr: ToastrService
) {
  
  this.siteDemographicdata ={} as siteRequestDemographic;
  this.agencyInfo ={} as AgencyData;
}

ngOnInit() {
      let that = this;
      this.activatedRoute.queryParams.subscribe(params => {
            that.siteRequestID = Number(params.siteRequestID);
            if(that.siteRequestID > 0)
              that.populateSiteInformation();
            that.tabSelectedIndex = 2;
      });
      this.evtSvcSub4 = this.siteRequestEventSvc.subscribeSiteRequestPrimaryServiceAgreement().subscribe(res => {
        if(res && res.has(this.siteRequestID)) { // siteRequestID..!!!! has??
          that.siteRequestPrimaryServiceAgreementIsValid = res.get(that.siteRequestID); // siteRequestID..!!!
        }
        else
          that.siteRequestPrimaryServiceAgreementIsValid = false;
      });
      this.evtSvcSub5 = this.siteRequestEventSvc.subscribeSiteRequestAgreementPopulationData().subscribe(res => {
        if(res && res.has(that.siteRequestID)) {
          that.siteRequestAgreementPopulationData = res.get(that.siteRequestID);
        }
      });

      this.getUserRolesInfo();
  }

getUserRolesInfo()
{
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if(this.userData) {
        if(this.userData.siteCategoryType.length > 0) {
          this.useroptions.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
          this.useroptions.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
          this.useroptions.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
          this.useroptions.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
          this.useroptions.optionUserId = this.userData.optionUserId;  
        }
      
          this.useroptions.isActive = true;
          if (this.useroptions.isIH == true) {
            this.useroptions.isDisableForInactiveSite=false;
            this.useroptions.isDisableForHP = false;
            this.useroptions.isdisableActiveRadiobutton=false;
          }
          else{
            this.useroptions.isDisableForInactiveSite=false;
            this.useroptions.isDisableForHP = false;
            this.useroptions.isdisableActiveRadiobutton=true;
          }
      
        }  
      }
    );
}

   loadSiteContacts()
   {
     if (this.siteRequestID > 0) {
      this.agnySiteReqReviewSvcSub1 = this.agnySiteReqReviewSvc.getSiteRequestContacts(this.siteRequestID)
        .subscribe(
          res => {
            this.siteContacts= res as SiteContact[];
            //this.getallcontacts();
          },
          error => console.error('Error!', error)
        );     
   }
  }

  get IsFormDataValidInTabs() : boolean {
      if(this.siteRequestAgreementPopulationData == null || this.siteRequestAgreementPopulationData == undefined || this.siteRequestAgreementPopulationData.length == 0)
        return false;

      let siteRequestAgreementPopulationUnitsIsValid : boolean = this.validateUnitRecords();

      if(this.siteRequestPrimaryServiceAgreementIsValid && siteRequestAgreementPopulationUnitsIsValid){
        return true; // this.siteRequestSiteContactsIsValid && this.siteRequestSiteDemographicsIsValid && 
      }
      else
        return false;
  }

  validateForm(){
      // if(this.sitedemographicComp) this.sitedemographicComp.onSubmit();
      // if(this.siteContactRequestComp) this.siteContactRequestComp.onSubmit();
      if(this.primaryServiceContractRequestComp) this.primaryServiceContractRequestComp.onSubmit();
      if(this.unitDetailsRequestGridComp) this.unitDetailsRequestGridComp.onSubmit();
      if(this.tabSelectedIndex == 2)
        this.showValidationMessage();
  }

  showValidationMessage(){
    if((this.siteRequestAgreementPopulationData && this.siteRequestAgreementPopulationData.length == 0) || this.validateUnitRecords() == false){
      this.toastr.warning("Unit details are mandatory, please complete the unit roster to proceed.", "Validation Error Message.");
      return;
    }
  }

  validateUnitRecords() : boolean{
    let siteRequestAgreementPopulationUnitsIsValid : boolean = true;

    if(this.siteRequestAgreementPopulationData == null || this.siteRequestAgreementPopulationData == undefined){
      return false;
    }
    let _siteRequestAgreementPopulationData = this.siteRequestAgreementPopulationData;
    if(this.siteRequestAgreementPopulationData && this.siteRequestAgreementPopulationData.length > 0){
      _siteRequestAgreementPopulationData = this.siteRequestAgreementPopulationData.filter(it => it.siteRequestID == this.siteRequestID);
    }

    if(_siteRequestAgreementPopulationData && _siteRequestAgreementPopulationData.length > 0){
      for(let item of _siteRequestAgreementPopulationData){
        if(item && item.totalUnits > item.totalUnitsDataEntered){
          siteRequestAgreementPopulationUnitsIsValid = false;
          break;
        }
      }
    } else{
      siteRequestAgreementPopulationUnitsIsValid = false;
    }

    return siteRequestAgreementPopulationUnitsIsValid;
  }

   loadSiteDemographics()
   {
    this.agnySiteReqReviewSvcSub2 = this.agnySiteReqReviewSvc.getSiteRequestDemographics(this.siteRequestID)
        .subscribe(
            res1 => {
              this.siteDemographicdata = res1 as siteRequestDemographic;
              this.siteFeatureData = this.convertStringToArray(this.siteDemographicdata.siteFeatures)
              this.agencyId=this.siteDemographicdata.agencyID;
              //this.sitedemographicComponent.populateDemographicdata();
              this.siteAdminSvcSub = this.siteAdminSvc.getAgencyList(this.agencyId.toString(),null)
                .subscribe(
                  res2 => {
                    this.agencyList = res2.body as AgencyData[];
                    this.agencyInfo=this.agencyList.find(e => e.agencyID ===this.agencyId);
                  },
                  error => console.error('Error!', error)
                );
            },
            error => console.error('Error!', error)
        );
   }
   
   populateSiteInformation()
   {
    this.loadSiteDemographics();
/*     this.siteReqSvcSub1 = this.siteReqSvc.getSitecreated().subscribe(res => {
      this.sitecreated = res;
        if(this.sitecreated) {
            this.siteRequestID=this.sitecreated[0].siteRequestID;
            if (this.sitecreated[0].name === null)
            {
              this.isSiteTabVisible = false;
            }
            this.getUserRolesInfo();
            this.loadSiteDemographics();
            this.loadSiteContacts();
            //this.GetAllUnitsBySite();
          
            if (this.isSiteTabVisible == false)
            {
              this.tabSelectedIndex= 2;

            }
            this.loadSiteStatus();
      }
        }); */
  }


convertStringToArray(s:string) : Array<number> {
  if (s != null)
  {
  var array = s.split(',').map(Number);
  return array;
  }
  else
  {
    return null;
  }
}

setTabStatusColor()
{
  if (this.siteStatus != null)
  {
    if (this.siteStatus.isSiteDemographicTabComplete != null) {
      this.siteDemographicDataTabStatus = this.siteStatus.isSiteDemographicTabComplete == true ? 1 : 0;
    }
    if (this.siteStatus.isSiteContactTabComplete != null) {
      this.siteContactTabStatus = this.siteStatus.isSiteContactTabComplete == true ? 1 : 0;
    }
    if (this.siteStatus.isPrimaryServiceContractTabComplete != null) {
      this.sitePrimaryServiceTabStatus = this.siteStatus.isPrimaryServiceContractTabComplete == true ? 1 : 0;
    }
    if (this.siteStatus.isDraft != null) {
      this.siteDraftStatus = this.siteStatus.isDraft == true ? 0 : 1;
    }
    if (this.siteStatus.isSiteApprovalTabComplete != null) {
      this.siteApprovalTabStatus = this.siteStatus.isSiteApprovalTabComplete == true ? 1 : 0;
    }

    if (this.siteStatus.isUnitDetailTabComplete != null) {
      this.siteUnitDetailTabStatus = this.siteStatus.isUnitDetailTabComplete == true ? 1 : 0;
    }
    
  } 

}


loadSiteStatus()
   {
    this.agnySiteReqReviewSvcSub3 = this.agnySiteReqReviewSvc.getSiteRequestDemographics(this.siteRequestID)
    .subscribe(
      res1 => {
        this.siteStatus = res1 as siteRequestDemographic;
        this.setTabStatusColor();
        },
      error => console.error('Error!', error)
    );
   }


getmodifiedData(modifiedData)
{
  this.siteDemographicdata = modifiedData;
}

OnSave(event : Event)
{
  let requestStatusType = 568; // UPDATE THE STATUS FROM DRAFT TO  pending approval
  this.siteReqSvcSub3 = this.siteRequestSvc.saveSiteRequestStatus(this.siteRequestID, requestStatusType, this.userData.optionUserId)
      .subscribe(
          data => {
            this.toastr.success("Successfully Saved", 'Save Success');
            this.router.navigate(['/dashboard']);
        },
        error => {this.toastr.error("Save Failed", 'Save Failed');}
  );

}

saveDemographicData(){
  // var sitedemographicValidation =this.sitedemographicComponent.onSubmit();
  // if (sitedemographicValidation == true)  {
  //   this.siteReqSvcSub2 = this.siteReqSvc.saveSiteRequestDemographics(this.siteDemographicdata)
  //                     .subscribe(
  //                       data => {this.toastr.success("Successfully Saved", 'Save Success');
  //                    this.loadSiteDemographics();
  //                    this.tabSelectedIndex=1;
  //                     },
  //                       error => {this.toastr.error("Save Failed", 'Save Failed');}
  //                     );
  //   }
}

nextPage() {
      // if (this.tabSelectedIndex  == 0 ) {
      //     this.saveDemographicData();
      // }
      if (this.tabSelectedIndex < 2) {
        this.tabSelectedIndex = this.tabSelectedIndex + 1;
      }
      this.showHideButton();
      this.validateForm();
}

// Previous button click
previousPage() {
  
  if (this.tabSelectedIndex != 0) {
    this.tabSelectedIndex = this.tabSelectedIndex - 1;
  } else if (this.tabSelectedIndex == 0) {
    //this.sidenavStatusService.routeToPreviousPage(this.router, this.activatedRoute);
  }
  this.showHideButton();
  this.validateForm();
}

tabChanged(tabChangeEvent: MatTabChangeEvent): void {
  // /* Saving Data Between Tab Navigation */
  // if (this.dvGroup.touched || this.dvGroup.dirty) {
  //   this.onSave(false);
  // }
  this.tabSelectedIndex = tabChangeEvent.index;
  this.loadSiteStatus();
  this.showHideButton();
  this.validateForm();
}

tabSiteChanged(tabsiteChangeEvent: MatTabChangeEvent): void {
 
  this.tabSiteSelectedIndex = tabsiteChangeEvent.index;
    if (this.sitecreated.length > 0)
    {
        this.siteRequestID=this.sitecreated[tabsiteChangeEvent.index].siteRequestID;
            // this.getUserRolesInfo();
            this.loadSiteDemographics();
            this.loadSiteContacts();
            this.loadSiteStatus();
            this.tabSelectedIndex=0;
    }
}

getallcontacts()
{
  var keys = ['firstName', 'lastName'];
   this.allcontacts = this.obsKeysToString(this.siteContacts,keys,',');
}

obsKeysToString(o, k, sep) {
  return k.map(key => o[key]).filter(v => v).join(sep);
 }


  showInfo(s:number)
  {
            this.siteRequestID=s;
            // this.getUserRolesInfo();
            this.loadSiteDemographics();
            this.loadSiteContacts();
  }

  unitCloseClick(t:boolean)
  {
    this.unitTabVisible= t;
    this.tabSelectedIndex = 2;
    this.loadSiteStatus();
  }


unitDetailOpenClick(param : SiteRequestAgreementPopulation)
{
  /*alert (param.siteAgreementPopulationID);*/
  this.siteAgreePopRequestID = param.siteAgreementPopulationRequestID;
  this.primaryserviceContractType=param.primaryServiceAgreementPopName;
  this.totalUnitsByAgreement = param.totalUnits
  this.GetAllUnitsBySite();
   this.tabSelectedIndex = 2;

}

showHideButton(){
  if(this.tabSelectedIndex == 2)
    this.isSaveBtnRequired = true;
  else
    this.isSaveBtnRequired = false;

  if(this.tabSelectedIndex == 2)
    this.isNextBtnRequired = false;
  else
    this.isNextBtnRequired = true;

    if(this.tabSelectedIndex == 0)
      this.isPrevBtnRequired = false;
    else
      this.isPrevBtnRequired = true;
}

GetAllUnitsBySite()
   {
    this.agnySiteReqReviewSvcSub4 = this.agnySiteReqReviewSvc.getSiteRequestUnits(this.siteRequestID)
    .subscribe(
      res1 => {
        this.FormatUnitsDataObject(res1);  
        },
      error => console.error('Error!', error)
    );
   }   

   FormatUnitsDataObject(res){
    let unitList = res as UnitRequest[];
    let newUnitList : UnitRequest[] = [];
    newUnitList = unitList.map(unit => {
      
      unit.unitFeatures = JSON.parse(unit.unitFeaturesJson);
      unit.unitFeaturesString = this.getFeaturesString(unit.unitFeatures);

      unit.rentalSubsidies = JSON.parse(unit.rentalSubsidyJson);
      unit.rentalSubsidyString = this.getSubsidiesString(unit.rentalSubsidies);
      
      return unit;
    });
  

    if (this.siteAgreePopRequestID !=0 ) {
      this.unitsDataObj = newUnitList.filter(d => (d.siteAgreementPopulationID=== this.siteAgreePopRequestID))
    }
      else {
      this.unitsDataObj = newUnitList; // massaged object for GRID
      }
      if  (this.unitsDataObj.length < this.totalUnitsByAgreement ) {
        this.unitTabVisible= true;
       }    
       else {   
       this.toastr.info("Can't add units", 'Unit details')
       }

  }

  getFeaturesString(UnitFeaturesArray : UnitFeature[]){
    let featuresString : string = null;

    if(UnitFeaturesArray) {
      UnitFeaturesArray.forEach(feature => {
        featuresString = featuresString ? (featuresString + ' ,' + feature.f_desc) : feature.f_desc;
      });
      return featuresString.substring(0, featuresString.length);
    }
    return ''; 
  }

  getSubsidiesString(rentSubsidyArray : RentalSubsidy[]){
    let subsidiesString : string = null;

    if(rentSubsidyArray) {
      rentSubsidyArray.forEach(subsidy => {
        subsidiesString = subsidiesString ? (subsidiesString + ' ,' + subsidy.sub_desc) : subsidy.sub_desc;
      });
      return subsidiesString.substring(0, subsidiesString.length);
    }
    return ''; 
  }

  ngOnDestroy() {
    if(this.userDataSub)  this.userDataSub.unsubscribe();
    if(this.agnySiteReqReviewSvcSub1) this.agnySiteReqReviewSvcSub1.unsubscribe();
    if(this.agnySiteReqReviewSvcSub2) this.agnySiteReqReviewSvcSub2.unsubscribe();
    if(this.agnySiteReqReviewSvcSub3) this.agnySiteReqReviewSvcSub3.unsubscribe();
    if(this.agnySiteReqReviewSvcSub4) this.agnySiteReqReviewSvcSub4.unsubscribe();

    if(this.siteReqSvcSub1) this.siteReqSvcSub1.unsubscribe();
    if(this.siteReqSvcSub2) this.siteReqSvcSub2.unsubscribe();
    if(this.siteReqSvcSub3) this.siteReqSvcSub3.unsubscribe();
    if(this.siteAdminSvcSub)  this.siteAdminSvcSub.unsubscribe();

    if(this.evtSvcSub4) this.evtSvcSub4.unsubscribe();
    if(this.evtSvcSub5) this.evtSvcSub5.unsubscribe();
    if(this.evtSvcSub6) this.evtSvcSub6.unsubscribe();
  }
 

}