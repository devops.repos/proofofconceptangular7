import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { siteRequestDemographic, SiteContact, SiteFeature, SiteRequestAgreementPopulation } from '../agency-site-maintenance/agency-site-model';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { UnitRequest } from '../agency-site-request/agency-site-request.model';

@Injectable({
  providedIn: 'root'
})
export class SiteAgreementRequestService {
  private createSiteAgreementRequestFromSiteURL = environment.pactApiUrl + 'Site/CreateSiteRequestFromSite';

  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }

  createSiteAgreementRequestFromSite(SiteID : number, UserId : number) : Observable<any>
  {
    return this.httpClient.post(this.createSiteAgreementRequestFromSiteURL, JSON.stringify({ "SiteID" : SiteID, "UserId" : UserId}), this.httpOptions);
  }

}