    export interface IPendingRequestQueue{
       agencyName:string;
       siteName:string;
       agencyReqID: number;
       siteReqID: number;
       primSvcAgreePop:string;
       reqBy:string;
       reqDate:string;
       reqStatusTypeID: number;
       reqStatusType:string;
       reqTypeID: number;
       reqType:string;
       siteCategoryTypeID: number;
       isHP: boolean;
       isRA: boolean;
       id: number;
       userId: number;
       userName: string;
    }
    export interface IPendingApprovalQueue{
        agencyName:string;
        siteName:string;
        agencyReqID: number;
        siteReqID: number;
        primSvcAgreePop:string;
        reqBy:string;
        reqDate:string;
        reqStatusTypeID: number;
        reqStatusType:string;
        reqTypeID: number;
        reqType:string;
        siteCategoryTypeID: number;
        isHP: boolean;
        isRA: boolean;
        id: number;
        userId: number;
        userName: string;
    }
    export interface IReviewedQueue{
        agencyName:string;
        siteName:string;
        agencyReqID: number;
        siteReqID: number;
        primSvcAgreePop:string;
        reqBy:string;
        reqDate:string;
        reqStatusTypeID: number;
        reqStatusType:string;
        reqTypeID: number;
        reqType:string;
        siteCategoryTypeID: number;
        isHP: boolean;
        isRA: boolean;
        id: number;
        userId: number;
        userName: string;

        reviewer: string;
        reviewedDate: string;
    }
    export interface IRequestSentQueue{
        agencyName:string;
        siteName:string;
        agencyReqID: number;
        siteReqID: number;
        primSvcAgreePop:string;
        reqBy:string;
        reqDate:string;
        reqStatusTypeID: number;
        reqStatusType:string;
        reqTypeID: number;
        reqType:string;
        siteCategoryTypeID: number;
        isHP: boolean;
        isRA: boolean;

        agencyAddr: string;
        requestorEmail: string;

        email1SentDate: string;
        email2SentDate: string;
        responseReceivedDate: string;

        id: number;
        userId: number;
        userName: string;
    }