import {BehaviorSubject, Subject} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class AgencySiteRequestQueueEventService{

    private actionResponseCompletedEvent = new BehaviorSubject<boolean>(false);
   
     emitActionResponseCompletedEvent(responseFlag: boolean){
        this.actionResponseCompletedEvent.next(responseFlag)
     }
   
     subscribeEventListner(){
        return this.actionResponseCompletedEvent.asObservable();
      } 
   
   }