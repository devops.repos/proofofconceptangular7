import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {AgencySiteRequestQueueEventService} from '../agency-site-request-queue-events.service';


@Component({
  selector: 'app-request-queue-file-upload-dialog',
  templateUrl: './request-queue-file-upload-dialog.component.html',
  styleUrls: ['./request-queue-file-upload-dialog.component.scss'],
})
export class RequestQueueFileUploadDialogComponent implements OnInit {

  siteReqID: number;
  agencyReqID: number;
  siteRequestType: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public agencySiteData: any,
    private dialogRef: MatDialogRef<RequestQueueFileUploadDialogComponent>,
    private evtSvc: AgencySiteRequestQueueEventService
  ) {
    this.siteReqID = this.agencySiteData.siteReqID;
    this.agencyReqID = this.agencySiteData.agencyReqID;
    this.siteRequestType = this.agencySiteData.siteRequestType;
  }


  // Close the dialog on close button
  CloseDialog() {
    this.dialogRef.close(true);
  }

  fileUploadCompleteHandler(event){
    this.dialogRef.close(true);
    this.evtSvc.emitActionResponseCompletedEvent(event);
  }

  // On Init
  ngOnInit() {}
}
