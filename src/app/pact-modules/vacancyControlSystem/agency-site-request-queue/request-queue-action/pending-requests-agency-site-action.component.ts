import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MatDialog } from '@angular/material';
import { RequestQueueFileUploadDialogComponent } from '../request-queue-dialog/request-queue-file-upload-dialog.component';
import { Subscription } from 'rxjs';
import {IPendingRequestQueue} from '../../agency-site-request-queue/agency-site-request-queue-interface.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-pending-request-agency-site-action',
  template: `
    <mat-icon
      class="rr-draft-icon"
      color="warn"
      [matMenuTriggerFor]="rrDraftAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #rrDraftAction="matMenu">
      <button mat-menu-item (click)="onFileUploadClick()">
        <mat-icon color="primary">assignment</mat-icon>Upload a PDF
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .rr-draft-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class PendingRequestAgencySiteActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  rowSelected: IPendingRequestQueue;

  deleteVCSReferralSub: Subscription;
  capByPactApplicationIDSub: Subscription;

  constructor(
    private dialog: MatDialog,
  ) {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onFileUploadClick() {
    this.rowSelected = this.params.data;
    this.openDialog();
  }

  openDialog(): void {
    this.dialog.open(RequestQueueFileUploadDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: { "agencyReqID": this.rowSelected.agencyReqID, "siteReqID": this.rowSelected.siteReqID, "siteRequestType": this.rowSelected.reqTypeID}
    });
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.deleteVCSReferralSub) {
      this.deleteVCSReferralSub.unsubscribe();
    }
    if (this.capByPactApplicationIDSub) {
      this.capByPactApplicationIDSub.unsubscribe();
    }
  }
}