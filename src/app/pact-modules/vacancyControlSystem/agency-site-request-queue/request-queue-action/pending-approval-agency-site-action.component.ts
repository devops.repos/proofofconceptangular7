import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import {IPendingApprovalQueue} from '../../agency-site-request-queue/agency-site-request-queue-interface.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-pending-approval-agency-site-action',
  template: `
    <mat-icon
      class="rr-draft-icon"
      color="warn"
      [matMenuTriggerFor]="rrDraftAction"
    >
      more_vert
    </mat-icon>
       <mat-menu #rrDraftAction="matMenu">
      <button mat-menu-item (click)="onMatMenuReviewClick()">
        <mat-icon color="primary">assignment</mat-icon>Review
      </button>
    </mat-menu> 
  `,
  styles: [
    `
      .rr-draft-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class PendingApprovalAgencySiteActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;
  rowSelected: IPendingApprovalQueue;

  constructor(
    private router: Router
  ) {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onMatMenuReviewClick() {
   this.rowSelected = this.params.data;

   if(this.rowSelected.siteCategoryTypeID === 5 && this.rowSelected.agencyReqID != null){
      this.router.navigate(['./admin/ra-agency-site-request-review/' + this.rowSelected.agencyReqID.toString() + '/' + this.rowSelected.siteReqID.toString() + '/' + this.rowSelected.reqTypeID] );
   } else if (this.rowSelected.siteCategoryTypeID === 5 && this.rowSelected.agencyReqID == null) {
    this.router.navigate(['./admin/ra-agency-site-request-review/0/' + this.rowSelected.siteReqID.toString() + '/' + this.rowSelected.reqTypeID] );
   } else if (this.rowSelected.agencyReqID == null) {
      this.router.navigate(['./admin/agency-site-request-review/0/' + this.rowSelected.siteReqID.toString() + '/' + this.rowSelected.reqTypeID] );
   } else {
      this.router.navigate(['./admin/agency-site-request-review/' + this.rowSelected.agencyReqID.toString() + '/' + this.rowSelected.siteReqID.toString() + '/' + this.rowSelected.reqTypeID] );
   }

  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {

  }
}