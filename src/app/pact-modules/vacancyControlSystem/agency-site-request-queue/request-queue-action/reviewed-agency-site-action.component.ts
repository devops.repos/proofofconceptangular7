import { Component, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { RequestQueueFileUploadDialogComponent } from '../request-queue-dialog/request-queue-file-upload-dialog.component';
import { Subscription } from 'rxjs';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-reviewed-agency-site-action',
  template: `
    <mat-icon
      class="rr-draft-icon"
      color="warn"
      [matMenuTriggerFor]="rrDraftAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #rrDraftAction="matMenu">
      <button mat-menu-item (click)="onMatMenuClick()">
        <mat-icon color="primary">assignment</mat-icon>Under Construction
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .rr-draft-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class ReviewedAgencySiteActionComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;

  deleteVCSReferralSub: Subscription;
  capByPactApplicationIDSub: Subscription;

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private router: Router
  ) {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onMatMenuClick() {
    // this.openDialog();
   }

  // Open Housing Application Supporting Documents Dialog
  openDialog(): void {
    this.dialog.open(RequestQueueFileUploadDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: null
    });
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.deleteVCSReferralSub) {
      this.deleteVCSReferralSub.unsubscribe();
    }
    if (this.capByPactApplicationIDSub) {
      this.capByPactApplicationIDSub.unsubscribe();
    }
  }
}