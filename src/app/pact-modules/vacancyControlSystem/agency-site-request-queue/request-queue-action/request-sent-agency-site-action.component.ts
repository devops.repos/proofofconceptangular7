import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import {AgencySiteRequestQueueEventService} from '../agency-site-request-queue-events.service';
import { Subscription } from 'rxjs';
import {IRequestSentQueue} from '../agency-site-request-queue-interface.model';
import {AgencySiteRequestQueueService} from '../agency-site-request-queue.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-request-sent-agency-site-action',
  template: `
    <mat-icon
      class="rr-draft-icon"
      color="warn"
      [matMenuTriggerFor]="rrDraftAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #rrDraftAction="matMenu">
      <button mat-menu-item (click)="onResendEmailClick()">
        <mat-icon color="primary">assignment</mat-icon>Resend Email
      </button>
    </mat-menu>
  `,
  styles: [
    `
      .rr-draft-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class RequestSentAgencySiteActionComponent implements ICellRendererAngularComp, OnInit, OnDestroy {
  params: any;
  public cell: any;
  rowSelected: IRequestSentQueue;
  currentUser: AuthData;
  currentUserSub: Subscription;

  constructor(
    private evtSvc: AgencySiteRequestQueueEventService,
    private agencySiteQueuSvc: AgencySiteRequestQueueService,
    private toastr: ToastrService,
    private router: Router,
    private userService: UserService,
  ) {}

  ngOnInit() {
    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
      }
    });
  }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onResendEmailClick() {

    // ONLY AGENCY-SITE REQUEST SHOULD BE ALLOW TO CALL THIS !!!!!!!!!!!!

    this.rowSelected = this.params.data;
    // responseReceivedDate should be null AND EITHER of EmailSentDate should be null
    if( (this.rowSelected.responseReceivedDate == undefined || this.rowSelected.responseReceivedDate == null || this.rowSelected.responseReceivedDate.trim().length == 0)
       && ((this.rowSelected.email1SentDate == undefined || this.rowSelected.email1SentDate == null || this.rowSelected.email1SentDate.trim().length == 0)
       || (this.rowSelected.email2SentDate == undefined || this.rowSelected.email2SentDate == null || this.rowSelected.email2SentDate.trim().length == 0))) {

        this.agencySiteQueuSvc.resendEmailAgencySiteRequest(this.rowSelected.agencyReqID, this.currentUser.optionUserId, this.currentUser.lanId, this.currentUser.email).subscribe(data => {
            
            this.toastr.success('Email is sent to the Requestor.', '');
            setTimeout(() => {
              this.evtSvc.emitActionResponseCompletedEvent(true); // reload the grid data on the parent page
            }, 1000);
        },
        error => {
          this.toastr.warning('Error while Saving !!', '');
        });
    } else {
      if(this.rowSelected.responseReceivedDate)
          this.toastr.info('Request is in "Pending Approval" status.', 'No Action Required.');
      else if(this.rowSelected.email1SentDate && this.rowSelected.email2SentDate)
          this.toastr.info('Email has already been sent on ' + this.rowSelected.email1SentDate + ' , and ' + this.rowSelected.email2SentDate, 'No Action Required.');
      else if(this.rowSelected.responseReceivedDate)
         this.toastr.info('Email has already been sent on ' + this.rowSelected.email1SentDate, 'No Action Required.');
      else
        this.toastr.info('No Action Required.', 'No Action Required.');
    }

   }


  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
  }
}