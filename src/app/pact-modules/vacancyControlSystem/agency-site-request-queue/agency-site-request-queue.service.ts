import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AgencySiteRequestQueueService {
  private apiURL = environment.pactApiUrl + 'Agency';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private currentRequestQueueTabIndex = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) {}

  getRequestQueueData(requestStatusTypeIDs: string) { // comma delimited requestStatusTypeIDs
    return this.http.post(this.apiURL + '/GetRequestQueue', JSON.stringify(requestStatusTypeIDs), this.httpOptions);
  }

  getCurrentRequestQueueTabIndex() {
    return this.currentRequestQueueTabIndex.asObservable();
  }

  setCurrentRequestQueueTabIndex(index: number) {
    this.currentRequestQueueTabIndex.next(index);
  }

  resendEmailAgencySiteRequest(agencyRequestId: number, userId: number, userName : string, userEmail : string) {
    let params = new HttpParams().set("agencyRequestId", agencyRequestId.toString()).set("userId", userId.toString()).set("userName", userName).set("userEmail", userEmail);
    return this.http.get(this.apiURL + '/ResendEmailAgencySiteRequest', {params : params, observe: 'response'});
  }



}