import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { CustomDateComponent } from '../custom-date-component.component';
import {
  IPendingRequestQueue, IPendingApprovalQueue, IReviewedQueue, IRequestSentQueue
} from './agency-site-request-queue-interface.model';
import { AgencySiteRequestQueueService } from './agency-site-request-queue.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { ReviewedAgencySiteActionComponent} from './request-queue-action/reviewed-agency-site-action.component';
import { RequestSentAgencySiteActionComponent} from './request-queue-action/request-sent-agency-site-action.component';
import { PendingRequestAgencySiteActionComponent} from './request-queue-action/pending-requests-agency-site-action.component';
import { PendingApprovalAgencySiteActionComponent} from './request-queue-action/pending-approval-agency-site-action.component';
import {AgencySiteRequestQueueEventService} from './agency-site-request-queue-events.service';
import { CommonService } from 'src/app/services/helper-services/common.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-agency-site-request-queue',
  templateUrl: './agency-site-request-queue.component.html',
  styleUrls: ['./agency-site-request-queue.component.scss']
})
export class AgencySiteRequestQueueComponent implements OnInit, OnDestroy {
  currentUser: AuthData;

  @ViewChild('pendingRequestsAgGrid') pendingRequestsAgGrid: AgGridAngular;
  @ViewChild('pendingApprovalAgGrid') pendingApprovalAgGrid: AgGridAngular;
  @ViewChild('reviewedAgGrid') reviewedAgGrid: AgGridAngular;
  @ViewChild('requestSentAgGrid') requestSentAgGrid: AgGridAngular;

  selectedTab;
  requestStatusTypes: RefGroupDetails[];

  pendingRequestsColumnDefs;
  pendingApprovalColumnDefs;
  reviewedColumnDefs;
  requestSentColumnDefs;

  pendingRequestGridApi;
  pendingRequestGridColumnApi;
  pendingApprovalGridApi;
  pendingApprovalGridColumnApi;
  reviewedGridApi;
  reviewedGridColumnApi;
  requestSentGridApi;
  requestSentGridColumnApi;

  defaultColDef;
  frameworkComponents;
  context;

  commonServiceSub: Subscription;

  public pendingRequestsGridOptions: GridOptions;
  public pendingApprovalGridOptions: GridOptions;
  public reviewedGridOptions: GridOptions;
  public requestSentGridOptions: GridOptions;

  pendingRequestsRowData: IPendingRequestQueue[] = [];
  pendingApprovalRowData: IPendingApprovalQueue[] = [];
  reviewedRowData: IReviewedQueue[] = [];
  requestSentRowData: IRequestSentQueue[] = [];

  currentUserSub: Subscription;
  selectedTabSub: Subscription;
  agencyListSub: Subscription;
  siteListSub: Subscription;
  referralRosterDataSub: Subscription;
  saveVCSReferralSub: Subscription;
  evtSvcSub: Subscription;

  constructor(
    private userService: UserService,
    private AgencySiteRequestQueueService: AgencySiteRequestQueueService,
    private toastr: ToastrService,private commonService: CommonService,
    private router: Router,
    private evtSvc: AgencySiteRequestQueueEventService
  ) {
    this.pendingRequestsGridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.pendingApprovalGridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.reviewedGridOptions = {
      rowHeight: 35
    } as GridOptions;
    this.requestSentGridOptions = {
      rowHeight: 35
    } as GridOptions;

    this.pendingRequestsColumnDefs = [
      {
        headerName: 'Agency Req.',
        field: 'agencyReqID',
        filter: 'agTextColumnFilter',
        maxWidth: 120,
      },
      {
        headerName: 'Site Req.',
        field: 'siteReqID',
        filter: 'agTextColumnFilter',
        maxWidth: 120,
        hide : true
      },
      {
        headerName: 'Agency Name',
        field: 'agencyName',
        filter: 'agTextColumnFilter',
        minWidth: 250,
      },
      {
        headerName: 'Site Name',
        field: 'siteName',
        filter: 'agTextColumnFilter',
        minWidth: 250,
      },
      {
        headerName: 'Primary Service Contracts',
        filter: 'agTextColumnFilter',
        field: 'primSvcAgreePop',
        minWidth: 300,
      },
      {
        headerName: 'Requested By',
        field: 'reqBy',
        filter: 'agTextColumnFilter',
        width: 200,
      },
      {
        headerName: 'Requested Date',
        filter: 'agTextColumnFilter',
        field: 'reqDate',
        maxWidth: 130,
      },
      {
        headerName: 'Request Type',
        filter: 'agTextColumnFilter',
        field: 'reqType',
        width: 200,
      },
      {
        headerName: 'Request Status',
        filter: 'agTextColumnFilter',
        field: 'reqStatusType',
        width: 200,
      },
      {
        headerName: 'Request Category',
        filter: 'agTextColumnFilter',
        field: 'isHP',
        width: 200,
        hide: true,
        valueGetter(params) {
          if (params.data.isHP === true) {
            return "Housing Provider Agency";
          } else if (params.data.isRA === true) {
            return "Referring Agency";
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'pendingRequestAgencySiteActionComponent'
      }
    ];
    this.pendingApprovalColumnDefs = [
      {
        headerName: 'Agency Req.',
        field: 'agencyReqID',
        filter: 'agTextColumnFilter',
        maxWidth: 120,
      },
      {
        headerName: 'Site Req.',
        field: 'siteReqID',
        filter: 'agTextColumnFilter',
        maxWidth: 120,
      },
      {
        headerName: 'Agency Name',
        field: 'agencyName',
        filter: 'agTextColumnFilter',
        minWidth: 250,
      },
      {
        headerName: 'Site Name',
        field: 'siteName',
        filter: 'agTextColumnFilter',
        minWidth: 250,
      },
      {
        headerName: 'Primary Service Contracts',
        filter: 'agTextColumnFilter',
        field: 'primSvcAgreePop',
        minWidth: 300,
      },
      {
        headerName: 'Requested By',
        field: 'reqBy',
        filter: 'agTextColumnFilter',
        width: 200,
      },
      {
        headerName: 'Requested Date',
        filter: 'agTextColumnFilter',
        field: 'reqDate',
        maxWidth: 130,
      },
      {
        headerName: 'Request Type',
        filter: 'agTextColumnFilter',
        field: 'reqType',
        width: 200,
      },
      {
        headerName: 'Request Status',
        filter: 'agTextColumnFilter',
        field: 'reqStatusType',
        width: 200,
      },
      {
        headerName: 'Request Category',
        filter: 'agTextColumnFilter',
        field: 'isHP',
        width: 200,
        hide: true,
        valueGetter(params) {
          if (params.data.isHP === true) {
            return "Housing Provider Agency";
          } else if (params.data.isRA === true) {
            return "Referring Agency";
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'pendingApprovalAgencySiteActionComponent'
      }
    ];
    this.reviewedColumnDefs = [
      // {
      //   headerName: 'Agency Req.',
      //   field: 'agencyReqID',
      //   filter: 'agTextColumnFilter',
      //   maxWidth: 120,
      // },
      // {
      //   headerName: 'Site Req.',
      //   field: 'siteReqID',
      //   filter: 'agTextColumnFilter',
      //   maxWidth: 120,
      // },
      {
        headerName: 'Agency Name',
        field: 'agencyName',
        filter: 'agTextColumnFilter',
        minWidth: 250,
      },
      {
        headerName: 'Site Name',
        field: 'siteName',
        filter: 'agTextColumnFilter',
        minWidth: 250,
      },
      {
        headerName: 'Primary Service Contracts',
        filter: 'agTextColumnFilter',
        field: 'primSvcAgreePop',
        minWidth: 300,
      },
      {
        headerName: 'Requested By',
        field: 'reqBy',
        filter: 'agTextColumnFilter',
        width: 200,
      },
      {
        headerName: 'Requested Date',
        filter: 'agTextColumnFilter',
        field: 'reqDate',
        maxWidth: 130,
      },
      {
        headerName: 'Reviewed By',
        filter: 'agTextColumnFilter',
        field: 'reviewer',
        width: 200,
      },
      {
        headerName: 'Reviewed Date',
        filter: 'agTextColumnFilter',
        field: 'reviewedDate',
        maxWidth: 130,
      },
      {
        headerName: 'Request Type',
        filter: 'agTextColumnFilter',
        field: 'reqType',
        width: 200,
      },
      {
        headerName: 'Request Status',
        filter: 'agTextColumnFilter',
        field: 'reqStatusType',
        width: 200,
      },
      {
        headerName: 'Request Category',
        filter: 'agTextColumnFilter',
        field: 'isHP',
        width: 200,
        hide: true,
        valueGetter(params) {
          if (params.data.isHP === true) {
            return "Housing Provider Agency";
          } else if (params.data.isRA === true) {
            return "Referring Agency";
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'reviewedAgencySiteActionComponent',
        hide: true
      }
    ];
    this.requestSentColumnDefs = [
      {
        headerName: 'Agency Req.',
        field: 'agencyReqID',
        filter: 'agTextColumnFilter',
        maxWidth: 120,
      },
      {
        headerName: 'Site Req.',
        field: 'siteReqID',
        filter: 'agTextColumnFilter',
        maxWidth: 120,
      },
      {
        headerName: 'Agency Name',
        field: 'agencyName',
        filter: 'agTextColumnFilter',
        minWidth: 250,
      },
      {
        headerName: 'Site Name',
        field: 'siteName',
        filter: 'agTextColumnFilter',
        minWidth: 250,
      },
      {
        headerName: 'Agency Address',
        field: 'agencyAddr',
        filter: 'agTextColumnFilter',
        minWidth: 300,
      },
      {
        headerName: 'Request Type',
        filter: 'agTextColumnFilter',
        field: 'reqType',
        width: 200,
      },
      {
        headerName: 'Requested By',
        field: 'reqBy',
        filter: 'agTextColumnFilter',
        width: 200,
      },
      {
        headerName: 'Requestor Email',
        field: 'requestorEmail',
        filter: 'agTextColumnFilter',
        width: 200,
      },
      {
        headerName: 'Requested Date',
        field: 'reqDate',
        filter: 'agTextColumnFilter',
        maxWidth: 130,
      },
      {
        headerName: 'Request Category',
        filter: 'agTextColumnFilter',
        field: 'isHP',
        width: 200,
        hide: true,
        valueGetter(params) {
          if (params.data.isHP === true) {
            return "Housing Provider Agency";
          } else if (params.data.isRA === true) {
            return "Referring Agency";
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        // suppressSizeToFit: true,
        cellRenderer: 'requestSentAgencySiteActionComponent'
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true,
      editable: false
    };
    this.context = { componentParent: this };
    this.frameworkComponents = {
        agDateInput: CustomDateComponent,
        reviewedAgencySiteActionComponent : ReviewedAgencySiteActionComponent,
        requestSentAgencySiteActionComponent : RequestSentAgencySiteActionComponent,
        pendingRequestAgencySiteActionComponent : PendingRequestAgencySiteActionComponent,
        pendingApprovalAgencySiteActionComponent : PendingApprovalAgencySiteActionComponent,
    };
  }

  ngOnInit() {
    /** Getting the currently active user info */
    this.currentUserSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.currentUser = userdata;
      }
    });

    /** Get the current tab index */
    this.AgencySiteRequestQueueService.getCurrentRequestQueueTabIndex().subscribe(res => {
        this.selectedTab = res;  // returns default value 0
    });

    setTimeout(() => {
      this.loadRefGroupDetails();
    }, 500);

    this.evtSvcSub = this.evtSvc.subscribeEventListner().subscribe(responseFlag =>{
      if(responseFlag === true)
       this.onGo();
    });

  }

  loadRefGroupDetails() {
    const requestStatusTypeId = 70;
    this.commonServiceSub = this.commonService.getRefGroupDetails(requestStatusTypeId.toString()).subscribe(
      res => {
          const data = res.body as RefGroupDetails[];
          this.requestStatusTypes = data.filter(d => (d.refGroupID === requestStatusTypeId));

          setTimeout(() => {
            this.onGo();
          }, 1000);
      },
      error => console.error('Error!', error)
    );
  }

  onGo() {
    let that = this;

     this.AgencySiteRequestQueueService.getCurrentRequestQueueTabIndex().subscribe(res => {
        that.selectedTab = res;
    });

    if(that.requestStatusTypes && that.requestStatusTypes.length > 0){

      let filteredReqtatusTypes : string = '';
      if (this.selectedTab === 0) {
        filteredReqtatusTypes = that.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Pending Request').refGroupDetailID.toString();
      } else if (this.selectedTab === 1 ) {
        filteredReqtatusTypes = that.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Pending Approval').refGroupDetailID.toString();
      } else if (this.selectedTab === 2) {
        filteredReqtatusTypes = that.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Approved').refGroupDetailID.toString();
        filteredReqtatusTypes += ',' + that.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Disapproved').refGroupDetailID.toString();
      } else if (this.selectedTab === 3) {
        // Both 'Pending Request' & 'Pending Approval'
        filteredReqtatusTypes = that.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Pending Request').refGroupDetailID.toString();
        filteredReqtatusTypes += ',' +  that.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Pending Approval').refGroupDetailID.toString();
        filteredReqtatusTypes += ',' +  that.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Approved').refGroupDetailID.toString();
        filteredReqtatusTypes += ',' +  that.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Disapproved').refGroupDetailID.toString();
      }

      this.referralRosterDataSub = this.AgencySiteRequestQueueService.getRequestQueueData(filteredReqtatusTypes).subscribe((
        res) => {

          if (that.selectedTab === 0) {
            that.pendingRequestsRowData = res as IPendingRequestQueue[];
          } else if (that.selectedTab === 1 ) {
            that.pendingApprovalRowData = res as IPendingApprovalQueue[];
          } else if (that.selectedTab === 2) {
            that.reviewedRowData = res as IReviewedQueue[];
          } else if (that.selectedTab === 3) {
            that.requestSentRowData  = res as IRequestSentQueue[];
          }
  
        });
    }

  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;
    // alert('selectedTabIndex: ' + this.selectedTab);
    this.AgencySiteRequestQueueService.setCurrentRequestQueueTabIndex(this.selectedTab);
    this.onGo();
    }

    onPendingRequestsGridReady(params) {
      params.api.setDomLayout('autoHeight');
      this.pendingRequestGridApi = params.api;
      this.pendingRequestGridColumnApi = params.columnApi;

      const allColumnIds = [];
      this.pendingRequestGridColumnApi.getAllColumns().forEach(column => {
        if (column.colId !== 'action') {
          if (column.colId !== 'clientEligibleFor') {
            // console.log('column.colID: ' + column.colId);
            allColumnIds.push(column.colId);
          }
        }
      });
      this.pendingRequestGridColumnApi.autoSizeColumns(allColumnIds);
      // params.api.expandAll();
    }

    onPendingApprovalGridReady(params) {
      params.api.setDomLayout('autoHeight');
      this.pendingApprovalGridApi = params.api;
      this.pendingApprovalGridColumnApi = params.columnApi;

      const allColumnIds = [];
      this.pendingApprovalGridColumnApi.getAllColumns().forEach(column => {
        if (column.colId !== 'action') {
          if (column.colId !== 'clientEligibleFor') {
            // console.log('column.colID: ' + column.colId);
            allColumnIds.push(column.colId);
          }
        }
      });
      this.pendingApprovalGridColumnApi.autoSizeColumns(allColumnIds);
      // params.api.expandAll();
    }

    onReviewedGridReady(params) {
      params.api.setDomLayout('autoHeight');
      this.reviewedGridApi = params.api;
      this.reviewedGridColumnApi = params.columnApi;

      const allColumnIds = [];
      this.reviewedGridColumnApi.getAllColumns().forEach(column => {
        if (column.colId !== 'action') {
          if (column.colId !== 'clientEligibleFor') {
            // console.log('column.colID: ' + column.colId);
            allColumnIds.push(column.colId);
          }
        }
      });
      this.reviewedGridColumnApi.autoSizeColumns(allColumnIds);
      // params.api.expandAll();
    }

    onRequestSentGridReady(params) {
      params.api.setDomLayout('autoHeight');
      this.requestSentGridApi = params.api;
      this.requestSentGridColumnApi = params.columnApi;

      const allColumnIds = [];
      this.requestSentGridColumnApi.getAllColumns().forEach(column => {
        if (column.colId !== 'action') {
          if (column.colId !== 'clientEligibleFor') {
            // console.log('column.colID: ' + column.colId);
            allColumnIds.push(column.colId);
          }
        }
      });
      this.requestSentGridColumnApi.autoSizeColumns(allColumnIds);
      // params.api.expandAll();
    }

    refreshAgGrid(val: string) {
      // this.flagClicked = '';
      // if (val === 'pendingRequests') {
      //   this.pendingRequestsGridOptions.api.setFilterModel(null);
      // } else if (val === 'pendingApproval') {
      //   this.pendingApprovalGridOptions.api.setFilterModel(null);
      // } else if (val === 'reviewed') {
      //   this.reviewedGridOptions.api.setFilterModel(null);
      // } else if (val === 'requestSent') {
      //   this.requestSentGridOptions.api.setFilterModel(null);
      // }
    }

    onBtExport(val: string) {
      /** appending the current datetime with the filename */
      // const date = new Date().toLocaleDateString().replace(/\//g, '-');
      // const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
      // const time = times.replace(/:/g, '-');

      // const params = {
      //   fileName: (val === 'd' ?  'draftReferralReport-' : val === 't' ? 'TransmittedReferralReport-' : val === 'p' ? 'PendingReferralReport-' : 'CompletedReferralReport')  + date + '-' + time
      //   // sheetName: document.querySelector("#sheetName").value,
      // };
      // if (val === 'd') {
      //   this.draftGridApi.exportDataAsExcel(params);
      // } else if (val === 't') {
      //   this.transmittedGridApi.exportDataAsExcel(params);
      // } else if (val === 'p') {
      //   this.pendingGridApi.exportDataAsExcel(params);
      // } else if (val === 'c') {
      //   this.completedGridApi.exportDataAsExcel(params);
      // }
    }

  ngOnDestroy() {
    if (this.currentUserSub) {
      this.currentUserSub.unsubscribe();
    }
    if (this.selectedTabSub) {
      this.selectedTabSub.unsubscribe();
    }
    if (this.agencyListSub) {
      this.agencyListSub.unsubscribe();
    }
    if (this.siteListSub) {
      this.siteListSub.unsubscribe();
    }
    if (this.referralRosterDataSub) {
      this.referralRosterDataSub.unsubscribe();
    }
    if (this.saveVCSReferralSub) {
      this.saveVCSReferralSub.unsubscribe();
    }
    if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
    if(this.evtSvcSub) this.evtSvcSub.unsubscribe();
  }
}