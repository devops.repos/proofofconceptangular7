import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AgencyData, AgencySiteData, PrimaryServiceContractAgreementPopulation } from '../agency-site-maintenance//agency-site-model';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { Observable,Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import {  FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';



@Component({
  selector: 'app-unit-roster',
  templateUrl: './unit-roster.component.html',
  styleUrls: ['./unit-roster.component.scss']
})
export class UnitRosterComponent implements OnInit {
  agencyList: AgencyData[];

  agencyInfo: AgencyData
  agencySites: AgencySiteData[] = [];
  commonServiceSub: Subscription;
  agencyId: number = 0;
  siteId: number = 0;
  siteAgreementPopulationSelected: number = 0;
  primarySvcContractTypes: PrimaryServiceContractAgreementPopulation[] = [];
  RA;
  isPE;
  isHP: boolean = false;
  isIH;

  userData: AuthData;
  userDataSub: Subscription;
  siteaddress: string = "";
  searchStringCtrl = new FormControl();
  agencyGroup: FormGroup;
  filteredAgencyData: Observable<any[]>;
  userid:number=0;

  allprimarySvcContractType: PrimaryServiceContractAgreementPopulation = { agreementPopulationID: 0, primaryServiceAgreementPopName: 'ALL', siteAgreementPopulationID: '0' };

  constructor(
    private siteAdminservice: SiteAdminService,
    private toastr: ToastrService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private commonService: CommonService) { }

  ngOnInit() {
    // this.loadagencylist();
    this.agencyGroup = this.formBuilder.group({
      searchStringCtrl: ['']
    });

    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        this.userid= this.userData.optionUserId;
        if (this.userData.siteCategoryType.length > 0) {
          this.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
          this.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
          this.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
          this.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
        }
      }
      if (this.isHP === true) {
        this.agencyId = this.userData.agencyId;
        this.refershAgencySites(this.agencyId);

      }
      this.loadagencylist();
    },
      error => console.error('Error!', error)
    );
  }



  refershSitePrimaryServiceContract() {
    if (this.siteId != 0) {
      this.loadprimaryServiceContract();
      var itm = this.agencySites.find(x => x.siteID == this.siteId) as AgencySiteData

      if (itm !== null) {
        this.siteaddress = itm.siteAddress;
      }
    }
  }

  refershAgencySites(id:number) {

    if (id != 0) {
      this.loadSiteInfo(id);
    }
  }

  loadagencylist() {
    this.siteAdminservice.getAgencyList(null, "7")
      .subscribe(
        res => {
          this.agencyList = res.body as AgencyData[];
          this.filteredAgencyData = this.searchStringCtrl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
          );
        },
        error => console.error('Error!', error)
      );
  }
  loadSiteInfo(agencyid: number) {
    this.siteAdminservice.getSites(agencyid.toString(),this.userid.toString())
      .subscribe(
        res => {
          const data = res.body as AgencySiteData[];
          this.agencySites = data.filter(d => (d.siteStatus === 'Active') && (d.siteCategoryType === 7));
        },
        error => console.error('Error!', error)
      );
  }

  loadprimaryServiceContract() {

    this.commonService.getPrimaryServiceAgreementPopulation(this.siteId).subscribe(
      res => {
        this.primarySvcContractTypes = res as PrimaryServiceContractAgreementPopulation[];
        this.primarySvcContractTypes.unshift(this.allprimarySvcContractType);
      },
      error => console.error('Error!', error)
    );
  }
  private _filter(value: string): AgencyData[] {
    const filterValue = value.toLowerCase();
    return this.agencyList.filter(agency => agency.name.toLowerCase().includes(filterValue) || agency.agencyNo.includes(filterValue));
  }



}