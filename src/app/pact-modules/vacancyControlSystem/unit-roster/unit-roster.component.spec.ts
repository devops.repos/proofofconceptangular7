import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitRosterComponent } from './unit-roster.component';

describe('UnitRosterComponent', () => {
  let component: UnitRosterComponent;
  let fixture: ComponentFixture<UnitRosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitRosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitRosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
