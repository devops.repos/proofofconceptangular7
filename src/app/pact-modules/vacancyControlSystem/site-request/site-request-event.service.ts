import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import {SiteRequestAgreementPopulationData} from './site-request.model';

@Injectable({
  providedIn: 'root'
})

export class SiteRequestEventService {

    private siteRequestAgreementPopulationData = new BehaviorSubject<Map<number, SiteRequestAgreementPopulationData[]>>(new Map());
  
    private siteRequestSiteContactsIsValid = new BehaviorSubject<Map<number, boolean>>(new Map());
    private siteRequestSiteDemographicsIsValid = new BehaviorSubject<Map<number, boolean>>(new Map());
    private siteRequestPrimaryServiceAgreementIsValid = new BehaviorSubject<Map<number, boolean>>(new Map());
    private siteRequestLocationType = new BehaviorSubject<Map<number, number>>(new Map());

    constructor() { }

      emitSiteRequestAgreementPopulationData(siteRequestID : number, data: SiteRequestAgreementPopulationData[]) {
        let mapObject = this.siteRequestAgreementPopulationData.getValue();
        mapObject.set(siteRequestID, data);
        this.siteRequestAgreementPopulationData.next(mapObject);
      }
    
      subscribeSiteRequestAgreementPopulationData(): Observable<Map<number, SiteRequestAgreementPopulationData[]>> {
        return this.siteRequestAgreementPopulationData.asObservable();
      }
    
      emitSiteRequestSiteContacts(siteRequestID : number, flagValue : boolean) {
        let mapObject = this.siteRequestSiteContactsIsValid.getValue();
        mapObject.set(siteRequestID, flagValue);
        this.siteRequestSiteContactsIsValid.next(mapObject);
      }
    
      subscribeSiteRequestSiteContacts(): Observable<Map<number, boolean>> {
        return this.siteRequestSiteContactsIsValid.asObservable();
      }
    
      emitSiteRequestSiteDemographics(siteRequestID : number, flagValue : boolean) {
        let mapObject = this.siteRequestSiteDemographicsIsValid.getValue();
        mapObject.set(siteRequestID, flagValue);
        this.siteRequestSiteDemographicsIsValid.next(mapObject);
      }
    
      subscribeSiteRequestSiteDemographics(): Observable<Map<number, boolean>> {
        return this.siteRequestSiteDemographicsIsValid.asObservable();
      }
    
      emitSiteRequestPrimaryServiceAgreement(siteRequestID : number, flagValue : boolean) {
        let mapObject = this.siteRequestPrimaryServiceAgreementIsValid.getValue();
        mapObject.set(siteRequestID, flagValue);
        this.siteRequestPrimaryServiceAgreementIsValid.next(mapObject);
      }
    
      subscribeSiteRequestPrimaryServiceAgreement(): Observable<Map<number, boolean>> {
        return this.siteRequestPrimaryServiceAgreementIsValid.asObservable();
      }

      emitSiteRequestLocationType(siteRequestID : number, locationType : number) {
        let mapObject = this.siteRequestLocationType.getValue();
        mapObject.set(siteRequestID, locationType);
        this.siteRequestLocationType.next(mapObject);
      }
    
      subscribeSiteRequestLocationType(): Observable<Map<number, number>> {
        return this.siteRequestLocationType.asObservable();
      }


    }