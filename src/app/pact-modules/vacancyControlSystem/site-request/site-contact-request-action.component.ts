
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { SiteContact } from '../agency-site-maintenance/agency-site-model';


@Component({
  selector: "site-contact-action",
  template: `<mat-icon  matTooltip='Edit Contact' (click)="onView()" class="site-action-icon" color="primary">edit</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete Contact' class="site-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .site-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class SiteContactRequestActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private siteContactSelected: SiteContact;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    this.siteContactSelected = {
    siteContactID : this.params.data.siteContactID,
    siteContactRequestID : this.params.data.siteContactRequestID,
    siteID : this.params.data.siteID,
    siteRequestID: this.params.data.siteRequestID,
    optionUserID: this.params.data.optionUserID,
    firstName :this.params.data.firstName,
    lastName :this.params.data.lastName,
    title:this.params.data.title,
    email :this.params.data.email,
    phone :this.params.data.phone,
    extension :this.params.data.extension,
    alternatePhone :this.params.data.alternatePhone,
    alternateExtension :this.params.data.alternateExtension,
    fax :this.params.data.fax,
    isSysAdmin:this.params.data.isSysAdmin,
    isActive :this.params.data.isActive,
    createdBy : this.params.data.createdBy,
    createdDate :this.params.data.createdDate,
    updatedBy : this.params.data.updatedBy,
    updatedDate :this.params.data.updatedDate,
    };
    this.params.context.componentParent.contactViewParent(this.siteContactSelected);
  }

  onDelete() {
    this.siteContactSelected = {
      siteContactID : this.params.data.siteContactID,
      siteContactRequestID : this.params.data.siteContactRequestID,
      siteID : this.params.data.siteID,
      siteRequestID: this.params.data.siteRequestID,
      optionUserID: this.params.data.optionUserID,
      firstName :this.params.data.firstName,
      lastName :this.params.data.lastName,
      title:this.params.data.title,
      email :this.params.data.email,
      phone :this.params.data.phone,
      extension :this.params.data.extension,
      alternatePhone :this.params.data.alternatePhone,
      alternateExtension :this.params.data.alternateExtension,
      fax :this.params.data.fax,
      isSysAdmin:this.params.data.isSysAdmin,
      isActive :this.params.data.isActive,
      createdBy : this.params.data.createdBy,
      createdDate :this.params.data.createdDate,
      updatedBy : this.params.data.updatedBy,
      updatedDate :this.params.data.updatedDate,
    };
    this.params.context.componentParent.siteContactDeleteParent(this.siteContactSelected);
  }

  refresh(): boolean {
    return false;
  }
}
