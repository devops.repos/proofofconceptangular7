
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { SiteRequestAgreementPopulation } from '../agency-site-maintenance/agency-site-model';


@Component({
  selector: "primary-service-contract-action",
  template: `<mat-icon matTooltip='Edit' (click)="onEdit()" class="doc-action-icon" color="primary">edit</mat-icon>
  <mat-icon matTooltip='Unit Roster' (click)="onView()" class="primary-service-contract-action-icon" color="primary">dvr</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete' class="primary-service-contract-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .primary-service-contract-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class PrimaryServiceContractRequestActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private siteRequestAgreePopSelected: SiteRequestAgreementPopulation;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    this.siteRequestAgreePopSelected = {
    siteID : this.params.data.siteID,
    siteRequestID : this.params.data.siteRequestID,

    siteAgreementPopulationID : this.params.data.siteAgreementPopulationID,
    siteAgreementPopulationRequestID: this.params.data.siteAgreementPopulationRequestID,

    siteAgreementPopulationRequestType: this.params.data.siteAgreementPopulationRequestType, 

    agreementPopulationID: this.params.data.agreementPopulationID,
    primaryServiceAgreementPopName: this.params.data.primaryServiceAgreementPopName,

    rentalSubsidies :this.params.data.rentalSubsidies,
    totalUnits:this.params.data.totalUnits,
    totalUnitsDataEntered:this.params.data.totalUnitsDataEntered,
    isActive :this.params.data.isActive,
    userId : this.params.data.userId
    };

    this.params.context.componentParent.primaryServiceContractViewParent(this.siteRequestAgreePopSelected);
  }

  onEdit() {
    this.siteRequestAgreePopSelected = {
    siteID : this.params.data.siteID,
    siteRequestID : this.params.data.siteRequestID,

    siteAgreementPopulationID : this.params.data.siteAgreementPopulationID,
    siteAgreementPopulationRequestID: this.params.data.siteAgreementPopulationRequestID,

    siteAgreementPopulationRequestType: this.params.data.siteAgreementPopulationRequestType, 

    agreementPopulationID: this.params.data.agreementPopulationID,
    primaryServiceAgreementPopName: this.params.data.primaryServiceAgreementPopName,

    rentalSubsidies :this.params.data.rentalSubsidies,
    totalUnits:this.params.data.totalUnits,
    totalUnitsDataEntered:this.params.data.totalUnitsDataEntered,
    isActive :this.params.data.isActive,
    userId : this.params.data.userId
    };

    this.params.context.componentParent.primaryServiceContractEditParent(this.siteRequestAgreePopSelected);
  }

  onDelete() {
    this.siteRequestAgreePopSelected = {
      siteID : this.params.data.siteID,
      siteRequestID : this.params.data.siteRequestID,
  
      siteAgreementPopulationID : this.params.data.siteAgreementPopulationID,
      siteAgreementPopulationRequestID: this.params.data.siteAgreementPopulationRequestID,
  
      siteAgreementPopulationRequestType: this.params.data.siteAgreementPopulationRequestType, 
  
      agreementPopulationID: this.params.data.agreementPopulationID,
      primaryServiceAgreementPopName: this.params.data.primaryServiceAgreementPopName,
  
      rentalSubsidies :this.params.data.rentalSubsidies,
      totalUnits:this.params.data.totalUnits,
      totalUnitsDataEntered:this.params.data.totalUnitsDataEntered,
      isActive :this.params.data.isActive,
      userId : this.params.data.userId
    };
    this.params.context.componentParent.sitePrimartSerivceContractDeleteParent(this.siteRequestAgreePopSelected);
  }

  refresh(): boolean {
    return false;
  }
}
