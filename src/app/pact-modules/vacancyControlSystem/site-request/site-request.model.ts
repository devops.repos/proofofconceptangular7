// import {UnitRequest} from '../agency-site-request/agency-site-request.model';
// import {SiteRequestAgreementPopulation} from '../agency-site-maintenance/agency-site-model';

export interface SiteRequestAgreementPopulationData
{
    siteRequestID :number;
    siteAgreementPopulationRequestID :number;
    agreementPopulationID :number;
    primaryServiceAgreementPopName :string;
    totalUnits :number;
    totalUnitsDataEntered :number;
}
