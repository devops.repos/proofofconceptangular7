import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { siteRequestDemographic, SiteContact, SiteFeature, SiteRequestAgreementPopulation } from '../agency-site-maintenance/agency-site-model';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { UnitRequest } from '../agency-site-request/agency-site-request.model';


@Injectable({
  providedIn: 'root'
})

export class SiteRequestService {
  private saveSiteRequestURL = environment.pactApiUrl + 'Site/SaveSiteRequest';
  private saveSiteRequestStatusURL = environment.pactApiUrl + 'Site/SaveSiteRequestStatus';
  private saveSiteDemographicsURL = environment.pactApiUrl + 'Site/SaveSiteDemographics';

  private saveUnitRequestURL = environment.pactApiUrl + 'UnitRequest/Save';
  private deleteUnitRequestURL = environment.pactApiUrl + 'UnitRequest/Delete';
  private getUnitsRequestURL = environment.pactApiUrl + 'UnitRequest/GetUnits';
  
  private saveSiteRequestContactURL = environment.pactApiUrl + 'Site/SaveSiteRequestContact';
  private saveSiteRequestAgreementPopulationURL = environment.pactApiUrl + 'Site/SaveSiteRequestAgreementPopulation';
  private saveSiteRequestFeatureURL = environment.pactApiUrl + 'Site/SaveSiteRequestFeature';
  private getUnitTypesRequestURL = environment.pactApiUrl + 'Site/GetSiteRequestAgreePopUnitTypes';
  private getRentalSubsidiesRequestURL = environment.pactApiUrl + 'Site/GetSiteRequestAgreePopRentalSubsidies';

  private deleteSiteRequestContactURL = environment.pactApiUrl + 'Site/DeleteSiteRequestContact';
  private deleteSiteRequestAgreementPopulationURL = environment.pactApiUrl + 'Site/DeleteSiteRequestAgreementPopulation';
  private deleteSiteRequestFeatureURL = environment.pactApiUrl + 'Site/DeleteSiteRequestFeature';

  private deleteAllSiteContactsURL = environment.pactApiUrl + 'Site/DeleteAllSiteRequestContacts';
  private deleteAllSiteAgreementPopulationsURL = environment.pactApiUrl + 'Site/DeleteAllSiteRequestAgreementPopulations';
  private deleteAllSiteFeaturesURL = environment.pactApiUrl + 'Site/DeleteAllSiteRequestFeatures';


  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }

  saveSiteRequest(siteRequestDemographic : siteRequestDemographic) : Observable<any>
  {
    return this.httpClient.post(this.saveSiteRequestURL, JSON.stringify(siteRequestDemographic), this.httpOptions);
  }

  saveSiteRequestStatus(siteRequestID : number, requestStatusType : number, userID : number): Observable<any> {
    return this.httpClient.post(this.saveSiteRequestStatusURL, JSON.stringify({ "SiteRequestID" : siteRequestID, "RequestStatusType" : requestStatusType, "UserID" : userID}), this.httpOptions);
  }

  saveSiteRequestDemographics(siteRequestDemographic : siteRequestDemographic) : Observable<any>
  {
    return this.httpClient.post(this.saveSiteDemographicsURL, JSON.stringify(siteRequestDemographic), this.httpOptions);
  }

  getSiteRequestUnits(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.getUnitsRequestURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  getAgreementPopulationUnitTypes(siteAgreementPopRequestID : number, contractAgencyID : number): Observable<any> {
    return this.httpClient.post(this.getUnitTypesRequestURL, JSON.stringify({ "SiteAgreementPopRequestID" : siteAgreementPopRequestID, "ContractAgencyID" : contractAgencyID}), this.httpOptions);
  }

  getAgreementPopulationRentalSubsidies(siteAgreementPopRequestID : number): Observable<any> {
    return this.httpClient.post(this.getRentalSubsidiesRequestURL, JSON.stringify(siteAgreementPopRequestID), this.httpOptions);
  }

  saveSiteRequestContact(siteContact : SiteContact): Observable<any> {
    return this.httpClient.post(this.saveSiteRequestContactURL, JSON.stringify(siteContact), this.httpOptions);
  }

  saveSiteRequestAgreementPopulation(siteAgreePop : SiteRequestAgreementPopulation): Observable<any> {
    return this.httpClient.post(this.saveSiteRequestAgreementPopulationURL, JSON.stringify(siteAgreePop), this.httpOptions);
  }

  saveSiteRequestFeature(siteFeature : SiteFeature): Observable<any> {
    return this.httpClient.post(this.saveSiteRequestFeatureURL, JSON.stringify(siteFeature), this.httpOptions);
  }

  saveUnitRequest(unitRequest : UnitRequest): Observable<any> {
    return this.httpClient.post(this.saveUnitRequestURL, JSON.stringify(unitRequest), this.httpOptions);
  }

  deleteUnitRequest(siteContactRequestID : number): Observable<any> {
    return this.httpClient.post(this.deleteUnitRequestURL, JSON.stringify(siteContactRequestID), this.httpOptions);
  }

  deleteSiteRequestContact(siteContactRequestID : number): Observable<any> {
    return this.httpClient.post(this.deleteSiteRequestContactURL, JSON.stringify(siteContactRequestID), this.httpOptions);
  }

  deleteSiteRequestAgreementPopulation(siteAgreementPopulationRequestID : number): Observable<any> {
    return this.httpClient.post(this.deleteSiteRequestAgreementPopulationURL, JSON.stringify(siteAgreementPopulationRequestID), this.httpOptions);
  }

  deleteSiteRequestFeature(siteFeatureMappingRequestID : number): Observable<any> {
    return this.httpClient.post(this.deleteSiteRequestFeatureURL, JSON.stringify(siteFeatureMappingRequestID), this.httpOptions);
  }

  deleteAllSiteContacts(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.deleteAllSiteContactsURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  deleteAllSiteAgreementPopulations(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.deleteAllSiteAgreementPopulationsURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

  deleteAllSiteFeatures(siteRequestID : number): Observable<any> {
    return this.httpClient.post(this.deleteAllSiteFeaturesURL, JSON.stringify(siteRequestID), this.httpOptions);
  }

}