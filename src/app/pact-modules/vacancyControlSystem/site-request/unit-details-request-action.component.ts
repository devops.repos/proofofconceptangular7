import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ToastrService } from 'ngx-toastr';
import { UnitRequest } from '../agency-site-request/agency-site-request.model';
// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "doc-action",
  template: `<mat-icon  matTooltip='Edit Unit' (click)="onView()" class="doc-action-icon" color="primary">edit</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete Unit' class="doc-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class UnitDetailsRequestActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private unitSelected: UnitRequest;
// <mat-icon  matTooltip='Unit History' (click)="onUnitHistroy()" class="doc-action-icon" color="primary">dvr</mat-icon>
  constructor(
    private toastr: ToastrService,
   ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    this.prepareUnitObject();
    this.params.context.componentParent.unitViewInParent(this.unitSelected);
  }

  onDelete() {
    this.prepareUnitObject();
    this.params.context.componentParent.unitDeleteInParent(this.unitSelected);
  }

  prepareUnitObject(){
    this.unitSelected = {
      unitRequestID: this.params.data.unitRequestID,
      name: this.params.data.name,
  
      address: this.params.data.address,
      city: this.params.data.city,
      state: this.params.data.state,
      zip: this.params.data.zip,

      siteRequestID:  this.params.data.siteRequestID,
      siteAgreementPopulationRequestID:  this.params.data.siteAgreementPopulationRequestID,
  
      isSideDoorAllowed: this.params.data.isSideDoorAllowed,
      isVacancyMonitorRequired: this.params.data.isVacancyMonitorRequired,

      isSideDoorAllowedString: this.params.data.isSideDoorAllowedString,
      isVacancyMonitorRequiredString: this.params.data.isVacancyMonitorRequiredString,

      siteAgreementPopulationID: this.params.data.siteAgreementPopulationID,
      primaryServiceAgreementPopName: this.params.data.primaryServiceAgreementPopName,
      agreementPopulationID: this.params.data.agreementPopulationID,
  
      unitType: this.params.data.unitType,
      unitTypeDesc: this.params.data.unitTypeDesc,
  
      unitStatusType: this.params.data.unitStatusType,
      unitStatusDesc: this.params.data.unitStatusDesc,
  
      contractingAgencyType: this.params.data.contractingAgencyType,
      contractAgencyDesc: this.params.data.contractAgencyDesc,
  
      unitFeatures: this.params.data.unitFeatures,
      rentalSubsidies: this.params.data.rentalSubsidies,
  
      unitFeaturesJson: this.params.data.unitFeaturesJson,
      unitFeaturesString: this.params.data.unitFeaturesString,
  
      rentalSubsidyJson: this.params.data.rentalSubsidyJson,
      rentalSubsidyString: this.params.data.rentalSubsidyString,
  
      userName: this.params.data.userName,
      isActive: this.params.data.isActive,

      siteAgreementPopulationSelected: this.params.data.siteAgreementPopulationSelected,
      contractAgencySelected: this.params.data.contractAgencySelected,
      unitTypeSelected: this.params.data.unitTypeSelected,
      unitStatusSelected: this.params.data.unitStatusSelected,
      isVerified:this.params.data.isVerified,

      createdBy : this.params.data.createdBy,
      createdDate : this.params.data.createdDate,
      updatedBy : this.params.data.updatedBy,
      updatedDate :this.params.data.updatedDate


    };
  }

  refresh(): boolean {
    return false;
  }

  onUnitHistroy() {
    this.prepareUnitObject();
    this.params.context.componentParent.unitHistoryInParent(this.unitSelected);
  }


}