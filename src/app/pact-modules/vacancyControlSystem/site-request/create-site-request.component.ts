import { Component, OnInit,  Input, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { siteDemographic, siteRequestDemographic,AgencyData } from '../agency-site-maintenance/agency-site-model';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { Router, Event } from '@angular/router';
import {SiteRequestService} from './site-request.service';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { CommonService } from '../../../services/helper-services/common.service';
import { FormGroup, FormBuilder, Validators,FormArray } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { isNumber } from 'util';

@Component({
  selector: 'app-create-site-request',
  templateUrl: './create-site-request.component.html',
  styleUrls: ['./create-site-request.component.scss']
})
export class CreateSiteRequestComponent implements OnInit {
  siteCreateForm: FormGroup;
  agencyList:AgencyData[];
  savedsites:siteRequestDemographic[];
  sites:siteRequestDemographic[]=[];
  site:siteRequestDemographic 
  userData: AuthData;

  userid :number;
  tabSiteSelectedIndex:number;
  message:string;

  siteCategoryTypes:RefGroupDetails[];
  boroughTypes:RefGroupDetails[];
  requestStatusTypes: RefGroupDetails[];
  requestTypes: RefGroupDetails[];
  isYesNoList : RefGroupDetails[];
  tagencyid:number=0;
 
  commonServiceSub: Subscription;
  userDataSub: Subscription;
  siteAdminSvcSub1: Subscription;
  siteAdminSvcSub2: Subscription;
  siteReqSvcSub: Subscription;

  constructor( private sidenavService: NavService,
    private router: Router,
    private formBuilder: FormBuilder,
    private siteAdminservice : SiteAdminService,
    private userService :UserService, private siteReqSvc : SiteRequestService,
    private toastr: ToastrService,
    private commonService:CommonService) {
     }

  ngOnInit() {
    this.clearsiteInfo();
     
    this.siteCreateForm = this.formBuilder.group({
      siteNameCtrl: ['', [ Validators.maxLength(100)]],
      addressCtrl: ['', [ Validators.maxLength(200)]],
      cityCtrl: ['', [ Validators.maxLength(50)]],
      stateCtrl: ['', [ Validators.maxLength(2)]],
      zipCtrl: ['',[ Validators.maxLength(5)]],
      agencyCtrl: ['',[Validators.required]],
      hdnsiteIDCtrl: [''],
      isHousingProviderSiteCtrl: ['',[Validators.required]],
      siteBoroughTypeCtrl:['',[Validators.required]]
      });


  this.loadagencylist();
  this.getUserRolesInfo();
  this.loadRefGroupDetails();
  setTimeout(() => {
    this.siteCreateForm.get('agencyCtrl').setValue(this.userData.agencyId);
    this.siteCreateForm.get('agencyCtrl').disable();
  }, 1000);
  
}

ngOnDestroy() {
  if(this.commonServiceSub)  this.commonServiceSub.unsubscribe();
  if(this.userDataSub)  this.userDataSub.unsubscribe();
  if(this.siteAdminSvcSub1)  this.siteAdminSvcSub1.unsubscribe();
  if(this.siteAdminSvcSub2)  this.siteAdminSvcSub2.unsubscribe();
  if(this.siteReqSvcSub)  this.siteReqSvcSub.unsubscribe();
}

loadagencylist()
{
  this.siteAdminSvcSub1 = this.siteAdminservice.getAgencyList(null,null)
  .subscribe(
    res => {
      this.agencyList = res.body as AgencyData[];
    },
    error => console.error('Error!', error)
  );
}

loadRefGroupDetails() {
  const siteCategoryRefGroupId = 2; const  siteboroughRefGroupId= 6; const requestStatusTypeId = 70; const requestTypeId = 30; const yesNoTypeId = 7;
  this.commonServiceSub = this.commonService.getRefGroupDetails(siteCategoryRefGroupId + ',' +  siteboroughRefGroupId+ ',' + requestStatusTypeId
              + ',' + requestTypeId + ',' + yesNoTypeId).subscribe(
    res => {
        const data = res.body as RefGroupDetails[];
          this.siteCategoryTypes = data.filter(d => (d.refGroupID === siteCategoryRefGroupId));
          this.boroughTypes = data.filter(d => (d.refGroupID === siteboroughRefGroupId));
          this.requestStatusTypes = data.filter(d => (d.refGroupID === requestStatusTypeId));
          this.requestTypes = data.filter(d => (d.refGroupID === requestTypeId));
          this.isYesNoList = data.filter(d => { return ((d.refGroupID === 7) && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34)) });
           },
    error => console.error('Error!', error)
  );
}

getUserRolesInfo()
{
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if(this.userData) {
         this.userid = this.userData.optionUserId;
        }
      }
    );
}

onSubmit = (): void => {
 // this.markFormGroupTouched(this.siteCreateForm);
 // if (this.siteCreateForm.valid) {
    //this.saveDocument();
 // }
}

private markFormGroupTouched = (formGroup: FormGroup) => {
  (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
    control.markAsTouched();

    if (control.controls) {
      this.markFormGroupTouched(control);

    }
  });
}

addnewSites()
{
  if (this.ValidateNewsites()) 
  {
    let hpSiteCatID = this.siteCategoryTypes.find(it => it.refGroupDetailDescription == "SH-HP").refGroupDetailID;
    let raSiteCatID = this.siteCategoryTypes.find(it => it.refGroupDetailDescription == "SH-RA").refGroupDetailID;
    let isHpSite : boolean = this.siteCreateForm.get('isHousingProviderSiteCtrl').value == "33" ? true : false;
    let siteCategoryTypeID : number = (isHpSite === true ? hpSiteCatID : raSiteCatID);

    // console.log('hpSiteCatID = ' + hpSiteCatID);
    // console.log('raSiteCatID = ' + raSiteCatID);
    // console.log('isHpSite = ' + isHpSite);
    // console.log('siteCategoryTypeID = ' + siteCategoryTypeID);

    let findsite =  this.sites.find(x => x.name.trim().toUpperCase() == this.site.name.trim().toUpperCase());
    this.tagencyid = this.userData.agencyId;
  if (findsite == undefined || findsite == null) {
        const  newitem: siteDemographic = {
          siteID:null,
          siteNo:null,
          agencyID:this.site.agencyID,
          housingProgram:null,
          name:this.site.name,
          address:this.site.address,
          city:this.site.city,
          state:this.site.state,
          zip:this.site.zip,
          locationType:null,
          referralAvailableType:null,
          tcoReadyType:null,
          tcoContractStartDate:null,
          taxCreditType:null,
          maxIncomeForStudio:null,
          maxIncomeForOneBR:null,
          levelOfCareType:null,
          contractType:null,
          contractStartDate:null,
          siteTrackedType:null,
          tadLiasion:null,
          sameAsSiteInterviewAddressType:null,
          interviewAddress:null,
          interviewCity:null,
          interviewState:null,
          interviewZip:null,
          interviewPhone:null,
          siteFeatures:null,
          siteTypeDesc:null,
          agencyName:null,
          agencyNo:null,
          userId:this.userid,
          siteCategoryType: siteCategoryTypeID,
          boroughType:this.site.boroughType
        }
        const  newitemRequest : siteRequestDemographic = {
          siteID:null,
          siteNo:null,
          agencyID:this.userData.agencyId,
          housingProgram:null,
          name:this.site.name,
          address:this.site.address,
          city:this.site.city,
          state:this.site.state,
          zip:this.site.zip,
          locationType:null,
          referralAvailableType:null,
          tcoReadyType:null,
          tcoContractStartDate:null,
          taxCreditType:null,
          maxIncomeForStudio:null,
          maxIncomeForOneBR:null,
          levelOfCareType:null,
          contractType:null,
          contractStartDate:null,
          siteTrackedType:null,
          tadLiasion:null,
          sameAsSiteInterviewAddressType:null,
          interviewAddress:null,
          interviewCity:null,
          interviewState:null,
          interviewZip:null,
          interviewPhone:null,
          siteFeatures:null,
          siteTypeDesc:null,
          agencyAddress:null,
          agencyCity:null,
          agencyState:null,
          agencyZip:null,
          agencyName:null,
          agencyNo:null,
          userId:this.userid,
          siteCategoryType:siteCategoryTypeID,
          boroughType:this.site.boroughType,

          isActive : true,
          isSiteDemographicTabComplete : true,
          isSiteContactTabComplete :false,
          isPrimaryServiceContractTabComplete :false,
          isSiteApprovalTabComplete :false,
          isUnitDetailTabComplete :false,
          isDraft :false,

          agencyRequestID: null,
          categoryTypeDesc:null,
          id:0,
          siteRequestID:null,
          siteType:null,
          userName:this.userData.lanId,
          email1SentDate:null,
          email2SentDate: null,
          responseReceivedDate: null,
          expectedAvailableDate: null,
          IsCapsMandate: null,
          siteRequestType:this.requestTypes.find(it => it.refGroupDetailDescription === 'New Site').refGroupDetailID,
      
          requestedBy : this.userid,
          requestedDate: new Date().toDateString(),
          reviewedBy : null,
          reviewedDate: null,
          approvedType:null,
          addlApproverComments: null,
      
          CreatedBy:this.userid,
          CreatedDate:null,
          UpdatedBy:null,
          UpdatedDate:null,
          RequestStatusType: this.requestStatusTypes.find(it => it.refGroupDetailDescription === 'Draft').refGroupDetailID,
        }
        let that = this;
        this.siteAdminSvcSub2 = this.siteAdminservice.checDuplicateSite(newitem)
                   .subscribe(
                            data => {
                              let checksites = data as boolean
                              if (!checksites)
                              {
                                that.sites.push(newitemRequest);
                                that.clearsiteInfo();
                                that.site.agencyID = that.userData.agencyId;
                                that.toastr.success("Successfully Added", 'Site Added');
                                that.siteCreateForm.get('agencyCtrl').disable();
                              }
                              else
                              {
                                that.toastr.error('duplicate site', 'Adding New site');
                              }
                            },
                            error => {that.toastr.error("checkduplicate", 'Check duplicate');}
                          );
        }
   else {      
    this.toastr.error('duplicate site', 'Adding New site');
   }

  }
}

  ValidateNewsites = (): boolean => {

    if (this.site.agencyID == null || this.site.agencyID == 0 )
    {
       this.message = "please select the Agency"
       this.toastr.error(this.message,"Error");
        return false;
    }

    if (this.site.name == null || this.site.name.trim() == '' )
    {
       this.message = "please enter  site name "
       this.toastr.error(this.message,"Error");
        return false;
    }

    if (this.site.address == null || this.site.address.trim() == '' )
    {
       this.message = "please enter Address"
       this.toastr.error(this.message,"Error");
        return false;
    }

    if (this.site.city == null || this.site.city.trim() == '' )
    {
       this.message = "please enter city"
       this.toastr.error(this.message,"Error");
        return false;
    }

    if (this.site.state == null || this.site.state.trim() == '' )
    {
       this.message = "please enter state"
       this.toastr.error(this.message,"Error");
        return false;
    }

    if (this.site.zip == null || this.site.zip.trim() == '' )
    {
       this.message = "please enter Zip "
       this.toastr.error(this.message,"Error");
        return false;
    }
    if (this.siteCreateForm.get('isHousingProviderSiteCtrl').value == null || this.siteCreateForm.get('isHousingProviderSiteCtrl').value == "" )
    {
       this.message = "please select the Housing Provider Site"
       this.toastr.error(this.message,"Error");
        return false;
    }
   
    if (this.site.boroughType == null || this.site.boroughType == 0 )
    {
       this.message = "please select the  borough"
       this.toastr.error(this.message,"Error");
        return false;
    }

    
    return true;
   
  }


 clearsiteInfo()
 {
  
  
this.site={    siteID:0,
    siteNo:null,
    agencyID:0,
    housingProgram:null,
    name:'',
    address:'',
    city:'',
    state:'',
    zip:'',
    locationType:null,
    referralAvailableType:null,
    tcoReadyType:null,
    tcoContractStartDate:null,
    taxCreditType:null,
    maxIncomeForStudio:null,
    maxIncomeForOneBR:null,
    levelOfCareType:null,
    contractType:null,
    contractStartDate:null,
    siteTrackedType:null,
    tadLiasion:null,
    sameAsSiteInterviewAddressType:null,
    interviewAddress:null,
    interviewCity:null,
    interviewState:null,
    interviewZip:null,
    interviewPhone:null,
    siteFeatures:null,
    siteTypeDesc:null,
    agencyName:null,
    agencyNo:null,
    agencyAddress:null,
    agencyCity:null,
    agencyState:null,
    agencyZip:null,
    userId:10,
    siteCategoryType:null,
    boroughType:null,
    isActive : true,
    isSiteDemographicTabComplete : true,
    isSiteContactTabComplete :false,
    isPrimaryServiceContractTabComplete :false,
    isSiteApprovalTabComplete :false,
    isUnitDetailTabComplete :false,
    isDraft :false,

    agencyRequestID: 0,
    categoryTypeDesc:null,
    id:0,
    siteRequestID:0,
    siteType:null,
    userName:null,
    email1SentDate:null,
          email2SentDate: null,
          responseReceivedDate: null,
          expectedAvailableDate: null,
          IsCapsMandate: false,
          siteRequestType:0,
      
          requestedBy : 0,
          requestedDate: null,
          reviewedBy : 0,
          reviewedDate: null,
          approvedType:0,
          addlApproverComments: null,
      
          CreatedBy:0,
          CreatedDate:null,
          UpdatedBy:0,
          UpdatedDate:null,
          RequestStatusType:0
  }
 
 }

 clearsite()
 {
   this.clearsiteInfo();
   this.site.agencyID = this.tagencyid;
 }

  Savesites()
  {
    let siteDemographic = this.sites[0];

    this.siteReqSvcSub = this.siteReqSvc.saveSiteRequest(siteDemographic)
              .subscribe(
                        data => {
                          if(isNumber(data) && data > 0){
                              this.toastr.success("Successfully Saved", 'Save Success');
                              // siteDemographic.siteRequestID = data; // NEW ID
                              // this.savedsites = []; this.savedsites.push(siteDemographic);
                              // this.siteReqSvc.setSiteCreated(this.savedsites); // next page
                              // console.log("this.siteCreateForm.get('isHousingProviderSiteCtrl').value = " + this.siteCreateForm.get('isHousingProviderSiteCtrl').value);
                              if(this.siteCreateForm.get('isHousingProviderSiteCtrl').value == "33"){
                                // console.log('routing to HP site create process');
                                this.router.navigate(['./admin/new-site-request-detail'], { queryParams: { "siteRequestID": data } });
                              }
                              else{
                                // console.log('routing to RA site create process');
                                this.router.navigate(["/admin/new-ra-site-request/" + data]);
                              }
                          } else{
                            this.toastr.error("The SiteRequestID returned from server is not valid.", 'Site Request');
                            }
                        },
                        error => {this.toastr.error("Save Failed", 'Save Failed');}
                      );
  }

  proceed()
  {
    if (this.sites.length > 0 )
    {
       this.Savesites();
    }
    else {
      this.toastr.warning("There is no new site added, cannot proceed. Please click ‘Add new’ before proceeding.", "Validation Error.");
    }
  }
  
  tabSiteChanged(tabsiteChangeEvent: MatTabChangeEvent): void {
    // /* Saving Data Between Tab Navigation */
    // if (this.dvGroup.touched || this.dvGroup.dirty) {
    //   this.onSave(false);
    // }
    this.tabSiteSelectedIndex = tabsiteChangeEvent.index;
     if (this.sites.length >0) {
      const s =  this.sites[this.tabSiteSelectedIndex]
      this.site = s;
    }
  }

  removeSites()
  {
    if (this.sites.length >0 )
    {
     this.sites.splice(this.tabSiteSelectedIndex,1);
    }
  }


}