
import { FormGroup, FormBuilder, Validators,FormArray } from '@angular/forms';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  OnDestroy
} from '@angular/core';
import {SiteRequestEventService} from '../site-request/site-request-event.service';
import 'ag-grid-enterprise';
import { Subscription } from 'rxjs';
import {SiteRequestAgreementPopulationData} from './site-request.model';
import { ToastrService } from 'ngx-toastr';
import { SiteRequestAgreementPopulation,PrimaryServiceContractAgreementPopulation,UserOptions } from '../agency-site-maintenance/agency-site-model';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { GridOptions } from 'ag-grid-community';
import { SiteRequestService } from './site-request.service';
import {PrimaryServiceContractRequestActionComponent} from './primary-service-contract-request-action.component'
import { CommonService } from '../../../services/helper-services/common.service';
import {AgencySiteRequestReviewService} from '../agency-site-request-review/agency-site-request-review.service';

@Component({
  selector: 'app-primary-service-contract-request',
  templateUrl: './primary-service-contract-request.component.html',
 // styleUrls: ['./primary-service-contract.component.scss']
})
export class PrimaryServiceContractRequestComponent implements OnInit, OnDestroy {
  
  primaryServiceContractForm: FormGroup;
  siteAgrrementPopulationData:SiteRequestAgreementPopulation[]=[];
  primaryServiceAgreementPopulationList : PrimaryServiceContractAgreementPopulation[]=[];
  siteAgreementPopulationModel: SiteRequestAgreementPopulation = {
    siteID : null,
    siteRequestID : null,

    siteAgreementPopulationID : null,
    siteAgreementPopulationRequestID: null,

    siteAgreementPopulationRequestType: null, 

    agreementPopulationID: null,
    primaryServiceAgreementPopName: null,

    rentalSubsidies :null,
    totalUnits: null,
    totalUnitsDataEntered: null,
    isActive : null,
    userId : null
  };

  siteRequestAgreementPopulationData : SiteRequestAgreementPopulationData[];

  message: string;
  @Input() siteRequestID: number;
  @Input() options:UserOptions;
  @Output() unitdetailopenbtnclick = new EventEmitter<SiteRequestAgreementPopulation>(null);
    validationMessages = {
  };

  agSiteReqReviewSvcSub: Subscription;
  siteRequestSvcSub1: Subscription;
  siteRequestSvcSub2: Subscription;
  commonSvcSub: Subscription;
  evtSvcSub: Subscription;
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  context;
  public gridOptions: GridOptions;
  overlayLoadingTemplate: string='';
  overlayNoRowsTemplate: string='';
  
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private confirmDialogService:ConfirmDialogService,
    private agSiteReqReviewSvc : AgencySiteRequestReviewService, private siteRequestSvc : SiteRequestService,
    private commonService : CommonService, private siteRequestEventSvc : SiteRequestEventService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
      sideBar: {
       toolPanels: [
               {
                   id: 'columns',
                   labelDefault: 'Columns',
                   labelKey: 'columns',
                   iconKey: 'columns',
                   toolPanel: 'agColumnsToolPanel',
                   toolPanelParams: {
                       suppressValues: true,
                       suppressPivots: true,
                       suppressPivotMode: true,
                       suppressRowGroups: false
                   }
               },
               {
                   id: 'filters',
                   labelDefault: 'Filters',
                   labelKey: 'filters',
                   iconKey: 'filter',
                   toolPanel: 'agFiltersToolPanel',
               }
           ],
           defaultToolPanel: ''
       }
    } as GridOptions;
    //this.gridOptions.api.hideOverlay();
   
    this.columnDefs = [
     {
       headerName: 'Site Agreement Population Req ID',
      filter: 'agTextColumnFilter',
      field : "siteAgreementPopulationRequestID",
      hide : true
     },
     {
      headerName: 'Site Req ID',
     filter: 'agTextColumnFilter',
     field : "siteRequestID",
     hide : true
    },
     {
       headerName: 'Primary Service Contract',
       field: 'primaryServiceAgreementPopName',
       minWidth: 350, maxWidth: 700,
      filter: 'agTextColumnFilter'
     },
     {
       headerName: 'No. of. Units',
       field: 'totalUnits',
       minWidth: 150, maxWidth: 150,
       filter: 'agTextColumnFilter'
     },
    
     {
         headerName: 'Actions',
         field: 'action',
         minWidth: 100, maxWidth: 100,
         filter: false,
         sortable: false,
         resizable: false,
         pinned: 'left',
         suppressMenu: true,
         cellRenderer: 'actionRenderer',
     }
    ];
   
    this.defaultColDef = {
     sortable: true,
     resizable: true,
     filter: false
    };
     this.rowSelection = 'single';
     this.pagination = false;
     this.context = { componentParent: this };
     this.frameworkComponents = {
      actionRenderer: PrimaryServiceContractRequestActionComponent
    };
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the primary service contracts are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No primary service contract are available</span>';
  }


  ngOnInit() {
    let that = this;
    this.primaryServiceContractForm = this.fb.group({
    agrrementPopulationCtrl: ['', [Validators.required]],
    totalUnitsCtrl: ['', [Validators.required]],
     hdnSiteAgreementPopulationIDCtrl:['']
    });
    this.loadPrimaryServiceContract();
    this.getPrimartSerivceContractList();

    this.siteRequestAgreementPopulationData = [];
  }


  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    let allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {

      if (column.colId !== 'action' ) {
          allColumnIds.push(column.colId);
      }
    });

    this.gridColumnApi.autoSizeColumns(allColumnIds);
    
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  loadPrimaryServiceContract()
   {
    this.commonSvcSub = this.commonService.getAllPrimaryServiceAgreementPopulation()
    .subscribe(
      res => {
        this.primaryServiceAgreementPopulationList= res.body as PrimaryServiceContractAgreementPopulation[];
      },
      error => console.error('Error!', error)
    );    
  }

  onSubmit(): void {
    if (this.siteAgrrementPopulationData && this.siteAgrrementPopulationData.length == 0){
      this.message = "Please enter atleast one Primary Service Contract";
      this.toastr.warning(this.message, "Validation Failed.");
      this.siteRequestEventSvc.emitSiteRequestPrimaryServiceAgreement(this.siteRequestID, false);
      return;
    }
    this.ValidatePrimaryServiceContract();
    this.markFormGroupTouched(this.primaryServiceContractForm);
    if (this.primaryServiceContractForm.valid) {
      //this.saveDocument();
    }
  }

  private markFormGroupTouched (formGroup: FormGroup) {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }
 
  sitePrimartSerivceContractDeleteParent(cell: SiteRequestAgreementPopulation) {
    const title = 'Confirm Delete';
    const primaryMessage = '';
    const secondaryMessage = `Are you sure you want to delete the selected primary service contract?`;
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => this.deleteSitePrimaryServiceContract(cell),
        (negativeResponse) => console.log(),
      );
  }

  getPrimartSerivceContractList() {
    let that = this;
  this.agSiteReqReviewSvcSub = this.agSiteReqReviewSvc.getSiteAgreementProfileRequest(this.siteRequestID)
      .subscribe(
        res => {
          if (res) {
            that.siteAgrrementPopulationData = res as SiteRequestAgreementPopulation[];
            that.RefillFromDatabaseSiteRequestAgreementPopulationData(that.siteAgrrementPopulationData);
          }
        },
        error => console.error('Error in retrieving the primary service contract Data By  ID...!', error)
      );
  }

  RefillFromDatabaseSiteRequestAgreementPopulationData(siteAgreePopData:SiteRequestAgreementPopulation[]){

      this.siteRequestAgreementPopulationData = [];
      if(siteAgreePopData && siteAgreePopData.length > 0){
          siteAgreePopData.forEach(it => {
              let item : SiteRequestAgreementPopulationData = {
                  siteRequestID : it.siteRequestID,
                  siteAgreementPopulationRequestID : it.siteAgreementPopulationRequestID,
                  agreementPopulationID : it.agreementPopulationID,
                  primaryServiceAgreementPopName : it.primaryServiceAgreementPopName,
                  totalUnits : it.totalUnits,
                  totalUnitsDataEntered : it.totalUnitsDataEntered
              }
              this.siteRequestAgreementPopulationData.push(item);
          });
      }
      this.siteRequestEventSvc.emitSiteRequestAgreementPopulationData(this.siteRequestID, this.siteRequestAgreementPopulationData);
  }

  deleteSitePrimaryServiceContract(dataSelected:SiteRequestAgreementPopulation) {
    dataSelected.userId = this.options.optionUserId;
   
    this.siteRequestSvcSub1 = this.siteRequestSvc.deleteSiteRequestAgreementPopulation(dataSelected.siteAgreementPopulationRequestID).subscribe(
      res => {
        this.message = 'primary service contract deleted successfully.';
        this.toastr.success(this.message, 'Delete');
        this.getPrimartSerivceContractList();
      },
      error => {
        this.message = 'Primary service contract did not delete because the Units may be associated.';
        this.toastr.error(this.message, 'Delete');
      }
    );
    
  }

  saveSitePrimaryServiceContract() {
    this.setSitePrimaryServiceContractValidator();

    if (this.primaryServiceContractForm.invalid) {
      this.toastr.warning('select Primary Service Contract and enter no. of. Units', "Validation Failed");
      return;
    }
 
    if (this.ValidatePrimaryServiceContract()) 
    {
      //this.siteContactModel.SiteContactID= this.contactForm.get('hdnContactIDCtrl').value;

      this.siteAgreementPopulationModel.siteRequestID = this.siteRequestID;
      this.siteAgreementPopulationModel.siteAgreementPopulationRequestType = 321;
      this.siteAgreementPopulationModel.isActive = true;
      if (this.siteAgreementPopulationModel.siteAgreementPopulationRequestID != null && this.siteAgreementPopulationModel.siteAgreementPopulationRequestID != 0 )
      {
        this.siteAgreementPopulationModel.userId = this.options.optionUserId;
      }
      else
      {
        this.siteAgreementPopulationModel.siteAgreementPopulationRequestID =0;
        this.siteAgreementPopulationModel.userId = this.options.optionUserId;
      }
      this.siteAgreementPopulationModel.siteAgreementPopulationID = null;
      this.siteRequestSvcSub2 = this.siteRequestSvc.saveSiteRequestAgreementPopulation(this.siteAgreementPopulationModel)
        .subscribe(
          data => {
            this.message = 'Primary Service Contract saved successfully.';
            this.toastr.success(this.message, 'Save');
            this.AddSiteRequestAgreementPopulationUnits(this.siteAgreementPopulationModel);
            this.getPrimartSerivceContractList();
            this.clearValidatorsFoSitePrimaryServiceContract();
            this.clearPrimaryServiceContractFields();
          }, 
          error => {
            this.message = 'Cant save primary service Contract';
            this.toastr.error(this.message, 'Save');
          }
        );
    }

  }


  setSitePrimaryServiceContractValidator() {

    this.primaryServiceContractForm.controls.agrrementPopulationCtrl.setValidators(Validators.compose([Validators.required]));
    this.primaryServiceContractForm.controls.totalUnitsCtrl.setValidators(Validators.compose([Validators.required]));
    this.primaryServiceContractForm.controls.agrrementPopulationCtrl.updateValueAndValidity();
    this.primaryServiceContractForm.controls.totalUnitsCtrl.updateValueAndValidity();
    
  }

  clearValidatorsFoSitePrimaryServiceContract() {
    this.primaryServiceContractForm.controls.agrrementPopulationCtrl.clearValidators();
    this.primaryServiceContractForm.controls.totalUnitsCtrl.clearValidators();
  }

  clearPrimaryServiceContractFields() {
      this.primaryServiceContractForm.get('hdnSiteAgreementPopulationIDCtrl').setValue(0);
      this.primaryServiceContractForm.get('totalUnitsCtrl').setValue(0);
      this.primaryServiceContractForm.get('agrrementPopulationCtrl').setValue('');
      this.siteAgreementPopulationModel = {
        siteID : null,
        siteRequestID : null,
    
        siteAgreementPopulationID : null,
        siteAgreementPopulationRequestID: null,
    
        siteAgreementPopulationRequestType: null, 
    
        agreementPopulationID: null,
        primaryServiceAgreementPopName: null,
    
        rentalSubsidies :null,
        totalUnits: null,
        totalUnitsDataEntered: null,
        isActive : null,
        userId : null
      }; 
  }

  primaryServiceContractViewParent(cell: SiteRequestAgreementPopulation) {
   
    if (cell.siteAgreementPopulationRequestID != null) {
      this.siteAgreementPopulationModel.siteAgreementPopulationRequestID = cell.siteAgreementPopulationRequestID;
    }
    if (cell.siteRequestID != null) {
      this.siteAgreementPopulationModel.siteRequestID = cell.siteRequestID;
    }
    if (cell.agreementPopulationID != null) {
      this.siteAgreementPopulationModel.agreementPopulationID = cell.agreementPopulationID;
    }
    if (cell.totalUnits != null) {
      this.siteAgreementPopulationModel.totalUnits = cell.totalUnits;
    }
    this.unitdetailopenbtnclick.emit(cell);
  }

  primaryServiceContractEditParent(cell: SiteRequestAgreementPopulation) {
   
    if (cell.siteAgreementPopulationRequestID != null) {
      this.siteAgreementPopulationModel.siteAgreementPopulationRequestID = cell.siteAgreementPopulationRequestID;
    }
    if (cell.siteRequestID != null) {
      this.siteAgreementPopulationModel.siteRequestID = cell.siteRequestID;
    }
    if (cell.agreementPopulationID != null) {
      this.siteAgreementPopulationModel.agreementPopulationID = cell.agreementPopulationID;
    }
    if (cell.totalUnits != null) {
      this.siteAgreementPopulationModel.totalUnits = cell.totalUnits;
    }
    // this.unitdetailopenbtnclick.emit(cell);
  }

  ValidatePrimaryServiceContract() : boolean {

    if ((this.siteAgreementPopulationModel.totalUnits == null || this.siteAgreementPopulationModel.totalUnits < 1) && this.siteAgreementPopulationModel.agreementPopulationID != null)
    {
       this.message = "Total units should be greater than 0"
       this.toastr.warning(this.message,"Validation Failed.");
       this.siteRequestEventSvc.emitSiteRequestPrimaryServiceAgreement(this.siteRequestID, false);
        return false;
    }
    if (this.siteAgreementPopulationModel.agreementPopulationID == null && this.siteAgreementPopulationModel.totalUnits != null && this.siteAgreementPopulationModel.totalUnits > 0)
    {
       this.message = "Please select the agreement population "
       this.toastr.warning(this.message,"Validation Failed.");
       this.siteRequestEventSvc.emitSiteRequestPrimaryServiceAgreement(this.siteRequestID, false);
        return false;
    }
    let findPrimaryservicecontract=  this.siteAgrrementPopulationData.find(x => x.agreementPopulationID == this.siteAgreementPopulationModel.agreementPopulationID) ;
    // && (x.siteRequestID == this.siteAgreementPopulationModel.siteRequestID)
    if (findPrimaryservicecontract != null && (this.siteAgreementPopulationModel.siteAgreementPopulationRequestID == null || this.siteAgreementPopulationModel.siteAgreementPopulationRequestID == undefined)) 
    {
      this.message = "Duplicate primary service agreement population "
      this.toastr.warning(this.message,"Validation Failed.");
      this.siteRequestEventSvc.emitSiteRequestPrimaryServiceAgreement(this.siteRequestID, false);
       return false;
    }

    this.siteRequestEventSvc.emitSiteRequestPrimaryServiceAgreement(this.siteRequestID, true);
   // this.toastr.info("No validation Errors on Primary Service Contract Tab.", "Validation passed");
    return true;
  }

  ngOnDestroy() {
    if(this.agSiteReqReviewSvcSub)  this.agSiteReqReviewSvcSub.unsubscribe();
    if(this.siteRequestSvcSub1) this.siteRequestSvcSub1.unsubscribe();
    if(this.siteRequestSvcSub2) this.siteRequestSvcSub2.unsubscribe();
    if(this.commonSvcSub) this.commonSvcSub.unsubscribe();
    if(this.evtSvcSub) this.evtSvcSub.unsubscribe();
  }

  enabledisablecontrol()
  {
    if (this.options.isDisableForInactiveSite){

      this.primaryServiceContractForm.disable();
    }  
    else
    {
      this.primaryServiceContractForm.enable();
    }
  }

  AddSiteRequestAgreementPopulationUnits(siteAgreementPopulationModel: SiteRequestAgreementPopulation){
      let _totalUnitsDataEntered : number = 0;
      if(this.siteRequestAgreementPopulationData && this.siteRequestAgreementPopulationData.length > 0){

            let item = this.siteRequestAgreementPopulationData.find(it => 
              it.siteAgreementPopulationRequestID == siteAgreementPopulationModel.siteAgreementPopulationRequestID);

          if(item && item.totalUnitsDataEntered > 0)  _totalUnitsDataEntered = item.totalUnitsDataEntered;
      }

      this.siteRequestAgreementPopulationData = this.siteRequestAgreementPopulationData.filter(it =>
         it.siteAgreementPopulationRequestID != siteAgreementPopulationModel.siteAgreementPopulationRequestID &&
         it.agreementPopulationID != siteAgreementPopulationModel.agreementPopulationID); // remove existing

         this.siteRequestAgreementPopulationData.push({
              "siteRequestID" : siteAgreementPopulationModel.siteRequestID,
              "siteAgreementPopulationRequestID" : siteAgreementPopulationModel.siteAgreementPopulationRequestID,
              "agreementPopulationID" : siteAgreementPopulationModel.agreementPopulationID,
              "primaryServiceAgreementPopName" : siteAgreementPopulationModel.primaryServiceAgreementPopName,
              "totalUnits" : siteAgreementPopulationModel.totalUnits,
              "totalUnitsDataEntered" : _totalUnitsDataEntered
          });
      this.siteRequestEventSvc.emitSiteRequestAgreementPopulationData(this.siteRequestID, this.siteRequestAgreementPopulationData); // updated.
  }

}