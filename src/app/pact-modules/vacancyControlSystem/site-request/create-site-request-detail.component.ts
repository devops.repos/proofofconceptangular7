import { Component, OnInit,ViewChild, OnDestroy, } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { NgForm, FormGroup, FormBuilder,  } from '@angular/forms';
import { CommonService } from '../../../services/helper-services/common.service';
import { SiteAdminService } from '../agency-site-maintenance/site-admin.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { AgencyData,SiteContact,siteRequestDemographic, UserOptions,siteApproval,RentalSubsidy,UnitFeature } from '../agency-site-maintenance/agency-site-model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { AgencySiteRequestReviewService } from '../agency-site-request-review/agency-site-request-review.service';
import { SiteDemographicRequestComponent } from '../agency-site-request-review/site-demographic.component';
import {SiteContactRequestComponent} from './site-contact-request.component';
import {PrimaryServiceContractRequestComponent} from './primary-service-contract-request.component';
import {UnitDetailsRequestGridComponent} from './unit-details-request-grid.component';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import {SiteRequestService} from './site-request.service';
import {UnitRequest} from '../agency-site-request/agency-site-request.model';
import {SiteRequestAgreementPopulation} from '../agency-site-maintenance/agency-site-model';
import {SiteRequestAgreementPopulationData} from './site-request.model';
import {SiteRequestEventService} from '../site-request/site-request-event.service';

@Component({
  selector: 'app-create-site-request-detail',
  templateUrl: './create-site-request-detail.component.html',
  styleUrls: ['./create-site-request-detail.component.scss']
})
export class CreateSiteRequestDetailComponent implements OnInit, OnDestroy {
  sitecreated : siteRequestDemographic[]=[];
 
  siteDemographicdata : siteRequestDemographic;
  siteStatus : siteRequestDemographic;
  siteApprovaldata :siteApproval;
  agencyInfo:AgencyData;
  siteRequestID: number;
  agencyId:number;
  agencyList:AgencyData[];
  siteContacts:SiteContact[];
  siteFeatureData :Array<number>;
  allcontacts :string;
  siteAgreePopRequestID:number =0;
  primaryserviceContractType:string='';
  unitsDataObj: UnitRequest[];
  totalUnitsByAgreement:number =0;
  
  useroptions: UserOptions ={
  RA : false,
    isPE : false,
    isHP : false,
    isIH : false,
    isDisableForHP:false,
    isDisableForInactiveSite:false,
    isdisableActiveRadiobutton:false,
    isActive  : false,
    optionUserId:0,
    isNewPage:false
};
 
  tabSelectedIndex: number = 0;
  tabSiteSelectedIndex:number=0;
  userData: AuthData;

  userDataSub: Subscription;
  agnySiteReqReviewSvcSub1: Subscription;
  agnySiteReqReviewSvcSub2: Subscription;
  agnySiteReqReviewSvcSub3: Subscription;
  agnySiteReqReviewSvcSub4: Subscription;
  evtSvcSub1: Subscription;
  evtSvcSub2: Subscription;
  evtSvcSub3: Subscription;
  evtSvcSub4: Subscription;
  evtSvcSub5: Subscription;
  evtSvcSub6: Subscription;
  siteReqSvcSub1: Subscription;
  siteReqSvcSub2: Subscription;
  siteReqSvcSub3: Subscription;
  siteAdminSvcSub: Subscription;

  isValidateFormCalled : boolean = false;
  isSiteTabVisible :boolean= true ;
  unitTabVisible:boolean =false;
  submitted : boolean = false; 
  isSaveRequired:boolean= true;
  siteDemographicDataTabStatus = 2;
  siteContactTabStatus = 2;
  siteDraftStatus = 2;
  siteApprovalTabStatus = 2
  sitePrimaryServiceTabStatus = 2;
  siteUnitDetailTabStatus = 2;
  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  siteRequestSiteContactsIsValid :boolean= false ;
  siteRequestSiteDemographicsIsValid :boolean= false ;
  siteRequestPrimaryServiceAgreementIsValid :boolean= false ;

  @ViewChild(SiteDemographicRequestComponent) sitedemographicComp: SiteDemographicRequestComponent;
  @ViewChild(SiteContactRequestComponent) siteContactRequestComp : SiteContactRequestComponent;
  @ViewChild(PrimaryServiceContractRequestComponent) primaryServiceContractRequestComp : PrimaryServiceContractRequestComponent;
  @ViewChild(UnitDetailsRequestGridComponent) unitDetailsRequestGridComp : UnitDetailsRequestGridComponent;

  siteRequestAgreementPopulationData : SiteRequestAgreementPopulationData[];
  
constructor(
  private agnySiteReqReviewSvc: AgencySiteRequestReviewService,
  private router: Router,
  private formBuilder: FormBuilder,private activatedRoute: ActivatedRoute,
  private siteAdminSvc : SiteAdminService, private siteRequestSvc : SiteRequestService,
  private commonService : CommonService,
  private userService :UserService, private siteRequestEventSvc : SiteRequestEventService,
  private confirmDialogService:ConfirmDialogService,
  private toastr: ToastrService
) {
  
  this.siteDemographicdata ={} as siteRequestDemographic;
  this.agencyInfo ={} as AgencyData;
}

ngOnInit() {
      let that = this;
      this.activatedRoute.queryParams.subscribe(params => {
          that.siteRequestID = Number(params.siteRequestID);
          that.populateSiteInforamtion();
          that.siteDemographicdata.siteRequestID = that.siteRequestID;
      });
      this.evtSvcSub2 = this.siteRequestEventSvc.subscribeSiteRequestSiteContacts().subscribe(res => {
        if(res && res.has(that.siteRequestID)) {
            that.siteRequestSiteContactsIsValid = res.get(that.siteRequestID); // siteRequestID...  has??
        } else
            that.siteRequestSiteContactsIsValid = false;
      });
      this.evtSvcSub3 = this.siteRequestEventSvc.subscribeSiteRequestSiteDemographics().subscribe(res => {
        if(res && res.has(this.siteRequestID)) { // siteRequestID..!!!! has??
          that.siteRequestSiteDemographicsIsValid = res.get(that.siteRequestID); // siteRequestID..!!!
        }
        else
          that.siteRequestSiteDemographicsIsValid = false;
      });
      this.evtSvcSub4 = this.siteRequestEventSvc.subscribeSiteRequestPrimaryServiceAgreement().subscribe(res => {
        if(res && res.has(this.siteRequestID)) { // siteRequestID..!!!! has??
          that.siteRequestPrimaryServiceAgreementIsValid = res.get(that.siteRequestID); // siteRequestID..!!!
        }
        else
          that.siteRequestPrimaryServiceAgreementIsValid = false;
      });
      this.evtSvcSub5 = this.siteRequestEventSvc.subscribeSiteRequestAgreementPopulationData().subscribe(res => {
        if(res && res.has(that.siteRequestID)) {
          that.siteRequestAgreementPopulationData = res.get(that.siteRequestID);
        }
      });

  }

getUserRolesInfo()
{
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if(this.userData) {
        if(this.userData.siteCategoryType.length > 0) {
          this.useroptions.RA = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 5);
          this.useroptions.isPE = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 6);
          this.useroptions.isHP = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 7);
          this.useroptions.isIH = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
          this.useroptions.optionUserId = this.userData.optionUserId;  
        }
      
          this.useroptions.isActive = true;
          if (this.useroptions.isIH == true) {
            this.useroptions.isDisableForInactiveSite=false;
            this.useroptions.isDisableForHP = false;
            this.useroptions.isdisableActiveRadiobutton=false;
          }
          else{
            this.useroptions.isDisableForInactiveSite=false;
            this.useroptions.isDisableForHP = false;
            this.useroptions.isdisableActiveRadiobutton=true;
          }
      
        }  
      }
    );
}

   loadSiteContacts()
   {
      if (this.siteRequestID > 0) {
        this.agnySiteReqReviewSvcSub1 = this.agnySiteReqReviewSvc.getSiteRequestContacts(this.siteRequestID)
          .subscribe(
            res => {
              this.siteContacts= res as SiteContact[];
              //this.getallcontacts();
            },
            error => console.error('Error!', error)
          );     
      }
  }



   loadSiteDemographics()
   {
    let that = this;
    this.agnySiteReqReviewSvcSub2 = this.agnySiteReqReviewSvc.getSiteRequestDemographics(this.siteRequestID)
        .subscribe(
            res1 => {
              that.siteDemographicdata = res1 as siteRequestDemographic;
              that.siteFeatureData = that.convertStringToArray(that.siteDemographicdata.siteFeatures)
              that.agencyId=that.siteDemographicdata.agencyID;
              //this.sitedemographicComponent.populateDemographicdata();
              that.siteAdminSvcSub = that.siteAdminSvc.getAgencyList(that.agencyId.toString(),null)
                .subscribe(
                  res2 => {
                    that.agencyList = res2.body as AgencyData[];
                    that.agencyInfo=that.agencyList.find(e => e.agencyID ===that.agencyId);
                  },
                  error => console.error('Error!', error)
                );
            },
            error => console.error('Error!', error)
        );
   }
   
   populateSiteInforamtion()
   {
        if(this.siteRequestID > 0){
          this.getUserRolesInfo();
            this.loadSiteDemographics();
            this.loadSiteContacts();
            this.loadSiteStatus();
        }
  }


convertStringToArray(s:string) : Array<number> {
  if (s != null)
  {
  let array = s.split(',').map(Number);
  return array;
  }
  else
  {
    return null;
  }
}

setTabStatusColor()
{
  if (this.siteStatus != null)
  {
    if (this.siteStatus.isSiteDemographicTabComplete != null) {
      this.siteDemographicDataTabStatus = this.siteStatus.isSiteDemographicTabComplete == true ? 1 : 0;
    }
    if (this.siteStatus.isSiteContactTabComplete != null) {
      this.siteContactTabStatus = this.siteStatus.isSiteContactTabComplete == true ? 1 : 0;
    }
    if (this.siteStatus.isPrimaryServiceContractTabComplete != null) {
      this.sitePrimaryServiceTabStatus = this.siteStatus.isPrimaryServiceContractTabComplete == true ? 1 : 0;
    }
    if (this.siteStatus.isDraft != null) {
      this.siteDraftStatus = this.siteStatus.isDraft == true ? 0 : 1;
    }
    if (this.siteStatus.isSiteApprovalTabComplete != null) {
      this.siteApprovalTabStatus = this.siteStatus.isSiteApprovalTabComplete == true ? 1 : 0;
    }
    if (this.siteStatus.isUnitDetailTabComplete != null) {
      this.siteUnitDetailTabStatus = this.siteStatus.isUnitDetailTabComplete == true ? 1 : 0;
    }
    
  }

}

loadSiteStatus()
   {
    this.agnySiteReqReviewSvcSub3 = this.agnySiteReqReviewSvc.getSiteRequestDemographics(this.siteRequestID)
    .subscribe(
      res1 => {
        this.siteStatus = res1 as siteRequestDemographic;
        this.setTabStatusColor();
        },
      error => console.error('Error!', error)
    );
   }


getmodifiedData(modifiedData)
{
  this.siteDemographicdata = modifiedData;
}

OnSave(event : Event)
{
    let that = this;
    let requestStatusType = 568; // UPDATE THE STATUS FROM DRAFT TO  pending approval
    this.siteReqSvcSub3 = this.siteRequestSvc.saveSiteRequestStatus(this.siteRequestID, requestStatusType, this.userData.optionUserId)
    .subscribe(
      data => {
        that.showPopupEmailSentOnSubmit('Initiating a New Site request process.');
    },
      error => {this.toastr.error("Save Failed", 'Save Failed');}
    );
}

saveDemographicData(){
  if(this.sitedemographicComp)
    this.sitedemographicComp.onSubmit();

  if (this.siteRequestSiteDemographicsIsValid)  {
    this.siteReqSvcSub2 = this.siteRequestSvc.saveSiteRequestDemographics(this.siteDemographicdata)
                      .subscribe(
                        data => {
                          this.toastr.success("Successfully Saved", 'Save Success');
                          this.loadSiteDemographics();
                      },
                        error => {this.toastr.error("Save Failed", 'Save Failed');}
                      );
  }
}

nextPage() {
    if (this.tabSelectedIndex == 0) {
      this.saveDemographicData();
    }
    if (this.tabSelectedIndex < 2) {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
    if(this.isValidateFormCalled == false)
      this.validateForm();
}

previousPage() {
    if (this.tabSelectedIndex != 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
    if(this.isValidateFormCalled == false)
      this.validateForm();
}

tabChanged(tabChangeEvent: MatTabChangeEvent): void {
  if (this.tabSelectedIndex == 0) {
    this.saveDemographicData();
  }
  this.loadSiteStatus();
  if(this.isValidateFormCalled == false)
    this.validateForm();
}

tabSiteChanged(tabsiteChangeEvent: MatTabChangeEvent): void {
 
  this.tabSiteSelectedIndex = tabsiteChangeEvent.index;
    if (this.sitecreated.length > 0)
    {
        this.siteRequestID=this.sitecreated[tabsiteChangeEvent.index].siteRequestID;
            this.getUserRolesInfo();
            this.loadSiteDemographics();
            this.loadSiteContacts();
            this.loadSiteStatus();
            this.tabSelectedIndex=0;
    }
}

getallcontacts()
{
  let keys = ['firstName', 'lastName'];
   this.allcontacts = this.obsKeysToString(this.siteContacts,keys,',');
}

obsKeysToString(o, k, sep) {
  return k.map(key => o[key]).filter(v => v).join(sep);
 }


  showInfo(s:number)
  {
            this.siteRequestID=s;
            this.getUserRolesInfo();
            this.loadSiteDemographics();
            this.loadSiteContacts();
  }

  unitCloseClick(t:boolean)
  {
  this.unitTabVisible= t;
  this.tabSelectedIndex = 2;
  this.loadSiteStatus();
  }


  unitDetailOpenClick(param : SiteRequestAgreementPopulation)
  {
    /*alert (param.siteAgreementPopulationID);*/
    this.siteAgreePopRequestID = param.siteAgreementPopulationRequestID;
    this.primaryserviceContractType=param.primaryServiceAgreementPopName;
    this.totalUnitsByAgreement = param.totalUnits
    this.GetAllUnitsBySite();
    this.tabSelectedIndex = 2;

  }


    GetAllUnitsBySite()
   {
    this.agnySiteReqReviewSvcSub4 = this.agnySiteReqReviewSvc.getSiteRequestUnits(this.siteRequestID)
    .subscribe(
      res1 => {
        this.FormatUnitsDataObject(res1);  
        },
      error => console.error('Error!', error)
    );
   }   

   FormatUnitsDataObject(res){
    let unitList = res as UnitRequest[];
    let newUnitList : UnitRequest[] = [];
    newUnitList = unitList.map(unit => {
      
      unit.unitFeatures = JSON.parse(unit.unitFeaturesJson);
      unit.unitFeaturesString = this.getFeaturesString(unit.unitFeatures);

      unit.rentalSubsidies = JSON.parse(unit.rentalSubsidyJson);
      unit.rentalSubsidyString = this.getSubsidiesString(unit.rentalSubsidies);
      
      return unit;
    });
  

    if (this.siteAgreePopRequestID !=0 ) {
      this.unitsDataObj = newUnitList.filter(d => (d.siteAgreementPopulationID=== this.siteAgreePopRequestID))
    }
      else {
      this.unitsDataObj = newUnitList; // massaged object for GRID
      }
      if  (this.unitsDataObj.length < this.totalUnitsByAgreement ) {
        this.unitTabVisible= true;
       }    
       else {   
       this.toastr.info("Can't add units", 'Unit details')
       }

  }

  getFeaturesString(UnitFeaturesArray : UnitFeature[]){
    let featuresString : string = null;

    if(UnitFeaturesArray) {
      UnitFeaturesArray.forEach(feature => {
        featuresString = featuresString ? (featuresString + ' ,' + feature.f_desc) : feature.f_desc;
      });
      return featuresString.substring(0, featuresString.length);
    }
    return ''; 
  }

  getSubsidiesString(rentSubsidyArray : RentalSubsidy[]){
    let subsidiesString : string = null;

    if(rentSubsidyArray) {
      rentSubsidyArray.forEach(subsidy => {
        subsidiesString = subsidiesString ? (subsidiesString + ' ,' + subsidy.sub_desc) : subsidy.sub_desc;
      });
      return subsidiesString.substring(0, subsidiesString.length);
    }
    return ''; 
  }

  ngOnDestroy() {
    if(this.userDataSub)  this.userDataSub.unsubscribe();
    if(this.agnySiteReqReviewSvcSub1) this.agnySiteReqReviewSvcSub1.unsubscribe();
    if(this.agnySiteReqReviewSvcSub2) this.agnySiteReqReviewSvcSub2.unsubscribe();
    if(this.agnySiteReqReviewSvcSub3) this.agnySiteReqReviewSvcSub3.unsubscribe();
    if(this.agnySiteReqReviewSvcSub4) this.agnySiteReqReviewSvcSub4.unsubscribe();

    if(this.siteReqSvcSub1) this.siteReqSvcSub1.unsubscribe();
    if(this.siteReqSvcSub2) this.siteReqSvcSub2.unsubscribe();
    if(this.siteReqSvcSub3) this.siteReqSvcSub3.unsubscribe();
    if(this.siteAdminSvcSub)  this.siteAdminSvcSub.unsubscribe();
    if(this.evtSvcSub1) this.evtSvcSub1.unsubscribe();
    if(this.evtSvcSub2) this.evtSvcSub2.unsubscribe();
    if(this.evtSvcSub3) this.evtSvcSub3.unsubscribe();
    if(this.evtSvcSub4) this.evtSvcSub4.unsubscribe();
    if(this.evtSvcSub5) this.evtSvcSub5.unsubscribe();
    if(this.evtSvcSub6) this.evtSvcSub6.unsubscribe();
  }

  get IsFormDataValidInTabs() : boolean {
      if(this.siteRequestAgreementPopulationData && this.siteRequestAgreementPopulationData.length == 0)
        return false;

      let siteRequestAgreementPopulationUnitsIsValid : boolean = this.validateUnitRecords();

      if(this.siteRequestSiteContactsIsValid && this.siteRequestSiteDemographicsIsValid && this.siteRequestPrimaryServiceAgreementIsValid && siteRequestAgreementPopulationUnitsIsValid){
        return true;
      }
      else
        return false;
  }

  validateForm(){
    this.isValidateFormCalled = true;
    let that = this;

    setTimeout(() => {
      if(that.sitedemographicComp && that.tabSelectedIndex != 0) that.sitedemographicComp.onSubmit();
    }, 500);
    setTimeout(() => {
      if(that.siteContactRequestComp && that.tabSelectedIndex != 1) that.siteContactRequestComp.onSubmit();
    }, 500);
    setTimeout(() => {
      if(that.primaryServiceContractRequestComp && that.tabSelectedIndex != 2) that.primaryServiceContractRequestComp.onSubmit();
    }, 500);
    setTimeout(() => {
      if(that.unitDetailsRequestGridComp && that.tabSelectedIndex != 3) that.unitDetailsRequestGridComp.onSubmit();
    }, 500);
    
    if(this.tabSelectedIndex == 2)
      this.showValidationMessage();

    setTimeout(() => {
        that.isValidateFormCalled = false;
    }, 1500);
  }
 
    showValidationMessage(){
      if((this.siteRequestAgreementPopulationData && this.siteRequestAgreementPopulationData.length == 0) || this.validateUnitRecords() == false){
        this.toastr.warning("Unit details are mandatory, please complete the unit roster to proceed.", "Validation Error Message.");
        return;
      }
    }

    validateUnitRecords() : boolean{
      let siteRequestAgreementPopulationUnitsIsValid : boolean = true;

      if(this.siteRequestAgreementPopulationData == null || this.siteRequestAgreementPopulationData == undefined || this.siteRequestAgreementPopulationData.length == 0){
        return false;
      }

      let _siteRequestAgreementPopulationData = this.siteRequestAgreementPopulationData;
      if(this.siteRequestAgreementPopulationData && this.siteRequestAgreementPopulationData.length > 0){
        _siteRequestAgreementPopulationData = this.siteRequestAgreementPopulationData.filter(it => it.siteRequestID == this.siteRequestID);
      }
  
      if(_siteRequestAgreementPopulationData && _siteRequestAgreementPopulationData.length > 0){
        for(let item of _siteRequestAgreementPopulationData){
          if(item.totalUnits > item.totalUnitsDataEntered){
            siteRequestAgreementPopulationUnitsIsValid = false;
            break;
          }
        }
      } else{
        siteRequestAgreementPopulationUnitsIsValid = false;
      }
      
      return siteRequestAgreementPopulationUnitsIsValid;
    }

    showPopupEmailSentOnSubmit(msg : string) {
      let that = this;
      const title = 'Confirmation';
      const primaryMessage = '';
      const secondaryMessage = msg; //`An email has been sent to the user to request a new agency/site.`;
      const confirmButtonName = 'Ok';
      const dismissButtonName = null;

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          (positiveResponse) => {
              that.toastr.success('Successfully Saved', '');
              that.router.navigate(['/dashboard']);
          },
          (negativeResponse) => console.log(),
        );
    }

}