import { Component, OnInit, Input, AfterViewInit, ChangeDetectorRef, OnDestroy, Output,
  EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl, FormControl } from '@angular/forms';
import { AgencySiteRequestReviewService } from '../agency-site-request-review/agency-site-request-review.service';
import {SiteRequestEventService} from '../site-request/site-request-event.service';
import { ConfirmDialogService } from '../../../shared/confirm-dialog/confirm-dialog.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import {SiteRequestService} from './site-request.service';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { ToastrService } from 'ngx-toastr';
import {SiteRequestAgreementPopulationData} from './site-request.model';
import { Router } from '@angular/router';

import { GridOptions } from 'ag-grid-community';
import {UnitDetailsRequestActionComponent} from './unit-details-request-action.component';
import {SiteAdminService} from '../agency-site-maintenance/site-admin.service';
import {RentalSubsidy, UnitFeature,PrimaryServiceContractAgreementPopulation, StatusBarHeader,siteDemographic,siteRequestDemographic, SiteRequestAgreementPopulation} from '../agency-site-maintenance/agency-site-model';
import { MatDialog } from '@angular/material';
import { UnitRequest } from '../agency-site-request/agency-site-request.model';
import { UserSiteType, ReferralStatusType } from 'src/app/models/pact-enums.enum';

@Component({
    selector: 'unit-details-request-grid',
    templateUrl: './unit-details-request-grid.component.html',
  //  styleUrls: ['./unit-details-maintenanace-grid.component.scss']
  })

  export class UnitDetailsRequestGridComponent implements OnInit, OnDestroy { 

    unitDetailsFormGroup: FormGroup;

    @Input() maxTotalUnits : number = 0; 
    @Input() siteRequestID : number; 
    @Input() siteAgreePopRequestID:number;
    @Output() unitclosebtnclick = new EventEmitter();

    @Input() readonlymode : boolean = false ;
    
    public gridOptions: GridOptions;

    userDataSub: Subscription;
    commonServiceSub: Subscription;
    commonServiceSub2: Subscription;
    UnitServiceSub1: Subscription;
    UnitServiceSub2: Subscription;
    UnitServiceSub3: Subscription;
    evtSvcSub1: Subscription;
    evtSvcSub2: Subscription;
    evtSvcSub3: Subscription;

    siteRequestSvcSub1: Subscription;
    siteRequestSvcSub2: Subscription;
    agencySiteReviewSvcSub: Subscription;
 
    contractAgencyTypes: RefGroupDetails[] = [];
    unitFeatures: RefGroupDetails[] = [];
    rentalSubsidies: RefGroupDetails[] = [];
    unitTypes: RefGroupDetails[] = [];
    unitStatuses: RefGroupDetails[] = [];
    primarySvcContractTypes: SiteRequestAgreementPopulation[] = [];
    unitidVisible:boolean= false;
    siteDetail:siteDemographic;
    unitAddressVisible:boolean = false;
    currentSiteLocationType: number;

    pagination: any;
    rowSelection: any;
    context: any;
    columnDefs: any;
    columnDefs_HP: any;
    
    defaultColDef: any;
    frameworkComponents: any;

    unitsDataObj: UnitRequest[];
    totalUnits: number = 0;
    unitSelected: UnitRequest = new UnitRequest();
    UnitHistoryHeader : StatusBarHeader;
    SaveLabel : string;

    userData: AuthData;
    is_SH_PE = false;
    is_SH_HP = false;
    is_SH_RA = false;
    is_CAS = false;
    
    siteRequestAgreementPopulationData : SiteRequestAgreementPopulationData[];

    validationMessages = {
  
      contractAgencyCtrl: {
        'required': 'Please select Contracting Agency.',
      },
      unitNameCtrl: {
          'required': 'Please enter Unit Name.',
      },
      unitTypeCtrl: {
        'required': 'Please select Unit Type.',
      },
      unitStatusCtrl: {
        'required': 'Please select Unit Status.',
      },
      unitFeaturesCtrl: {
        'required': 'Please select Unit Features.',
      },
      rentalSubsidyCtrl: {
        'required': 'Please select Rental Subsidies.',
      },  
  
    }

    formErrors = {
      contractAgencyCtrl: '',
      unitNameCtrl: '',
      unitTypeCtrl: '',
      unitStatusCtrl:'',
      unitFeaturesCtrl:'',
      rentalSubsidyCtrl:'',


    };

    constructor(
        private toastr: ToastrService,
        private router: Router,
        private userService: UserService,
        private commonService: CommonService, private siteRequestEventSvc : SiteRequestEventService,
        private confirmDialogService: ConfirmDialogService, private agencySiteReviewSvc : AgencySiteRequestReviewService,
        private siteAdminSvc : SiteAdminService, private siteRequestSvc : SiteRequestService,
        private formBuilder: FormBuilder, 
        public dialog: MatDialog
        ) { 

        this.rowSelection = 'single';
        this.pagination = true;
        this.context = { componentParent: this };

          this.gridOptions = {
            rowHeight: 35,
            sideBar: {
              toolPanels: [
                      {
                          id: 'columns',
                          labelDefault: 'Columns',
                          labelKey: 'columns',
                          iconKey: 'columns',
                          toolPanel: 'agColumnsToolPanel',
                          toolPanelParams: {
                              suppressValues: true,
                              suppressPivots: true,
                              suppressPivotMode: true,
                              suppressRowGroups: false
                          }
                      },
                      {
                          id: 'filters',
                          labelDefault: 'Filters',
                          labelKey: 'filters',
                          iconKey: 'filter',
                          toolPanel: 'agFiltersToolPanel',
                      }
                  ],
                  defaultToolPanel: ''
              }
          } as GridOptions;

          this.frameworkComponents = {
            actionRenderer: UnitDetailsRequestActionComponent
          };

            this.columnDefs = [
                {
                    headerName: 'Actions',
                    field: 'action',
                    width: 80,
                    filter: false,
                    sortable: false,
                    resizable: false,
                    pinned: 'left',
                    suppressMenu: true,
                    suppressSizeToFit: true,
                    cellRenderer: 'actionRenderer',
                },
                {
                  headerName: 'Primary Service Agreement Pop',
                  field: 'primaryServiceAgreementPopName',
                  width: 150,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Contracting Agency',
                  field: 'contractAgencyDesc',
                  width: 130,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Unit#',
                  field: 'unitID',
                  width: 70,
                  hide: true
                },
                {
                    headerName: 'Unit Name',
                    field: 'name',
                    width: 150,
                    filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Unit Type',
                  field: 'unitTypeDesc',
                  width: 150,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Unit Status',
                  field: 'unitStatusDesc',
                  width: 100,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Address',
                  field: 'address',
                  width: 100,
                  filter: 'agTextColumnFilter',
                  hide: true
                },
                {
                  headerName: 'City',
                  field: 'city',
                  width: 100,
                  filter: 'agTextColumnFilter',
                  hide: true
                },
                {
                  headerName: 'State',
                  field: 'state',
                  width: 70,
                  filter: 'agTextColumnFilter',
                  hide: true
                },
                {
                  headerName: 'Zip',
                  field: 'zip',
                  width: 70,
                  filter: 'agTextColumnFilter',
                  hide: true
                },
                {
                  headerName: 'Unit Features',
                  field: 'unitFeaturesString',
                  width: 300,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Rental Subsidies',
                  field: 'rentalSubsidyString',
                  width: 300,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Direct Referral',
                  field: 'isSideDoorAllowedString',
                  width: 100,
                  filter: 'agTextColumnFilter',
                  valueGetter(params) {
                      if (params.data.isSideDoorAllowed === true) {
                        return "Yes";
                      } else {
                        return "No";
                      }
                  }
                },
                {
                  headerName: 'Monitor Vacancy',
                  field: 'isVacancyMonitorRequiredString',
                  width: 100,
                  filter: 'agTextColumnFilter',
                  valueGetter(params) {
                      if (params.data.isVacancyMonitorRequired === true) {
                        return "Yes";
                      } else {
                        return "No";
                      }
                  }
                }
              ];
              this.columnDefs_HP = [
                {
                    headerName: 'Actions',
                    field: 'action',
                    width: 80,
                    filter: false,
                    sortable: false,
                    resizable: false,
                    pinned: 'left',
                    suppressMenu: true,
                    suppressSizeToFit: true,
                    cellRenderer: 'actionRenderer',
                },
                {
                  headerName: 'Primary Service Agreement Pop',
                  field: 'primaryServiceAgreementPopName',
                  width: 150,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Contracting Agency',
                  field: 'contractAgencyDesc',
                  width: 130,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Unit#',
                  field: 'unitID',
                  width: 70,
                  hide: true
                },
                {
                    headerName: 'Unit Name',
                    field: 'name',
                    width: 150,
                    filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Unit Type',
                  field: 'unitTypeDesc',
                  width: 150,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Unit Status',
                  field: 'unitStatusDesc',
                  width: 100,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Address',
                  field: 'address',
                  width: 100,
                  filter: 'agTextColumnFilter',
                  hide: true
                },
                {
                  headerName: 'City',
                  field: 'city',
                  width: 100,
                  filter: 'agTextColumnFilter',
                  hide: true
                },
                {
                  headerName: 'State',
                  field: 'state',
                  width: 70,
                  filter: 'agTextColumnFilter',
                  hide: true
                },
                {
                  headerName: 'Zip',
                  field: 'zip',
                  width: 70,
                  filter: 'agTextColumnFilter',
                  hide: true
                },
                {
                  headerName: 'Unit Features',
                  field: 'unitFeaturesString',
                  width: 300,
                  filter: 'agTextColumnFilter'
                },
                {
                  headerName: 'Rental Subsidies',
                  field: 'rentalSubsidyString',
                  width: 300,
                  filter: 'agTextColumnFilter'
                }
              ];
              this.defaultColDef = {
                sortable: true,
                resizable: true,
                filter: true,
                floatingFilter: true               
              };
        }
    
    ngOnInit() {
        let that = this;
        this.unitDetailsFormGroup = this.formBuilder.group({
          primarySvcContractCtrl: [{"value": this.siteAgreePopRequestID, "disabled": true}, []],
          contractAgencyCtrl: [this.unitSelected.contractAgencySelected, [Validators.required]],
          unitIdCtrl: ['', []],
          unitNameCtrl: [this.unitSelected.name, [Validators.required]],
          unitFeaturesCtrl: ['', [Validators.required]],
          unitFeaturesListCtrl: ['', []],
          rentalSubsidyCtrl: ['', [Validators.required]],
          rentalSubsidyListCtrl: ['', []],
          unitTypeCtrl: [this.unitSelected.unitTypeSelected, [Validators.required]],
          unitStatusCtrl: [this.unitSelected.unitStatusSelected, [Validators.required]],
          unitAddressCtrl: ['', []],
          unitCityCtrl: ['', []],
          unitStateCtrl: ['', []],
          unitZipCtrl: ['', []],
          sideDoorCtrl: ['', []],
          vacancyMonitorCtrl: ['', []],
        });
  
        this.loadRefGroupDetails();
        this.GetAllUnitsBySite(this.siteRequestID);
        this.getSiteDetails();

        this.userDataSub = this.userService.getUserData().subscribe(res => {
            
            that.userData = res;

            if (that.userData ) {
              that.userData.siteCategoryType.forEach(ct => {
                if (ct.siteCategory === UserSiteType.SH_PE) {
                  that.is_SH_PE = true;
                }
                else if (ct.siteCategory === UserSiteType.SH_HP) {
                  that.is_SH_HP = true;
                }
                else if (ct.siteCategory === UserSiteType.SH_RA) {
                  that.is_SH_RA = true;
                }
                else if (ct.siteCategory === UserSiteType.CAS) {
                  that.is_CAS = true;
                }
            });
            if(that.is_SH_HP) that.columnDefs = that.columnDefs_HP; // for HP, no additional columns like VacancyMonitor/SideDoor
            }
        });

        this.evtSvcSub1 = this.siteRequestEventSvc.subscribeSiteRequestAgreementPopulationData().subscribe(res => {
          if(res && res.has(that.siteRequestID)) {
                that.siteRequestAgreementPopulationData = res.get(that.siteRequestID);
          }
        });

        this.evtSvcSub3 = this.siteRequestEventSvc.subscribeSiteRequestLocationType().subscribe(res => {
          if(res && res.has(that.siteRequestID)) {
                that.currentSiteLocationType = res.get(that.siteRequestID); // this is required to show Unit Address for "Scatter" Site Location Type
            }
        });
        this.SaveLabel = "Add";
      }

      get disableAddUnits() : boolean {
        let count = 0;
        if(this.unitsDataObj && this.unitsDataObj.length > 0){
          this.totalUnits = this.unitsDataObj.length;
          count = this.unitsDataObj.length;
        }
        if(count < this.maxTotalUnits || this.unitSelected.unitRequestID > 0)
          return false;
        else
          return true;
      }


      loadRefGroupDetails()  { 
        let that = this;
        const rentalSubsidiesRefGroupId = 15; const unitTypeRefGroupId = 29; const unitFeaturesRefGroupId = 14;
        const unitStatusRefGroupId = 52;  const contractAgencyRefGroupId = 55;
        this.commonServiceSub = this.commonService.getRefGroupDetails(rentalSubsidiesRefGroupId + ',' + unitTypeRefGroupId + ',' + unitFeaturesRefGroupId + ',' 
                                    + contractAgencyRefGroupId + ',' + unitStatusRefGroupId).subscribe(
          res => {
              const data = res.body as RefGroupDetails[];

              that.contractAgencyTypes = data.filter(d => (d.refGroupID === contractAgencyRefGroupId));
              that.unitTypes = data.filter(d => (d.refGroupID === unitTypeRefGroupId));
              that.unitStatuses = data.filter(d => (d.refGroupID === unitStatusRefGroupId && (d.refGroupDetailID === 472 || d.refGroupDetailID === 473 )));
              that.rentalSubsidies = data.filter(d => (d.refGroupID === rentalSubsidiesRefGroupId));
              that.unitFeatures = data.filter(d => (d.refGroupID === unitFeaturesRefGroupId));
          },
          error => console.error('Error!', error)
        );
  
        this.commonServiceSub2 = this.agencySiteReviewSvc.getSiteAgreementProfileRequest(this.siteRequestID).subscribe(
          res => {
                    that.primarySvcContractTypes = res as SiteRequestAgreementPopulation[];
              },
              error => console.error('Error!', error)
        );
      }

      unitFeatureSelected(data){

        let featuresSelected : UnitFeature[] = [];

        data.value.forEach(element => {
          let item = this.unitFeatures.find(it => it.refGroupDetailID === element);
          featuresSelected.push({
            "f_Id": item.refGroupDetailID,
            "f_desc": item.refGroupDetailDescription
          });
        });
        this.unitSelected.unitFeatures = featuresSelected;
      }

      rentalSubsidySelected(data){

          let subsidySelected : RentalSubsidy[] = [];

          data.value.forEach(element => {
            let item = this.rentalSubsidies.find(it => it.refGroupDetailID === element);
            subsidySelected.push({
              "sub_Id": item.refGroupDetailID,
              "sub_desc": item.refGroupDetailDescription
            });
          });
          this.unitSelected.rentalSubsidies = subsidySelected;
      }

      onFirstDataRendered(params) {
        //params.api.sizeColumnsToFit();
      }
    
      onGridReady(params) {
        params.api.setDomLayout('autoHeight');

      }

      GetAllUnitsBySite(siteRequestID){

        this.UnitServiceSub1 = this.siteRequestSvc.getSiteRequestUnits(this.siteRequestID).subscribe(
          res => {
                this.FormatUnitsDataObject(res);           
              },
              error => console.error('Error!', error)
        );
      }

      FormatUnitsDataObject(res){
          let unitList = res as UnitRequest[];
          let newUnitList : UnitRequest[] = [];
          newUnitList = unitList.map(unit => {
            
            unit.unitFeatures = JSON.parse(unit.unitFeaturesJson);
            unit.unitFeaturesString = this.getFeaturesString(unit.unitFeatures);

            unit.rentalSubsidies = JSON.parse(unit.rentalSubsidyJson);
            unit.rentalSubsidyString = this.getSubsidiesString(unit.rentalSubsidies);

            unit.isSideDoorAllowedString = (unit.isSideDoorAllowed === true) ? "Yes" : "No";
            unit.isVacancyMonitorRequiredString = (unit.isVacancyMonitorRequired === true) ? "Yes" : "No";
            
            return unit;
          });

          if (this.siteAgreePopRequestID > 0) {
            this.unitsDataObj = newUnitList.filter(d => (d.siteAgreementPopulationRequestID === this.siteAgreePopRequestID))
            this.loadUnitTypes(this.siteAgreePopRequestID, 0);
            this.loadRentalsubsities(this.siteAgreePopRequestID);
          }
          else {
            this.unitsDataObj = newUnitList; // massaged object for GRID
          }
        
          let that = this;
          if(this.unitsDataObj && this.unitsDataObj.length > 0){
            let unitCount = 0;
            let tempUnitsDataObj = this.unitsDataObj.filter(it => it.siteAgreementPopulationRequestID == this.siteAgreePopRequestID);
            if(tempUnitsDataObj && tempUnitsDataObj.length > 0) unitCount = tempUnitsDataObj.length;

            let _siteRequestAgreementPopulationData = that.siteRequestAgreementPopulationData.find(x => x.siteAgreementPopulationRequestID == this.siteAgreePopRequestID);
            _siteRequestAgreementPopulationData.totalUnitsDataEntered = unitCount;
            that.siteRequestAgreementPopulationData = that.siteRequestAgreementPopulationData.filter(it => it.siteAgreementPopulationRequestID != that.siteAgreePopRequestID);
            that.siteRequestAgreementPopulationData.push(_siteRequestAgreementPopulationData);
            this.siteRequestEventSvc.emitSiteRequestAgreementPopulationData(this.siteRequestID, that.siteRequestAgreementPopulationData);

          }

      }

      getFeaturesString(UnitFeaturesArray : UnitFeature[]){
        let featuresString : string = null;

        if(UnitFeaturesArray) {
          UnitFeaturesArray.forEach(feature => {
            featuresString = featuresString ? (featuresString + ' ,' + feature.f_desc) : feature.f_desc;
          });
          return featuresString.substring(0, featuresString.length);
        }
        return ''; 
      }

      getSubsidiesString(rentSubsidyArray : RentalSubsidy[]){
        let subsidiesString : string = null;

        if(rentSubsidyArray) {
          rentSubsidyArray.forEach(subsidy => {
            subsidiesString = subsidiesString ? (subsidiesString + ' ,' + subsidy.sub_desc) : subsidy.sub_desc;
          });
          return subsidiesString.substring(0, subsidiesString.length);
        }
        return ''; 
      }

      unitViewInParent(unitSelect: UnitRequest) {
        /* if (cell.email != null) {
          this.siteContactModel.email = cell.email;
        }
        if (cell.extension != null) {
          this.siteContactModel.extension = cell.extension;
        }
        else{
          this.siteContactModel.extension = null;
        } */
        unitSelect.siteAgreementPopulationSelected = unitSelect.siteAgreementPopulationRequestID;
        unitSelect.unitStatusSelected = unitSelect.unitStatusType;
        unitSelect.unitTypeSelected = unitSelect.unitType;
        unitSelect.contractAgencySelected = unitSelect.contractingAgencyType;

        this.unitSelected = unitSelect;

        let newfeaturesSelected : number[] = [];
        if(this.unitSelected.unitFeatures){
          this.unitSelected.unitFeatures.forEach(it => {
            newfeaturesSelected.push(it.f_Id);
          });
        }
        this.unitDetailsFormGroup.get('unitFeaturesCtrl').setValue(newfeaturesSelected);

        let newSubsidySelected : number[] = [];
        if(this.unitSelected.rentalSubsidies){
          this.unitSelected.rentalSubsidies.forEach(it => {
            newSubsidySelected.push(it.sub_Id);
          });
        }
        this.unitDetailsFormGroup.get('rentalSubsidyCtrl').setValue(newSubsidySelected);
        this.SaveLabel = "Save";
      }

      unitDeleteInParent(unitDelete: UnitRequest){
        this.UnitServiceSub3 = this.siteRequestSvc.deleteUnitRequest(unitDelete.unitRequestID).subscribe(
          res => {
                  this.GetAllUnitsBySite(this.siteRequestID);
                  this.clearUnit();         
              },
              error => console.error('Error!', error)
        );
      }

      saveUnit(){
        if(this.validateForm() == false) return;

        this.unitSelected.createdBy = this.userData.optionUserId;
        this.unitSelected.updatedBy = this.userData.optionUserId;
        this.unitSelected.userName = this.userData.lanId;
        this.unitSelected.siteRequestID = this.siteRequestID;
        if (this.siteAgreePopRequestID != 0)
        {
         this.unitSelected.siteAgreementPopulationSelected = this.siteAgreePopRequestID;
        }
        this.unitSelected.isActive = true;
        if(this.unitDetailsFormGroup.valid){
          if(this.unitSelected && this.unitSelected.unitRequestID > 0){

          }
          else {
            this.GetFormFieldsIntoUnitDetails();
          }
          if (this.siteAgreePopRequestID != 0)
          {
           this.unitSelected.siteAgreementPopulationSelected = this.siteAgreePopRequestID;
           this.unitSelected.siteAgreementPopulationRequestID = this.siteAgreePopRequestID;
          }
          let that = this;
          this.UnitServiceSub2 = this.siteRequestSvc.saveUnitRequest(this.unitSelected).subscribe(
            res => {
                    that.toastr.success("Unit information is successfully Saved", 'Save Success');
                    that.GetAllUnitsBySite(this.siteRequestID);
                    setTimeout(() => {
                      that.GetAllUnitsBySite(that.siteRequestID);
                    }, 1500);
                    setTimeout(() => {
                      that.clearUnit();
                    }, 1500);
                },
                error => console.error('Error!', error)
          );
        }

      }

      validateForm() : boolean {
        this.resetFormError();

        let messages : any ;
        let key : any;

        if (!this.unitDetailsFormGroup.controls.contractAgencyCtrl.valid ){
          key ="contractAgencyCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.unitDetailsFormGroup.controls.contractAgencyCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=messages[errorKey] + '\n';
            }
          } 
        }
        if (!this.unitDetailsFormGroup.controls.unitNameCtrl.valid ){
          key ="unitNameCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.unitDetailsFormGroup.controls.unitNameCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=messages[errorKey] + '\n';
            }
          } 
        }
        if (!this.unitDetailsFormGroup.controls.unitTypeCtrl.valid ){
          key ="unitTypeCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.unitDetailsFormGroup.controls.unitTypeCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=messages[errorKey] + '\n';
            }
          } 
        }
        if (!this.unitDetailsFormGroup.controls.unitStatusCtrl.valid ){
          key ="unitStatusCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.unitDetailsFormGroup.controls.unitStatusCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=messages[errorKey] + '\n';
            }
          } 
        }
        if (!this.unitDetailsFormGroup.controls.unitFeaturesCtrl.valid ){
          key ="unitFeaturesCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.unitDetailsFormGroup.controls.unitFeaturesCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=messages[errorKey] + '\n';
            }
          } 
        }
        if (!this.unitDetailsFormGroup.controls.rentalSubsidyCtrl.valid ){
          key ="rentalSubsidyCtrl";
          messages= this.validationMessages[key];
          for (const errorKey in this.unitDetailsFormGroup.controls.rentalSubsidyCtrl.errors) {
            if (errorKey) {
              this.formErrors[key] +=messages[errorKey] + '\n';
            }
          } 
        }

        let errormessage = '';
        for (const errorkey in this.formErrors) {
          if (errorkey) {
            errormessage += this.formErrors[errorkey];
          }
        }
        if (errormessage != '') 
        {
          this.toastr.warning(errormessage,"Validation Failed.");
          return false;
        }
        else
        {
          // this.toastr.info("No validation Errors on Unit Details Tab.", "Validation passed");
           return true;
        }

      }
      
    
      clearUnit(){
        this.unitSelected = new UnitRequest();
        this.unitDetailsFormGroup.get('unitFeaturesCtrl').setValue('');
        this.unitDetailsFormGroup.get('rentalSubsidyCtrl').setValue('');
        this.SaveLabel = "Add";
      }

      GetFormFieldsIntoUnitDetails(){
        this.unitSelected.unitRequestID = 0;
        this.unitSelected.contractingAgencyType = this.unitSelected.contractAgencySelected; //this.unitDetailsFormGroup.get('rentalSubsidyCtrl').value;
        this.unitSelected.siteAgreementPopulationRequestID = this.unitSelected.siteAgreementPopulationSelected; //this.unitDetailsFormGroup.get('rentalSubsidyCtrl').value;
        this.unitSelected.unitStatusType = this.unitSelected.unitStatusSelected; //this.unitDetailsFormGroup.get('rentalSubsidyCtrl').value;
        this.unitSelected.unitType = this.unitSelected.unitTypeSelected; //this.unitDetailsFormGroup.get('rentalSubsidyCtrl').value;
      }

      ngOnDestroy() {
        if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
        if(this.commonServiceSub2) this.commonServiceSub2.unsubscribe();
        if(this.userDataSub) this.userDataSub.unsubscribe();
        if(this.UnitServiceSub1) this.UnitServiceSub1.unsubscribe();
        if(this.UnitServiceSub2) this.UnitServiceSub2.unsubscribe();
        if(this.UnitServiceSub3) this.UnitServiceSub3.unsubscribe();
        if(this.siteRequestSvcSub1) this.siteRequestSvcSub1.unsubscribe();
        if(this.siteRequestSvcSub2) this.siteRequestSvcSub2.unsubscribe();
        if(this.agencySiteReviewSvcSub) this.agencySiteReviewSvcSub.unsubscribe();
        if(this.evtSvcSub1) this.evtSvcSub1.unsubscribe();
        if(this.evtSvcSub2) this.evtSvcSub2.unsubscribe();
        if(this.evtSvcSub3) this.evtSvcSub3.unsubscribe();
      }

      onUnitclosebtnclick(): boolean {
         //   this.siteRequestEventSvc.emitSiteRequestUnitsData(this.siteRequestID, this.siteRequestUnitsData);
             this.unitclosebtnclick.emit();
             return true;
      }

      onSubmit(){
        //console.log('submit button clicked');
      }
         
 unitHistoryInParent(unitHistory: UnitRequest){
   var siteinfo ;
/*     this.agencySiteReviewService.getSiteDemogramics(this.siteRequestID.toString())
    .subscribe(
      res1 => {
        siteinfo = res1.body as siteRequestDemographic;
        this.UnitHistoryHeader = {
          AgencyID:siteinfo.agencyID,
          AgencyName :siteinfo.agencyName,
          AgencyAddress:'',
          SiteRequestId: this.siteRequestID,
          SiteName:siteinfo.name,
          SiteAddress:siteinfo.address,
          SiteAgreementPopulationID:unitHistory.siteAgreementPopulationID,
          AgreementpopulationName :unitHistory.primaryServiceAgreementPopName,
          UnitID:unitHistory.unitID,
          UnitName:unitHistory.name
         }
         
           this.dialog.open(UnitHistoryDialogComponent, {
             width: '1200px',
             maxHeight: '550px',
             disableClose: true,
             autoFocus: false,
             data: this.UnitHistoryHeader
           });     
        },
      error => console.error('Error!', error)
    ); */
   }   


   getSiteDetails(){
    let that = this;
    if (this.siteRequestID >0 ) {
            this.agencySiteReviewSvcSub = this.agencySiteReviewSvc.getSiteRequestDemographics(this.siteRequestID)
            .subscribe(
              res1 => {
                          that.siteDetail = res1 as siteRequestDemographic;
                          if (that.siteDetail) {
                              if (that.siteDetail.locationType ) {
                                  if (that.siteDetail.locationType === 233)
                                    that.unitAddressVisible = false;
                                  else
                                    that.unitAddressVisible = true;
                              }
                          }
                    },
              error => console.error('Error!', error)
            );
        }   
  }
         
 loadUnitTypes(siteagrid: number, contractAgencyID : number)
   {
    let that = this;
    this.siteRequestSvcSub1 = this.siteRequestSvc.getAgreementPopulationUnitTypes(siteagrid, contractAgencyID)
    .subscribe(
      res => {
          that.unitTypes = res as RefGroupDetails[];
        },
        error => console.error('Error!', error)
    );
  }

  loadRentalsubsities(siteagrid: number)
   {
    let that = this;
    this.siteRequestSvcSub2 = this.siteRequestSvc.getAgreementPopulationRentalSubsidies(siteagrid)
    .subscribe(
      res => {
            that.rentalSubsidies = res as RefGroupDetails[];
        },
        error => console.error('Error!', error)
    );
  }

  refershdropdowns()
  {
    let contractAgencySelected : number = 0;
    if(this.unitSelected && this.unitSelected.contractAgencySelected) contractAgencySelected = this.unitSelected.contractAgencySelected;

    this.loadUnitTypes(this.siteAgreePopRequestID, contractAgencySelected);
    this.loadRentalsubsities(this.siteAgreePopRequestID);
  }
    
  resetFormError()
  {
        this.formErrors = {
          contractAgencyCtrl: '',
          unitNameCtrl: '',
          unitTypeCtrl: '',
          unitStatusCtrl:'',
          unitFeaturesCtrl:'',
          rentalSubsidyCtrl:''

        }

  }      

  }