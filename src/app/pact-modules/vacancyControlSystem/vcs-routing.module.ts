import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PactLandingPageComponent } from 'src/app/core/pact-landing-page/pact-landing-page.component';
import { ClientAwaitingPlacementComponent } from './client-awaiting-placement/client-awaiting-placement.component';
import { C2VReferralHandshakeComponent } from './client-awaiting-placement/c2v-referral-handshake/c2v-referral-handshake.component';
import { C2VReferralHandshakeSuccessComponent } from './client-awaiting-placement/c2v-referral-handshake-success/c2v-referral-handshake-success.component';
import { C2vSchedulerComponent } from './client-awaiting-placement/c2v-scheduler/c2v-scheduler.component';
import { V2cSchedulerComponent } from './vacancy-listing/v2c-scheduler/v2c-scheduler.component';
import { UnitRosterComponent } from './unit-roster/unit-roster.component';
import { ReferralRosterComponent } from './referral-roster/referral-roster.component';
import { InterviewOutcomeComponent } from './referral-roster/interview-outcome/interview-outcome.component';
import { TenantRosterComponent } from './tenant-roster/tenant-roster.component';
import { TadComponent } from './tad/tad.component';
import { SchedularComponent } from './schedular/schedular.component';
import { SiteTypeMasterAuthGuard } from 'src/app/services/auth/site-type-auth-guards/site-type-master-auth.guard';
import { SiteTypeGuard, MixedAuthGuard, UserRoleGuard } from 'src/app/models/pact-enums.enum';

import { CreateSiteRequestComponent } from './site-request/create-site-request.component';
import { CreateSiteRequestDetailComponent } from './site-request/create-site-request-detail.component';

import { NewAgencySiteRequestComponent } from './agency-site-request/new-agency-site-request.component';
import { NewAgencySiteDocumentComponent } from './agency-site-document/new-agency-site-document.component';
import { AgencySiteRequestQueueComponent } from './agency-site-request-queue/agency-site-request-queue.component';
import { SiteProfileRequestComponent } from './agency-site-request-review/site-profile.component';
import { PrimaryServiceAgreePopMainGridComponent } from './primary-service-agreement-pop-config/MainGrid/primary-service-agreement-pop-mainGrid.component';
import { AddPrimaryServiceContractComponent } from './primary-service-agreement-pop-config/new-primary-service-contract/add-primary-service-contract.component';
import { RequestReviewRaComponent } from './agency-site-request-review-ra/request-review-ra.component';

import { AgencySiteMaintenanceComponent } from './agency-site-maintenance/agency-site-maintenance.component';
import { SiteProfileComponent } from './agency-site-maintenance/site-profile/site-profile.component';

import { VacancyListingComponent } from './vacancy-listing/vacancy-listing.component';
import { V2cReferralHandshakeComponent } from './vacancy-listing/v2c-referral-handshake/v2c-referral-handshake.component';
import { V2cReferralHandshakeSuccessComponent } from './vacancy-listing/v2c-referral-handshake-success/v2c-referral-handshake-success.component';
import { MonitorVacanciesComponent } from './monitor-vacancies/monitor-vacancies.component';
import { CocReferralQueueComponent } from './coc-referral-queue/coc-referral-queue.component';
import { CocReferralDetailsComponent } from './coc-referral-queue/coc-referral-details/coc-referral-details.component';
import { AddCocReferralComponent } from './coc-referral-queue/add-coc-referral/add-coc-referral.component';
import { HomelessInformationComponent } from './referral-package/homeless-information/homeless-information.component';
import { ReferralReadyTrackingComponent } from './referral-package/referral-ready-tracking/referral-ready-tracking.component';
import { ReferralHousingPreferenceComponent } from './referral-package/referral-housing-preference/referral-housing-preference.component';
import { ReferralAndPlacementSummaryComponent } from './referral-package/referral-and-placement-summary/referral-and-placement-summary.component';
import { CreateSiteComponent } from './agency-site-maintenance/site-profile/create-site.component';
import { CreateSiteDetailComponent } from './agency-site-maintenance/site-profile/create-site-detail.component';
import { CreateAgencySiteComponent } from './agency-site-maintenance/site-profile/create-agency-site.component';
import { TransferSiteComponent } from './agency-site-maintenance/site-profile/transfer-site.component';
import { VerifyVacancyComponent } from './monitor-vacancies/verify-vacancy.component';
import { SideDoorClientSearchComponent } from './tenant-roster/side-door-client-search/side-door-client-search.component';
import { SideDoorClientSearchResultComponent } from './tenant-roster/side-door-client-search-result/side-door-client-search-result.component';
import { SideDoorMoveInComponent } from './tenant-roster/side-door-move-in/side-door-move-in.component';
import { UpdateTenantProfileComponent } from './tenant-roster/update-tenant-profile/update-tenant-profile.component';
import { MoveOutTenantComponent } from './tenant-roster/move-out-tenant/move-out-tenant.component';
import { UserAdministrationComponent } from './user-administration/user-administration.component';
import { UserProfileComponent } from './user-administration/user-profile/user-profile.component';
import { PlacementsAwaitingVerificationComponent } from './placements-awaiting-verification/placements-awaiting-verification.component';
import { ClientPlacementsHistoryComponent } from './placements-awaiting-verification/client-placements-history/client-placements-history.component';
import { PlacementClientCaseFolderComponent } from './placements-awaiting-verification/client-placements-history/placement-client-case-folder/placement-client-case-folder.component';
import { PlacementHistoryClientSearchComponent } from './placements-awaiting-verification/placement-history-client-search/placement-history-client-search.component';
import { SiteAgreementRequestComponent } from './site-agreement-request/site-agreement-request.component';
import { TadSubmissionComponent } from './tad-submission/tad-submission.component';
import { TadUnitRosterComponent } from './tad-submission/tad-unit-roster/tad-unit-roster.component';
import { TadReferralRosterComponent } from './tad-submission/tad-referral-roster/tad-referral-roster.component';
import { TadTenantRosterComponent } from './tad-submission/tad-tenant-roster/tad-tenant-roster.component';
import { TadHPComponent } from './tad-submission/tad-hp/tad-hp.component';
import { TadAwaitingVerificationComponent } from './tad-awaiting-verification/tad-awaiting-verification.component';
import { TadVerificationComponent } from './tad-awaiting-verification/tad-verification/tad-verification.component';
import { AdminToolsComponent } from './Administration/admin-tools/admin-tools.component';
import { DeleteReferralComponent } from './Administration/admin-tools/delete-referral.component';
import { ExtendDeterminationComponent } from './Administration/admin-tools/extend-determination.component';
import { LiftSignoffComponent } from './Administration/admin-tools/lift-signoff.component';
import { MoveReferralComponent } from './Administration/admin-tools/move-referral.component';
import { AssignTadLiaisonComponent } from './assign-tad-liaison/assign-tad-liaison.component';
import { AgencyDetailComponent } from './agency-site-maintenance/site-profile/agency-detail.component';
import { AdminReferralHoldComponent } from './Administration/admin-tools/admin-referral-hold/admin-referral-hold.component';
import { UpdateClientComponent } from './Administration/admin-tools/update-client.component';
// import { NotificationResolver } from 'src/app/services/resolvers/notification.resolver';
import { AdminClientCaseFolderComponent } from './Administration/admin-tools/admin-client-case-folder/admin-client-case-folder.component';
import { ReferralRosterResolver } from 'src/app/services/resolvers/referral-roster.resolver';
import { HpAgencySiteResolver } from 'src/app/services/resolvers/hp-agency-site.resolver';
import { RequestRaSiteComponent } from './site-request-ra/site-request-ra.component';
import { LegacyTadComponent } from './legacyTAD/legacytad.component'

const vcsRoutes: Routes = [
  {
    path: '',
    component: PactLandingPageComponent,
    // resolve: { notifications: NotificationResolver },
    children: [
      {
        path: 'vcs',
        children: [
          {
            path: 'client-awaiting-placement',
            component: ClientAwaitingPlacementComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'c2v-referral-handshake',
            component: C2VReferralHandshakeComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'c2v-referral-handshake-success',
            component: C2VReferralHandshakeSuccessComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'c2v-scheduler',
            component: C2vSchedulerComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'referral-package/homeless-information',
            component: HomelessInformationComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'referral-package/referral-ready-tracking',
            component: ReferralReadyTrackingComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'referral-package/housing-preference',
            component: ReferralHousingPreferenceComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'referral-package/referral-and-placement-summary',
            component: ReferralAndPlacementSummaryComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'vacancy-listing',
            component: VacancyListingComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'v2c-referral-handshake',
            component: V2cReferralHandshakeComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'v2c-referral-handshake-success',
            component: V2cReferralHandshakeSuccessComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'v2c-scheduler',
            component: V2cSchedulerComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'monitor-vacancies',
            component: MonitorVacanciesComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'monitor-vacancies/verify-vacancy/:vacancyVerificationID',
            component: VerifyVacancyComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'schedular',
            component: SchedularComponent,
            resolve: { rahpAgenciesSites: HpAgencySiteResolver },
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, SiteTypeGuard.SH_PE, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'coc-referral-queue',
            component: CocReferralQueueComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'coc-referral-queue/coc-referral-details',
            component: CocReferralDetailsComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'add-coc-referral',
            component: AddCocReferralComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'unit-roster',
            component: UnitRosterComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'referral-roster',
            component: ReferralRosterComponent,
            resolve: { rahpAgenciesSites: ReferralRosterResolver },
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, SiteTypeGuard.SH_PE, MixedAuthGuard.CAS_SYS_ADMIN]
            }
          },
          {
            path: 'referral-roster/interview-outcome',
            component: InterviewOutcomeComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_PE, SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN]
            }
          },
          {
            path: 'tenant-roster',
            component: TenantRosterComponent,
            resolve: { rahpAgenciesSites: HpAgencySiteResolver },
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, SiteTypeGuard.CAS, SiteTypeGuard.SH_PE]
            }
          },
          {
            path: 'tenant-roster/side-door-client-search',
            component: SideDoorClientSearchComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'tenant-roster/side-door-client-search-result',
            component: SideDoorClientSearchResultComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'tenant-roster/side-door-move-in',
            component: SideDoorMoveInComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'tenant-roster/update-tenant-profile',
            component: UpdateTenantProfileComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'tenant-roster/move-out-tenant',
            component: MoveOutTenantComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'placements-awaiting-verification',
            component: PlacementsAwaitingVerificationComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'client-placements-history',
            component: ClientPlacementsHistoryComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'placement-history-client-search',
            component: PlacementHistoryClientSearchComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'client-placements-history/client-case-folder',
            component: PlacementClientCaseFolderComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'tad',
            component: TadComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN]
            }
          },
          {
            path: 'legacytad',
            component: LegacyTadComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN]
            }
          },
          { path: 'tad/tad-submission', component: TadSubmissionComponent, canActivate: [SiteTypeMasterAuthGuard], data: { guards: [SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN] } },
          { path: 'tad/unit-roster', component: TadUnitRosterComponent, canActivate: [SiteTypeMasterAuthGuard], data: { guards: [SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN] } },
          { path: 'tad/referral-roster', component: TadReferralRosterComponent, resolve: { rahpAgenciesSites: ReferralRosterResolver}, canActivate: [SiteTypeMasterAuthGuard], data: { guards: [SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN] } },
          { path: 'tad/tenant-roster', component: TadTenantRosterComponent, resolve: { rahpAgenciesSites: HpAgencySiteResolver }, canActivate: [SiteTypeMasterAuthGuard], data: { guards: [SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN] } },
          { path: 'tad/tad-hp', component: TadHPComponent, canActivate: [SiteTypeMasterAuthGuard], data: { guards: [SiteTypeGuard.SH_HP, MixedAuthGuard.CAS_SYS_ADMIN] } },
          { path: 'tad-awaiting-verification', component: TadAwaitingVerificationComponent, canActivate: [SiteTypeMasterAuthGuard], data: { guards: [SiteTypeGuard.CAS] } },
          { path: 'tad-verification', component: TadVerificationComponent, canActivate: [SiteTypeMasterAuthGuard], data: { guards: [SiteTypeGuard.CAS] } },
          {
            path: 'new-agency-site-document', component: NewAgencySiteDocumentComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN]
            }
          },


        ]
      },
      {
        path: 'admin',
        // canActivate: [SiteTypeMasterAuthGuard],
        // data: {
        //   guards: [UserRoleGuard.SYS_ADMIN, UserRoleGuard.MANAGER, UserRoleGuard.SUPERVISOR]
        // },
        children: [
          {
            path: 'admin-tools',
            component: AdminToolsComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS, MixedAuthGuard.SH_PE_MANAGER, MixedAuthGuard.SH_PE_SYS_ADMIN]
            }
          },
          {
            path: 'extendDetermination',
            component: ExtendDeterminationComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'liftSignoff',
            component: LiftSignoffComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'moveReferral',
            component: MoveReferralComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'updateClient',
            component: UpdateClientComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'deleteReferral',
            component: DeleteReferralComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'user-administration',
            component: UserAdministrationComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [UserRoleGuard.SYS_ADMIN, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'user-profile',
            component: UserProfileComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [UserRoleGuard.SYS_ADMIN, SiteTypeGuard.CAS]
            }
          },
          {
            path: 'assign-tad-liaison',
            component: AssignTadLiaisonComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'agency-site-maintenance', component: AgencySiteMaintenanceComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS, UserRoleGuard.SYS_ADMIN]
            }
          },
          {
            path: 'agency-site-maintenance/:agencyID', component: AgencySiteMaintenanceComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS, UserRoleGuard.SYS_ADMIN]
            }
          },
          {
            path: 'createsite', component: CreateSiteComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'createsitedetail', component: CreateSiteDetailComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'createsitedetail/:page', component: CreateSiteDetailComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'transfersite', component: TransferSiteComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'createagencysite', component: CreateAgencySiteComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'site-profile', component: SiteProfileComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS, UserRoleGuard.SYS_ADMIN]
            }
          },
          {
            path: 'editagencydetail', component: AgencyDetailComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'referral-hold',
            component: AdminReferralHoldComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SUPERVISOR, MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.SH_PE_MANAGER, MixedAuthGuard.SH_PE_SYS_ADMIN]
            }
          },
          {
            path: 'client-case-folder/:applicationId',
            component: AdminClientCaseFolderComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'agency-site-request-queue', component: AgencySiteRequestQueueComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'primary-service-agreement-population-config', component: PrimaryServiceAgreePopMainGridComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN]
            }
          },
          {
            path: 'new-agency-site', component: NewAgencySiteRequestComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.CAS]
            }
          },
          {
            path: 'add-primary-service-contract', component: AddPrimaryServiceContractComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN]
            }
          },
          {
            path: 'edit-primary-service-contract/:primaryServiceContractID', component: AddPrimaryServiceContractComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN]
            }
          },
          {
            path: 'agency-site-request-review/:agencyReqID/:siteReqID/:requestTypeID', component: SiteProfileRequestComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'ra-agency-site-request-review/:agencyReqID/:siteReqID/:requestTypeID', component: RequestReviewRaComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.CAS_SYS_ADMIN, MixedAuthGuard.CAS_SUPERVISOR]
            }
          },
          {
            path: 'new-site-request', component: CreateSiteRequestComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.SH_HP_SYS_ADMIN, MixedAuthGuard.SH_RA_SYS_ADMIN, MixedAuthGuard.SH_RA_MANAGER, MixedAuthGuard.SH_HP_MANAGER]
            }
          },
          {
            path: 'new-site-request-detail', component: CreateSiteRequestDetailComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [MixedAuthGuard.SH_HP_SYS_ADMIN, MixedAuthGuard.SH_RA_SYS_ADMIN, MixedAuthGuard.SH_RA_MANAGER, MixedAuthGuard.SH_HP_MANAGER]
            }
          },
          {
            path: 'site-agreement-request', component: SiteAgreementRequestComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_HP]
            }
          },
          {
            path: 'new-ra-site-request/:siteReqID', component: RequestRaSiteComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [SiteTypeGuard.SH_RA]
            }
          },
        ]
      }

    ]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(vcsRoutes)],
  exports: [RouterModule],
  providers: [ReferralRosterResolver, HpAgencySiteResolver]
})
export class VcsRoutingModule { }
