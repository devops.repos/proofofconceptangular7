import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild,
  OnDestroy
} from '@angular/core';
import { HttpEventType } from '@angular/common/http';
import 'ag-grid-enterprise';
import { MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';

import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { NewAgencySiteDocumentService } from './new-agency-site-document.service';
import { VCSAgencySiteDocument } from 'src/app/shared/document/document.model';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-new-agency-site-document',
  templateUrl: './new-agency-site-document.component.html',
  styleUrls: ['./new-agency-site-document.component.scss']
})
export class NewAgencySiteDocumentComponent implements OnInit, OnDestroy {
  userData: AuthData;
  userDataSub: Subscription;
  commonServiceSub : Subscription;

  SERVER_URL = environment.pactApiUrl;
  documentForm: FormGroup;
  docTypes: RefGroupDetails[];
  fileToUpload: File = null;
  progress: number;
  message: string;
  action: string;
  @Input() docTitle: string;
  @Input() agencyReqID: number = 0;
  @Input() siteReqID: number = 0;
  @Input() siteRequestType : number = 0;

  @Output() fileUploadCompleteEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  documentData: VCSAgencySiteDocument = {
    pactFile: null,
    docName: null,
    docExtension: null,
    userID: 0,
    userName: null,
    userEmail: null,
    agencyReqID: 0,
    siteReqID: 0,
    siteRequestType: 0
  };

  validationMessages = {
    docFileCtrl: {
      required: 'File is required.',
      fileExtn:
        'The file format you are attempting to attach is restricted. Please attach an acceptable format.',
      fileName: 'The file name should not contains special characters.'
    },
  };

  formErrors = {
    docFileCtrl: '',
  };

  constructor(
    private fb: FormBuilder,
    private documentService: NewAgencySiteDocumentService,
    private snackBar: MatSnackBar,
    private userService: UserService,
    private confirmDialogService: ConfirmDialogService,
    private toastr: ToastrService,
  ) {
  }

  ngOnInit() {
    /** Getting the CurrentUser Details from UserService */
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    this.documentForm = this.fb.group({
      fileCtrl: [''],
      docFileCtrl: ['', [Validators.required, CustomValidators.fileNameExtn()]],
    });

    if (!this.docTitle) {
      this.docTitle = 'Upload a Document';
    }

    this.documentForm.valueChanges.subscribe(data => {
      this.logValidationErrors(this.documentForm);
    });
  }

  onSubmit()  {
    this.setValidators();

    this.markFormGroupTouched(this.documentForm);
    this.logValidationErrors(this.documentForm);

    if (this.documentForm.valid && !this.isDocNameExists()) {
      this.saveDocument();
    } else {
      this.showValidationErrors();
    }
  }

  isDocNameExists() {
    // const uploadFileName = this.fileToUpload.name;
    // const formDocName = uploadFileName.substr(0, uploadFileName.lastIndexOf('.'));
    // if (this.rowData.filter(x => x.docName.toLowerCase() === formDocName.toLowerCase()).length > 0) {
    //   this.toastr.error('There is already a document with the same name. Please use the different name.');
    //   return true;
    // } else {
      return false;
    // }
  }

  onHandleFileInput(files: FileList)  {
    this.fileToUpload = files[0] as File;
    if (this.fileToUpload) {
      this.documentForm.get('docFileCtrl').setValue(this.fileToUpload.name);
    }
  }

  saveDocument() {
    const uploadFileName = this.fileToUpload.name;
    this.documentData.pactFile = this.fileToUpload;
    this.documentData.docName = uploadFileName.substr(
      0,
      uploadFileName.lastIndexOf('.')
    );
    this.documentData.docExtension = uploadFileName.split('.').pop();

    const formData = new FormData();
    formData.append('SiteReqID',this.siteReqID.toString());
    formData.append('AgencyReqID',this.agencyReqID.toString());
    formData.append('SiteRequestType', this.siteRequestType.toString());

    formData.append('PACTFile', this.documentData.pactFile);
    formData.append('DocName', this.documentData.docName);
    formData.append('DocExtension', this.documentData.docExtension);

    formData.append('UserId', this.userData.optionUserId.toString());
    formData.append('UserName', this.userData.firstName + ' ' + this.userData.lastName);
    formData.append('UserEmail', this.userData.email);

    this.documentService.saveDocument(formData).subscribe(
      data => {
        if (data.type === HttpEventType.Response) {
          
          if(data.body === true){
            this.message = 'Document is uploaded and processed successfully.';
            this.action = 'Validation success.';
            this.toastr.success(this.message, this.action);
            this.ResponseCompleted(true);
            } else {
              this.message = 'There was an error in processing the document. Please check Email to see the validation errors.', 'Document upload Failed!';
              this.action = 'Validation failed.';
              this.toastr.error(this.message, this.action);
              this.ResponseCompleted(false);
            }

        }
      }
    
    );
  }

  ResponseCompleted(response : boolean){
    this.fileUploadCompleteEvent.emit(response);
    this.resetValues(); 
  }

  resetValues() {
    this.message = '';
    this.progress = 0;
    this.documentForm.get('docFileCtrl').setValue('');
    this.clearValidators();
  }

  clearValidators() {
    this.documentForm.controls.docFileCtrl.clearValidators();
    this.documentForm.controls.docFileCtrl.updateValueAndValidity();
  }

  setValidators() {
    this.documentForm.controls.docFileCtrl.setValidators([Validators.required, CustomValidators.fileNameExtn()]);
    this.documentForm.controls.docFileCtrl.updateValueAndValidity();
  }

  showValidationErrors() {
    if (this.formErrors.docFileCtrl) {
      this.toastr.error(this.formErrors.docFileCtrl.toString());
    } 
  }

  logValidationErrors(group: FormGroup = this.documentForm) {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      } else {
        this.formErrors[key] = '';
        if (
          abstractControl &&
          !abstractControl.valid &&
          (abstractControl.touched || abstractControl.dirty)
        ) {
          const messages = this.validationMessages[key];
          for (const errorKey in abstractControl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + ' ';
            }
          }
        }
      }
    });
  }

  private markFormGroupTouched(formGroup: FormGroup)  {
    (Object as any).values(formGroup.controls).forEach((control: FormGroup) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  ngOnDestroy() {
    if(this.userDataSub)  this.userDataSub.unsubscribe();
    if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
  }

}