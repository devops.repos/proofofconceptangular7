import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType,
  HttpHeaders,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class NewAgencySiteDocumentService {
  SERVER_URL = environment.pactApiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) {}

    public saveDocument (formData: FormData): Observable<any>  {
      const saveDocumentURL = `${this.SERVER_URL}Document/SaveDocumentAgencySiteRequest`; // VCS

      return this.httpClient.post(
        saveDocumentURL,
        //  documentData,
        formData,
        {
          reportProgress: true,
          observe: 'events'
        }
      );
    }



}
