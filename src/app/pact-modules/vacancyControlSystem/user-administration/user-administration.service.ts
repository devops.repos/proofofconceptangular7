import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { UserList, UserData, SelectedFilters } from './user-administration.model'
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
  })

  export class UserAdminService {

    private userSelected = new BehaviorSubject<UserList>(null);
    //private filtersSelected = new BehaviorSubject<SelectedFilters>(null);
    private filtersSelected = new SelectedFilters();

    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    
    constructor(private httpClient: HttpClient) { }

    setSelectedUser(data: UserList) {
      this.userSelected.next(data);
      // console.log(' setting clientAwaitingPlacementSelected : ', data);
    }
  
    getSelectedUser(): Observable<UserList> {
      return this.userSelected.asObservable();
    }

    setSelectedFilters(filters: SelectedFilters){
      //this.filtersSelected.next(filters);

      this.filtersSelected = filters;
    }
    getSelectedFilters()  {
      //return this.filtersSelected.asObservable();
      return this.filtersSelected;
    }

    getAgencyList(){
      const getUrl = environment.pactApiUrl + 'UserAdministration/GetAgencyList/';
      return this.httpClient.get(getUrl);
     }
  

    getSiteListByAgency(agencyID: number){
      const getUrl = environment.pactApiUrl + 'UserAdministration/GetSiteListByAgency/';
      return this.httpClient.get(getUrl + agencyID);
     }

     getSiteListByUser(optionUserID: number){
      const getUrl = environment.pactApiUrl + 'UserAdministration/GetSiteListByUser/';
      return this.httpClient.get(getUrl + optionUserID);
     }

    getUserList(selectedFilters: SelectedFilters) {
        const getUrl = environment.pactApiUrl + 'UserAdministration/GetUserList/';
        return this.httpClient.get(getUrl, { params: { agencyID: selectedFilters.agencyID.toString(), siteID: selectedFilters.siteID.toString(), siteTypeID: selectedFilters.typeID.toString() }, observe: 'response'});
      }

    getSelectedUserData(optionUserID: number){
      const getUrl = environment.pactApiUrl + 'UserAdministration/GetSelectedUserData/';
      return this.httpClient.get(getUrl + optionUserID);
    }

    getSiteContact(siteID: number) {
      const getUrl = environment.pactApiUrl + 'UserAdministration/GetSiteContact/';
      return this.httpClient.get(getUrl + siteID);
    }

    saveUserData(user: UserData){
      const postUrl = environment.pactApiUrl + 'UserAdministration/SaveUserData/';
      return this.httpClient.post(postUrl, user, this.httpOptions);
    }

    // getUserListBySuper(optionUserID: number){
    //   const getUrl = environment.pactApiUrl + 'UserAdministration/GetUserListBySuper/';
    //   return this.httpClient.get(getUrl + optionUserID);
    // }

    // saveSupervisorUpdate(users: UserList[]){
    //   const postUrl = environment.pactApiUrl + 'UserAdministration/SaveSupervisorUpdate/';
    //   return this.httpClient.post(postUrl, users, this.httpOptions);
    // }

  }