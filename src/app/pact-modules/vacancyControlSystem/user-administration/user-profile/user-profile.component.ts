import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTabChangeEvent, MatRadioChange, MatSelectChange } from '@angular/material';
import { Subscription, Observable } from 'rxjs';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from "src/app/models/refGroupDetailsDropDown.model";
import { UserRole } from 'src/app/models/pact-enums.enum';
import { UserList, UserData, SiteData, SiteContact, AgencyData, SelectedFilters } from '../user-administration.model';
import { UserAdminService } from '../user-administration.service';
import { UserSiteContactActionComponent } from './site-contact-action.component';
import { ToastrService } from "ngx-toastr";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { AssignSupervisorComponent } from './assign-supervisor-action.component'
import { Router, NavigationStart } from '@angular/router';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {

  userDataSub: Subscription;
  agencyListSub: Subscription;
  siteListSub: Subscription;
  rowDataSub: Subscription;
  commonServiceSub: Subscription;
  selectedUserDataSub: Subscription;
  getSiteContactsSub: Subscription;
  saveUserSub: Subscription;
  //getUserListBySuperSub: Subscription;
  //saveSuperSub: Subscription;
  routeSub: Subscription;

  //selectedTab = 0;

  userForm: FormGroup;

  userData: AuthData;
  agencyList: AgencyData[] = [];
  siteList: SiteData[] = [];
  siteListFull: SiteData[] = [];
  responseItems: RefGroupDetails[];
  userInactiveReasons: RefGroupDetails[];
  selectedAgencyID: number = 0;
  selectedFilters = new SelectedFilters();
  selectedSiteNameUserTab: string;
  rowData: UserList[] = [];
  //userListBySuper: UserList[];
  contactRowData: SiteContact[];
  savedSelectedUserID: number = 0;
  selectedUserData: UserData;
  assignedSites: SiteData[] = [];
  availableSites: SiteData[];
  availableSitesFull: SiteData[];
  roles = UserRole;
  filteredAgencyData: Observable<any[]>;
  //searchStringCtrl = new FormControl();

  is_SYS_ADMIN = false; // PE, HP and RA Sys Admins
  is_SUPER_USER = false; //CAS users

  editMode = false;
  showLiason = false;
  showTransmit = false;
  showContact = false;
  formUpdated = false;
  userActive = true;
  showUpdateSuper = false;

  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  rowSelection;
  frameworkComponents;
  context;
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;

  //contactFrameworkComponents;
  contactDefaultColDef;
  contactColumnDefs;
  contactOverlayLoadingTemplate: string;
  contactOverlayNoRowsTemplate: string;

  superTabColumnDefs;

  public gridOptions: GridOptions;

  keys;

  constructor(private userService: UserService,
    private commonService: CommonService,
    private userAdminService: UserAdminService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private confirmDialogService: ConfirmDialogService,
    private router: Router) {

    this.gridOptions = {
      rowHeight: 35
    } as GridOptions;

    this.contactOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the contact list is loading.</span>';
    this.contactOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Contacts Available</span>';

    this.contactColumnDefs = [
      {
        headerName: 'First Name',
        field: 'firstName'
      },
      {
        headerName: 'Last Name',
        field: 'lastName'
      },
      {
        headerName: 'Title',
        field: 'title'
      },
      {
        headerName: 'Email Address',
        field: 'email'
      },
      {
        headerName: 'Office Phone',
        field: 'phone'
      },
      {
        headerName: 'Extension',
        field: 'extension'
      },
      {
        headerName: 'Cell Phone',
        field: 'alternatePhone'
      },
      {
        headerName: 'Fax Phone',
        field: 'fax'
      },
      {
        headerName: 'System Administrator',
        valueGetter(params) {
          return params.data.isSysAdmin == 1 ? 'Yes' : 'No'
        },
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        //cellRenderer: 'siteContact' 
        cellRendererSelector(params) {
          const actionButton = {
            component: 'siteContact'
          };
          if (!params.data.optionUserID) {
            return actionButton;
          } else {
            return null;
          }
        }
      }
    ];

    // this.superTabColumnDefs = [
    //   {
    //     headerName: 'Name (L,F)',
    //     valueGetter(params) {
    //       return params.data.lastName + ', ' + params.data.firstName;
    //     }
    //   },
    //   {
    //     headerName: 'UserID',
    //     field: 'lanID'
    //   },
    //   {
    //     headerName: 'Agency/Site',
    //     valueGetter(params) {
    //       return params.data.agencyNo + '/' + params.data.siteNo;
    //     }
    //   },
    //   {
    //     headerName: 'Site Supervisor',
    //     field: 'availableSupervisorList',
    //     width: 225,
    //     cellRenderer: 'dropdownRenderer',
    //   }
    // ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.contactDefaultColDef = {
      sortable: false,
      resizable: false,
      filter: false
    };
    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.frameworkComponents = {
      siteContact: UserSiteContactActionComponent,
      dropdownRenderer: AssignSupervisorComponent
    };

    this.keys = Object.keys(this.roles).filter(k => !isNaN(Number(k)));
  }

  ngOnInit() {
    /** Getting the currently active user info */
    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.userData = userdata;
        //if (this.userData.roleId === UserRole.SYS_ADMIN) {
          if (this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8)) {

            this.is_SUPER_USER = true;
          }
          else {
            this.is_SYS_ADMIN = true;

            this.selectedAgencyID = this.userData.agencyId;
          }
        //}

        if (this.is_SYS_ADMIN) {
          this.siteListSub = this.userAdminService.getSiteListByUser(this.userData.optionUserId).subscribe(res => {
            if (res) {
              this.availableSites = this.availableSitesFull = res as SiteData[];
              this.siteList = this.siteListFull = res as SiteData[];
              this.checkDHS();
              this.getSelectedUser();
            }
          });
        }

        /** Get the Agency List */
        if (this.is_SUPER_USER) {
          this.agencyListSub = this.userAdminService.getAgencyList().subscribe(res => {
            if (res) {
              this.agencyList = res as AgencyData[];
              this.filteredAgencyData = this.userForm.controls.agencyID.valueChanges.pipe(
                startWith(''),
                map(value => this._filter(value))
              );
              this.getSelectedUser();
            }
          });
        }
      }

    });

    this.commonServiceSub = this.commonService.getRefGroupDetails("1,7").subscribe(
      res => {
        const data = res.body as RefGroupDetails[];

        this.userInactiveReasons = data.filter(d => { return d.refGroupID === 1; });

        this.responseItems = data.filter(d => {
          return d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34);
        });

      },
      error => console.error("Error!", error)
    );


    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (event.url != "/admin/user-profile") {
          this.userAdminService.setSelectedUser(null);
        }
        if (event.url != "/admin/user-profile" && event.url != "/admin/user-administration") {
          this.selectedFilters.agencyID = 0;
          this.selectedFilters.siteID = 0;
          this.selectedFilters.typeID = 0;
          this.userAdminService.setSelectedFilters(this.selectedFilters);
        }
      }
    });


    this.buildForm();
  }

  buildForm() {
    this.userForm = this.formBuilder.group({
      userContactID: [""],
      optionUserID: [""],
      isUserExternal: ["", Validators.required],
      agencyID: ["", Validators.required],
      //searchAgencyID:["", Validators.required],
      lanID: ["", [Validators.required, Validators.pattern('[a-zA-Z0-9]+')]],
      juniperID: [{ value: "", disabled: true }, Validators.pattern("[a-zA-Z0-9]+")],
      firstName: ["", [Validators.required, Validators.pattern('[a-zA-Z \,\'\-]+')]],
      lastName: ["", [Validators.required, Validators.pattern('[a-zA-Z \,\'\-]+')]],
      roleID: [0, Validators.required],
      nonHRALanID: [{ value: "DHS\\", disabled: true }],
      email: ["", [Validators.required, Validators.email]],
      officePhone: ["", Validators.required],
      officeExt: [""],
      cellPhone: [""],
      faxPhone: [""],
      comments: [""],
      canTransmit: [33],
      vcs: [""],
      isActive: [true],
      inactiveReason: [0],
      inactiveReasonComments: [""],
      isAssignedSupervisor: [""],
      siteContactList: [0],
      lastAccessDate: [{ value: "", disabled: true }],
      createdBy: [{ value: "", disabled: true }],
      createdDate: [{ value: "", disabled: true }],
      updatedBy: [{ value: "", disabled: true }],
      updatedDate: [{ value: "", disabled: true }]

    });


    if (this.selectedAgencyID == 0) {
      this.userForm.disable();
      this.userForm.controls.agencyID.enable();
      //this.userForm.controls.searchAgencyID.enable();
    }
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, true);
    this.gridApi.sizeColumnsToFit(allColumnIds);

  }

  onFormChange() {
    this.formUpdated = true;
  }

  onSelectedContact(cell) {
    if (this.formUpdated) {
      const title = 'Confirm Update';
      const primaryMessage = '';
      const secondaryMessage = `Do you want to overwrite the current data?`;
      const confirmButtonName = 'Yes';
      const dismissButtonName = 'Cancel';

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          (positiveResponse) => {

            //this.userForm.reset();
            this.populateSelectedContact(cell);

          },
          (negativeResponse) => console.log(),
        );
    }
    else {
      this.populateSelectedContact(cell);
    }
  }

  populateSelectedContact(cell) {
    this.userForm.controls.firstName.setValue(cell.firstName);
    this.userForm.controls.lastName.setValue(cell.lastName);
    this.userForm.controls.email.setValue(cell.email);
    this.userForm.controls.officePhone.setValue(cell.phone);
    this.userForm.controls.officeExt.setValue(cell.extension);
    this.userForm.controls.cellPhone.setValue(cell.alternatePhone);
    this.userForm.controls.faxPhone.setValue(cell.fax);
    this.userForm.controls.userContactID.setValue(cell.siteContactID);
    if (cell.isSysAdmin) {
      this.userForm.controls.roleID.setValue("4");
    }
    if ((cell.email.indexOf('dss.nyc.gov') != -1 || cell.email.indexOf('hra.nyc.gov') != -1 || cell.email.indexOf('dhs.nyc.gov') != -1)) {
      this.userForm.controls.isUserExternal.setValue(33);
    }
    else {
      this.userForm.controls.isUserExternal.setValue(34);
    }
    this.formUpdated = true;
  }

  getSelectedUser() {
    this.selectedUserDataSub = this.userAdminService.getSelectedUser().subscribe(user => {
      if (user) {
        this.savedSelectedUserID = user.optionUserID;
        this.getSelectedUserData(this.savedSelectedUserID);
      } else {
        this.savedSelectedUserID = 0;
      }
    })
  }

  getSelectedUserData(savedSelectedUserID: number) {

    this.selectedUserDataSub = this.userAdminService.getSelectedUserData(savedSelectedUserID).subscribe(res => {
      this.selectedUserData = res as UserData;

      if (this.selectedUserData.siteList) {
        this.checkUserFunctionality(this.selectedUserData.siteList);
      } else {
        this.selectedFilters = this.userAdminService.getSelectedFilters();

        this.selectedUserData.agencyID = this.selectedFilters.agencyID;

      }
      this.userForm.patchValue(this.selectedUserData);
      if(this.is_SUPER_USER){
        var agency = this.agencyList.filter(d => { return d.agencyID === this.selectedUserData.agencyID })
        var selectedAgencyName = agency[0].agencyNo + ' - ' + agency[0].agencyName;
        this.userForm.controls.agencyID.setValue(selectedAgencyName);
        this.selectedAgencyID = agency[0].agencyID;
      }

      this.userForm.controls.roleID.setValue(this.selectedUserData.roleID.toString());
      if (this.selectedUserData.siteList && this.selectedUserData.siteList.length > 0) {
        this.assignedSites = this.selectedUserData.siteList;
      }
      else {
        this.assignedSites = []
      }
      this.userForm.controls.isUserExternal.setValue(this.selectedUserData.isUserExternal == true ? 34 : 33);

      if (this.selectedUserData.nonHRALanID) {
        this.userForm.controls.nonHRALanID.setValue("DHS\\" + this.userForm.controls.nonHRALanID.value);
      }
      else {
        this.userForm.controls.nonHRALanID.setValue("DHS\\");
      }
      this.userForm.enable();
      this.userForm.controls.agencyID.disable();
      this.userForm.controls.isUserExternal.disable();
      //this.userForm.controls.searchAgencyID.disable();

      this.disableFields();

      this.getAvailableSites(true);

      this.userActive = this.selectedUserData.isActive;
    });

    this.editMode = true;

  }
  // tabChanged(tabChangeEvent: MatTabChangeEvent): void {

  //   this.selectedTab = tabChangeEvent.index;

  // }

  isActiveChange(event) {

    if (event.value == false && this.editMode && this.selectedUserData.roleID == 4) {
      var canDeactivate = true;
      this.assignedSites.forEach(site => {
        if (!site.canDeassign) {
          canDeactivate = false;
        }
      })
      if (!canDeactivate) {
        this.toastr.error("This user is the only System Administrator for some of the assigned sites.", "Assign a new System Administrator before deactivating the user");
        this.userActive = true;
        this.userForm.controls.isActive.setValue(true);
      }
      else {
        this.userActive = event.value;

        this.userForm.controls.inactiveReason.setValue(0);
        this.userForm.controls.inactiveReasonComments.setValue("");
      }
    }
    else {
      if (event.value == false && this.editMode && this.selectedUserData.roleID == 2) {
        this.toastr.error("The action will de-assign the supervisor from staff, if any.");
      }
      this.userActive = event.value;

      this.userForm.controls.inactiveReason.setValue(0);
      this.userForm.controls.inactiveReasonComments.setValue("");
    }
  }

  getAvailableSites(filter: boolean) {
    if (this.is_SUPER_USER) {
      // this.siteListSub = this.userAdminService.getSiteListByAgency(this.userForm.controls.agencyID.value).subscribe(res => {
      this.siteListSub = this.userAdminService.getSiteListByAgency(this.selectedAgencyID).subscribe(res => {
        if (res) {
          this.availableSitesFull = this.availableSites = res as SiteData[];
          this.checkDHS();
          if (filter) {
            this.filterOutAssignedSites();
          }
        }
      });
    }
    else if (this.is_SYS_ADMIN) {
      this.siteListSub = this.userAdminService.getSiteListByUser(this.userData.optionUserId).subscribe(res => {
        if (res) {
          this.availableSitesFull = this.availableSites = res as SiteData[];
          this.checkDHS();
          if (filter) {
            this.filterOutAssignedSites();
          }
        }
      });
    }
  }

  filterOutAssignedSites() {
    if (this.assignedSites) {
      this.assignedSites.forEach(item => {
        this.availableSites = this.availableSites.filter(obj => {
          return obj.siteID !== item.siteID;
        });
      })
    }
  }

  checkDHS() {
    this.userForm.controls.nonHRALanID.disable();
    var agencyType, agencyNo;
    if (this.is_SUPER_USER) {
      // var agencyID = this.selectedUserData ? this.selectedUserData.agencyID : this.userForm.controls.agencyID.value;
      var agencyID = this.selectedUserData ? this.selectedUserData.agencyID : this.selectedAgencyID;
      var agency = this.agencyList.filter(r => { return r.agencyID == agencyID });
      agencyType = agency[0].agencyType;
      agencyNo = agency[0].agencyNo;
    }
    else if (this.is_SYS_ADMIN) {
      agencyType = this.userData.agencyType;
      agencyNo = this.userData.agencyNo;
    }

    if (agencyType == '10' || agencyType == '48' || agencyType == '50' || agencyNo == "2001") {
      this.userForm.controls.nonHRALanID.enable();
    }
    else {
      this.availableSitesFull.forEach(s => {
        if (s.siteType == '10' || s.siteType == '48' || s.siteType == '50' || s.siteType == "84" || s.siteType == "85") {
          this.userForm.controls.nonHRALanID.enable();
        }
      })

    }
  }
  onAgencySelectedUserTab(agencyID: number) {
    this.selectedAgencyID = agencyID;
    //if (this.userForm.controls.agencyID.value != 0) {
    if (this.selectedAgencyID != 0) {
      this.userForm.enable();
      this.getAvailableSites(false);
    }
    else {
      this.userForm.disable();
      this.userForm.controls.agencyID.enable();
      //this.userForm.controls.searchAgencyID.enable();
      this.availableSitesFull = this.availableSites = [];
    }
    this.assignedSites = [];
    this.showLiason = false;
    this.showTransmit = false;
    this.showContact = false;

    this.userForm.controls.siteContactList.setValue(0);
    this.userForm.controls.canTransmit.setValue(33);
    this.userForm.controls.vcs.setValue(null);

    this.disableFields();
  }

  disableFields() {
    this.userForm.controls.createdBy.disable();
    this.userForm.controls.createdDate.disable();
    this.userForm.controls.updatedBy.disable();
    this.userForm.controls.updatedDate.disable();
    this.userForm.controls.lastAccessDate.disable();
    this.userForm.controls.juniperID.disable();

  }

  sortSiteList() {
    this.availableSites.sort((a, b) => {
      return a.siteNo > b.siteNo ? 1 : -1
    });

    this.assignedSites.sort((a, b) => {
      return a.siteNo > b.siteNo ? 1 : -1
    });
  }
  onClickAvailSiteList(event, siteID: number) {
    if (this.userForm.controls.isActive.value == false) {
      event.preventDefault();
      this.toastr.error("Can not assign site to an inactive user.");
    }
    else {
      this.onAvailSiteListChangeUser(siteID)
    }
  }
  onAvailSiteListChangeUser(siteID: number) {
    if (this.userForm.controls.isActive.value == true) {
      var newSiteSelected = new SiteData();

      newSiteSelected = this.availableSites.filter(r => { return r.siteID === siteID })[0];
      newSiteSelected.canDeassign = true;
      this.assignedSites.push(newSiteSelected);
      this.availableSites = this.availableSites.filter(obj => {
        return obj.siteID !== siteID;
      });
      this.sortSiteList();
      this.checkUserFunctionality(this.assignedSites);
    }

  }
  onClickAssignedSiteList(event, siteID: number) {
    var newSiteSelected = new SiteData();
    newSiteSelected = this.assignedSites.filter(r => { return r.siteID === siteID })[0];
    if (!newSiteSelected.canDeassign && this.selectedUserData && this.selectedUserData.roleID == 4) {

      event.preventDefault();
      // var roleID = this.selectedUserData.roleID;
      this.toastr.error("This user is the only System Administrator for this site.", "Assign a new System Administrator before removing the site");

    }
    else {
      if (this.selectedUserData && this.selectedUserData.roleID == 2) {
        this.toastr.error("The action will de-assign the supervisor from staff, if any.");
      }
      this.onAssignedSiteListChange(newSiteSelected)
    }
  }

  onAssignedSiteListChange(newSiteSelected: SiteData) {
    //if (newSiteSelected.canDeassign) {
    this.availableSites.push(newSiteSelected);
    this.assignedSites = this.assignedSites.filter(obj => {
      return obj.siteID !== newSiteSelected.siteID;
    });
    this.sortSiteList();
    this.showTransmit = false;
    this.showLiason = false;
    this.checkUserFunctionality(this.assignedSites);
    //}
  }

  onSupervisorSelectedUserTab(event: MatSelectChange, siteID: number) {
    if (event.value != 0) {
      this.assignedSites.forEach(item => {
        if (item.siteID == siteID) {
          item.assignedSupervisorID = event.value;
        }
      })
    }

  }

  checkUserFunctionality(sites: SiteData[]) {
    sites.forEach(site => {
      if (site.siteCategoryType == 7) {
        this.showTransmit = true;
        this.showLiason = true;
      }
      if (site.siteCategoryType == 5) {
        this.showTransmit = true;
      }
    })

  }
  onSiteSelectedUserTab(event: MatSelectChange) {
    this.showContact = true;
    var siteSelected = this.availableSitesFull.filter(r => { return r.siteID === event.value })[0];

    this.selectedSiteNameUserTab = 'Site Contacts for Site ' + siteSelected.siteNo + ' - ' + siteSelected.siteName;

    this.getSiteContactsSub = this.userAdminService.getSiteContact(event.value).subscribe(res => {
      if (res) {
        this.contactRowData = res as SiteContact[];
      }
    });
  }

  validateUser() {
    var isUserExternal = this.userForm.controls.isUserExternal.value;
    var lanID = this.userForm.controls.lanID.value;
    var agencyID = agencyID = this.selectedAgencyID;
    // if (this.is_SUPER_USER) {
    //   agencyID = this.userForm.controls.agencyID.value;
    // } else {
    //   agencyID = this.selectedAgencyID;
    // };

    var firstName = this.userForm.controls.firstName.value;
    var lastName = this.userForm.controls.lastName.value;
    var roleID = this.userForm.controls.roleID.value;
    var email = this.userForm.controls.email.value;
    var phone = this.userForm.controls.officePhone.value;
    var isActive = this.userForm.controls.isActive.value;
    var inactiveReason = this.userForm.controls.inactiveReason.value;
    var inactiveReasonComments = this.userForm.controls.inactiveReasonComments.value;
    var vcs = this.userForm.controls.vcs.value;
    var canTransmit = this.userForm.controls.canTransmit.value;

    if (!isUserExternal) {
      this.toastr.error("Select if user is HRA staff.");
      return false;
    }
    if (agencyID === 0) {
      this.toastr.error("Agency is required.");
      return false;
    }
    if (isUserExternal === 33) {
      if (!lanID) {
        this.toastr.error("Lan ID is required for HRA user.");
        return false;
      }
    }
    if (!firstName) {
      this.toastr.error("First Name is required.");
      return false;
    }
    if (!lastName) {
      this.toastr.error("Last Name is required.");
      return false;
    }
    if (roleID == 0) {
      this.toastr.error("Access level is required.");
      return false;
    }
    if (!email) {
      this.toastr.error("Email is required.");
      return false;
    }
    if (!this.checkEmail()) {
      this.toastr.error("Email does not match HRA/non-HRA user type.");
      return false;
    }
    if (!phone) {
      this.toastr.error("Office Phone is required.");
      return false;
    }
    if (!isActive) {
      if (!inactiveReasonComments || inactiveReason == 0) {
        this.toastr.error("Reason and comments are required to inactivate the user.");
        return false;
      }
    }
    if (this.assignedSites.length == 0 && isActive) {
      this.toastr.error("Must assign at least one site.");
      return false;
    }
    if (this.showTransmit && !canTransmit) {
      this.toastr.error("Select if user can transmit Application.");
      return false;
    }
    if (this.showLiason && !vcs) {
      this.toastr.error("Select if user is Housing Liason.");
      return false;
    }
    return true;
  }

  validatePattern() {
    if (!this.userForm.controls.firstName.valid) {
      this.toastr.error("First name can contain letters only.");
      return false;

    } if (!this.userForm.controls.lastName.valid) {
      this.toastr.error("Last name can contain letters only.");
      return false;

    }
    if (!this.userForm.controls.email.valid) {
      this.toastr.error("Email is not valid.");
      return false;

    }
    return true;
  }
  SaveUser() {   
    // if(this.selectedTab == 0){
    if (this.validateUser() && this.validatePattern()) {
      this.selectedUserData = this.userForm.getRawValue();
      this.selectedUserData.isUserExternal = this.userForm.controls.isUserExternal.value == 33 ? false : true;
      this.selectedUserData.siteList = this.assignedSites;
      this.selectedUserData.inactiveReason = this.selectedUserData.inactiveReason == 0 ? null : this.selectedUserData.inactiveReason;
      this.selectedUserData.canTransmit = this.showTransmit ? this.selectedUserData.canTransmit : null;
      this.selectedUserData.vcs = this.showLiason ? this.selectedUserData.vcs : null;
      this.selectedUserData.isAssignedSupervisor = this.selectedUserData.isAssignedSupervisor ? this.selectedUserData.isAssignedSupervisor : false;
      this.selectedUserData.agencyID = this.selectedAgencyID;
      if (this.userForm.controls.isUserExternal.value == 34) {
        this.selectedUserData.lanID = this.userForm.controls.juniperID.value;
      }
      // if (this.is_SUPER_USER) {
      //   this.selectedUserData.agencyID = this.userForm.controls.agencyID.value;
      // } else {
      //   this.selectedUserData.agencyID = this.selectedAgencyID;
      // };
      this.selectedUserData.agencyID = this.selectedAgencyID;

      if (this.selectedUserData.nonHRALanID.indexOf('DHS') != -1) {
        this.selectedUserData.nonHRALanID = this.selectedUserData.nonHRALanID.substring(4);
      }

      /*      if(this.selectedUserData.isActive == false && this.selectedUserData.isAssignedSupervisor == true){
     
             const title = '';
             const primaryMessage = 'User is assigned as the Supervisor to one or more Users.';
             const secondaryMessage = ` Use Update Supervisor Tab before this user can be inactivated.`;
             const confirmButtonName = 'Ok';
             const dismissButtonName = '';
     
             this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
               .then(
                 (positiveResponse) => {
                   this.showUpdateSuper = true;
                   this.getUserListBySuperSub = this.userAdminService.getUserListBySuper(this.selectedUserData.optionUserID).subscribe(res => {
                     if (res) {
                       this.userListBySuper = res as UserList[];
                       this.selectedTab = 1;
                     }
                   });
     
                 },
                 (negativeResponse) => console.log(),
               );
           } */
      // else {
      this.saveUserSub = this.userAdminService.saveUserData(this.selectedUserData).subscribe(res => {
        var result = res as number;
        if (result >= 0) {
          if (this.editMode) {
            this.toastr.success("User was successfully updated.");
            setTimeout(() => {
              this.Return();
            }, 2500);
          }
          else {
            this.toastr.success("New User was successfully created.");

            setTimeout(() => {
              this.Return();
            }, 2500);
          }
        }
        else if (result == -110) {
          this.toastr.error("User with this User ID already exists.");
        }
        else if (result == -120) {
          this.toastr.error("User with this email already exists.");
        }
      },
        error => console.error("Error!", error)
      )
      //} 
    }
    // }
    // else if(this.selectedTab == 1){
    //   var allUsersAssigned = true;
    //   this.userListBySuper.forEach(item => { 
    //     if(item.assignedSupervisorID == 0){
    //       allUsersAssigned = false;
    //     }
    //   });
    //   if(allUsersAssigned) {
    //     this.saveSuperSub = this.userAdminService.saveSupervisorUpdate(this.userListBySuper).subscribe(res => {

    //       this.toastr.success("Supervisor assignment was successfully updated.", "");

    //       this.userForm.controls.isAssignedSupervisor.setValue(false);
    //       this.selectedTab = 0;
    //     },
    //     error =>
    //       this.toastr.error("Supervisor assignment was not updated.", "Error while saving"));
    //   }
    //   else {
    //     this.toastr.error("Not all users are assigned a supervisor.", "");
    //   }
    // }
  }

  Return() {
    this.userAdminService.setSelectedUser(null);
    this.resetUserForm();
    this.router.navigate(['/admin/user-administration']);
  }

  checkEmail() {
    var email = this.userForm.controls.email.value;
    var external = this.userForm.controls.isUserExternal.value;

    if ((email.toLowerCase().indexOf('dss.nyc.gov') != -1 || email.toLowerCase().indexOf('hra.nyc.gov') != -1) && external == 33) { //|| email.toLowerCase().indexOf('dhs.nyc.gov') != -1 
      return true;
    }
    else if ((email.toLowerCase().indexOf('dss.nyc.gov') == -1 && email.toLowerCase().indexOf('hra.nyc.gov') == -1) && external == 34) { // && email.toLowerCase().indexOf('dhs.nyc.gov') == -1
      return true;
    }
    else {
      return false;
    }
  }

  onCancel() {
    const title = 'Warning';
    const primaryMessage = 'Wish to reset the screen?';
    const secondaryMessage = ``;
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'Cancel';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => {
          this.resetUserForm();
          if (this.editMode) {
            this.getSelectedUserData(this.selectedUserData.optionUserID);
          }

        },
        (negativeResponse) => console.log(),
      );

  }

  resetUserForm() {
    this.userForm.reset();
    this.assignedSites = [];
    this.availableSites = [];
    this.userForm.controls.roleID.setValue(0);
    this.userForm.controls.nonHRALanID.setValue("DHS\\");
    this.userForm.controls.nonHRALanID.disable();
    this.userForm.controls.canTransmit.setValue(33);
    this.userForm.controls.isActive.setValue(true);
    this.userForm.controls.inactiveReason.setValue(0);
    this.userForm.controls.siteContactList.setValue(0);
    this.showLiason = false;
    this.showTransmit = false;
    this.userActive = true;
    this.showContact = false;

    if (this.is_SUPER_USER) {
      this.userForm.disable();
      this.userForm.controls.agencyID.enable();
      //this.userForm.controls.agencyID.setValue(0);
      //this.userForm.controls.searchAgencyID.enable();
      this.userForm.controls.agencyID.setValue("");
    }
  }

  private _filter(value: string): AgencyData[] {
    if (value && !this.editMode) {
      const filterValue = value.toLowerCase();
      return this.agencyList.filter(agency => agency.agencyName.toLowerCase().includes(filterValue) || agency.agencyNo.includes(filterValue));
    }
    else {
      return this.agencyList;
    }
  }

  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.agencyListSub) {
      this.agencyListSub.unsubscribe();
    }
    if (this.siteListSub) {
      this.siteListSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.saveUserSub) {
      this.saveUserSub.unsubscribe();
    }
    if (this.getSiteContactsSub) {
      this.getSiteContactsSub.unsubscribe();
    }
    if (this.selectedUserDataSub) {
      this.selectedUserDataSub.unsubscribe();
    }
    // if (this.getUserListBySuperSub) {
    //   this.getUserListBySuperSub.unsubscribe();
    // }
    // if (this.saveSuperSub) {
    //   this.saveSuperSub.unsubscribe();
    // }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }


}


