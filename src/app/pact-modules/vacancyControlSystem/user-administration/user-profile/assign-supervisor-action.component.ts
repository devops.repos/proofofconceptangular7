import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import { MasterDropdown } from 'src/app/models/masterDropdown.module';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: 'app-assign-supervisor',
    template: `
    <mat-form-field appearance="outline" id="supervisor-select">
        <mat-select [(ngModel)]="selectedValue">
        <mat-option [value]="0">Select One</mat-option>
        <mat-option (click)="onSupervisorSelected()" *ngFor="let user of params.value" [value]="user.supervisorID">
            {{user.supervisorName}}
        </mat-option>
        </mat-select>
    </mat-form-field>
    `
})
export class AssignSupervisorComponent implements ICellRendererAngularComp {
    params: any;
    public cell: any;

    selectedValue = 0;

    agInit(params: any): void {
        this.params = params;
        // console.log(this.params);
        
        this.cell = {row: params.node.data, col: params.colDef.headerName};
        this.cell.row.assignedSupervisorID = this.selectedValue;
    }

    onSupervisorSelected(): void {
        this.cell.row.assignedSupervisorID = this.selectedValue;
        // alert('Selected Unit Type from DROPDOWN : ' + JSON.stringify(this.cell.row));
      }

    refresh(): boolean {
        return false;
    }
}
