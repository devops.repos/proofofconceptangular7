import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';


@Component({
  selector: 'app-contact-action',
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="userAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #userAction="matMenu">
      <button mat-menu-item (click)="onContactSelected()">Add User </button>
    </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class UserSiteContactActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;

  constructor() {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onContactSelected() {
    this.params.context.componentParent.onSelectedContact(this.params.data);
  }

  refresh(): boolean {
    return false;
  }
}
