import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { CommonService } from '../../../services/helper-services/common.service';
import { RefGroupDetails } from "src/app/models/refGroupDetailsDropDown.model";
import { UserRole } from 'src/app/models/pact-enums.enum';
import { UserList, UserData, SiteData, AgencyData, SelectedFilters } from './user-administration.model';
import { UserAdminService } from './user-administration.service';
import { UserActionComponent } from './user-action.component';
import { ToastrService } from "ngx-toastr";
import { Router, NavigationStart } from '@angular/router';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-user-administration',
  templateUrl: './user-administration.component.html',
  styleUrls: ['./user-administration.component.scss']
})
export class UserAdministrationComponent implements OnInit, OnDestroy {

  userDataSub: Subscription;
  agencyListSub: Subscription;
  siteListSub: Subscription;
  rowDataSub: Subscription;
  commonServiceSub: Subscription;
  routeSub: Subscription;

  userData: AuthData;
  agencyList: AgencyData[] = [];
  siteList: SiteData[] = [];
  siteListFull: SiteData[] = [];
  selectedFilters = new SelectedFilters();
  siteTypeList: RefGroupDetails[];
  responseItems: RefGroupDetails[];

  selectedAgencyName: string;
  rowCount: number = 0;
  rowData: UserList[] = [];
  filteredAgencyData: Observable<any[]>;
  filteredSiteData: Observable<any[]>;

  selectedUserData: UserData;

  is_SYS_ADMIN = false; // PE, HP and RA Sys Admins
  is_SUPER_USER = false; // CAS users
  //is_DropDown_Disabled = true;
  firstLoad = true;

  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  rowSelection;
  frameworkComponents;
  context;
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;
  searchAgency = new FormControl('');
  searchSite = new FormControl('');
  siteType = new FormControl(0);

  public gridOptions: GridOptions;

  constructor(private userService: UserService,
    private commonService: CommonService,
    private userAdminService: UserAdminService,
    private toastr: ToastrService,
    private router: Router) {

    this.gridOptions = {
      rowHeight: 35
    } as GridOptions;

    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the user list is loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Users Available</span>';

    this.columnDefs = [
      {
        headerName: 'Name (L, F)',
        filter: 'agTextColumnFilter',
        valueGetter(params) {
          return params.data.lastName + ', ' + params.data.firstName;
        }
      },
      {
        headerName: 'User ID',
        filter: 'agTextColumnFilter',
        field: 'lanID'
      },
      {
        headerName: 'Juniper',
        field: 'juniperID',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Level',
        field: 'levelName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Access',
        filter: 'agTextColumnFilter',
        autoHeight: true,
        cellRenderer(params) {
          var cares = params.data.nonHRALanID ? 'CARES' : '';
          var transmit = params.data.canTransmit == 1 ? 'Transmit' : '';
          var liason = params.data.vcs == 1 ? 'Liason' : '';

          var access = '';
          if (cares) {
            access += cares + '<br />'
          }
          if (transmit) {
            access += transmit + '<br />'
          }
          if (liason) {
            access += liason + '<br />'
          }

          return access;
          //return transmit ? (cares? transmit + ', ' + cares: transmit) : (cares? cares: '');
        },

      },
      {
        headerName: 'Site – Supervisor (L,F)',
        valueGetter(params) {
          var superName = params.data.supervisorName ? params.data.supervisorName : '';
          return superName;
        },
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'User Status',
        valueGetter(params) {
          return params.data.isActive == 1 ? 'A' : 'I'
        },
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Email',
        field: 'email',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Office Phone',
        field: 'officePhone',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Last Log In',
        field: 'lastAccessDate',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: UserActionComponent
    };

  }

  ngOnInit() {
    /** Getting the currently active user info */
    this.userDataSub = this.userService.getUserData().subscribe(userdata => {
      if (userdata) {
        this.userData = userdata;
        //if (this.userData.roleId === UserRole.SYS_ADMIN) {
          if (this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8)) {

            this.is_SUPER_USER = true;
          }
          else {
            this.is_SYS_ADMIN = true;
          }
        //}

        if (this.is_SYS_ADMIN) {
          this.selectedAgencyName = this.userData.agencyNo + ' - ' + this.userData.agencyName;
          //this.is_DropDown_Disabled = false;

          this.siteListSub = this.userAdminService.getSiteListByUser(this.userData.optionUserId).subscribe(res => {
            if (res) {
              this.siteList = this.siteListFull = res as SiteData[];
              this.filteredSiteData = this.searchSite.valueChanges.pipe(
                startWith(''),
                map(value => this.filterSite(value))
              );

              this.selectedFilters.agencyID = this.userData.agencyId;
              this.selectedFilters.siteID = 0;
              this.selectedFilters.typeID = 0;

              this.onGo();
            }
          });
        }

        /** Get the Agency List */
        if (this.is_SUPER_USER) {
          this.agencyListSub = this.userAdminService.getAgencyList().subscribe(res => {
            if (res) {
              this.agencyList = res as AgencyData[];
              this.filteredAgencyData = this.searchAgency.valueChanges.pipe(
                startWith(''),
                map(value => this.filterAgency(value))
              );
              this.searchSite.disable();
              this.siteType.disable();
              //this.is_DropDown_Disabled = true;
              this.selectedFilters = this.userAdminService.getSelectedFilters();
              if (Object.keys(this.selectedFilters).length === 0) {
                this.selectedFilters.agencyID = 0;
                this.selectedFilters.siteID = 0;
                this.selectedFilters.typeID = 0;
              }
              else if (this.selectedFilters.agencyID != 0) {

                this.onAgencySelected(this.selectedFilters.agencyID);
                this.onGo();
              }
            }
          });
        }
      }

    });

    this.commonServiceSub = this.commonService.getRefGroupDetails("2").subscribe(
      res => {
        const data = res.body as RefGroupDetails[];

        this.siteTypeList = data.filter(d => {
          return d.refGroupID === 2 && (d.refGroupDetailID === 5 || d.refGroupDetailID === 6 || d.refGroupDetailID === 7 || d.refGroupDetailID === 8);
        });

      },
      error => console.error("Error!", error)
    );

    this.selectedFilters = this.userAdminService.getSelectedFilters();
    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (event.url != "/admin/user-profile" && event.url != "/admin/user-administration") {
          this.selectedFilters.agencyID = 0;
          this.selectedFilters.siteID = 0;
          this.selectedFilters.typeID = 0;
        }
        this.userAdminService.setSelectedFilters(this.selectedFilters);
      };
    });
    //this.siteType.setValue(0);
  }

  onTypeSelected(event) {

    this.selectedFilters.typeID = event.value;
    if (this.selectedFilters.typeID != 0) {
      this.siteList = this.siteListFull.filter(d => { return d.siteCategoryType === this.selectedFilters.typeID });
    }
    else {
      this.siteList = this.siteListFull;
    }
    this.selectedFilters.siteID = 0;
    this.searchSite.setValue("");
  }
  
  onSiteSelected(siteID: number) {
    this.selectedFilters.siteID = siteID;
    if (this.selectedFilters.siteID != 0) {
      this.selectedFilters.typeID = this.siteList.filter(d => { return d.siteID === this.selectedFilters.siteID })[0].siteCategoryType;
    }
    else {
      this.selectedFilters.typeID = 0;
    }
    this.siteType.setValue(this.selectedFilters.typeID);
  }

  onAgencySelected(agencyID: number) {
    this.selectedFilters.agencyID = agencyID;
    if (this.selectedFilters.agencyID != 0) {
      var agency = this.agencyList.filter(d => { return d.agencyID === this.selectedFilters.agencyID })

      this.selectedAgencyName = agency[0].agencyNo + ' - ' + agency[0].agencyName;
      //this.searchAgency.setValue(this.selectedAgencyName);
      this.siteListSub = this.userAdminService.getSiteListByAgency(this.selectedFilters.agencyID).subscribe(res => {
        if (res) {
          this.siteList = this.siteListFull = res as SiteData[];
          this.filteredSiteData = this.searchSite.valueChanges.pipe(
            startWith(''),
            map(value => this.filterSite(value))
          );
        }
      });

      this.searchSite.enable();
      this.siteType.enable();
      //this.is_DropDown_Disabled = false;
    }
    else {
      //this.is_DropDown_Disabled = true;
      this.selectedFilters.agencyID = 0;
      this.searchSite.disable();
      this.siteType.disable();
    }
    this.selectedFilters.siteID = 0;
    this.searchSite.setValue('');
    this.selectedFilters.typeID = 0;
    this.siteType.setValue(this.selectedFilters.typeID);
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        allColumnIds.push(column.colId);
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, true);
    this.gridApi.sizeColumnsToFit(allColumnIds);
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'UserReport-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.gridApi.exportDataAsExcel(params);
  }

  onGo() {
    if (this.selectedFilters.agencyID == 0) {
      this.toastr.error("Please select Agency.", "");
      return;
    }
    else {
      /** API call to get the grid data */
      this.rowDataSub = this.userAdminService.getUserList(this.selectedFilters).subscribe(res => {
        this.rowData = res.body as UserList[];
        if (this.rowData != null) {
          this.rowCount = this.rowData.length;
        }
      });
    }
  }

  selectedUserSave(cell) {

    this.userAdminService.setSelectedUser(cell);

  }

  filterAgency(value: string): AgencyData[] {
    if (value) {
      const filterValue = value.toLowerCase();
      return this.agencyList.filter(agency => agency.agencyName.toLowerCase().includes(filterValue) || agency.agencyNo.includes(filterValue));
    }
    else {
      this.selectedFilters.agencyID = 0;
      //this.searchAgency.setValue('');
      this.selectedFilters.siteID = 0;
      this.searchSite.setValue('');
      this.selectedFilters.typeID = 0;
      this.siteType.setValue(this.selectedFilters.typeID);
      //this.is_DropDown_Disabled = true;
      this.siteType.disable();
      this.searchSite.disable();
      return this.agencyList;
    }
  }

  filterSite(value: string): SiteData[] {
    if (value) {
      const filterValue = value.toLowerCase();
      return this.siteList.filter(site => site.siteName.toLowerCase().includes(filterValue) || site.siteNo.includes(filterValue));
    }
    else {
      this.selectedFilters.siteID = 0;
      return this.siteList;

    }
  }

  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.agencyListSub) {
      this.agencyListSub.unsubscribe();
    }
    if (this.siteListSub) {
      this.siteListSub.unsubscribe();
    }
    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

}


