export class UserList {
    optionUserID: number;
    firstName: string;
    lastName: string
    lanID: string;
    juniperID: string;
    levelName: string;
    nonHRALanID: string;
    canTransmit: number;
    vcs: number;
    supervisorName: string;
    isActive: boolean;
    email: string;
    officePhone: string;
    lastAccessDate: string;
    //for Supervisor tab
    siteNo: string;
    siteID: number;
    agencyNo: string;
    assignedSupervisorID: number;
    availableSupervisorList: SupervisorData[];
    
}

export class UserData {
    optionUserID: number;
    isUserExternal: boolean;
    userContactID: number;
    agencyID: number;
    firstName: string;
    lastName: string;
    lanID: string;
    juniperID: string;
    nonHRALanID: string;
    roleID: number;
    email: string;
    officePhone: string;
    officeExt: string;   
    cellPhone: string;
    faxPhone: string;
    comments: string;
    canTransmit: number;
    vcs: number;
    isActive: boolean;
    inactiveReason: number;
    inactiveReasonComments: string;
    isAssignedSupervisor: boolean;
    lastAccessDate: string;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;
    siteList: SiteData[];
}


export class SiteContact {
    siteContactID: number;
    siteID: number;
    optionUserID: number;
    firstName: string;
    lastName: string;
    title: string;
    email: string;
    phone: string;
    extension: string;
    alternatePhone: string;
    alternateExtension: string;
    fax: string;
    isActive: boolean;
    isSysAdmin: boolean;

}

export class SupervisorData {
    supervisorID: number;
    supervisorName: string
    siteID: number;
}

  
export class SiteData {
    siteID: number;
    siteNo: string;
    agencyID: number;
    siteName: string;
    siteCategoryType: number;
    siteCategoryName: string;
    siteType: string;
    assignedSupervisorID: number;
    canDeassign: boolean;
    availableSupervisorList: SupervisorData[];
}


export class AgencyData {
    agencyID: number;
    agencyNo: string;
    agencyName: string;
    agencyType: string;
}

export class SelectedFilters {
    agencyID: number;
    typeID: number;
    siteID: number;
}