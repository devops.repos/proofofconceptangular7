import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationStart } from '@angular/router';
import { ReferralPackageNavService } from 'src/app/core/sidenav-list/referral-package-nav.service';
import { MatTabChangeEvent, MatRadioChange, MatTabGroup, MatTabHeader, MatTab } from '@angular/material';
import { CommonService } from "../../../../services/helper-services/common.service";
import { RefGroupDetails } from "../../../../models/refGroupDetailsDropDown.model";
import { FormBuilder, FormGroup } from "@angular/forms";
import { C2vService } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v.service';
import { ReferralService } from "./../referral-package.service";
import { VCSReferralReady } from '../referral-package.model';
import { ToastrService } from "ngx-toastr";
// import { AgGridAngular } from 'ag-grid-angular';
// import 'ag-grid-enterprise';
// import { GridOptions } from 'ag-grid-community';
import { iHousingApplicationSupportingDocumentsData } from 'src/app/pact-modules/supportiveHousingSystem/consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications.model'
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { ReferralHoldComponent } from 'src/app/shared/referral-hold/referral-hold.component';
import { HousingApplicationSupportingDocumentsComponent } from 'src/app/shared/housing-application-supporting-documents/housing-application-supporting-documents.component';

@Component({
  selector: 'app-referral-ready-tracking',
  templateUrl: './referral-ready-tracking.component.html',
  styleUrls: ['./referral-ready-tracking.component.scss']
})
export class ReferralReadyTrackingComponent implements OnInit, OnDestroy {

  selectedTab = 0;
  sidenavTabSub: Subscription;  
  commonServiceSub: Subscription; 
  clientDataSub: Subscription; 
  navigationSub: Subscription; 
  getClientDataSub: Subscription; 
  getRefHistorySub: Subscription; 
  saveRefSub: Subscription; 
  routeSub: Subscription;

  responseItems: RefGroupDetails[];
  //holdReasons: RefGroupDetails[];
  clientDocGroup: FormGroup;
  clientDocData: VCSReferralReady = new VCSReferralReady();
  oldClientDocData: VCSReferralReady = new VCSReferralReady();
  applicationID: number;
  // history: VCSReferralReadyHistory[] = [];

  
  //  gridColumnApi: any;
  //  defaultColDef: any;
  //  //context: any;
  //  overlayLoadingTemplate: string;
  //  overlayNoRowsTemplate: string;
  // columnDefs: any;
  
  // public gridOptions: GridOptions;

  housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null,
    expandSection: null
  };

  showBtn: boolean = false;
  clearBtnShow: boolean = false;

  @ViewChild('tabs') tabs: MatTabGroup;
  @ViewChild(ReferralHoldComponent) refChild: ReferralHoldComponent;
  @ViewChild(HousingApplicationSupportingDocumentsComponent) docChild: HousingApplicationSupportingDocumentsComponent;

  constructor(
    private router: Router,
    private referralPackageNavService: ReferralPackageNavService,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private c2vService: C2vService,
    private referralService: ReferralService,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService
    
  ) {


    // this.gridOptions = {
    //   rowHeight: 35
    // } as GridOptions;
    // this.defaultColDef = {
    //   filter: true
    // };

    // this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the history is loading.</span>';
    // this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No History Available</span>';

    // this.columnDefs = [       
    //   {
    //     headerName: 'Date',
    //     field: 'createdDate',
    //     width: 150,
    //     filter: 'agTextColumnFilter'
    //   },
    //   {
    //     headerName: 'Client Status',
    //     field: 'clientStatus',
    //     width: 150
    //   },
    //   {
    //     headerName: 'Comments',
    //     field: 'clientComments',
    //     width: 150
    //   }
    // ]; 

   }

  ngOnInit() {
    this.sidenavTabSub = this.referralPackageNavService.getCurrentReferralReadyTrackingTabIndex().subscribe(res => {
        this.selectedTab = res;     
      });

    this.commonServiceSub = this.commonService.getRefGroupDetails("7,58").subscribe(
        res => {
          const data = res.body as RefGroupDetails[];  
            
          this.responseItems = data.filter(d => {
            return d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34);
          }); 

          // this.holdReasons = data.filter(d => { 
          //   return d.refGroupID === 58;
          // });
                     
        },
        error => console.error("Error!", error)
      );
        
      this.clientDataSub = this.c2vService.getClientAwaitingPlacementSelected().subscribe(c2vRes => {
        if (c2vRes) {
          this.applicationID = c2vRes.pactApplicationID;        
          this.getRefReadyData();


            /*   this.housingApplicationSupportingDocumentsData.agencyNumber = '1402';
              this.housingApplicationSupportingDocumentsData.siteNumber = '003';
              this.housingApplicationSupportingDocumentsData.firstName = 'KRISTEL';
              this.housingApplicationSupportingDocumentsData.lastName = 'MONDENSIR';
              this.housingApplicationSupportingDocumentsData.dob = '07/21/1986';
              this.housingApplicationSupportingDocumentsData.cin = '';
              this.housingApplicationSupportingDocumentsData.ssn = '064725878';
              this.housingApplicationSupportingDocumentsData.pactClientId = null;
              this.housingApplicationSupportingDocumentsData.approvalExpiryDate = '12/12/2019';
              this.housingApplicationSupportingDocumentsData.pactApplicationId = 212416;
              this.housingApplicationSupportingDocumentsData.expandSection = 1;
 */
          this.housingApplicationSupportingDocumentsData.agencyNumber = c2vRes.referringAgencyNo;
          this.housingApplicationSupportingDocumentsData.siteNumber = c2vRes.referringSiteNo;
          this.housingApplicationSupportingDocumentsData.firstName = c2vRes.firstName;
          this.housingApplicationSupportingDocumentsData.lastName = c2vRes.lastName;
          this.housingApplicationSupportingDocumentsData.dob = c2vRes.dateOfBirth;
          this.housingApplicationSupportingDocumentsData.cin = null;
          this.housingApplicationSupportingDocumentsData.ssn = c2vRes.ssn;
          this.housingApplicationSupportingDocumentsData.pactClientId = c2vRes.pactClientID;
          this.housingApplicationSupportingDocumentsData.approvalExpiryDate = c2vRes.approvalToDate;
          this.housingApplicationSupportingDocumentsData.pactApplicationId = c2vRes.pactApplicationID;
          this.housingApplicationSupportingDocumentsData.expandSection = 1; 
          
        } else {
          /* If user refresh(reload) the page the client selected data will be empty, so redirect them back to ClientAwaitingPlacement Page */
          this.router.navigate(['/vcs/client-awaiting-placement']);
        }
      });   

      this.clientDocGroup = this.formBuilder.group({
        vcsClientPackageID: [0],
        isPkgRefReady: [""],
        pkgRefReadyComments: [""],
        //isClientRefReady: [0],
        //clientRefReadyComment: [""],
        //clientNotReadyReason: [""],
        //clientNotReadyOtherSpecify: [""],
        //clientNotReadyAddlComments: [""]
      });
     
      this.tabs._handleClick = this.interceptTabChange.bind(this);

      this.routeSub = this.router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
          if(event.url != "/vcs/referral-package/referral-ready-tracking" && event.url != "/vcs/referral-package/housing-preference" && event.url != "/vcs/referral-package/referral-and-placement-summary" && event.url != "/vcs/referral-package/homeless-information"){
            this.referralPackageNavService.setCurrentHomelessInformationTabIndex(0);
            this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null);
            this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
            this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null);
          }
        }
      });
  }

  // onFirstDataRendered(params) {
  //   params.api.sizeColumnsToFit();
  // } 

  // onGridReady(params: { api: any; columnApi: any })  {
  //   //this.gridApi = params.api;
  //   this.gridColumnApi = params.columnApi;
  //   params.api.sizeColumnsToFit();
  //   params.api.setDomLayout('autoHeight'); 

  //   this.gridColumnApi.autoSizeColumns(); 

  // }
  
  // isClientRefReadyChange(event: MatSelectChange) {
  //   this.showBtn= true;
  //   if (event.value == 33) {
  //   this.clientDocGroup.controls["clientNotReadyReason"].setValue("");
  //   this.clientDocGroup.controls["clientNotReadyAddlComments"].setValue("");
  //   this.clientDocGroup.controls["clientNotReadyOtherSpecify"].setValue("");
  //  }
  //  else if (event.value == 34) {
  //   this.clientDocGroup.controls["clientRefReadyComment"].setValue("");
  //  }
  //  else if  (event.value == 0) {
  //   this.clientDocGroup.controls["clientNotReadyReason"].setValue("");
  //   this.clientDocGroup.controls["clientNotReadyAddlComments"].setValue("");
  //   this.clientDocGroup.controls["clientRefReadyComment"].setValue("");
  //   this.clientDocGroup.controls["clientNotReadyOtherSpecify"].setValue("");    
  //  }
  // }

  // clientNotReadyReasonChange(event: MatRadioChange) {
  //   this.showBtn = true;
  //   if (event.value != 510) {
  //     this.clientDocGroup.controls["clientNotReadyOtherSpecify"].setValue("");    
  //   }
  // }

  isPkgRefReadyChange(event: MatRadioChange) {
    this.showBtn = true;
    if (event.value == 33) {
      this.clientDocGroup.controls["pkgRefReadyComments"].setValue("");    
    }
  }
 
  getRefReadyData(){
    this.getClientDataSub = this.referralService.getClientDocData(this.applicationID).subscribe(res => {
      if (res != null){
        this.clientDocData = res as VCSReferralReady;
      
        if (this.clientDocData.isClientRefReady == null)
        {
            this.clientDocData.isClientRefReady = 0;
        }
        this.clientDocGroup.patchValue(this.clientDocData); 

        // this.getRefReadyHistory(this.clientDocData.vcsClientPackageID);        
      }  
      else {
        this.clientDocData.vcsClientPackageID = null;
        this.clientDocData.pactApplicationID = null;
        this.clientDocData.isPkgRefReady = null;
        this.clientDocData.pkgRefReadyComments = null;
        this.clientDocData.isClientRefReady = 0;
        this.clientDocData.clientRefReadyComment = null;
        this.clientDocData.clientNotReadyReason = null;
        this.clientDocData.clientNotReadyOtherSpecify = null;
        this.clientDocData.clientNotReadyAddlComments = null;
        
      }
      
      this.oldClientDocData = JSON.parse(JSON.stringify(this.clientDocData));
    });
  }

  // getRefReadyHistory(packageID: number){
  //   this.getRefHistorySub = this.referralService.getClientHistory(packageID).subscribe(res => {
  //     this.history = res as VCSReferralReadyHistory[];
  //   });

  // }

  onReferralHoldChange(referralChange: VCSReferralReady){
 
    this.clientDocData.isClientRefReady = referralChange.isClientRefReady;
    this.clientDocData.clientRefReadyComment = referralChange.clientRefReadyComment;
    this.clientDocData.clientNotReadyReason = referralChange.clientNotReadyReason;
    this.clientDocData.clientNotReadyOtherSpecify = referralChange.clientNotReadyOtherSpecify;
    this.clientDocData.clientNotReadyAddlComments = referralChange.clientNotReadyAddlComments;
    this.showBtn = true;
  }

  interceptTabChange(tab: MatTab, tabHeader: MatTabHeader, idx: number) {    
    if(this.showBtn || this.clearBtnShow) {
      this.confirmNavigation();      
    }
    else {
      return MatTabGroup.prototype._handleClick.apply(this.tabs, arguments);
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {  
    this.selectedTab = tabChangeEvent.index;
    this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(this.selectedTab);  
 
  }

  onSave(showToast: boolean) {
    if(this.selectedTab == 1){
      this.clientDocData.isPkgRefReady = this.clientDocGroup.controls.isPkgRefReady.value;
      this.clientDocData.pkgRefReadyComments = this.clientDocGroup.controls.pkgRefReadyComments.value;
      
    }
      if (this.clientDocData.isClientRefReady == 0) {
        this.clientDocData.isClientRefReady = null;
      }
    
    this.clientDocData.pactApplicationID = this.applicationID;    

    this.saveRefSub = this.referralService.saveReferral(this.clientDocData).subscribe(
      res => {
        if (showToast) {
          this.toastr.success("The information was saved successfully.", "");
          this.showBtn = false;
          if (this.clientDocData.isClientRefReady == null)
          {
            this.clientDocData.isClientRefReady = 0;
          }
          this.oldClientDocData = JSON.parse(JSON.stringify(this.clientDocData));
          if(this.selectedTab == 2){
            this.refChild.getRefReadyHistory(res as number); 
          }
          //this.getRefReadyHistory(res as number);           
        }
      },
      error =>
        this.toastr.error("The information was not saved.", "Error while saving")
    );
  }
  
  confirmNavigation(){
    const title = 'Data will be lost';
    const primaryMessage = '';
    const secondaryMessage = `Data is not saved, please Save or Clear the changes`;
    const confirmButtonName = 'Ok';
    const dismissButtonName = '';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => {
          
        },
        (negativeResponse) => console.log(),
      );
  }

   // Next button click
   nextPage() {
    if(this.showBtn || this.clearBtnShow) {
      this.confirmNavigation();
    }
    else {
      if (this.selectedTab < 2) {
        this.selectedTab = this.selectedTab + 1;
      } else if (this.selectedTab == 2) {
        this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(0);
        this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null);
        this.router.navigate(['/vcs/referral-package/housing-preference']);
      }
    }
  }

  // Previous button click
  previousPage() {
    if(this.showBtn || this.clearBtnShow) {
      this.confirmNavigation();
    }
    else {
      if (this.selectedTab != 0) {
        this.selectedTab = this.selectedTab - 1;
      } else if (this.selectedTab == 0) {
        this.referralPackageNavService.setCurrentHomelessInformationTabIndex(0);  
        this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null); 
        this.router.navigate(['/vcs/referral-package/homeless-information']);
      }
    }
  }

  clearPage(){
    if(this.selectedTab == 0){
      this.docChild.resetDocForm();
      this.clearBtnShow = false;
    }
    else if(this.selectedTab == 1) {
      this.clientDocGroup.patchValue(this.oldClientDocData);   
    } 
    else if (this.selectedTab == 2) {   
      this.refChild.onClear(this.oldClientDocData);
    }
    this.showBtn = false;
  }

  documentValueChange(event: string){
    if(event == "Input clear"){
      this.clearBtnShow = false;
    }
    else if (event == "Input change"){
      this.clearBtnShow = true;
    }
  }
  
  onChange(){    
    this.showBtn = true;
  }

  ngOnDestroy() {
    if(this.sidenavTabSub){
      this.sidenavTabSub.unsubscribe();   
    }
    if(this.commonServiceSub){
      this.commonServiceSub.unsubscribe();   
    }
    if(this.clientDataSub){
      this.clientDataSub.unsubscribe();   
    }
    if(this.navigationSub){
      this.navigationSub.unsubscribe();   
    }
    if(this.getClientDataSub){
      this.getClientDataSub.unsubscribe();   
    }
    if(this.getRefHistorySub){
      this.getRefHistorySub.unsubscribe();   
    }
    if(this.saveRefSub){
      this.saveRefSub.unsubscribe();   
    }
    if(this.routeSub){
      this.routeSub.unsubscribe();
    }
  }

}

