import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, EventEmitter, Output, Input, HostListener } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { MatTabChangeEvent, MatTabGroup, MatTabHeader, MatTab } from '@angular/material';
import { Subscription } from 'rxjs';
import { ReferralPackageNavService } from 'src/app/core/sidenav-list/referral-package-nav.service';
import { RefGroupDetails } from "../../../../models/refGroupDetailsDropDown.model";
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { CommonService } from "../../../../services/helper-services/common.service";
import { VCSHomelessInfo, VCSClientShelterContact } from "../referral-package.model";
import { ReferralService } from "./../referral-package.service";
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridOptions } from 'ag-grid-community';
import { ContactsActionComponent } from './homeless-information-contacts-action.component';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { C2vService } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v.service';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';

@Component({
  selector: 'app-homeless-information',
  templateUrl: './homeless-information.component.html',
  styleUrls: ['./homeless-information.component.scss']/* ,
  encapsulation: ViewEncapsulation.None
   */
})
export class HomelessInformationComponent implements OnInit, OnDestroy {

  selectedTab = 0;
  sidenavTabSub: Subscription;
  commonServiceSub: Subscription;
  navigationSub: Subscription;
  selectedReferralSub: Subscription;
  saveShelterContact: Subscription;
  shelterDataSub: Subscription;
  deleteContactSub: Subscription;
  saveShelterDataSub: Subscription;
  routeSub: Subscription;
  userDataSub: Subscription;

  responseItems: RefGroupDetails[];
  shelterGroup: FormGroup;
  shelterData: VCSHomelessInfo;
  applicationID: number;
  agencyName: string;

  vcsReferralID: number;
  showTable: boolean;
  contactData: VCSClientShelterContact;
  contactDataList: VCSClientShelterContact[] = [];
  contactGroup: FormGroup;
  selectedShelterContactID: number;

  showBtn: boolean = false;
  showContactCancelBtn: boolean = false;
  is_Prev_Required: boolean = false;

  housingHomelessGuid: string;
  reportParams: PACTReportUrlParams;
  reportParameters: PACTReportParameters[] = [];
  userData: AuthData;

  //gridApi: any;
  gridColumnApi: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  context: any;
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;

  columnDefs: any;
  frameworkComponents: any;

  public gridOptions: GridOptions;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @Input() preSelectedRelationshipType: number;
  @Output() uploadFinished = new EventEmitter();
  @ViewChild('tabs') tabs: MatTabGroup;

  constructor(
    private router: Router,
    private referralPackageNavService: ReferralPackageNavService,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private shelterService: ReferralService,
    private confirmDialogService: ConfirmDialogService,
    private toastr: ToastrService,
    private c2vService: C2vService,
    private userService: UserService
  ) {


    this.gridOptions = {
      rowHeight: 35
    } as GridOptions;

    this.rowSelection = 'multiple';
    this.pagination = true;
    this.context = { componentParent: this };
    this.defaultColDef = {
      resizable: true
    };

    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the contacts are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Contacts Available</span>';

    this.columnDefs = [
      {
        headerName: 'vcsClientShelterContactID',
        field: 'vcsClientShelterContactID',
        hide: true
      },
      {
        headerName: 'vcsReferralPackageID',
        field: 'vcsReferralPackageID',
        hide: true
      },
      {
        headerName: 'First Name',
        field: 'firstName'
      },
      {
        headerName: 'Last Name',
        field: 'lastName'
      },
      {
        headerName: 'Email',
        field: 'email'
      },
      {
        headerName: 'Office Title',
        field: 'officeTitle'
      },
      {
        headerName: 'Primary Phone',
        field: 'primaryPhone'
      },
      {
        headerName: 'Extension',
        field: 'extension'
      },
      {
        headerName: 'Actions',
        field: 'action',
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];

    this.frameworkComponents = {
      actionRenderer: ContactsActionComponent
    };
  }


  ngOnInit() {

    // User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    this.sidenavTabSub = this.referralPackageNavService.getCurrentHomelessInformationTabIndex().subscribe(res => {
      this.selectedTab = res;
    });
    this.commonServiceSub = this.commonService.getRefGroupDetails("7").subscribe(
      res => {
        const data = res.body as RefGroupDetails[];

        this.responseItems = data.filter(d => {
          return d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34);
        });

      },
      error => console.error("Error!", error)
    );

    this.selectedReferralSub = this.c2vService.getClientAwaitingPlacementSelected().subscribe(c2vRes => {
      if (c2vRes) {
        this.applicationID = c2vRes.pactApplicationID;
        this.agencyName = c2vRes.agencyName;
        this.selectedShelterContactID = 0;
        this.getShelterData();
      } else {
        /* If user refresh(reload) the page the client selected data will be empty, so redirect them back to ClientAwaitingPlacement Page */
        this.router.navigate(['/vcs/client-awaiting-placement']);
      }
    });

    this.shelterGroup = this.formBuilder.group({
      vcsClientPackageID: [0],
      resShelterNo: ["", Validators.required],
      shelterName: [this.agencyName],
      shelterPhone: [""]
    });

    this.contactGroup = this.formBuilder.group({
      //hdnvcsClientShelterContactID:[0],
      vcsClientShelterContactID: [""],
      vcsReferralPackageID: [""],
      firstName: ["", Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z]*$"), this.whitespaceValidator])],
      lastName: ["", Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z]*$"), this.whitespaceValidator])],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      officeTitle: ["", Validators.pattern("^[a-zA-Z]*$")],
      primaryPhone: [""],
      extension: ["", Validators.pattern("^[0-9]*$")]
    });

    this.tabs._handleClick = this.interceptTabChange.bind(this);

    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event.url != "/vcs/referral-package/referral-ready-tracking" && event.url != "/vcs/referral-package/housing-preference" && event.url != "/vcs/referral-package/referral-and-placement-summary" && event.url != "/vcs/referral-package/homeless-information") {
          this.referralPackageNavService.setCurrentHomelessInformationTabIndex(0);
          this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null);
          this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
          this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null);
        }
      }
    });
  }

  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  interceptTabChange(tab: MatTab, tabHeader: MatTabHeader, idx: number) {
    if (this.showBtn) {
      this.confirmNavigation();
    }
    else {
      return MatTabGroup.prototype._handleClick.apply(this.tabs, arguments);
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    this.selectedTab = tabChangeEvent.index;
    this.referralPackageNavService.setCurrentHomelessInformationTabIndex(this.selectedTab);

    this.is_Prev_Required = true;
    if (this.selectedTab == 0) {
      this.is_Prev_Required = false;
    }

  }

  getShelterData() {
    this.shelterDataSub = this.shelterService.getShelterData(this.applicationID).subscribe(res => {
      this.shelterData = res as VCSHomelessInfo;

      if (this.shelterData != null) {
        if (this.shelterData.shelterName == null || this.shelterData.shelterName.length == 0) {
          this.shelterData.shelterName = this.agencyName;
        }
        this.shelterGroup.patchValue(this.shelterData);
        this.vcsReferralID = this.shelterData.vcsClientPackageID
        this.getContactsList();
      }
    });

  }

  addContact() {
    this.showTable = true;

    if (this.shelterData == null) {
      if (this.shelterGroup.invalid) {
        this.toastr.error('Please save shelter information first.', '');

      } else {
        this.onSave(false);
      }
    }
  }

  saveContact(showToastr: boolean) {
    if (this.contactGroup.invalid || this.contactGroup.controls.firstName.value == null || this.contactGroup.controls.lastName.value == null || this.contactGroup.controls.email.value == null) {
      if (showToastr) {
        this.toastr.error('Data is not valid or missing.', 'Not Saved');
      }
      return;
    }
    else {
      if (this.shelterData != null) {
        this.contactData = this.contactGroup.value;
        this.contactData.vcsClientShelterContactID = this.selectedShelterContactID; //this.contactGroup.get('hdnvcsClientShelterContactID').value;  
        var duplicate = false;
        for (let element of this.contactDataList) {
          if (element.email == this.contactData.email && element.vcsClientShelterContactID != this.contactData.vcsClientShelterContactID) {
            if (showToastr) {
              this.toastr.error('Contact with this email already exists.', 'Duplicate');
            }
            duplicate = true;
            break;
          }
        }

        this.contactData.vcsReferralPackageID = this.shelterData.vcsClientPackageID;

        if (!duplicate) {
          this.saveShelterContact = this.shelterService.saveVCSShelterContact(this.contactData).subscribe(
            data => {
              if (showToastr) {
                this.toastr.success('Contact was saved successfully.', 'Save');

              }
              this.getContactsList();
              this.selectedShelterContactID = 0;
              this.showContactCancelBtn = false;
              this.clearValidatorsForContactTab();
              this.contactGroup.reset();
              this.addValidators();

            },
            error => {
              if (showToastr) {
                this.toastr.error('Contact was not Saved.', 'Not Saved');
              }
            }

          );
        }
      }
      else {
        this.toastr.error('Please save shelter information first.', 'Not Saved');
      }
    }

  }

  clearValidatorsForContactTab = () => {
    this.contactGroup.controls.firstName.clearValidators();
    this.contactGroup.controls.lastName.clearValidators();
    this.contactGroup.controls.email.clearValidators();
    this.contactGroup.updateValueAndValidity();
  }

  addValidators() {
    this.contactGroup.controls.firstName.setValidators([Validators.required, Validators.pattern("^[a-zA-Z]*$"), this.whitespaceValidator]);
    this.contactGroup.controls.lastName.setValidators([Validators.required, Validators.pattern("^[a-zA-Z]*$"), this.whitespaceValidator]);
    this.contactGroup.controls.email.setValidators([Validators.required, Validators.email]);

    this.contactGroup.updateValueAndValidity();
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params: { api: any; columnApi: any }) {
    //this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    params.api.setDomLayout('autoHeight');

    this.gridColumnApi.autoSizeColumns();

  }

  getContactsList() {
    this.shelterDataSub = this.shelterService.getContactbyShelter(this.vcsReferralID)
      .subscribe(
        res => {
          if (res != null) {
            this.contactDataList = res as VCSClientShelterContact[];

            if (this.contactDataList.length != 0) {
              this.showTable = true;
            }
          }
        },
        error => console.error('Error in retrieving the Contact Data!', error)
      );
  }

  contatDeleteParent(cell: VCSClientShelterContact) {

    const title = 'Confirm Delete';
    const primaryMessage = '';
    const secondaryMessage = `Are you sure you want to delete the selected contact?`;
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => {
          this.deleteContact(cell);
          this.contactGroup.reset();
          //this.clearContactFields();
        },
        (negativeResponse) => console.log(),
      );

  }

  deleteContact(dataSelected: VCSClientShelterContact) {

    this.deleteContactSub = this.shelterService.deleteShelterContact(dataSelected.vcsClientShelterContactID).subscribe(
      res => {
        this.toastr.success('Contact was deleted successfully.', 'Delete');
        // console.error('Success!', res);
        this.getContactsList();

      },
      error => {
        this.toastr.error('Contact was not deleted.', 'Delete');
        console.error('Error!', error);
      }
    );
  }

  contactViewParent(cell: VCSClientShelterContact) {
    if (cell.vcsClientShelterContactID != null) {
      this.selectedShelterContactID = cell.vcsClientShelterContactID;
    }

    if (cell.firstName != null) {
      this.contactGroup.get('firstName').setValue(cell.firstName);
    }
    if (cell.lastName != null) {
      this.contactGroup.get('lastName').setValue(cell.lastName);
    }
    if (cell.email != null) {
      this.contactGroup.get('email').setValue(cell.email);
    }
    if (cell.officeTitle != null) {
      this.contactGroup.get('officeTitle').setValue(cell.officeTitle);
    }
    if (cell.primaryPhone != null) {
      this.contactGroup.get('primaryPhone').setValue(cell.primaryPhone);
    }
    if (cell.extension != null) {
      this.contactGroup.get('extension').setValue(cell.extension);
    }

    this.onContactChange();
  }

  confirmNavigation() {
    const title = 'Data will be lost';
    const primaryMessage = '';
    const secondaryMessage = `Data is not saved, please Save or Clear the changes`;
    const confirmButtonName = 'Ok';
    const dismissButtonName = '';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => {

        },
        (negativeResponse) => console.log(),
      );
  }

  onSave(showToastr: boolean) {
    if (this.shelterGroup.invalid) {
      if (showToastr) {
        this.toastr.error('Data is not valid or missing.', 'Not Saved');
      }
      return;
    }
    else {
      console.log(this.shelterGroup.controls.shelterName.value);
      if (this.shelterGroup.controls.resShelterNo.value == 33 && this.shelterGroup.controls.shelterName.value == '') {
        if (showToastr) {
          this.toastr.error('Please provide the shelter name.', 'Not Saved');
        }
        return;
      }
      else {
        this.shelterData = this.shelterGroup.value;

        this.shelterData.pactApplicationID = this.applicationID;
        this.saveShelterDataSub = this.shelterService.saveShelterData(this.shelterData).subscribe(
          data => {
            //this.newPackageID = data as number;
            this.shelterData.vcsClientPackageID = this.vcsReferralID = data as number;
            this.showBtn = false;
            if (showToastr) {
              this.toastr.success('Data was saved successfully.', 'Save');
            }
          }, // console.log('Success!', data),
          error => {
            if (showToastr) {
              this.toastr.error('Data was not Saved.', 'Not Saved');
            }
          }
          // console.error('Error!', error)
        );
      }
    }
  }

  viewReport(reportName: string) {
    if (!this.housingHomelessGuid) {
      this.reportParameters = [
        { parameterName: 'applicationID', parameterValue: this.applicationID.toString(), CreatedBy: this.userData.optionUserId },
        { parameterName: 'reportName', parameterValue: reportName, CreatedBy: this.userData.optionUserId }
      ];
      this.commonService.generateGUID(this.reportParameters)
        .subscribe(
          res => {
            if (res) {
              this.housingHomelessGuid = res;
              this.generateReport(res, reportName);
            }
          }
        );
    } else {
      this.generateReport(this.housingHomelessGuid, reportName);
    }
  }

  generateReport(reportGuid: string, reportName: string) {
    this.reportParams = { reportParameterID: reportGuid, reportName: reportName, "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          const data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.commonService.OpenWindow(URL.createObjectURL(data));
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }

  onHHHReportClick() {
    this.viewReport('HHHReport');
  }

  // Next button click
  nextPage() {
    if (this.showBtn) {
      this.confirmNavigation();
    }
    else {
      if (this.selectedTab < 1) {
        this.selectedTab = this.selectedTab + 1;
      } else if (this.selectedTab == 1) {
        this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(0);
        this.referralPackageNavService.setCurrentHomelessInformationTabIndex(null);
        this.router.navigate(['/vcs/referral-package/referral-ready-tracking']);
      }
    }
  }

  // Previous button click
  previousPage() {
    if (this.showBtn) {
      this.confirmNavigation();

    }
    else {
      if (this.selectedTab != 0) {
        this.selectedTab = this.selectedTab - 1;
      }
    }
  }
  clearPage() {
    this.shelterGroup.patchValue(this.shelterData);
    this.contactGroup.reset();
    this.showBtn = false;
    this.showContactCancelBtn = false;
    this.selectedShelterContactID = 0;
  }

  onChange() {

    this.showBtn = true;
  }

  onContactChange() {
    this.showContactCancelBtn = true;
  }

  get firstName() {
    return this.contactGroup.get("firstName");
  }
  get lastName() {
    return this.contactGroup.get("lastName");
  }
  get email() {
    return this.contactGroup.get("email");
  }

  ngOnDestroy() {
    if (this.sidenavTabSub) {
      this.sidenavTabSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.navigationSub) {
      this.navigationSub.unsubscribe();
    }
    if (this.selectedReferralSub) {
      this.selectedReferralSub.unsubscribe();
    }
    if (this.saveShelterContact) {
      this.saveShelterContact.unsubscribe();
    }
    if (this.shelterDataSub) {
      this.shelterDataSub.unsubscribe();
    }
    if (this.deleteContactSub) {
      this.deleteContactSub.unsubscribe();
    }
    if (this.saveShelterDataSub) {
      this.saveShelterDataSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

}
