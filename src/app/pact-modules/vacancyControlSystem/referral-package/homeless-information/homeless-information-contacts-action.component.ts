import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { VCSClientShelterContact } from "../referral-package.model";

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "doc-action",
  template: `<mat-icon  matTooltip='Edit Contact' (click)="onView()" class="doc-action-icon" color="primary">edit</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete Contact' class="doc-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class ContactsActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;  
  private contactSelected: VCSClientShelterContact;
  constructor() { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {

    this.contactSelected = {
        vcsClientShelterContactID: this.params.data.vcsClientShelterContactID,
        vcsReferralPackageID: this.params.data.vcsReferralPackageID,
        firstName: this.params.data.firstName,
        lastName: this.params.data.lastName,
        email: this.params.data.email,
        //officeTitle: this.params.data.officeTitle,
        officeTitle: this.params.data.officeTitle,
        primaryPhone: this.params.data.primaryPhone,
        extension: this.params.data.extension,      
        createdBy: this.params.data.createdBy,
        updatedBy: this.params.data.updatedBy,
    };

    this.params.context.componentParent.contactViewParent(this.contactSelected);
  }

  onDelete() {

    this.contactSelected = {
        vcsClientShelterContactID: this.params.data.vcsClientShelterContactID,
        vcsReferralPackageID: this.params.data.vcsReferralPackageID,
        firstName: this.params.data.firstName,
        lastName: this.params.data.lastName,
        email: this.params.data.email,
        //officeTitleType: this.params.data.officeTitleType,
        officeTitle: this.params.data.officeTitle,
        primaryPhone: this.params.data.phprimaryPhoneone,
        extension: this.params.data.extension,      
        createdBy: this.params.data.createdBy,
        updatedBy: this.params.data.updatedBy,
    };

    this.params.context.componentParent.contatDeleteParent(this.contactSelected);
  }

  refresh(): boolean {
    return false;
  }
}
