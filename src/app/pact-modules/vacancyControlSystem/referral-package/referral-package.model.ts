export class VCSHomelessInfo {
    vcsClientPackageID: number;
    pactApplicationID: number;
    resShelterNo: number;
    shelterName: string;
    shelterPhone: string; 
    createdBy: string;
    updatedBy: string;
  
  }

 
export class VCSClientShelterContact{
    vcsClientShelterContactID: number;
    vcsReferralPackageID: number;
    firstName: string;
    lastName: string;
    email: string;
    //officeTitleType: number;
    officeTitle: string;
    primaryPhone: string;
    extension: number;
    createdBy: string;
    updatedBy: string;
}

export class VCSReferralReady {
    vcsClientPackageID: number;
    pactApplicationID: number;
    isPkgRefReady: number;
    pkgRefReadyComments: string;
    isClientRefReady: number;
    clientRefReadyComment:string;
    clientNotReadyReason:number;
    clientNotReadyOtherSpecify: string;
    clientNotReadyAddlComments: string;
    createdBy: string;
    updatedBy: string;

}

export class VCSReferralReadyHistory {
    vcsClientPackageHistID: number;
    vcsClientPackageID: number;    
    clientStatus: string;
    clientComments: string;
    holdReason: string;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;
}

export class VCSReferralSummary {
    agencyNo: string;
	siteNo: string;
	siteName: string;
	agencyName: string;
	unitType: string;
	interviewDate: Date;
	interviewTime: Date;	
	interviewAddress: string;
    interviewCity: string;
	interviewState: string;
	interviewZip: string;	
    referredDate: string;
    referredBy: string;
    statusDate: string;
    referralStatusType: number;
}

export class VCSPlacementSummary {
    agencyNo: string;
	siteNo: string;
    siteName: string;
    agencyName: string;
    siteType: string;
    siteAddress: string;
    siteCity: string;
    siteState: string;
    siteZip: string;
    moveInDate: string;
    moveOutDate: string;
    verificationReason: string;
    status: string;
    verifiedBy: string;
    verifiedDate: string;

}


export class VCSApplicationPreference {
    pactHousingPreferenceID: number;
    boroughPreference1: number;
    isBoroughPreference1Changed: boolean;
    boroughPreference2: number;
    isBoroughPreference2Changed: boolean;
    boroughExclusion: number;
    isBoroughExclusionChanged: boolean;
    boroughExclusionComment: string;
    isBoroughExclusionCommentChanged: boolean;
    boroughServiceLocated: number;
    isBoroughServiceLocatedChanged: boolean;
    aptPrefWillingToShareType: number;
    isAptPrefWillingToShareTypeChanged: boolean;
    aptPrefWillingToShareComment: string;
    isAptPrefWillingToShareCommentChanged: boolean;
    aptPrefElevatorOr1stFloorType: number;
    isAptPrefElevatorOr1stFloorTypeChanged: boolean;
    aptPrefWheelchairAccessType: number;
    isAptPrefWheelchairAccessTypeChanged: boolean;
    aptPrefSmokeFreeType: number;
    isAptPrefSmokeFreeTypeChanged: boolean;
    aptPrefStaywithLGBTType: number;
    isAptPrefStaywithLGBTTypeChanged: boolean;
    aptPrefNeedTranslatorType: number;
    isAptPrefNeedTranslatorTypeChanged: boolean;
    primaryLanguage: string;
    aptPrefEmoSupportAnimalType: number;
    isAptPrefEmoSupportAnimalTypeChanged: boolean;
    //ResidenceType
    aptPrefOtherType: number;
    isAptPrefOtherTypeChanged: boolean;
    aptPrefOtherComment: string;
    isAptPrefOtherCommentChanged: boolean;
    //HosPrefOtherType,
    //HosPrefOtherComment
}
