import { Component, OnInit, OnDestroy, HostListener, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationStart } from '@angular/router';
import { ReferralPackageNavService } from 'src/app/core/sidenav-list/referral-package-nav.service';
import { MatTabChangeEvent, MatTabGroup, MatTabHeader, MatTab } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { C2vService } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v.service';
import { HousingPreferencesModel, RecommendedServices } from '../../../supportiveHousingSystem/housing-preferences/housing-preferences.model';
import { HousingPreferenceService } from '../../../supportiveHousingSystem/housing-preferences/housing-preferences.service';
import { CommonService } from "../../../../services/helper-services/common.service";
import { RefGroupDetails } from "../../../../models/refGroupDetailsDropDown.model";
import { ApplicantPreference } from 'src/app/shared/applicant-preferences/applicant-preferences.model';
import { ReferralService } from "./../referral-package.service";
import { VCSApplicationPreference } from "./../referral-package.model";
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-referral-housing-preference',
  templateUrl: './referral-housing-preference.component.html',
  styleUrls: ['./referral-housing-preference.component.scss']
})
export class ReferralHousingPreferenceComponent implements OnInit, OnDestroy {

  selectedTab = 0;
  sidenavTabSub: Subscription;
  commonServiceSub: Subscription;
  selectedClientSub: Subscription;
  routerSub: Subscription;
  housingPrefSub: Subscription;
  appPrefSub: Subscription;
  saveAppPrefSub: Subscription;
  routeSub: Subscription;


  // showSaveBtn: boolean = true;
  // showClearBtn: boolean = true;
  showBtn: boolean = false;

  applicationID: number;

  housingPreferences: HousingPreferencesModel;
  recommendedServices: RecommendedServices;
  apartmentOptions: RefGroupDetails[];
  boroughOptions: RefGroupDetails[] = [];
  //residenceOptions: RefGroupDetails[];
  appPrefVCSRecord: VCSApplicationPreference;
  appPreference: ApplicantPreference = {
    preference1: null,
    preference2: null,
    excludePreference: null,
    excludeComments: null,
    serviceLocated: null,
    aptQuestion1: null,
    aptQuestion1Comment: null,
    aptQuestion2: null,
    aptQuestion3: null,
    aptQuestion4: null,
    aptQuestion5: null,
    aptQuestion6: null,
    aptQuestion7: null,
    aptQuestion8: null,
    aptQuestion8Comment: null,
    primaryLanguage: null
    //aptQuestion9:null, 
    //aptQuestion10:null, 
    //aptQuestion10Comment:null 
  };

  isPageTouched: boolean = false;
  preferenceObject = {} as any;
  prefValid: boolean = true;
  commentValid: boolean = true;


  @ViewChild('tabs') tabs: MatTabGroup;

  /* @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
     if (this.isPageTouched == true) {
      this.onSave(false);
    } 
    
  } */

  constructor(
    private router: Router,
    private referralPackageNavService: ReferralPackageNavService,
    private commonService: CommonService,
    private toastr: ToastrService,
    private c2vService: C2vService,
    private housingPreferencesService: HousingPreferenceService,
    private referralService: ReferralService,
    private confirmDialogService: ConfirmDialogService
  ) { }

  ngOnInit() {
    this.sidenavTabSub = this.referralPackageNavService.getCurrentHousingPreferenceTabIndex().subscribe(res => {
      this.selectedTab = res;
    });

    this.selectedClientSub = this.c2vService.getClientAwaitingPlacementSelected().subscribe(c2vRes => {
      if (c2vRes) {
        this.applicationID = c2vRes.pactApplicationID;

        this.getHousingPreference();
        this.getApplicantPreferences();

      } else {
        /* If user refresh(reload) the page the client selected data will be empty, so redirect them back to ClientAwaitingPlacement Page */
        this.router.navigate(['/vcs/client-awaiting-placement']);
      }
    });

    this.tabs._handleClick = this.interceptTabChange.bind(this);

    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event.url != "/vcs/referral-package/referral-ready-tracking" && event.url != "/vcs/referral-package/housing-preference" && event.url != "/vcs/referral-package/referral-and-placement-summary" && event.url != "/vcs/referral-package/homeless-information") {
          this.referralPackageNavService.setCurrentHomelessInformationTabIndex(0);
          this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null);
          this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
          this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null);
        }
      }
    });
  }
  getHousingPreference() {
    this.housingPrefSub = this.housingPreferencesService.getHousingPreference(this.applicationID).subscribe(
      res => {
        this.housingPreferences = res as HousingPreferencesModel;

        this.recommendedServices = this.housingPreferences.housingPreference.recommendedServices;

        if (this.boroughOptions.length < 1) {
          this.housingPreferences.boroughOptions.filter(x => x.refGroupDetailID == 29).forEach(x => this.boroughOptions.push(x))
          this.housingPreferences.boroughOptions.filter(x => x.refGroupDetailID != 29).forEach(x => this.boroughOptions.push(x));
        }
        this.apartmentOptions = this.housingPreferences.apartmentOptions;
      });
  }

  getApplicantPreferences() {
    this.appPrefSub = this.referralService.getApplicantPreferences(this.applicationID).subscribe(
      res => {
        this.appPrefVCSRecord = res as VCSApplicationPreference;
        if (this.appPrefVCSRecord != null || this.appPrefVCSRecord != undefined) {
          this.setInitialApplicationPref();

          /*         this.appPreference = {
                    preference1: res["boroughPreference1"],
                    preference2: res["boroughPreference2"],
                    excludePreference: res["boroughExclusion"],
                    serviceLocated: res["boroughServiceLocated"],
                    excludeComments: res["boroughExclusionComment"],
                
                    aptQuestion1: res["aptPrefWillingToShareType"],
                    aptQuestion2: res["aptPrefElevatorOr1stFloorType"],
                    aptQuestion3: res["aptPrefWheelchairAccessType"],
                    aptQuestion4: res["aptPrefSmokeFreeType"],
                    aptQuestion5: res["aptPrefStaywithLGBTType"],
                    aptQuestion6: res["aptPrefNeedTranslatorType"],
                    aptQuestion7: res["aptPrefEmoSupportAnimalType"],
                    aptQuestion8: res["aptPrefOtherType"],
                    aptQuestion8Comment: res["aptPrefOtherComment"],
                    primaryLanguage: res["primaryLanguage"],
                    //aptQuestion9: res["residenceType"],
                    //aptQuestion10: res["hosPrefOtherType"],
                    aptQuestion1Comment: res["aptPrefWillingToShareComment"]          
                    //aptQuestion10Comment: res["hosPrefOtherComment"]
                  }  */
        }
      });

  }

  setInitialApplicationPref() {
    if (this.appPrefVCSRecord) {
      this.appPreference = {
        preference1: this.appPrefVCSRecord.boroughPreference1,
        preference2: this.appPrefVCSRecord.boroughPreference2,
        excludePreference: this.appPrefVCSRecord.boroughExclusion,
        serviceLocated: this.appPrefVCSRecord.boroughServiceLocated,
        excludeComments: this.appPrefVCSRecord.boroughExclusionComment,

        aptQuestion1: this.appPrefVCSRecord.aptPrefWillingToShareType,
        aptQuestion2: this.appPrefVCSRecord.aptPrefElevatorOr1stFloorType,
        aptQuestion3: this.appPrefVCSRecord.aptPrefWheelchairAccessType,
        aptQuestion4: this.appPrefVCSRecord.aptPrefSmokeFreeType,
        aptQuestion5: this.appPrefVCSRecord.aptPrefStaywithLGBTType,
        aptQuestion6: this.appPrefVCSRecord.aptPrefNeedTranslatorType,
        aptQuestion7: this.appPrefVCSRecord.aptPrefEmoSupportAnimalType,
        aptQuestion8: this.appPrefVCSRecord.aptPrefOtherType,
        aptQuestion8Comment: this.appPrefVCSRecord.aptPrefOtherComment,
        primaryLanguage: this.appPrefVCSRecord.primaryLanguage,
        aptQuestion1Comment: this.appPrefVCSRecord.aptPrefWillingToShareComment
      }
    }
    else {
      this.appPreference = {
        preference1: null,
        preference2: null,
        excludePreference: null,
        excludeComments: null,
        serviceLocated: null,
        aptQuestion1: null,
        aptQuestion1Comment: null,
        aptQuestion2: null,
        aptQuestion3: null,
        aptQuestion4: null,
        aptQuestion5: null,
        aptQuestion6: null,
        aptQuestion7: null,
        aptQuestion8: null,
        aptQuestion8Comment: null,
        primaryLanguage: null
      }
    }
  }

  onApplicantPrefChange(appPref: ApplicantPreference) {
    this.isPageTouched = true;
    this.appPreference = appPref;
    this.showBtn = true;
  }

  interceptTabChange(tab: MatTab, tabHeader: MatTabHeader, idx: number) {
    if (this.showBtn) {
      this.confirmNavigation();
    }
    else {
      return MatTabGroup.prototype._handleClick.apply(this.tabs, arguments);
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    // /* Saving Data Between Tab Navigation */
    /*   if (this.isPageTouched == true) {
        this.onSave(false);
      } */
    this.selectedTab = tabChangeEvent.index;
    this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(this.selectedTab);
    /* if (this.selectedTab == 1 || this.selectedTab == 2)
    {
      this.showSaveBtn = false;
      this.showClearBtn = false;
    }
    else {
      this.showSaveBtn = true;
      this.showClearBtn = true;
    } */
  }
  setApplicantPreferences() {
    this.preferenceObject.PACTApplicationID = this.applicationID;
    this.preferenceObject.boroughPreference1 = this.appPreference.preference1;
    this.preferenceObject.boroughPreference2 = this.appPreference.preference2;
    this.preferenceObject.boroughExclusion = this.appPreference.excludePreference;
    this.preferenceObject.boroughServiceLocated = this.appPreference.serviceLocated;
    this.preferenceObject.boroughExclusionComment = this.appPreference.excludeComments;
    this.preferenceObject.aptPrefWillingToShareType = this.appPreference.aptQuestion1;
    this.preferenceObject.aptPrefElevatorOr1stFloorType = this.appPreference.aptQuestion2;
    this.preferenceObject.aptPrefWheelchairAccessType = this.appPreference.aptQuestion3;
    this.preferenceObject.aptPrefSmokeFreeType = this.appPreference.aptQuestion4;
    this.preferenceObject.aptPrefStaywithLGBTType = this.appPreference.aptQuestion5;
    this.preferenceObject.aptPrefNeedTranslatorType = this.appPreference.aptQuestion6;
    this.preferenceObject.aptPrefEmoSupportAnimalType = this.appPreference.aptQuestion7;
    this.preferenceObject.aptPrefOtherType = this.appPreference.aptQuestion8;
    //this.preferenceObject.residenceType = this.appPreference.aptQuestion9;
    //this.preferenceObject.hosPrefOtherType = this.appPreference.aptQuestion10;
    this.preferenceObject.aptPrefWillingToShareComment = this.appPreference.aptQuestion1Comment;
    this.preferenceObject.aptPrefOtherComment = this.appPreference.aptQuestion8Comment;
    this.preferenceObject.primaryLanguage = this.appPreference.primaryLanguage;
    //this.preferenceObject.hosPrefOtherComment = this.appPreference.aptQuestion10Comment;
  }

  preferenceCheck() {
    if (this.appPreference.preference1 == 0 || this.appPreference.preference2 == 0 || this.appPreference.excludePreference == 0 ||
      this.appPreference.serviceLocated == 0 || this.appPreference.aptQuestion1 == 0 || this.appPreference.aptQuestion2 == 0 ||
      this.appPreference.aptQuestion3 == 0 || this.appPreference.aptQuestion4 == 0 || this.appPreference.aptQuestion5 == 0 ||
      this.appPreference.aptQuestion6 == 0 || this.appPreference.aptQuestion7 == 0 || this.appPreference.aptQuestion8 == 0) {
      this.prefValid = false;
    }

    if (this.appPreference.preference1 == null || this.appPreference.preference2 == null || this.appPreference.excludePreference == null ||
      this.appPreference.serviceLocated == null || this.appPreference.aptQuestion1 == null || this.appPreference.aptQuestion2 == null ||
      this.appPreference.aptQuestion3 == null || this.appPreference.aptQuestion4 == null || this.appPreference.aptQuestion5 == null ||
      this.appPreference.aptQuestion6 == null || this.appPreference.aptQuestion7 == null || this.appPreference.aptQuestion8 == null) {
      this.prefValid = false;
    }
    if (this.appPreference.aptQuestion1 == 34 && (this.appPreference.aptQuestion1Comment == null || this.appPreference.aptQuestion1Comment.trim() == '')) {
      this.commentValid = false;
    }
    if (this.appPreference.aptQuestion8 == 33 && (this.appPreference.aptQuestion8Comment == null || this.appPreference.aptQuestion8Comment.trim() == '')) {
      this.commentValid = false;
    }
    if ((this.appPreference.excludePreference == 27 || this.appPreference.excludePreference == 28 ||
      this.appPreference.excludePreference == 29 || this.appPreference.excludePreference == 30 ||
      this.appPreference.excludePreference == 31) && (this.appPreference.excludeComments == null || this.appPreference.excludeComments.trim() == '')) {
      this.commentValid = false;
    }
  }

  onSave(showToastr: boolean) {
    this.preferenceCheck();
    if (this.prefValid) {
      if (this.commentValid) {
        this.setApplicantPreferences();

        this.saveAppPrefSub = this.referralService.saveApplicantPreferences(this.preferenceObject).subscribe(
          data => {
            if (showToastr) {
              this.toastr.success("The information was saved successfully.", "");
              this.isPageTouched = false;
              this.getApplicantPreferences();
              this.showBtn = false;
            }
          },
          error =>
            this.toastr.error("The information was not saved.", "Error while saving")
        );
      }
      else {
        this.toastr.error( "The information was not saved.", "Comments are required")
        this.commentValid = true;
      }
    }
    else {
      this.toastr.error("The information was not saved.", "All preference questions are required")
      this.prefValid = true;
    }
  }

  validatePreference() {

  }

  confirmNavigation() {
    const title = 'Data will be lost';
    const primaryMessage = '';
    const secondaryMessage = `Data is not saved, please Save or Clear the changes`;
    const confirmButtonName = 'Ok';
    const dismissButtonName = '';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => {

        },
        (negativeResponse) => console.log(),
      );
  }

  // Next button click
  nextPage() {
    if (this.showBtn) {
      this.confirmNavigation();
    }
    else {
      if (this.selectedTab < 2) {
        this.selectedTab = this.selectedTab + 1;
      } else if (this.selectedTab == 2) {
        this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(0);
        this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
        this.router.navigate(['/vcs/referral-package/referral-and-placement-summary']);
      }
    }
  }

  // Previous button click
  previousPage() {
    if (this.showBtn) {
      this.confirmNavigation();
    }
    else {
      if (this.selectedTab != 0) {
        this.selectedTab = this.selectedTab - 1;
      } else if (this.selectedTab == 0) {
        this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(0);
        this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
        this.router.navigate(['/vcs/referral-package/referral-ready-tracking']);
      }
    }
  }

  clearPage() {
    this.setInitialApplicationPref();
    this.showBtn = false;
  }

  ngOnDestroy() {
    if (this.sidenavTabSub) {
      this.sidenavTabSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.selectedClientSub) {
      this.selectedClientSub.unsubscribe();
    }
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
    if (this.housingPrefSub) {
      this.housingPrefSub.unsubscribe();
    }
    if (this.appPrefSub) {
      this.appPrefSub.unsubscribe();
    }
    if (this.saveAppPrefSub) {
      this.saveAppPrefSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

}
