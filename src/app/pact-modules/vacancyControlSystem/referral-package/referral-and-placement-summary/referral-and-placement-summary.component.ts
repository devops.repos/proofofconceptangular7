import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationStart } from '@angular/router';
import { ReferralPackageNavService } from 'src/app/core/sidenav-list/referral-package-nav.service';
import { MatTabChangeEvent } from '@angular/material';
import { VCSReferralSummary, VCSPlacementSummary } from '../referral-package.model';
import { ReferralService } from "./../referral-package.service";
import { C2vService } from 'src/app/pact-modules/vacancyControlSystem/client-awaiting-placement/c2v.service';
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: 'app-referral-and-placement-summary',
  templateUrl: './referral-and-placement-summary.component.html',
  styleUrls: ['./referral-and-placement-summary.component.scss']
})
export class ReferralAndPlacementSummaryComponent implements OnInit, OnDestroy {

  selectedTab = 0;
  sidenavTabSub: Subscription;
  placementSumSub: Subscription;
  clientSelectedSub: Subscription;
  refSumSub: Subscription;
  routeSub: Subscription;

  applicationID: number;
  vcsClientID: number;
  is_Next_Required: boolean = true;

  columnDefs: any;
  referralColumnDefs: any;

  gridColumnApi: any;

  context: any;
  defaultColDef: any;
  gridApi: any;
  gridOptions: GridOptions;

  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;

  refOverlayLoadingTemplate: string;
  refOverlayNoRowsTemplate: string;

  referralList: VCSReferralSummary[];
  completedReferralList: VCSReferralSummary[] = [];
  pendingReferralList: VCSReferralSummary[] = [];
  placementList: VCSPlacementSummary[] = [];

  constructor(
    private router: Router,
    private referralPackageNavService: ReferralPackageNavService,
    private referralService: ReferralService,
    private c2vService: C2vService,
  ) {
    this.gridOptions = {
      rowHeight: 50
    } as GridOptions;
    this.context = { componentParent: this };
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    this.refOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the referrals are loading.</span>';
    this.refOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Referrals Available</span>';

    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the placements are loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Placements Available</span>';



    this.columnDefs = [
      {
        headerName: 'Housing Provider Agency/Site #',
        valueGetter: function (params) {
          return params.data.agencyNo + '/' + params.data.siteNo
        },
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Housing Provider Agency/Site Name',
        valueGetter: function (params) {
          return params.data.agencyName + '/' + params.data.siteName
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Type',
        field: 'siteType',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Site Address',
        cellRenderer: function (params) {
          if (params.data.siteAddress) {
            return params.data.siteAddress + '<br/>' + params.data.siteCity + ' ' + params.data.siteState + ', ' + params.data.siteZip
          }
          else {
            return '';
          }
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Move-in/Move-out Date',
        valueGetter: function (params) {
          var moveOut = params.data.moveOutDate ? params.data.moveOutDate : '';
          var moveIn = params.data.moveInDate ? params.data.moveInDate : '';

          return moveIn + ' / ' + moveOut
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Verification Reason',
        valueGetter: function (params) {
          var reason = params.data.verificationReason ? params.data.verificationReason : '';

          return reason;
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Status',
        field: 'status',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Verified By/ Verified Date',
        cellRenderer: function (params) {
          var verBy = params.data.verifiedBy ? params.data.verifiedBy : '';
          var verDate = params.data.verifiedDate ? params.data.verifiedDate : '';
          return verBy + '<br/>' + verDate
        },
        filter: 'agTextColumnFilter'
      }
    ];

    this.referralColumnDefs = [
      {
        headerName: 'Housing Provider Agency/Site #',
        valueGetter: function (params) {
          return params.data.agencyNo + '/' + params.data.siteNo
        },
        filter: 'agNumberColumnFilter'
      },
      {
        headerName: 'Housing Provider Agency/Site Name',
        valueGetter: function (params) {
          return params.data.agencyName + '/' + params.data.siteName
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Unit Type',
        field: 'unitType',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Interview Date/Time',
        valueGetter: function (params) {
          var interviewDate = params.data.interviewDate ? params.data.interviewDate : '';
          var interviewTime = params.data.interviewTime ? params.data.interviewTime : '';

          return interviewDate + ' ' + interviewTime
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Interview Location',
        cellRenderer: function (params) {
          if (params.data.interviewAddress) {
            var address = params.data.interviewAddress ? params.data.interviewAddress : '';
            var city = params.data.interviewCity ? params.data.interviewCity : '';
            var state = params.data.interviewState ? params.data.interviewState : '';
            var zip = params.data.interviewZip ? params.data.interviewZip : '';
            return address + '<br/>' + city + ' ' + state + ', ' + zip
          }
          else {
            return '';
          }
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referred By-Date',
        cellRenderer: function (params) {
          return params.data.referredBy + '<br/>' + params.data.referredDate
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Status Date',
        field: 'statusDate',
        filter: 'agTextColumnFilter'
      }
    ];

  }

  ngOnInit() {


    this.sidenavTabSub = this.referralPackageNavService.getCurrentReferralPlacementSummaryTabIndex().subscribe(res => {
      this.selectedTab = res;
      if (this.selectedTab == 2) {
        this.is_Next_Required = false;
      }
    });
    this.clientSelectedSub = this.c2vService.getClientAwaitingPlacementSelected().subscribe(c2vRes => {
      if (c2vRes) {
        this.applicationID = c2vRes.pactApplicationID;
        //this.vcsClientID = c2vRes.vcsClientID;   

        this.getRefSummary();
        this.getPlacementSummary();

      } else {
        /* If user refresh(reload) the page the client selected data will be empty, so redirect them back to ClientAwaitingPlacement Page */
        this.router.navigate(['/vcs/client-awaiting-placement']);
      }
    });

    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event.url != "/vcs/referral-package/referral-ready-tracking" && event.url != "/vcs/referral-package/housing-preference" && event.url != "/vcs/referral-package/referral-and-placement-summary" && event.url != "/vcs/referral-package/homeless-information") {
          this.referralPackageNavService.setCurrentHomelessInformationTabIndex(0);
          this.referralPackageNavService.setCurrentReferralReadyTrackingTabIndex(null);
          this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(null);
          this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null);
        }
      }
    });
  }

  getRefSummary() {
    this.refSumSub = this.referralService.getRefSummary(this.applicationID).subscribe(res => {
      this.referralList = res as VCSReferralSummary[];


      this.completedReferralList = this.referralList.filter(d => {
        return d.referralStatusType === 514 || d.referralStatusType === 515 || d.referralStatusType === 516
          || d.referralStatusType === 517 || d.referralStatusType === 519 || d.referralStatusType === 520
          || d.referralStatusType === 521 || d.referralStatusType === 522 || d.referralStatusType === 523
          || d.referralStatusType === 524;
      });

      this.pendingReferralList = this.referralList.filter(d => {
        return d.referralStatusType === 512 || d.referralStatusType === 513 || d.referralStatusType === 518 || d.referralStatusType === 529;
      });

    });
  }

  getPlacementSummary() {
    this.placementSumSub = this.referralService.getPlacementSummary(this.applicationID).subscribe(res => {
      this.placementList = res as VCSPlacementSummary[];
    });
  }

  onGridReady(params: { api: any; columnApi: any }) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    params.api.setDomLayout('autoHeight');

    const allColumnIds = [];

    this.gridColumnApi.getAllColumns().forEach(function (column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
    this.gridApi.sizeColumnsToFit();

  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }


  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    this.selectedTab = tabChangeEvent.index;
    this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(this.selectedTab);
    this.is_Next_Required = true;

    if (this.selectedTab == 2) {
      this.is_Next_Required = false;
    }
  }

  onSave() { }

  // Next button click
  nextPage() {
    if (this.selectedTab < 2) {
      this.selectedTab = this.selectedTab + 1;
    }
    // else if (this.selectedTab == 2) {
    //   this.referralPackageNavService.setCurrentHomelessInformationTabIndex(0); 
    //   this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null); 
    //   this.router.navigate(['/vcs/referral-package/homeless-information']);
    // }
  }

  // Previous button click
  previousPage() {
    if (this.selectedTab != 0) {
      this.selectedTab = this.selectedTab - 1;
    } else if (this.selectedTab == 0) {
      this.referralPackageNavService.setCurrentHousingPreferenceTabIndex(0);
      this.referralPackageNavService.setCurrentReferralPlacementSummaryTabIndex(null);
      this.router.navigate(['/vcs/referral-package/housing-preference']);
    }
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');
    var params;

    if (this.selectedTab == 0) {
      params = {
        fileName: 'PendingReferrals-' + date + '-' + time
      };
    }
    else if (this.selectedTab == 1) {
      params = {
        fileName: 'CompletedReferrals-' + date + '-' + time
      };
    }
    else if (this.selectedTab == 2) {
      params = {
        fileName: 'Placements-' + date + '-' + time
      };
    }
    this.gridApi.exportDataAsExcel(params);
  }
  /* 
    onBtPrint() {
     
    
      //this.setPrinterFriendly(this.gridApi);
    console.log(1);
    debugger;
      var api = this.gridApi;
      this.gridApi.setDomLayout('print');
      this.gridApi.sizeColumnsToFit();
      
      console.log(2);
      setTimeout(function() {
        print();
        console.log(3);
        debugger;
        
        api.setDomLayout('autoHeight');
        api.sizeColumnsToFit();
        this.refOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Referrals Available</span>';
        
        console.log(4);
        //this.setNormal(this.gridApi);
      }, 2000);
    }
    
    setPrinterFriendly(api) {
      //var eGridDiv = document.querySelector('#myGrid');
      //eGridDiv.style.height = '';
      api.setDomLayout('print');
    }
    
    setNormal(api) {
      //var eGridDiv = document.querySelector('#myGrid');
      //eGridDiv.style.width = '600px';
     // eGridDiv.style.height = '200px';
    
      api.setDomLayout(null);
    }
   */
  ngOnDestroy() {
    if (this.sidenavTabSub) {
      this.sidenavTabSub.unsubscribe();
    }
    if (this.placementSumSub) {
      this.placementSumSub.unsubscribe();
    }
    if (this.clientSelectedSub) {
      this.clientSelectedSub.unsubscribe();
    }
    if (this.refSumSub) {
      this.refSumSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

}
