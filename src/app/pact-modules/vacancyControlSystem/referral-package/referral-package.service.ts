import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { VCSHomelessInfo, VCSClientShelterContact, VCSReferralReady } from './referral-package.model'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ApplicantPreference } from 'src/app/shared/applicant-preferences/applicant-preferences.model';

@Injectable({
    providedIn: 'root'
  })

  export class ReferralService {

    SERVER_URL = environment.pactApiUrl;
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    
    constructor(private httpClient: HttpClient) { }

    // Homeless Information
    getShelterData(applicationID: number){
        const getUrl = environment.pactApiUrl + 'VCSReferralPackage/GetShelterInfo/';
        return this.httpClient.get(getUrl + applicationID);
    }

    saveShelterData(shelterData: VCSHomelessInfo){  
      const saveUrl = environment.pactApiUrl + 'VCSReferralPackage/SaveShelterInfo';              
      return this.httpClient.post(saveUrl, shelterData, this.httpOptions);
     
    }

    ////Shelter Contact
    getContactbyShelter(referralID: number){
      const getUrl = environment.pactApiUrl + 'VCSReferralPackage/GetContactList/';
      return this.httpClient.get(getUrl + referralID);
    }

    deleteShelterContact(contactID: number){      
      const getUrl = environment.pactApiUrl + 'VCSReferralPackage/DeleteContact/';
      return this.httpClient.post(getUrl, contactID, this.httpOptions);
    }

    saveVCSShelterContact(contactData: VCSClientShelterContact){
      const saveUrl = environment.pactApiUrl + 'VCSReferralPackage/SaveShelterContact';              
      return this.httpClient.post(saveUrl, contactData, this.httpOptions);
     
    }

    // Referral Ready Tracking
    getClientDocData(applicationID: number){
      const getUrl = environment.pactApiUrl + 'VCSReferralPackage/GetReferralReadyInfo/';
      return this.httpClient.get(getUrl + applicationID);
    }

    saveReferral(referralInfo: VCSReferralReady){
      const saveUrl = environment.pactApiUrl + 'VCSReferralPackage/SaveReferralReadyInfo';              
      return this.httpClient.post(saveUrl, referralInfo, this.httpOptions);
     
    }

    getClientHistory(vcsClientPackageID: number){
      const getUrl = environment.pactApiUrl + 'VCSReferralPackage/GetReferralReadyHist/';
      return this.httpClient.get(getUrl + vcsClientPackageID);
    }

    //Summary tab
    getRefSummary(applicationID: number){
      const getUrl = environment.pactApiUrl + 'VCSReferralPackage/getRefSummary/';
      return this.httpClient.get(getUrl + applicationID);
    }

    getPlacementSummary(applicationID: number){
      const getUrl = environment.pactApiUrl + 'VCSReferralPackage/getPlacementSummary/';
      return this.httpClient.get(getUrl + applicationID);
    }

    //Housing Preference
    getApplicantPreferences(applicationID: number){
      const getUrl = environment.pactApiUrl + 'VCSReferralPackage/getApplicantPreferences/';
      return this.httpClient.get(getUrl + applicationID);
    }

    saveApplicantPreferences(preferenceObject: Object){
      const saveUrl = environment.pactApiUrl + 'VCSReferralPackage/saveApplicantPreferences';              
      return this.httpClient.post(saveUrl, preferenceObject, this.httpOptions);
    }
  }