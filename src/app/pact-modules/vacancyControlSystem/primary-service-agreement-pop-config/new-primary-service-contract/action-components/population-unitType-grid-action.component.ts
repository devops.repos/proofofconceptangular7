import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PopulationUnitTypeGridRow } from '../../primary-service-contract-agreement-population-model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "doc-action",
  template: `<mat-icon matTooltip='Edit' (click)="onView()" class="doc-action-icon" color="primary">edit</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete' class="doc-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class PopulationUnitTypeGridActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private rowSelected: PopulationUnitTypeGridRow;

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    this.prepareObject();
    this.params.context.componentParent.popUnitTypeViewInParent(this.rowSelected); 
  }

  onDelete() {
    this.prepareObject();
    this.params.context.componentParent.popUnitTypeDeleteInParent(this.rowSelected);
  }

  prepareObject(){
    this.rowSelected = {
      name: this.params.data.name,
      id:this.params.data.id,
      unitTypes: this.params.data.unitTypes,
      unitTypesString: this.params.data.unitTypesString,
      uniqueID : this.params.data.uniqueID
    };
  }

  refresh(): boolean {
    return false;
  }


}