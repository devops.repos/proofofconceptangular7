import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PlacementAgencyGridRow } from '../../primary-service-contract-agreement-population-model';


// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "doc-action",
  template: `<mat-icon matTooltip='Edit' (click)="onView()" class="doc-action-icon" color="primary">edit</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete' class="doc-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class PlacementAgencyGridActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private rowSelected: PlacementAgencyGridRow;

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    this.prepareObject();
    this.params.context.componentParent.placeAgencyItemViewInParent(this.rowSelected); 
  }

  onDelete() {
    this.prepareObject();
    this.params.context.componentParent.placeAgencyItemDeleteInParent(this.rowSelected);
  }

  prepareObject(){
    this.rowSelected = {
        id: this.params.data.id,
        uniqueID: this.params.data.uniqueID,
        name: this.params.data.name,
        primaryServiceContractName : this.params.data.primaryServiceContractName,
        placementAgencies: this.params.data.placementAgencies,
        placementAgenciesString: this.params.data.placementAgenciesString
    };
  }

  refresh(): boolean {
    return false;
  }


}