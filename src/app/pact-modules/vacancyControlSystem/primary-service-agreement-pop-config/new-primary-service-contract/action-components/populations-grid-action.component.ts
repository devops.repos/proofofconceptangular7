import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PopulationGridRow } from '../../primary-service-contract-agreement-population-model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "doc-action",
  template: `<mat-icon matTooltip='Edit' (click)="onView()" class="doc-action-icon" color="primary">edit</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete' class="doc-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class PopulationsGridActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private rowSelected: PopulationGridRow;

  constructor( ) { }

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {
    this.prepareObject();
    this.params.context.componentParent.popViewInParent(this.rowSelected); 
  }

  onDelete() {
    this.prepareObject();
    this.params.context.componentParent.popDeleteInParent(this.rowSelected);
  }

  prepareObject(){
    this.rowSelected = {
      name: this.params.data.name,
      desc: this.params.data.desc,
      id:this.params.data.id,
      applicationMatch:this.params.data.applicationMatch,
      applicationMatchString: this.params.data.applicationMatchString,
      uniqueID  : this.params.data.uniqueID
    };
  }

  refresh(): boolean {
    return false;
  }



}