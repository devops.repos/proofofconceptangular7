import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { CommonService } from 'src/app/services/helper-services/common.service';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { Subscription } from 'rxjs';
import { PopulationGridRow , PlacementAgency, PlacementAgencyGridRow} from '../primary-service-contract-agreement-population-model';
import {PrimaryServiceAgreementPopConfigService} from '../primary-service-agreement-pop.service';
import { ToastrService } from 'ngx-toastr';
import {  MatSelectChange } from '@angular/material';
import { PopulationGridEventService} from '../population-grid-event.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import {PlacementAgencyGridActionComponent} from './action-components/placement-agency-grid-action.component';
import {PlacementGridEventService} from '../placement-grid-event.service';

@Component({
  selector: 'app-additional-info-primary-service-contract',
  templateUrl: './additional-information.component.html',
 // styleUrls: ['./add-primary-service-contract.component.scss'],
})
export class AdditionalInfoPrimaryServiceContractComponent implements OnInit, OnDestroy {

  refGroupDetailsData : RefGroupDetails[];
  saveOrAddLabel3 : string = "Add";
  tabSelectedIndex: number = 0;

  placementAgencySub: Subscription;
  evtSvcSub2: Subscription;

  placementGridData: PlacementAgencyGridRow[];
  placementAgencies: PlacementAgency[];
  popNamesDataModel: any;
  placementAgencyDataModel: any;
  populationInputGridData: PopulationGridRow[];

  agreePopCtrlMultiple : boolean = false;
  agreePopSelectedText: string;
  agreePopSelectedID: number = 0;
  agreePopSelectedUniqueID: number = 0;

  placementAgencyCtrlMultiple : boolean = true;
  placementAgenciesSelectedText: any;

  placementColumnDefs: any;
  defaultColDef: any;
  gridOptions: GridOptions;
  pagination: any;
  rowSelection: any;
  context: any;
  placementGridComponents: any;

  additionalInfoFormGroup: FormGroup;

  @ViewChild('agPlacementGrid') agPlacementGrid: AgGridAngular;
  @Input() primaryServiceContractName : string;
  
constructor(
  private popGridEventSvc :PopulationGridEventService,
  private formBuilder: FormBuilder,
  private commonService : CommonService,
  private agreementConfigService : PrimaryServiceAgreementPopConfigService,
  private toastr: ToastrService,
  private placementEvtSvc : PlacementGridEventService
) {
      this.refGroupDetailsData = [];

      this.placementColumnDefs = [
        {
            headerName: 'Actions',
            field: 'action',
            width: 80,
            filter: false,
            sortable: false,
            resizable: false,
            pinned: 'left',
            suppressMenu: true,
            suppressSizeToFit: true,
            cellRenderer: 'actionRenderer',
        },
        {
          headerName: 'Population ID',
          field: 'id',
          minWidth: 100,
          filter: 'agTextColumnFilter',
          hide: true
        },
        {
          headerName: 'Service Contract',
          field: 'primaryServiceContractName', sortable: false,
          minWidth: 200,
          filter: 'agTextColumnFilter'
        },
        {
          headerName: 'Population Name',
          field: 'name', sortable: true,
          minWidth: 200,
          filter: 'agTextColumnFilter'
        },
        {
          headerName: 'Placement Agencies',
          field: 'placementAgenciesString',
          minWidth: 500,
          filter: 'agTextColumnFilter'
        },
      ];

      this.defaultColDef = {
        sortable: false,
        resizable: true,
        filter: true,
        comparator: this.customComparator
      };
      this.placementGridComponents = {
       actionRenderer: PlacementAgencyGridActionComponent
      };
      this.rowSelection = 'single';
      this.pagination = true;
      this.context = { componentParent: this };

      this.gridOptions = {
        rowHeight: 35
      } as GridOptions;

      this.placementGridData = [];
      this.placementAgencies = [];
      this.populationInputGridData = [];
  }

  ngOnInit() {
  
   this.additionalInfoFormGroup = this.formBuilder.group({
        populationNamesCtrl :  ['',], // Validators.required
        placementAgencyCtrl :  ['',],
    });
    let that = this;
    this.evtSvcSub2 = this.popGridEventSvc.subscribeEventListner().subscribe(res => {
        if(res != undefined && res != null){
            that.populationInputGridData = res;
            if(that.populationInputGridData && that.populationInputGridData.length == 0){
                that.placementGridData = [];
            }
            else {
              that.placementGridData = that.placementGridData.map(it => {
                          it.name = that.populationInputGridData.find(x => x.id == it.id).name;
                          it.primaryServiceContractName = that.primaryServiceContractName;
                          return it;
                        }
                  );
            }
        }
    });

    this.placementAgencySub = this.agreementConfigService.getAllPlacementAgency().subscribe(
      res => {
        this.placementAgencies = res as PlacementAgency[];
      },
      err => {
        error => console.error('Error!', error)
      }
    );
  }

  placeAgencyItemViewInParent(rowSelected : PlacementAgencyGridRow){
      this.agreePopSelectedID = rowSelected.id;
      this.agreePopSelectedUniqueID = rowSelected.uniqueID;
      this.additionalInfoFormGroup.get('populationNamesCtrl').setValue(rowSelected.uniqueID); // uniqueID is being set in [value] in case NEW
     // this.additionalInfoFormGroup.get('populationNamesCtrl').setValue(rowSelected.name); // name is being set in [value] in case NEW
      this.additionalInfoFormGroup.get('placementAgencyCtrl').setValue(rowSelected.placementAgencies.map(it => { return it.agencyNo_SiteNo }));
      this.agreementPopSelectChange(null);
      this.placementSelectChange(null);
      this.saveOrAddLabel3 = "Save";
      this.additionalInfoFormGroup.controls.populationNamesCtrl.disable();
      console.log('placeAgencyItemViewInParent :: this.agreePopSelectedID = ' + this.agreePopSelectedID);
      console.log('placeAgencyItemViewInParent :: this.agreePopSelectedUniqueID = ' + this.agreePopSelectedUniqueID);
  }

  placeAgencyItemDeleteInParent(rowSelected : PlacementAgencyGridRow){
    let newArray = this.placementGridData.filter(it => it.name != rowSelected.name);

    this.gridOptions.api.updateRowData({ remove: this.placementGridData }); // remove existing
    this.placementGridData = newArray;
    this.gridOptions.api.updateRowData({ add: this.placementGridData }); // add new
    this.gridOptions.api.setSortModel([ { colId: 'name', sort: 'asc' },  ]); // SORT

    this.placementEvtSvc.emitCompletedEvent(this.placementGridData) // EMIT
  }

  savePlacementAgency(){
    let that = this;
    let selectedPlacementAgencyArray: PlacementAgency[] = [];
    if(this.placementAgencyDataModel)
      this.placementAgencyDataModel.forEach(it => {
          let obj = that.placementAgencies.find(ag => ag.agencyNo_SiteNo == it);
          selectedPlacementAgencyArray.push(obj);
      });

    let row : PlacementAgencyGridRow = {
      id: that.agreePopSelectedID,
      name: that.agreePopSelectedText,
      primaryServiceContractName : that.primaryServiceContractName,
      placementAgencies: selectedPlacementAgencyArray,
      placementAgenciesString: that.placementAgenciesSelectedText,
      uniqueID : this.agreePopSelectedUniqueID
    }

    if(row.name == undefined || row.name == null || row.name === ''){
      this.toastr.warning('Please select Agreement Population.');
      return;
    }

    else if(row.placementAgencies == undefined || row.placementAgencies == null || row.placementAgencies.length == 0){
      this.toastr.warning('Please select atleast one Placement Agency.');
      return;
    }

    let excludeItemGridData : PlacementAgencyGridRow[] = [];
    if(row.id > 0){
      excludeItemGridData = this.placementGridData.filter(it => it.id != row.id);
    } else{
      excludeItemGridData = this.placementGridData.filter(it => it.name.toUpperCase() != row.name.toUpperCase());
    }

    this.clearPlacementAgency();
    this.gridOptions.api.updateRowData({ remove: this.placementGridData }); // old

    this.placementGridData = excludeItemGridData.concat(row); // updated

    this.gridOptions.api.updateRowData({ add: this.placementGridData });
    this.gridOptions.api.setSortModel([ { colId: 'name', sort: 'asc' },  ]); // SORT

    this.placementEvtSvc.emitCompletedEvent(this.placementGridData) // EMIT
  }

  clearPlacementAgency(){
    this.agreePopSelectedUniqueID = 0;
    this.agreePopSelectedID = 0;
    this.agreePopSelectedText = '';
    this.placementAgenciesSelectedText = '';
    this.additionalInfoFormGroup.get('populationNamesCtrl').setValue(null);
    this.additionalInfoFormGroup.get('placementAgencyCtrl').setValue(null);
    this.placementAgencyDataModel = null;
    this.saveOrAddLabel3 = "Add";
    this.additionalInfoFormGroup.controls.populationNamesCtrl.enable();
  }

  agreementPopSelectChange(evnt: MatSelectChange){
      let wrapper = this.populationInputGridData.map(it => {
        return {
          "id": it.id, "name": it.name, "uniqueID": it.uniqueID
        }
      });
      if (this.agreePopCtrlMultiple) {
        this.agreePopSelectedText = this.commonService.getDropDownTextMultiSelect(this.popNamesDataModel, wrapper);
      } else {
        let res = this.commonService.getDropDownText(this.popNamesDataModel, wrapper);
        if(res && res.length > 0){
          this.agreePopSelectedText = this.commonService.getDropDownText(this.popNamesDataModel, wrapper)[0].name;
          this.agreePopSelectedID = this.populationInputGridData.find(it => it.name == this.agreePopSelectedText).id;
          this.agreePopSelectedUniqueID = this.populationInputGridData.find(it => it.name == this.agreePopSelectedText).uniqueID;
        }
      }
      // console.log('this.agreePopSelectedText = ' + this.agreePopSelectedText);
      // console.log('this.agreePopSelectedID = ' + this.agreePopSelectedID);
      // console.log('this.agreePopSelectedUniqueID = ' + this.agreePopSelectedUniqueID);
  }

  placementSelectChange(evnt: MatSelectChange){
    let tempArray : PlacementAgency[] = null;
    let placementAgenciesWrapper = this.placementAgencies.map(it => {
      return { "agencyNo_SiteNo": it.agencyNo_SiteNo, "createdBy": it.createdBy, 
                "isActive": it.isActive, "placementAgencyName": it.placementAgencyName, "updatedBy": it.updatedBy,
                "id": it.agencyNo_SiteNo, // changed
                "name": it.placementAgencyName  // new properties
            }
    });
    if (this.placementAgencyCtrlMultiple) {
      tempArray = this.commonService.getDropDownText(this.placementAgencyDataModel, placementAgenciesWrapper);
    } else {
      tempArray = this.commonService.getDropDownText(this.placementAgencyDataModel, placementAgenciesWrapper)[0].name;
    }

    if(tempArray && tempArray.length > 0)
      this.placementAgenciesSelectedText = tempArray.map(it => {
          return it.placementAgencyName;
      }).join(', ');
    //  console.log('this.placementAgenciesSelectedText = ' + this.placementAgenciesSelectedText);
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
  }

  customComparator(valueA, valueB) {
    return valueA.toLowerCase().localeCompare(valueB.toLowerCase());
  }

  ngOnDestroy() {
    if(this.evtSvcSub2) this.evtSvcSub2.unsubscribe();
    if(this.placementAgencySub) this.placementAgencySub.unsubscribe();
  }

}