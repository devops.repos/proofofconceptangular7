import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import {  FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { GridOptions } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { PlacementAgencyGridRow, PopulationGridRow, PopulationUnitTypeGridRow, UnitTypeSpecial } from '../primary-service-contract-agreement-population-model';
import { AuthData } from 'src/app/models/auth-data.model';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent, MatSelectChange } from '@angular/material';
import { PopulationGridEventService} from '../population-grid-event.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import {PopulationUnitTypeGridActionComponent} from './action-components/population-unitType-grid-action.component';
import {PopulationsGridActionComponent} from './action-components/populations-grid-action.component';
import {PlacementGridEventService} from '../placement-grid-event.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-contract-info-primary-service-contract',
  templateUrl: './contract-information.component.html',
 // styleUrls: ['./add-primary-service-contract.component.scss'],
})
export class ContractInfoPrimaryServiceContractComponent implements OnInit, AfterViewInit, OnDestroy {

  refGroupDetailsData : RefGroupDetails[];
  saveOrAddLabel : string = "Add";
  saveOrAddLabel2 : string = "Add";
  includePopulationModel: number;
  disabledStype : any;
  psNamelabel : string = "Enter the new primary service contract name";
  tabSelectedIndex: number = 0;
  userData: AuthData;

  commonServiceSub: Subscription;
  placementEvtSvcSub: Subscription;

  gridOptionsPop: GridOptions;
  gridOptionsPopUnitType: GridOptions;
  pagination: any;
  rowSelection: any;
  context1: any;
  context2: any;
  populationUnitColumnDefs: any;
  populationColumnDefs: any;
  defaultColDef: any;
  unitTypeGridComponents: any;  //frameworkComponents
  popGridComponents: any;  //frameworkComponents

  agreePopCtrlMultiple : boolean = false;
  agreePopSelectedText: string;
  agreePopSelectedID: number = 0;
  agreePopSelectedUniqueID: number = 0;
  agreePopDataModel: any;

  unitTypeCtrlMultiple : boolean = true;
  unitTypeSelectedText: any;
  unitTypesDataModel: [];
  popUnitTypesGridData: PopulationUnitTypeGridRow[];
  populationGridData: PopulationGridRow[];
  placementGridData: PlacementAgencyGridRow[];

  rentalSubsidiesData: any;
  yesNoTypes: any;
  rentalSubsidies: any;
  unitTypes: UnitTypeSpecial[];

  contractInfoFormGroup: FormGroup;

  @ViewChild('agPopulationUnitTypeGrid') agPopulationUnitTypeGrid: AgGridAngular;
  @ViewChild('agPopulationsGrid') agPopulationsGrid: AgGridAngular;
  
constructor(
  private formBuilder: FormBuilder,
  private commonService : CommonService,
  private popGridEventSvc :PopulationGridEventService,
  private toastr: ToastrService, private confirmDialogService: ConfirmDialogService,
  private placementEvtSvc : PlacementGridEventService
) {
      this.refGroupDetailsData = [];

      this.defaultColDef = {
        sortable: false,
        resizable: true,
        filter: true,
        comparator: this.customComparator
      };
      this.unitTypeGridComponents = {
        actionRenderer: PopulationUnitTypeGridActionComponent
      };
      this.popGridComponents = {
         actionRenderer: PopulationsGridActionComponent
       };

      this.rowSelection = 'single';
      this.pagination = true;
      this.context1 = { componentParent: this };
      this.context2 = { componentParent: this };

      this.gridOptionsPop = {
        rowHeight: 35
      } as GridOptions;
      this.gridOptionsPopUnitType = {
        rowHeight: 35
      } as GridOptions;

      this.populationColumnDefs = [
        {
            headerName: 'Actions',
            field: 'action',
            width: 80,
            filter: false,
            sortable: false,
            resizable: false,
            pinned: 'left',
            suppressMenu: true,
            suppressSizeToFit: true,
            cellRenderer: 'actionRenderer',
        },
        {
          headerName: 'Population ID',
          field: 'id',
          minWidth: 100,
          filter: 'agTextColumnFilter',
          hide: true
        },
        {
          headerName: 'Population Name',
          field: 'name', sortable: true,
          minWidth: 150,
          filter: 'agTextColumnFilter'
        },
        {
          headerName: 'Population Description',
          field: 'desc',
          minWidth: 400,
          filter: 'agTextColumnFilter'
        },
        {
          headerName: 'Application Match',
          field: 'applicationMatchString',
          minWidth: 100,
          filter: 'agTextColumnFilter'
        },
        {
          headerName: 'Unique ID',
          field: 'uniqueID',
          minWidth: 100,
          filter: 'agTextColumnFilter',
          hide: true
        }
      ];

      this.populationUnitColumnDefs = [
        {
            headerName: 'Actions',
            field: 'action',
            width: 80,
            filter: false,
            sortable: false,
            resizable: false,
            pinned: 'left',
            suppressMenu: true,
            suppressSizeToFit: true,
            cellRenderer: 'actionRenderer',
        },
        {
          headerName: 'Population ID',
          field: 'id',
          minWidth: 100,
          filter: 'agTextColumnFilter',
          hide: true
        },
        {
          headerName: 'Population Name',
          field: 'name', sortable: true,
          minWidth: 150,
          filter: 'agTextColumnFilter'
        },
        {
          headerName: 'Unit Types',
          field: 'unitTypesString',
          minWidth: 500,
          filter: 'agTextColumnFilter'
        },
        {
          headerName: 'Unique ID',
          field: 'uniqueID',
          minWidth: 100,
          filter: 'agTextColumnFilter',
          hide: true
        }
      ];

      this.popUnitTypesGridData = [];
      this.populationGridData = [];
  }

  ngOnInit() {
  
   this.contractInfoFormGroup = this.formBuilder.group({
        additionalcommentsCtrl: ['',Validators.required],
        primaryServiceNameCtrl: ['',Validators.required],
        primaryServiceContractIDHiddenCtrl: ['',],
        includePopulationCtrl:  ['',CustomValidators.radioGroupRequired()],
        popIDHiddenCtrl: ['',],
        popUniqueIDHiddenCtrl: ['',],
        popNameCtrl: ['',],
        popDescriptionCtrl: ['',],
        rentalSubsidiesCtrl:  ['',Validators.required],
        agreePopCtrl:  ['',],
        unitTypesCtrl:  ['',],
        applicationMatchCtrl:  ['',Validators.required],
    });

    this.placementEvtSvcSub = this.placementEvtSvc.subscribeEventListener().subscribe(res => {
      if(res != undefined && res != null)
        this.placementGridData = res;
    });
  
    this.loadRefGroupDetails();
  }

  ngAfterViewInit(){
    this.gridOptionsPop.api.setSortModel([ { colId: 'name', sort: 'asc' },  ]); // SORT
    this.gridOptionsPopUnitType.api.setSortModel([ { colId: 'name', sort: 'asc' },  ]); // SORT
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
  }
 

  loadRefGroupDetails()
  {
    const yesNoRefGroupId = 7; const rentalSubsidiesRefGroupId = 15; const unitTypeRefGroupId = 29;
    this.commonServiceSub = this.commonService.getRefGroupDetails(yesNoRefGroupId + ',' + rentalSubsidiesRefGroupId+ ',' + unitTypeRefGroupId)
            .subscribe(
              res => {
                const data = res.body as RefGroupDetails[];
                this.refGroupDetailsData = data;
                this.rentalSubsidies = data.filter(d => (d.refGroupID === rentalSubsidiesRefGroupId));
                let tempUnitTypes = data.filter(d => (d.refGroupID === unitTypeRefGroupId));
                this.yesNoTypes = data.filter(d => d.refGroupID === yesNoRefGroupId && (d.refGroupDetailDescription.toUpperCase() === 'YES' || d.refGroupDetailDescription.toUpperCase() === 'NO'));

                this.unitTypes = tempUnitTypes.map(it => {
                  const myObj : UnitTypeSpecial = {
                  name : it.refGroupDetailDescription,
                  id : it.refGroupDetailID.toString(),
                  refGroupDetailID : it.refGroupDetailID,
                  refGroupDetailDescription : it.refGroupDetailDescription,
                  refGroupID : it.refGroupID,
                  refGroupDescription : it.refGroupDescription,
                  sortOrder : it.sortOrder,
                  longDescription : it.longDescription,
                  parentRefGroupDetailID : it.parentRefGroupDetailID}
                  return myObj; });
              },
              error => console.error('Error while fetching getRefGroupDetails data.', error)
            );
   }

   saveUnitTypes(){
    let row : PopulationUnitTypeGridRow = {
      id: this.agreePopSelectedID,
      name: this.agreePopSelectedText,
      unitTypes: this.unitTypesDataModel,
      unitTypesString: this.unitTypeSelectedText,
      uniqueID : this.agreePopSelectedUniqueID
    }
    console.log('saveUnitTypes row.uniqueID = ' + row.uniqueID);

    if(row.name == undefined || row.name == null || row.name === ''){
      this.toastr.warning('Please select Agreement Population.');
      return;
    }

    else if(row.unitTypes == undefined || row.unitTypes == null || row.unitTypes.length == 0){
      this.toastr.warning('Please select atleast one Unit Type.');
      return;
    }

    let excludeItemGridData : PopulationUnitTypeGridRow[] = [];
    if(row.id && row.id > 0){
      excludeItemGridData = this.popUnitTypesGridData.filter(it => it.id != row.id);
    } else{
      // excludeItemGridData = this.popUnitTypesGridData.filter(it => it.name.toUpperCase() != row.name.toUpperCase());
      excludeItemGridData = this.popUnitTypesGridData.filter(it => it.uniqueID != row.uniqueID);
    }

    this.clearUnitTypes();
    this.gridOptionsPopUnitType.api.updateRowData({ remove: this.popUnitTypesGridData }); // old

    this.popUnitTypesGridData = excludeItemGridData.concat(row); // updated

    this.gridOptionsPopUnitType.api.updateRowData({ add: this.popUnitTypesGridData });
    this.gridOptionsPopUnitType.api.setSortModel([ { colId: 'name', sort: 'asc' },  ]); // SORT
   }

   clearUnitTypes(){
      this.agreePopSelectedID = 0;
      this.agreePopSelectedUniqueID = 0;
      this.agreePopSelectedText = '';
      this.unitTypeSelectedText = '';
      this.contractInfoFormGroup.get('agreePopCtrl').setValue(null);
      this.contractInfoFormGroup.get('unitTypesCtrl').setValue(null);
      this.unitTypesDataModel = [];
      this.saveOrAddLabel2 = "Add";
   }

   savePopulation(){

    let id = this.contractInfoFormGroup.get('popIDHiddenCtrl').value;
    if(id === undefined || id === null || Number(id) > 0 == false)
      this.contractInfoFormGroup.get('popIDHiddenCtrl').setValue(0); // DEFAULT

    let _uniqueID = this.contractInfoFormGroup.get('popUniqueIDHiddenCtrl').value;
    if(_uniqueID === undefined || _uniqueID === null || Number(_uniqueID) > 0 == false)
      this.contractInfoFormGroup.get('popUniqueIDHiddenCtrl').setValue((new Date()).getTime()); // DEFAULT

    let row : PopulationGridRow = {
      id: Number(this.contractInfoFormGroup.get('popIDHiddenCtrl').value),
      name: this.contractInfoFormGroup.get('popNameCtrl').value,
      desc: this.contractInfoFormGroup.get('popDescriptionCtrl').value,
      applicationMatch: this.contractInfoFormGroup.get('applicationMatchCtrl').value,
      applicationMatchString: this.contractInfoFormGroup.get('applicationMatchCtrl').value === true ? 'Yes' : 'No',
      uniqueID : Number(this.contractInfoFormGroup.get('popUniqueIDHiddenCtrl').value)
    }
    console.log('savePopulation row.id = ' + row.id);
    console.log('savePopulation row.uniqueID = ' + row.uniqueID);
    
    if(row.name == undefined || row.name == null || row.name.trim() === ''){
      this.toastr.warning('Please enter Population Name.');
      return;
    }
    else if(row.desc == undefined || row.desc == null || row.desc.trim() === ''){
      this.toastr.warning('Please enter Population Description.');
      return;
    }

    row.name = row.name.trim();
    row.desc = row.desc.trim();

    if(this.populationGridData.filter(it => it.uniqueID != row.uniqueID && it.name.toUpperCase() === row.name.toUpperCase()) != null 
      && this.populationGridData.filter(it => it.uniqueID != row.uniqueID && it.name.toUpperCase() === row.name.toUpperCase()).length > 0){

      this.toastr.warning('Duplicate Population Name Exists.');
      return;
    }

    let excludeItemGridData : PopulationGridRow[] = [];
    if(row.id && row.id > 0){
      excludeItemGridData = this.populationGridData.filter(it => it.id != row.id);
    } else {
      // excludeItemGridData = this.populationGridData.filter(it => it.name.toUpperCase() != row.name.toUpperCase());
      excludeItemGridData = this.populationGridData.filter(it => it.uniqueID != row.uniqueID); // uniqueID
    }

    this.clearPopulation();
    this.gridOptionsPop.api.updateRowData({ remove: this.populationGridData }); // old

    this.populationGridData = excludeItemGridData.concat(row); // updated

    this.gridOptionsPop.api.updateRowData({ add: this.populationGridData });
    this.gridOptionsPop.api.setSortModel([ { colId: 'name', sort: 'asc' },  ]); // SORT

    this.popGridEventSvc.emitCompletedEvent(this.populationGridData); // EMIT

    let that = this;
      this.popUnitTypesGridData = this.popUnitTypesGridData.map(it => {
            //  it.name = that.populationGridData.find(x => x.id > 0 ? (x.id == it.id) : (x.name.toUpperCase() == it.name.toUpperCase())).name;
              it.name = that.populationGridData.find(x => x.id > 0 ? (x.id == it.id) : (x.uniqueID == it.uniqueID)).name;
              return it;
            }
      );
   }

   clearPopulation(){
      this.contractInfoFormGroup.get('popUniqueIDHiddenCtrl').setValue('');
      this.contractInfoFormGroup.get('popIDHiddenCtrl').setValue('');
      this.contractInfoFormGroup.get('popNameCtrl').setValue('');
      this.contractInfoFormGroup.get('popDescriptionCtrl').setValue('');
      this.contractInfoFormGroup.get('applicationMatchCtrl').setValue(null);
      this.saveOrAddLabel = "Add";
   }

   AgreementPopSelectChange(evnt: MatSelectChange) {
      let wrapper = this.populationGridData.map(it => {
        return {
          "id": it.id, "name": it.name, "uniqueID": it.uniqueID
        }
      });
      if (this.agreePopCtrlMultiple) {
        this.agreePopSelectedText = this.commonService.getDropDownTextMultiSelect(this.agreePopDataModel, wrapper);
      } else {
        let res = this.commonService.getDropDownText(this.agreePopDataModel, wrapper);
        if(res && res.length > 0){
          this.agreePopSelectedText = this.commonService.getDropDownText(this.agreePopDataModel, wrapper)[0].name;
          this.agreePopSelectedID = this.populationGridData.find(it => it.name == this.agreePopSelectedText).id;
          this.agreePopSelectedUniqueID = this.populationGridData.find(it => it.name == this.agreePopSelectedText).uniqueID;
        } 
      }
      // console.log('this.agreePopSelectedText = ' + this.agreePopSelectedText);
      // console.log('this.agreePopSelectedID = ' + this.agreePopSelectedID);
      // console.log('this.agreePopSelectedUniqueID = ' + this.agreePopSelectedUniqueID);
    }

    unitTypeSelectChange(evnt: MatSelectChange){
      let tempArray : UnitTypeSpecial[] = null;
      if (this.unitTypeCtrlMultiple) {
        tempArray = this.commonService.getDropDownTextMultiSelect(this.unitTypesDataModel, this.unitTypes);
      } else {
        tempArray = this.commonService.getDropDownTextMultiSelect(this.unitTypesDataModel, this.unitTypes)[0].name;
      }
      if(tempArray && tempArray.length > 0)
        this.unitTypeSelectedText = tempArray.map(it => {
            return it.name;
        }).join(', ');
        //  console.log('this.unitTypeSelectedText = ' + this.unitTypeSelectedText);
    }

    includePopulationChange(event: MatRadioChange){

        if(event.source.name === 'includePopulationCtrl' && event.source.value == 33){
            this.setIncludePopulation(true);
        }
        else if(event.source.name === 'includePopulationCtrl' && event.source.value == 34){
          this.showConfimDialog();
        }
        else
          return;
    }

    showConfimDialog(){
        const title = 'Confirm include Population';
        const primaryMessage = 'All the Unit Types and Placement Agencies information would be lost ?';
        const secondaryMessage = ''
        const confirmButtonName = 'Yes';
        const dismissButtonName = 'No';

        this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
          (positiveResponse) => {
              this.setIncludePopulation(false); // because OK for NO population
          },
          (negativeResponse) => {
              this.setIncludePopulation(true);  // because NO Population is confirmed
          },
        );
    }

    setIncludePopulation(include : boolean){
      if(include){
          this.includePopulationModel = 33;
          this.contractInfoFormGroup.get('popNameCtrl').setValue('');
          this.contractInfoFormGroup.get('popDescriptionCtrl').setValue('');
          this.contractInfoFormGroup.get('popNameCtrl').enable();
          this.contractInfoFormGroup.get('popDescriptionCtrl').enable();
          this.disabledStype = {
            background: 'white',
        }
      }
      else {
          this.includePopulationModel = 34;
          this.contractInfoFormGroup.get('popNameCtrl').setValue('NO POPULATION');
          this.contractInfoFormGroup.get('popDescriptionCtrl').setValue('NO POPULATION');
          this.contractInfoFormGroup.get('popNameCtrl').disable();
          this.contractInfoFormGroup.get('popDescriptionCtrl').disable();
          this.disabledStype = {
            background: '#dddddd;',
          }
          this.populationGridData = [];
          this.popUnitTypesGridData = [];
          this.popGridEventSvc.emitCompletedEvent(this.populationGridData) // EMIT
      }
    }

  popViewInParent(rowSelected : PopulationGridRow){
    this.contractInfoFormGroup.get('popUniqueIDHiddenCtrl').setValue(rowSelected.uniqueID);
    this.contractInfoFormGroup.get('popIDHiddenCtrl').setValue(rowSelected.id);
    this.contractInfoFormGroup.get('popNameCtrl').setValue(rowSelected.name);
    this.contractInfoFormGroup.get('popDescriptionCtrl').setValue(rowSelected.desc);
    this.contractInfoFormGroup.get('applicationMatchCtrl').setValue(rowSelected.applicationMatch);
    this.saveOrAddLabel = "Save";
  }

  popDeleteInParent(rowSelected : PopulationGridRow){
    let newArray : any; let existItemUnitTypeGrid : PopulationUnitTypeGridRow; let existItemPlacmentGrid : PlacementAgencyGridRow;
    if(rowSelected.id > 0){
      newArray = this.populationGridData.filter(it => it.id !== rowSelected.id);
      existItemUnitTypeGrid = this.popUnitTypesGridData ? this.popUnitTypesGridData.find(it =>  it.id == rowSelected.id) : null; // DO NOT delete if exists in UnitTypesGrid or PlacementAgencyGrid
      existItemPlacmentGrid = this.placementGridData ? this.placementGridData.find(it =>  it.id == rowSelected.id) : null;
    }
    else{
      newArray = this.populationGridData.filter(it => it.name.toUpperCase() !== rowSelected.name.toUpperCase());
      existItemUnitTypeGrid = this.popUnitTypesGridData ? this.popUnitTypesGridData.find(it => it.name.toUpperCase() == rowSelected.name.toUpperCase()) : null; // DO NOT delete if exists in UnitTypesGrid or PlacementAgencyGrid
      existItemPlacmentGrid = this.placementGridData ? this.placementGridData.find(it => it.name.toUpperCase() == rowSelected.name.toUpperCase()) : null;
    }
    if(existItemUnitTypeGrid != undefined || existItemUnitTypeGrid != null){
      this.toastr.warning('This Agreement is used in Agreement-UnitType mapping. First delete there, and try again !!!');
      return;
    }
    if(existItemPlacmentGrid != undefined || existItemPlacmentGrid != null){
      this.toastr.warning('This Agreement is used in Agreement-Placement mapping. First delete there, and try again !!!');
      return;
    }

    this.gridOptionsPop.api.updateRowData({ remove: this.populationGridData }); // remove existing
    this.populationGridData = newArray;
    this.gridOptionsPop.api.updateRowData({ add: this.populationGridData }); // add new
    this.gridOptionsPop.api.setSortModel([ { colId: 'name', sort: 'asc' },  ]); // SORT

    this.popGridEventSvc.emitCompletedEvent(this.populationGridData) // EMIT
  }

  popUnitTypeViewInParent(rowSelected : PopulationUnitTypeGridRow){
    this.agreePopSelectedID = rowSelected.id;
    this.agreePopSelectedUniqueID = rowSelected.uniqueID;
    this.contractInfoFormGroup.get('agreePopCtrl').setValue(rowSelected.uniqueID); // uniqueID is being set in [value] in case NEW
  //  this.contractInfoFormGroup.get('agreePopCtrl').setValue(rowSelected.name); // name is being set in [value] in case NEW
    this.contractInfoFormGroup.get('unitTypesCtrl').setValue(rowSelected.unitTypes);
    this.AgreementPopSelectChange(null);
    this.unitTypeSelectChange(null);
    this.saveOrAddLabel2 = "Save";
    console.log('popUnitTypeViewInParent :: this.agreePopSelectedID = ' + this.agreePopSelectedID);
    console.log('popUnitTypeViewInParent :: this.agreePopSelectedUniqueID = ' + this.agreePopSelectedUniqueID);
  }

  popUnitTypeDeleteInParent(rowSelected : PopulationUnitTypeGridRow){
    let newArray = this.popUnitTypesGridData.filter(it => it.name != rowSelected.name);

    this.gridOptionsPopUnitType.api.updateRowData({ remove: this.popUnitTypesGridData }); // remove existing
    this.popUnitTypesGridData = newArray;
    this.gridOptionsPopUnitType.api.updateRowData({ add: this.popUnitTypesGridData }); // add new
    this.gridOptionsPopUnitType.api.setSortModel([ { colId: 'name', sort: 'asc' },  ]); // SORT
  }

  customComparator(valueA, valueB) {
    return valueA.toLowerCase().localeCompare(valueB.toLowerCase());
  }

  primaryServiceNameBlurEvent(evnt){
    this.popGridEventSvc.emitCompletedEvent(this.populationGridData);
  }

  ngOnDestroy() {
    if(this.commonServiceSub) this.commonServiceSub.unsubscribe();
    if(this.placementEvtSvcSub) this.placementEvtSvcSub.unsubscribe();
  }

}