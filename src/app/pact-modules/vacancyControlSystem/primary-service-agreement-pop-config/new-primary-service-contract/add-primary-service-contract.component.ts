import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import {  FormGroup, FormBuilder } from '@angular/forms';
import {PrimaryServiceAgreementPopConfigService} from '../primary-service-agreement-pop.service';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { PrimaryServiceAgreementPopulationConfiguration, PrimaryServiceContract, AgreementPopulation,PopulationGridRow,
          AgreementPopulationUnitTypeMapping, AgreementRentalSubsidyMapping, PlacementAgency, AgreementPopulationPlacementAgency,
          PopulationUnitTypeGridRow, PlacementAgencyGridRow } 
          from '../primary-service-contract-agreement-population-model';
import { ToastrService } from 'ngx-toastr';
import { MatTabChangeEvent } from '@angular/material';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import {ContractInfoPrimaryServiceContractComponent} from './contract-information.component';
import {AdditionalInfoPrimaryServiceContractComponent} from './additional-information.component';


@Component({
  selector: 'app-add-primary-service-contract',
  templateUrl: './add-primary-service-contract.component.html',
  styleUrls: ['./add-primary-service-contract.component.scss'],
})
export class AddPrimaryServiceContractComponent implements OnInit, OnDestroy {

  static psName : string = "";
  refGroupDetailsData : RefGroupDetails[];
  formValid : boolean = false;
 
  tabSelectedIndex: number = 0;
  userData: AuthData;

  userDataSub: Subscription;
  agreeConfigSvcSub1: Subscription;
  agreeConfigSvcSub2: Subscription;
  agreeConfigSvcSub3: Subscription;

  editPrimaryServiceContractID: number = 0; // query param for EDIT functionality
  pageNameX: string = 'Add New Contract';
  btnLabel: string = 'Exit';

  addPrimaryServiceContractFormGroup: FormGroup;

  @ViewChild(ContractInfoPrimaryServiceContractComponent) contractInfoComponent: ContractInfoPrimaryServiceContractComponent;
  @ViewChild(AdditionalInfoPrimaryServiceContractComponent) additionalInfoComponent: AdditionalInfoPrimaryServiceContractComponent;
  
constructor(

  private router: Router,
  private formBuilder: FormBuilder,
  private agreementConfigService : PrimaryServiceAgreementPopConfigService,
  private userService :UserService,
  private toastr: ToastrService, private confirmDialogService: ConfirmDialogService,
  private activatedRoute: ActivatedRoute
  ) {
  this.refGroupDetailsData = [];

  }

    ngOnInit() {
    
    this.addPrimaryServiceContractFormGroup = this.formBuilder.group({

      contractInfoControls: this.contractInfoComponent.contractInfoFormGroup,
      additionalInfoControls: this.additionalInfoComponent.additionalInfoFormGroup

      });

      this.userDataSub = this.userService.getUserData().subscribe(res => {
        this.userData = res;
      });

      this.activatedRoute.paramMap.subscribe(params => {
          let id : number = Number(params.get('primaryServiceContractID'));
          this.editPrimaryServiceContractID = (id > 0) ? id : 0;

            if(this.editPrimaryServiceContractID != undefined && this.editPrimaryServiceContractID != null && this.editPrimaryServiceContractID > 0){
              setTimeout(() => {
                this.getPrimaryServiceAgreementPopulationConfiguration(this.editPrimaryServiceContractID);
              }, 1000);
              this.pageNameX = 'Edit Contract';
            }
      });

    }

    getPrimaryServiceAgreementPopulationConfiguration(primaryServiceContractID : number)  {

        this.agreeConfigSvcSub3 = this.agreementConfigService.getPrimaryServiceAgreementPopulationConfiguration(primaryServiceContractID).subscribe(
          data => {
            const formDataFromDB : PrimaryServiceAgreementPopulationConfiguration = data;
                setTimeout(() => {
                  this.setFormDataFromDB(formDataFromDB);
                }, 1000);
          },
          error => {
            this.toastr.warning('Error while getting PrimaryServiceAgreementPopulationConfiguration = ' + primaryServiceContractID, '');
            console.error(error);
          }
      );
    }

    setFormDataFromDB(formDataFromDB : PrimaryServiceAgreementPopulationConfiguration){
        let formDataFromDB2 = formDataFromDB;
        this.setPrimaryServiceContract(formDataFromDB.primaryServiceContract);
        this.setAgreementPopulationArray(formDataFromDB.agreementPopulationArray);
        this.setRentalSubsidyArray(formDataFromDB.rentalSubsidyArray);
        this.setUnitTypeArray(formDataFromDB.unitTypeArray);
        this.setPlacementAgencyArray(formDataFromDB.placementAgencyArray);
    }

    setPrimaryServiceContract(psContract : PrimaryServiceContract){
      this.contractInfoComponent.contractInfoFormGroup.get('primaryServiceContractIDHiddenCtrl').setValue(psContract.primaryServiceContractID);
      this.contractInfoComponent.contractInfoFormGroup.get('primaryServiceNameCtrl').setValue(psContract.name);
      //this.contractInfoComponent.contractInfoFormGroup.get('includePopulationCtrl').setValue(val);
      this.contractInfoComponent.psNamelabel = "Primary service contract name";
      let val : number = psContract.hasPopulation === true ? 33 : 34;
      this.contractInfoComponent.includePopulationModel = val;

      this.contractInfoComponent.setIncludePopulation(psContract.hasPopulation);
    }

    setAgreementPopulationArray(agreementPopulationArray : AgreementPopulation[]){
        let populationGridData : PopulationGridRow[] = []; 
        let _uniqueID : number = (new Date()).getTime();
        if(agreementPopulationArray && agreementPopulationArray.length > 0)
            agreementPopulationArray.forEach(it => {
                  _uniqueID = _uniqueID + 1 ;
                  let item : PopulationGridRow = {
                    name : (it.populationName && it.populationName.trim() != '') ? it.populationName : 'NO POPULATION', // WHEN BLANK NAME for NO POPULATION
                    desc: it.populationDescription,
                    id: it.agreementPopulationID,
                    applicationMatch: it.isApplicationMatchRequired,
                    applicationMatchString: it.isApplicationMatchRequired === true ? 'Yes' : 'No',
                    uniqueID : _uniqueID
                  }
                  console.log('population.uniqueID = ' + item.uniqueID);
                  populationGridData.push(item);
            });
        this.contractInfoComponent.populationGridData = populationGridData;
    }

    setRentalSubsidyArray(rentalSubsidyArray: AgreementRentalSubsidyMapping[]){
      let rentalSubsidiesData : any[] = [];

      if(rentalSubsidyArray && rentalSubsidyArray.length > 0){

          rentalSubsidyArray.forEach(it => {
                rentalSubsidiesData.push(it.rentalSubsidyType);
          });
      }
      this.contractInfoComponent.rentalSubsidiesData = rentalSubsidiesData;
    }

    setUnitTypeArray(unitTypeArray : AgreementPopulationUnitTypeMapping[]){
      let popUnitTypeGridData : PopulationUnitTypeGridRow[] = [];
      let that = this;
      if(unitTypeArray && unitTypeArray.length > 0){

        unitTypeArray.forEach(it => {
          let existItem = popUnitTypeGridData.find(it2 => it2.id == it.agreementPopulationID);
          if(existItem == undefined || existItem == null){
                let name1 = that.contractInfoComponent.populationGridData.find(X => X.id == it.agreementPopulationID) ? that.contractInfoComponent.populationGridData.find(X => X.id == it.agreementPopulationID).name : '';
                let unitTypeString1 = that.contractInfoComponent.unitTypes.find(i => i.id == it.unitType.toString()) ? that.contractInfoComponent.unitTypes.find(i => i.id == it.unitType.toString()).name : '';
                let _uniqueID = that.contractInfoComponent.populationGridData.find(X => X.id == it.agreementPopulationID) ? that.contractInfoComponent.populationGridData.find(X => X.id == it.agreementPopulationID).uniqueID : 0;
                let item : PopulationUnitTypeGridRow = {
                  name : name1 == ''  ? 'NO POPULATION' : name1,
                  id: it.agreementPopulationID,
                  unitTypes: [it.unitType.toString()],
                  unitTypesString: unitTypeString1,
                  uniqueID : _uniqueID
              }
              console.log('UNIT population.uniqueID = ' + item.uniqueID);
              popUnitTypeGridData.push(item); // new item
          } else {
            let newArray = popUnitTypeGridData.filter(it3 => it3.id != existItem.id);// removed exist
            existItem.unitTypes = existItem.unitTypes.concat([it.unitType.toString()]);
            existItem.unitTypesString =  existItem.unitTypes.reduce(function(acc, cur){
                                                    return acc + ', ' + that.contractInfoComponent.unitTypes.find(i => i.id == cur).name
                                                }, '');
            existItem.unitTypesString = existItem.unitTypesString && existItem.unitTypesString.startsWith(', ') ? existItem.unitTypesString.substring(2, existItem.unitTypesString.length) : existItem.unitTypesString;
            newArray.push(existItem); // exist item (but updated)
            popUnitTypeGridData = newArray; // reset
          }
        });
      }
      this.contractInfoComponent.popUnitTypesGridData = popUnitTypeGridData;
    }

    setPlacementAgencyArray(placementAgencyArray : AgreementPopulationPlacementAgency[]){
        let placementGridData : PlacementAgencyGridRow[] = [];
        let that = this;
        if(placementAgencyArray && placementAgencyArray.length > 0){

            placementAgencyArray.forEach(it => {
              let existItem = placementGridData.find(it2 => it2.id == it.agreementPopulationID);
              if(existItem == undefined || existItem == null){
                    let name1 = that.contractInfoComponent.populationGridData.find(X => X.id == it.agreementPopulationID) ? that.contractInfoComponent.populationGridData.find(X => X.id == it.agreementPopulationID).name : '';
                    let placeAgency1 = it.placementAgencyName ? it.placementAgencyName : '';
                    let placeAgencyString : PlacementAgency= that.additionalInfoComponent.placementAgencies.find(it3 => it3.placementAgencyName.toUpperCase() == placeAgency1.toUpperCase()) ? that.additionalInfoComponent.placementAgencies.find(it3 => it3.placementAgencyName.toUpperCase() == placeAgency1.toUpperCase()) : null;
                    let _uniqueID = that.contractInfoComponent.populationGridData.find(X => X.id == it.agreementPopulationID) ? that.contractInfoComponent.populationGridData.find(X => X.id == it.agreementPopulationID).uniqueID : 0;

                    let item : PlacementAgencyGridRow = {
                      name : name1 == ''  ? 'NO POPULATION' : name1,
                      id: it.agreementPopulationID,
                      uniqueID : _uniqueID,
                      primaryServiceContractName : that.primaryServiceContractName,
                      placementAgencies: that.additionalInfoComponent.placementAgencies.filter(it3 => it3.placementAgencyName.toUpperCase() == placeAgency1.toUpperCase()),
                      placementAgenciesString: placeAgencyString ? placeAgencyString.placementAgencyName : '',
                  }
                  placementGridData.push(item); // new item
              } else {
                let newArray = placementGridData.filter(it3 => it3.id != existItem.id);// removed exist
                existItem.placementAgencies = existItem.placementAgencies.concat(that.additionalInfoComponent.placementAgencies.find(it3 => it3.placementAgencyName.toUpperCase() == it.placementAgencyName.toUpperCase()));
                existItem.placementAgencies = existItem.placementAgencies.filter(it => (it !== undefined && it !== null));
                existItem.placementAgenciesString =  existItem.placementAgencies.reduce(function(acc, cur) {
                                                                              return acc + ', ' + cur.placementAgencyName
                                                                            }, '');
                existItem.placementAgenciesString = existItem.placementAgenciesString && existItem.placementAgenciesString.startsWith(', ') ? existItem.placementAgenciesString.substring(2) : existItem.placementAgenciesString; 
                newArray.push(existItem); // exist item (but updated)
                placementGridData = newArray; // reset
              }

            });
        }

        this.contractInfoComponent.placementGridData =  placementGridData;
        this.additionalInfoComponent.placementGridData = placementGridData;
        this.additionalInfoComponent.populationInputGridData = this.contractInfoComponent.populationGridData; // dropdown values
    }

    checkDuplicatePrimaryServiceContractName(){
      let psName : string = this.contractInfoComponent.contractInfoFormGroup.get('primaryServiceNameCtrl').value;
          this.agreeConfigSvcSub2 = this.agreementConfigService.checkPrimaryServiceNameExists(psName).subscribe(
            data => {
                if(data == true){
                    this.toastr.warning('Duplicate Name exists for Primary Service Contract', '');
                    this.formValid = false;
                }
                else{
                    this.toastr.info('No duplicate names exists for Primary Service Contract', '');
                    this.formValid = true;
                }
            },
            error => {
              this.toastr.warning('Error while checking Duplicate Primary Service Contract Name !!', '');
              console.error(error);
            }
        );
    }

    saveClicked(event : Event)
    {
      this.validateClicked(event);
      if(this.formValid){
        let formData: PrimaryServiceAgreementPopulationConfiguration = this.GetFormData();

          this.agreeConfigSvcSub1 = this.agreementConfigService.savePrimaryServiceAgreement(formData).subscribe(
              data => {
                  this.toastr.success('Successfully Saved !!', '');
                  this.router.navigate(['/admin/primary-service-agreement-population-config']);
              },
              error => {
                this.toastr.warning('Error while Saving !!', '');
                console.error(error);
              }
          );
      }
    }

    validateClicked(event : Event){

        this.markFormGroupTouched(this.addPrimaryServiceContractFormGroup);
        this.markFormGroupTouched(this.contractInfoComponent.contractInfoFormGroup);
        this.markFormGroupTouched(this.additionalInfoComponent.additionalInfoFormGroup);

        this.addPrimaryServiceContractFormGroup.updateValueAndValidity();
        this.contractInfoComponent.contractInfoFormGroup.updateValueAndValidity();
        this.additionalInfoComponent.additionalInfoFormGroup.updateValueAndValidity();

        if(this.validateForm()){
         // this.toastr.info('No validation errors found !!', '');
          if(this.editPrimaryServiceContractID === 0)
              this.checkDuplicatePrimaryServiceContractName(); // DUPLICATE NAME CHECK for NEW
          else
              this.formValid = true;
        } else{
          this.formValid = false;
        }

    }

    validateForm() : boolean {
      if(this.validatePrimaryServiceContract() && this.validateAgreementPopulation() 
              && this.validateRentalSubsidy() && this.validateUnitType() && this.validatePlacementAgency())
        return true;
      else
        return false;
    }

    validatePrimaryServiceContract() : boolean {
      let psName : string = this.contractInfoComponent.contractInfoFormGroup.get('primaryServiceNameCtrl').value;
      if(psName === undefined || psName === null || psName === "" || psName.trim().length == 0) {
          this.toastr.warning('Please enter Primary Service Contract Name.', '');
        return false;
      }

      let hasPop = this.contractInfoComponent.contractInfoFormGroup.get('includePopulationCtrl').value;
      if(hasPop === undefined || hasPop === null || hasPop === ""){
        this.toastr.warning('Please select field "include populations.', '');
        return false;
      }

      return true;
    }

    validateAgreementPopulation() : boolean {
      if(this.getAgreementPopulationArray().length == 0){
        this.toastr.warning('Please enter at least one Agreement population.', '');
        return false;
      }

      return true;
    }

    validateRentalSubsidy() : boolean{
      let rentalSubsidyArray: AgreementRentalSubsidyMapping[] = this.getRentalSubsidyArray();
      if(rentalSubsidyArray == undefined || rentalSubsidyArray == null || rentalSubsidyArray.length == 0){
        this.toastr.warning('Please enter at least one Rental Subsidy.', '');
        return false;
      }

      return true;
    }

    validateUnitType() : boolean{
      let unitTypeArray : AgreementPopulationUnitTypeMapping[] = this.getUnitTypeArray();
      if(unitTypeArray == undefined || unitTypeArray == null || unitTypeArray.length == 0){
        this.toastr.warning('Please select at least one Unit Type for an Agreement Population', '');
        return false;
      }

      return true;
    }

    validatePlacementAgency() : boolean{
      let placementAgencyArray: PlacementAgency[] = this.getPlacementAgencyArray();

      if(placementAgencyArray == undefined || placementAgencyArray == null || placementAgencyArray.length == 0){
        this.toastr.warning('Please select at least one Placement Agency for an Agreement Population', '');
        return false;
      }
      return true;
    }

    GetFormData(): PrimaryServiceAgreementPopulationConfiguration {
      const formData : PrimaryServiceAgreementPopulationConfiguration = {
          primaryServiceContract : null,
          agreementPopulationArray : null,
          unitTypeArray : null,
          rentalSubsidyArray : null,
          placementAgencyArray : null,
      }

      formData.primaryServiceContract = this.getPrimaryServiceContract(); // SET the value to property
      formData.agreementPopulationArray = this.getAgreementPopulationArray(); // SET the value to property
      formData.rentalSubsidyArray = this.getRentalSubsidyArray(); // SET the value to property
      formData.unitTypeArray = this.getUnitTypeArray(); // SET the value to property
      formData.placementAgencyArray = this.getPlacementAgencyArray(); // SET the value to property

      return formData;
    }

    getPrimaryServiceContract(): PrimaryServiceContract{
      let _val = this.contractInfoComponent.contractInfoFormGroup.get('primaryServiceNameCtrl').value;
      _val = _val ? _val.trim() : "";
      let primaryServiceContract : PrimaryServiceContract = {
        primaryServiceContractID: this.editPrimaryServiceContractID, 
        name: _val, // TRIM
        hasPopulation: (this.contractInfoComponent.includePopulationModel == 33) ? true : false,
        isActive: true,
        createdBy: this.userData.optionUserId,
        updatedBy: this.userData.optionUserId
      }
      return primaryServiceContract;
    }

    getAgreementPopulationArray() : AgreementPopulation[]{
      let agreementPopulationArray : AgreementPopulation[] = [];

      if(this.contractInfoComponent.includePopulationModel == 33){
        this.contractInfoComponent.populationGridData.forEach(it => {
                agreementPopulationArray.push({
                agreementPopulationID: it.id,
                primaryServiceContractID: this.editPrimaryServiceContractID,
                populationName: it.name,
                populationDescription: it.desc,
                isApplicationMatchRequired: (it.applicationMatch === true ? true : false),
                isActive: true,
                createdBy: this.userData.optionUserId,
                updatedBy: this.userData.optionUserId
              });
        });
      } 
      else if(this.contractInfoComponent.includePopulationModel == 34){
              this.contractInfoComponent.populationGridData.forEach(it => {
                agreementPopulationArray.push({
                agreementPopulationID: it.id,
                primaryServiceContractID: this.editPrimaryServiceContractID,
                populationName: '', // it.name, // BLANK instead of 'NO POPULATION'
                populationDescription: it.desc,
                isApplicationMatchRequired: (it.applicationMatch === true ? true : false),
                isActive: true,
                createdBy: this.userData.optionUserId,
                updatedBy: this.userData.optionUserId
              });
          });
      }
      return agreementPopulationArray;
    }

    getRentalSubsidyArray(): AgreementRentalSubsidyMapping[]{
      let rentalSubsidyArray : AgreementRentalSubsidyMapping[] = [];

      if(this.contractInfoComponent.rentalSubsidiesData && this.contractInfoComponent.rentalSubsidiesData.length > 0){
            this.contractInfoComponent.rentalSubsidiesData.forEach(it => {
              rentalSubsidyArray.push({
                  agreementRentalSubsidyMappingID: 0,
                  primaryServiceContractID: this.editPrimaryServiceContractID,

                  rentalSubsidyType: it,
                  rentalSubsidyTypeDESC: '',

                  isActive: true,

                  createdBy: this.userData.optionUserId,
                  updatedBy: this.userData.optionUserId,
              });
            });
      }

      return rentalSubsidyArray;
    }

    getUnitTypeArray(): AgreementPopulationUnitTypeMapping[]{
      let unitTypeArray : AgreementPopulationUnitTypeMapping[] = [];

      if(this.contractInfoComponent.popUnitTypesGridData && this.contractInfoComponent.popUnitTypesGridData.length > 0){
          this.contractInfoComponent.popUnitTypesGridData.forEach(it => {
              if(it.unitTypes != null && it.unitTypes.length > 0){

                it.unitTypes.forEach(row => {

                      unitTypeArray.push({
                        agreementPopulationUnitTypeMappingID: 0,
                        agreementPopulationID: it.id,
                        agreementPopulation: (this.contractInfoComponent.includePopulationModel == 34) ? '' : it.name, // BLANK instead of 'NO POPULATION'
                    
                        unitType: Number(row), // unit type
                        unitTypeDESC: null,
                    
                        isActive: true,
                    
                        createdBy: this.userData.optionUserId,
                        updatedBy: this.userData.optionUserId,
                    });
                });
              }
          });
      }

      return unitTypeArray;
    }

    getPlacementAgencyArray() : AgreementPopulationPlacementAgency[] {
      let placementAgencyArray : AgreementPopulationPlacementAgency[] = [];

        if(this.additionalInfoComponent.placementGridData && this.additionalInfoComponent.placementGridData.length > 0){
            
            this.additionalInfoComponent.placementGridData.forEach(it => {

                if(it.placementAgencies != null && it.placementAgencies.length > 0){

                    it.placementAgencies.forEach(placeAgny => {

                      placementAgencyArray.push({

                        agencyNo_SiteNo : placeAgny.agencyNo_SiteNo,
                        placementAgencyName: placeAgny.placementAgencyName,
                        agreementPopulationID: it.id,
                        agreementPopulationName: (this.contractInfoComponent.includePopulationModel == 34) ? '' : it.name, // BLANK instead of 'NO POPULATION'
                        id: 0,
                        createdBy: this.userData.optionUserId,
                        updatedBy: this.userData.optionUserId,
                        isActive: true,
                    });
                });
              }
            });

        }
      return placementAgencyArray;
    }

    get primaryServiceContractName() : string {
      let _val = this.contractInfoComponent.contractInfoFormGroup.get('primaryServiceNameCtrl').value;
      _val = _val ? _val.trim() : "";
        if(this.contractInfoComponent && this.contractInfoComponent.contractInfoFormGroup){
          AddPrimaryServiceContractComponent.psName = _val;
        }
        return AddPrimaryServiceContractComponent.psName;
    }

    clearClicked(event : Event){
      const title = 'Confirm to Clear';
      const primaryMessage = 'Are you sure you want to clear all the entries ?';
      const secondaryMessage = ''
      const confirmButtonName = 'Yes';
      const dismissButtonName = 'No';

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        (positiveResponse) => {
          //  this.setIncludePopulation(false); // because OK for NO population
            if(this.addPrimaryServiceContractFormGroup)  this.addPrimaryServiceContractFormGroup.reset();
            if(this.contractInfoComponent.contractInfoFormGroup)  this.contractInfoComponent.contractInfoFormGroup.reset();
            if(this.additionalInfoComponent.additionalInfoFormGroup)  this.additionalInfoComponent.additionalInfoFormGroup.reset();
        },
        (negativeResponse) => {
           // this.setIncludePopulation(true);  // because NO Population is confirmed
        },
      );


    }

    markFormGroupTouched(formGroup: FormGroup) {
      (<any>Object).values(formGroup.controls).forEach(control => {
        control.markAsTouched();

        if (control.controls) {
          this.markFormGroupTouched(control);
        }
      });
    }

    nextPage() {
      if (this.tabSelectedIndex < 1) {
        this.tabSelectedIndex = this.tabSelectedIndex + 1;
      } 
    }

    // Previous button click
    previousPage() {
      if (this.tabSelectedIndex != 0) {
        this.tabSelectedIndex = this.tabSelectedIndex - 1;
      } else {
          this.router.navigate(['/admin/primary-service-agreement-population-config']);
      }
    }

    tabChanged(tabChangeEvent: MatTabChangeEvent): void {

      this.tabSelectedIndex = tabChangeEvent.index;
      if(this.tabSelectedIndex == 0)
        this.btnLabel = "Exit";
      else if(this.tabSelectedIndex == 1)
        this.btnLabel = "Previous";
    }

    ngOnDestroy() {
        if(this.userDataSub)  this.userDataSub.unsubscribe();
        if(this.agreeConfigSvcSub1) this.agreeConfigSvcSub1.unsubscribe();
        if(this.agreeConfigSvcSub2) this.agreeConfigSvcSub2.unsubscribe();
        if(this.agreeConfigSvcSub3) this.agreeConfigSvcSub3.unsubscribe();
    }

    }