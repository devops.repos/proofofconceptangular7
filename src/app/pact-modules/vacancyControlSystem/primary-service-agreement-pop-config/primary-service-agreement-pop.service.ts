import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {PrimaryServiceAgreementPopulationConfiguration} from './primary-service-contract-agreement-population-model'

@Injectable({
  providedIn: 'root'
})
export class PrimaryServiceAgreementPopConfigService {

  getAllPlacementAgencyURL = environment.pactApiUrl + 'Agency/GetAllPlacementAgency';
  savePrimaryServiceAgreementURL = environment.pactApiUrl + 'AgreementConfiguration/SavePrimaryServiceAgreementPopulation';
  checkPrimaryServiceNameExistsURL = environment.pactApiUrl + 'AgreementConfiguration/CheckPrimaryServiceNameExists';
  getPrimaryServiceAgreementPopulationConfigurationURL = environment.pactApiUrl + 'AgreementConfiguration/GetPrimaryServiceAgreementPopulationConfiguration';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    response: 'json',
  };

  constructor(private http: HttpClient) {}

  getAllPlacementAgency() {
    return this.http.get(this.getAllPlacementAgencyURL, this.httpOptions);
  }

  savePrimaryServiceAgreement(psAgreementConfig:PrimaryServiceAgreementPopulationConfiguration) : Observable<any>{
    return this.http.post(this.savePrimaryServiceAgreementURL, JSON.stringify(psAgreementConfig), this.httpOptions);
  }

  checkPrimaryServiceNameExists(primaryServiceName : string) : Observable<any>{
    return this.http.post(this.checkPrimaryServiceNameExistsURL, JSON.stringify(primaryServiceName), this.httpOptions);
  }

  getPrimaryServiceAgreementPopulationConfiguration(primaryServiceContractID : number) : Observable<any>{
    return this.http.post(this.getPrimaryServiceAgreementPopulationConfigurationURL, JSON.stringify(primaryServiceContractID), this.httpOptions);
  }

}