
export interface PrimaryServiceContract {
    primaryServiceContractID: number;
    name: string;
    hasPopulation: boolean;
    isActive: boolean;

    createdBy: number;
    updatedBy: number;
}

export interface AgreementPopulation {
    agreementPopulationID: number;
    primaryServiceContractID: number;
    populationName: string;
    populationDescription: string;
    isApplicationMatchRequired: boolean;
    isActive: boolean;

    createdBy: number;
    updatedBy: number;
}

export interface AgreementPopulationUnitTypeMapping {
    agreementPopulationUnitTypeMappingID: number;
    agreementPopulationID: number;
    agreementPopulation: string;

    unitType: number;
    unitTypeDESC: string;

    isActive: boolean;

    createdBy: number;
    updatedBy: number;
}

export interface AgreementRentalSubsidyMapping {
        agreementRentalSubsidyMappingID: number;
        primaryServiceContractID: number;

        rentalSubsidyType: number;
        rentalSubsidyTypeDESC: string;

        isActive: boolean;

        createdBy: number;
        updatedBy: number;
}

export interface PrimaryServiceAgreementPopulationConfiguration {
    primaryServiceContract : PrimaryServiceContract;
    agreementPopulationArray : AgreementPopulation[];
    unitTypeArray : AgreementPopulationUnitTypeMapping[];
    rentalSubsidyArray : AgreementRentalSubsidyMapping[];
    placementAgencyArray : AgreementPopulationPlacementAgency[];
}

export interface AgreementPopulationPlacementAgency {
    agencyNo_SiteNo : string;
    placementAgencyName: string;

    agreementPopulationID: number;
    agreementPopulationName: string;
    
    id:number;
    isActive: boolean;

    createdBy: number;
    updatedBy: number;
}

export interface PlacementAgency {
    agencyNo_SiteNo : string;
    placementAgencyName: string;
   
    id:number;
    isActive: boolean;

    createdBy: number;
    updatedBy: number;
}

export interface PlacementAgencyGridRow {
    placementAgencies: PlacementAgency[];
    placementAgenciesString: string;
    primaryServiceContractName: string;
    name : string;
    id:number;
    uniqueID : number;
}

export interface PopulationGridRow {
    name : string;
    desc: string;
    id:number;
    applicationMatch:boolean;
    applicationMatchString: string;
    uniqueID : number;
}

export interface PopulationUnitTypeGridRow {
    name : string;
    id:number;
    unitTypes: string[];
    unitTypesString: string;
    uniqueID : number;
}

export interface UnitTypeSpecial {
    name : string;
    id:string;
    refGroupID: number;
    refGroupDescription: string;
    refGroupDetailID: number;
    refGroupDetailDescription: string;
    longDescription: string;
    parentRefGroupDetailID: number;
    sortOrder: number;
}
