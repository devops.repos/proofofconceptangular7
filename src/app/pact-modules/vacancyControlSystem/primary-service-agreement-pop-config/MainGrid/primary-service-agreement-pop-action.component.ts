import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IPrimaryServiceAgreementPopulationsRentalSubsidies } from '../MainGrid/primary-service-agreement-pop-interface.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: 'app-cap-action',
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="capAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #capAction="matMenu">
      <button mat-menu-item (click)="onEditAgreement()">Edit Agreement Profile</button>
    </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class PrimaryServiceAgreementConfigurationActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private capSelected: IPrimaryServiceAgreementPopulationsRentalSubsidies;

  constructor(private router: Router) {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onEditAgreement() {
    let rowSelected = this.params.data; 
    this.router.navigate(['./admin/edit-primary-service-contract/' + rowSelected.primaryServiceContractID.toString()]);
  }

  refresh(): boolean {
    return false;
  }
}
