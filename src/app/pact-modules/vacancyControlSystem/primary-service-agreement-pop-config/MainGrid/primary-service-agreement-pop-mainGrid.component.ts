import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { PrimaryServiceAgreementConfigurationActionComponent } from './primary-service-agreement-pop-action.component';
import { Subscription } from 'rxjs';
import { IPrimaryServiceAgreementPopulationsRentalSubsidies } from './primary-service-agreement-pop-interface.model';
import { CommonService } from 'src/app/services/helper-services/common.service';

@Component({
  selector: 'app-primary-service-agreement-population',
  templateUrl: './primary-service-agreement-pop-mainGrid.component.html',
  styleUrls: ['./primary-service-agreement-pop.component.scss']
})
export class PrimaryServiceAgreePopMainGridComponent implements OnInit, OnDestroy {

  numberOfCAP = 0;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  gridApi;
  gridColumnApi;

  columnDefs;
  defaultColDef;
  rowSelection;
  frameworkComponents;
  context;

  public gridOptions: GridOptions;
  rowData: IPrimaryServiceAgreementPopulationsRentalSubsidies[];

  rowDataSub: Subscription;

  constructor(
    private commonSvc: CommonService
  ) {
    this.gridOptions = {
      rowHeight: 35,
    } as GridOptions;
    this.columnDefs = [
      {
        headerName: 'Primary Service Contract ID',
        field: 'primaryServiceContractID',
        filter: 'agTextColumnFilter',
        width: 150,
        hide : true,
        suppressSizeToFit: true
      },
      {
        headerName: 'Primary Service Contract Type',
        field: 'primaryServiceContractName',
        filter: 'agTextColumnFilter',
        minWidth: 200,
        flex: 1
      },
      {
        headerName: 'Population',
        field: 'agreementPopulations',
        filter: 'agTextColumnFilter',
        minWidth: 300,
        flex: 2
      },
      {
        headerName: 'Rental Subsidies',
        field: 'rentalSubsidies',
        filter: 'agTextColumnFilter',
        minWidth: 300,
        flex: 2
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        cellRenderer: 'primarySvcActionComponent',
      }
    ];
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.rowSelection = 'single';
    this.context = { componentParent: this };
    this.frameworkComponents = {
    //  agDateInput: CustomDateComponent,
      primarySvcActionComponent: PrimaryServiceAgreementConfigurationActionComponent,
    };
  }

  ngOnInit() {

  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    /** API call to get the grid data */
    this.rowDataSub = this.commonSvc.getPrimaryServiceAgreementPopulationsRentalSubsidies().subscribe(
        res => {
          this.rowData = res.body as IPrimaryServiceAgreementPopulationsRentalSubsidies[];
          this.numberOfCAP = this.rowData.length;
        },
        error => console.error('Error!', error)
        );

    const allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(column => {
      if (column.colId !== 'action') {
        if (column.colId !== 'clientEligibleFor') {
          allColumnIds.push(column.colId);
        }
      }
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
   // this.gridColumnApi.sizeColumnsToFit(); 
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'PrimaryServiceContractAgreementPopulationRentalSusisdyList-' + date + '-' + time
      // sheetName: document.querySelector("#sheetName").value,
    };

    this.gridApi.exportDataAsExcel(params);
  }

  ngOnDestroy() {

    if (this.rowDataSub) {
      this.rowDataSub.unsubscribe();
    }
  }
}
