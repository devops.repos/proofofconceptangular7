export interface IPrimaryServiceAgreementPopulationsRentalSubsidies{
    
    primaryServiceContractID: number;
    primaryServiceContractName: string;
    agreementPopulations: string;
    rentalSubsidies: string;

    id: number;
    userId: number;
    userName: string;
}