import {BehaviorSubject} from 'rxjs';
import {Injectable} from '@angular/core';
import { PopulationGridRow } from './primary-service-contract-agreement-population-model';

@Injectable({
    providedIn: 'root'
  })

export class PopulationGridEventService{

    private actionResponseCompletedEvent = new BehaviorSubject<PopulationGridRow[]>(null);
   
     emitCompletedEvent(popGridData: PopulationGridRow[]){
        this.actionResponseCompletedEvent.next(popGridData)
     }
   
     subscribeEventListner(){
        return this.actionResponseCompletedEvent.asObservable();
      }
   
  }