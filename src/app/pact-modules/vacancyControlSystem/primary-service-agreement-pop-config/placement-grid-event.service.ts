import {BehaviorSubject} from 'rxjs';
import {Injectable} from '@angular/core';
import { PlacementAgencyGridRow } from './primary-service-contract-agreement-population-model';

@Injectable({
    providedIn: 'root'
  })

export class PlacementGridEventService{

    private actionResponseCompletedEvent = new BehaviorSubject<PlacementAgencyGridRow[]>(null);
   
     emitCompletedEvent(placementGridData: PlacementAgencyGridRow[]){
        this.actionResponseCompletedEvent.next(placementGridData)
     }
   
     subscribeEventListener(){
        return this.actionResponseCompletedEvent.asObservable();
     }
   
  }