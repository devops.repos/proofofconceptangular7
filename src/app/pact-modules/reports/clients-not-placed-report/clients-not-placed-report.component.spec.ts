import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsNotPlacedReportComponent } from './clients-not-placed-report.component';

describe('ClientsNotPlacedReportComponent', () => {
  let component: ClientsNotPlacedReportComponent;
  let fixture: ComponentFixture<ClientsNotPlacedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsNotPlacedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsNotPlacedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
