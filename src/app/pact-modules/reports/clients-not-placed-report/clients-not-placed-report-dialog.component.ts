import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { ClientsNotPlacedReportComponent } from './clients-not-placed-report.component';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: 'app-clients-not-placed-report-dialog',
    template: `
        <div class="cases-approved-level-one-report-dialog" fxLayoutGap="20px" fxLayout="column">
        <h2 mat-dialog-title>
          <mat-toolbar role="toolbar" class="task-header">
            <span class="fx-spacer"></span>
            <span><b>Clients Not Placed Report</b></span>
            <span class="fx-spacer"></span>
          </mat-toolbar>
        </h2>
        <mat-divider></mat-divider>
        <div mat-dialog-content>
          <app-clients-not-placed-report (dialogCloseEvent)="CloseDialog()"></app-clients-not-placed-report>
        </div>
        </div>
    `,
    styles: [
    `
      .rr-draft-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})
export class ClientsNotPlacedReportDialogComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  public cell: any;

  eventSvcSub: Subscription;

  constructor(
    @Inject(MAT_DIALOG_DATA) public reportData: any,
    private dialogRef: MatDialogRef<ClientsNotPlacedReportDialogComponent>
  ) {}

  agInit(params: any): void {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }

  // Close the dialog on close button
  CloseDialog() {
    this.dialogRef.close(true);
  }

  ngOnDestroy() {
    if (this.eventSvcSub) {
      this.eventSvcSub.unsubscribe();
    }

  }
}
