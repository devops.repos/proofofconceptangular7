import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { Router } from '@angular/router';
import { ToastrService } from "ngx-toastr";
import { CommonService } from 'src/app/services/helper-services/common.service';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { DatePipe } from '@angular/common';
import { ReportService } from './../reports.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

@Component({
  selector: 'app-clients-not-placed-report',
  templateUrl: './clients-not-placed-report.component.html',
  styleUrls: ['./clients-not-placed-report.component.scss'],
})
export class ClientsNotPlacedReportComponent implements OnInit, OnDestroy {

  userData: AuthData;
  userDataSub: Subscription;
  CAS: boolean = false;
  reportFilterGroup: FormGroup;
  from: string;
  to: string;
  minDate: Date;
  maxDate: Date;
  submitted : boolean = false;
  reportFormatList = [
    {
      formatID: 1,
      formatName: 'PDF'
    },
    {
      formatID: 2,
      formatName: 'EXCEL'
    },
  ]

  reportParameters: PACTReportParameters[] = [];
  reportUrlParams: PACTReportUrlParams;

  @Output() dialogCloseEvent = new EventEmitter();

  constructor(private formBuilder: FormBuilder,
    private confirmDialogService: ConfirmDialogService,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private userService: UserService,
    private reportService: ReportService ) {

    this.reportFilterGroup = this.formBuilder.group({
      reportFormat: ['', [CustomValidators.radioGroupRequired()]]
    });

    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDate();
    this.maxDate = new Date(currentYear, currentMonth, currentDay);
  }

  ngOnInit() {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        if (this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8)) {
          this.CAS = true;
        }

        if(!this.CAS && this.userData.agencyNo != '7777' && this.userData.agencyNo != '8888'){
          this.router.navigate(['/dashboard']);
        }
      }
    });
  }

  onSubmitClick() {
    this.submitted = true;
    if (this.validateForm()) {
      this.reportParameters = [
        { parameterName: 'reportName', parameterValue: 'CAPSReportsClientsNotPlaced', CreatedBy: this.userData.optionUserId }
      ];
       this.commonService.generateGUID(this.reportParameters)
       .subscribe(
         res => {
           if (res) {
             console.log(res);
             this.generateReport(res, "CAPSReportsClientsNotPlaced");
           }
         }
       );
      
    }
  }

  dialogCloseClicked(){
    this.dialogCloseEvent.emit();
  }
  
  //Generate Report
  generateReport(reportGuid: string, reportName: string) {
    if (this.reportFilterGroup.controls.reportFormat.value == "PDF") {
        this.reportUrlParams = { "reportParameterID": reportGuid, "reportName": reportName, "reportFormat": "PDF" }; 
        this.commonService.generateReport(this.reportUrlParams)
          .subscribe(
            res => {
              let data = new Blob([res.body], { type: 'application/pdf' });
              if (data.size > 512) {
                this.downloadFile(data, ".pdf");           
              }
            },
            error => {
              throw new error(error.message);
            }
          );
    } else  if (this.reportFilterGroup.controls.reportFormat.value == "EXCEL"){
        this.reportUrlParams = { "reportParameterID": reportGuid, "reportName": reportName, "reportFormat": "EXCEL" }; 
        this.commonService.generateReport(this.reportUrlParams)
          .subscribe(
            res => {
              let data = new Blob([res.body], { type: 'application/vnd.ms-excel' });
              if (data.size > 512) {
                this.downloadFile(data, ".xls");           
              }
            },
            error => {
              throw new error(error.message);
            }
          );
    }
  }

  downloadFile(data: Blob ,extension: string){
    const file = window.URL.createObjectURL(data);

    let link = document.createElement('a');
    link.href = file;
    link.download = 'Clients-Not-Placed-Report' + extension;
    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

    setTimeout(function () {
      // For Firefox it is necessary to delay revoking the ObjectURL
      window.URL.revokeObjectURL(file);
      link.remove();
    }, 100);
  }

  validateForm() {
 //   console.log("this.reportFilterGroup.get('reportFormat').value = " + this.reportFilterGroup.get('reportFormat').value);
    let formValid = true;
    if (!this.reportFilterGroup.valid ) {
      formValid = false;
      this.toastr.warning("All the fields are required");
    }
    return formValid;
  }

  onClearClick() {
    const title = 'Clear';
    const primaryMessage = `Are you sure you want to clear the filters?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        /* Clear the Form */
        this.reportFilterGroup.reset();
        this.submitted = false;
      },
      negativeResponse => { }
    );
  }


  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }
}
