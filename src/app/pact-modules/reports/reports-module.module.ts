import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentBannerModule } from '../../shared/content-banner/content-banner.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';

import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsDashboardComponent } from './reports-dashboard.component';
import { SummaryReportComponent } from './summary-report/summary-report.component';
import { AssessmentReportComponent } from './assessment-report/assessment-report.component';
import { ImpactedAdultReportComponent } from './impacted-adult-report/impacted-adult-report.component';
import { ImpactedAdultReportDialogComponent } from './impacted-adult-report/impacted-adult-report-dialog.component';
import { CasesApprovedLevelOneReportComponent } from './cases-approved-level-1-report/cases-approved-level-1-report.component';
import { CasesApprovedLevelOneReportDialogComponent } from './cases-approved-level-1-report/cases-approved-level-1-report-dialog.component';
import { ClientsNotPlacedReportComponent } from './clients-not-placed-report/clients-not-placed-report.component';
import { ClientsNotPlacedReportDialogComponent } from './clients-not-placed-report/clients-not-placed-report-dialog.component';
import { DeterminationbyWorkerReportDialogComponent } from './determination-by-worker-report/determination-by-worker-report-dialog.component';
import { DeterminationbyWorkerReportComponent } from './determination-by-worker-report/determination-by-worker-report.component';
import {OpenCasesReportComponent} from './open-cases-report/open-cases-report.component';
import {OpenCasesReportDialogComponent} from './open-cases-report/open-cases-report-dialog.component';

@NgModule({
  declarations: [
    ReportsDashboardComponent,
    SummaryReportComponent,
    AssessmentReportComponent,
    ImpactedAdultReportComponent, ImpactedAdultReportDialogComponent,
    CasesApprovedLevelOneReportComponent, CasesApprovedLevelOneReportDialogComponent,
    CasesApprovedLevelOneReportComponent,
    ClientsNotPlacedReportComponent,ClientsNotPlacedReportDialogComponent,
    OpenCasesReportComponent, OpenCasesReportDialogComponent,
    DeterminationbyWorkerReportComponent,DeterminationbyWorkerReportDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReportsRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    ContentBannerModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
  ],
  entryComponents: [
    ImpactedAdultReportDialogComponent,CasesApprovedLevelOneReportDialogComponent, ClientsNotPlacedReportDialogComponent, DeterminationbyWorkerReportDialogComponent,
    OpenCasesReportDialogComponent

  ],
})
export class ReportsModule { }
