import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeterminationbyWorkerReportComponent } from './determination-by-worker-report.component';

describe('DeterminationByWorkerReportComponent', () => {
  let component: DeterminationbyWorkerReportComponent;
  let fixture: ComponentFixture<DeterminationbyWorkerReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeterminationbyWorkerReportComponent ]
    })
    .compileComponents();
    
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeterminationbyWorkerReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
