import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { Router } from '@angular/router';
import { ToastrService } from "ngx-toastr";
import { CommonService } from 'src/app/services/helper-services/common.service';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { DatePipe } from '@angular/common';
import { ReportService } from './../reports.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

//Models
import { CasReviewerDetails } from '../determination-by-worker-report/determination-by-worker-report.model';
import { iReviewers } from '../determination-by-worker-report/determination-by-worker-report.model';

@Component({
  selector: 'app-determination-by-worker-report',
  templateUrl: './determination-by-worker-report.component.html',
  styleUrls: ['./determination-by-worker-report.component.scss'],
  providers: [DatePipe]
})
export class DeterminationbyWorkerReportComponent implements OnInit {

  userData: AuthData;
  userDataSub: Subscription;
  CAS: boolean = false;
  reportFilterGroup: FormGroup;
  from: string;
  to: string;
  minDate: Date;
  maxDate: Date;
  submitted : boolean = false;
  userIdList: string;
  reviewer: number;
  reviewerList = new Array();
  reportFormatList = [
    {
      formatID: 1,
      formatName: 'PDF'
    },
    {
      formatID: 2,
      formatName: 'EXCEL'
    },
  ]
  userList: CasReviewerDetails[];
  reportParameters: PACTReportParameters[] = [];
  reportUrlParams: PACTReportUrlParams;

  @Output() dialogCloseEvent = new EventEmitter();

  constructor(private formBuilder: FormBuilder,
    private confirmDialogService: ConfirmDialogService,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private userService: UserService,
    private datePipe: DatePipe,
    private reportService: ReportService ) {

    this.reportFilterGroup = this.formBuilder.group({
      referralDateFrom: ['', [Validators.required]],
      referralDateTo: ['', [Validators.required]],
      reportFormat: ['', [CustomValidators.radioGroupRequired()]],
      reviewerListCtrl: ['', [Validators.required]]
    });
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDate();
    this.maxDate = new Date(currentYear, currentMonth, currentDay);
  }

  ngOnInit() {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        if (this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8)) {
          this.CAS = true;
        }

        if(!this.CAS && this.userData.agencyNo != '7777' && this.userData.agencyNo != '8888'){
          this.router.navigate(['/dashboard']);
        }

         //Get Workers From CAS Reviewer List
    this.commonService.getCasReviewer()
      .subscribe(res => {
          const data = res as CasReviewerDetails[];
          this.userList = data;
          this.userList.push({id: 0 , name:'ALL'});
          this.userList.sort((a, b) => a.id - b.id);
          
        },
        error => {
          throw new Error(error.message);
        }
      );
      }
    });
  }

  onSubmitClick() {
    this.submitted = true;
    if (this.validateForm()) {
      this.from = this.datePipe.transform(this.reportFilterGroup.controls.referralDateFrom.value, 'MM/dd/yyyy');
      this.to = this.datePipe.transform(this.reportFilterGroup.controls.referralDateTo.value, 'MM/dd/yyyy');

      this.commonService.getCasReviewer()
      .subscribe(res => {
        if(res){
          const data = res as iReviewers[];
          if (data && data.length > 0) {
            data.forEach((element) => {
              if (element.reviewers) {
                this.reviewerList.push(element.reviewers);
              }
            });
            this.reportFilterGroup.controls['reviewerListCtrl'].setValue(this.reviewerList);

          }
        }
        },
        error => {
          throw new Error(error.message);
        }
      );


      if (this.reportFilterGroup.get('reviewerListCtrl').value && this.reportFilterGroup.get('reviewerListCtrl').value.length > 0) {
        this.userIdList = this.reportFilterGroup.get('reviewerListCtrl').value.join(',');
      }
      else {
        this.userIdList = null;
      }
      this.reportParameters = [
        { parameterName: 'referralFrom', parameterValue: this.from, CreatedBy: this.userData.optionUserId },
        { parameterName: 'referralTo', parameterValue: this.to, CreatedBy: this.userData.optionUserId },
        { parameterName: 'userIdList', parameterValue: this.userIdList, CreatedBy: this.userData.optionUserId },
        { parameterName: 'reportName', parameterValue: 'DeterminationByWorkerReport', CreatedBy: this.userData.optionUserId }
      ];
       this.commonService.generateGUID(this.reportParameters)
       .subscribe(
         res => {
           if (res) {
             console.log(res);
             this.generateReport(res, "DeterminationByWorkerReport");
           }
         }
       );
    }
  }

  dialogCloseClicked(){
    this.dialogCloseEvent.emit();
  }
  
  //Generate Report
  generateReport(reportGuid: string, reportName: string) {
    if (this.reportFilterGroup.controls.reportFormat.value == "PDF") {
        this.reportUrlParams = { "reportParameterID": reportGuid, "reportName": reportName, "reportFormat": "PDF" }; 
        this.commonService.generateReport(this.reportUrlParams)
          .subscribe(
            res => {
              let data = new Blob([res.body], { type: 'application/pdf' });
              if (data.size > 512) {
                this.downloadFile(data, ".pdf");           
              }
            },
            error => {
              throw new error(error.message);
            }
          );
    } else  if (this.reportFilterGroup.controls.reportFormat.value == "EXCEL"){
        this.reportUrlParams = { "reportParameterID": reportGuid, "reportName": reportName, "reportFormat": "EXCEL" }; 
        this.commonService.generateReport(this.reportUrlParams)
          .subscribe(
            res => {
              let data = new Blob([res.body], { type: 'application/vnd.ms-excel' });
              if (data.size > 512) {
                this.downloadFile(data, ".xls");           
              }
            },
            error => {
              throw new error(error.message);
            }
          );
    }
  }

  downloadFile(data: Blob ,extension: string){
    const file = window.URL.createObjectURL(data);

    let link = document.createElement('a');
    link.href = file;
    link.download = 'Determination-By-Worker-Report-' + this.from + '-To-' + this.to + extension;
    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

    setTimeout(function () {
      // For Firefox it is necessary to delay revoking the ObjectURL
      window.URL.revokeObjectURL(file);
      link.remove();
    }, 100);
  }

  validateForm() {
 //   console.log("this.reportFilterGroup.get('reportFormat').value = " + this.reportFilterGroup.get('reportFormat').value);
    let formValid = true;
    if (!this.reportFilterGroup.valid ) {
      formValid = false;
      this.toastr.warning("All the fields are required");
    }
    else {
      if (this.reportFilterGroup.controls.referralDateFrom.value > this.reportFilterGroup.controls.referralDateTo.value) {
        formValid = false;
        this.toastr.warning("From date cannot be greater than To date");
      }
    }

    return formValid;
  }

  onClearClick() {
    const title = 'Clear';
    const primaryMessage = `Are you sure you want to clear the filters?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        /* Clear the Form */
        this.reportFilterGroup.reset();
        while (this.reviewerList.length !== 0) {
          this.reviewerList.splice(0);
        }
        this.reportFilterGroup.get('reviewerListCtrl').reset();
        this.submitted = false;
      },
      negativeResponse => { }
    );
  }


  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }
}
