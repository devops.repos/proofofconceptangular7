import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from "ngx-toastr";
import { CommonService } from 'src/app/services/helper-services/common.service';
import { PACTReportParameters } from 'src/app/models/pactReportParameters.model';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ReportService } from './reports.service';
import { UserAgencyType, UserSiteType, UserRole, appStatusColor } from '../../models/pact-enums.enum';
import { MatDialog } from '@angular/material';
import { ImpactedAdultReportDialogComponent } from './impacted-adult-report/impacted-adult-report-dialog.component';
import { CasesApprovedLevelOneReportDialogComponent } from './cases-approved-level-1-report/cases-approved-level-1-report-dialog.component';
import { ClientsNotPlacedReportDialogComponent } from './clients-not-placed-report/clients-not-placed-report-dialog.component';
import { DeterminationbyWorkerReportDialogComponent } from './determination-by-worker-report/determination-by-worker-report-dialog.component';
import {OpenCasesReportDialogComponent} from './open-cases-report/open-cases-report-dialog.component';

@Component({
  selector: 'app-reports-dashboard',
  templateUrl: './reports-dashboard.component.html',
  styleUrls: ['./reports-dashboard.component.scss']
})
export class ReportsDashboardComponent implements OnInit, OnDestroy {

  userData: AuthData;
  userDataSub: Subscription;
  is_CAS: boolean = false;
  agencyNo7777: boolean = false;
  agencyNo8888: boolean = false;

  reportParameters: PACTReportParameters[] = [];
  reportParams: PACTReportParameters;

  constructor(
    private router: Router, private dialog: MatDialog,
    private toastr: ToastrService,
    private commonService: CommonService,
    private userService: UserService,
    private reportService: ReportService ) {

  }

  ngOnInit() {
    let that = this;

      this.userDataSub = this.userService.getUserData().subscribe(res => {
          if (res) {
            that.userData = res;
            if(that.userData){
                if (that.commonService._doesValueExistInJson(that.userData.siteCategoryType, UserSiteType.CAS)) {
                  that.is_CAS = true;
                }
                if(that.userData.agencyNo == '7777'){
                  that.agencyNo7777 = true;
                }
                if(that.userData.agencyNo == '8888'){
                  that.agencyNo8888 = true;
                }
            }


          }
      });
  }

  onReportSelectedImpactAdultHome(){
    // routerLink="/reports/impacted-adult-report"
    this.dialog.open(ImpactedAdultReportDialogComponent, {
      width: '700px',
      maxHeight: '770px',
      disableClose: false,
      autoFocus: false,
      data: { "testParam": "testValue",  }
    });
  }

  onReportSelectedCasesApproveLevelOne(){
    // routerLink="/reports/cases-approved-level-one-report"
    this.dialog.open(CasesApprovedLevelOneReportDialogComponent, {
      width: '650px',
      maxHeight: '770px',
      disableClose: false,
      autoFocus: false,
      data: { "testParam": "testValue",  }
    });
  }

  onReportSelectedClientsNotPlaced(){
    // routerLink="/reports/clients-not-placed-report"
    this.dialog.open(ClientsNotPlacedReportDialogComponent, {
      width: '500px',
      maxHeight: '770px',
      disableClose: false,
      autoFocus: false,
      data: { "testParam": "testValue",  }
    });
  }

  onReportSelectedDeterminationbyWorker(){
    // routerLink="/reports/determination-by-worker-report"
    this.dialog.open(DeterminationbyWorkerReportDialogComponent, {
      width: '700px',
      maxHeight: '770px',
      disableClose: false,
      autoFocus: false,
      data: { "testParam": "testValue",  }
    });
  }

  onReportSelectedOpenCases(){
        // routerLink="/reports/clients-not-placed-report"
        this.dialog.open(OpenCasesReportDialogComponent, {
          width: '500px', 
          maxHeight: '770px',
          disableClose: false,
          autoFocus: false,
          data: { "testParam": "testValue",  }
        });
  }

  onExitClick() {
    this.router.navigate(['/dashboard']);
  }

  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

}
