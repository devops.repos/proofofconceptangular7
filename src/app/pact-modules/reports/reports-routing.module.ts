import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PactLandingPageComponent } from 'src/app/core/pact-landing-page/pact-landing-page.component';
import { SummaryReportComponent } from './summary-report/summary-report.component';
import { AssessmentReportComponent } from './assessment-report/assessment-report.component';
// import { NotificationResolver } from 'src/app/services/resolvers/notification.resolver';
import { ImpactedAdultReportComponent }  from './impacted-adult-report/impacted-adult-report.component';
import { CasesApprovedLevelOneReportComponent }  from './cases-approved-level-1-report/cases-approved-level-1-report.component';
import { ReportsDashboardComponent }  from './reports-dashboard.component';
import { SiteTypeGuard } from 'src/app/models/pact-enums.enum';
import { SiteTypeMasterAuthGuard } from 'src/app/services/auth/site-type-auth-guards/site-type-master-auth.guard';

const reportsRoutes: Routes = [
  {
    path: '',
    component: PactLandingPageComponent,
    // resolve: { notifications: NotificationResolver},
    children: [
      {
        path: 'reports',
          children: [
            { path: 'reports-dashboard', 
              component: ReportsDashboardComponent, 
              canActivate: [SiteTypeMasterAuthGuard],
              data: {
                guards: [
                  SiteTypeGuard.CAS, SiteTypeGuard.SH_PE, SiteTypeGuard.SH_RA
                ]
              }
            },
            { path: 'summary-report', component: SummaryReportComponent },
            { path: 'assessment-report', component: AssessmentReportComponent },
            { path: 'impacted-adult-report', 
              component: ImpactedAdultReportComponent, 
              canActivate: [SiteTypeMasterAuthGuard],
              data: {
                guards: [
                  SiteTypeGuard.CAS, SiteTypeGuard.SH_PE, SiteTypeGuard.SH_RA
                ]
              }
            },
            { path: 'cases-approved-level-one-report', 
              component: CasesApprovedLevelOneReportComponent, 
              canActivate: [SiteTypeMasterAuthGuard],
              data: {
                guards: [
                  SiteTypeGuard.CAS,
                ]
              }
            },
         
        ]
      },
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(reportsRoutes)],
  exports: [RouterModule],
  // providers: [NotificationResolver]
})
export class ReportsRoutingModule { }
