import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http'
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
  })

  export class ReportService {

    private impactedAdultsCSVReportUrl =  environment.pactApiUrl + 'Report/GenerateImpactedAdultsCVSReport';
    private casesApprovedLevelOneCVSReportUrl =  environment.pactApiUrl + 'Report/GenerateCasesApprovedLevelOneCSVReport';

    httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        response: 'json',
      };
    
    constructor(private httpClient: HttpClient) { }

    generateImpactedAdultsCVSReport(reportGuid: string): Observable<any> { 
        return this.httpClient.request(new HttpRequest(
        'GET',
        `${this.impactedAdultsCSVReportUrl}?reportParameterID=${reportGuid}`,
        null,
        {
        reportProgress: true,
        responseType: 'blob'
        }));}

    generateCasesApprovedLevelOneCSVReport(reportGuid: string): Observable<any> { 
      return this.httpClient.request(new HttpRequest(
      'GET',
      `${this.casesApprovedLevelOneCVSReportUrl}?reportParameterID=${reportGuid}`,
      null,
      {
      reportProgress: true,
      responseType: 'blob'
      }));}

  }