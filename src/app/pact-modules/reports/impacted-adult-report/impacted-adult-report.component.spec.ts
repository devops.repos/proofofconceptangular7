import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpactedAdultReportComponent } from './impacted-adult-report.component';

describe('ImpactedAdultReportComponent', () => {
  let component: ImpactedAdultReportComponent;
  let fixture: ComponentFixture<ImpactedAdultReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpactedAdultReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpactedAdultReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
