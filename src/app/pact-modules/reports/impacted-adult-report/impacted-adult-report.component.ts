import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { Router } from '@angular/router';
import { ToastrService } from "ngx-toastr";
import { CommonService } from 'src/app/services/helper-services/common.service';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { DatePipe } from '@angular/common';
import { ReportService } from './../reports.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

@Component({
  selector: 'app-impacted-adult-report',
  templateUrl: './impacted-adult-report.component.html',
  styleUrls: ['./impacted-adult-report.component.scss'],
  providers: [DatePipe]
})
export class ImpactedAdultReportComponent implements OnInit, OnDestroy {

  userData: AuthData;
  userDataSub: Subscription;
  CAS: boolean = false;
  reportFilterGroup: FormGroup;
  from: string;
  to: string;
  minDate: Date;
  maxDate: Date;
  submitted : boolean = false;
  reportFormatList = [
    {
      formatID: 1,
      formatName: 'PDF'
    },
    {
      formatID: 2,
      formatName: 'TXT'
    },
  ]

  siteTypeList = [
    {
      siteTypeID: 1,
      siteTypeName: 'All'
    },
    {
      siteTypeID: 2,
      siteTypeName: 'IAH Supportive Housing Provider'
    },
  ]

  reportParameters: PACTReportParameters[] = [];
  reportParams: PACTReportUrlParams;

  @Output() dialogCloseEvent = new EventEmitter();

  constructor(private formBuilder: FormBuilder,
    private confirmDialogService: ConfirmDialogService,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private userService: UserService,
    private datePipe: DatePipe,
    private reportService: ReportService ) {

    this.reportFilterGroup = this.formBuilder.group({
      referralDateFrom: ['', [Validators.required]],
      referralDateTo: ['', [Validators.required]],
      siteType: [0, [CustomValidators.dropdownRequired]],
      reportFormat: ['', [CustomValidators.radioGroupRequired()]]
    });
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDate();
    this.maxDate = new Date(currentYear, currentMonth, currentDay);
  }

  ngOnInit() {
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        if (this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8)) {
          this.CAS = true;
        }

        if(!this.CAS && this.userData.agencyNo != '7777' && this.userData.agencyNo != '8888'){
          this.router.navigate(['/dashboard']);
        }
      }
    });
  }

  onSubmitClick() {
    this.submitted = true;
    if (this.validateForm()) {
      this.from = this.datePipe.transform(this.reportFilterGroup.controls.referralDateFrom.value, 'MM/dd/yyyy');
      this.to = this.datePipe.transform(this.reportFilterGroup.controls.referralDateTo.value, 'MM/dd/yyyy');
      this.reportParameters = [
        { parameterName: 'referralFrom', parameterValue: this.from, CreatedBy: this.userData.optionUserId },
        { parameterName: 'referralTo', parameterValue: this.to, CreatedBy: this.userData.optionUserId },
        { parameterName: 'reportName', parameterValue: 'ImpactedAdults', CreatedBy: this.userData.optionUserId }
      ];
       this.commonService.generateGUID(this.reportParameters)
       .subscribe(
         res => {
           if (res) {
             console.log(res);
             this.generateReport(res, "ImpactedAdults");
           }
         }
       );
      //this.generateReport('33c7b495-eba5-4f35-8c86-5750071c1697', "ImpactedAdults");
    }
  }

  dialogCloseClicked(){
    this.dialogCloseEvent.emit();
  }
  
  //Generate Report
  generateReport(reportGuid: string, reportName: string) {
    if (this.reportFilterGroup.controls.reportFormat.value == 1) {
      this.reportParams = { reportParameterID: reportGuid, reportName: reportName, "reportFormat": "PDF" }; 
      this.commonService.generateReport(this.reportParams)
        .subscribe(
          res => {
            let data = new Blob([res.body], { type: 'application/pdf' });
            if (data.size > 512) {
              this.downloadFile(data, ".pdf");           
            }
          },
          error => {
            throw new error(error.message);
          }
        );
    } else  if (this.reportFilterGroup.controls.reportFormat.value == 2){
      this.reportService.generateImpactedAdultsCVSReport(reportGuid).subscribe(res => {
        let data = new Blob([res.body], { type: 'application/text' });
        if (data.size > 280) {
        this.downloadFile(data, ".txt");
        }
      });
    }
  }

  downloadFile(data: Blob ,extension: string){
    const file = window.URL.createObjectURL(data);

    let link = document.createElement('a');
    link.href = file;
    link.download = 'Impacted Adults Home Application Listing-' + this.from + '-' + this.to + extension;
    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

    setTimeout(function () {
      // For Firefox it is necessary to delay revoking the ObjectURL
      window.URL.revokeObjectURL(file);
      link.remove();
    }, 100);
  }

  validateForm() {
    let formValid = true;
    if (!this.reportFilterGroup.valid || this.reportFilterGroup.controls.siteType.value == 0) {
      formValid = false;
      this.toastr.warning("All the fields are required");
    }
    else {
      if (this.reportFilterGroup.controls.referralDateFrom.value > this.reportFilterGroup.controls.referralDateTo.value) {
        formValid = false;
        this.toastr.warning("From date cannot be greater than To date");
      }
    }

    return formValid;
  }

  onClearClick() {
    const title = 'Verify';
    const primaryMessage = `Are you sure you want to clear the filters?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'OK';
    const dismissButtonName = 'Cancel';
    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      positiveResponse => {
        /* Clear the Form */
        this.reportFilterGroup.reset();
        this.reportFilterGroup.controls.siteType.setValue(0);
        this.submitted = false;
      },
      negativeResponse => { }
    );
  }

  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }
}
