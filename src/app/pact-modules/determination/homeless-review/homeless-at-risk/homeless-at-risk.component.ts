import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { MatRadioChange } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import {
  HomelessAtRisk,
  iHomelessAtRiskInput,
  SaveHomelessAtRisk,
  iHeadOfHouseholdInput,
  HeadOfHousehold
} from '../homeless-review.model';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { HomelesReviewService } from '../homeless-review.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

@Component({
  selector: 'app-homeless-at-risk',
  templateUrl: './homeless-at-risk.component.html',
  styleUrls: ['./homeless-at-risk.component.scss']
})

export class HomelessAtRiskComponent implements OnInit, OnDestroy {
  @Output() nextHomelessAtRiskTab = new EventEmitter();
  @Output() previousHomelessAtRiskTab = new EventEmitter();

  //Global Variables
  clientCategory: number = 0;
  isHeadOfHouseHoldEnabled: boolean = false;
  isExplainRequired: boolean = false;

  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  homelessAtRiskFormGroup: FormGroup;

  homelessAtRisk = new HomelessAtRisk();
  homelessAtRiskInput: iHomelessAtRiskInput = {
    pactApplicationId: null,
    clientCategory: null,
    optionUserId: null
  }
  saveHomelessAtRisk = new SaveHomelessAtRisk();

  headOfHousehold = new HeadOfHousehold();
  headOfHouseholdInput: iHeadOfHouseholdInput = {
    pactApplicationId: null,
    optionUserId: null
  }

  haveLimitedEducationDescription: string;
  haveLimitedEmpHistoryDescription: string;
  yaWithChildrenDescription: string;
  isVictimOfDVInPast24MonthsDescription: string;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private homelessReviewService: HomelesReviewService,
    private message: ToastrService,
    private determinationSideNavService: DeterminationSideNavService) {

    //Homeless At Risk Form Group
    this.homelessAtRiskFormGroup = this.formBuilder.group({
      hasFrequentMovesPast1YearCtrl: ['', Validators.required],
      haveLimitedEducationCtrl: ['', Validators.required],
      haveLimitedEmploymentHistoryCtrl: ['', Validators.required],
      youngAdultWithChildrenCtrl: ['', Validators.required],
      shelterStayInPast24MonthsCtrl: ['', Validators.required],
      isVictimOfDVInPast24MonthsCtrl: ['', Validators.required],
      otherCriteriaForHomelessCtrl: ['', Validators.required],
      explainCtrl: ['']
    });
  }

  ngOnInit() {
    //Diable Explain Control
    this.homelessAtRiskFormGroup.get("explainCtrl").disable();

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData()
      .subscribe(res => {
        if (res) {
          const data = res as iApplicationDeterminationData;
          if (data) {
            this.applicationDeterminationData = data;
            if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType) {
              this.clientCategory = this.applicationDeterminationData.clientCategoryType;
              this.homelessAtRiskInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
              this.homelessAtRiskInput.clientCategory = this.applicationDeterminationData.clientCategoryType;
              this.homelessAtRiskInput.optionUserId = this.userData.optionUserId;
              this.homelessReviewService.getHomelessAtRisk(this.homelessAtRiskInput)
                .subscribe(res => {
                  if (res) {
                    const data = res as HomelessAtRisk;
                    if (data) {
                      this.populateHomlessAtRisk(data);
                    }
                  }
                });
            }
          }
        }
      });
  }

  //Destroy 
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //On Has Frequent Moves Past 1 Year Change
  onHasFrequentMovesPast1YearChange($event: MatRadioChange) {
    if ($event.value === "Y") {
      this.isHeadOfHouseHoldEnabled = true;
      this.homelessAtRisk.hasFrequentMovesPast1YearSource = 680;
      if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
        this.headOfHouseholdInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
        this.headOfHouseholdInput.optionUserId = this.userData.optionUserId;
        this.homelessReviewService.getHeadOfHousehold(this.headOfHouseholdInput)
          .subscribe(res => {
            if (res) {
              const data = res as HeadOfHousehold;
              if (data) {
                this.populateHeadOfHousehold(data);
              }
            }
          });
      }
    }
    else if ($event.value === "N") {
      this.isHeadOfHouseHoldEnabled = false;
      this.homelessAtRiskFormGroup.get("haveLimitedEducationCtrl").reset();
      this.homelessAtRiskFormGroup.get("haveLimitedEmploymentHistoryCtrl").reset();
      this.homelessAtRiskFormGroup.get("youngAdultWithChildrenCtrl").reset();
      this.homelessAtRiskFormGroup.get("shelterStayInPast24MonthsCtrl").reset();
      this.homelessAtRiskFormGroup.get("isVictimOfDVInPast24MonthsCtrl").reset();
      this.homelessAtRiskFormGroup.get("otherCriteriaForHomelessCtrl").reset();
      this.homelessAtRiskFormGroup.get("explainCtrl").setValue(null);
      this.homelessAtRiskFormGroup.get("explainCtrl").disable();
      this.homelessAtRisk.hasFrequentMovesPast1YearSource = 680;
      this.homelessAtRisk.haveLimitedEducationSource = null;
      this.homelessAtRisk.haveLimitedEmpHistorySource = null;
      this.homelessAtRisk.yaWithChildrenSource = null;
      this.homelessAtRisk.shelterStayInPast24MonthsSource = null;
      this.homelessAtRisk.isVictimOfDVInPast24MonthsSource = null;
      this.homelessAtRisk.otherCriteriaForHomelessSource = null;
      this.haveLimitedEducationDescription = null;
      this.haveLimitedEmpHistoryDescription = null;
      this.yaWithChildrenDescription = null;
      this.isVictimOfDVInPast24MonthsDescription = null;
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //Populate Head Of Household
  populateHeadOfHousehold(headOfHousehold: HeadOfHousehold) {
    if (headOfHousehold) {
      this.headOfHousehold = headOfHousehold;
      //Have Limited Education
      if (headOfHousehold.haveLimitedEducationType === 33) {
        this.homelessAtRiskFormGroup.controls['haveLimitedEducationCtrl'].setValue('Y');
      }
      else if (headOfHousehold.haveLimitedEducationType === 34) {
        this.homelessAtRiskFormGroup.controls['haveLimitedEducationCtrl'].setValue('N');
      }
      if (headOfHousehold.haveLimitedEducationDescription) {
        this.haveLimitedEducationDescription = "(" + headOfHousehold.haveLimitedEducationDescription + ")";
      }
      this.homelessAtRisk.haveLimitedEducationSource = this.headOfHousehold.haveLimitedEducationSource;
      this.homelessAtRisk.haveLimitedEducationOrigType = this.headOfHousehold.haveLimitedEducationOrigType;
      //Have Limited Employment History
      if (headOfHousehold.haveLimitedEmpHistoryType === 33) {
        this.homelessAtRiskFormGroup.controls['haveLimitedEmploymentHistoryCtrl'].setValue('Y');
      }
      else if (headOfHousehold.haveLimitedEmpHistoryType === 34) {
        this.homelessAtRiskFormGroup.controls['haveLimitedEmploymentHistoryCtrl'].setValue('N');
      }
      if (headOfHousehold.haveLimitedEmpHistoryDescription) {
        this.haveLimitedEmpHistoryDescription = "(" + headOfHousehold.haveLimitedEmpHistoryDescription + ")";
      }
      this.homelessAtRisk.haveLimitedEmpHistorySource = this.headOfHousehold.haveLimitedEmpHistorySource;
      this.homelessAtRisk.haveLimitedEmpHistoryOrigType = this.headOfHousehold.haveLimitedEmpHistoryOrigType;
      //Young Adult With Children
      if (headOfHousehold.yaWithChildrenType === 33) {
        this.homelessAtRiskFormGroup.controls['youngAdultWithChildrenCtrl'].setValue('Y');
      }
      else if (headOfHousehold.yaWithChildrenType === 34) {
        this.homelessAtRiskFormGroup.controls['youngAdultWithChildrenCtrl'].setValue('N');
      }
      if (headOfHousehold.yaWithChildrenDescription) {
        this.yaWithChildrenDescription = "(" + headOfHousehold.yaWithChildrenDescription + ")";
      }
      this.homelessAtRisk.yaWithChildrenSource = this.headOfHousehold.yaWithChildrenSource;
      this.homelessAtRisk.yaWithChildrenOrigType = this.headOfHousehold.yaWithChildrenOrigType;
      //Shelter Stay In Past 24 Months
      if (headOfHousehold.shelterStayInPast24MonthsType === 33) {
        this.homelessAtRiskFormGroup.controls['shelterStayInPast24MonthsCtrl'].setValue('Y');
      }
      else if (headOfHousehold.shelterStayInPast24MonthsType === 34) {
        this.homelessAtRiskFormGroup.controls['shelterStayInPast24MonthsCtrl'].setValue('N');
      }
      this.homelessAtRisk.shelterStayInPast24MonthsSource = this.headOfHousehold.shelterStayInPast24MonthsSource;
      this.homelessAtRisk.shelterStayInPast24MonthsOrigType = this.headOfHousehold.shelterStayInPast24MonthsOrigType;
      //Is Victim Of DV In Past 24 Months
      if (headOfHousehold.isVictimOfDVInPast24MonthsType === 33) {
        this.homelessAtRiskFormGroup.controls['isVictimOfDVInPast24MonthsCtrl'].setValue('Y');
      }
      else if (headOfHousehold.isVictimOfDVInPast24MonthsType === 34) {
        this.homelessAtRiskFormGroup.controls['isVictimOfDVInPast24MonthsCtrl'].setValue('N');
      }
      if (headOfHousehold.isVictimOfDVInPast24MonthsDescription) {
        this.isVictimOfDVInPast24MonthsDescription = "(" + headOfHousehold.isVictimOfDVInPast24MonthsDescription + ")";
      }
      this.homelessAtRisk.isVictimOfDVInPast24MonthsSource = this.headOfHousehold.isVictimOfDVInPast24MonthsSource;
      this.homelessAtRisk.isVictimOfDVInPast24MonthsOrigType = this.headOfHousehold.isVictimOfDVInPast24MonthsOrigType;
    }
  }

  //Populate Homeless At Risk
  populateHomlessAtRisk(homelessAtRisk: HomelessAtRisk) {
    if (homelessAtRisk) {
      this.homelessAtRisk = homelessAtRisk;
      //Has Frequent Moves Past 1 Year
      if (homelessAtRisk.hasFrequentMovesPast1YearType === 33) {
        this.homelessAtRiskFormGroup.controls['hasFrequentMovesPast1YearCtrl'].setValue('Y');
        this.isHeadOfHouseHoldEnabled = true;
        this.homelessAtRiskFormGroup.get("explainCtrl").enable();
      }
      else if (homelessAtRisk.hasFrequentMovesPast1YearType === 34) {
        this.homelessAtRiskFormGroup.controls['hasFrequentMovesPast1YearCtrl'].setValue('N');
      }
      //Have Limited Education
      if (homelessAtRisk.haveLimitedEducationType === 33) {
        this.homelessAtRiskFormGroup.controls['haveLimitedEducationCtrl'].setValue('Y');
      }
      else if (homelessAtRisk.haveLimitedEducationType === 34) {
        this.homelessAtRiskFormGroup.controls['haveLimitedEducationCtrl'].setValue('N');
      }
      if (homelessAtRisk.haveLimitedEducationDescription) {
        this.haveLimitedEducationDescription = "(" + homelessAtRisk.haveLimitedEducationDescription + ")";
      }
      //Have Limited Employment History
      if (homelessAtRisk.haveLimitedEmpHistoryType === 33) {
        this.homelessAtRiskFormGroup.controls['haveLimitedEmploymentHistoryCtrl'].setValue('Y');
      }
      else if (homelessAtRisk.haveLimitedEmpHistoryType === 34) {
        this.homelessAtRiskFormGroup.controls['haveLimitedEmploymentHistoryCtrl'].setValue('N');
      }
      if (homelessAtRisk.haveLimitedEmpHistoryDescription) {
        this.haveLimitedEmpHistoryDescription = "(" + homelessAtRisk.haveLimitedEmpHistoryDescription + ")";
      }
      //Young Adult With Children
      if (homelessAtRisk.yaWithChildrenType === 33) {
        this.homelessAtRiskFormGroup.controls['youngAdultWithChildrenCtrl'].setValue('Y');
      }
      else if (homelessAtRisk.yaWithChildrenType === 34) {
        this.homelessAtRiskFormGroup.controls['youngAdultWithChildrenCtrl'].setValue('N');
      }
      if (homelessAtRisk.yaWithChildrenDescription) {
        this.yaWithChildrenDescription = "(" + homelessAtRisk.yaWithChildrenDescription + ")";
      }
      //Shelter Stay In Past 24 Months
      if (homelessAtRisk.shelterStayInPast24MonthsType === 33) {
        this.homelessAtRiskFormGroup.controls['shelterStayInPast24MonthsCtrl'].setValue('Y');
      }
      else if (homelessAtRisk.shelterStayInPast24MonthsType === 34) {
        this.homelessAtRiskFormGroup.controls['shelterStayInPast24MonthsCtrl'].setValue('N');
      }
      //Is Victim Of DV In Past 24 Months
      if (homelessAtRisk.isVictimOfDVInPast24MonthsType === 33) {
        this.homelessAtRiskFormGroup.controls['isVictimOfDVInPast24MonthsCtrl'].setValue('Y');
      }
      else if (homelessAtRisk.isVictimOfDVInPast24MonthsType === 34) {
        this.homelessAtRiskFormGroup.controls['isVictimOfDVInPast24MonthsCtrl'].setValue('N');
      }
      if (homelessAtRisk.isVictimOfDVInPast24MonthsDescription) {
        this.isVictimOfDVInPast24MonthsDescription = "(" + homelessAtRisk.isVictimOfDVInPast24MonthsDescription + ")";
      }
      //Other Criteria For Homeless
      if (homelessAtRisk.otherCriteriaForHomelessType === 33) {
        this.homelessAtRiskFormGroup.controls['otherCriteriaForHomelessCtrl'].setValue('Y');
        this.homelessAtRiskFormGroup.controls['explainCtrl'].setValue(homelessAtRisk.explain);
        this.homelessAtRiskFormGroup.get("explainCtrl").enable();
        this.isExplainRequired = true;
      }
      else if (homelessAtRisk.otherCriteriaForHomelessType === 34) {
        this.homelessAtRiskFormGroup.controls['otherCriteriaForHomelessCtrl'].setValue('N');
        this.homelessAtRiskFormGroup.get("explainCtrl").disable();
        this.isExplainRequired = false;
      }
    }
  }

  //On Have Limited Education Change
  onHaveLimitedEducationChange($event: MatRadioChange) {
    if (this.homelessAtRisk) {
      if (this.homelessAtRisk.haveLimitedEducationOrigType === 33 && $event.value === "Y") {
        this.homelessAtRisk.haveLimitedEducationSource = 679;
      }
      else if (this.homelessAtRisk.haveLimitedEducationOrigType === 34 && $event.value === "N") {
        this.homelessAtRisk.haveLimitedEducationSource = 679;
      }
      else {
        this.homelessAtRisk.haveLimitedEducationSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Have Limited Employment History Change
  onHaveLimitedEmploymentHistoryChange($event: MatRadioChange) {
    if (this.homelessAtRisk) {
      if (this.homelessAtRisk.haveLimitedEmpHistoryOrigType === 33 && $event.value === "Y") {
        this.homelessAtRisk.haveLimitedEmpHistorySource = 679;
      }
      else if (this.homelessAtRisk.haveLimitedEmpHistoryOrigType === 34 && $event.value === "N") {
        this.homelessAtRisk.haveLimitedEmpHistorySource = 679;
      }
      else {
        this.homelessAtRisk.haveLimitedEmpHistorySource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Young Adult With Children Change
  onYoungAdultWithChildrenChange($event: MatRadioChange) {
    if (this.homelessAtRisk) {
      if (this.homelessAtRisk.yaWithChildrenOrigType === 33 && $event.value === "Y") {
        this.homelessAtRisk.yaWithChildrenSource = 679;
      }
      else if (this.homelessAtRisk.yaWithChildrenOrigType === 34 && $event.value === "N") {
        this.homelessAtRisk.yaWithChildrenSource = 679;
      }
      else {
        this.homelessAtRisk.yaWithChildrenSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Shelter Stay In Past 24 Months Change
  onShelterStayInPast24MonthsChange($event: MatRadioChange) {
    if (this.homelessAtRisk) {
      if (this.homelessAtRisk.shelterStayInPast24MonthsOrigType === 33 && $event.value === "Y") {
        this.homelessAtRisk.shelterStayInPast24MonthsSource = 679;
      }
      else if (this.homelessAtRisk.shelterStayInPast24MonthsOrigType === 34 && $event.value === "N") {
        this.homelessAtRisk.shelterStayInPast24MonthsSource = 679;
      }
      else {
        this.homelessAtRisk.shelterStayInPast24MonthsSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Is Victim Of DV In Past 24 Months Change
  onIsVictimOfDVInPast24MonthsChange($event: MatRadioChange) {
    if (this.homelessAtRisk) {
      if (this.homelessAtRisk.isVictimOfDVInPast24MonthsOrigType === 33 && $event.value === "Y") {
        this.homelessAtRisk.isVictimOfDVInPast24MonthsSource = 679;
      }
      else if (this.homelessAtRisk.isVictimOfDVInPast24MonthsOrigType === 34 && $event.value === "N") {
        this.homelessAtRisk.isVictimOfDVInPast24MonthsSource = 679;
      }
      else {
        this.homelessAtRisk.isVictimOfDVInPast24MonthsSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Other Criteria For Homeless Change
  onOtherCriteriaForHomelessChange($event: MatRadioChange) {
    if (this.homelessAtRisk) {
      if (this.homelessAtRisk.otherCriteriaForHomelessOrigType === 33 && $event.value === "Y") {
        this.homelessAtRisk.otherCriteriaForHomelessSource = 679;
      }
      else if (this.homelessAtRisk.otherCriteriaForHomelessOrigType === 34 && $event.value === "N") {
        this.homelessAtRisk.otherCriteriaForHomelessSource = 679;
      }
      else {
        this.homelessAtRisk.otherCriteriaForHomelessSource = 680;
      }
    }
    if ($event.value === "Y") {
      this.homelessAtRiskFormGroup.get("explainCtrl").enable();
      this.isExplainRequired = true;
    }
    else if ($event.value === "N") {
      this.isExplainRequired = false;
      this.homelessAtRiskFormGroup.get("explainCtrl").setValue(null);
      this.homelessAtRiskFormGroup.get("explainCtrl").disable();
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //Set Next Tab Value
  setNextTab(value: boolean) {
    this.homelessReviewService.setHomelessSummaryTabEnabled(value);
    this.homelessReviewService.setHomelessReportTabEnabled(value);
  }

  //Disable Next Side Navigation
  disableNextSideNavigation(value: boolean) {
    this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(value);
    this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(value);
    this.determinationSideNavService.setIsDeterminationSummaryDisabled(value);
    this.determinationSideNavService.setIsSignOffFollowUpDisabled(value);
  }

  //Validate Homeless At Risk
  validateHomelessAtRisk(): boolean {
    if (this.homelessAtRiskFormGroup.get('hasFrequentMovesPast1YearCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Has Frequent Moves Past 1 Year is required.");
      }
      return false;
    }
    if (this.isHeadOfHouseHoldEnabled) {
      if (this.homelessAtRiskFormGroup.get('haveLimitedEducationCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Have Limited Education is required.");
        }
        return false;
      }
      if (this.homelessAtRiskFormGroup.get('haveLimitedEmploymentHistoryCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Have Limited Employment History is required.");
        }
        return false;
      }
      if (this.homelessAtRiskFormGroup.get('youngAdultWithChildrenCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Young Adult With Children is required.");
        }
        return false;
      }
      if (this.homelessAtRiskFormGroup.get('shelterStayInPast24MonthsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Shelter Stay In Past 24 Months is required.");
        }
        return false;
      }
      if (this.homelessAtRiskFormGroup.get('isVictimOfDVInPast24MonthsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Is Victim Of DV In Past 24 Months is required.");
        }
        return false;
      }
      if (this.homelessAtRiskFormGroup.get('otherCriteriaForHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Other Criteria For Homeless is required.");
        }
        return false;
      }
      if (this.isExplainRequired) {
        if (this.homelessAtRiskFormGroup.get('explainCtrl').value.length == 1) {
          if (!this.message.currentlyActive) {
            this.message.error("Explain cannot be less than 2 characters.");
          }
          return false;
        }
        else if (this.homelessAtRiskFormGroup.get('explainCtrl').value == null || this.homelessAtRiskFormGroup.get('explainCtrl').value.trim() == "") {
          if (!this.message.currentlyActive) {
            this.message.error("Explain is required.");
          }
          return false;
        }
      }
    }
    return true;
  }

  //Next Homeless At Risk Button Click
  nextTab() {
    if (this.validateHomelessAtRisk()) {
      this.saveHomelessAtRiskDetails(2);
    }
  }

  //Previous Homeless At Risk Button Click
  previousTab() {
    if (this.validateHomelessAtRisk()) {
      this.saveHomelessAtRiskDetails(3);
    }
  }

  //Save Homeless At Risk Data
  saveHomelessAtRiskData() {
    if (this.validateHomelessAtRisk()) {
      this.saveHomelessAtRiskDetails(1);
    }
  }

  //Save Homeless At Risk Details
  saveHomelessAtRiskDetails(path: number) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId && this.applicationDeterminationData.clientCategoryType && this.homelessAtRisk) {
      this.saveHomelessAtRisk.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.saveHomelessAtRisk.clientCategory = this.applicationDeterminationData.clientCategoryType;
      this.saveHomelessAtRisk.optionUserId = this.userData.optionUserId;
      this.saveHomelessAtRisk.hasFrequentMovesPast1YearOrigType = this.homelessAtRiskFormGroup.get('hasFrequentMovesPast1YearCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('hasFrequentMovesPast1YearCtrl').value === 'N' ? 34 : null;
      this.saveHomelessAtRisk.hasFrequentMovesPast1YearType = this.homelessAtRiskFormGroup.get('hasFrequentMovesPast1YearCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('hasFrequentMovesPast1YearCtrl').value === 'N' ? 34 : null;
      this.saveHomelessAtRisk.hasFrequentMovesPast1YearSource = this.homelessAtRisk.hasFrequentMovesPast1YearSource;
      if (this.isHeadOfHouseHoldEnabled) {
        this.saveHomelessAtRisk.haveLimitedEducationOrigType = this.homelessAtRisk.haveLimitedEducationOrigType;
        this.saveHomelessAtRisk.haveLimitedEducationType = this.homelessAtRiskFormGroup.get('haveLimitedEducationCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('haveLimitedEducationCtrl').value === 'N' ? 34 : null;
        this.saveHomelessAtRisk.haveLimitedEducationSource = this.homelessAtRisk.haveLimitedEducationSource;
        this.saveHomelessAtRisk.haveLimitedEmpHistoryOrigType = this.homelessAtRisk.haveLimitedEmpHistoryOrigType;
        this.saveHomelessAtRisk.haveLimitedEmpHistoryType = this.homelessAtRiskFormGroup.get('haveLimitedEmploymentHistoryCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('haveLimitedEmploymentHistoryCtrl').value === 'N' ? 34 : null;
        this.saveHomelessAtRisk.haveLimitedEmpHistorySource = this.homelessAtRisk.haveLimitedEmpHistorySource;
        this.saveHomelessAtRisk.yaWithChildrenOrigType = this.homelessAtRisk.yaWithChildrenOrigType;
        this.saveHomelessAtRisk.yaWithChildrenType = this.homelessAtRiskFormGroup.get('youngAdultWithChildrenCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('youngAdultWithChildrenCtrl').value === 'N' ? 34 : null;
        this.saveHomelessAtRisk.yaWithChildrenSource = this.homelessAtRisk.yaWithChildrenSource;
        this.saveHomelessAtRisk.shelterStayInPast24MonthsOrigType = this.homelessAtRisk.shelterStayInPast24MonthsOrigType;
        this.saveHomelessAtRisk.shelterStayInPast24MonthsType = this.homelessAtRiskFormGroup.get('shelterStayInPast24MonthsCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('shelterStayInPast24MonthsCtrl').value === 'N' ? 34 : null;
        this.saveHomelessAtRisk.shelterStayInPast24MonthsSource = this.homelessAtRisk.shelterStayInPast24MonthsSource;
        this.saveHomelessAtRisk.isVictimOfDVInPast24MonthsOrigType = this.homelessAtRisk.isVictimOfDVInPast24MonthsOrigType;
        this.saveHomelessAtRisk.isVictimOfDVInPast24MonthsType = this.homelessAtRiskFormGroup.get('isVictimOfDVInPast24MonthsCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('isVictimOfDVInPast24MonthsCtrl').value === 'N' ? 34 : null;
        this.saveHomelessAtRisk.isVictimOfDVInPast24MonthsSource = this.homelessAtRisk.isVictimOfDVInPast24MonthsSource;
        this.saveHomelessAtRisk.otherCriteriaForHomelessOrigType = this.homelessAtRiskFormGroup.get('otherCriteriaForHomelessCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('otherCriteriaForHomelessCtrl').value === 'N' ? 34 : null;
        this.saveHomelessAtRisk.otherCriteriaForHomelessType = this.homelessAtRiskFormGroup.get('otherCriteriaForHomelessCtrl').value === 'Y' ? 33 : this.homelessAtRiskFormGroup.get('otherCriteriaForHomelessCtrl').value === 'N' ? 34 : null;
        this.saveHomelessAtRisk.otherCriteriaForHomelessSource = this.homelessAtRisk.otherCriteriaForHomelessSource;
        if (this.isExplainRequired) {
          if (this.homelessAtRiskFormGroup.get('explainCtrl').value != null) {
            this.saveHomelessAtRisk.explain = this.homelessAtRiskFormGroup.get('explainCtrl').value.trim();
          }
        }
      }
      this.homelessReviewService.saveHomelessAtRisk(this.saveHomelessAtRisk)
        .subscribe(res => {
          if (res) {
            if (path === 1) {
              if (!this.message.currentlyActive) {
                this.message.success("Homeless at risk form updated successfully!");
              }
            }
            else if (path === 2) {
              this.nextHomelessAtRiskTab.emit();
            }
            else if (path === 3) {
              this.previousHomelessAtRiskTab.emit();
            }
            else {
              return;
            }
          }
        });
    }
  }
}
