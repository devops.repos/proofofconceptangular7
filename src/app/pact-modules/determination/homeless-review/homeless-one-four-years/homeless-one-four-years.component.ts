import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import { MatRadioChange } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import {
  iHomelessCounts,
  iHomelessCountsInput,
  iHomeless1To4Years,
  iHomeless1To4YearsInput,
  SaveHomeless1To4Years
} from '../homeless-review.model';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { HomelesReviewService } from '../homeless-review.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

@Component({
  selector: 'app-homeless-one-four-years',
  templateUrl: './homeless-one-four-years.component.html',
  styleUrls: ['./homeless-one-four-years.component.scss']
})

export class HomelessOneFourYearsComponent implements OnInit, OnDestroy {
  @Output() nextHomeless1To4YearsTab = new EventEmitter();
  @Output() previousHomeless1To4YearsTab = new EventEmitter();

  //Global Variables
  clientCategory: number = 0;
  homelessCountsExpanded: boolean;

  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  homeless1To4YearsFormGroup: FormGroup;

  homelessCountsColumnDefs = [];
  homelessCountsRowData: iHomelessCounts[];
  homelessCountsData: iHomelessCounts[];
  homelessCountsDefaultColDef = {};
  homelessCountsGridOptions: GridOptions;
  homelessCountsOverlayNoRowsTemplate: string;

  homelessCountsInput: iHomelessCountsInput = {
    pactApplicationId: null,
    optionUserId: null
  }

  homeless1To4Years: iHomeless1To4Years;
  homeless1To4YearsInput: iHomeless1To4YearsInput = {
    pactApplicationId: null,
    clientCategory: null,
    optionUserId: null
  }

  saveHomeless1To4Years = new SaveHomeless1To4Years();

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private homelessReviewService: HomelesReviewService,
    private message: ToastrService,
    private determinationSideNavService: DeterminationSideNavService) {

    //Homeless 1-4 Years Form Grroup
    this.homeless1To4YearsFormGroup = this.formBuilder.group({
      currentlyHomelessCtrl: ['', Validators.required],
      homeless2OfPast4YearsCtrl: ['', Validators.required],
      homeless1OfPast2YearsCtrl: ['', Validators.required],
      homeless6mOfPast1YearCtrl: ['', Validators.required],
      homeless4mOfPast1YearCtrl: ['', Validators.required],
      currentlyInDHSShelterCtrl: ['', Validators.required],
      homeless90DaysOfPast1YearCtrl: ['', Validators.required],
      homeless14OfPast60DaysCtrl: ['', Validators.required],
      continuePriorIandIIEligibilityCtrl: ['', Validators.required],
      homeless365daysOfPast1YearCtrl: ['', Validators.required],
      homeless4EpisodesPast3YearsCtrl: ['', Validators.required]
    });

    //Homeless Counts Grid Column Definitions
    this.homelessCountsColumnDefs = [
      {
        headerName: 'Data Source',
        field: 'dataSource',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Past 4 Years (730 Required)',
        field: 'past4Years',
        cellStyle: function (params: { value: number; data: { dataSource: string }; }) {
          if (params.value >= 730 && params.data.dataSource === 'Total') {
            return { backgroundColor: 'lightgreen' };
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Past 2 Years (365 Required)',
        field: 'past2Years',
        cellStyle: function (params: { value: number; data: { dataSource: string }; }) {
          if (params.value >= 365 && params.data.dataSource === 'Total') {
            return { backgroundColor: 'lightgreen' };
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Past 1 Year (182 Required)',
        field: 'past1Year182',
        cellStyle: function (params: { value: number; data: { dataSource: string }; }) {
          if (params.value >= 182 && params.data.dataSource === 'Total') {
            return { backgroundColor: 'lightgreen' };
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Last 120 Days (120 Required)',
        field: 'last120Days',
        cellStyle: function (params: { value: number; data: { dataSource: string }; }) {
          if (params.value >= 120 && params.data.dataSource === 'Total') {
            return { backgroundColor: 'lightgreen' };
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Past 1 Year (90 Required)',
        field: 'past1Year90',
        cellStyle: function (params: { value: number; data: { dataSource: string }; }) {
          if (params.value >= 90 && params.data.dataSource === 'Total') {
            return { backgroundColor: 'lightgreen' };
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Last 60 Days (14 Required)',
        field: 'last60Days',
        cellStyle: function (params: { value: number; data: { dataSource: string }; }) {
          if (params.value >= 14 && params.data.dataSource === 'Total') {
            return { backgroundColor: 'lightgreen' };
          }
        },
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Client Name (L,F)',
        field: 'clientName',
        filter: 'agTextColumnFilter',
        width: 200
      },
      {
        headerName: 'Client #',
        field: 'clientNumber',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'SSN',
        field: 'ssn',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'DOB',
        field: 'dob',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Currently in Shelter',
        field: 'currentlyInShelter',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Facility Name',
        field: 'facilityName',
        filter: 'agTextColumnFilter',
        width: 300
      }
    ];
    this.homelessCountsDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData()
      .subscribe(res => {
        if (res) {
          const data = res as iApplicationDeterminationData;
          if (data) {
            this.applicationDeterminationData = data;
            if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType && this.applicationDeterminationData.pactApplicationId) {
              this.clientCategory = this.applicationDeterminationData.clientCategoryType;
              this.homelessCountsInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
              this.homelessCountsInput.optionUserId = this.userData.optionUserId;
              this.homelessReviewService.getHomelessCounts(this.homelessCountsInput)
                .subscribe(res => {
                  if (res) {
                    const data = res as iHomelessCounts[];
                    if (data) {
                      this.homelessCountsData = data;
                      this.homeless1To4YearsInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
                      this.homeless1To4YearsInput.clientCategory = this.applicationDeterminationData.clientCategoryType;
                      this.homeless1To4YearsInput.optionUserId = this.userData.optionUserId;
                      this.homelessReviewService.getHomeless1To4Years(this.homeless1To4YearsInput)
                        .subscribe(res => {
                          if (res) {
                            const data = res as iHomeless1To4Years;
                            if (data) {
                              this.populateHomeless1To4Years(data);
                            }
                          }
                        });
                    }
                  }
                });
            }
            else {
              throw new Error("Missing Client Category or Pact Application ID");
            }
          }
        }
      });
  }

  //Destroy 
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Homeless Counts Grid Ready
  homelessCountsOnGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.homelessCountsOverlayNoRowsTemplate = '<span style="color: #337ab7">No Counts To Show</span>';
    if (this.homelessCountsData) {
      this.homelessCountsRowData = this.homelessCountsData;
    }
  }

  //Populate Homeless 1-4 Years
  populateHomeless1To4Years(homeless1To4Years: iHomeless1To4Years) {
    if (homeless1To4Years) {
      this.homeless1To4Years = homeless1To4Years;
      //Currently Homeless
      if (homeless1To4Years.currentlyHomelessType === 33) {
        this.homeless1To4YearsFormGroup.controls['currentlyHomelessCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.currentlyHomelessType === 34) {
        this.homeless1To4YearsFormGroup.controls['currentlyHomelessCtrl'].setValue('N');
      }
      //Homeless 2 Of Past 4 Years
      if (homeless1To4Years.homeless2OfPast4YearsType === 33) {
        this.homeless1To4YearsFormGroup.controls['homeless2OfPast4YearsCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.homeless2OfPast4YearsType === 34) {
        this.homeless1To4YearsFormGroup.controls['homeless2OfPast4YearsCtrl'].setValue('N');
      }
      //Homeless 1 Of Past 2 Years
      if (homeless1To4Years.homeless1OfPast2YearsType === 33) {
        this.homeless1To4YearsFormGroup.controls['homeless1OfPast2YearsCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.homeless1OfPast2YearsType === 34) {
        this.homeless1To4YearsFormGroup.controls['homeless1OfPast2YearsCtrl'].setValue('N');
      }
      //Homeless 6 Months Of Past 1 Year
      if (homeless1To4Years.homeless6mOfPast1YearType === 33) {
        this.homeless1To4YearsFormGroup.controls['homeless6mOfPast1YearCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.homeless6mOfPast1YearType === 34) {
        this.homeless1To4YearsFormGroup.controls['homeless6mOfPast1YearCtrl'].setValue('N');
      }
      //Homeless 4 Months Of Past 1 Year
      if (homeless1To4Years.homeless4mOfPast1YearType === 33) {
        this.homeless1To4YearsFormGroup.controls['homeless4mOfPast1YearCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.homeless4mOfPast1YearType === 34) {
        this.homeless1To4YearsFormGroup.controls['homeless4mOfPast1YearCtrl'].setValue('N');
      }
      //Currently In DHS Shelter
      if (homeless1To4Years.currentlyInDHSShelterType === 33) {
        this.homeless1To4YearsFormGroup.controls['currentlyInDHSShelterCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.currentlyInDHSShelterType === 34) {
        this.homeless1To4YearsFormGroup.controls['currentlyInDHSShelterCtrl'].setValue('N');
      }
      //Homeless 90 Days Of Past 1 Year
      if (homeless1To4Years.homeless90DaysOfPast1YearType === 33) {
        this.homeless1To4YearsFormGroup.controls['homeless90DaysOfPast1YearCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.homeless90DaysOfPast1YearType === 34) {
        this.homeless1To4YearsFormGroup.controls['homeless90DaysOfPast1YearCtrl'].setValue('N');
      }
      //Homeless 14 Days Of Past 60 Days
      if (homeless1To4Years.homeless14OfPast60DaysType === 33) {
        this.homeless1To4YearsFormGroup.controls['homeless14OfPast60DaysCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.homeless14OfPast60DaysType === 34) {
        this.homeless1To4YearsFormGroup.controls['homeless14OfPast60DaysCtrl'].setValue('N');
      }
      //Continue Prior I And II Eligibility
      if (homeless1To4Years.continuePriorIandIIEligibilityType === 33) {
        this.homeless1To4YearsFormGroup.controls['continuePriorIandIIEligibilityCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.continuePriorIandIIEligibilityType === 34) {
        this.homeless1To4YearsFormGroup.controls['continuePriorIandIIEligibilityCtrl'].setValue('N');
      }
      //Homeless 365 Days Of Past 1 Year
      if (homeless1To4Years.homeless365daysOfPast1YearOrigType === 33) {
        this.homeless1To4YearsFormGroup.controls['homeless365daysOfPast1YearCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.homeless365daysOfPast1YearOrigType === 34) {
        this.homeless1To4YearsFormGroup.controls['homeless365daysOfPast1YearCtrl'].setValue('N');
      }
      //Homeless 4 Episodes Of Past 3 Years
      if (homeless1To4Years.homeless4EpisodesPast3YearsOrigType === 33) {
        this.homeless1To4YearsFormGroup.controls['homeless4EpisodesPast3YearsCtrl'].setValue('Y');
      }
      else if (homeless1To4Years.homeless4EpisodesPast3YearsOrigType === 34) {
        this.homeless1To4YearsFormGroup.controls['homeless4EpisodesPast3YearsCtrl'].setValue('N');
      }
      //At Risk Tab Elibgibility
      if (homeless1To4Years.homeless2OfPast4YearsType === 33 || homeless1To4Years.homeless1OfPast2YearsType === 33) {
        this.homelessReviewService.setAtRiskTabEligible(false);
      }
      else {
        this.homelessReviewService.setAtRiskTabEligible(true);
      }
    }
  }

  //On Currently Homeless Change
  onCurrentlyHomelessChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.currentlyHomelessOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.currentlyHomelessSource = 679;
      }
      else if (this.homeless1To4Years.currentlyHomelessOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.currentlyHomelessSource = 679;
      }
      else {
        this.homeless1To4Years.currentlyHomelessSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Homeless 2 Of Past 4 Years Change
  onHomeless2OfPast4YearsChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.homeless2OfPast4YearsOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.homeless2OfPast4YearsSource = 679;
      }
      else if (this.homeless1To4Years.homeless2OfPast4YearsOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.homeless2OfPast4YearsSource = 679;
      }
      else {
        this.homeless1To4Years.homeless2OfPast4YearsSource = 680;
      }
    }
    //At Risk Tab Eligibility
    if ($event.value === 'Y' || this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'Y') {
      this.homelessReviewService.setAtRiskTabEligible(false);
    }
    else {
      this.homelessReviewService.setAtRiskTabEligible(true);
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Homeless 1 Of Past 2 Years Change
  onHomeless1OfPast2YearsChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.homeless1OfPast2YearsOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.homeless1OfPast2YearsSource = 679;
      }
      else if (this.homeless1To4Years.homeless1OfPast2YearsOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.homeless1OfPast2YearsSource = 679;
      }
      else {
        this.homeless1To4Years.homeless1OfPast2YearsSource = 680;
      }
    }
    //At Risk Tab Eligibility
    if ($event.value === 'Y' || this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'Y') {
      this.homelessReviewService.setAtRiskTabEligible(false);
    }
    else {
      this.homelessReviewService.setAtRiskTabEligible(true);
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Homeless 6 Months Of Past 1 Year Change
  onHomeless6mOfPast1YearChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.homeless6mOfPast1YearOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.homeless6mOfPast1YearSource = 679;
      }
      else if (this.homeless1To4Years.homeless6mOfPast1YearOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.homeless6mOfPast1YearSource = 679;
      }
      else {
        this.homeless1To4Years.homeless6mOfPast1YearSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Homeless 4 Months Of Past 1 Year Change
  onHomeless4mOfPast1YearChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.homeless4mOfPast1YearOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.homeless4mOfPast1YearSource = 679;
      }
      else if (this.homeless1To4Years.homeless4mOfPast1YearOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.homeless4mOfPast1YearSource = 679;
      }
      else {
        this.homeless1To4Years.homeless4mOfPast1YearSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Currenlty In DHS Shelter Change
  onCurrentlyInDHSShelterChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.currentlyInDHSShelterOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.currentlyInDHSShelterSource = 679;
      }
      else if (this.homeless1To4Years.currentlyInDHSShelterOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.currentlyInDHSShelterSource = 679;
      }
      else {
        this.homeless1To4Years.currentlyInDHSShelterSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Homeless 90 Days In 1 Year Change
  onHomeless90DaysOfPast1YearChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.homeless90DaysOfPast1YearOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.homeless90DaysOfPast1YearSource = 679;
      }
      else if (this.homeless1To4Years.homeless90DaysOfPast1YearOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.homeless90DaysOfPast1YearSource = 679;
      }
      else {
        this.homeless1To4Years.homeless90DaysOfPast1YearSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Homeless 14 Days Past 60 Days Change
  onHomeless14OfPast60DaysChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.homeless14OfPast60DaysOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.homeless14OfPast60DaysSource = 679;
      }
      else if (this.homeless1To4Years.homeless14OfPast60DaysOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.homeless14OfPast60DaysSource = 679;
      }
      else {
        this.homeless1To4Years.homeless14OfPast60DaysSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Continue Prior I and II Eligibility Change
  onContinuePriorIandIIEligibilityChange($event: MatRadioChange) {
    if (this.homeless1To4Years) {
      if (this.homeless1To4Years.continuePriorIandIIEligibilityOrigType === 33 && $event.value === "Y") {
        this.homeless1To4Years.continuePriorIandIIEligibilitySource = 679;
      }
      else if (this.homeless1To4Years.continuePriorIandIIEligibilityOrigType === 34 && $event.value === "N") {
        this.homeless1To4Years.continuePriorIandIIEligibilitySource = 679;
      }
      else {
        this.homeless1To4Years.continuePriorIandIIEligibilitySource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //Set Next Tab Value
  setNextTab(value: boolean) {
    this.homelessReviewService.setResidentialTreatmentTabEnabled(value);
    this.homelessReviewService.setHomelessAtRiskTabEnabled(value);
    this.homelessReviewService.setHomelessSummaryTabEnabled(value);
    this.homelessReviewService.setHomelessReportTabEnabled(value);
  }

  //Disable Next Side Navigation
  disableNextSideNavigation(value: boolean) {
    this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(value);
    this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(value);
    this.determinationSideNavService.setIsDeterminationSummaryDisabled(value);
    this.determinationSideNavService.setIsSignOffFollowUpDisabled(value);
  }

  //Save Homeless 1-4 Years
  saveHomeless1To4YearsData() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType) {
      if (this.validateHomless1To4Years(this.applicationDeterminationData.clientCategoryType)) {
        this.saveHomeless1To4YearDetails(1);
      }
      else {
        throw new Error("Invalid Client Category!");
      }
    }
  }

  //Save Homeless 1 To 4 Year Details
  saveHomeless1To4YearDetails(path: number) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId && this.applicationDeterminationData.clientCategoryType && this.homeless1To4Years) {
      this.saveHomeless1To4Years.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.saveHomeless1To4Years.clientCategory = this.applicationDeterminationData.clientCategoryType;
      this.saveHomeless1To4Years.optionUserId = this.userData.optionUserId;
      if (this.applicationDeterminationData.clientCategoryType === 671) {
        this.saveHomeless1To4Years.currentlyHomelessType = this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.currentlyHomelessSource = this.homeless1To4Years.currentlyHomelessSource;
        this.saveHomeless1To4Years.homeless2OfPast4YearsType = this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless2OfPast4YearsSource = this.homeless1To4Years.homeless2OfPast4YearsSource;
        this.saveHomeless1To4Years.homeless1OfPast2YearsType = this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless1OfPast2YearsSource = this.homeless1To4Years.homeless1OfPast2YearsSource;
        this.saveHomeless1To4Years.homeless6mOfPast1YearType = this.homeless1To4YearsFormGroup.get('homeless6mOfPast1YearCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless6mOfPast1YearCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless6mOfPast1YearSource = this.homeless1To4Years.homeless6mOfPast1YearSource;
        this.saveHomeless1To4Years.homeless4mOfPast1YearType = this.homeless1To4YearsFormGroup.get('homeless4mOfPast1YearCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless4mOfPast1YearCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless4mOfPast1YearSource = this.homeless1To4Years.homeless4mOfPast1YearSource;
        this.saveHomeless1To4Years.homeless14OfPast60DaysType = this.homeless1To4YearsFormGroup.get('homeless14OfPast60DaysCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless14OfPast60DaysCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless14OfPast60DaysSource = this.homeless1To4Years.homeless14OfPast60DaysSource;
        this.saveHomeless1To4Years.continuePriorIandIIEligibilityType = this.homeless1To4YearsFormGroup.get('continuePriorIandIIEligibilityCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('continuePriorIandIIEligibilityCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.continuePriorIandIIEligibilitySource = this.homeless1To4Years.continuePriorIandIIEligibilitySource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 672) {
        this.saveHomeless1To4Years.currentlyHomelessType = this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.currentlyHomelessSource = this.homeless1To4Years.currentlyHomelessSource;
        this.saveHomeless1To4Years.homeless2OfPast4YearsType = this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless2OfPast4YearsSource = this.homeless1To4Years.homeless2OfPast4YearsSource;
        this.saveHomeless1To4Years.homeless1OfPast2YearsType = this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless1OfPast2YearsSource = this.homeless1To4Years.homeless1OfPast2YearsSource;
        this.saveHomeless1To4Years.homeless6mOfPast1YearType = this.homeless1To4YearsFormGroup.get('homeless6mOfPast1YearCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless6mOfPast1YearCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless6mOfPast1YearSource = this.homeless1To4Years.homeless6mOfPast1YearSource;
        this.saveHomeless1To4Years.homeless4mOfPast1YearType = this.homeless1To4YearsFormGroup.get('homeless4mOfPast1YearCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless4mOfPast1YearCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless4mOfPast1YearSource = this.homeless1To4Years.homeless4mOfPast1YearSource;
        this.saveHomeless1To4Years.homeless90DaysOfPast1YearType = this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless90DaysOfPast1YearSource = this.homeless1To4Years.homeless90DaysOfPast1YearSource;
        this.saveHomeless1To4Years.homeless14OfPast60DaysType = this.homeless1To4YearsFormGroup.get('homeless14OfPast60DaysCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless14OfPast60DaysCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless14OfPast60DaysSource = this.homeless1To4Years.homeless14OfPast60DaysSource;
        this.saveHomeless1To4Years.continuePriorIandIIEligibilityType = this.homeless1To4YearsFormGroup.get('continuePriorIandIIEligibilityCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('continuePriorIandIIEligibilityCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.continuePriorIandIIEligibilitySource = this.homeless1To4Years.continuePriorIandIIEligibilitySource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 673) {
        this.saveHomeless1To4Years.currentlyHomelessType = this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.currentlyHomelessSource = this.homeless1To4Years.currentlyHomelessSource;
        this.saveHomeless1To4Years.homeless2OfPast4YearsType = this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless2OfPast4YearsSource = this.homeless1To4Years.homeless2OfPast4YearsSource;
        this.saveHomeless1To4Years.homeless1OfPast2YearsType = this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless1OfPast2YearsSource = this.homeless1To4Years.homeless1OfPast2YearsSource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 674) {
        this.saveHomeless1To4Years.currentlyHomelessType = this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.currentlyHomelessSource = this.homeless1To4Years.currentlyHomelessSource;
        this.saveHomeless1To4Years.homeless2OfPast4YearsType = this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless2OfPast4YearsSource = this.homeless1To4Years.homeless2OfPast4YearsSource;
        this.saveHomeless1To4Years.homeless1OfPast2YearsType = this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless1OfPast2YearsSource = this.homeless1To4Years.homeless1OfPast2YearsSource;
        this.saveHomeless1To4Years.homeless90DaysOfPast1YearType = this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless90DaysOfPast1YearSource = this.homeless1To4Years.homeless90DaysOfPast1YearSource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 675) {
        this.saveHomeless1To4Years.currentlyHomelessType = this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.currentlyHomelessSource = this.homeless1To4Years.currentlyHomelessSource;
        this.saveHomeless1To4Years.currentlyInDHSShelterType = this.homeless1To4YearsFormGroup.get('currentlyInDHSShelterCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('currentlyInDHSShelterCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.currentlyInDHSShelterSource = this.homeless1To4Years.currentlyInDHSShelterSource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 676) {
        this.saveHomeless1To4Years.currentlyHomelessType = this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.currentlyHomelessSource = this.homeless1To4Years.currentlyHomelessSource;
        this.saveHomeless1To4Years.currentlyInDHSShelterType = this.homeless1To4YearsFormGroup.get('currentlyInDHSShelterCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('currentlyInDHSShelterCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.currentlyInDHSShelterSource = this.homeless1To4Years.currentlyInDHSShelterSource;
        this.saveHomeless1To4Years.homeless90DaysOfPast1YearType = this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').value === 'Y' ? 33 : this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').value === 'N' ? 34 : null;
        this.saveHomeless1To4Years.homeless90DaysOfPast1YearSource = this.homeless1To4Years.homeless90DaysOfPast1YearSource;
      }
      this.homelessReviewService.saveHomeless1To4Years(this.saveHomeless1To4Years)
        .subscribe(res => {
          if (res) {
            if (path === 1) {
              if (!this.message.currentlyActive) {
                this.message.success("Homeless 1-4 Years form updated successfully!");
              }
            }
            else if (path === 2) {
              this.nextHomeless1To4YearsTab.emit();
            }
            else if (path === 3) {
              this.previousHomeless1To4YearsTab.emit();
            }
            else {
              return;
            }
          }
        });
    }
  }

  //Validate Homeless 1-4 Years
  validateHomless1To4Years(clientCategory: number): boolean {
    if (clientCategory === 671) {
      if (this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Homeless is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 2 Of Past 4 Years is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 1 Of Past 2 Years is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless6mOfPast1YearCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 6 Months Past 1 Year is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless4mOfPast1YearCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 4 Months Past 1 Year is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless14OfPast60DaysCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 14 Days Past 60 Days is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('continuePriorIandIIEligibilityCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Continue With Prior I and II Eligibility is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 672) {
      if (this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Homeless is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 2 Of Past 4 Years is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 1 Of Past 2 Years is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless6mOfPast1YearCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 6 Months Past 1 Year is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless4mOfPast1YearCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 4 Months Past 1 Year is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 90 Days Past 1 Year is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless14OfPast60DaysCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 14 Days Past 60 Days is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('continuePriorIandIIEligibilityCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Continue With Prior I and II Eligibility is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 673) {
      if (this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Homeless is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 2 Of Past 4 Years is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 1 Of Past 2 Years is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 674) {
      if (this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Homeless is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless2OfPast4YearsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 2 Of Past 4 Years is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless1OfPast2YearsCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 1 Of Past 2 Years is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 90 Days Past 1 Year is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 675) {
      if (this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Homeless is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('currentlyInDHSShelterCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently In DHS Shelter is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 676) {
      if (this.homeless1To4YearsFormGroup.get('currentlyHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Homeless is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('currentlyInDHSShelterCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently In DHS Shelter is required.");
        }
        return false;
      }
      if (this.homeless1To4YearsFormGroup.get('homeless90DaysOfPast1YearCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Homeless 90 Days Past 1 Year is required.");
        }
        return false;
      }
      return true;
    }
    else {
      return false;
    }
  }

  //Next Homeless 1-4 Years Button Click
  nextTab() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType) {
      if (this.validateHomless1To4Years(this.applicationDeterminationData.clientCategoryType)) {
        this.saveHomeless1To4YearDetails(2);
      }
      else {
        throw new Error("Invalid Client Category!");
      }
    }
  }

  //Previous Homeless 1-4 Years Button Click
  previousTab() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType) {
      if (this.validateHomless1To4Years(this.applicationDeterminationData.clientCategoryType)) {
        this.saveHomeless1To4YearDetails(3);
      }
      else {
        throw new Error("Invalid Client Category!");
      }
    }
  }
}
