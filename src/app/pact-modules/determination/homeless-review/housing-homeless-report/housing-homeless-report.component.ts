import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';

@Component({
  selector: 'app-housing-homeless-report',
  templateUrl: './housing-homeless-report.component.html',
  styleUrls: ['./housing-homeless-report.component.scss']
})

export class HousingHomelessReportComponent implements OnInit, OnDestroy {
  @Output() nextHomelessReportTab = new EventEmitter();
  @Output() previousHomelessReportTab = new EventEmitter();

  //Global Variables
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  housingHomelessGuid: string;
  reportParams: PACTReportUrlParams;
  reportParameters: PACTReportParameters[] = [];
  housingHomelessReportURL: SafeResourceUrl = null;

  constructor(private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData()
      .subscribe(res => {
        if (res) {
          const data = res as iApplicationDeterminationData;
          if (data) {
            this.applicationDeterminationData = data;
            if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
              this.viewServerReport(this.applicationDeterminationData, 'HHHReport');
            }
          }
        }
      });
  }

  //Destroy 
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //View Server Report
  viewServerReport(applicationDeterminationData: iApplicationDeterminationData, reportName: string) {
    if (!this.housingHomelessGuid) {
      this.reportParameters = [
        { parameterName: 'applicationID', parameterValue: applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
        { parameterName: 'reportName', parameterValue: reportName, CreatedBy: this.userData.optionUserId }
      ];
      this.commonService.generateGUID(this.reportParameters)
        .subscribe(
          res => {
            if (res) {
              this.housingHomelessGuid = res;
              this.generateReport(res, reportName);
            }
          }
        );
    }
    else {
      this.generateReport(this.housingHomelessGuid, reportName);
    }
  }

  //Generate Report
  generateReport(reportGuid: string, reportName: string) {
    this.reportParams = { reportParameterID: reportGuid, reportName: reportName, "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.housingHomelessReportURL = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(data));
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }

  //Next Homeless Report Button Click
  nextTab() {
    this.nextHomelessReportTab.emit();
  }

  //Previous Homeless Report Button Click
  previousTab() {
    this.previousHomelessReportTab.emit();
  }
}
