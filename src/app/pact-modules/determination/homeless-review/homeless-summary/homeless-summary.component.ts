import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { DETClinicalHomelessMedicaidReviewInput } from 'src/app/pact-modules/determination/clinical-review/clinical-review.model';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';

@Component({
  selector: 'app-homeless-summary',
  templateUrl: './homeless-summary.component.html',
  styleUrls: ['./homeless-summary.component.scss']
})

export class HomelessSummaryComponent implements OnInit, OnDestroy {
  @Output() nextHomelessSummaryTab = new EventEmitter<number>();
  @Output() previousHomelessSummaryTab = new EventEmitter();

  //Global Variables
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  homelessPath: number;

  clinicalHomelessMedicaidReviewInput: DETClinicalHomelessMedicaidReviewInput = {
    pactApplicationID: null,
    summaryType: null,
    clientCategoryType: null,
    userID: null
  }

  //Constructor
  constructor(private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService) {
  }

  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData()
      .subscribe(res => {
        if (res) {
          const data = res as iApplicationDeterminationData;
          if (data) {
            this.applicationDeterminationData = data;
            if (this.applicationDeterminationData &&
              this.applicationDeterminationData.pactApplicationId &&
              this.applicationDeterminationData.clientCategoryType) {
              this.clinicalHomelessMedicaidReviewInput.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
              this.clinicalHomelessMedicaidReviewInput.summaryType = 612;
              this.clinicalHomelessMedicaidReviewInput.clientCategoryType = this.applicationDeterminationData.clientCategoryType;
              this.clinicalHomelessMedicaidReviewInput.userID = this.userData.optionUserId;
            }
          }
        }
      });
  }

  //Destroy 
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Calculate Homeless InEligible
  calculateHomelessInEligible(data: number) {
    this.homelessPath = data;
  }

  //Next Homeless Summary Button Click
  nextTab() {
    this.nextHomelessSummaryTab.emit(this.homelessPath);
  }

  //Previous Homeless Summary Button Click
  previousTab() {
    this.previousHomelessSummaryTab.emit();
  }
}
