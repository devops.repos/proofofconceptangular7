import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { MatRadioChange } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import {
  iResidentialTreatmentHistory,
  iResidentialTreatmentHistoryInput,
  SaveResidentialTreatmentHistory
} from '../homeless-review.model';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { HomelesReviewService } from '../homeless-review.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

@Component({
  selector: 'app-residential-treatment-history',
  templateUrl: './residential-treatment-history.component.html',
  styleUrls: ['./residential-treatment-history.component.scss']
})

export class ResidentialTreatmentHistoryComponent implements OnInit, OnDestroy {
  @Output() nextResidentialTreatmentHistoryTab = new EventEmitter();
  @Output() previousResidentialTreatmentHistoryTab = new EventEmitter();

  //Global Variables
  clientCategory: number = 0;

  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  homelessResidentialTreatmentFormGroup: FormGroup;

  residentialTreatmentHistory: iResidentialTreatmentHistory;
  residentialTreatmentHistoryInput: iResidentialTreatmentHistoryInput = {
    pactApplicationId: null,
    clientCategory: null,
    optionUserId: null
  }
  saveResidentialTreatmentHistory = new SaveResidentialTreatmentHistory();

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private homelessReviewService: HomelesReviewService,
    private message: ToastrService,
    private determinationSideNavService: DeterminationSideNavService) {

    //Residential Treatment Form Group
    this.homelessResidentialTreatmentFormGroup = this.formBuilder.group({
      incarceratedCtrl: ['', Validators.required],
      psychiatricallyHospitalizedCtrl: ['', Validators.required],
      currentlyInNYSPsychiatricCenterCtrl: ['', Validators.required],
      historyOfTreatmentInNYSRTFStatePFCtrl: ['', Validators.required],
      receivingSUDTreatmentCtrl: ['', Validators.required],
      leavingFosterCareCtrl: ['', Validators.required],
      inFCfor5YearsAfterAge16Ctrl: ['', Validators.required],
      yaCurrentlyIncarceratedCtrl: ['', Validators.required],
      inCRSROAPTTreatmentProgramCtrl: ['', Validators.required],
      inSkilledNursingFacilityCtrl: ['', Validators.required],
      inChildrensCommunityResidenceCtrl: ['', Validators.required],
      canReturnToPriorResidenceCtrl: ['', Validators.required],
      requiresSupportiveServicesCtrl: ['', Validators.required],
      appearsRiskOfHomelessCtrl: ['', Validators.required]
    });
  }

  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData()
      .subscribe(res => {
        if (res) {
          const data = res as iApplicationDeterminationData;
          if (data) {
            this.applicationDeterminationData = data;
            if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType) {
              this.clientCategory = this.applicationDeterminationData.clientCategoryType;
              this.residentialTreatmentHistoryInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
              this.residentialTreatmentHistoryInput.clientCategory = this.applicationDeterminationData.clientCategoryType;
              this.residentialTreatmentHistoryInput.optionUserId = this.userData.optionUserId;
              this.homelessReviewService.getResidentailTreatmentHistory(this.residentialTreatmentHistoryInput)
                .subscribe(res => {
                  if (res) {
                    const data = res as iResidentialTreatmentHistory;
                    if (data) {
                      this.populateResidentialTreatmentHistory(data);
                    }
                  }
                });
            }
          }
        }
      });
  }

  //Destroy 
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Populate Residential Treatment History
  populateResidentialTreatmentHistory(residentialTreatmentHistory: iResidentialTreatmentHistory) {
    if (residentialTreatmentHistory) {
      this.residentialTreatmentHistory = residentialTreatmentHistory;
      //Currently Incarcerated
      if (residentialTreatmentHistory.incarceratedType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['incarceratedCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.incarceratedType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['incarceratedCtrl'].setValue('N');
      }
      //Psychiatrically Hospitalized
      if (residentialTreatmentHistory.psychiatricallyHospitalizedType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['psychiatricallyHospitalizedCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.psychiatricallyHospitalizedType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['psychiatricallyHospitalizedCtrl'].setValue('N');
      }
      //Currently In NYS Psychiatric Center
      if (residentialTreatmentHistory.currentlyInNYSPsychiatricCenterType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['currentlyInNYSPsychiatricCenterCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.currentlyInNYSPsychiatricCenterType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['currentlyInNYSPsychiatricCenterCtrl'].setValue('N');
      }
      //History Of Treatment In NYS RTF State PF
      if (residentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['historyOfTreatmentInNYSRTFStatePFCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['historyOfTreatmentInNYSRTFStatePFCtrl'].setValue('N');
      }
      //Receiving SUD Treatment
      if (residentialTreatmentHistory.receivingSUDTreatmentType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['receivingSUDTreatmentCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.receivingSUDTreatmentType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['receivingSUDTreatmentCtrl'].setValue('N');
      }
      //Leaving Foster Care
      if (residentialTreatmentHistory.leavingFosterCareType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['leavingFosterCareCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.leavingFosterCareType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['leavingFosterCareCtrl'].setValue('N');
      }
      //In Foster Care For 5 Years After Age 16
      if (residentialTreatmentHistory.inFCfor5YearsAfterAge16Type === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['inFCfor5YearsAfterAge16Ctrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.inFCfor5YearsAfterAge16Type === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['inFCfor5YearsAfterAge16Ctrl'].setValue('N');
      }
      //YA Currently Incarcerated
      if (residentialTreatmentHistory.yaCurrentlyIncarceratedType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['yaCurrentlyIncarceratedCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.yaCurrentlyIncarceratedType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['yaCurrentlyIncarceratedCtrl'].setValue('N');
      }
      //In CR/SRO Apartment Treatment Program
      if (residentialTreatmentHistory.inCRSROAPTTreatmentProgramType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['inCRSROAPTTreatmentProgramCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.inCRSROAPTTreatmentProgramType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['inCRSROAPTTreatmentProgramCtrl'].setValue('N');
      }
      //In Skilled Nursing Facility
      if (residentialTreatmentHistory.inSkilledNursingFacilityType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['inSkilledNursingFacilityCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.inSkilledNursingFacilityType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['inSkilledNursingFacilityCtrl'].setValue('N');
      }
      //In Childrens Community Residence
      if (residentialTreatmentHistory.inChildrensCommunityResidenceType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['inChildrensCommunityResidenceCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.inChildrensCommunityResidenceType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['inChildrensCommunityResidenceCtrl'].setValue('N');
      }
      //Can Return To Prior Residence
      if (residentialTreatmentHistory.canReturnToPriorResidenceType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['canReturnToPriorResidenceCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.canReturnToPriorResidenceType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['canReturnToPriorResidenceCtrl'].setValue('N');
      }
      //Requires Supportive Services
      if (residentialTreatmentHistory.requiresSupportiveServicesType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['requiresSupportiveServicesCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.requiresSupportiveServicesType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['requiresSupportiveServicesCtrl'].setValue('N');
      }
      //Appears Risk Of Homeless
      if (residentialTreatmentHistory.appearsRiskOfHomelessType === 33) {
        this.homelessResidentialTreatmentFormGroup.controls['appearsRiskOfHomelessCtrl'].setValue('Y');
      }
      else if (residentialTreatmentHistory.appearsRiskOfHomelessType === 34) {
        this.homelessResidentialTreatmentFormGroup.controls['appearsRiskOfHomelessCtrl'].setValue('N');
      }
    }
  }

  //On Incarcerated Change
  onIncarceratedChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.incarceratedOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.incarceratedSource = 679;
      }
      else if (this.residentialTreatmentHistory.incarceratedOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.incarceratedSource = 679;
      }
      else {
        this.residentialTreatmentHistory.incarceratedSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Psychiatrically Hospitalized Change
  onPsychiatricallyHospitalizedChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.psychiatricallyHospitalizedOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.psychiatricallyHospitalizedSource = 679;
      }
      else if (this.residentialTreatmentHistory.psychiatricallyHospitalizedOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.psychiatricallyHospitalizedSource = 679;
      }
      else {
        this.residentialTreatmentHistory.psychiatricallyHospitalizedSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On History Of Treatment In NYS RTF State PF Change
  onHistoryOfTreatmentInNYSRTFStatePFChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFSource = 679;
      }
      else if (this.residentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFSource = 679;
      }
      else {
        this.residentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Receiving SUD Treatment Change
  onReceivingSUDTreatmentChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.receivingSUDTreatmentOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.receivingSUDTreatmentSource = 679;
      }
      else if (this.residentialTreatmentHistory.receivingSUDTreatmentOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.receivingSUDTreatmentSource = 679;
      }
      else {
        this.residentialTreatmentHistory.receivingSUDTreatmentSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Leaving Foster Care Change
  onLeavingFosterCareChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.leavingFosterCareOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.leavingFosterCareSource = 679;
      }
      else if (this.residentialTreatmentHistory.leavingFosterCareOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.leavingFosterCareSource = 679;
      }
      else {
        this.residentialTreatmentHistory.leavingFosterCareSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On In FC For 5 Years After Age 16 Change
  onInFCfor5YearsAfterAge16Change($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.inFCfor5YearsAfterAge16OrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.inFCfor5YearsAfterAge16Source = 679;
      }
      else if (this.residentialTreatmentHistory.inFCfor5YearsAfterAge16OrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.inFCfor5YearsAfterAge16Source = 679;
      }
      else {
        this.residentialTreatmentHistory.inFCfor5YearsAfterAge16Source = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On YA Currently Incarcerated Change
  onYACurrentlyIncarceratedChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.yaCurrentlyIncarceratedOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.yaCurrentlyIncarceratedSource = 679;
      }
      else if (this.residentialTreatmentHistory.yaCurrentlyIncarceratedOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.yaCurrentlyIncarceratedSource = 679;
      }
      else {
        this.residentialTreatmentHistory.yaCurrentlyIncarceratedSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //on In CR/SRO Apartment Treatment Program Change
  onInCRSROAPTTreatmentProgramChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.inCRSROAPTTreatmentProgramOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.inCRSROAPTTreatmentProgramSource = 679;
      }
      else if (this.residentialTreatmentHistory.inCRSROAPTTreatmentProgramOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.inCRSROAPTTreatmentProgramSource = 679;
      }
      else {
        this.residentialTreatmentHistory.inCRSROAPTTreatmentProgramSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On In Skilled Nursing Facility Change
  onInSkilledNursingFacilityChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.inSkilledNursingFacilityOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.inSkilledNursingFacilitySource = 679;
      }
      else if (this.residentialTreatmentHistory.inSkilledNursingFacilityOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.inSkilledNursingFacilitySource = 679;
      }
      else {
        this.residentialTreatmentHistory.inSkilledNursingFacilitySource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On In Childrens Community Residence Change
  onInChildrensCommunityResidenceChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.inChildrensCommunityResidenceOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.inChildrensCommunityResidenceSource = 679;
      }
      else if (this.residentialTreatmentHistory.inChildrensCommunityResidenceOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.inChildrensCommunityResidenceSource = 679;
      }
      else {
        this.residentialTreatmentHistory.inChildrensCommunityResidenceSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //On Appears Risk Of Homeless Change
  onAppearsRiskOfHomelessChange($event: MatRadioChange) {
    if (this.residentialTreatmentHistory) {
      if (this.residentialTreatmentHistory.appearsRiskOfHomelessOrigType === 33 && $event.value === "Y") {
        this.residentialTreatmentHistory.appearsRiskOfHomelessSource = 679;
      }
      else if (this.residentialTreatmentHistory.appearsRiskOfHomelessOrigType === 34 && $event.value === "N") {
        this.residentialTreatmentHistory.appearsRiskOfHomelessSource = 679;
      }
      else {
        this.residentialTreatmentHistory.appearsRiskOfHomelessSource = 680;
      }
    }
    this.setNextTab(false);
    this.disableNextSideNavigation(true);
  }

  //Set Next Tab Value
  setNextTab(value: boolean) {
    this.homelessReviewService.setHomelessAtRiskTabEnabled(value);
    this.homelessReviewService.setHomelessSummaryTabEnabled(value);
    this.homelessReviewService.setHomelessReportTabEnabled(value);
  }

  //Disable Next Side Navigation
  disableNextSideNavigation(value: boolean) {
    this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(value);
    this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(value);
    this.determinationSideNavService.setIsDeterminationSummaryDisabled(value);
    this.determinationSideNavService.setIsSignOffFollowUpDisabled(value);
  }

  //Validate Residential Treatment History
  validateResidentialTreatmentHistory(clientCategory: number): boolean {
    if (clientCategory === 671) {
      if (this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Incarcerated is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Psychiatrically Hospitalized is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Receiving SUD Treatment is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inCRSROAPTTreatmentProgramCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In CR/SRO Apartment Treatment Program is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In Skilled Nursing Facility is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Appears Risk Of Homeless is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 672) {
      if (this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Incarcerated is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Psychiatrically Hospitalized is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('historyOfTreatmentInNYSRTFStatePFCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("History Of Treatment In NYS/RTF State Psychiatric Facility is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Receiving SUD Treatment is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Leaving Foster Care is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inFCfor5YearsAfterAge16Ctrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In Forster Care For 5 Years After Age 16 is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('yaCurrentlyIncarceratedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Young Adult Currently Incarcerated is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inCRSROAPTTreatmentProgramCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In CR/SRO Apartment Treatment Program is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In Skilled Nursing Facility is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inChildrensCommunityResidenceCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In Childrens Community Residence is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Appears Risk Of Homeless is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 673) {
      if (this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Incarcerated is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Psychiatrically Hospitalized is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Receiving SUD Treatment is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In Skilled Nursing Facility is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Appears Risk Of Homeless is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 674) {
      if (this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Incarcerated is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Currently Psychiatrically Hospitalized is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Receiving SUD Treatment is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In Skilled Nursing Facility is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Leaving Foster Care is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Appears Risk Of Homeless is required.");
        }
        return false;
      }
      return true;
    }
    else if (clientCategory === 676) {
      if (this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Leaving Foster Care is required.");
        }
        return false;
      }
      if (this.homelessResidentialTreatmentFormGroup.get('inFCfor5YearsAfterAge16Ctrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("In Forster Care For 5 Years After Age 16 is required.");
        }
        return false;
      }
      return true;
    }
    else {
      return false;
    }
  }

  //Next Residential Treatment History Button Click
  nextTab() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType) {
      if (this.validateResidentialTreatmentHistory(this.applicationDeterminationData.clientCategoryType)) {
        this.saveResidentialTreatmentHistoryDetails(2);
      }
    }
    else {
      throw new Error("Invalid Client Category!");
    }
  }

  //Previous Residential Treatment History Button Click
  previousTab() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType) {
      if (this.validateResidentialTreatmentHistory(this.applicationDeterminationData.clientCategoryType)) {
        this.saveResidentialTreatmentHistoryDetails(3);
      }
    }
    else {
      throw new Error("Invalid Client Category!");
    }
  }

  //Save Residential Treatment History Data
  saveResidentialTreatmentHistoryData() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType) {
      if (this.validateResidentialTreatmentHistory(this.applicationDeterminationData.clientCategoryType)) {
        this.saveResidentialTreatmentHistoryDetails(1);
      }
    }
    else {
      throw new Error("Invalid Client Category!");
    }
  }

  //Save Residential Treatment History Details
  saveResidentialTreatmentHistoryDetails(path: number) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId && this.applicationDeterminationData.clientCategoryType && this.residentialTreatmentHistory) {
      this.saveResidentialTreatmentHistory.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.saveResidentialTreatmentHistory.clientCategory = this.applicationDeterminationData.clientCategoryType;
      this.saveResidentialTreatmentHistory.optionUserId = this.userData.optionUserId;
      if (this.applicationDeterminationData.clientCategoryType === 671) {
        this.saveResidentialTreatmentHistory.incarceratedType = this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.incarceratedSource = this.residentialTreatmentHistory.incarceratedSource;
        this.saveResidentialTreatmentHistory.psychiatricallyHospitalizedType = this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.psychiatricallyHospitalizedSource = this.residentialTreatmentHistory.psychiatricallyHospitalizedSource;
        this.saveResidentialTreatmentHistory.receivingSUDTreatmentType = this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.receivingSUDTreatmentSource = this.residentialTreatmentHistory.receivingSUDTreatmentSource;
        this.saveResidentialTreatmentHistory.inCRSROAPTTreatmentProgramType = this.homelessResidentialTreatmentFormGroup.get('inCRSROAPTTreatmentProgramCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inCRSROAPTTreatmentProgramCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inCRSROAPTTreatmentProgramSource = this.residentialTreatmentHistory.inCRSROAPTTreatmentProgramSource;
        this.saveResidentialTreatmentHistory.inSkilledNursingFacilityType = this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inSkilledNursingFacilitySource = this.residentialTreatmentHistory.inSkilledNursingFacilitySource;
        this.saveResidentialTreatmentHistory.appearsRiskOfHomelessType = this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.appearsRiskOfHomelessSource = this.residentialTreatmentHistory.appearsRiskOfHomelessSource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 672) {
        this.saveResidentialTreatmentHistory.incarceratedType = this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.incarceratedSource = this.residentialTreatmentHistory.incarceratedSource;
        this.saveResidentialTreatmentHistory.psychiatricallyHospitalizedType = this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.psychiatricallyHospitalizedSource = this.residentialTreatmentHistory.psychiatricallyHospitalizedSource;
        this.saveResidentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFType = this.homelessResidentialTreatmentFormGroup.get('historyOfTreatmentInNYSRTFStatePFCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('historyOfTreatmentInNYSRTFStatePFCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFSource = this.residentialTreatmentHistory.historyOfTreatmentInNYSRTFStatePFSource;
        this.saveResidentialTreatmentHistory.receivingSUDTreatmentType = this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.receivingSUDTreatmentSource = this.residentialTreatmentHistory.receivingSUDTreatmentSource;
        this.saveResidentialTreatmentHistory.leavingFosterCareType = this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.leavingFosterCareSource = this.residentialTreatmentHistory.leavingFosterCareSource;
        this.saveResidentialTreatmentHistory.inFCfor5YearsAfterAge16Type = this.homelessResidentialTreatmentFormGroup.get('inFCfor5YearsAfterAge16Ctrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inFCfor5YearsAfterAge16Ctrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inFCfor5YearsAfterAge16Source = this.residentialTreatmentHistory.inFCfor5YearsAfterAge16Source;
        this.saveResidentialTreatmentHistory.yaCurrentlyIncarceratedType = this.homelessResidentialTreatmentFormGroup.get('yaCurrentlyIncarceratedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('yaCurrentlyIncarceratedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.yaCurrentlyIncarceratedSource = this.residentialTreatmentHistory.yaCurrentlyIncarceratedSource;
        this.saveResidentialTreatmentHistory.inCRSROAPTTreatmentProgramType = this.homelessResidentialTreatmentFormGroup.get('inCRSROAPTTreatmentProgramCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inCRSROAPTTreatmentProgramCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inCRSROAPTTreatmentProgramSource = this.residentialTreatmentHistory.inCRSROAPTTreatmentProgramSource;
        this.saveResidentialTreatmentHistory.inSkilledNursingFacilityType = this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inSkilledNursingFacilitySource = this.residentialTreatmentHistory.inSkilledNursingFacilitySource;
        this.saveResidentialTreatmentHistory.inChildrensCommunityResidenceType = this.homelessResidentialTreatmentFormGroup.get('inChildrensCommunityResidenceCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inChildrensCommunityResidenceCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inChildrensCommunityResidenceSource = this.residentialTreatmentHistory.inChildrensCommunityResidenceSource;
        this.saveResidentialTreatmentHistory.appearsRiskOfHomelessType = this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.appearsRiskOfHomelessSource = this.residentialTreatmentHistory.appearsRiskOfHomelessSource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 673) {
        this.saveResidentialTreatmentHistory.incarceratedType = this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.incarceratedSource = this.residentialTreatmentHistory.incarceratedSource;
        this.saveResidentialTreatmentHistory.psychiatricallyHospitalizedType = this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.psychiatricallyHospitalizedSource = this.residentialTreatmentHistory.psychiatricallyHospitalizedSource;
        this.saveResidentialTreatmentHistory.receivingSUDTreatmentType = this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.receivingSUDTreatmentSource = this.residentialTreatmentHistory.receivingSUDTreatmentSource;
        this.saveResidentialTreatmentHistory.inSkilledNursingFacilityType = this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inSkilledNursingFacilitySource = this.residentialTreatmentHistory.inSkilledNursingFacilitySource;
        this.saveResidentialTreatmentHistory.appearsRiskOfHomelessType = this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.appearsRiskOfHomelessSource = this.residentialTreatmentHistory.appearsRiskOfHomelessSource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 674) {
        this.saveResidentialTreatmentHistory.incarceratedType = this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('incarceratedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.incarceratedSource = this.residentialTreatmentHistory.incarceratedSource;
        this.saveResidentialTreatmentHistory.psychiatricallyHospitalizedType = this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('psychiatricallyHospitalizedCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.psychiatricallyHospitalizedSource = this.residentialTreatmentHistory.psychiatricallyHospitalizedSource;
        this.saveResidentialTreatmentHistory.receivingSUDTreatmentType = this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('receivingSUDTreatmentCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.receivingSUDTreatmentSource = this.residentialTreatmentHistory.receivingSUDTreatmentSource;
        this.saveResidentialTreatmentHistory.inSkilledNursingFacilityType = this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inSkilledNursingFacilityCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inSkilledNursingFacilitySource = this.residentialTreatmentHistory.inSkilledNursingFacilitySource;
        this.saveResidentialTreatmentHistory.leavingFosterCareType = this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.leavingFosterCareSource = this.residentialTreatmentHistory.leavingFosterCareSource;
        this.saveResidentialTreatmentHistory.appearsRiskOfHomelessType = this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('appearsRiskOfHomelessCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.appearsRiskOfHomelessSource = this.residentialTreatmentHistory.appearsRiskOfHomelessSource;
      }
      else if (this.applicationDeterminationData.clientCategoryType === 676) {
        this.saveResidentialTreatmentHistory.leavingFosterCareType = this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('leavingFosterCareCtrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.leavingFosterCareSource = this.residentialTreatmentHistory.leavingFosterCareSource;
        this.saveResidentialTreatmentHistory.inFCfor5YearsAfterAge16Type = this.homelessResidentialTreatmentFormGroup.get('inFCfor5YearsAfterAge16Ctrl').value === 'Y' ? 33 : this.homelessResidentialTreatmentFormGroup.get('inFCfor5YearsAfterAge16Ctrl').value === 'N' ? 34 : null;
        this.saveResidentialTreatmentHistory.inFCfor5YearsAfterAge16Source = this.residentialTreatmentHistory.inFCfor5YearsAfterAge16Source;
      }
      this.homelessReviewService.saveResidentialTreatmentHistory(this.saveResidentialTreatmentHistory)
        .subscribe(res => {
          if (res) {
            if (path === 1) {
              if (!this.message.currentlyActive) {
                this.message.success("Residential treatment history form updated successfully!");
              }
            }
            else if (path === 2) {
              this.nextResidentialTreatmentHistoryTab.emit();
            }
            else if (path === 3) {
              this.previousResidentialTreatmentHistoryTab.emit();
            }
            else {
              return;
            }
          }
        });
    }
  }
}
