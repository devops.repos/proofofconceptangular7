import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

//Model References
import { DETClinicalHomelessMedicaidReviewInput, DETMentalHealthCondition, DETSubstanceUse, DETMedicalCondition
         , DETClinicalAtRisk } from './clinical-review.model';



@Injectable({
  providedIn: 'root'
})
export class ClinicalReviewService {
  //Mental Health Condition
  getDETMentalHealthConditionUrl = environment.pactApiUrl + 'DETClinicalReview/GetDETMentalHealthCondition';
  saveDETMentalHealthConditionUrl = environment.pactApiUrl + 'DETClinicalReview/SaveDETMentalHealthCondition';

  //Substance Use
  getDETSubstanceUseUrl = environment.pactApiUrl + 'DETClinicalReview/GetDETSubstanceUse';
  saveDETSubstanceUseUrl = environment.pactApiUrl + 'DETClinicalReview/SaveDETSubstanceUse';

  //Medical Condition
  getDETMedicalConditionUrl = environment.pactApiUrl + 'DETClinicalReview/GetDETMedicalCondition';
  saveDETMedicalConditionUrl = environment.pactApiUrl + 'DETClinicalReview/SaveDETMedicalCondition';

  //At Risk
  getDETClinicalAtRiskUrl = environment.pactApiUrl + 'DETClinicalReview/GetDETClinicalAtRisk';
  saveDETClinicalAtRiskUrl = environment.pactApiUrl + 'DETClinicalReview/SaveDETClinicalAtRisk';

  //Clinical Summary
  //getDETClinicalHomelessMedicaidSummaryUrl = environment.pactApiUrl + 'DETClinicalReview/GetDETClinicalHomelessMedicaidSummary';
    
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),
    response : "json",
  };
  
  constructor(private httpClient: HttpClient) { }

  //Mental Health Condition
  getDETMentalHealthCondition(detClinicalReviewInput : DETClinicalHomelessMedicaidReviewInput) : Observable<any>
  {
     return this.httpClient.post(this.getDETMentalHealthConditionUrl, JSON.stringify(detClinicalReviewInput), this.httpOptions);
  }

  saveDETMentalHealthCondition(detMentalHealthCondition : DETMentalHealthCondition) : Observable<any>
  {
    return this.httpClient.post(this.saveDETMentalHealthConditionUrl, JSON.stringify(detMentalHealthCondition), this.httpOptions);
  }

  //Substance Use
  getDETSubstanceUse(detClinicalReviewInput : DETClinicalHomelessMedicaidReviewInput) : Observable<any>
  {
     return this.httpClient.post(this.getDETSubstanceUseUrl, JSON.stringify(detClinicalReviewInput), this.httpOptions);
  }

  saveDETSubstanceUse(detSubstanceUse : DETSubstanceUse) : Observable<any>
  {
    return this.httpClient.post(this.saveDETSubstanceUseUrl, JSON.stringify(detSubstanceUse), this.httpOptions);
  }
  
  //Medical Condition
  getDETClinicalAtRisk(detClinicalReviewInput : DETClinicalHomelessMedicaidReviewInput) : Observable<any>
  {
     return this.httpClient.post(this.getDETClinicalAtRiskUrl, JSON.stringify(detClinicalReviewInput), this.httpOptions);
  }

  saveDETMedicalCondition(detMedicalCondition : DETMedicalCondition) : Observable<any>
  {
    return this.httpClient.post(this.saveDETMedicalConditionUrl, JSON.stringify(detMedicalCondition), this.httpOptions);
  }

  //Medical Condition
  getDETMedicalCondition(detClinicalReviewInput : DETClinicalHomelessMedicaidReviewInput) : Observable<any>
  {
     return this.httpClient.post(this.getDETMedicalConditionUrl, JSON.stringify(detClinicalReviewInput), this.httpOptions);
  }

  saveDETClinicalAtRisk(detClinicalAtRisk : DETClinicalAtRisk) : Observable<any>
  {
    return this.httpClient.post(this.saveDETClinicalAtRiskUrl, JSON.stringify(detClinicalAtRisk), this.httpOptions);
  }

  // //Clinical Summary
  // getDETClinicalHomelessMedicaidSummary(detClinicalReviewInput : DETClinicalHomelessMedicaidReviewInput) : Observable<any>
  // {
  //    return this.httpClient.post(this.getDETClinicalHomelessMedicaidSummaryUrl, JSON.stringify(detClinicalReviewInput), this.httpOptions);
  // }
}

