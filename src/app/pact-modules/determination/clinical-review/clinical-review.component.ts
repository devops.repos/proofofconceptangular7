import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';

//Services
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ClinicalReviewService } from '../clinical-review/clinical-review.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { ClinicalAssessmentService } from 'src/app/pact-modules/supportiveHousingSystem/clinical-assessment/clinical-assessment.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

//Model
import { PACTClinicalMHRDiagnosisDetails } from 'src/app/shared/diagnosis/diagnosis.model';
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model';
import {
  DETClinicalHomelessMedicaidReviewInput, DETMentalHealthCondition, DETSubstanceUse
  , DETMedicalCondition, DETClinicalAtRisk
} from '../clinical-review/clinical-review.model';
import { iHousingDeterminationDocumentsData } from 'src/app/models/determination-documents.model';
import { ClinicalAssessmentModel } from 'src/app/pact-modules/supportiveHousingSystem/clinical-assessment/clinical-assessment.model';
import { DETTabTimeDuration } from 'src/app/models/determination-common.model';

@Component({
  selector: 'app-clinical-review',
  templateUrl: './clinical-review.component.html',
  styleUrls: ['./clinical-review.component.scss']
})

export class ClinicalReviewComponent implements OnInit, OnDestroy {

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  clinicalReviewDataSub: Subscription;
  clinicalAssessmentDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;
  detClinicalReviewInput: DETClinicalHomelessMedicaidReviewInput = {
    pactApplicationID: null, summaryType: null, clientCategoryType: null, userID: null
  };
  clientCategoryType: number;
  summaryType: number = 611; //611 - Clinical
  supportNotOfferedByGenPopType: number;
  detTabTimeDuration: DETTabTimeDuration = {
    pactApplicationId: null, tabInName: null
    , tabOutName: null, optionUserId: null
  };

  currentTab: number = 0;
  selectedTab: number = 0;
  isClinicalPreDeterminationTabCompleted: boolean = false;
  isMentalHealthCompleted: boolean = false;
  isSubstanceUseCompleted: boolean = false;
  isMedicalConditionCompleted: boolean = false;
  isClinicalAtRiskCompleted: boolean = false;
  isClinicalSummaryCompleted: boolean = false;
  isSaveButtonVisible: boolean = true;

  isMentalHealthConditionTabEnabled: boolean = false;
  isSubstanceUseTabEnabled: boolean = false;
  isMedicalConditionTabEnabled: boolean = false;
  isClinicalAtRiskTabEnabled: boolean = false;
  isClinicalSummaryTabEnabled: boolean = false;

  isClinicalInEligible: boolean;

  //Diagnosis
  medicalDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 48, historyOf: true, riskOf: true, provisional: false };
  principalDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 49, historyOf: true, riskOf: true, provisional: true };
  otherDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 51, historyOf: false, riskOf: true, provisional: false };
  mdList: PACTClinicalMHRDiagnosisDetails[] = [];
  pdList: PACTClinicalMHRDiagnosisDetails[] = [];
  odList: PACTClinicalMHRDiagnosisDetails[] = [];
  diagnosisList: PACTClinicalMHRDiagnosisDetails[] = [];
  diagnosisSearchList: PACTClinicalMHRDiagnosisDetails[] = [];
  hasMedicalDiagnosis: boolean;
  hasPrincipalDiagnosis: boolean;
  hasOtherDiagnosis: boolean;

  //Documents 
  mentalHealthDocuments: iHousingDeterminationDocumentsData = { pactApplicationId: null, documentType: [] };
  substanceUseDocuments: iHousingDeterminationDocumentsData = { pactApplicationId: null, documentType: [] };
  medicalDocuments: iHousingDeterminationDocumentsData = { pactApplicationId: null, documentType: [] };
  pactApplicationId: number = 0;

  //Mental Health Fields
  mentalHealthGroup: FormGroup;
  detMentalHealthCondition: DETMentalHealthCondition;
  detMentalHealthConditionId: number;
  significantFunctionalImpairmentExplainSourceText: string;
  stabilizedForPlacementExplainSourceText: string;

  currentMHDiagnosisSourceText: string;
  currentMHDiagnosisOrigText: string;
  currentMHDiagnosisOrConditionOrigText: string;
  isCurrentMHDiagnosisOrConditionDisabled: boolean = false;
  currentMHDiagnosisOrConditionSourceText: string;
  significantFunctionalImpairmentSourceText: string;
  isSignificantFunctionalImpairmentDisabled: boolean = false;
  marginalFunctionalImpairmentPsychiatricSourceText: string;
  isMarginalFunctionalImpairmentPsychiatricDisabled: boolean = false;
  marginalFunctionalImpairmentDevelopmentalSourceText: string;
  isMarginalFunctionalImpairmentDevelopmentalDisabled: boolean = false;
  excludingSUDNonSPMISourceText: string;
  isExcludingSUDNonSPMIDisabled: boolean = false;
  seriousMentalIllnessSourceText: string;
  isSeriousMentalIllnessDisabled: boolean = false;
  seriousMentalIllnessOrigText: string;
  haveSEDDiagnosedBefore18SourceText: string;
  isHaveSEDDiagnosedBefore18Disabled: boolean = false;
  haveSEDDiagnosedBefore18OrigText: string;
  stabilizedForPlacementSourceText: string;
  isStabilizedForPlacementDisabled: boolean = false;
  developmentalDisabilityOrigText: string;
  isDevelopmentalDisabilityDisabled: boolean = false;
  developmentalDisabilitySourceText: string;

  //Substance Use Fields
  substanceUseGroup: FormGroup;
  detSubstanceUse: DETSubstanceUse;
  detSubstanceUseId: number;
  haveSUDOrigText: string;
  haveSUDSourceText: string;
  usedSubstancesPast3MonthsOrigText: string;
  usedSubstancesPast3MonthsSourceText: string;
  isUsedSubstancesPast3MonthsDisabled: boolean = false;
  sudEvidencePast5YearsSourceText: string;
  isSudEvidencePast5YearsDisabled: boolean = false;
  sudRestrictionOfADLSourceText: string;
  sudRestrictionOfADLExlpainSourceText: string;
  isSudRestrictionOfADLDisabled: boolean = false;
  sudTreatmentCompletionSourceText: string;
  isSudTreatmentCompletionDisabled: boolean = false;

  //Medical Condition Fields
  medicalConditionGroup: FormGroup;
  detMedicalCondition: DETMedicalCondition;
  detMedicalConditionId: number;
  haveHIVAIDSDocumentedOrigText: string;
  isActiveInHASASourceText: string;
  isActiveInHASADisabled: boolean = false;
  haveHIVAIDSDocumentedSourceText: string;
  isHaveHIVAIDSDocumentedDisabled: boolean = false;
  haveCurrentMedicalDiagnosisSourceText: string;
  isHaveCurrentMedicalDiagnosisDisabled: boolean = false;
  restrictionOfADLSourceText: string;
  isRestrictionOfADLDisabled: boolean = false;
  haveChronicMedicalConditionSourceText: string;
  isHaveChronicMedicalConditionDisabled: boolean = false;
  marginalFunctionalImpairmentMedicalSourceText: string;
  isMarginalFunctionalImpairmentMedicalDisabled: boolean = false;
  haveNeuroCognitiveImpairmentOrigText: string;
  isHaveNeuroCognitiveImpairmentDisabled: boolean = false;
  haveNeuroCognitiveImpairmentSourceText: string;
  marginalFuntionalImpairmentNeuroSourceText: string;
  isMarginalFuntionalImpairmentNeuroDisabled: boolean = false;

  //At Risk Fields
  clinicalAtRiskGroup: FormGroup;
  detClinicalAtRisk: DETClinicalAtRisk;
  detClinicalAtRiskId: number;
  isFunctionalImpairmentsMarkedSourceText: string;
  haveLimitedEducationSourceText: string;
  isHaveLimitedEducationDisabled: boolean = false;
  haveLimitedEmpHistorySourceText: string;
  isHaveLimitedEmpHistoryDisabled: boolean = false;
  haveHistoryOfTraumaSourceText: string;
  isHaveHistoryOfTraumaDisabled: boolean = false;
  isInvolvedinTwoSystemsSourceText: string;
  isInvolvedinTwoSystemsDisabled: boolean = false;
  otherRiskFactorIssuesSourceText: string;
  isOtherRiskFactorIssuesDisabled: boolean = false;
  otherRiskFactorIssuesExplainSourceText: string;

  isFunctionalImpairmentsMarkedOrigText: string;
  haveLimitedEducationOrigDescription: string;
  haveLimitedEmpHistoryOrigDescription: string;
  haveHistoryOfTraumaOrigText: string;
  isInvolvedinTwoSystemsOrigText: string;


  constructor(private formBuilder: FormBuilder
    , private toastrService: ToastrService
    , private route: ActivatedRoute
    , private userService: UserService
    , private clinicalReviewService: ClinicalReviewService
    , private applicationDeterminationService: ApplicationDeterminationService
    , private clinicalAssessmentService: ClinicalAssessmentService
    , private commonService: CommonService
    , private router: Router
    , private confirmDialogService: ConfirmDialogService
    , private determinationSideNavService: DeterminationSideNavService
  ) { }


  //Save Data on page refresh
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.saveTabData(0, this.selectedTab);
  };

  // ngOnInit() {
  //   //Mental Health Tab Form Fields
  //   this.mentalHealthGroup = this.formBuilder.group({
  //     currentMHDiagnosisOrig: [''],
  //     currentMHDiagnosis: [''],
  //     currentMHDiagnosisSource: [''],
  //     currentMHDiagnosisOrConditionOrig: [''],
  //     currentMHDiagnosisOrCondition: [''],
  //     currentMHDiagnosisOrConditionSource: [''],
  //     significantFunctionalImpairmentOrig: [''],
  //     significantFunctionalImpairment: [''],
  //     significantFunctionalImpairmentSource: [''],
  //     significantFunctionalImpairmentExplain: [''],
  //     marginalFunctionalImpairmentPsychiatricOrig: [''],
  //     marginalFunctionalImpairmentPsychiatric: [''],
  //     marginalFunctionalImpairmentPsychiatricSource: [''],
  //     marginalFunctionalImpairmentDevelopmentalOrig: [''],
  //     marginalFunctionalImpairmentDevelopmental: [''],
  //     marginalFunctionalImpairmentDevelopmentalSource: [''],
  //     excludingSUDNonSPMIOrig: [''],
  //     excludingSUDNonSPMI: [''],
  //     excludingSUDNonSPMISource: [''],
  //     seriousMentalIllnessOrig: [''],
  //     seriousMentalIllness: [''],
  //     seriousMentalIllnessSource: [''],
  //     haveSEDDiagnosedBefore18Orig: [''],
  //     haveSEDDiagnosedBefore18: [''],
  //     haveSEDDiagnosedBefore18Source: [''],
  //     stabilizedForPlacementOrig: [''],
  //     stabilizedForPlacement: [''],
  //     stabilizedForPlacementSource: [''],
  //     stabilizedForPlacementExplain: [''],
  //     developmentalDisabilityOrig: [''],
  //     developmentalDisability: [''],
  //     developmentalDisabilitySource: [''],
  //     isMentalHealthConditionCompleted: [''],
  //   });

  //   //Substance Use Tab Form Fields
  //   this.substanceUseGroup = this.formBuilder.group({
  //     haveSUDOrig: [''],
  //     haveSUD: [''],
  //     haveSUDSource: [''],
  //     usedSubstancesPast3MonthsOrig: [''],
  //     usedSubstancesPast3Months: [''],
  //     usedSubstancesPast3MonthsSource: [''],
  //     sudEvidencePast5YearsOrig: [''],
  //     sudEvidencePast5Years: [''],
  //     sudEvidencePast5YearsSource: [''],
  //     sudRestrictionOfADLOrig: [''],
  //     sudRestrictionOfADL: [''],
  //     sudRestrictionOfADLSource: [''],
  //     sudRestrictionOfADLExplain: [''],
  //     sudTreatmentCompletionOrig: [''],
  //     sudTreatmentCompletion: [''],
  //     sudTreatmentCompletionSource: [''],
  //     isSubstanceUseCompleted: ['']
  //   });

  //   //Medical Condition Tab Form Fields
  //   this.medicalConditionGroup = this.formBuilder.group({
  //     isActiveInHASAOrig: [''],
  //     isActiveInHASA: [''],
  //     isActiveHASASource: [''],
  //     haveHIVAIDSDocumentedOrig: [''],
  //     haveHIVAIDSDocumented: [''],
  //     haveHIVAIDSDocumentedSource: [''],
  //     haveCurrentMedicalDiagnosisOrig: [''],
  //     haveCurrentMedicalDiagnosis: [''],
  //     haveCurrentMedicalDiagnosisSource: [''],
  //     restrictionOfADLOrig: [''],
  //     restrictionOfADL: [''],
  //     restrictionOfADLSource: [''],
  //     haveChronicMedicalConditionOrig: [''],
  //     haveChronicMedicalCondition: [''],
  //     haveChronicMedicalConditionSource: [''],
  //     marginalFunctionalImpairmentMedicalOrig: [''],
  //     marginalFunctionalImpairmentMedical: [''],
  //     marginalFunctionalImpairmentMedicalSource: [''],
  //     haveNeuroCognitiveImpairmentOrig: [''],
  //     haveNeuroCognitiveImpairment: [''],
  //     haveNeuroCognitiveImpairmentSource: [''],
  //     marginalFuntionalImpairmentNeuroOrig: [''],
  //     marginalFuntionalImpairmentNeuro: [''],
  //     marginalFuntionalImpairmentNeuroSource: [''],
  //     isMedicalConditionCompleted: [''],
  //   });

  //   //Clinical At Risk Tab Form Fields
  //   this.clinicalAtRiskGroup = this.formBuilder.group({
  //     isFunctionalImpairmentsMarkedOrig: [''],
  //     isFunctionalImpairmentsMarked: [''],
  //     isFunctionalImpairmentsMarkedSource: [''],
  //     haveLimitedEducationOrig: [''],
  //     haveLimitedEducation: [''],
  //     haveLimitedEducationSource: [''],
  //     haveLimitedEmpHistoryOrig: [''],
  //     haveLimitedEmpHistory: [''],
  //     haveLimitedEmpHistorySource: [''],
  //     haveHistoryOfTraumaOrig: [''],
  //     haveHistoryOfTrauma: [''],
  //     haveHistoryOfTraumaSource: [''],
  //     isInvolvedinTwoSystemsOrig: [''],
  //     isInvolvedinTwoSystems: [''],
  //     isInvolvedinTwoSystemsSource: [''],
  //     otherRiskFactorIssuesOrig: [''],
  //     otherRiskFactorIssues: [''],
  //     otherRiskFactorIssuesSource: [''],
  //     otherRiskFactorIssuesExplain: [''],
  //     isClinicalAtRiskCompleted: [''],
  //   });

  //   //Get User Data
  //   this.userDataSub = this.userService.getUserData()
  //     .subscribe(res => {
  //       if (res)
  //         this.userData = res;
  //     });

  //   //Side Navigation Service For Determination
  //   this.activatedRouteSub = this.route.paramMap.subscribe(param => {
  //     if (param) {
  //       const paramApplicationId = param.get('applicationId');
  //       if (paramApplicationId) {
  //         const numericApplicationId = parseInt(paramApplicationId);
  //         if (isNaN(numericApplicationId)) {
  //           throw new Error("Invalid application number!");
  //         } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
  //           this.applicationDeterminationService.setApplicationDataForDeterminationWithApplicationId(numericApplicationId)
  //             .subscribe(data => {
  //               this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
  //               this.applicationDeterminationData = data as iApplicationDeterminationData;
  //               this.applicationDeterminationService.setApplicationDeterminationData(this.applicationDeterminationData);
  //               this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
  //               this.populateInitData();
  //             },
  //               error => {
  //                 throw new Error("There is an error while getting Application Determination Data.");
  //               });
  //         }
  //         else {
  //           return;
  //         }
  //       }
  //     }
  //   });
  // }

  ngOnInit() {
    //Mental Health Tab Form Fields
    this.mentalHealthGroup = this.formBuilder.group({
      currentMHDiagnosisOrig: [''],
      currentMHDiagnosis: [''],
      currentMHDiagnosisSource: [''],
      currentMHDiagnosisOrConditionOrig: [''],
      currentMHDiagnosisOrCondition: [''],
      currentMHDiagnosisOrConditionSource: [''],
      significantFunctionalImpairmentOrig: [''],
      significantFunctionalImpairment: [''],
      significantFunctionalImpairmentSource: [''],
      significantFunctionalImpairmentExplain: [''],
      marginalFunctionalImpairmentPsychiatricOrig: [''],
      marginalFunctionalImpairmentPsychiatric: [''],
      marginalFunctionalImpairmentPsychiatricSource: [''],
      marginalFunctionalImpairmentDevelopmentalOrig: [''],
      marginalFunctionalImpairmentDevelopmental: [''],
      marginalFunctionalImpairmentDevelopmentalSource: [''],
      excludingSUDNonSPMIOrig: [''],
      excludingSUDNonSPMI: [''],
      excludingSUDNonSPMISource: [''],
      seriousMentalIllnessOrig: [''],
      seriousMentalIllness: [''],
      seriousMentalIllnessSource: [''],
      haveSEDDiagnosedBefore18Orig: [''],
      haveSEDDiagnosedBefore18: [''],
      haveSEDDiagnosedBefore18Source: [''],
      stabilizedForPlacementOrig: [''],
      stabilizedForPlacement: [''],
      stabilizedForPlacementSource: [''],
      stabilizedForPlacementExplain: [''],
      developmentalDisabilityOrig: [''],
      developmentalDisability: [''],
      developmentalDisabilitySource: [''],
      isMentalHealthConditionCompleted: [''],
    });

    //Substance Use Tab Form Fields
    this.substanceUseGroup = this.formBuilder.group({
      haveSUDOrig: [''],
      haveSUD: [''],
      haveSUDSource: [''],
      usedSubstancesPast3MonthsOrig: [''],
      usedSubstancesPast3Months: [''],
      usedSubstancesPast3MonthsSource: [''],
      sudEvidencePast5YearsOrig: [''],
      sudEvidencePast5Years: [''],
      sudEvidencePast5YearsSource: [''],
      sudRestrictionOfADLOrig: [''],
      sudRestrictionOfADL: [''],
      sudRestrictionOfADLSource: [''],
      sudRestrictionOfADLExplain: [''],
      sudTreatmentCompletionOrig: [''],
      sudTreatmentCompletion: [''],
      sudTreatmentCompletionSource: [''],
      isSubstanceUseCompleted: ['']
    });

    //Medical Condition Tab Form Fields
    this.medicalConditionGroup = this.formBuilder.group({
      isActiveInHASAOrig: [''],
      isActiveInHASA: [''],
      isActiveHASASource: [''],
      haveHIVAIDSDocumentedOrig: [''],
      haveHIVAIDSDocumented: [''],
      haveHIVAIDSDocumentedSource: [''],
      haveCurrentMedicalDiagnosisOrig: [''],
      haveCurrentMedicalDiagnosis: [''],
      haveCurrentMedicalDiagnosisSource: [''],
      restrictionOfADLOrig: [''],
      restrictionOfADL: [''],
      restrictionOfADLSource: [''],
      haveChronicMedicalConditionOrig: [''],
      haveChronicMedicalCondition: [''],
      haveChronicMedicalConditionSource: [''],
      marginalFunctionalImpairmentMedicalOrig: [''],
      marginalFunctionalImpairmentMedical: [''],
      marginalFunctionalImpairmentMedicalSource: [''],
      haveNeuroCognitiveImpairmentOrig: [''],
      haveNeuroCognitiveImpairment: [''],
      haveNeuroCognitiveImpairmentSource: [''],
      marginalFuntionalImpairmentNeuroOrig: [''],
      marginalFuntionalImpairmentNeuro: [''],
      marginalFuntionalImpairmentNeuroSource: [''],
      isMedicalConditionCompleted: [''],
    });

    //Clinical At Risk Tab Form Fields
    this.clinicalAtRiskGroup = this.formBuilder.group({
      isFunctionalImpairmentsMarkedOrig: [''],
      isFunctionalImpairmentsMarked: [''],
      isFunctionalImpairmentsMarkedSource: [''],
      haveLimitedEducationOrig: [''],
      haveLimitedEducation: [''],
      haveLimitedEducationSource: [''],
      haveLimitedEmpHistoryOrig: [''],
      haveLimitedEmpHistory: [''],
      haveLimitedEmpHistorySource: [''],
      haveHistoryOfTraumaOrig: [''],
      haveHistoryOfTrauma: [''],
      haveHistoryOfTraumaSource: [''],
      isInvolvedinTwoSystemsOrig: [''],
      isInvolvedinTwoSystems: [''],
      isInvolvedinTwoSystemsSource: [''],
      otherRiskFactorIssuesOrig: [''],
      otherRiskFactorIssues: [''],
      otherRiskFactorIssuesSource: [''],
      otherRiskFactorIssuesExplain: [''],
      isClinicalAtRiskCompleted: [''],
    });

    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination1(numericApplicationId).then(flag => {
              if (flag) {
                //Get User Data
                this.userDataSub = this.userService.getUserData().subscribe(res => {
                  if (res) {
                    this.userData = res;
                    //Getting the application details for determination from ApplicationDeterminationService
                    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                      if (res) {
                        const data = res as iApplicationDeterminationData;
                        if (data) {
                          this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
                          this.applicationDeterminationData = data;
                          if (this.applicationDeterminationData) {
                            this.populateInitData();
                          }
                        }
                      }
                    });
                  }
                });
              }
            });
          }
          else {
            return;
          }
        }
      }
    });

  }

  populateInitData() {
    if (this.applicationDeterminationData) {
      // //Get User Data
      // this.userDataSub = this.userService.getUserData()
      //   .subscribe(res => {
      //     if (res)
      //       this.userData = res;
      //       console.log('UserData - ', this.userData);
      //   });

      this.pactApplicationId = this.applicationDeterminationData.pactApplicationId;

      if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType !== null)
        this.clientCategoryType = this.applicationDeterminationData.clientCategoryType;

      this.supportNotOfferedByGenPopType = this.applicationDeterminationData.supportNotOfferedByGenPopType;

      if (this.applicationDeterminationData.isClinicalPreDeterminationTabCompleted !== null)
        this.isMentalHealthConditionTabEnabled = this.applicationDeterminationData.isClinicalPreDeterminationTabCompleted;
      else
        this.isMentalHealthConditionTabEnabled = false;

      if (this.applicationDeterminationData.isMentalHealthConditionTabCompleted !== null)
        this.isSubstanceUseTabEnabled = this.applicationDeterminationData.isMentalHealthConditionTabCompleted;
      else
        this.isSubstanceUseTabEnabled = false;

      if (this.applicationDeterminationData.isSubstanceUseTabCompleted !== null)
        this.isMedicalConditionTabEnabled = this.applicationDeterminationData.isSubstanceUseTabCompleted;
      else
        this.isMedicalConditionTabEnabled = false;

      if (this.clientCategoryType === 672 || this.clientCategoryType === 674
        || this.clientCategoryType === 676) {
        this.isClinicalAtRiskTabEnabled = this.applicationDeterminationData.isMedicalConditionsTabCompleted;
        this.isClinicalSummaryTabEnabled = this.applicationDeterminationData.isClinicalAtRiskTabCompleted;
      }
      else
        this.isClinicalSummaryTabEnabled = this.applicationDeterminationData.isMedicalConditionsTabCompleted;

      //Clinical Review Input for all Tabs
      // this.detClinicalReviewInput = {
      //   pactApplicationID: this.pactApplicationId, summaryType: this.summaryType
      //   , clientCategoryType: this.clientCategoryType, userID: this.userData.optionUserId
      // };

      //Clinical Review Input for all Tabs
      this.detClinicalReviewInput.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
      this.detClinicalReviewInput.summaryType = this.summaryType;
      this.detClinicalReviewInput.clientCategoryType = this.applicationDeterminationData.clientCategoryType;
      this.detClinicalReviewInput.userID = this.userData.optionUserId;

      //Get Diagnosis and Documents
      if (this.pactApplicationId) {
        this.getPACTMHRDiagnosis(this.pactApplicationId);
        this.mentalHealthDocuments.pactApplicationId = this.substanceUseDocuments.pactApplicationId =
          this.medicalDocuments.pactApplicationId = this.pactApplicationId;
        this.mentalHealthDocuments.documentType = [52, 53, 328, 329, 330];
        this.substanceUseDocuments.documentType = [56];
        this.medicalDocuments.documentType = [54];
      }

      //Load Mental Health Condition Data
      this.loadMentalHealthConditionData();

      //Enable Pending Review on Page Load
      this.determinationSideNavService.enablePendingReview(this.applicationDeterminationData);
      this.determinationSideNavService.setIsClinicalReviewDisabled(false);

      //Enable Side Nav List till Clinical Review
      //this.enableClinicalSideNavList();
    }
    else
      throw new Error("There is an error while getting Application Determination Data.");
  }

  //Get Diagnosis Details
  getPACTMHRDiagnosis(pactApplicationId: number) {
    this.clinicalAssessmentDataSub = this.clinicalAssessmentService.getAssessment(pactApplicationId)
      .subscribe(
        res => {
          const data = res as ClinicalAssessmentModel;
          if (data !== null) {
            this.hasMedicalDiagnosis = data.hasMedicalDiagnosis;
            this.hasPrincipalDiagnosis = data.hasPrincipalDiagnosis;
            this.hasOtherDiagnosis = data.hasOtherDiagnosis;
            this.diagnosisList = data.diagnosisList;

            if (this.diagnosisList !== null) {
              this.mdList = this.diagnosisList.filter(x => x.diagnosisTypeID === 48 && x.isActive === true);
              this.pdList = this.diagnosisList.filter(x => x.diagnosisTypeID === 49 && x.isActive === true);
              this.odList = this.diagnosisList.filter(x => x.diagnosisTypeID === 51 && x.isActive === true);
            }
          }
        },
        error => {
          throw new Error("There is an error while getting Diagnosis.");
        });
  }

  // enableClinicalSideNavList() {
  //   this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
  //   this.determinationSideNavService.setIsClinicalReviewDisabled(false);
  // }

  //#region Tab Save, Change Events

  //Tab Changes Event
  tabChanged() {
    //console.log('Current Tab - ', this.currentTab);
    //console.log('selected Tab - ', this.selectedTab);

    switch (this.currentTab) {
      case 0:
        if (this.validateMentalHealthForm()) {
          this.saveMentalHealthCondition(0);
          //this.saveTabTimeDuration(this.currentTab);
          this.currentTab = this.selectedTab;
          this.loadSelectedTabData();
        }
        else {
          this.selectedTab = 0;
          return;
        }
        break;
      case 1:
        if (this.validateSubstanceUseForm()) {
          this.saveSubstanceUse(0);
          //this.saveTabTimeDuration(this.currentTab);
          this.currentTab = this.selectedTab;
          this.loadSelectedTabData();
        }
        else {
          this.selectedTab = 1;
          return;
        }
        break;
      case 2:
        if (this.validateMedicalConditionForm()) {
          this.saveMedicalCondition(0);
          //this.saveTabTimeDuration(this.currentTab);
          this.currentTab = this.selectedTab;
          this.loadSelectedTabData();
        }
        else {
          this.selectedTab = 2;
          return;
        }
        break;
      case 3:
        if (this.clientCategoryType === 671 || this.clientCategoryType === 673 || this.clientCategoryType === 675) {
          this.isClinicalAtRiskCompleted = true;
          this.saveTabTimeDuration(this.currentTab);
          this.currentTab = this.selectedTab;
          this.loadSelectedTabData();
          //this.setSaveButtonVisibility();
        }
        else {
          if (this.validateAtRiskForm()) {
            this.saveAtRisk(0);
            //this.saveTabTimeDuration(this.currentTab);
            this.currentTab = this.selectedTab;
            this.loadSelectedTabData();
          }
          else {
            this.selectedTab = 3;
            return;
          }

        }
        break;
      case 4:
        if ((this.clientCategoryType === 672 || this.clientCategoryType === 674 || this.clientCategoryType === 676)
          && this.isClinicalAtRiskCompleted === true) {
          this.isClinicalAtRiskCompleted = true;
          this.saveTabTimeDuration(this.currentTab);
          this.currentTab = this.selectedTab;
          //this.setSaveButtonVisibility();
          this.loadSelectedTabData();
        }
        break;
    }
  }

  loadSelectedTabData() {
    this.isSaveButtonVisible = true;

    switch (this.selectedTab) {
      case 0:
        this.loadMentalHealthConditionData();
        break;
      case 1:
        this.loadSubstanceUseData();
        break;
      case 2:
        this.loadMedicalConditionData();
        break;
      case 3:
        if (this.clientCategoryType === 671 || this.clientCategoryType === 673 || this.clientCategoryType === 675) {
          this.isClinicalAtRiskCompleted = true;
        }
        else {
          this.loadAtRiskData();
        }
        break;
      case 4:
        if ((this.clientCategoryType === 672 || this.clientCategoryType === 674 || this.clientCategoryType === 676)
          && this.isClinicalAtRiskCompleted === true) {
          this.isClinicalAtRiskCompleted = true;
        }
        break;
    }

    this.setSaveButtonVisibility();
  }

  //Next button click
  nextTab() {
    this.saveTabData(1, this.currentTab);
    //this.saveTabTimeDuration(this.selectedTab);
  }

  //Previous button click
  previousTab() {
    if (this.currentTab != 0) {
      this.saveTabData(2, this.currentTab);
    }
    else {
      this.saveTabData(2, this.currentTab);
    }
  }

  setSaveButtonVisibility() {
    if (this.clientCategoryType === 671 || this.clientCategoryType === 673 || this.clientCategoryType === 675) {
      if (this.selectedTab === 3)
        this.isSaveButtonVisible = false;
      else
        this.isSaveButtonVisible = true;
    }
    else {
      if (this.selectedTab === 4)
        this.isSaveButtonVisible = false;
      else
        this.isSaveButtonVisible = true;
    }
    //console.log('isSaveButtonVisible - ' , this.isSaveButtonVisible);
  }

  //Get the flag from Summary Component to Route the user on Homeless or Final Summary
  calculateClinicalInEligible(data: any): void {
    if (data != null)
      this.isClinicalInEligible = data;
  }

  //Save Tab Time Duration
  saveTabTimeDuration(tabIndex: number) {
    let tabOutName: string;

    if (tabIndex === 0) {
      tabOutName = "MentalHealthCondition"
    }
    else if (tabIndex === 1) {
      tabOutName = "SubstanceUse";
    }
    else if (tabIndex === 2) {
      tabOutName = "MedicalConditions";
    }
    else if (tabIndex === 3) {
      if (this.clientCategoryType === 672 || this.clientCategoryType === 674 || this.clientCategoryType === 676)
        tabOutName = "ClinicalAtRisk";
      else
        tabOutName = "ClinicalSummary";
    }
    else if (tabIndex === 4) {
      tabOutName = "ClinicalSummary";
    }

    if (this.pactApplicationId) {
      this.detTabTimeDuration.pactApplicationId = this.pactApplicationId;
      this.detTabTimeDuration.tabOutName = tabOutName;
      this.detTabTimeDuration.optionUserId = this.userData.optionUserId;
      this.commonService.saveTabTimeDuration(this.detTabTimeDuration).subscribe();
    }
  }

  // Value - 0 For Save, 1 For Save And Next
  saveTabData(value: number, tabIndex: number) {
    //console.log('Save tab data Index - ', tabIndex);

    switch (tabIndex) {
      case 0:
        this.isMentalHealthCompleted = this.validateMentalHealthForm();
        if (this.isMentalHealthCompleted) {
          this.saveMentalHealthCondition(value);

          // if (value === 1)
          //   this.selectedTab = this.selectedTab + 1;
          // else if (value === 2)
          //   this.selectedTab = this.selectedTab - 1;
        }
        break;
      case 1:
        this.isSubstanceUseCompleted = this.validateSubstanceUseForm();
        if (this.isSubstanceUseCompleted) {
          this.saveSubstanceUse(value);

          // if (value === 1)
          //   this.selectedTab = this.selectedTab + 1;
          // else if (value === 2)
          //   this.selectedTab = this.selectedTab - 1;
        }
        break;
      case 2:
        this.isMedicalConditionCompleted = this.validateMedicalConditionForm();
        if (this.isMedicalConditionCompleted) {
          this.saveMedicalCondition(value);

          // if (value === 1)
          //   this.selectedTab = this.selectedTab + 1;
          // else if (value === 2)
          //   this.selectedTab = this.selectedTab - 1;
        }
        break;
      case 3:
        if (this.clientCategoryType === 672 || this.clientCategoryType === 674 || this.clientCategoryType === 676) {
          this.isClinicalAtRiskCompleted = this.validateAtRiskForm();
          if (this.isClinicalAtRiskCompleted) {
            this.saveAtRisk(value);

            // if (value === 1)
            //   this.selectedTab = this.selectedTab + 1;
            // else if (value === 2)
            //   this.selectedTab = this.selectedTab - 1;
          }
        }
        else {
          //console.log('isClinicalInEligible  - ', this.isClinicalInEligible) ;

          this.saveTabTimeDuration(tabIndex);
          //Logic to Navigate on Homeless Review Page
          if (value === 1) {
            if (this.isClinicalInEligible) {
              this.determinationSideNavService.setIsDeterminationSummaryDisabled(false);
              this.router.navigate(['/ds/determination-summary/', this.pactApplicationId]);
            }
            else {
              this.determinationSideNavService.setIsHomelessReviewDisabled(false);
              this.router.navigate(['ds/homeless-review/', this.pactApplicationId]);
            }
          }
          else if (value === 2)
            this.selectedTab = this.selectedTab - 1;
        }
        break;
      case 4: //Logic to Navigate on Homeless Review Page
        this.saveTabTimeDuration(tabIndex);
        if (value === 1) {
          if (this.isClinicalInEligible) {
            this.determinationSideNavService.setIsDeterminationSummaryDisabled(false);
            this.router.navigate(['/ds/determination-summary/', this.pactApplicationId]);
          }
          else {
            this.determinationSideNavService.setIsHomelessReviewDisabled(false);
            this.router.navigate(['ds/homeless-review/', this.pactApplicationId]);
          }
        }
        else if (value === 2)
          this.selectedTab = this.selectedTab - 1;
        break;
    }

    this.setSaveButtonVisibility();
  }

  setNextTabVisibility() {
    switch (this.selectedTab) {
      case 0:
        this.isSubstanceUseTabEnabled = this.isMedicalConditionTabEnabled = this.isClinicalAtRiskTabEnabled
          = this.isClinicalSummaryTabEnabled = false;
        break;
      case 1:
        this.isMedicalConditionTabEnabled = this.isClinicalAtRiskTabEnabled = this.isClinicalSummaryTabEnabled = false;
        break;
      case 2:
        this.isClinicalAtRiskTabEnabled = this.isClinicalSummaryTabEnabled = false;
        break;
      case 3:
        this.isClinicalSummaryTabEnabled = false;
        break;
    }
  }

  disableOtherNavigationItemsOnClinicalChange() {
    this.determinationSideNavService.setIsHomelessReviewDisabled(true);
    this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(true);
    this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(true);
    this.determinationSideNavService.setIsDeterminationSummaryDisabled(true);
    this.determinationSideNavService.setIsSignOffFollowUpDisabled(true);
  }

  //#endregion

  //#region Mental Health Condition
  loadMentalHealthConditionData() {
    if (this.detClinicalReviewInput.userID === null) {
      this.detClinicalReviewInput.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
      this.detClinicalReviewInput.summaryType = this.summaryType;
      this.detClinicalReviewInput.clientCategoryType = this.applicationDeterminationData.clientCategoryType;
      this.detClinicalReviewInput.userID = this.userData.optionUserId;
    }

    //console.log('DetClinicalReviewInput - ', this.detClinicalReviewInput);
    this.clinicalReviewDataSub = this.clinicalReviewService.getDETMentalHealthCondition(this.detClinicalReviewInput)
      .subscribe(
        res => {
          //console.log('DETMentalHealthCondition - ', res);
          this.detMentalHealthCondition = res as DETMentalHealthCondition;
          if (this.detMentalHealthCondition.detMentalHealthConditionID > 0)
            this.populateMentalHealthCondition(this.detMentalHealthCondition);
        }
      );
  }

  //Populate Mental Health Condition Form Data When Selected
  populateMentalHealthCondition(formData: DETMentalHealthCondition) {
    //console.log('Mental Health Condition- ', formData);
    if (formData != null) {
      this.detMentalHealthConditionId = formData.detMentalHealthConditionID;
      this.isMentalHealthCompleted = formData.isMentalHealthConditionCompleted;
      // if (this.clientCategoryType === 675 || this.clientCategoryType === 676)
      //   this.isSubstanceUseTabEnabled = formData.isMentalHealthConditionCompleted;
      // else {
      if (formData.stabilizedForPlacement === 34)
        this.isSubstanceUseTabEnabled = this.isMedicalConditionTabEnabled
          = this.isClinicalAtRiskTabEnabled = this.isClinicalSummaryTabEnabled = false;
      else
        this.isSubstanceUseTabEnabled = formData.isMentalHealthConditionCompleted;
      //}

      //Question 1 Fields
      if (formData.currentMHDiagnosis !== null) {
        this.mentalHealthGroup.controls['currentMHDiagnosisOrig'].setValue(formData.currentMHDiagnosisOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['currentMHDiagnosis'].setValue(formData.currentMHDiagnosis == 33 ? 'Y' : 'N');
        this.currentMHDiagnosisOrigText = formData.currentMHDiagnosisOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.mentalHealthGroup.controls['currentMHDiagnosisOrig'].setValue(null);
        this.mentalHealthGroup.controls['currentMHDiagnosis'].setValue(null);
        this.currentMHDiagnosisOrigText = '';
      }

      if (formData.currentMHDiagnosisSource !== null) {
        this.mentalHealthGroup.controls['currentMHDiagnosisSource'].setValue(formData.currentMHDiagnosisSource);
        this.currentMHDiagnosisSourceText = formData.currentMHDiagnosisSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['currentMHDiagnosisSource'].setValue(null);
        this.currentMHDiagnosisSourceText = '';
      }

      //Question 2 Fields
      if (formData.currentMHDiagnosisOrCondition !== null) {
        this.mentalHealthGroup.controls['currentMHDiagnosisOrConditionOrig'].setValue(formData.currentMHDiagnosisOrConditionOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['currentMHDiagnosisOrCondition'].setValue(formData.currentMHDiagnosisOrCondition == 33 ? 'Y' : 'N');
        this.currentMHDiagnosisOrConditionOrigText = formData.currentMHDiagnosisOrConditionOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.mentalHealthGroup.controls['currentMHDiagnosisOrConditionOrig'].setValue(null);
        this.mentalHealthGroup.controls['currentMHDiagnosisOrCondition'].setValue(null);
        this.currentMHDiagnosisOrConditionOrigText = '';
      }

      if (formData.currentMHDiagnosisOrConditionSource != null) {
        this.mentalHealthGroup.controls['currentMHDiagnosisOrConditionSource'].setValue(formData.currentMHDiagnosisOrConditionSource);
        this.currentMHDiagnosisOrConditionSourceText = formData.currentMHDiagnosisOrConditionSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['currentMHDiagnosisOrConditionSource'].setValue(null);
        this.currentMHDiagnosisOrConditionSourceText = '';
      }

      //Question 3 Fields
      if (formData.significantFunctionalImpairment != null) {
        this.mentalHealthGroup.controls['significantFunctionalImpairmentOrig'].setValue(formData.significantFunctionalImpairmentOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['significantFunctionalImpairment'].setValue(formData.significantFunctionalImpairment == 33 ? 'Y' : 'N');
      }
      else {
        this.mentalHealthGroup.controls['significantFunctionalImpairmentOrig'].setValue(null);
        this.mentalHealthGroup.controls['significantFunctionalImpairment'].setValue(null);
        this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].disable();
      }

      if (formData.significantFunctionalImpairmentSource != null) {
        this.mentalHealthGroup.controls['significantFunctionalImpairmentSource'].setValue(formData.significantFunctionalImpairmentSource);
        this.significantFunctionalImpairmentSourceText = formData.significantFunctionalImpairmentSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['significantFunctionalImpairmentSource'].setValue(null);
        this.significantFunctionalImpairmentSourceText = '';
      }

      if (formData.significantFunctionalImpairmentExplain != null) {
        this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].setValue(formData.significantFunctionalImpairmentExplain);
        if (formData.significantFunctionalImpairmentExplain !== null || formData.significantFunctionalImpairmentExplain !== '')
          this.significantFunctionalImpairmentExplainSourceText = 'Reviewer';
        else
          this.significantFunctionalImpairmentExplainSourceText = '';
      }
      else {
        this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].setValue(null);
        this.significantFunctionalImpairmentExplainSourceText = '';
      }

      //Question 4 Fields
      if (formData.marginalFunctionalImpairmentPsychiatric != null) {
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatricOrig'].setValue(formData.marginalFunctionalImpairmentPsychiatricOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].setValue(formData.marginalFunctionalImpairmentPsychiatric == 33 ? 'Y' : 'N');
      }
      else {
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatricOrig'].setValue(null);
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].setValue(null);
      }

      if (formData.marginalFunctionalImpairmentPsychiatricSource != null) {
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatricSource'].setValue(formData.marginalFunctionalImpairmentPsychiatricSource);
        this.marginalFunctionalImpairmentPsychiatricSourceText = formData.marginalFunctionalImpairmentPsychiatricSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatricSource'].setValue(null);
        this.marginalFunctionalImpairmentPsychiatricSourceText = '';
      }

      //Question 5 Fields
      if (formData.marginalFunctionalImpairmentDevelopmental != null) {
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmentalOrig'].setValue(formData.marginalFunctionalImpairmentDevelopmentalOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].setValue(formData.marginalFunctionalImpairmentDevelopmental == 33 ? 'Y' : 'N');
      }
      else {
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmentalOrig'].setValue(null);
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].setValue(null);
      }

      if (formData.marginalFunctionalImpairmentDevelopmentalSource != null) {
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmentalSource'].setValue(formData.marginalFunctionalImpairmentDevelopmentalSource);
        this.marginalFunctionalImpairmentDevelopmentalSourceText = formData.marginalFunctionalImpairmentDevelopmentalSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmentalSource'].setValue(null);
        this.marginalFunctionalImpairmentDevelopmentalSourceText = '';
      }

      //Question 7 Fields
      if (formData.excludingSUDNonSPMI != null) {
        this.mentalHealthGroup.controls['excludingSUDNonSPMIOrig'].setValue(formData.excludingSUDNonSPMIOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['excludingSUDNonSPMI'].setValue(formData.excludingSUDNonSPMI == 33 ? 'Y' : 'N');
      }
      else {
        this.mentalHealthGroup.controls['excludingSUDNonSPMIOrig'].setValue(null);
        this.mentalHealthGroup.controls['excludingSUDNonSPMI'].setValue(null);
      }

      if (formData.excludingSUDNonSPMISource != null) {
        this.mentalHealthGroup.controls['excludingSUDNonSPMISource'].setValue(formData.excludingSUDNonSPMISource);
        this.excludingSUDNonSPMISourceText = formData.excludingSUDNonSPMISource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['excludingSUDNonSPMISource'].setValue(null);
        this.excludingSUDNonSPMISourceText = '';
      }

      //Question 8 Fields
      if (formData.seriousMentalIllness != null) {
        this.mentalHealthGroup.controls['seriousMentalIllnessOrig'].setValue(formData.seriousMentalIllnessOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['seriousMentalIllness'].setValue(formData.seriousMentalIllness == 33 ? 'Y' : 'N');
        this.seriousMentalIllnessOrigText = formData.seriousMentalIllnessOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.mentalHealthGroup.controls['seriousMentalIllnessOrig'].setValue(null);
        this.mentalHealthGroup.controls['seriousMentalIllness'].setValue(null);
      }

      if (formData.seriousMentalIllnessSource != null) {
        this.mentalHealthGroup.controls['seriousMentalIllnessSource'].setValue(formData.seriousMentalIllnessSource);
        this.seriousMentalIllnessSourceText = formData.seriousMentalIllnessSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['seriousMentalIllnessSource'].setValue(null);
        this.seriousMentalIllnessSourceText = '';
      }

      //Question 9 Fields
      if (formData.haveSEDDiagnosedBefore18 != null) {
        this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Orig'].setValue(formData.haveSEDDiagnosedBefore18Orig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].setValue(formData.haveSEDDiagnosedBefore18 == 33 ? 'Y' : 'N');
        this.haveSEDDiagnosedBefore18OrigText = formData.haveSEDDiagnosedBefore18Orig == 33 ? 'Yes' : 'No';
      }
      else {
        this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Orig'].setValue(null);
        this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].setValue(null);
      }

      if (formData.haveSEDDiagnosedBefore18Source != null) {
        this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Source'].setValue(formData.haveSEDDiagnosedBefore18Source);
        this.haveSEDDiagnosedBefore18SourceText = formData.haveSEDDiagnosedBefore18Source == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Source'].setValue(null);
        this.haveSEDDiagnosedBefore18SourceText = '';
      }

      //Question 10 Fields
      if (formData.stabilizedForPlacement != null) {
        this.mentalHealthGroup.controls['stabilizedForPlacementOrig'].setValue(formData.stabilizedForPlacementOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['stabilizedForPlacement'].setValue(formData.stabilizedForPlacement == 33 ? 'Y' : 'N');
      }
      else {
        this.mentalHealthGroup.controls['stabilizedForPlacementOrig'].setValue(null);
        this.mentalHealthGroup.controls['stabilizedForPlacement'].setValue(null);
        this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].disable();
      }

      if (formData.stabilizedForPlacementSource != null) {
        this.mentalHealthGroup.controls['stabilizedForPlacementSource'].setValue(formData.stabilizedForPlacementSource);
        this.stabilizedForPlacementSourceText = formData.stabilizedForPlacementSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['stabilizedForPlacementSource'].setValue(null);
        this.stabilizedForPlacementSourceText = '';
      }

      if (formData.stabilizedForPlacementExplain != null) {
        this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].setValue(formData.stabilizedForPlacementExplain);
        if (formData.stabilizedForPlacementExplain !== '')
          this.stabilizedForPlacementExplainSourceText = 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].setValue(null);
        this.stabilizedForPlacementExplainSourceText = '';
      }

      //Question 11 Fields
      if (formData.developmentalDisability != null) {
        this.mentalHealthGroup.controls['developmentalDisabilityOrig'].setValue(formData.developmentalDisabilityOrig == 33 ? 'Y' : 'N');
        this.mentalHealthGroup.controls['developmentalDisability'].setValue(formData.developmentalDisability == 33 ? 'Y' : 'N');
        this.developmentalDisabilityOrigText = formData.developmentalDisabilityOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.mentalHealthGroup.controls['developmentalDisabilityOrig'].setValue(null);
        this.mentalHealthGroup.controls['developmentalDisability'].setValue(null);
        this.developmentalDisabilityOrigText = '';
      }

      if (formData.developmentalDisabilitySource != null) {
        this.mentalHealthGroup.controls['developmentalDisabilitySource'].setValue(formData.developmentalDisabilitySource);
        this.developmentalDisabilitySourceText = formData.developmentalDisabilitySource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.mentalHealthGroup.controls['developmentalDisabilitySource'].setValue(null);
        this.developmentalDisabilitySourceText = '';
      }

    }
    else {
      this.detMentalHealthCondition = new DETMentalHealthCondition();
      this.detMentalHealthCondition.pactApplicationID = this.pactApplicationId;

      this.mentalHealthGroup.controls['currentMHDiagnosisOrig'].setValue(null);
      this.currentMHDiagnosisOrigText = '';
      this.mentalHealthGroup.controls['currentMHDiagnosis'].setValue(null);
      this.mentalHealthGroup.controls['currentMHDiagnosisSource'].setValue(null);
      this.currentMHDiagnosisSourceText = '';
      this.mentalHealthGroup.controls['currentMHDiagnosisOrConditionOrig'].setValue(null);
      this.currentMHDiagnosisOrConditionOrigText = '';
      this.mentalHealthGroup.controls['currentMHDiagnosisOrCondition'].setValue(null);
      this.mentalHealthGroup.controls['currentMHDiagnosisOrConditionSource'].setValue(null);
      this.currentMHDiagnosisOrConditionSourceText = '';
      this.mentalHealthGroup.controls['significantFunctionalImpairmentOrig'].setValue(null);
      this.mentalHealthGroup.controls['significantFunctionalImpairment'].setValue(null);
      this.mentalHealthGroup.controls['significantFunctionalImpairmentSource'].setValue(null);
      this.significantFunctionalImpairmentSourceText = '';
      this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].setValue(null);
      this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatricOrig'].setValue(null);
      this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].setValue(null);
      this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatricSource'].setValue(null);
      this.marginalFunctionalImpairmentPsychiatricSourceText = '';
      this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmentalOrig'].setValue(null);
      this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].setValue(null);
      this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmentalSource'].setValue(null);
      this.marginalFunctionalImpairmentDevelopmentalSourceText = '';
      this.mentalHealthGroup.controls['excludingSUDNonSPMIOrig'].setValue(null);
      this.mentalHealthGroup.controls['excludingSUDNonSPMI'].setValue(null);
      this.mentalHealthGroup.controls['excludingSUDNonSPMISource'].setValue(null);
      this.excludingSUDNonSPMISourceText = '';
      this.mentalHealthGroup.controls['seriousMentalIllnessOrig'].setValue(null);
      this.seriousMentalIllnessOrigText = '';
      this.mentalHealthGroup.controls['seriousMentalIllness'].setValue(null);
      this.mentalHealthGroup.controls['seriousMentalIllnessSource'].setValue(null);
      this.seriousMentalIllnessSourceText = '';
      this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Orig'].setValue(null);
      this.haveSEDDiagnosedBefore18OrigText = '';
      this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].setValue(null);
      this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Source'].setValue(null);
      this.haveSEDDiagnosedBefore18SourceText = '';
      this.mentalHealthGroup.controls['stabilizedForPlacementOrig'].setValue(null);
      this.mentalHealthGroup.controls['stabilizedForPlacement'].setValue(null);
      this.mentalHealthGroup.controls['stabilizedForPlacementSource'].setValue(null);
      this.stabilizedForPlacementSourceText = '';
      this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].setValue(null);
      this.mentalHealthGroup.controls['developmentalDisabilityOrig'].setValue(null);
      this.developmentalDisabilityOrigText = '';
      this.mentalHealthGroup.controls['developmentalDisability'].setValue(null);
      this.mentalHealthGroup.controls['developmentalDisabilitySource'].setValue(null);
      this.developmentalDisabilitySourceText = '';
    }

    this.setTabsQuestionsVisibility();
  }

  //#region All Mental Health Question Selection Methods
  currentMHDiagnosisSelected(selectedValue: string) {
    this.mentalHealthGroup.controls['currentMHDiagnosis'].setValue(selectedValue);

    if (this.mentalHealthGroup.get('currentMHDiagnosisOrig').value != null) {
      var currentMHDiagnosisOrig = this.mentalHealthGroup.get('currentMHDiagnosisOrig').value;
      var currentMHDiagnosisSource = currentMHDiagnosisOrig === selectedValue ? 679 : 680;
      this.mentalHealthGroup.controls['currentMHDiagnosisSource'].setValue(currentMHDiagnosisSource);
      this.currentMHDiagnosisSourceText = currentMHDiagnosisSource === 679 ? 'Application' : 'Reviewer';
    }

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  currentMHDiagnosisOrConditionSelected(selectedValue: string) {
    this.mentalHealthGroup.controls['currentMHDiagnosisOrCondition'].setValue(selectedValue);

    if (this.mentalHealthGroup.get('currentMHDiagnosisOrConditionOrig').value != null) {
      var currentMHDiagnosisOrConditionOrig = this.mentalHealthGroup.get('currentMHDiagnosisOrConditionOrig').value;
      var currentMHDiagnosisOrConditionSource = currentMHDiagnosisOrConditionOrig == selectedValue ? 679 : 680;
      this.mentalHealthGroup.controls['currentMHDiagnosisOrConditionSource'].setValue(currentMHDiagnosisOrConditionSource);
      this.currentMHDiagnosisOrConditionSourceText = currentMHDiagnosisOrConditionSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  significantFunctionalImpairmentSelected(selectedValue: string) {
    this.mentalHealthGroup.controls['significantFunctionalImpairment'].setValue(selectedValue);
    this.mentalHealthGroup.controls['significantFunctionalImpairmentSource'].setValue(680);
    this.significantFunctionalImpairmentSourceText = 'Reviewer';

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  calculateSignificantFunctionalImpairmentExplainSource(value: string): void {
    if (value.length > 0)
      this.significantFunctionalImpairmentExplainSourceText = 'Reviewer';
    else
      this.significantFunctionalImpairmentExplainSourceText = '';
  }

  marginalFunctionalImpairmentPsychiatricSelected(selectedValue: string) {
    this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].setValue(selectedValue);
    this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatricSource'].setValue(680);
    this.marginalFunctionalImpairmentPsychiatricSourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  seriousMentalIllnessSelected(selectedValue: string) {
    this.mentalHealthGroup.controls['seriousMentalIllness'].setValue(selectedValue);

    if (this.mentalHealthGroup.get('seriousMentalIllnessOrig').value != null) {
      var seriousMentalIllnessOrig = this.mentalHealthGroup.get('seriousMentalIllnessOrig').value;
      var seriousMentalIllnessSource = seriousMentalIllnessOrig == selectedValue ? 679 : 680;
      this.mentalHealthGroup.controls['seriousMentalIllnessSource'].setValue(seriousMentalIllnessSource);
      this.seriousMentalIllnessSourceText = seriousMentalIllnessSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  stabilizedForPlacementSelected(selectedValue: string) {
    //console.log('Selected Stabilized - ',this.mentalHealthGroup.get('stabilizedForPlacement').value);
    this.mentalHealthGroup.controls['stabilizedForPlacement'].setValue(selectedValue);
    this.mentalHealthGroup.controls['stabilizedForPlacementSource'].setValue(680);
    this.stabilizedForPlacementSourceText = 'Reviewer';

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  calculatestabilizedForPlacementExplainSource(value: string): void {
    if (value.length > 0)
      this.stabilizedForPlacementExplainSourceText = 'Reviewer';
    else
      this.stabilizedForPlacementExplainSourceText = '';
  }

  haveSEDDiagnosedBefore18Selected(selectedValue: string) {
    this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].setValue(selectedValue);
    this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Source'].setValue(680);
    this.haveSEDDiagnosedBefore18SourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  developmentalDisabilitySelected(selectedValue: string) {

    this.mentalHealthGroup.controls['developmentalDisability'].setValue(selectedValue);

    if (this.mentalHealthGroup.get('developmentalDisabilityOrig').value != null) {
      var developmentalDisabilityOrig = this.mentalHealthGroup.get('developmentalDisabilityOrig').value;
      var developmentalDisabilitySource = developmentalDisabilityOrig == selectedValue ? 679 : 680;
      this.mentalHealthGroup.controls['developmentalDisabilitySource'].setValue(developmentalDisabilitySource);
      this.developmentalDisabilitySourceText = developmentalDisabilitySource == 679 ? 'Application' : 'Reviewer';
    }

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  excludingSUDNonSPMISelected(selectedValue: string) {
    this.mentalHealthGroup.controls['excludingSUDNonSPMI'].setValue(selectedValue);
    this.mentalHealthGroup.controls['excludingSUDNonSPMISource'].setValue(680);
    this.excludingSUDNonSPMISourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  marginalFunctionalImpairmentDevelopmentalSelected(selectedValue: string) {
    this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].setValue(selectedValue);
    this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmentalSource'].setValue(680);
    this.marginalFunctionalImpairmentDevelopmentalSourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //#endregion 

  //Save Mental Health Condition
  saveMentalHealthCondition(value: number) {
    //Question 1
    if (this.mentalHealthGroup.get('currentMHDiagnosisOrig').value !== null)
      this.detMentalHealthCondition.currentMHDiagnosisOrig = this.mentalHealthGroup.get('currentMHDiagnosisOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.currentMHDiagnosisOrig = null;

    if (this.mentalHealthGroup.get('currentMHDiagnosis').value !== null)
      this.detMentalHealthCondition.currentMHDiagnosis = this.mentalHealthGroup.get('currentMHDiagnosis').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.currentMHDiagnosis = null;

    if (this.mentalHealthGroup.get('currentMHDiagnosisSource').value !== null)
      this.detMentalHealthCondition.currentMHDiagnosisSource = this.mentalHealthGroup.get('currentMHDiagnosisSource').value;
    else
      this.detMentalHealthCondition.currentMHDiagnosisSource = null;

    //Question 2
    if (this.mentalHealthGroup.get('currentMHDiagnosisOrConditionOrig').value !== null)
      this.detMentalHealthCondition.currentMHDiagnosisOrConditionOrig = this.mentalHealthGroup.get('currentMHDiagnosisOrConditionOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.currentMHDiagnosisOrConditionOrig = null;

    if (this.mentalHealthGroup.get('currentMHDiagnosisOrCondition').value !== null)
      this.detMentalHealthCondition.currentMHDiagnosisOrCondition = this.mentalHealthGroup.get('currentMHDiagnosisOrCondition').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.currentMHDiagnosisOrCondition = null;

    if (this.mentalHealthGroup.get('currentMHDiagnosisOrConditionSource').value !== null)
      this.detMentalHealthCondition.currentMHDiagnosisOrConditionSource = this.mentalHealthGroup.get('currentMHDiagnosisOrConditionSource').value;
    else
      this.detMentalHealthCondition.currentMHDiagnosisOrConditionSource = null;

    //Question 3
    if (this.mentalHealthGroup.get('significantFunctionalImpairmentOrig').value !== null)
      this.detMentalHealthCondition.significantFunctionalImpairmentOrig = this.mentalHealthGroup.get('significantFunctionalImpairmentOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.significantFunctionalImpairmentOrig = null;

    if (this.mentalHealthGroup.get('significantFunctionalImpairment').value !== null)
      this.detMentalHealthCondition.significantFunctionalImpairment = this.mentalHealthGroup.get('significantFunctionalImpairment').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.significantFunctionalImpairment = null;

    if (this.mentalHealthGroup.get('significantFunctionalImpairmentSource').value !== null)
      this.detMentalHealthCondition.significantFunctionalImpairmentSource = this.mentalHealthGroup.get('significantFunctionalImpairmentSource').value;
    else
      this.detMentalHealthCondition.significantFunctionalImpairmentSource = null;

    if (this.mentalHealthGroup.get('significantFunctionalImpairmentExplain').value !== null)
      this.detMentalHealthCondition.significantFunctionalImpairmentExplain = this.mentalHealthGroup.get('significantFunctionalImpairmentExplain').value;
    else
      this.detMentalHealthCondition.significantFunctionalImpairmentExplain = null;

    //Question 4
    if (this.mentalHealthGroup.get('marginalFunctionalImpairmentPsychiatricOrig').value !== null)
      this.detMentalHealthCondition.marginalFunctionalImpairmentPsychiatricOrig = this.mentalHealthGroup.get('marginalFunctionalImpairmentPsychiatricOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.marginalFunctionalImpairmentPsychiatricOrig = null;

    if (this.mentalHealthGroup.get('marginalFunctionalImpairmentPsychiatric').value !== null)
      this.detMentalHealthCondition.marginalFunctionalImpairmentPsychiatric = this.mentalHealthGroup.get('marginalFunctionalImpairmentPsychiatric').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.marginalFunctionalImpairmentPsychiatric = null;

    if (this.mentalHealthGroup.get('marginalFunctionalImpairmentPsychiatricSource').value !== null)
      this.detMentalHealthCondition.marginalFunctionalImpairmentPsychiatricSource = this.mentalHealthGroup.get('marginalFunctionalImpairmentPsychiatricSource').value;
    else
      this.detMentalHealthCondition.marginalFunctionalImpairmentPsychiatricSource = null;

    //Question 5
    if (this.mentalHealthGroup.get('marginalFunctionalImpairmentDevelopmentalOrig').value !== null)
      this.detMentalHealthCondition.marginalFunctionalImpairmentDevelopmentalOrig = this.mentalHealthGroup.get('marginalFunctionalImpairmentDevelopmentalOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.marginalFunctionalImpairmentDevelopmentalOrig = null;

    if (this.mentalHealthGroup.get('marginalFunctionalImpairmentDevelopmental').value !== null)
      this.detMentalHealthCondition.marginalFunctionalImpairmentDevelopmental = this.mentalHealthGroup.get('marginalFunctionalImpairmentDevelopmental').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.marginalFunctionalImpairmentDevelopmental = null;

    if (this.mentalHealthGroup.get('marginalFunctionalImpairmentDevelopmentalSource').value !== null)
      this.detMentalHealthCondition.marginalFunctionalImpairmentDevelopmentalSource = this.mentalHealthGroup.get('marginalFunctionalImpairmentDevelopmentalSource').value;
    else
      this.detMentalHealthCondition.marginalFunctionalImpairmentDevelopmentalSource = null;

    //Question 7
    if (this.mentalHealthGroup.get('excludingSUDNonSPMIOrig').value !== null)
      this.detMentalHealthCondition.excludingSUDNonSPMIOrig = this.mentalHealthGroup.get('excludingSUDNonSPMIOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.excludingSUDNonSPMIOrig = null;

    if (this.mentalHealthGroup.get('excludingSUDNonSPMI').value !== null)
      this.detMentalHealthCondition.excludingSUDNonSPMI = this.mentalHealthGroup.get('excludingSUDNonSPMI').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.excludingSUDNonSPMI = null;

    if (this.mentalHealthGroup.get('excludingSUDNonSPMISource').value !== null)
      this.detMentalHealthCondition.excludingSUDNonSPMISource = this.mentalHealthGroup.get('excludingSUDNonSPMISource').value;
    else
      this.detMentalHealthCondition.excludingSUDNonSPMISource = null;

    //Question 8
    if (this.mentalHealthGroup.get('seriousMentalIllnessOrig').value !== null)
      this.detMentalHealthCondition.seriousMentalIllnessOrig = this.mentalHealthGroup.get('seriousMentalIllnessOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.seriousMentalIllnessOrig = null;

    if (this.mentalHealthGroup.get('seriousMentalIllness').value !== null)
      this.detMentalHealthCondition.seriousMentalIllness = this.mentalHealthGroup.get('seriousMentalIllness').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.seriousMentalIllness = null;

    if (this.mentalHealthGroup.get('seriousMentalIllnessSource').value !== null)
      this.detMentalHealthCondition.seriousMentalIllnessSource = this.mentalHealthGroup.get('seriousMentalIllnessSource').value;
    else
      this.detMentalHealthCondition.seriousMentalIllnessSource = null;

    //Question 9
    if (this.mentalHealthGroup.get('haveSEDDiagnosedBefore18Orig').value !== null)
      this.detMentalHealthCondition.haveSEDDiagnosedBefore18Orig = this.mentalHealthGroup.get('haveSEDDiagnosedBefore18Orig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.haveSEDDiagnosedBefore18Orig = null;

    if (this.mentalHealthGroup.get('haveSEDDiagnosedBefore18').value !== null)
      this.detMentalHealthCondition.haveSEDDiagnosedBefore18 = this.mentalHealthGroup.get('haveSEDDiagnosedBefore18').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.haveSEDDiagnosedBefore18 = null;

    if (this.mentalHealthGroup.get('haveSEDDiagnosedBefore18Source').value !== null)
      this.detMentalHealthCondition.haveSEDDiagnosedBefore18Source = this.mentalHealthGroup.get('haveSEDDiagnosedBefore18Source').value;
    else
      this.detMentalHealthCondition.haveSEDDiagnosedBefore18Source = null;

    //Question 10
    if (this.mentalHealthGroup.get('stabilizedForPlacementOrig').value !== null)
      this.detMentalHealthCondition.stabilizedForPlacementOrig = this.mentalHealthGroup.get('stabilizedForPlacementOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.stabilizedForPlacementOrig = null;

    if (this.mentalHealthGroup.get('stabilizedForPlacement').value !== null)
      this.detMentalHealthCondition.stabilizedForPlacement = this.mentalHealthGroup.get('stabilizedForPlacement').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.stabilizedForPlacement = null;

    if (this.mentalHealthGroup.get('stabilizedForPlacementSource').value !== null)
      this.detMentalHealthCondition.stabilizedForPlacementSource = this.mentalHealthGroup.get('stabilizedForPlacementSource').value;
    else
      this.detMentalHealthCondition.stabilizedForPlacementSource = null;

    if (this.mentalHealthGroup.get('stabilizedForPlacementExplain').value !== null)
      this.detMentalHealthCondition.stabilizedForPlacementExplain = this.mentalHealthGroup.get('stabilizedForPlacementExplain').value;
    else
      this.detMentalHealthCondition.stabilizedForPlacementExplain = null;

    //Question 11
    if (this.mentalHealthGroup.get('developmentalDisabilityOrig').value !== null)
      this.detMentalHealthCondition.developmentalDisabilityOrig = this.mentalHealthGroup.get('developmentalDisabilityOrig').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.developmentalDisabilityOrig = null;

    if (this.mentalHealthGroup.get('developmentalDisability').value !== null)
      this.detMentalHealthCondition.developmentalDisability = this.mentalHealthGroup.get('developmentalDisability').value == 'Y' ? 33 : 34;
    else
      this.detMentalHealthCondition.developmentalDisability = null;

    if (this.mentalHealthGroup.get('developmentalDisabilitySource').value !== null)
      this.detMentalHealthCondition.developmentalDisabilitySource = this.mentalHealthGroup.get('developmentalDisabilitySource').value;
    else
      this.detMentalHealthCondition.developmentalDisabilitySource = null;

    this.detMentalHealthCondition.detMentalHealthConditionID = this.detMentalHealthConditionId;
    this.detMentalHealthCondition.pactApplicationID = this.pactApplicationId;
    this.detMentalHealthCondition.userID = this.userData.optionUserId;
    this.detMentalHealthCondition.isMentalHealthConditionCompleted = this.isMentalHealthCompleted;

    //console.log('Save MentalHealth - ', this.detMentalHealthCondition);
    var stabilizedForPlacement = this.mentalHealthGroup.get('stabilizedForPlacement').value;

    if (stabilizedForPlacement === 'N') {
      const title = 'Clinical Review';
      const primaryMessage = `Application marked as 'Unable To Review' When Applicant's psychiatric condition `;
      const secondaryMessage = `is not sufficiently stabilized for placement. Do you still want to continue?`;
      const confirmButtonName = 'Yes';
      const dismissButtonName = 'No';

      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
        (positiveResponse) => {
          this.callSaveMentalHealthConditionService(value);
        },
        (negativeResponse) => {

        },
      );
    }
    else
      this.callSaveMentalHealthConditionService(value);

    //}

  }

  callSaveMentalHealthConditionService(value: number) {
    this.clinicalReviewDataSub = this.clinicalReviewService.saveDETMentalHealthCondition(this.detMentalHealthCondition)
      .subscribe(data => {
        //console.log('Success!', data);
        if (value === 0) {
          if (!this.toastrService.currentlyActive)
            this.toastrService.success("Mental Health Condition Data has been saved.");
        }

        this.populateMentalHealthCondition(data);
        //this.setNextTabVisibility();

        //Code added here as not working on SaveTabData Next Button Click
        if (value === 1) {
          if (this.clientCategoryType === 675 || this.clientCategoryType === 676)
            this.selectedTab = this.selectedTab + 1;
          else {
            if (data.stabilizedForPlacement === 34) {
              this.determinationSideNavService.setIsDeterminationSummaryDisabled(false);
              this.router.navigate(['/ds/determination-summary/', this.pactApplicationId]);
            }
            else if (data.stabilizedForPlacement === 33)
              this.selectedTab = this.selectedTab + 1;
            else if (data.stabilizedForPlacement === null && (data.currentMHDiagnosis === 34 || data.significantFunctionalImpairment === 34))
              this.selectedTab = this.selectedTab + 1;
          }
        }
        else if (value === 2) {
          this.router.navigate(['/ds/outcome/', this.pactApplicationId]);
        }
      },
        error => {
          throw new Error(error.message);
        });
  }

  validateMentalHealthForm() {
    var currentMHDiagnosis = this.mentalHealthGroup.get('currentMHDiagnosis').value;
    var currentMHDiagnosisOrCondition = this.mentalHealthGroup.get('currentMHDiagnosisOrCondition').value;
    var significantFunctionalImpairment = this.mentalHealthGroup.get('significantFunctionalImpairment').value;
    var significantFunctionalImpairmentExplain = this.mentalHealthGroup.get('significantFunctionalImpairmentExplain').value;
    var marginalFunctionalImpairmentPsychiatric = this.mentalHealthGroup.get('marginalFunctionalImpairmentPsychiatric').value;
    var marginalFunctionalImpairmentDevelopmental = this.mentalHealthGroup.get('marginalFunctionalImpairmentDevelopmental').value;
    var excludingSUDNonSPMI = this.mentalHealthGroup.get('excludingSUDNonSPMI').value;
    var haveSEDDiagnosedBefore18 = this.mentalHealthGroup.get('haveSEDDiagnosedBefore18').value;
    var stabilizedForPlacement = this.mentalHealthGroup.get('stabilizedForPlacement').value;
    var stabilizedForPlacementExplain = this.mentalHealthGroup.get('stabilizedForPlacementExplain').value;
    var developmentalDisability = this.mentalHealthGroup.get('developmentalDisability').value;
    var seriousMentalIllness = this.mentalHealthGroup.get('seriousMentalIllness').value;

    if (this.clientCategoryType === 671 || this.clientCategoryType === 672
      || this.clientCategoryType === 673 || this.clientCategoryType === 674) {

      if (currentMHDiagnosis === 'Y') {

        if (significantFunctionalImpairment === null) {
          this.callToastrService("Significant Function impairments is required.");
          return false;
        }

        if (significantFunctionalImpairment === 'N' &&
          (significantFunctionalImpairmentExplain === null || significantFunctionalImpairmentExplain === '')) {
          this.callToastrService("Significant Function impairments Explain is required.");
          return false;
        }
        else if (significantFunctionalImpairment === 'N' && significantFunctionalImpairmentExplain.trim().length < 2) {
          this.callToastrService("Significant Function impairments Explain should be at least 2 character long.");
          return false;
        }

        if (significantFunctionalImpairment === 'Y') {
          if (this.clientCategoryType === 672) {
            // if (haveSEDDiagnosedBefore18 === null && significantFunctionalImpairment === 'Y') {
            //   this.callToastrService("SED condition before 18 is required.");
            //   return false;
            // }
            if (haveSEDDiagnosedBefore18 === null) {
              this.callToastrService("SED condition before 18 is required.");
              return false;
            }
          }

          if (this.clientCategoryType === 673 || this.clientCategoryType === 674) {
            // if (significantFunctionalImpairment === 'N' && excludingSUDNonSPMI === null) {
            //   this.callToastrService("Mental health (non-SPMI) condition is required.");
            //   return false;
            // }
            if (excludingSUDNonSPMI === null) {
              this.callToastrService("Mental health (non-SPMI) condition is required.");
              return false;
            }
          }

          if (stabilizedForPlacement === null) {
            this.callToastrService("Stabilized For Placement is required.");
            return false;
          }

          if (stabilizedForPlacement === 'N' &&
            (stabilizedForPlacementExplain === null || stabilizedForPlacementExplain === '')) {
            this.callToastrService("Stabilized For Placement Explain is required.");
            return false;
          }
          else if (stabilizedForPlacement === 'N' && stabilizedForPlacementExplain.trim().length < 2) {
            this.callToastrService("Stabilized For Placement Explain should be at least 2 character long.");
            return false;
          }
        }

        if (this.clientCategoryType === 673 || this.clientCategoryType === 674) {
          if (developmentalDisability === null) {
            this.callToastrService("Developmental Disability is required.");
            return false;
          }
        }

      }

    }
    else if (this.clientCategoryType === 675 || this.clientCategoryType === 676) {

      if (this.supportNotOfferedByGenPopType === 34) {
        if (currentMHDiagnosisOrCondition === 'Y') {
          if (marginalFunctionalImpairmentPsychiatric === null) {
            this.callToastrService("Marginal Functional Impairments Psychiatric Diagnosis  is required.");
            return false;
          }
        }

        if (developmentalDisability === 'Y') {
          if (marginalFunctionalImpairmentDevelopmental === null) {
            this.callToastrService("Marginal Functional Impairments Developmental Disability is required.");
            return false;
          }
        }
      }
    }

    return true;
  }

  //#endregion

  //#region  SubstanceUse
  //Question 1 Selection
  loadSubstanceUseData() {
    if (this.detClinicalReviewInput.userID === null) {
      this.detClinicalReviewInput.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
      this.detClinicalReviewInput.summaryType = this.summaryType;
      this.detClinicalReviewInput.clientCategoryType = this.applicationDeterminationData.clientCategoryType;
      this.detClinicalReviewInput.userID = this.userData.optionUserId;
    }

    //console.log('DetClinicalReviewInput - ', this.detClinicalReviewInput);

    this.clinicalReviewDataSub = this.clinicalReviewService.getDETSubstanceUse(this.detClinicalReviewInput)
      .subscribe(
        res => {
          this.detSubstanceUse = res as DETSubstanceUse;
          this.populateSubstanceUse(this.detSubstanceUse);
        }
      );
  }

  //Populate Substance Use Form Data When Selected
  populateSubstanceUse(formData: DETSubstanceUse) {
    //console.log('Substance Use - ', this.detSubstanceUse);
    if (formData != null) {
      this.detSubstanceUse = formData;
      this.detSubstanceUseId = formData.detSubstanceUseID;
      this.isSubstanceUseCompleted = formData.isSubstanceUseCompleted;
      this.isMedicalConditionTabEnabled = formData.isSubstanceUseCompleted;

      //Question 1 Fields
      if (formData.haveSUD != null) {
        this.substanceUseGroup.controls['haveSUDOrig'].setValue(formData.haveSUDOrig == 33 ? 'Y' : 'N');
        this.substanceUseGroup.controls['haveSUD'].setValue(formData.haveSUD == 33 ? 'Y' : 'N');
        this.haveSUDOrigText = formData.haveSUDOrig == 33 ? 'Yes' : 'No';

        if (!this.isUsedSubstancesPast3MonthsDisabled) {
          this.substanceUseGroup.controls['usedSubstancesPast3Months'].disable();
        }
      }
      else {
        this.substanceUseGroup.controls['haveSUDOrig'].setValue(null);
        this.substanceUseGroup.controls['haveSUD'].setValue(null);
      }

      if (formData.haveSUDSource != null) {
        this.substanceUseGroup.controls['haveSUDSource'].setValue(formData.haveSUDSource);
        this.haveSUDSourceText = formData.haveSUDSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.substanceUseGroup.controls['haveSUDSource'].setValue(null);
        this.haveSUDSourceText = '';
      }

      //Question 2 Fields
      if (formData.usedSubstancesPast3Months != null) {
        this.substanceUseGroup.controls['usedSubstancesPast3MonthsOrig'].setValue(formData.usedSubstancesPast3MonthsOrig == 33 ? 'Y' : 'N');
        this.substanceUseGroup.controls['usedSubstancesPast3Months'].setValue(formData.usedSubstancesPast3Months == 33 ? 'Y' : 'N');
        this.usedSubstancesPast3MonthsOrigText = formData.usedSubstancesPast3MonthsOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.substanceUseGroup.controls['usedSubstancesPast3MonthsOrig'].setValue(null);
        this.substanceUseGroup.controls['usedSubstancesPast3Months'].setValue(null);
      }

      if (formData.haveSUDSource != null) {
        this.substanceUseGroup.controls['usedSubstancesPast3MonthsSource'].setValue(formData.usedSubstancesPast3MonthsSource);
        this.usedSubstancesPast3MonthsSourceText = formData.usedSubstancesPast3MonthsSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.substanceUseGroup.controls['usedSubstancesPast3MonthsSource'].setValue(null);
        this.usedSubstancesPast3MonthsSourceText = '';
      }

      //Question 3 Fields
      if (formData.sudEvidencePast5Years != null)
        this.substanceUseGroup.controls['sudEvidencePast5Years'].setValue(formData.sudEvidencePast5Years == 33 ? 'Y' : 'N');
      else {
        this.substanceUseGroup.controls['sudEvidencePast5YearsOrig'].setValue(null);
        this.substanceUseGroup.controls['sudEvidencePast5Years'].setValue(null);
      }

      if (formData.sudEvidencePast5YearsSource != null) {
        this.substanceUseGroup.controls['sudEvidencePast5YearsSource'].setValue(formData.sudEvidencePast5YearsSource);
        this.sudEvidencePast5YearsSourceText = formData.sudEvidencePast5YearsSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.substanceUseGroup.controls['sudEvidencePast5YearsSource'].setValue(null);
        this.sudEvidencePast5YearsSourceText = '';
      }

      //Question 4 Fields
      if (formData.sudRestrictionOfADL != null) {
        this.substanceUseGroup.controls['sudRestrictionOfADL'].setValue(formData.sudRestrictionOfADL == 33 ? 'Y' : 'N');
      }
      else {
        this.substanceUseGroup.controls['sudRestrictionOfADLOrig'].setValue(null);
        this.substanceUseGroup.controls['sudRestrictionOfADL'].setValue(null);
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].setValue(null);
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].disable();
      }

      if (formData.sudRestrictionOfADLSource != null) {
        this.substanceUseGroup.controls['sudRestrictionOfADLSource'].setValue(formData.sudRestrictionOfADLSource);
        this.sudRestrictionOfADLSourceText = formData.sudRestrictionOfADLSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.substanceUseGroup.controls['sudRestrictionOfADLSource'].setValue(null);
        this.sudRestrictionOfADLSourceText = '';
      }

      if (formData.sudRestrictionOfADLExplain != null && formData.sudRestrictionOfADLExplain !== '') {
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].setValue(formData.sudRestrictionOfADLExplain);
        this.sudRestrictionOfADLExlpainSourceText = 'Reviewer';
      }
      else {
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].setValue(null);
        this.sudRestrictionOfADLExlpainSourceText = '';
      }

      //Question 5 Fields
      if (formData.sudTreatmentCompletion != null)
        this.substanceUseGroup.controls['sudTreatmentCompletion'].setValue(formData.sudTreatmentCompletion == 33 ? 'Y' : 'N');
      else {
        this.substanceUseGroup.controls['sudTreatmentCompletionOrig'].setValue(null);
        this.substanceUseGroup.controls['sudTreatmentCompletion'].setValue(null);
      }

      if (formData.sudTreatmentCompletionSource != null) {
        this.substanceUseGroup.controls['sudTreatmentCompletionSource'].setValue(formData.sudTreatmentCompletionSource);
        this.sudTreatmentCompletionSourceText = formData.sudTreatmentCompletionSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.substanceUseGroup.controls['sudTreatmentCompletionSource'].setValue(null);
        this.sudTreatmentCompletionSourceText = '';
      }

    }
    else {
      this.detSubstanceUse = new DETSubstanceUse();
      this.detSubstanceUse.pactApplicationID = this.pactApplicationId;

      this.substanceUseGroup.controls['haveSUDOrig'].setValue(null);
      this.haveSUDOrigText = '';
      this.substanceUseGroup.controls['haveSUD'].setValue(null);
      this.substanceUseGroup.controls['haveSUDSource'].setValue(null);
      this.haveSUDSourceText = '';
      this.substanceUseGroup.controls['usedSubstancesPast3MonthsOrig'].setValue(null);
      this.usedSubstancesPast3MonthsOrigText = '';
      this.substanceUseGroup.controls['usedSubstancesPast3Months'].setValue(null);
      this.substanceUseGroup.controls['usedSubstancesPast3MonthsSource'].setValue(null);
      this.usedSubstancesPast3MonthsSourceText = '';
      this.substanceUseGroup.controls['sudEvidencePast5YearsOrig'].setValue(null);
      this.substanceUseGroup.controls['sudEvidencePast5Years'].setValue(null);
      this.substanceUseGroup.controls['sudEvidencePast5YearsSource'].setValue(null);
      this.sudEvidencePast5YearsSourceText = '';
      this.substanceUseGroup.controls['sudRestrictionOfADLOrig'].setValue(null);
      this.substanceUseGroup.controls['sudRestrictionOfADL'].setValue(null);
      this.substanceUseGroup.controls['sudRestrictionOfADLSource'].setValue(null);
      this.sudRestrictionOfADLSourceText = '';
      this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].setValue(null);
      this.substanceUseGroup.controls['sudTreatmentCompletionOrig'].setValue(null);
      this.substanceUseGroup.controls['sudTreatmentCompletion'].setValue(null);
      this.substanceUseGroup.controls['sudTreatmentCompletionSource'].setValue(null);
      this.sudTreatmentCompletionSourceText = '';

    }

    this.setTabsQuestionsVisibility();
  }

  //#region All Substance Use Question Selection Methods
  haveSUDSelected(selectedValue: string) {
    this.substanceUseGroup.controls['haveSUD'].setValue(selectedValue);

    if (this.substanceUseGroup.get('haveSUDOrig').value != null) {
      var haveSUDOrig = this.substanceUseGroup.get('haveSUDOrig').value;
      var haveSUDSource = haveSUDOrig == selectedValue ? 679 : 680;
      this.substanceUseGroup.controls['haveSUDSource'].setValue(haveSUDSource);
      this.haveSUDSourceText = haveSUDSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //Question 2 Selection
  usedSubstancesPast3MonthsSelected(selectedValue: string) {
    this.substanceUseGroup.controls['usedSubstancesPast3Months'].setValue(selectedValue);

    if (this.substanceUseGroup.get('usedSubstancesPast3MonthsOrig').value != null) {
      var usedSubstancesPast3MonthsOrig = this.substanceUseGroup.get('usedSubstancesPast3MonthsOrig').value;
      var usedSubstancesPast3MonthsSource = usedSubstancesPast3MonthsOrig == selectedValue ? 679 : 680;
      this.substanceUseGroup.controls['usedSubstancesPast3MonthsSource'].setValue(usedSubstancesPast3MonthsSource);
      this.usedSubstancesPast3MonthsSourceText = usedSubstancesPast3MonthsSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //Question 3 Selection
  sudRestrictionOfADLSelected(selectedValue: string) {
    this.substanceUseGroup.controls['sudRestrictionOfADL'].setValue(selectedValue);
    this.substanceUseGroup.controls['sudRestrictionOfADLSource'].setValue(680);
    this.sudRestrictionOfADLSourceText = 'Reviewer';

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  calculateSUDRestrictionOfADLExplainSource(value: string): void {
    if (value.length > 0)
      this.sudRestrictionOfADLExlpainSourceText = 'Reviewer';
    else
      this.sudRestrictionOfADLExlpainSourceText = '';
  }

  //Question 4 Selection
  sudTreatmentCompletionSelected(selectedValue: string) {
    this.substanceUseGroup.controls['sudTreatmentCompletion'].setValue(selectedValue);
    this.substanceUseGroup.controls['sudTreatmentCompletionSource'].setValue(680);
    this.sudTreatmentCompletionSourceText = 'Reviewer';

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //Question 5 Selection
  sudEvidencePast5YearsSelected(selectedValue: string) {
    this.substanceUseGroup.controls['sudEvidencePast5Years'].setValue(selectedValue);
    this.substanceUseGroup.controls['sudEvidencePast5YearsSource'].setValue(680);
    this.sudEvidencePast5YearsSourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //#endregion

  //Save Substance Use
  saveSubstanceUse(value: number) {
    //Question 1
    if (this.substanceUseGroup.get('haveSUDOrig').value != null)
      this.detSubstanceUse.haveSUDOrig = this.substanceUseGroup.get('haveSUDOrig').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.haveSUDOrig = null;

    if (this.substanceUseGroup.get('haveSUD').value != null)
      this.detSubstanceUse.haveSUD = this.substanceUseGroup.get('haveSUD').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.haveSUD = null;

    if (this.substanceUseGroup.get('haveSUDSource').value != null)
      this.detSubstanceUse.haveSUDSource = this.substanceUseGroup.get('haveSUDSource').value;
    else
      this.detSubstanceUse.haveSUDSource = null;

    //Question 2
    if (this.substanceUseGroup.get('usedSubstancesPast3MonthsOrig').value != null)
      this.detSubstanceUse.usedSubstancesPast3MonthsOrig = this.substanceUseGroup.get('usedSubstancesPast3MonthsOrig').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.usedSubstancesPast3MonthsOrig = null;

    if (this.substanceUseGroup.get('usedSubstancesPast3Months').value != null)
      this.detSubstanceUse.usedSubstancesPast3Months = this.substanceUseGroup.get('usedSubstancesPast3Months').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.usedSubstancesPast3Months = null;

    if (this.substanceUseGroup.get('usedSubstancesPast3MonthsSource').value != null)
      this.detSubstanceUse.usedSubstancesPast3MonthsSource = this.substanceUseGroup.get('usedSubstancesPast3MonthsSource').value;
    else
      this.detSubstanceUse.usedSubstancesPast3MonthsSource = null;

    //Question 3
    if (this.substanceUseGroup.get('sudEvidencePast5YearsOrig').value != null)
      this.detSubstanceUse.sudEvidencePast5YearsOrig = this.substanceUseGroup.get('sudEvidencePast5YearsOrig').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.sudEvidencePast5YearsOrig = null;

    if (this.substanceUseGroup.get('sudEvidencePast5Years').value != null)
      this.detSubstanceUse.sudEvidencePast5Years = this.substanceUseGroup.get('sudEvidencePast5Years').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.sudEvidencePast5YearsOrig = null;

    if (this.substanceUseGroup.get('sudEvidencePast5YearsSource').value != null)
      this.detSubstanceUse.sudEvidencePast5YearsSource = this.substanceUseGroup.get('sudEvidencePast5YearsSource').value;
    else
      this.detSubstanceUse.sudEvidencePast5YearsSource = null;

    //Question 4
    if (this.substanceUseGroup.get('sudRestrictionOfADLOrig').value !== null)
      this.detSubstanceUse.sudRestrictionOfADLOrig = this.substanceUseGroup.get('sudRestrictionOfADLOrig').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.sudRestrictionOfADLOrig = null;

    if (this.substanceUseGroup.get('sudRestrictionOfADL').value != null)
      this.detSubstanceUse.sudRestrictionOfADL = this.substanceUseGroup.get('sudRestrictionOfADL').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.sudRestrictionOfADL = null;

    if (this.substanceUseGroup.get('sudRestrictionOfADLSource').value != null)
      this.detSubstanceUse.sudRestrictionOfADLSource = this.substanceUseGroup.get('sudRestrictionOfADLSource').value;
    else
      this.detSubstanceUse.sudRestrictionOfADLSource = null;

    if (this.substanceUseGroup.get('sudRestrictionOfADLExplain').value != null)
      this.detSubstanceUse.sudRestrictionOfADLExplain = this.substanceUseGroup.get('sudRestrictionOfADLExplain').value;
    else
      this.detSubstanceUse.sudRestrictionOfADLExplain = null;

    //Question 5
    if (this.substanceUseGroup.get('sudTreatmentCompletionOrig').value != null)
      this.detSubstanceUse.sudTreatmentCompletionOrig = this.substanceUseGroup.get('sudTreatmentCompletionOrig').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.sudTreatmentCompletionOrig = null;

    if (this.substanceUseGroup.get('sudTreatmentCompletion').value != null)
      this.detSubstanceUse.sudTreatmentCompletion = this.substanceUseGroup.get('sudTreatmentCompletion').value == 'Y' ? 33 : 34;
    else
      this.detSubstanceUse.sudTreatmentCompletion = null;

    if (this.substanceUseGroup.get('sudTreatmentCompletionSource').value != null)
      this.detSubstanceUse.sudTreatmentCompletionSource = this.substanceUseGroup.get('sudTreatmentCompletionSource').value;
    else
      this.detSubstanceUse.sudTreatmentCompletionSource = null;

    this.detSubstanceUse.detSubstanceUseID = this.detSubstanceUseId;
    this.detSubstanceUse.pactApplicationID = this.pactApplicationId;
    this.detSubstanceUse.userID = this.userData.optionUserId;
    this.detSubstanceUse.isSubstanceUseCompleted = this.isSubstanceUseCompleted;

    //console.log('Data Passed ', this.detSubstanceUse);

    this.clinicalReviewDataSub = this.clinicalReviewService.saveDETSubstanceUse(this.detSubstanceUse)
      .subscribe(data => {
        //console.log('Success!', data);
        if (value === 0) {
          if (!this.toastrService.currentlyActive)
            this.toastrService.success("Substance Use Data has been saved.");
        }

        this.populateSubstanceUse(data);
        //this.setNextTabVisibility();

        if (value === 1)
          this.selectedTab = this.selectedTab + 1;
        else if (value === 2)
          this.selectedTab = this.selectedTab - 1;
      },
        error => {
          throw new Error(error.message);
        });
    //}

  }

  validateSubstanceUseForm() {
    var haveSUD = this.substanceUseGroup.get('haveSUD').value;
    var usedSubstancesPast3Months = this.substanceUseGroup.get('usedSubstancesPast3Months').value;
    var sudRestrictionOfADL = this.substanceUseGroup.get('sudRestrictionOfADL').value;
    var sudEvidencePast5Years = this.substanceUseGroup.get('sudEvidencePast5Years').value;
    var sudTreatmentCompletion = this.substanceUseGroup.get('sudTreatmentCompletion').value;
    var sudRestrictionOfADLExplain = this.substanceUseGroup.get('sudRestrictionOfADLExplain').value;

    if (haveSUD === "Y") {
      if (this.clientCategoryType === 671 || this.clientCategoryType === 672 || this.clientCategoryType === 673
        || this.clientCategoryType === 674) {
        if (sudRestrictionOfADL === null) {
          this.callToastrService("SUD Restriction of ADL is required.");
          return false;
        }

        if (sudRestrictionOfADL === 'N' &&
          (sudRestrictionOfADLExplain === null || sudRestrictionOfADLExplain === '')) {
          this.callToastrService("SUD Restriction of ADL Explain is required.");
          return false;
        }
        else if (sudRestrictionOfADL === 'N' && sudRestrictionOfADLExplain.trim().length < 2) {
          this.callToastrService("SUD Restriction of ADL Explain should be at least 2 character long.");
          return false;
        }

        if (haveSUD === 'Y' && usedSubstancesPast3Months === 'N') {
          if (sudTreatmentCompletion === null) {
            this.callToastrService("SUD Treatment Completion is required.");
            return false;
          }
        }
      }
      else if (this.clientCategoryType === 675 || this.clientCategoryType === 676) {
        if (sudTreatmentCompletion === null) {
          this.callToastrService("SUD Treatment Completion is required.");
          return false;
        }

        if (sudTreatmentCompletion === 'N') {
          if (sudEvidencePast5Years === null) {
            this.callToastrService("SUD Evidence Past 5 Year is required.");
            return false;
          }
        }
      }
    }

    return true;
  }

  //#endregion

  //#region Medical Condition
  loadMedicalConditionData() {
    if (this.detClinicalReviewInput.userID === null) {
      this.detClinicalReviewInput.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
      this.detClinicalReviewInput.summaryType = this.summaryType;
      this.detClinicalReviewInput.clientCategoryType = this.applicationDeterminationData.clientCategoryType;
      this.detClinicalReviewInput.userID = this.userData.optionUserId;
    }

    //console.log('DetClinicalReviewInput - ', this.detClinicalReviewInput);

    this.clinicalReviewDataSub = this.clinicalReviewService.getDETMedicalCondition(this.detClinicalReviewInput)
      .subscribe(
        res => {
          this.detMedicalCondition = res as DETMedicalCondition;
          this.populateMedicalCondition(this.detMedicalCondition);
        }
      );
  }

  //Populate Medical Condition Form Data When Selected
  populateMedicalCondition(formData: DETMedicalCondition) {
    //console.log('Medical Condition - ', formData);
    if (formData !== null) {
      this.detMedicalCondition = formData;
      this.detMedicalConditionId = formData.detMedicalConditionID;
      this.isMedicalConditionCompleted = formData.isMedicalConditionCompleted;

      if (this.isMedicalConditionCompleted) {
        if (this.clientCategoryType === 671 || this.clientCategoryType === 673 || this.clientCategoryType === 675) {
          this.isClinicalSummaryTabEnabled = true;
        }
        else {
          this.isClinicalAtRiskTabEnabled = true;
        }
      }

      //Question 1 Fields
      if (formData.isActiveInHASA != null) {
        this.medicalConditionGroup.controls['isActiveInHASAOrig'].setValue(formData.isActiveInHASAOrig == 33 ? 'Y' : 'N');
        this.medicalConditionGroup.controls['isActiveInHASA'].setValue(formData.isActiveInHASA == 33 ? 'Y' : 'N');
      }
      else {
        this.medicalConditionGroup.controls['isActiveInHASAOrig'].setValue(null);
        this.medicalConditionGroup.controls['isActiveInHASA'].setValue(null);
      }

      this.medicalConditionGroup.controls['isActiveInHASA'].disable();

      if (formData.isActiveHASASource != null) {
        this.medicalConditionGroup.controls['isActiveHASASource'].setValue(formData.isActiveHASASource);
        this.isActiveInHASASourceText = formData.isActiveHASASource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.medicalConditionGroup.controls['isActiveHASASource'].setValue(null);
        this.isActiveInHASASourceText = '';
      }

      //Question 2 Fields
      if (formData.haveHIVAIDSDocumented != null) {
        this.medicalConditionGroup.controls['haveHIVAIDSDocumentedOrig'].setValue(formData.haveHIVAIDSDocumentedOrig == 33 ? 'Y' : 'N');
        this.medicalConditionGroup.controls['haveHIVAIDSDocumented'].setValue(formData.haveHIVAIDSDocumented == 33 ? 'Y' : 'N');

        this.haveHIVAIDSDocumentedOrigText = formData.haveHIVAIDSDocumentedOrig == 33 ? 'Yes' : 'No';

        //Disable Question
        if (this.detMentalHealthCondition.currentMHDiagnosis === 34 && this.detSubstanceUse.haveSUD === 34)
          this.medicalConditionGroup.controls['haveHIVAIDSDocumented'].disable();
        else
          this.medicalConditionGroup.controls['haveHIVAIDSDocumented'].enable();
      }
      else {
        this.medicalConditionGroup.controls['haveHIVAIDSDocumentedOrig'].setValue(null);
        this.medicalConditionGroup.controls['haveHIVAIDSDocumented'].setValue(null);
      }


      if (formData.haveHIVAIDSDocumentedSource != null) {
        this.medicalConditionGroup.controls['haveHIVAIDSDocumentedSource'].setValue(formData.haveHIVAIDSDocumentedSource);
        this.haveHIVAIDSDocumentedSourceText = formData.haveHIVAIDSDocumentedSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.medicalConditionGroup.controls['haveHIVAIDSDocumentedSource'].setValue(null);
        this.haveHIVAIDSDocumentedSourceText = '';
      }

      //Question 3 Fields
      if (formData.haveCurrentMedicalDiagnosis != null)
        this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosis'].setValue(formData.haveCurrentMedicalDiagnosis == 33 ? 'Y' : 'N');
      else {
        this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosisOrig'].setValue(null);
        this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosis'].setValue(null);
      }

      if (formData.haveCurrentMedicalDiagnosisSource != null) {
        this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosisSource'].setValue(formData.haveCurrentMedicalDiagnosisSource);
        this.haveCurrentMedicalDiagnosisSourceText = formData.haveCurrentMedicalDiagnosisSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosisSource'].setValue(null);
        this.haveCurrentMedicalDiagnosisSourceText = '';
      }

      //Question 4 Fields
      if (formData.restrictionOfADL != null) {
        this.medicalConditionGroup.controls['restrictionOfADL'].setValue(formData.restrictionOfADL == 33 ? 'Y' : 'N');
      }
      else {
        this.medicalConditionGroup.controls['restrictionOfADLOrig'].setValue(null);
        this.medicalConditionGroup.controls['restrictionOfADL'].setValue(null);
      }

      if (formData.restrictionOfADLSource != null) {
        this.medicalConditionGroup.controls['restrictionOfADLSource'].setValue(formData.restrictionOfADLSource);
        this.restrictionOfADLSourceText = formData.restrictionOfADLSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.medicalConditionGroup.controls['restrictionOfADLSource'].setValue(null);
        this.restrictionOfADLSourceText = '';
      }

      //Question 5 Fields
      if (formData.haveChronicMedicalCondition != null)
        this.medicalConditionGroup.controls['haveChronicMedicalCondition'].setValue(formData.haveChronicMedicalCondition == 33 ? 'Y' : 'N');
      else {
        this.medicalConditionGroup.controls['haveChronicMedicalConditionOrig'].setValue(null);
        this.medicalConditionGroup.controls['haveChronicMedicalCondition'].setValue(null);
      }

      if (formData.haveChronicMedicalConditionSource != null) {
        this.medicalConditionGroup.controls['haveChronicMedicalConditionSource'].setValue(formData.haveChronicMedicalConditionSource);
        this.haveChronicMedicalConditionSourceText = formData.haveChronicMedicalConditionSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.medicalConditionGroup.controls['haveChronicMedicalConditionSource'].setValue(null);
        this.haveChronicMedicalConditionSourceText = '';
      }

      //Question 6 Fields
      if (formData.marginalFunctionalImpairmentMedical != null)
        this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].setValue(formData.marginalFunctionalImpairmentMedical == 33 ? 'Y' : 'N');
      else {
        this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedicalOrig'].setValue(null);
        this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].setValue(null);
      }

      if (formData.marginalFunctionalImpairmentMedicalSource != null) {
        this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedicalSource'].setValue(formData.marginalFunctionalImpairmentMedicalSource);
        this.marginalFunctionalImpairmentMedicalSourceText = formData.marginalFunctionalImpairmentMedicalSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedicalSource'].setValue(null);
        this.marginalFunctionalImpairmentMedicalSourceText = '';
      }

      //Question 7 Fields
      if (formData.haveNeuroCognitiveImpairment != null) {
        this.medicalConditionGroup.controls['haveNeuroCognitiveImpairmentOrig'].setValue(formData.haveNeuroCognitiveImpairmentOrig == 33 ? 'Y' : 'N');
        this.medicalConditionGroup.controls['haveNeuroCognitiveImpairment'].setValue(formData.haveNeuroCognitiveImpairment == 33 ? 'Y' : 'N');
        this.haveNeuroCognitiveImpairmentOrigText = formData.haveNeuroCognitiveImpairmentOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.medicalConditionGroup.controls['haveNeuroCognitiveImpairmentOrig'].setValue(null);
        this.medicalConditionGroup.controls['haveNeuroCognitiveImpairment'].setValue(null);
      }

      if (formData.haveNeuroCognitiveImpairmentSource != null) {
        this.medicalConditionGroup.controls['haveNeuroCognitiveImpairmentSource'].setValue(formData.haveNeuroCognitiveImpairmentSource);
        this.haveNeuroCognitiveImpairmentSourceText = formData.haveNeuroCognitiveImpairmentSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.medicalConditionGroup.controls['haveNeuroCognitiveImpairmentSource'].setValue(null);
        this.haveNeuroCognitiveImpairmentSourceText = '';
      }

      //Question 8 Fields
      if (formData.marginalFuntionalImpairmentNeuro != null)
        this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].setValue(formData.marginalFuntionalImpairmentNeuro == 33 ? 'Y' : 'N');
      else {
        this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuroOrig'].setValue(null);
        this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].setValue(null);
      }

      if (formData.marginalFuntionalImpairmentNeuroSource != null) {
        this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuroSource'].setValue(formData.marginalFuntionalImpairmentNeuroSource);
        this.marginalFuntionalImpairmentNeuroSourceText = formData.marginalFuntionalImpairmentNeuroSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuroSource'].setValue(null);
        this.marginalFuntionalImpairmentNeuroSourceText = '';
      }

    }
    else {
      this.detMedicalCondition = new DETMedicalCondition();
      this.detMedicalCondition.pactApplicationID = this.pactApplicationId;
      this.medicalConditionGroup.controls['isActiveInHASAOrig'].setValue(null);
      this.medicalConditionGroup.controls['isActiveInHASA'].setValue(null);
      this.medicalConditionGroup.controls['isActiveHASASource'].setValue(null);
      this.isActiveInHASASourceText = '';
      this.medicalConditionGroup.controls['haveHIVAIDSDocumentedOrig'].setValue(null);
      this.haveHIVAIDSDocumentedOrigText = '';
      this.medicalConditionGroup.controls['haveHIVAIDSDocumented'].setValue(null);
      this.medicalConditionGroup.controls['haveHIVAIDSDocumentedSource'].setValue(null);
      this.haveHIVAIDSDocumentedSourceText = '';
      this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosisOrig'].setValue(null);
      this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosis'].setValue(null);
      this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosisSource'].setValue(null);
      this.haveCurrentMedicalDiagnosisSourceText = '';
      this.medicalConditionGroup.controls['restrictionOfADLOrig'].setValue(null);
      this.medicalConditionGroup.controls['restrictionOfADL'].setValue(null);
      this.medicalConditionGroup.controls['restrictionOfADLSource'].setValue(null);
      this.restrictionOfADLSourceText = '';
      this.medicalConditionGroup.controls['haveChronicMedicalConditionOrig'].setValue(null);
      this.medicalConditionGroup.controls['haveChronicMedicalCondition'].setValue(null);
      this.medicalConditionGroup.controls['haveChronicMedicalConditionSource'].setValue(null);
      this.haveChronicMedicalConditionSourceText = '';
      this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedicalOrig'].setValue(null);
      this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].setValue(null);
      this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedicalSource'].setValue(null);
      this.marginalFunctionalImpairmentMedicalSourceText = '';
      this.medicalConditionGroup.controls['haveNeuroCognitiveImpairmentOrig'].setValue(null);
      this.medicalConditionGroup.controls['haveNeuroCognitiveImpairment'].setValue(null);
      this.medicalConditionGroup.controls['haveNeuroCognitiveImpairmentSource'].setValue(null);
      this.haveNeuroCognitiveImpairmentSourceText = '';
      this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuroOrig'].setValue(null);
      this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].setValue(null);
      this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuroSource'].setValue(null);
      this.marginalFuntionalImpairmentNeuroSourceText = '';
    }

    this.setTabsQuestionsVisibility();
  }

  //#region All Medical Condition Question Selection Methods
  //Question 2 Selection
  haveHIVAIDSDocumentedSelected(selectedValue: string) {
    this.medicalConditionGroup.controls['haveHIVAIDSDocumented'].setValue(selectedValue);

    if (this.medicalConditionGroup.get('haveHIVAIDSDocumentedOrig').value != null) {
      var haveHIVAIDSDocumentedOrig = this.medicalConditionGroup.get('haveHIVAIDSDocumentedOrig').value;
      var haveHIVAIDSDocumentedSource = haveHIVAIDSDocumentedOrig == selectedValue ? 679 : 680;
      this.medicalConditionGroup.controls['haveHIVAIDSDocumentedSource'].setValue(haveHIVAIDSDocumentedSource);
      this.haveHIVAIDSDocumentedSourceText = haveHIVAIDSDocumentedSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  haveCurrentMedicalDiagnosisSelected(selectedValue: string) {
    this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosis'].setValue(selectedValue);
    this.medicalConditionGroup.controls['haveCurrentMedicalDiagnosisSource'].setValue(680);
    this.haveCurrentMedicalDiagnosisSourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  medicalRestrictionOfADLSelected(selectedValue: string) {
    this.medicalConditionGroup.controls['restrictionOfADL'].setValue(selectedValue);
    this.medicalConditionGroup.controls['restrictionOfADLSource'].setValue(680);
    this.restrictionOfADLSourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  haveChronicMedicalConditionSelected(selectedValue: string) {
    this.medicalConditionGroup.controls['haveChronicMedicalCondition'].setValue(selectedValue);
    this.medicalConditionGroup.controls['haveChronicMedicalConditionSource'].setValue(680);
    this.haveChronicMedicalConditionSourceText = 'Reviewer';

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  haveNeuroCognitiveImpairmentSelected(selectedValue: string) {
    this.medicalConditionGroup.controls['haveNeuroCognitiveImpairment'].setValue(selectedValue);

    if (this.medicalConditionGroup.get('haveNeuroCognitiveImpairmentOrig').value != null) {
      var haveNeuroCognitiveImpairmentOrig = this.medicalConditionGroup.get('haveNeuroCognitiveImpairmentOrig').value;
      var haveNeuroCognitiveImpairmentSource = haveNeuroCognitiveImpairmentOrig == selectedValue ? 679 : 680;
      this.medicalConditionGroup.controls['haveNeuroCognitiveImpairmentSource'].setValue(haveNeuroCognitiveImpairmentSource);
      this.haveNeuroCognitiveImpairmentSourceText = haveNeuroCognitiveImpairmentSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  marginalFunctionalImpairmentMedicalSelected(selectedValue: string) {
    this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].setValue(selectedValue);
    this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedicalSource'].setValue(680);
    this.marginalFunctionalImpairmentMedicalSourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  marginalFuntionalImpairmentNeuroSelected(selectedValue: string) {
    this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].setValue(selectedValue);
    this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuroSource'].setValue(680);
    this.marginalFuntionalImpairmentNeuroSourceText = 'Reviewer';

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //#endregion

  //Save Medical Condition
  saveMedicalCondition(value: number) {
    //Question 1
    if (this.medicalConditionGroup.get('isActiveInHASAOrig').value != null)
      this.detMedicalCondition.isActiveInHASAOrig = this.medicalConditionGroup.get('isActiveInHASAOrig').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.isActiveInHASAOrig = null;

    if (this.medicalConditionGroup.get('isActiveInHASA').value != null)
      this.detMedicalCondition.isActiveInHASA = this.medicalConditionGroup.get('isActiveInHASA').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.isActiveInHASA = null;

    if (this.medicalConditionGroup.get('isActiveHASASource').value != null)
      this.detMedicalCondition.isActiveHASASource = this.medicalConditionGroup.get('isActiveHASASource').value;
    else
      this.detMedicalCondition.isActiveHASASource = null;

    //Question 2
    if (this.medicalConditionGroup.get('haveHIVAIDSDocumentedOrig').value != null)
      this.detMedicalCondition.haveHIVAIDSDocumentedOrig = this.medicalConditionGroup.get('haveHIVAIDSDocumentedOrig').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.haveHIVAIDSDocumentedOrig = null;

    if (this.medicalConditionGroup.get('haveHIVAIDSDocumented').value != null)
      this.detMedicalCondition.haveHIVAIDSDocumented = this.medicalConditionGroup.get('haveHIVAIDSDocumented').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.haveHIVAIDSDocumented = null;

    if (this.medicalConditionGroup.get('haveHIVAIDSDocumentedSource').value != null)
      this.detMedicalCondition.haveHIVAIDSDocumentedSource = this.medicalConditionGroup.get('haveHIVAIDSDocumentedSource').value;
    else
      this.detMedicalCondition.haveHIVAIDSDocumentedSource = null;

    //Question 3
    if (this.medicalConditionGroup.get('haveCurrentMedicalDiagnosisOrig').value != null)
      this.detMedicalCondition.haveCurrentMedicalDiagnosisOrig = this.medicalConditionGroup.get('haveCurrentMedicalDiagnosisOrig').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.haveCurrentMedicalDiagnosisOrig = null;

    if (this.medicalConditionGroup.get('haveCurrentMedicalDiagnosis').value != null)
      this.detMedicalCondition.haveCurrentMedicalDiagnosis = this.medicalConditionGroup.get('haveCurrentMedicalDiagnosis').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.haveCurrentMedicalDiagnosis = null;

    if (this.medicalConditionGroup.get('haveCurrentMedicalDiagnosisSource').value != null)
      this.detMedicalCondition.haveCurrentMedicalDiagnosisSource = this.medicalConditionGroup.get('haveCurrentMedicalDiagnosisSource').value;
    else
      this.detMedicalCondition.haveCurrentMedicalDiagnosisSource = null;

    //Question 4
    if (this.medicalConditionGroup.get('restrictionOfADLOrig').value != null)
      this.detMedicalCondition.restrictionOfADLOrig = this.medicalConditionGroup.get('restrictionOfADLOrig').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.restrictionOfADLOrig = null;

    if (this.medicalConditionGroup.get('restrictionOfADL').value != null)
      this.detMedicalCondition.restrictionOfADL = this.medicalConditionGroup.get('restrictionOfADL').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.restrictionOfADL = null;

    if (this.medicalConditionGroup.get('restrictionOfADLSource').value != null)
      this.detMedicalCondition.restrictionOfADLSource = this.medicalConditionGroup.get('restrictionOfADLSource').value;
    else
      this.detMedicalCondition.restrictionOfADLSource = null;

    //Question 5
    if (this.medicalConditionGroup.get('haveChronicMedicalConditionOrig').value != null)
      this.detMedicalCondition.haveChronicMedicalConditionOrig = this.medicalConditionGroup.get('haveChronicMedicalConditionOrig').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.haveChronicMedicalConditionOrig = null;

    if (this.medicalConditionGroup.get('haveChronicMedicalCondition').value != null)
      this.detMedicalCondition.haveChronicMedicalCondition = this.medicalConditionGroup.get('haveChronicMedicalCondition').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.haveChronicMedicalCondition = null;

    if (this.medicalConditionGroup.get('haveChronicMedicalConditionSource').value != null)
      this.detMedicalCondition.haveChronicMedicalConditionSource = this.medicalConditionGroup.get('haveChronicMedicalConditionSource').value;
    else
      this.detMedicalCondition.haveChronicMedicalConditionSource = null;

    //Question 6
    if (this.medicalConditionGroup.get('marginalFunctionalImpairmentMedicalOrig').value != null)
      this.detMedicalCondition.marginalFunctionalImpairmentMedicalOrig = this.medicalConditionGroup.get('marginalFunctionalImpairmentMedicalOrig').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.marginalFunctionalImpairmentMedicalOrig = null;

    if (this.medicalConditionGroup.get('marginalFunctionalImpairmentMedical').value != null)
      this.detMedicalCondition.marginalFunctionalImpairmentMedical = this.medicalConditionGroup.get('marginalFunctionalImpairmentMedical').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.marginalFunctionalImpairmentMedical = null;

    if (this.medicalConditionGroup.get('marginalFunctionalImpairmentMedicalSource').value != null)
      this.detMedicalCondition.marginalFunctionalImpairmentMedicalSource = this.medicalConditionGroup.get('marginalFunctionalImpairmentMedicalSource').value;
    else
      this.detMedicalCondition.marginalFunctionalImpairmentMedicalSource = null;

    //Question 7
    if (this.medicalConditionGroup.get('haveNeuroCognitiveImpairmentOrig').value != null)
      this.detMedicalCondition.haveNeuroCognitiveImpairmentOrig = this.medicalConditionGroup.get('haveNeuroCognitiveImpairmentOrig').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.haveNeuroCognitiveImpairmentOrig = null;

    if (this.medicalConditionGroup.get('haveNeuroCognitiveImpairment').value != null)
      this.detMedicalCondition.haveNeuroCognitiveImpairment = this.medicalConditionGroup.get('haveNeuroCognitiveImpairment').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.haveNeuroCognitiveImpairment = null;

    if (this.medicalConditionGroup.get('haveNeuroCognitiveImpairmentSource').value != null)
      this.detMedicalCondition.haveNeuroCognitiveImpairmentSource = this.medicalConditionGroup.get('haveNeuroCognitiveImpairmentSource').value;
    else
      this.detMedicalCondition.haveNeuroCognitiveImpairmentSource = null;

    //Question 8
    if (this.medicalConditionGroup.get('marginalFuntionalImpairmentNeuroOrig').value != null)
      this.detMedicalCondition.marginalFuntionalImpairmentNeuroOrig = this.medicalConditionGroup.get('marginalFuntionalImpairmentNeuroOrig').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.marginalFuntionalImpairmentNeuroOrig = null;

    if (this.medicalConditionGroup.get('marginalFuntionalImpairmentNeuro').value != null)
      this.detMedicalCondition.marginalFuntionalImpairmentNeuro = this.medicalConditionGroup.get('marginalFuntionalImpairmentNeuro').value == 'Y' ? 33 : 34;
    else
      this.detMedicalCondition.marginalFuntionalImpairmentNeuro = null;

    if (this.medicalConditionGroup.get('marginalFuntionalImpairmentNeuroSource').value != null)
      this.detMedicalCondition.marginalFuntionalImpairmentNeuroSource = this.medicalConditionGroup.get('marginalFuntionalImpairmentNeuroSource').value;
    else
      this.detMedicalCondition.marginalFuntionalImpairmentNeuroSource = null;

    this.detMedicalCondition.detMedicalConditionID = this.detMedicalConditionId;
    this.detMedicalCondition.pactApplicationID = this.pactApplicationId;
    this.detMedicalCondition.userID = this.userData.optionUserId;
    this.detMedicalCondition.isMedicalConditionCompleted = this.isMedicalConditionCompleted;

    //console.log('Data Passed ', this.detMedicalCondition);

    this.clinicalReviewDataSub = this.clinicalReviewService.saveDETMedicalCondition(this.detMedicalCondition)
      .subscribe(data => {
        //console.log('Success!', data);
        if (value === 0) {
          if (!this.toastrService.currentlyActive)
            this.toastrService.success("Medical Condition Data has been saved.");
        }

        this.populateMedicalCondition(data);
        //this.setNextTabVisibility();

        if (value === 1)
          this.selectedTab = this.selectedTab + 1;
        else if (value === 2)
          this.selectedTab = this.selectedTab - 1;

      },
        error => {
          throw new Error(error.message);
        });
    //}

  }

  validateMedicalConditionForm() {
    var isActiveInHASA = this.medicalConditionGroup.get('isActiveInHASA').value;
    var haveHIVAIDSDocumented = this.medicalConditionGroup.get('haveHIVAIDSDocumented').value;
    var haveCurrentMedicalDiagnosis = this.medicalConditionGroup.get('haveCurrentMedicalDiagnosis').value;
    var restrictionOfADL = this.medicalConditionGroup.get('restrictionOfADL').value;
    var haveChronicMedicalCondition = this.medicalConditionGroup.get('haveChronicMedicalCondition').value;
    var marginalFunctionalImpairmentMedical = this.medicalConditionGroup.get('marginalFunctionalImpairmentMedical').value;
    var haveNeuroCognitiveImpairment = this.medicalConditionGroup.get('haveNeuroCognitiveImpairment').value;
    var marginalFuntionalImpairmentNeuro = this.medicalConditionGroup.get('marginalFuntionalImpairmentNeuro').value;

    if (this.clientCategoryType === 673 || this.clientCategoryType === 674) {
      if (haveCurrentMedicalDiagnosis === null) {
        this.callToastrService("Current Medical Diagnosis is required.");
        return false;
      }

      if (restrictionOfADL === null) {
        this.callToastrService("Restirction of ADL is required.");
        return false;
      }
    }
    else if (this.clientCategoryType === 675 || this.clientCategoryType === 676) {

      if (this.supportNotOfferedByGenPopType === 34) {
        if (haveChronicMedicalCondition === null) {
          this.callToastrService("Chronic Medical Condition is required.");
          return false;
        }

        if (haveChronicMedicalCondition === 'Y') {
          if (marginalFunctionalImpairmentMedical === null) {
            this.callToastrService("Marginal Functional Impairment Medical is required.");
            return false;
          }
        }

        if (haveNeuroCognitiveImpairment === null) {
          this.callToastrService("Neurocognitive condition or impairment is required.");
          return false;
        }

        if (haveNeuroCognitiveImpairment === 'Y') {
          if (marginalFuntionalImpairmentNeuro === null) {
            this.callToastrService("Marginal Functional Impairment Neurocognitive condition is required.");
            return false;
          }
        }
      }
    }

    return true;
  }
  //#endregion 

  //#region At Risk
  loadAtRiskData() {
    if (this.detClinicalReviewInput.userID === null) {
      this.detClinicalReviewInput.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
      this.detClinicalReviewInput.summaryType = this.summaryType;
      this.detClinicalReviewInput.clientCategoryType = this.applicationDeterminationData.clientCategoryType;
      this.detClinicalReviewInput.userID = this.userData.optionUserId;
    }

    //console.log('DetClinicalReviewInput - ', this.detClinicalReviewInput);
    this.clinicalReviewDataSub = this.clinicalReviewService.getDETClinicalAtRisk(this.detClinicalReviewInput)
      .subscribe(
        res => {
          //console.log('DETMentalHealthCondition - ', res);
          this.detClinicalAtRisk = res as DETClinicalAtRisk;
          this.populateClinicalAtRisk(this.detClinicalAtRisk);
        }
      );
  }

  //Populate Mental Health Condition Form Data When Selected
  populateClinicalAtRisk(formData: DETClinicalAtRisk) {
    //console.log('Mental Health Condition- ', formData);
    if (formData != null) {
      this.detClinicalAtRiskId = formData.detClinicalAtRiskID;
      this.isClinicalAtRiskCompleted = formData.isClinicalAtRiskCompleted;
      this.isClinicalSummaryTabEnabled = formData.isClinicalAtRiskCompleted;

      //Question 1 Fields
      if (formData.isFunctionalImpairmentsMarked !== null) {
        this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarkedOrig'].setValue(formData.isFunctionalImpairmentsMarkedOrig == 33 ? 'Y' : 'N');
        this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarked'].setValue(formData.isFunctionalImpairmentsMarked == 33 ? 'Y' : 'N');

        this.isFunctionalImpairmentsMarkedOrigText = formData.isFunctionalImpairmentsMarkedOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarkedOrig'].setValue(null);
        this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarked'].setValue(null);

        this.isFunctionalImpairmentsMarkedOrigText = null;
      }

      if (formData.isFunctionalImpairmentsMarkedSource !== null) {
        this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarkedSource'].setValue(formData.isFunctionalImpairmentsMarkedSource);
        this.isFunctionalImpairmentsMarkedSourceText = formData.isFunctionalImpairmentsMarkedSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarkedSource'].setValue(null);
      }

      //Question 2 Fields
      if (formData.haveLimitedEducation !== null) {
        this.clinicalAtRiskGroup.controls['haveLimitedEducationOrig'].setValue(formData.haveLimitedEducationOrig == 33 ? 'Y' : 'N');
        this.clinicalAtRiskGroup.controls['haveLimitedEducation'].setValue(formData.haveLimitedEducation == 33 ? 'Y' : 'N');
        this.haveLimitedEducationOrigDescription = formData.haveLimitedEducationOrigDescription;
      }
      else {
        this.clinicalAtRiskGroup.controls['haveLimitedEducationOrig'].setValue(null);
        this.clinicalAtRiskGroup.controls['haveLimitedEducation'].setValue(null);
        this.haveLimitedEducationOrigDescription = null;
      }

      if (formData.haveLimitedEducationSource != null) {
        this.clinicalAtRiskGroup.controls['haveLimitedEducationSource'].setValue(formData.haveLimitedEducationSource);
        this.haveLimitedEducationSourceText = formData.haveLimitedEducationSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.clinicalAtRiskGroup.controls['haveLimitedEducationSource'].setValue(null);
        this.haveLimitedEducationSourceText = null;
      }

      //Question 3 Fields
      if (formData.haveLimitedEmpHistory != null) {
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistoryOrig'].setValue(formData.haveLimitedEmpHistoryOrig == 33 ? 'Y' : 'N');
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistory'].setValue(formData.haveLimitedEmpHistory == 33 ? 'Y' : 'N');
        this.haveLimitedEmpHistoryOrigDescription = formData.haveLimitedEmpHistoryOrigDescription;
      }
      else {
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistoryOrig'].setValue(null);
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistory'].setValue(null);
        this.haveLimitedEmpHistoryOrigDescription = null;
      }

      if (formData.haveLimitedEmpHistorySource != null) {
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistorySource'].setValue(formData.haveLimitedEmpHistorySource);
        this.haveLimitedEmpHistorySourceText = formData.haveLimitedEmpHistorySource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistorySource'].setValue(null);
        this.haveLimitedEmpHistorySourceText = null;
      }

      //Question 4 Fields
      if (formData.haveHistoryOfTrauma != null) {
        this.clinicalAtRiskGroup.controls['haveHistoryOfTraumaOrig'].setValue(formData.haveHistoryOfTraumaOrig == 33 ? 'Y' : 'N');
        this.clinicalAtRiskGroup.controls['haveHistoryOfTrauma'].setValue(formData.haveHistoryOfTrauma == 33 ? 'Y' : 'N');
        this.haveHistoryOfTraumaOrigText = formData.haveHistoryOfTraumaOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.clinicalAtRiskGroup.controls['haveHistoryOfTraumaOrig'].setValue(null);
        this.clinicalAtRiskGroup.controls['haveHistoryOfTrauma'].setValue(null);
        this.haveHistoryOfTraumaOrigText = null;
      }

      if (formData.haveHistoryOfTraumaSource != null) {
        this.clinicalAtRiskGroup.controls['haveHistoryOfTraumaSource'].setValue(formData.haveHistoryOfTraumaSource);
        this.haveHistoryOfTraumaSourceText = formData.haveHistoryOfTraumaSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.clinicalAtRiskGroup.controls['haveHistoryOfTraumaSource'].setValue(null);
        this.haveHistoryOfTraumaSourceText = null;
      }

      //Question 5 Fields
      if (formData.isInvolvedinTwoSystems != null) {
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystemsOrig'].setValue(formData.isInvolvedinTwoSystemsOrig == 33 ? 'Y' : 'N');
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystems'].setValue(formData.isInvolvedinTwoSystems == 33 ? 'Y' : 'N');
        this.isInvolvedinTwoSystemsOrigText = formData.isInvolvedinTwoSystemsOrig == 33 ? 'Yes' : 'No';
      }
      else {
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystemsOrig'].setValue(null);
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystems'].setValue(null);
        this.isInvolvedinTwoSystemsOrigText = null;
      }

      if (formData.isInvolvedinTwoSystemsSource != null) {
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystemsSource'].setValue(formData.isInvolvedinTwoSystemsSource);
        this.isInvolvedinTwoSystemsSourceText = formData.isInvolvedinTwoSystemsSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystemsSource'].setValue(null);
        this.isInvolvedinTwoSystemsSourceText = null;
      }

      //Question 6 Fields
      if (formData.otherRiskFactorIssues != null) {
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesOrig'].setValue(formData.otherRiskFactorIssuesOrig == 33 ? 'Y' : 'N');
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssues'].setValue(formData.otherRiskFactorIssues == 33 ? 'Y' : 'N');
      }
      else {
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesOrig'].setValue(null);
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssues'].setValue(null);
      }

      if (formData.otherRiskFactorIssuesSource != null) {
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesSource'].setValue(formData.otherRiskFactorIssuesSource);
        this.otherRiskFactorIssuesSourceText = formData.otherRiskFactorIssuesSource == 679 ? 'Application' : 'Reviewer';
      }
      else {
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesSource'].setValue(null);
        this.otherRiskFactorIssuesSourceText = '';
      }

      if (formData.otherRiskFactorIssuesExplain != null) {
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].setValue(formData.otherRiskFactorIssuesExplain);
        if (formData.otherRiskFactorIssuesExplain !== null || formData.otherRiskFactorIssuesExplain !== '')
          this.otherRiskFactorIssuesExplainSourceText = 'Reviewer';
        else
          this.otherRiskFactorIssuesExplainSourceText = '';
      }
      else {
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].setValue(null);
        this.otherRiskFactorIssuesExplainSourceText = '';
      }

    }
    else {
      this.detClinicalAtRisk = new DETClinicalAtRisk();
      this.detClinicalAtRisk.pactApplicationID = this.pactApplicationId;

      this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarkedOrig'].setValue(null);
      this.isFunctionalImpairmentsMarkedOrigText = '';
      this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarked'].setValue(null);
      this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarkedSource'].setValue(null);
      this.isFunctionalImpairmentsMarkedSourceText = '';
      this.clinicalAtRiskGroup.controls['haveLimitedEducationOrig'].setValue(null);
      this.haveLimitedEducationOrigDescription = '';
      this.clinicalAtRiskGroup.controls['haveLimitedEducation'].setValue(null);
      this.clinicalAtRiskGroup.controls['haveLimitedEducationSource'].setValue(null);
      this.haveLimitedEducationSourceText = '';
      this.clinicalAtRiskGroup.controls['haveLimitedEmpHistoryOrig'].setValue(null);
      this.haveLimitedEmpHistoryOrigDescription = '';
      this.clinicalAtRiskGroup.controls['haveLimitedEmpHistory'].setValue(null);
      this.clinicalAtRiskGroup.controls['haveLimitedEmpHistorySource'].setValue(null);
      this.haveLimitedEmpHistorySourceText = '';
      this.clinicalAtRiskGroup.controls['haveHistoryOfTraumaOrig'].setValue(null);
      this.clinicalAtRiskGroup.controls['haveHistoryOfTrauma'].setValue(null);
      this.clinicalAtRiskGroup.controls['haveHistoryOfTraumaSource'].setValue(null);
      this.haveHistoryOfTraumaSourceText = '';
      this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystemsOrig'].setValue(null);
      this.isInvolvedinTwoSystemsOrigText = '';
      this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystems'].setValue(null);
      this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystemsSource'].setValue(null);
      this.isInvolvedinTwoSystemsSourceText = '';
      this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesOrig'].setValue(null);
      this.clinicalAtRiskGroup.controls['otherRiskFactorIssues'].setValue(null);
      this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesSource'].setValue(null);
      this.otherRiskFactorIssuesSourceText = '';
      this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].setValue(null);
      this.clinicalAtRiskGroup.controls['isClinicalAtRiskCompleted'].setValue(null);
    }

    this.setTabsQuestionsVisibility();

  }

  //#region All At Risk Question Selection Methods
  //Question 1
  isFunctionalImpairmentsMarkedSelected(selectedValue: string) {
    this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarked'].setValue(selectedValue);

    if (this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarkedOrig').value != null) {
      var isFunctionalImpairmentsMarkedOrig = this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarkedOrig').value;
      var isFunctionalImpairmentsMarkedSource = isFunctionalImpairmentsMarkedOrig == selectedValue ? 679 : 680;
      this.clinicalAtRiskGroup.controls['isFunctionalImpairmentsMarkedSource'].setValue(isFunctionalImpairmentsMarkedSource);
      this.isFunctionalImpairmentsMarkedSourceText = isFunctionalImpairmentsMarkedSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //Question 2
  haveLimitedEducationSelected(selectedValue: string) {
    this.clinicalAtRiskGroup.controls['haveLimitedEducation'].setValue(selectedValue);

    if (this.clinicalAtRiskGroup.get('haveLimitedEducationOrig').value != null) {
      var haveLimitedEducationOrig = this.clinicalAtRiskGroup.get('haveLimitedEducationOrig').value;
      var haveLimitedEducationSource = haveLimitedEducationOrig == selectedValue ? 679 : 680;
      this.clinicalAtRiskGroup.controls['haveLimitedEducationSource'].setValue(haveLimitedEducationSource);
      this.haveLimitedEducationSourceText = haveLimitedEducationSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //Question 3
  haveLimitedEmpHistorySelected(selectedValue: string) {
    this.clinicalAtRiskGroup.controls['haveLimitedEmpHistory'].setValue(selectedValue);

    if (this.clinicalAtRiskGroup.get('haveLimitedEmpHistoryOrig').value != null) {
      var haveLimitedEmpHistoryOrig = this.clinicalAtRiskGroup.get('haveLimitedEmpHistoryOrig').value;
      var haveLimitedEmpHistorySource = haveLimitedEmpHistoryOrig == selectedValue ? 679 : 680;
      this.clinicalAtRiskGroup.controls['haveLimitedEmpHistorySource'].setValue(haveLimitedEmpHistorySource);
      this.haveLimitedEmpHistorySourceText = haveLimitedEmpHistorySource == 679 ? 'Application' : 'Reviewer';
    }

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //Question 4
  haveHistoryOfTraumaSelected(selectedValue: string) {
    this.clinicalAtRiskGroup.controls['haveHistoryOfTrauma'].setValue(selectedValue);

    if (this.clinicalAtRiskGroup.get('haveHistoryOfTraumaOrig').value != null) {
      var haveHistoryOfTraumaOrig = this.clinicalAtRiskGroup.get('haveHistoryOfTraumaOrig').value;
      var haveHistoryOfTraumaSource = haveHistoryOfTraumaOrig == selectedValue ? 679 : 680;
      this.clinicalAtRiskGroup.controls['haveHistoryOfTraumaSource'].setValue(haveHistoryOfTraumaSource);
      this.haveHistoryOfTraumaSourceText = haveHistoryOfTraumaSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //Question 5
  isInvolvedinTwoSystemsSelected(selectedValue: string) {
    this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystems'].setValue(selectedValue);

    if (this.clinicalAtRiskGroup.get('isInvolvedinTwoSystemsOrig').value != null) {
      var isInvolvedinTwoSystemsOrig = this.clinicalAtRiskGroup.get('isInvolvedinTwoSystemsOrig').value;
      var isInvolvedinTwoSystemsSource = isInvolvedinTwoSystemsOrig == selectedValue ? 679 : 680;
      this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystemsSource'].setValue(isInvolvedinTwoSystemsSource);
      this.isInvolvedinTwoSystemsSourceText = isInvolvedinTwoSystemsSource == 679 ? 'Application' : 'Reviewer';
    }

    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  //Question 5
  otherRiskFactorIssuesSelected(selectedValue: string) {
    this.clinicalAtRiskGroup.controls['otherRiskFactorIssues'].setValue(selectedValue);
    this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesSource'].setValue(680);
    this.otherRiskFactorIssuesSourceText = 'Reviewer';

    this.setTabsQuestionsVisibility();
    this.setNextTabVisibility();
    this.disableOtherNavigationItemsOnClinicalChange();
  }

  calculateOtherRiskFactorIssuesExplainSource(value: string): void {
    if (value.length > 0)
      this.otherRiskFactorIssuesExplainSourceText = 'Reviewer';
    else
      this.otherRiskFactorIssuesExplainSourceText = '';
  }

  //#endregion

  //Save At Risk
  saveAtRisk(value: number) {
    //Question 1
    if (this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarkedOrig').value !== null)
      this.detClinicalAtRisk.isFunctionalImpairmentsMarkedOrig = this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarkedOrig').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.isFunctionalImpairmentsMarkedOrig = null;

    if (this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarked').value !== null)
      this.detClinicalAtRisk.isFunctionalImpairmentsMarked = this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarked').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.isFunctionalImpairmentsMarked = null;

    if (this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarkedSource').value !== null)
      this.detClinicalAtRisk.isFunctionalImpairmentsMarkedSource = this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarkedSource').value;
    else
      this.detClinicalAtRisk.isFunctionalImpairmentsMarkedSource = null;

    //Question 2
    if (this.clinicalAtRiskGroup.get('haveLimitedEducationOrig').value !== null)
      this.detClinicalAtRisk.haveLimitedEducationOrig = this.clinicalAtRiskGroup.get('haveLimitedEducationOrig').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.haveLimitedEducationOrig = null;

    if (this.clinicalAtRiskGroup.get('haveLimitedEducation').value !== null)
      this.detClinicalAtRisk.haveLimitedEducation = this.clinicalAtRiskGroup.get('haveLimitedEducation').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.haveLimitedEducation = null;

    if (this.clinicalAtRiskGroup.get('haveLimitedEducationSource').value !== null)
      this.detClinicalAtRisk.haveLimitedEducationSource = this.clinicalAtRiskGroup.get('haveLimitedEducationSource').value;
    else
      this.detClinicalAtRisk.haveLimitedEducationSource = null;

    //Question 3
    if (this.clinicalAtRiskGroup.get('haveLimitedEmpHistoryOrig').value !== null)
      this.detClinicalAtRisk.haveLimitedEmpHistoryOrig = this.clinicalAtRiskGroup.get('haveLimitedEmpHistoryOrig').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.haveLimitedEmpHistoryOrig = null;

    if (this.clinicalAtRiskGroup.get('haveLimitedEmpHistory').value !== null)
      this.detClinicalAtRisk.haveLimitedEmpHistory = this.clinicalAtRiskGroup.get('haveLimitedEmpHistory').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.haveLimitedEmpHistory = null;

    if (this.clinicalAtRiskGroup.get('haveLimitedEmpHistorySource').value !== null)
      this.detClinicalAtRisk.haveLimitedEmpHistorySource = this.clinicalAtRiskGroup.get('haveLimitedEmpHistorySource').value;
    else
      this.detClinicalAtRisk.haveLimitedEmpHistorySource = null;

    //Question 4
    if (this.clinicalAtRiskGroup.get('haveHistoryOfTraumaOrig').value !== null)
      this.detClinicalAtRisk.haveHistoryOfTraumaOrig = this.clinicalAtRiskGroup.get('haveHistoryOfTraumaOrig').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.haveHistoryOfTraumaOrig = null;

    if (this.clinicalAtRiskGroup.get('haveHistoryOfTrauma').value !== null)
      this.detClinicalAtRisk.haveHistoryOfTrauma = this.clinicalAtRiskGroup.get('haveHistoryOfTrauma').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.haveHistoryOfTrauma = null;

    if (this.clinicalAtRiskGroup.get('haveHistoryOfTraumaSource').value !== null)
      this.detClinicalAtRisk.haveHistoryOfTraumaSource = this.clinicalAtRiskGroup.get('haveHistoryOfTraumaSource').value;
    else
      this.detClinicalAtRisk.haveHistoryOfTraumaSource = null;

    //Question 5
    if (this.clinicalAtRiskGroup.get('isInvolvedinTwoSystemsOrig').value !== null)
      this.detClinicalAtRisk.isInvolvedinTwoSystemsOrig = this.clinicalAtRiskGroup.get('isInvolvedinTwoSystemsOrig').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.isInvolvedinTwoSystemsOrig = null;

    if (this.clinicalAtRiskGroup.get('isInvolvedinTwoSystems').value !== null)
      this.detClinicalAtRisk.isInvolvedinTwoSystems = this.clinicalAtRiskGroup.get('isInvolvedinTwoSystems').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.isInvolvedinTwoSystems = null;

    if (this.clinicalAtRiskGroup.get('isInvolvedinTwoSystemsSource').value !== null)
      this.detClinicalAtRisk.isInvolvedinTwoSystemsSource = this.clinicalAtRiskGroup.get('isInvolvedinTwoSystemsSource').value;
    else
      this.detClinicalAtRisk.isInvolvedinTwoSystemsSource = null;

    //Question 7
    if (this.clinicalAtRiskGroup.get('otherRiskFactorIssuesOrig').value !== null)
      this.detClinicalAtRisk.otherRiskFactorIssuesOrig = this.clinicalAtRiskGroup.get('otherRiskFactorIssuesOrig').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.otherRiskFactorIssuesOrig = null;

    if (this.clinicalAtRiskGroup.get('otherRiskFactorIssues').value !== null)
      this.detClinicalAtRisk.otherRiskFactorIssues = this.clinicalAtRiskGroup.get('otherRiskFactorIssues').value == 'Y' ? 33 : 34;
    else
      this.detClinicalAtRisk.otherRiskFactorIssues = null;

    if (this.clinicalAtRiskGroup.get('otherRiskFactorIssuesSource').value !== null)
      this.detClinicalAtRisk.otherRiskFactorIssuesSource = this.clinicalAtRiskGroup.get('otherRiskFactorIssuesSource').value;
    else
      this.detClinicalAtRisk.otherRiskFactorIssuesSource = null;

    if (this.clinicalAtRiskGroup.get('otherRiskFactorIssuesExplain').value !== null)
      this.detClinicalAtRisk.otherRiskFactorIssuesExplain = this.clinicalAtRiskGroup.get('otherRiskFactorIssuesExplain').value;
    else
      this.detClinicalAtRisk.otherRiskFactorIssuesExplain = null;

    this.detClinicalAtRisk.detClinicalAtRiskID = this.detClinicalAtRiskId;
    this.detClinicalAtRisk.pactApplicationID = this.pactApplicationId;
    this.detClinicalAtRisk.userID = this.userData.optionUserId;
    this.detClinicalAtRisk.isClinicalAtRiskCompleted = this.isClinicalAtRiskCompleted;

    //console.log('Save At Risk - ', this.detClinicalAtRisk);

    this.clinicalReviewDataSub = this.clinicalReviewService.saveDETClinicalAtRisk(this.detClinicalAtRisk)
      .subscribe(data => {
        //console.log('Success!', data);

        if (value === 0) {
          if (!this.toastrService.currentlyActive)
            this.toastrService.success("Clinical At Risk Data has been saved.");
        }

        this.populateClinicalAtRisk(data);

        if (value === 1)
          this.selectedTab = this.selectedTab + 1;
        else if (value === 2)
          this.selectedTab = this.selectedTab - 1;
      },
        error => {
          throw new Error(error.message);
        });
    //}

  }

  validateAtRiskForm() {

    var isFunctionalImpairmentsMarked = this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarked').value;
    var otherRiskFactorIssues = this.clinicalAtRiskGroup.get('otherRiskFactorIssues').value;
    var otherRiskFactorIssuesExplain = this.clinicalAtRiskGroup.get('otherRiskFactorIssuesExplain').value;

    if (isFunctionalImpairmentsMarked === 'Y') {
      if (this.clientCategoryType === 672 || this.clientCategoryType === 674 || this.clientCategoryType === 676) {
        if (otherRiskFactorIssues === null) {
          this.callToastrService("Other serious issues Risk Factor is required.");
          return false;
        }

        if (otherRiskFactorIssues === 'Y' &&
          (otherRiskFactorIssuesExplain === null || otherRiskFactorIssuesExplain === '')) {
          this.callToastrService("Other serious issues Risk Factor Explain is required.");
          return false;
        }
        else if (otherRiskFactorIssues === 'Y' && otherRiskFactorIssuesExplain.trim().length < 2) {
          this.callToastrService("Other serious issues Risk Factor Explain should be at least 2 character long.");
          return false;
        }
      }
    }

    return true;
  }

  //#endregion

  setTabsQuestionsVisibility() {
    //Mental Health Condition
    if (this.selectedTab === 0) {

      if (this.clientCategoryType === 671 || this.clientCategoryType === 672
        || this.clientCategoryType === 673 || this.clientCategoryType === 674) {
        //BRD 3.1.2.5 - Question 1
        if (this.mentalHealthGroup.get('currentMHDiagnosis').value === 'N') {

          this.mentalHealthGroup.controls['significantFunctionalImpairment'].disable();
          this.isSignificantFunctionalImpairmentDisabled = true;
          this.mentalHealthGroup.controls['significantFunctionalImpairment'].setValue(null);
          this.mentalHealthGroup.controls['significantFunctionalImpairmentSource'].setValue(null);
          this.significantFunctionalImpairmentSourceText = null;
          this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].disable();
          this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].setValue(null);
          this.significantFunctionalImpairmentExplainSourceText = null;

          this.mentalHealthGroup.controls['seriousMentalIllness'].disable();
          this.isSeriousMentalIllnessDisabled = true;
          var seriousMentalIllnessOrig = this.mentalHealthGroup.get('seriousMentalIllnessOrig').value;
          this.mentalHealthGroup.controls['seriousMentalIllness'].setValue(seriousMentalIllnessOrig);
          this.mentalHealthGroup.controls['seriousMentalIllnessSource'].setValue(679);
          this.seriousMentalIllnessSourceText = 'Application';

          this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].disable();
          this.isHaveSEDDiagnosedBefore18Disabled = true;
          this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].setValue(null);
          this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Source'].setValue(null);
          this.haveSEDDiagnosedBefore18SourceText = null;

          this.mentalHealthGroup.controls['excludingSUDNonSPMI'].disable();
          this.isExcludingSUDNonSPMIDisabled = true;
          this.mentalHealthGroup.controls['excludingSUDNonSPMI'].setValue(null);
          //this.mentalHealthGroup.controls['excludingSUDNonSPMISource'].setValue(null);
          this.excludingSUDNonSPMISourceText = null;

          this.mentalHealthGroup.controls['stabilizedForPlacement'].disable();
          this.isStabilizedForPlacementDisabled = true;
          this.mentalHealthGroup.controls['stabilizedForPlacement'].setValue(null);
          this.mentalHealthGroup.controls['stabilizedForPlacementSource'].setValue(null);
          this.stabilizedForPlacementSourceText = null;
          this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].disable();
          this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].setValue(null);
          this.stabilizedForPlacementExplainSourceText = null;
        }
        else {
          this.mentalHealthGroup.controls['significantFunctionalImpairment'].enable();
          this.isSignificantFunctionalImpairmentDisabled = false;
          this.mentalHealthGroup.controls['seriousMentalIllness'].enable();
          this.isSeriousMentalIllnessDisabled = false;
          this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].enable();
          this.isHaveSEDDiagnosedBefore18Disabled = false;
          this.mentalHealthGroup.controls['excludingSUDNonSPMI'].enable();
          this.isExcludingSUDNonSPMIDisabled = false;
          this.mentalHealthGroup.controls['stabilizedForPlacement'].enable();
          this.isStabilizedForPlacementDisabled = false;
        }

        //BRD 3.1.2.9 - Question 2
        if (this.mentalHealthGroup.get('significantFunctionalImpairment').value === 'N') {
          this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].enable();

          this.mentalHealthGroup.controls['seriousMentalIllness'].setValue("N");
          this.mentalHealthGroup.controls['seriousMentalIllnessSource'].setValue(679);
          this.seriousMentalIllnessSourceText = 'Application';

          this.mentalHealthGroup.controls['seriousMentalIllness'].disable();
          this.isSeriousMentalIllnessDisabled = true;

          //BRD 3.2.4.5 and 3.4.5.4
          //if (this.clientCategoryType === 672 || this.clientCategoryType === 674) {
          this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].disable();
          this.isHaveSEDDiagnosedBefore18Disabled = true;
          this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].setValue(null);
          this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18Source'].setValue(null);
          this.haveSEDDiagnosedBefore18SourceText = null;

          this.mentalHealthGroup.controls['excludingSUDNonSPMI'].disable();
          this.isExcludingSUDNonSPMIDisabled = true;
          this.mentalHealthGroup.controls['excludingSUDNonSPMI'].setValue(null);
          //this.mentalHealthGroup.controls['excludingSUDNonSPMISource'].setValue(null);
          this.excludingSUDNonSPMISourceText = null;

          this.mentalHealthGroup.controls['stabilizedForPlacement'].disable();
          this.isStabilizedForPlacementDisabled = true;
          this.mentalHealthGroup.controls['stabilizedForPlacement'].setValue(null);
          this.mentalHealthGroup.controls['stabilizedForPlacementSource'].setValue(null);
          this.stabilizedForPlacementSourceText = null;
          this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].disable();
          this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].setValue(null);
          this.stabilizedForPlacementExplainSourceText = null;
          //}
        }
        else if (this.mentalHealthGroup.get('significantFunctionalImpairment').value === 'Y') {
          this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].disable();
          this.mentalHealthGroup.controls['significantFunctionalImpairmentExplain'].setValue(null);
          this.significantFunctionalImpairmentExplainSourceText = '';

          var seriousMentalIllnessOrig = this.mentalHealthGroup.get('seriousMentalIllnessOrig').value;
          if (seriousMentalIllnessOrig != null) {
            this.mentalHealthGroup.controls['seriousMentalIllness'].enable();
            this.isSeriousMentalIllnessDisabled = false;
          }
          else {
            this.mentalHealthGroup.controls['seriousMentalIllness'].setValue(null);
            this.mentalHealthGroup.controls['seriousMentalIllness'].enable();
            this.isSeriousMentalIllnessDisabled = false;
            this.mentalHealthGroup.controls['seriousMentalIllnessSource'].setValue(null);
          }

          //BRD 3.2.4.5 and 3.4.5.4
          //if (this.clientCategoryType === 672 || this.clientCategoryType === 674) {
          this.mentalHealthGroup.controls['haveSEDDiagnosedBefore18'].enable();
          this.isHaveSEDDiagnosedBefore18Disabled = false;
          this.mentalHealthGroup.controls['excludingSUDNonSPMI'].enable();
          this.isExcludingSUDNonSPMIDisabled = false;
          this.mentalHealthGroup.controls['stabilizedForPlacement'].enable();
          this.isStabilizedForPlacementDisabled = false;
          //}
        }

        //BRD 3.1.2.15
        // if (this.clientCategoryType === 671) {
        //   if (this.mentalHealthGroup.get('currentMHDiagnosis').value === 'N') {
        //     this.mentalHealthGroup.controls['stabilizedForPlacement'].disable();
        //     this.isStabilizedForPlacementDisabled = true;
        //     this.mentalHealthGroup.controls['stabilizedForPlacement'].setValue(null);
        //     this.mentalHealthGroup.controls['stabilizedForPlacementSource'].setValue(null);
        //     this.stabilizedForPlacementSourceText = null;
        //     this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].disable();
        //     this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].setValue(null);
        //     this.stabilizedForPlacementExplainSourceText = null;
        //   }
        //   else {
        //     this.mentalHealthGroup.controls['stabilizedForPlacement'].enable();
        //     this.isStabilizedForPlacementDisabled = false;
        //   }
        // }


        //BRD 3.1.2.16 - Question 4 
        if (this.mentalHealthGroup.get('stabilizedForPlacement').value === 'N') {
          this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].enable();

          this.isSubstanceUseTabEnabled = this.isMedicalConditionTabEnabled = this.isClinicalAtRiskTabEnabled =
            this.isClinicalSummaryTabEnabled = false;
        }
        else {
          // this.isSubstanceUseTabEnabled = this.isMedicalConditionTabEnabled = this.isClinicalAtRiskTabEnabled =
          //   this.isClinicalSummaryTabEnabled = true;
          this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].disable();
          this.mentalHealthGroup.controls['stabilizedForPlacementExplain'].setValue(null);
          this.stabilizedForPlacementExplainSourceText = null;
        }

        // //BRD 3.3.2.15
        // if (this.clientCategoryType === 673 || this.clientCategoryType === 674) {
        //   var isExcludingSUDNonSPMIEnabled: boolean = false;
        //   if (this.mentalHealthGroup.get('currentMHDiagnosis').value === 'Y')
        //     isExcludingSUDNonSPMIEnabled = true;


        //   //console.log('isExcludingSUDNonSPMIEnabled - ', isExcludingSUDNonSPMIEnabled);
        //   if (isExcludingSUDNonSPMIEnabled) {
        //     this.mentalHealthGroup.controls['excludingSUDNonSPMI'].enable();
        //     this.isExcludingSUDNonSPMIDisabled = false;
        //   }
        //   else {
        //     this.mentalHealthGroup.controls['excludingSUDNonSPMI'].disable();
        //     this.isExcludingSUDNonSPMIDisabled = true;
        //     this.mentalHealthGroup.controls['excludingSUDNonSPMI'].setValue(null);
        //     this.mentalHealthGroup.controls['excludingSUDNonSPMISource'].setValue(null);
        //     this.excludingSUDNonSPMISourceText = null;
        //   }
        // }
      }

      //BRD 3.5.2.0
      if (this.clientCategoryType === 675 || this.clientCategoryType === 676) {
        if (this.supportNotOfferedByGenPopType === 33) {
          this.mentalHealthGroup.controls['currentMHDiagnosisOrCondition'].disable();
          this.isCurrentMHDiagnosisOrConditionDisabled = true;
          this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].disable();
          this.isMarginalFunctionalImpairmentPsychiatricDisabled = true;
          this.mentalHealthGroup.controls['developmentalDisability'].disable();
          this.isDevelopmentalDisabilityDisabled = true;
          this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].disable();
          this.isMarginalFunctionalImpairmentDevelopmentalDisabled = true;
        }
        else {
          this.mentalHealthGroup.controls['currentMHDiagnosisOrCondition'].enable();
          this.isCurrentMHDiagnosisOrConditionDisabled = false;
          this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].enable();
          this.isMarginalFunctionalImpairmentPsychiatricDisabled = false;
          this.mentalHealthGroup.controls['developmentalDisability'].enable();
          this.isDevelopmentalDisabilityDisabled = false;
          this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].enable();
          this.isMarginalFunctionalImpairmentDevelopmentalDisabled = false;

          //BRD 3.5.2.5
          if (this.mentalHealthGroup.get('currentMHDiagnosisOrCondition').value === 'N') {
            this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].disable();
            this.isMarginalFunctionalImpairmentPsychiatricDisabled = true;
            this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].setValue(null);
            this.marginalFunctionalImpairmentPsychiatricSourceText = null;
          }
          else {
            this.mentalHealthGroup.controls['marginalFunctionalImpairmentPsychiatric'].enable();
            this.isMarginalFunctionalImpairmentPsychiatricDisabled = false;
          }


          //BRD 3.5.2.13
          if (this.mentalHealthGroup.get('developmentalDisability').value === 'N') {
            this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].disable();
            this.isMarginalFunctionalImpairmentDevelopmentalDisabled = true;
            this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].setValue(null);
            this.marginalFunctionalImpairmentDevelopmentalSourceText = null;
          }
          else {
            this.mentalHealthGroup.controls['marginalFunctionalImpairmentDevelopmental'].enable();
            this.isMarginalFunctionalImpairmentDevelopmentalDisabled = false;
          }
        }
      }

    }

    //Substance Use Condition
    if (this.selectedTab === 1) {
      //Tabs
      // //BRD 3.1.5.6, 3.2.5.7
      // if (this.clientCategoryType === 671 || this.clientCategoryType === 672) {
      //   var currentMHDiagnosis = this.mentalHealthGroup.get('currentMHDiagnosis').value;
      //   var haveSUD = this.substanceUseGroup.get('haveSUD').value;

      //   if (currentMHDiagnosis === 'N' && haveSUD === 'N')
      //     this.isMedicalConditionTabEnabled = false;
      //   else
      //     this.isMedicalConditionTabEnabled = true;
      // }

      //Questions
      //Question  1
      this.isUsedSubstancesPast3MonthsDisabled = this.isSudRestrictionOfADLDisabled = this.isSudTreatmentCompletionDisabled
        = this.isSudEvidencePast5YearsDisabled = this.substanceUseGroup.get('haveSUD').value === "Y" ? false : true;
      //BRD 3.1.2.21
      if (this.substanceUseGroup.get('haveSUD').value === "N") {
        this.substanceUseGroup.controls['usedSubstancesPast3Months'].disable();
        var usedSubstancesPast3MonthsOrig = this.substanceUseGroup.get('usedSubstancesPast3MonthsOrig').value;
        this.substanceUseGroup.controls['usedSubstancesPast3Months'].setValue(usedSubstancesPast3MonthsOrig);
        this.substanceUseGroup.controls['usedSubstancesPast3MonthsSource'].setValue(679);
        this.usedSubstancesPast3MonthsSourceText = 'Application';

        this.substanceUseGroup.controls['sudRestrictionOfADL'].disable();
        this.substanceUseGroup.controls['sudRestrictionOfADL'].setValue(null);
        this.substanceUseGroup.controls['sudRestrictionOfADLSource'].setValue(null);
        this.sudRestrictionOfADLSourceText = null;
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].disable();
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].setValue(null);
        this.sudRestrictionOfADLExlpainSourceText = null;

        this.substanceUseGroup.controls['sudTreatmentCompletion'].disable();
        this.substanceUseGroup.controls['sudTreatmentCompletion'].setValue(null);
        this.substanceUseGroup.controls['sudTreatmentCompletionSource'].setValue(null);
        this.sudTreatmentCompletionSourceText = null;

        this.substanceUseGroup.controls['sudEvidencePast5Years'].disable();
        this.substanceUseGroup.controls['sudEvidencePast5Years'].setValue(null);
        this.substanceUseGroup.controls['sudEvidencePast5YearsSource'].setValue(null);
        this.sudEvidencePast5YearsSourceText = null;
      }
      else {
        this.substanceUseGroup.controls['usedSubstancesPast3Months'].enable();
        this.substanceUseGroup.controls['sudRestrictionOfADL'].enable();
        this.substanceUseGroup.controls['sudEvidencePast5Years'].enable();

        //BRD 3.1.2.24
        var haveSUD = this.substanceUseGroup.get('haveSUD').value;
        var usedSubstancesPast3Months = this.substanceUseGroup.get('usedSubstancesPast3Months').value;
        if (haveSUD === 'Y' && usedSubstancesPast3Months === 'N') {
          this.substanceUseGroup.controls['sudTreatmentCompletion'].enable();
          this.isSudTreatmentCompletionDisabled = false;
        }
        else {
          this.substanceUseGroup.controls['sudTreatmentCompletion'].disable();
          this.substanceUseGroup.controls['sudTreatmentCompletion'].setValue(null);
          this.substanceUseGroup.controls['sudTreatmentCompletionSource'].setValue(null);
          this.sudTreatmentCompletionSourceText = null;
          this.isSudTreatmentCompletionDisabled = true;
        }
      }

      //Question 3 - sudRestrictionOfADL
      if (this.substanceUseGroup.get('sudRestrictionOfADL').value === "N") {
        this.sudRestrictionOfADLSourceText = 'Reviewer';
        this.substanceUseGroup.controls['sudRestrictionOfADLSource'].setValue(680);
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].enable();
      }
      else {
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].disable();
        this.substanceUseGroup.controls['sudRestrictionOfADLExplain'].setValue(null);
        this.sudRestrictionOfADLExlpainSourceText = null;
      }

      //BRD 3.5.2.24
      if (this.clientCategoryType === 675 || this.clientCategoryType === 676) {
        if (this.substanceUseGroup.get('sudTreatmentCompletion').value === "Y") {
          this.substanceUseGroup.controls['sudEvidencePast5Years'].disable();
          this.isSudEvidencePast5YearsDisabled = true;
          this.substanceUseGroup.controls['sudEvidencePast5Years'].setValue(null);
          this.substanceUseGroup.controls['sudEvidencePast5YearsSource'].setValue(null);
          this.sudEvidencePast5YearsSourceText = null;
        }
        else if (this.substanceUseGroup.get('sudTreatmentCompletion').value === "N") {
          this.substanceUseGroup.controls['sudEvidencePast5Years'].enable();
          this.isSudEvidencePast5YearsDisabled = false;
        }
      }

    }

    //Medical Condition
    if (this.selectedTab === 2) {
      if (this.clientCategoryType === 675 || this.clientCategoryType === 676) {
        //BRD 3.5.1.10
        if (this.supportNotOfferedByGenPopType === 33) {
          this.medicalConditionGroup.controls['haveChronicMedicalCondition'].disable();
          this.isHaveChronicMedicalConditionDisabled = true;
          this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].disable();
          this.isMarginalFunctionalImpairmentMedicalDisabled = true;
          this.medicalConditionGroup.controls['haveNeuroCognitiveImpairment'].disable();
          this.isHaveNeuroCognitiveImpairmentDisabled = true;
          this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].disable();
          this.isMarginalFuntionalImpairmentNeuroDisabled = true;
        }
        else {
          this.medicalConditionGroup.controls['haveChronicMedicalCondition'].enable();
          this.isHaveChronicMedicalConditionDisabled = false;
          this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].enable();
          this.isMarginalFunctionalImpairmentMedicalDisabled = false;
          this.medicalConditionGroup.controls['haveNeuroCognitiveImpairment'].enable();
          this.isHaveNeuroCognitiveImpairmentDisabled = false;
          this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].enable();
          this.isMarginalFuntionalImpairmentNeuroDisabled = false;

          //BRD 3.5.2.37
          if (this.medicalConditionGroup.get('haveChronicMedicalCondition').value === "N") {
            this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].disable();
            this.isMarginalFunctionalImpairmentMedicalDisabled = true;
            this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].setValue(null);
            this.marginalFunctionalImpairmentMedicalSourceText = null;
          }
          else {
            this.medicalConditionGroup.controls['marginalFunctionalImpairmentMedical'].enable();
            this.isMarginalFunctionalImpairmentMedicalDisabled = false;
          }


          //BRD 3.5.2.43
          if (this.medicalConditionGroup.get('haveNeuroCognitiveImpairment').value === "Y") {
            this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].enable();
            this.isMarginalFuntionalImpairmentNeuroDisabled = false;
          }
          else {
            this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].disable();
            this.isMarginalFuntionalImpairmentNeuroDisabled = true;
            this.medicalConditionGroup.controls['marginalFuntionalImpairmentNeuro'].setValue(null);
            this.marginalFuntionalImpairmentNeuroSourceText = null;
          }
        }
      }
      else if (this.clientCategoryType === 671 || this.clientCategoryType === 672) {
        //BRD 3.1.2.41, 3.1.5.6, 3.2.5.7
        var currentMHDiagnosis = this.mentalHealthGroup.get('currentMHDiagnosis').value;
        var haveSUD = this.substanceUseGroup.get('haveSUD').value;

        if (currentMHDiagnosis === 'N' && haveSUD === 'N') {
          this.medicalConditionGroup.controls['haveHIVAIDSDocumented'].disable();
          this.isHaveHIVAIDSDocumentedDisabled = true;
        }
        else {
          this.medicalConditionGroup.controls['haveHIVAIDSDocumented'].enable();
          this.isHaveHIVAIDSDocumentedDisabled = false;
        }

      }
    }

    //At Risk
    if (this.selectedTab === 3) {
      //1.
      if (this.clinicalAtRiskGroup.get('isFunctionalImpairmentsMarked').value === "N") {
        this.clinicalAtRiskGroup.controls['haveLimitedEducation'].disable();
        this.isHaveLimitedEducationDisabled = true;
        var haveLimitedEducationOrig = this.clinicalAtRiskGroup.get('haveLimitedEducationOrig').value;
        this.clinicalAtRiskGroup.controls['haveLimitedEducation'].setValue(haveLimitedEducationOrig);
        this.clinicalAtRiskGroup.controls['haveLimitedEducationSource'].setValue(679);
        this.haveLimitedEducationSourceText = 'Application';

        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistory'].disable();
        this.isHaveLimitedEmpHistoryDisabled = true;
        var haveLimitedEmpHistoryOrig = this.clinicalAtRiskGroup.get('haveLimitedEmpHistoryOrig').value;
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistory'].setValue(haveLimitedEmpHistoryOrig);
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistorySource'].setValue(679);
        this.haveLimitedEmpHistorySourceText = 'Application';

        this.clinicalAtRiskGroup.controls['haveHistoryOfTrauma'].disable();
        this.isHaveHistoryOfTraumaDisabled = true;
        var haveHistoryOfTraumaOrig = this.clinicalAtRiskGroup.get('haveHistoryOfTraumaOrig').value;
        this.clinicalAtRiskGroup.controls['haveHistoryOfTrauma'].setValue(haveHistoryOfTraumaOrig);
        this.clinicalAtRiskGroup.controls['haveHistoryOfTraumaSource'].setValue(679);
        this.haveHistoryOfTraumaSourceText = 'Application';

        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystems'].disable();
        this.isInvolvedinTwoSystemsDisabled = true;
        var isInvolvedinTwoSystemsOrig = this.clinicalAtRiskGroup.get('isInvolvedinTwoSystemsOrig').value;
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystems'].setValue(isInvolvedinTwoSystemsOrig);
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystemsSource'].setValue(679);
        this.isInvolvedinTwoSystemsSourceText = 'Application';

        this.clinicalAtRiskGroup.controls['otherRiskFactorIssues'].disable();
        this.isOtherRiskFactorIssuesDisabled = true;
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssues'].setValue(null);
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesSource'].setValue(null);
        this.otherRiskFactorIssuesSourceText = null;
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].disable();
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].setValue(null);
        this.otherRiskFactorIssuesExplainSourceText = null;
      }
      else {
        this.clinicalAtRiskGroup.controls['haveLimitedEducation'].enable();
        this.isHaveLimitedEducationDisabled = false;
        this.clinicalAtRiskGroup.controls['haveLimitedEmpHistory'].enable();
        this.isHaveLimitedEmpHistoryDisabled = false;
        this.clinicalAtRiskGroup.controls['haveHistoryOfTrauma'].enable();
        this.isHaveHistoryOfTraumaDisabled = false;
        this.clinicalAtRiskGroup.controls['isInvolvedinTwoSystems'].enable();
        this.isInvolvedinTwoSystemsDisabled = false;
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssues'].enable();
        this.isOtherRiskFactorIssuesDisabled = false;
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].enable();
      }

      //2. Explain 
      if (this.clinicalAtRiskGroup.get('otherRiskFactorIssues').value === "Y") {
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].enable();
      }
      else {
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].disable();
        this.clinicalAtRiskGroup.controls['otherRiskFactorIssuesExplain'].setValue(null);
        this.otherRiskFactorIssuesExplainSourceText = null;
      }
    }

  }

  //Call Toaster Service Message
  callToastrService(message: string) {
    if (!this.toastrService.currentlyActive)
      this.toastrService.error(message);
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }

    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }

    if (this.clinicalReviewDataSub) {
      this.saveTabData(0, this.selectedTab);
      this.clinicalReviewDataSub.unsubscribe();
    }

    if (this.clinicalAssessmentDataSub) {
      this.clinicalAssessmentDataSub.unsubscribe();
    }

    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }
}
