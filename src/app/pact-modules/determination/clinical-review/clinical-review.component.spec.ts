import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalReviewComponent } from './clinical-review.component';

describe('ClinicalReviewComponent', () => {
  let component: ClinicalReviewComponent;
  let fixture: ComponentFixture<ClinicalReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicalReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
