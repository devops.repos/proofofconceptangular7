export class DETMentalHealthCondition {
    detMentalHealthConditionID? : number;
    pactApplicationID? : number;
    currentMHDiagnosisOrig? : number;
    currentMHDiagnosis? : number;
    currentMHDiagnosisSource? : number;
    currentMHDiagnosisOrConditionOrig? : number;
    currentMHDiagnosisOrCondition? : number;
    currentMHDiagnosisOrConditionSource? : number;
    significantFunctionalImpairmentOrig? : number;
    significantFunctionalImpairment? : number;
    significantFunctionalImpairmentSource? : number;
    significantFunctionalImpairmentExplain : string;
    marginalFunctionalImpairmentPsychiatricOrig? : number;
    marginalFunctionalImpairmentPsychiatric? : number;
    marginalFunctionalImpairmentPsychiatricSource? : number;
    marginalFunctionalImpairmentDevelopmentalOrig? : number;
    marginalFunctionalImpairmentDevelopmental? : number;
    marginalFunctionalImpairmentDevelopmentalSource? : number;
    excludingSUDNonSPMIOrig? : number;
    excludingSUDNonSPMI? : number;
    excludingSUDNonSPMISource? : number;
    seriousMentalIllnessOrig? : number;
    seriousMentalIllness? : number;
    seriousMentalIllnessSource? : number;
    haveSEDDiagnosedBefore18Orig? : number;
    haveSEDDiagnosedBefore18? : number;
    haveSEDDiagnosedBefore18Source? : number;
    stabilizedForPlacementOrig? : number;
    stabilizedForPlacement? : number;
    stabilizedForPlacementSource? : number;
    stabilizedForPlacementExplain : string;
    developmentalDisabilityOrig? : number;
    developmentalDisability? : number;
    developmentalDisabilitySource? : number;
    isMentalHealthConditionCompleted? : boolean;
    userID?: number;
}

export class DETSubstanceUse {
    detSubstanceUseID? : number;
    pactApplicationID? : number;
    haveSUDOrig? : number;
    haveSUD? : number;
    haveSUDSource? : number;
    usedSubstancesPast3MonthsOrig? : number;
    usedSubstancesPast3Months? : number;
    usedSubstancesPast3MonthsSource? : number;
    sudEvidencePast5YearsOrig? : number;
    sudEvidencePast5Years? : number;
    sudEvidencePast5YearsSource? : number;
    sudRestrictionOfADLOrig? : number;
    sudRestrictionOfADL? : number;
    sudRestrictionOfADLSource? : number;
    sudRestrictionOfADLExplain: string;
    sudTreatmentCompletionOrig? : number;
    sudTreatmentCompletion? : number;
    sudTreatmentCompletionSource? : number;
    isSubstanceUseCompleted? : boolean;
    userID?: number;
  }

  export class DETMedicalCondition {
    detMedicalConditionID? : number;
    pactApplicationID? : number;
    isActiveInHASAOrig? : number;
    isActiveInHASA? : number;
    isActiveHASASource? : number;
    haveHIVAIDSDocumentedOrig? : number;
    haveHIVAIDSDocumented? : number;
    haveHIVAIDSDocumentedSource? : number;
    haveCurrentMedicalDiagnosisOrig? : number;
    haveCurrentMedicalDiagnosis? : number;
    haveCurrentMedicalDiagnosisSource? : number;
    restrictionOfADLOrig? : number;
    restrictionOfADL? : number;
    restrictionOfADLSource? : number;
    haveChronicMedicalConditionOrig? : number;
    haveChronicMedicalCondition? : number;
    haveChronicMedicalConditionSource? : number;
    marginalFunctionalImpairmentMedicalOrig? : number;
    marginalFunctionalImpairmentMedical? : number;
    marginalFunctionalImpairmentMedicalSource? : number;
    haveNeuroCognitiveImpairmentOrig? : number;
    haveNeuroCognitiveImpairment? : number;
    haveNeuroCognitiveImpairmentSource? : number;
    marginalFuntionalImpairmentNeuroOrig? : number;
    marginalFuntionalImpairmentNeuro? : number;
    marginalFuntionalImpairmentNeuroSource? : number;
    isMedicalConditionCompleted? : boolean;
    userID?: number;
  }

  export class DETClinicalAtRisk{
    detClinicalAtRiskID? : number;
    pactApplicationID? : number;
    isFunctionalImpairmentsMarkedOrig? : number;
    isFunctionalImpairmentsMarked? : number;
    isFunctionalImpairmentsMarkedSource? : number;
    haveLimitedEducationOrig? : number;
    haveLimitedEducationOrigDescription : string;
    haveLimitedEducation? : number;
    haveLimitedEducationSource? : number;
    haveLimitedEmpHistoryOrig? : number;
    haveLimitedEmpHistoryOrigDescription : string;
    haveLimitedEmpHistory? : number;
    haveLimitedEmpHistorySource? : number;
    haveHistoryOfTraumaOrig? : number;
    haveHistoryOfTrauma? : number;
    haveHistoryOfTraumaSource? : number;
    isInvolvedinTwoSystemsOrig? : number;
    isInvolvedinTwoSystems? : number;
    isInvolvedinTwoSystemsSource? : number;
    otherRiskFactorIssuesOrig? : number;
    otherRiskFactorIssues? : number;
    otherRiskFactorIssuesSource? : number;
    otherRiskFactorIssuesExplain : string;
    isClinicalAtRiskCompleted? : boolean;
    userID?: number;
  }

  export class DETClinicalHomelessMedicaidReviewInput{
    pactApplicationID? : number;
    summaryType? : number;
    clientCategoryType? : number;
    userID?: number;
  }

  
        
        
        
       
        