import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { AssignmentHistoryComponent } from './assignment-history.component';
import { ClientAssignmentHistory, ClientHistoryDetailsSearch } from './assignment-history.model';
import { environment } from '../../../../environments/environment';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AssignmentHistoryService {
  getAssignmentHistoryListUrl = environment.pactApiUrl + 'CaseAssignment/GetAssignmentHistoryDetails'; 

  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    }),
    response : "json",
  };
    
  constructor(private httpClient: HttpClient) {
  }


  //Get Assignment History List Details
  GetAssignmentHistoryDetails(PACTApplicationID : number): Observable<any> {
    if (PACTApplicationID > 0){
      console.log('service - ', PACTApplicationID);
      //return this.httpClient.post(this.getAssignmentHistoryListUrl + PACTApplicationID, this.httpOptions);
      //return this.httpClient.post(this.getAssignmentHistoryListUrl+"/"+PACTApplicationID, this.httpOptions);
      return this.httpClient.post(this.getAssignmentHistoryListUrl, JSON.stringify(PACTApplicationID), this.httpOptions);
    }
  }


}