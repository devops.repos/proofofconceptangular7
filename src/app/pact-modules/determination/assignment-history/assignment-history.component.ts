import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import * as moment from 'moment';

//Service
import { UserService } from 'src/app/services/helper-services/user.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { AssignmentHistoryService } from './assignment-history.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

//Models
import { ClientAssignmentHistory, ClientHistoryDetailsSearch } from './assignment-history.model';

@Component({
  selector: 'app-assignment-history',
  templateUrl: './assignment-history.component.html',
  styleUrls: ['./assignment-history.component.scss']
})

export class AssignmentHistoryComponent implements OnInit, OnDestroy {
  //Global Varaibles
  userData: AuthData;
  userDataSub: Subscription;
  activatedRouteSub: Subscription;

  assignmentHistoryGroup: FormGroup;
  assignmentHistoryRowData : ClientAssignmentHistory;
  pactApplicationId: number; 

  assignmentHistoryClientColumnDefs = [];
  assignmentHistoryClientRowData: ClientHistoryDetailsSearch[];
  assignmentHistoryClientDefaultColDef = {};
  assignmentHistoryClientGridOptions: GridOptions;
  assignmentHistoryClientOverlayNoRowsTemplate: string;
  assignmentHistoryClientGridColumnApi: any;
  context: any;

  clientSearchData: ClientHistoryDetailsSearch = {
    pactApplicationID: null,
    caseAssignmentID: null,
    caseAssignmentHistoryID: null,
    clientID: null,
    firstName: null,
    lastName: null,
    applicationDate: null,
    dateAssigned: null,
    reviewerAssigned: null,
    actionTaken: null,
    comments: null,
    optionUserID: null
  };


  //Constructor
  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private assignmentHistoryService: AssignmentHistoryService,
    private commonService: CommonService,
    private message: ToastrService,
    private dialogService: ConfirmDialogService) {
    //Assignment History Group Form Builder
    this.assignmentHistoryGroup = this.formBuilder.group({
      pactApplicationIdCtrl: [''],
      lastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      firstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      clientIdCtrl: ['', Validators.required],
    });

    //Assignemnt History Client Grid Column Definitions
    this.assignmentHistoryClientColumnDefs = [
      { headerName: 'Application Id (S.R. #)', field: 'pactApplicationID', filter: 'agTextColumnFilter', width: 150, },
      { headerName: 'Client #', field: 'clientID', filter: 'agTextColumnFilter', width: 150, },
      {
        headerName: 'Transmit Date',
        field: 'applicationDate',
        width: 150,
        filter: 'agTextColumnFilter',
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? moment(data.value).format('MM/DD/YYYY hh:mm A') : '';
        }
      },
      {
        headerName: 'Date Assigned',
        field: 'dateAssigned',
        width: 150,
        filter: 'agTextColumnFilter',
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? moment(data.value).format('MM/DD/YYYY hh:mm A') : '';
        }
      },
      { headerName: 'ReviewerAssigned', field: 'reviewerAssigned', filter: 'agTextColumnFilter', width: 200, },
      { headerName: 'Action Taken', field: 'actionTaken', filter: 'agTextColumnFilter', width: 200, },
      { headerName: 'Comments', field: 'comments', filter: 'agTextColumnFilter', width: 250, },

    ];
    this.assignmentHistoryClientDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.context = { componentParent: this };
  }

  ngOnInit() {
 
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            //Get Client Assignment History  Summary Details
            this.getClientAssignmentHistoryDetails(numericApplicationId);
            
          }
          else {
            return;
          }
        }
      }
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }

  }

  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  //Assignment History Client Grid Ready
  assignmentHistoryClientGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    //this.assignmentHistoryClientGridColumnApi = params.columnApi;
    this.assignmentHistoryClientOverlayNoRowsTemplate = '<span style="color: #337ab7">No Assignment History to display</span>';
  }


    getClientAssignmentHistoryDetails = (pactApplicationId: number) => {
      if (pactApplicationId > 0){
        console.log('pactApplicationId: ', pactApplicationId);
          this.assignmentHistoryService.GetAssignmentHistoryDetails(pactApplicationId)
            .subscribe(
              res => {
                this.assignmentHistoryRowData = res as ClientAssignmentHistory;
                //console.log('assignmentHistoryRowData.clientSearchList: ', this.assignmentHistoryRowData.clientSearchList);
                console.log('assignmentHistoryRowData: ', this.assignmentHistoryRowData);
                this.assignmentHistoryClientRowData = this.assignmentHistoryRowData.clientHistoryDetailsSearch;
                this.assignmentHistoryGroup.controls['pactApplicationIdCtrl'].setValue(pactApplicationId);
                this.assignmentHistoryGroup.controls['clientIdCtrl'].setValue(this.assignmentHistoryRowData.clientID);        
                this.assignmentHistoryGroup.controls['lastNameCtrl'].setValue(this.assignmentHistoryRowData.lastName);
                this.assignmentHistoryGroup.controls['firstNameCtrl'].setValue(this.assignmentHistoryRowData.firstName);

               },
              error => console.error('Error!', error)
            );
      }
    }



  //Search Client
  searchClient() {
    console.log('search client');
    this.pactApplicationId =  this.assignmentHistoryGroup.get('pactApplicationIdCtrl').value;
    this.getClientAssignmentHistoryDetails(this.pactApplicationId);

  }

   //No Client Match Dialog
   noClientMatchDialog() {
    // this.dialogService.confirmDialog('No Match Found', 'Do you want to continue without match?', '', 'Continue Without Match', 'Cancel')
    //   .then(() => {
    //     if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
    //       this.setCompleteFlag.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
    //       this.setCompleteFlag.sourceSystemType = 368;
    //       //Api call to create a new client 
    //       this.matchSourcesService.updateTabCompleteForMatchSummary(this.setCompleteFlag)
    //         .subscribe(res => {
    //           if (res) {
    //             const data = res as SetCompleteFlag
    //             if (data && data.isSuccess) {
    //               this.matchSourcesService.setDhsMatchTabComplete(data.isSuccess);
    //             }
    //           }
    //         });
    //     }
    //     else {
    //       this.message.error("Unable to set complete flag.");
    //     }
    //   }, () => {
    //   });
  }

    //Validation Message
    validationMessage() {
      if (this.assignmentHistoryGroup.get('pactApplicationIdCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Invalid Application Id #.");
        }
      }
      // else if (this.assignmentHistoryGroup.get('ssnCtrl').invalid) {
      //   if (!this.message.currentlyActive) {
      //     this.message.error("Invalid Client#.");
      //   }
      // }
      // if (this.assignmentHistoryGroup.get('firstNameCtrl').invalid) {
      //   if (!this.message.currentlyActive) {
      //     this.message.error("First Name is required.");
      //   }
      // }
      // else if (this.assignmentHistoryGroup.get('lastNameCtrl').invalid) {
      //   if (!this.message.currentlyActive) {
      //     this.message.error("Last Name is required.");
      //   }
      // }

    }
  
    //Clear Form
    onClearClick() {
      this.assignmentHistoryGroup.reset();
      this.assignmentHistoryRowData = null;
    }

}
