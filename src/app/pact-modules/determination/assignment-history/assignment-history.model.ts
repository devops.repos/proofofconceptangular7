export interface ClientAssignmentHistory {
    pactApplicationID?: number;
    clientID?: number;
    firstName: string;
    lastName: string;
    clientHistoryDetailsSearch: ClientHistoryDetailsSearch[];
}


export interface ClientHistoryDetailsSearch {
    pactApplicationID?: number;
    caseAssignmentID?: number;
    caseAssignmentHistoryID?: number;
    clientID?: number;
    firstName: string;
    lastName: string;
    applicationDate?: Date;
    dateAssigned?: Date;
    reviewerAssigned: string;
    actionTaken : string;
    comments: string;
    optionUserID?: number;
}