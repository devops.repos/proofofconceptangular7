import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CaseAssignmentActionDialog } from '../case-assignment.model'

@Component({
  selector: 'app-case-assignment-dialog',
  templateUrl: './case-assignment-dialog.component.html',
  styleUrls: ['./case-assignment-dialog.component.scss']
})
export class CaseAssignmentDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public caseAssignmentActionDialog = new CaseAssignmentActionDialog(),
    private dialogRef: MatDialogRef<CaseAssignmentDialogComponent>) {
}

//Close the dialog on close button
CloseDialog() {
    this.dialogRef.close(true);
}

//On Init
ngOnInit() {
}

}
