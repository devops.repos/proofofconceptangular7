import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { CaseAssignmentService } from './case-assignment.service'
import { Reviewer } from './case-assignment.model'

@Component({
  selector: 'app-reviewer-control',
  template: `
  <div>
    <select style="width: 100%" [(ngModel)]="value" (change)="onSelect($event)">
        <option [value]="0"> Select One </option>
        <option *ngFor="let  reviewer of reviewers" [value]="reviewer.id">{{reviewer.name}}</option>
    </select>
    </div>
  `,
  styles: []
})
export class ReviewerComponent implements ICellRendererAngularComp {
  reviewers : Reviewer[];
    value: any;
    params: any;
  constructor(private caseAssignmentService : CaseAssignmentService) { }

    ngOnInit() {
      this.reviewers = this.caseAssignmentService.getReviewers();
    }

    agInit(params): void {
    this.params = params;
    this.value = params.value == undefined ? 0 : parseInt(params.value);
    }
    refresh(): boolean {
    return false;
    }
    onSelect($event) {
    this.params.context.componentParent.onReviewerChange($event.target.value,this.params.node);
    }

}
