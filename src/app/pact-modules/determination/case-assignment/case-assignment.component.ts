import { Component, OnInit, OnDestroy, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions, RowNode } from 'ag-grid-community';
import { CaseAssignmentActionComponent } from './case-assignment-action/case-assignment-action.component';
import { CaseAssignmentModel, CaseAssignment, Reviewer,AssignedList, iDocumentLink } from './case-assignment.model';
import { MatDialog } from '@angular/material';
import { CustomHeader } from './custom-header.component';
import 'ag-grid-enterprise';
import { AgGridAngular  } from 'ag-grid-angular';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ReviewerComponent } from './reviewer-control.component';
import { UserService } from 'src/app/services/helper-services/user.service';
import { CaseAssignmentService } from './case-assignment.service'
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { NumberDirective } from 'src/app/shared/directives/number.directive';
 
@Component({
  selector: 'app-case-assignment',
  templateUrl: './case-assignment.component.html',
  styleUrls: ['./case-assignment.component.scss']
})


export class CaseAssignmentComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('templateCell', {}) templateCell: TemplateRef<any>;
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  headerComponentFramework :any
  public gridOptions: GridOptions;
  rowData: CaseAssignmentModel[];
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;
  context: any;
  rowNodes : RowNode[];
  userDataSub : AuthData;
  getRowHeight;
  multiSelectVal : number
  unAssignedCount : number
  caseList :  CaseAssignmentModel[];
  reviewers : Reviewer[];
  assignedList : AssignedList = { caseAssignments :this.caseList, userID :0 };
  UnAssign : number = 0;

 

  grandTotal : number
  totalAssigned : number
  totalUnAssigned : number
  individualUnAssigned : number
  familyUnAssigned : number
  individualAssigned : number
  familyAssigned : number

// caseAssignment : CaseAssignment = {
//   totalCases : 130,
//   unAssignedCases : 70,
//   assignedCases : 60,
//   unAssignedByType : {
//     individual : {
//       count : 35,
//       adultSingle : 20,
//       youngAdultSingle : 10
//     },
//     family : {
//       count : 15,
//       adultFamily :10,
//       youngAdultFamily : 10,
//       familyWithchildren : 5
//     }
//   },
//   caseList : this.caseList

//}
  params: any;
  userData: any;

constructor(
          private toastr: ToastrService, 
          private dialog: MatDialog, 
          private router: Router,
          private userService: UserService,
          private commonService: CommonService,
          private caseAssignmentService: CaseAssignmentService,
          ) {
    this.gridOptions = {
      rowHeight: 30,
      singleClickEdit : true,
      onFilterChanged : () => {
        
          this.gridOptions.api.getColumnDef('SelectedCases').headerName = this.gridOptions.api.isAnyFilterPresent() ? 'Select All' : 'Select Cases';
          this.gridOptions.api.getColumnDef('SelectedCases').headerCheckboxSelection = this.gridOptions.api.isAnyFilterPresent();
          this.gridOptions.api.getColumnDef('SelectedCases').headerCheckboxSelectionFilteredOnly = this.gridOptions.api.isAnyFilterPresent();
          this.gridOptions.api.refreshHeader();
        },
    } as GridOptions;

    this.columnDefs = [
      {
        headerName: 'Referral Date',
        field: 'referralDate',
        width: 160,
        filter: 'agTextColumnFilter' ,
        comparator: this.commonService.dateComparator  
      },
      {
        headerName: ' Application #',
        field: 'pactApplicationID',
        width: 150,
        filter: 'agTextColumnFilter',
        cellRenderer: (params: { value: string; data: { pactApplicationID: number; applicationSummaryReportFileNetDocID: number }; }) => {
          var link = document.createElement('a');
          link.href = '#';
          link.innerText = params.value;
          link.addEventListener('click', (e) => {
            e.preventDefault();
            this.commonService.setIsOverlay(true);
            this.openApplicationSummaryReport(params.data.applicationSummaryReportFileNetDocID, params.data.pactApplicationID.toString(), this.userData.optionUserId);
          });
          return link;
        },

      },
      {
        headerName: 'Client #',
        field: 'clientNumber',
        width: 100,
        filter: 'agTextColumnFilter'      
      },
      {
        headerName: 'Case Type',
        field: 'caseType',
        width: 150,
        filter: 'agTextColumnFilter',
        cellRenderer: this.hasOriginalReviewer,
        tooltipField: 'originalReviewer'
      },
      {
        headerName: ' Client Name (L,N)',
        field: 'clientName',
        width: 160,
        // filter: 'agSetColumnFilter'
        filter: 'agTextColumnFilter' 
      },
      {
        headerName: 'Pop Type',
        field: 'populationAppliedFor',
        width: 150,
        filter: 'agTextColumnFilter',
        tooltipField: 'populationAppliedFor'
      },

      {
        headerName: 'Referral Agency/Site',
        field: 'referalAgencySite',
        width: 300,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Referred By (L,N)',
        field: 'referredByName',
        width: 180,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Reviewer Assigned',
        field: 'reviewerAssignedTo',
        width: 180,
        // filter: 'agSetColumnFilter',     
        filter: 'agTextColumnFilter'    
      },
      {
        headerName: 'Date Assigned',
        field: 'reviewAssignedDate',
        width: 160,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Review Status',
        field: 'caseStatus',
        width: 160,
        filter: 'agTextColumnFilter'
        
      },
      {
        headerName: 'Comments',
        field: 'comments',
        width: 300,
        filter: 'agTextColumnFilter'
      }, 
      {
        headerName: 'Select Cases',
        field: 'SelectedCases',
        width: 100,
        pinned: 'right',
        checkboxSelection: true,
        filter: false,
        sortable: false,
        resizable: false,
        suppressSizeToFit: true,
      },
      {  
        headerName: "Multi Reviewer",  
        field: "reviewer",  
        width: 200,  
        pinned: 'right',
        filter: false,
        sortable: false,
        resizable: false,
        suppressSizeToFit: true,
        headerComponentFramework : CustomHeader,
        cellRendererFramework : ReviewerComponent,    
      },
      {
        headerName: 'Action',
        field: 'caseStatus',       
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];
    
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      //suppressMenu : true
    };
    this.getRowHeight = function(params) {
      return params.data.originalReviewer != null ? 30 : 30;
    };

    this.rowSelection = 'multiple';
    this.pagination = true;
    this.context = { componentParent: this };
    
    this.frameworkComponents = {
     actionRenderer: CaseAssignmentActionComponent,
    };
  }
  
  ngOnInit() {
    
    this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    this.getAssignments();
  }

  getAssignments(){
    this.caseAssignmentService.getCaseAssignments(1).subscribe(
      data => {
        if (data != null) {
        this.caseList = data.assignment;
        this.reviewers = data.reviewerList;
        this.reviewers.push({ id : 999, name : 'Un-Assign', count : 0});
        this.caseAssignmentService.setReviewers(this.reviewers.filter(x => x.id !== 999));
        this.reviewers.forEach(x => x.count = 0);
        this.grandTotal = this.caseList.length;
        this.totalUnAssigned = this.caseList.filter(x => x.caseStatus == 'Un-Assigned').length;    
        this.totalAssigned = this.caseList.filter(x => x.caseStatus != 'Un-Assigned').length;        
        this.individualUnAssigned = this.caseList.filter(x => x.applicationType == 'Individual' && x.reviewerAssignedTo == null && x.caseStatus == 'Un-Assigned' ).length;
        //this.singleYoungAdult = this.caseList.filter(x => x.applicationType == 'Individual' && x.reviewerAssignedTo == null && x.age >= 26).length;
        //this.singleAdult = this.caseList.filter(x => x.applicationType == 'Individual' && x.reviewerAssignedTo == null x.age < 26)).length;  

        this.familyUnAssigned = this.caseList.filter(x => x.applicationType != 'Individual' && x.reviewerAssignedTo == null && x.caseStatus == 'Un-Assigned' ).length;
        this.individualAssigned = this.caseList.filter(x => x.applicationType == 'Individual' && x.reviewerAssignedTo != null && x.caseStatus != 'Un-Assigned'  ).length 
        this.familyAssigned = this.caseList.filter(x => x.applicationType != 'Individual' && x.reviewerAssignedTo != null && x.caseStatus != 'Un-Assigned').length;
        document.getElementById('btnAssign').innerHTML = 'Assign';
        this.UnAssign = 0;
        this.refreshCaseList();
        }
      },
      error => console.error('Error!', error)
    );
    this.refreshCaseList();
  }

  extractValues(mappings) {
    return Object.keys(mappings);
  }

  onFirstDataRendered(params) {
    params.api.setDomLayout('autoHeight');
  }

  onGridReady = (params: { api: any; columnApi: any }) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.refreshCaseList();
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the diagnosis loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Diagnosis Available</span>';

    this.gridOptions.api.refreshHeader();  
    params.api.setDomLayout('autoHeight');
  }


  hasOriginalReviewer(params) {
  return params.data.caseType == "Resubmission" && params.data.originalReviewer != null ?  params.data.caseType + `<mat-icon class="mat-icon material-icons"  style="padding-left: 5px; font-size: 15px !important;" [matTooltip]="" matTooltipPosition="after">info</mat-icon>` :  params.data.caseType; }

  getAssignment () {
    if(this.reviewers != undefined) {
      return this.reviewers.filter(x => x.count > 0);
    }
  }
  onMultiReviewerChange (id : any) {
    this.multiSelectVal = id
    this.rowNodes = this.gridOptions.api.getSelectedNodes();
      this.rowNodes.forEach(x => x.setDataValue('reviewer', id))
    this.updateMultiAssignmentTally(id);
  }

  onReviewerChange (id : any, rowNodes :RowNode) {
    rowNodes.setSelected(id > 0); 
    if (rowNodes.data.reviewer > 0){
      this.reviewers.find(x => x.id == rowNodes.data.reviewer).count --;
      rowNodes.setDataValue('reviewer', id);
    } else {
      rowNodes.setDataValue('reviewer', id);
    }
      rowNodes.setDataValue('reviewer', id);
     // console.log('onReviewerChange - ', id, rowNodes);
    this.updateAssignmentTally(id);
  }

  onPageSizeChanged(selectedPageSize) {
    this.gridOptions.api.paginationSetPageSize(Number(selectedPageSize));
    this.gridOptions.api.refreshHeader();  
  }

  onRowSelected(event) {
    if(!event.node.selected && event.node.data.reviewer > 0) {
      this.reviewers.find(x => x.id == event.node.data.reviewer).count --;
      event.node.setDataValue('reviewer', 0)
    }
  }

  onSelectionChanged(event) {
    console.log(event);
  }

  updateMultiAssignmentTally (id : any) {
    if (id > 0 && id != 999){
      this.rowNodes = this.gridOptions.api.getSelectedNodes();
      this.rowNodes.forEach(x => x.setDataValue('reviewer', id))
      this.refreshTally(id);
      this.UnAssign = 2;
      document.getElementById('btnAssign').innerHTML = 'Assign';
    }
    else {    
      this.rowNodes.forEach(x => x.setDataValue('reviewer', 0))
      // this.refreshTally(id);
      if(id == 999) { 
        this.reviewers.find(x => x.id == 999).count = this.rowNodes.length; //+ this.reviewers.find(x => x.id == 999).count ;
        this.UnAssign = 1;
        document.getElementById('btnAssign').innerHTML = 'UnAssign';
      }
    }
  }
  updateAssignmentTally (id : any) { 
      this.refreshTally(id);
  }

  refreshTally (id :  any) {
    if ( id == -1){
      if (this.reviewers != undefined && this.caseList != undefined) {
        this.reviewers.filter(
          x => x.count = this.caseList.filter(z => z.reviewer == x.id).length
        )
        this.checkUnAssigned();
      }

    }
    else {
      if (this.reviewers != undefined && this.caseList != undefined) {
        this.reviewers.filter(
          x => x.id == id)[0].count = this.caseList.filter(z => z.reviewer == id).length
        this.checkUnAssigned();
      }

    }
  }
  refreshCaseList () {    
    if (this.caseList != undefined) {
      this.rowData = this.caseList;
    }
    this.refreshTally(-1);
  }

  checkUnAssigned() {     
    if (this.caseList.filter(x => x.reviewer == 0 && x.reviewerAssignedTo != '').length > 0 && this.caseList.filter(x => x.reviewer > 0).length == 0)
    { // All UnAssign
      this.UnAssign = 1;
      document.getElementById('btnAssign').innerHTML = 'UnAssign';
    }else if (this.caseList.filter(x => x.reviewer == 0 && x.reviewerAssignedTo != '').length == 0 && this.caseList.filter(x => x.reviewer > 0).length > 0) 
    { //All Assign
      this.UnAssign = 2;
      document.getElementById('btnAssign').innerHTML = 'Assign';
    } else if (this.caseList.filter(x => x.reviewer == 0 && x.reviewerAssignedTo != '').length > 0 && this.caseList.filter(x => x.reviewer > 0).length > 0) 
    {// Mixed of Assign & UnAssgin
      this.UnAssign = 3;
      document.getElementById('btnAssign').innerHTML = 'Assign';
    }
  }

  assignCases () {
   this.assignedList.caseAssignments = this.caseList.filter(x => x.reviewer > 0 || (x.reviewerAssignedTo != '' && x.reviewer == 0))
   this.checkUnAssigned();
   if(this.assignedList.caseAssignments.length > 0){
     this.assignedList.userID = this.userData.optionUserId;
      this.caseAssignmentService.saveAssignments(this.assignedList).subscribe(
        res => {
          this.getAssignments();
          if(this.UnAssign == 1) //All UnAssign
          {
            this.toastr.success('Case(s) UnAssigned successfully!', 'Saved..');
          } 
          else if(this.UnAssign == 2) { // All Assign
            this.toastr.success('Case(s) Assigned successfully!', 'Saved..');
          } else if(this.UnAssign == 3) { // Mix of UnAssign and Assign
            this.toastr.success('Case(s) Successfully Assigned & Un-Assigned', 'Saved..');
          }
        },
        error => {
          console.log(error);
        });
   }
   else{
     this.toastr.warning("No case selected, Please select at least one case.", "Warning!");
   }
  }

   //On Client Case Folder
   onClientCaseFolder(data: CaseAssignmentModel) {
    if (data && data.pactApplicationID) {
      this.router.navigate(['/admin/client-case-folder', data.pactApplicationID]);
    }
  }

	//Open Application Summary Report
  openApplicationSummaryReport(applicationSummaryReportFileNetDocID: number, selectedApplicationID: string, optionUserId: number) {
    if (applicationSummaryReportFileNetDocID) {
      this.commonService.getFileNetDocumentLink(applicationSummaryReportFileNetDocID.toString()).subscribe(res => {
        const data = res as iDocumentLink;
        if (data && data.linkURL) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    }
    else {
      this.commonService.displayApplicationSummaryReport(selectedApplicationID, optionUserId);
    }
  }


 unAssign(id : number) {
   if(this.caseList.filter(x => x.pactApplicationID == id).length > 0 && this.caseList.filter(x => x.pactApplicationID == id)[0].reviewer != 0) {
    this.caseList.filter(x => x.pactApplicationID == id)[0].reviewer = 0;
    this.reviewers.find(x => x.id == 999).count = this.reviewers.find(x => x.id == 999).count+1;
    this.checkUnAssigned();
   }
 }
  clear () {
    this.getAssignments();
  }
tst() {
  this.caseList = this.caseList.filter(x => x.clientNumber > 0);
  this.refreshCaseList();
}
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub = null;
      this.caseList  = null;
      this.reviewers = null;
    }
  }
  
}
