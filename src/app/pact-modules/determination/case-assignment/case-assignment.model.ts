export interface CaseAssignmentModel {
    srNo : number;
    pactApplicationID? : number;
    clientNumber? : number;
    clientName? : string;
    referralDate? : string;
    referalAgencySite : string;
    referredByName? : string;
    populationAppliedFor? : string;
    caseType? : string;
    reviewerAssignedTo? : string;
    reviewAssignedDate? : string;
    reviewStatDate? : string;
    caseStatus : string;
    comments? : string;
    reviewer? : number;
    applicationType?: string;
    isCaseSelected? : boolean
    other? : string;
    previouslyAssigned? : string;
    originalReviewer? : string;
    signOffDate? : Date
    applicationDate? : Date;
    childApplicationID?: number;
    surveyReportFileNetDocID?: number;
    applicationSummaryReportFileNetDocID?: number;
}

export interface AssignedList {
    caseAssignments : CaseAssignmentModel[],
    userID? : number
}
export interface Reviewer {
    name : string,
    id : number
    count : number;
}
//Prior Supportive Housing Application Dialog Data
export class CaseAssignmentActionDialog {
    // housingApplicationSupportingDocumentsData = new HousingApplicationSupportingDocumentsData();
    pactApplicationId: number;
    isReferralHistory: boolean;
}

export interface CaseAssignment {
    totalCases : number,
    unAssignedCases : number,
    assignedCases : number,
    unAssignedByType : UnAssignedByType,
    caseList : CaseAssignmentModel[]
}

export interface UnAssignedByType {
    individual : Individual,
    family : Family
}

export interface Individual {
    count : number,
    adultSingle : number,
    youngAdultSingle : number
}

export interface Family { 
    count : number,   
    adultFamily : number,
    youngAdultFamily : number,
    familyWithchildren : number
}

//Document Link
export interface iDocumentLink {
    linkURL: string;
  }
