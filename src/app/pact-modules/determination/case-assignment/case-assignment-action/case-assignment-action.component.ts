import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from "ag-grid-angular";
import { MatDialog } from '@angular/material';
import { CaseAssignmentActionDialog } from '../case-assignment.model'
import { CaseAssignmentDialogComponent } from '../case-assignment-dialog/case-assignment-dialog.component'

@Component({
  selector: 'app-case-assignment-action',
  templateUrl: './case-assignment-action.component.html',
  styleUrls: ['./case-assignment-action.component.scss']
})
export class CaseAssignmentActionComponent implements ICellRendererAngularComp {
  params: any;
  status : string
  selectedApplicationID: any;
  isClientCaseFolder: boolean;

  constructor(public dialog: MatDialog) { }
  
  agInit(params: any): void {
    this.params = params;
    this.status = this.params.value;
    this.selectedApplicationID = this.params.node.data.pactApplicationID;
    this.isClientCaseFolder = this.params.node.data.clientNumber ? true: false;
  }

  refresh(): boolean {
    return false;
  }

    //On Client Case Folder
    onClientCaseFolder() {
      this.params.context.componentParent.onClientCaseFolder(this.params.data);
    }

  unAssign() {
    this.params.context.componentParent.unAssign(this.selectedApplicationID);
  }
  
    openDialog(assignmentAtionsDialogData = new CaseAssignmentActionDialog()): void {
      this.dialog.open(CaseAssignmentDialogComponent, {
        width: '1200px',
        maxHeight: '550px',
        disableClose: true,
        autoFocus: false,
        data: assignmentAtionsDialogData
      });
    }
}
