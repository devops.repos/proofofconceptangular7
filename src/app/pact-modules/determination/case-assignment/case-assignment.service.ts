import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Reviewer, AssignedList, CaseAssignmentModel } from './case-assignment.model'

@Injectable({
  providedIn: 'root'
})
export class CaseAssignmentService {
  getCaseAssignmentURL = environment.pactApiUrl + 'CaseAssignment/GetCaseAssignmentList';
  saveAssignmentURL = environment.pactApiUrl + 'CaseAssignment/SaveAssignments';
  reviewersList : Reviewer[];
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }
  
  getCaseAssignments(pactapplicationID : number) : Observable<any>
  {
    //return this.httpClient.get(this.getCaseAssignmentURL+"/"+pactapplicationID, this.httpOptions);
    return this.httpClient.post(this.getCaseAssignmentURL,JSON.stringify(pactapplicationID), this.httpOptions);
  }

  saveAssignments(assignments : AssignedList) : Observable<any>
  {
    return this.httpClient.post(this.saveAssignmentURL, JSON.stringify(assignments), this.httpOptions);
  }
  setReviewers (reviewerList : Reviewer[]){
    this.reviewersList = reviewerList;
  }
  getReviewers () : Reviewer[] {
    return this.reviewersList;
  }
 }

