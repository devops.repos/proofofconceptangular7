import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import "ag-grid-enterprise";
import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';

import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';
// import { CaseAssignmentComponent } from './case-assignment.component';
import { CaseAssignmentActionComponent } from './case-assignment-action/case-assignment-action.component';
import { CaseAssignmentDialogComponent } from './case-assignment-dialog/case-assignment-dialog.component';
import { CustomHeader } from './custom-header.component';

import { ReviewerComponent } from './reviewer-control.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      progressBar: true,
      progressAnimation: 'increasing'
    }),
    AgGridModule.withComponents([
      CaseAssignmentActionComponent,
      ReviewerComponent,
      CustomHeader
    ])
  ],
  declarations: [
    // CaseAssignmentComponent,
    CaseAssignmentActionComponent,
    CaseAssignmentDialogComponent,
    ReviewerComponent,
    CustomHeader
  ],
  exports: [
    CaseAssignmentActionComponent,
    ReviewerComponent
  ]
})
export class CaseAssignmentModule { }
