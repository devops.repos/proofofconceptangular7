import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { CaseAssignmentService } from './case-assignment.service'
import { Reviewer } from './case-assignment.model'

@Component({
  selector: 'app-loading-overlay',
  template: `
    <div>
    <select style="width: 100%" (change)="onSelect($event)">
      <option [value]="0"> Multi Assign to Reviewer </option>
      <option *ngFor="let  reviewer of reviewers" [value]="reviewer.id">{{reviewer.name}}</option>
      <option [value]="999">Un-Assign</option>
    </select>
    </div>
  `,
  styles: [
    `
      .customHeaderMenuButton,
      .customHeaderLabel,
      .customSortDownLabel,
      .customSortUpLabel,
      .customSortRemoveLabel {
        float: left;
        margin: 0 0 0 3px;
      }

      .customSortUpLabel {
        margin: 0;
      }

      .customSortRemoveLabel {
        font-size: 11px;
      }

      .active {
        color: cornflowerblue;
      }
    `,
  ],
})
export class CustomHeader implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  reviewers : Reviewer[];

// unAssingItem : any =  { id : 999, name : 'Un-Assign', count : 0};

  constructor(private caseAssignmentService : CaseAssignmentService) { }
    ngOnInit() {
      this.reviewers = this.caseAssignmentService.getReviewers();
    }

  agInit(params): void {
    this.params = params;
  }

  refresh(): boolean {
    return false;
  }
  onSelect($event) {
    this.params.context.componentParent.onMultiReviewerChange($event.target.value);
  }

}