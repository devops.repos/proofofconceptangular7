import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import { MatRadioChange } from '@angular/material';
import { OutcomeActionComponent } from './outcome-action.component';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

//Service
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { OutcomeService } from './outcome.service'
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import {
  iApplicationOutcome,
  iApplicationRejectionDetails,
  iSaveApplicationRejection,
  iApplicationRejectionReasons,
  iClinicalPreDeterminationInput,
  iClinicalPreDetermination,
  iSaveClinicalPreDeterminationModel
} from './outcome.model';
import { iPriorReferrals } from '../../supportiveHousingSystem/consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications.model';
import { DETTabTimeDuration } from 'src/app/models/determination-common.model';
import { iDocumentPackage, iDocumentLink } from 'src/app/models/document-package.model'
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';

@Component({
  selector: 'app-outcome',
  templateUrl: './outcome.component.html',
  styleUrls: ['./outcome.component.scss']
})

export class OutcomeComponent implements OnInit, OnDestroy {
  //Global Varaibles
  tabSelectedIndex: number = 0;
  currentTabIndex: number = 0;

  isWithdrawn: boolean = false;
  isRejectedQuestion: boolean = false;
  showAdministrativeRejectTab: boolean = false;
  showClinicalPreDeterminationTab: boolean = false;
  showDeterminationLetterTab: boolean = false;
  isAdministrativeRejectAdditionalLetterCommentsRequired: boolean = false;
  isAdditionalLetterCommentsRequired: boolean = false;

  outcomeFormGroup: FormGroup;
  administrativeRejectFormGroup: FormGroup;
  clinicalPreDeterminationFormGroup: FormGroup;

  documentPackageDataSub: Subscription;
  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  existingApprovalsColumnDefs = [];
  existingApprovalsRowData: iPriorReferrals[];
  existingApprovalsDefaultColDef = {};
  existingApprovalsGridOptions: GridOptions;
  existingApprovalsOverlayNoRowsTemplate: string;
  existingApprovalsFrameworkComponents: any;
  existingApprovalsGridColumnApi: any;
  existingApprovalsContext: any;

  applicationRejectionReasonsList = new Array();
  applicationRejectionDetailsArray = new Array();

  withdrawReasons: RefGroupDetails[];
  administrativeRejectReasons: RefGroupDetails[];

  preDeterminationOutcomeText: string = null;
  approvedApplicationPast5YearsText: string = null;
  haveSMIDiagnosisText: string = null;
  hasSubstanceUseLast3MonthsText: string = null;
  hasHospitalizedText: string = null;

  isGenpop: boolean = false;
  isGenpopExplain: boolean = false;
  isClinicalPreDeterminationSaved: boolean = false;

  determinationLetterURL: SafeResourceUrl = null;
  documentPackage: iDocumentPackage[];
  determinationLetterData: iDocumentPackage;
  applicationSummaryReportData: iDocumentPackage;

  determinationLetterGuid: string;
  applicationSummaryGuid: string;

  reportParams: PACTReportUrlParams;
  reportParameters: PACTReportParameters[] = [];

  applicationOutcome: iApplicationOutcome = {
    pactApplicationId: null,
    reasonToWithdraw: null,
    withdrawReasonCode: null,
    additionalLetterComment: null,
    reasonToReject: null,
    optionUserId: null
  }

  saveApplicationRejection: iSaveApplicationRejection = {
    pactApplicationId: null,
    rejectionDocumentsDetail: null,
    rejectionReasons: null,
    addtionalLetterComments: null,
    optionUserId: null
  }

  clinicalPreDeterminationInput: iClinicalPreDeterminationInput = {
    pactApplicationId: null,
    optionUserId: null
  }

  saveClinicalPreDeterminationModel: iSaveClinicalPreDeterminationModel = {
    pactApplicationId: null,
    clinicalPreDeterminationOutcomeType: null,
    housingProgramSelectedByReviewerType: null,
    supportNotOfferedByGenPopType: null,
    supportNotOfferedByGenPopExplain: null,
    optionUserId: null
  }

  detTabTimeDuration: DETTabTimeDuration = {
    pactApplicationId: null,
    tabInName: null,
    tabOutName: null,
    optionUserId: null
  };

  //Constructor
  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private message: ToastrService,
    private outcomeService: OutcomeService,
    private sanitizer: DomSanitizer,
    private determinationSideNavService: DeterminationSideNavService,
    private confirmDialogService: ConfirmDialogService) {
    //Outcome Group Form Builder
    this.outcomeFormGroup = this.formBuilder.group({
      withdrawCtrl: ['', Validators.required],
      withdrawReasonCtrl: ['', Validators.required],
      additionalLetterCommentsCtrl: [''],
      rejectQuestionCtrl: ['', Validators.required]
    });

    //Administrative Reject Form Builder
    this.administrativeRejectFormGroup = this.formBuilder.group({
      administrativeRejectReasonCtrl: [''],
      administrativeRejectAdditionalLetterCommentsCtrl: ['']
    });

    //Clinical Pre Determination Form Builder
    this.clinicalPreDeterminationFormGroup = this.formBuilder.group({
      approvedApplicationPast5YearsCtrl: [''],
      haveSMIDiagnosisCtrl: [''],
      hasSubstanceUseLast3MonthsCtrl: [''],
      hasHospitalizedPast3YearsCtrl: [''],
      housingProgramSelectedCtrl: ['', Validators.required],
      requiresSupportCtrl: ['', Validators.required],
      requiresSupportExplainCtrl: ['']
    });

    //Existing Approvals Grid Column Definitions
    this.existingApprovalsColumnDefs = [
      { headerName: 'Referral Date', field: 'referralDate', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Referring Agency/Site', field: 'referringAgencySite', filter: 'agTextColumnFilter', width: 400 },
      { headerName: 'Eligibility', field: 'eligibility', filter: 'agTextColumnFilter', width: 400 },
      { headerName: 'Prioritization', field: 'prioritization', filter: 'agTextColumnFilter', width: 130 },
      { headerName: 'Service Needs', field: 'serviceNeeds', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Type', field: 'type', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Approval Period', field: 'approvalPeriod', filter: 'agTextColumnFilter', width: 160 },
      { headerName: 'Placement Agency/Site', field: 'placementAgencySite', filter: 'agTextColumnFilter', width: 400 },
      { headerName: 'Move In/Move Out', field: 'moveInMoveOut', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Reason Moved', field: 'reasonMoved', filter: 'agTextColumnFilter', width: 250 },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.existingApprovalsDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.existingApprovalsContext = {
      componentParent: this
    };
    this.existingApprovalsFrameworkComponents = {
      actionRenderer: OutcomeActionComponent
    };
  }

  //On Init
  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
          }
          else {
            return;
          }
        }
      }
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
      if (res) {
        const data = res as iApplicationDeterminationData;
        if (data) {
          this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
          this.applicationDeterminationData = data;
          this.populateOutcomeForm(this.applicationDeterminationData);
          this.getDocumentPackage(this.applicationDeterminationData);
        }
      }
    });

    //Get Withdrawal Reasons From ReFGroupDetails
    var refGroupList = "97,98";
    this.commonService.getRefGroupDetails(refGroupList)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.withdrawReasons = data.filter(d => { return d.refGroupID === 97 });
          this.administrativeRejectReasons = data.filter(d => { return d.refGroupID === 98 });
        },
        error => {
          throw new Error(error.message);
        }
      );
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.setTabTimeDuration();
      this.applicationDeterminationDataSub.unsubscribe();
    }
    if (this.documentPackageDataSub) {
      this.documentPackageDataSub.unsubscribe();
    }
  }

  //Set Tab Values For Tab Time Duration
  setTabTimeDuration() {
    let tabOutName: string;
    let tabInName: string;
    //Current Tab Index && TabOutName
    if (this.currentTabIndex === 0) {
      tabOutName = "ApplicationOutcome"
    }
    else if (this.showAdministrativeRejectTab && this.currentTabIndex === 1) {
      tabOutName = "AdministrativeReject";
    }
    else if (this.showClinicalPreDeterminationTab && this.currentTabIndex === 1) {
      tabOutName = "ClinicalPreDetermination";
    }
    else if (this.showDeterminationLetterTab && this.currentTabIndex === 1) {
      tabOutName = "ApplicationDeterminationLetter";
    }
    else if (this.showDeterminationLetterTab && this.currentTabIndex === 2) {
      tabOutName = "ApplicationDeterminationLetter";
    }
    //Selected Tab Index && TabInName
    if (this.tabSelectedIndex === 0) {
      tabInName = "ApplicationOutcome";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.showAdministrativeRejectTab && this.tabSelectedIndex === 1) {
      tabInName = "AdministrativeReject";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.showClinicalPreDeterminationTab && this.tabSelectedIndex === 1) {
      tabInName = "ClinicalPreDetermination";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.showDeterminationLetterTab && this.tabSelectedIndex === 1) {
      tabInName = "ApplicationDeterminationLetter";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.showDeterminationLetterTab && this.tabSelectedIndex === 2) {
      tabInName = "ApplicationDeterminationLetter";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.saveTabTimeDuration(this.applicationDeterminationData.pactApplicationId, tabOutName, tabInName, this.userData.optionUserId);
    }
  }

  //Save Tab Time Duration
  saveTabTimeDuration(pactApplicationId: number, tabOutName: string, tabInName: string, optionUserId: number) {
    this.detTabTimeDuration.pactApplicationId = pactApplicationId;
    this.detTabTimeDuration.tabOutName = tabOutName;
    this.detTabTimeDuration.tabInName = tabInName;
    this.detTabTimeDuration.optionUserId = optionUserId;
    this.commonService.saveTabTimeDuration(this.detTabTimeDuration).subscribe();
  }

  //Outcome Tab Change Event
  outcomeTabChange() {
    this.setTabTimeDuration();
  }

  //On Administrative Withdraw Change Event
  onAdministrativeWithdrawChange($event: MatRadioChange) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.isMentalHealthConditionTabCompleted) {
      const title = 'Confirm Change';
      const primaryMessage = 'Changing the administrative withdraw question, will restart the Housing Determination.';
      const secondaryMessage = 'Do you want to continue?';
      const confirmButtonName = 'Yes';
      const dismissButtonName = 'No';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          positiveResponse => {
            this.changeAdministrativeWithdraw($event.value);
            this.applicationDeterminationData.isMentalHealthConditionTabCompleted = false;
          },
          negativeResponse => {
            if ($event.value === "Y") {
              this.outcomeFormGroup.controls['withdrawCtrl'].setValue('N');
            }
            else if ($event.value === "N") {
              this.outcomeFormGroup.controls['withdrawCtrl'].setValue('Y');
            }
          }
        );
    }
    else {
      this.changeAdministrativeWithdraw($event.value);
    }
  }

  //Change Adminstrative Withdraw
  changeAdministrativeWithdraw(response: string) {
    if (response === "Y") {
      this.isWithdrawn = true;
      this.isRejectedQuestion = false;
      this.showAdministrativeRejectTab = false;
      this.showClinicalPreDeterminationTab = false;
      this.showDeterminationLetterTab = false;
    }
    else if (response === "N") {
      this.isRejectedQuestion = true;
      this.isWithdrawn = false;
      this.showDeterminationLetterTab = false;
      this.outcomeFormGroup.controls["withdrawReasonCtrl"].reset();
      this.outcomeFormGroup.controls['additionalLetterCommentsCtrl'].setValue(null);
    }
    this.outcomeFormGroup.controls["rejectQuestionCtrl"].reset();
    this.saveOutcomeForm(1);
    this.determinationSideNavService.setAllDeterminationDisabled(true);
  }

  //On Administrative Reject Change Event
  onAdministrativeRejectChange($event: MatRadioChange) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.isMentalHealthConditionTabCompleted) {
      const title = 'Confirm Change';
      const primaryMessage = 'Changing the administrative reject question, will restart the Housing Determination.';
      const secondaryMessage = 'Do you want to continue?';
      const confirmButtonName = 'Yes';
      const dismissButtonName = 'No';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          positiveResponse => {
            this.changeAdministrativeReject($event.value);
            this.applicationDeterminationData.isMentalHealthConditionTabCompleted = false;
          },
          negativeResponse => {
            if ($event.value === "Y") {
              this.outcomeFormGroup.controls['rejectQuestionCtrl'].setValue('N');
            }
            else if ($event.value === "N") {
              this.outcomeFormGroup.controls['rejectQuestionCtrl'].setValue('Y');
            }
          }
        );
    }
    else {
      this.changeAdministrativeReject($event.value);
    }
  }

  //Change Adminstrative Reject
  changeAdministrativeReject(response: string) {
    if (response === "Y") {
      this.refreshAdminstrativeRejectForm();
      if (this.applicationDeterminationData) {
        this.getApplicationRejectionDetails(this.applicationDeterminationData);
        this.getApplicationRejectionReasons(this.applicationDeterminationData);
      }
      this.showAdministrativeRejectTab = true;
      this.showClinicalPreDeterminationTab = false;
      this.determinationLetterURL = null;
      this.showDeterminationLetterTab = false;
    }
    else if (response === "N") {
      if (this.applicationDeterminationData) {
        if ((this.applicationDeterminationData.siteType === "10"
          || this.applicationDeterminationData.siteType === "50"
          || this.applicationDeterminationData.siteType === "84"
          || this.applicationDeterminationData.siteType === "85")
          && this.applicationDeterminationData.applicationType === 46) {
          if (this.applicationDeterminationData.pactClientId) {
            this.getClinicalPreDetermination();
            this.showClinicalPreDeterminationTab = true;
          }
          else {
            if (!this.message.currentlyActive) {
              this.message.error("Missing pact client");
            }
            return;
          }
        }
      }
      this.showAdministrativeRejectTab = false;
      this.showDeterminationLetterTab = false;
    }
    this.outcomeFormGroup.controls["withdrawReasonCtrl"].reset();
    this.outcomeFormGroup.controls['additionalLetterCommentsCtrl'].setValue(null);
    this.saveOutcomeForm(2);
    this.determinationSideNavService.setAllDeterminationDisabled(true);
    this.tabSelectedIndex = this.tabSelectedIndex + 1;
  }


  //Existing Approvals Grid Ready
  existingApprovalsGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.existingApprovalsOverlayNoRowsTemplate = '<span style="color: #337ab7">No Previous Referrals</span>';
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactClientId) {
      this.populateExistingApprovalsGrid(this.applicationDeterminationData.pactClientId);
    }
  }

  //Populate Ecisting Approvals Grid
  populateExistingApprovalsGrid(pactClientId: number) {
    if (pactClientId > 0) {
      this.outcomeService.getLastClientApplication(pactClientId)
        .subscribe(res => {
          if (res) {
            const data = res as iPriorReferrals[];
            if (data && data.length > 0) {
              this.existingApprovalsRowData = data;
            }
            else {
              this.existingApprovalsRowData = null;
            }
          }
        });
    }
  }

  //Get Application Rejection Details
  getApplicationRejectionDetails(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData && applicationDeterminationData.pactApplicationId) {
      //Api call to get Application Rejection Details
      this.outcomeService.getApplicationRejectionDetails(applicationDeterminationData.pactApplicationId)
        .subscribe(res => {
          if (res) {
            const data = res as iApplicationRejectionDetails[];
            if (data && data.length > 0) {
              data.forEach((element) => {
                if (element.applicationRejectionDetails) {
                  this.addToApplicationRejectDetailsArray(element.applicationRejectionDetails)
                }
              });
              if (this.applicationRejectionDetailsArray.indexOf(8) !== -1
                || this.applicationRejectionDetailsArray.indexOf(16) !== -1
                || this.applicationRejectionDetailsArray.indexOf(24) !== -1
                || this.applicationRejectionDetailsArray.indexOf(32) !== -1) {
                this.isAdministrativeRejectAdditionalLetterCommentsRequired = true;
              }
              else {
                this.isAdministrativeRejectAdditionalLetterCommentsRequired = false;
              }
            }
          }
        });
    }
  }

  //Get Application Rejection Reasons
  getApplicationRejectionReasons(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData && applicationDeterminationData.pactApplicationId) {
      //Api call to get Application Rejection Reasons
      this.outcomeService.getApplicationRejectionReasons(applicationDeterminationData.pactApplicationId)
        .subscribe(res => {
          if (res) {
            const data = res as iApplicationRejectionReasons[];
            if (data && data.length > 0) {
              data.forEach((element) => {
                if (element.applicationRejectionReasons) {
                  this.applicationRejectionReasonsList.push(element.applicationRejectionReasons);
                }
              });
              this.administrativeRejectFormGroup.controls['administrativeRejectReasonCtrl'].setValue(this.applicationRejectionReasonsList);
            }
          }
        });
    }
  }

  //On Reject Cell Change Event
  onRejectCellChange(event: { checked: boolean; }, rejectDetailId: number) {
    if (event.checked) {
      this.addToApplicationRejectDetailsArray(rejectDetailId);
    }
    else {
      this.removeFromApplicationRejectDetailsArray(rejectDetailId);
    }
    if (this.applicationRejectionDetailsArray.indexOf(8) !== -1
      || this.applicationRejectionDetailsArray.indexOf(16) !== -1
      || this.applicationRejectionDetailsArray.indexOf(24) !== -1
      || this.applicationRejectionDetailsArray.indexOf(32) !== -1) {
      this.isAdministrativeRejectAdditionalLetterCommentsRequired = true;
    }
    else {
      this.isAdministrativeRejectAdditionalLetterCommentsRequired = false;
    }
    this.showDeterminationLetterTab = false;
    this.determinationLetterURL = null;
  }

  //Add Item To applicationRejectionDetailsArray
  addToApplicationRejectDetailsArray(value: number) {
    const index: number = this.applicationRejectionDetailsArray.indexOf(value);
    if (index === -1) {
      this.applicationRejectionDetailsArray.push(value);
    }
  }

  //Remove Item From applicationRejectionDetailsArray
  removeFromApplicationRejectDetailsArray(value: number) {
    const index: number = this.applicationRejectionDetailsArray.indexOf(value);
    if (index !== -1) {
      this.applicationRejectionDetailsArray.splice(index, 1);
    }
  }

  //Populate Outcome Form
  populateOutcomeForm(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      this.saveTabTimeDuration(applicationDeterminationData.pactApplicationId, null, "ApplicationOutcome", this.userData.optionUserId);
      if (this.applicationDeterminationData.reasonToWithdrawType === 33) {
        this.isWithdrawn = true;
        this.outcomeFormGroup.controls['withdrawCtrl'].setValue('Y');
        this.outcomeFormGroup.controls['withdrawReasonCtrl'].setValue(applicationDeterminationData.withdrawReasonCodeType);
        if (applicationDeterminationData.withdrawReasonCodeType === 759) {
          this.isAdditionalLetterCommentsRequired = true;
        }
        else {
          this.isAdditionalLetterCommentsRequired = false;
        }
        this.outcomeFormGroup.controls['additionalLetterCommentsCtrl'].setValue(applicationDeterminationData.letterComment);
      }
      else if (this.applicationDeterminationData.reasonToWithdrawType === 34) {
        this.isRejectedQuestion = true;
        this.isWithdrawn = false;
        this.outcomeFormGroup.controls['withdrawCtrl'].setValue('N');
      }
      if (this.applicationDeterminationData.reasonToRejectType === 33) {
        this.showAdministrativeRejectTab = true;
        this.showClinicalPreDeterminationTab = false;
        this.showDeterminationLetterTab = false;
        this.outcomeFormGroup.controls['rejectQuestionCtrl'].setValue('Y');
        this.refreshAdminstrativeRejectForm();
        this.getApplicationRejectionDetails(applicationDeterminationData);
        this.getApplicationRejectionReasons(applicationDeterminationData);
        this.administrativeRejectFormGroup.controls['administrativeRejectAdditionalLetterCommentsCtrl'].setValue(applicationDeterminationData.letterComment);
      }
      else if (this.applicationDeterminationData.reasonToRejectType === 34) {
        if (this.applicationDeterminationData) {
          if ((this.applicationDeterminationData.siteType === "10"
            || this.applicationDeterminationData.siteType === "50"
            || this.applicationDeterminationData.siteType === "84"
            || this.applicationDeterminationData.siteType === "85")
            && this.applicationDeterminationData.applicationType === 46) {
            if (this.applicationDeterminationData.pactClientId) {
              this.getClinicalPreDetermination();
              this.showClinicalPreDeterminationTab = true;
            }
            else {
              this.outcomeFormGroup.controls['rejectQuestionCtrl'].setValue('Y');
              if (!this.message.currentlyActive) {
                this.message.error("Please complete the client match to proceed with clinical pre-determination!");
              }
              return;
            }
          }
        }
        this.showAdministrativeRejectTab = false;
        this.showDeterminationLetterTab = false;
        this.outcomeFormGroup.controls['rejectQuestionCtrl'].setValue('N');
      }
    }
  }

  //Refresh Administrative Reject Form
  refreshAdminstrativeRejectForm() {
    while (this.applicationRejectionDetailsArray.length !== 0) {
      this.applicationRejectionDetailsArray.splice(0);
    }
    while (this.applicationRejectionReasonsList.length !== 0) {
      this.applicationRejectionReasonsList.splice(0);
    }
    this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').reset();
    this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').reset();
  }

  //Next Outcome Tab
  nextOutcomeTab() {
    if (this.outcomeFormGroup.get('withdrawCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Administrative Withdraw Question is required.");
      }
      return;
    }
    if (this.isRejectedQuestion && this.outcomeFormGroup.get('rejectQuestionCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Administrative Reject Question is required.");
      }
      return;
    }
    if (this.isWithdrawn) {
      if (this.isWithdrawn && this.outcomeFormGroup.get('withdrawReasonCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Withdrawn Reason is required.");
        }
        return;
      }
      if (this.isWithdrawn && this.outcomeFormGroup.get('withdrawReasonCtrl').value === 759) {
        if (this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value == null) {
          if (!this.message.currentlyActive) {
            this.message.error("Additional Letter Comments is required.");
          }
          return;
        }
        if (!this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value.trim()) {
          if (!this.message.currentlyActive) {
            this.message.error("Additional Letter Comments is required.");
          }
          return;
        }
        if (this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value.length == 1) {
          if (!this.message.currentlyActive) {
            this.message.error("Additional Letter Comments cannot be less than 2 characters.");
          }
          return;
        }
      }
      this.saveOutcomeForm(2);
      this.showDeterminationLetterTab = true;
      this.getDeterminationLetterUrl();
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
    else if (this.outcomeFormGroup.get('rejectQuestionCtrl').value === 'Y') {
      this.saveOutcomeForm(2);
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
    else {
      if (this.applicationDeterminationData) {
        if ((this.applicationDeterminationData.siteType === "10"
          || this.applicationDeterminationData.siteType === "50"
          || this.applicationDeterminationData.siteType === "84"
          || this.applicationDeterminationData.siteType === "85")
          && this.applicationDeterminationData.applicationType === 46) {
          this.tabSelectedIndex = this.tabSelectedIndex + 1;
        }
        else {
          if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
            this.saveClinicalPreDeterminationModel.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
            this.saveClinicalPreDeterminationModel.clinicalPreDeterminationOutcomeType = 669;
            this.saveClinicalPreDeterminationModel.housingProgramSelectedByReviewerType = null;
            this.saveClinicalPreDeterminationModel.supportNotOfferedByGenPopType = null;
            this.saveClinicalPreDeterminationModel.supportNotOfferedByGenPopExplain = null;
            this.saveClinicalPreDeterminationModel.optionUserId = this.userData.optionUserId;
            this.saveClinicalPreDeterminationApi(this.saveClinicalPreDeterminationModel, 1);
          }
        }
      }
    }
  }

  //Previous Outcome Tab
  previousOutcomeTab() {
    if (this.isWithdrawn) {
      if (this.isWithdrawn && this.outcomeFormGroup.get('withdrawReasonCtrl').invalid) {
        if (!this.message.currentlyActive) {
          this.message.error("Withdrawn Reason is required.");
        }
        return;
      }
      if (this.isWithdrawn && this.outcomeFormGroup.get('withdrawReasonCtrl').value === 759) {
        if (this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value == null) {
          if (!this.message.currentlyActive) {
            this.message.error("Additional Letter Comments is required.");
          }
          return;
        }
        if (!this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value.trim()) {
          if (!this.message.currentlyActive) {
            this.message.error("Additional Letter Comments is required.");
          }
          return;
        }
        if (this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value.length == 1) {
          if (!this.message.currentlyActive) {
            this.message.error("Additional Letter Comments cannot be less than 2 characters.");
          }
          return;
        }
      }
      this.saveOutcomeForm(2);
    }
    this.router.navigate(['/ds/client-case-folder', this.applicationDeterminationData.pactApplicationId]);
  }

  //Save Outcome
  saveOutcome() {
    if (this.isWithdrawn && this.outcomeFormGroup.get('withdrawReasonCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Withdrawn Reason is required.");
      }
      return;
    }
    if (this.isWithdrawn && this.outcomeFormGroup.get('withdrawReasonCtrl').value === 759) {
      if (this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value == null) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments is required.");
        }
        return;
      }
      if (!this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value.trim()) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments is required.");
        }
        return;
      }
      if (this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value.length == 1) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments cannot be less than 2 characters.");
        }
        return;
      }
    }
    this.saveOutcomeForm(2);
    if (!this.message.currentlyActive) {
      this.message.success("Form saved successfully.");
    }
  }

  //Save Outcome Form
  saveOutcomeForm(path: number) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.applicationOutcome.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.applicationOutcome.reasonToWithdraw = this.outcomeFormGroup.get('withdrawCtrl').value === 'Y' ? 33 : this.outcomeFormGroup.get('withdrawCtrl').value === 'N' ? 34 : null;
      this.applicationOutcome.withdrawReasonCode = this.outcomeFormGroup.get('withdrawReasonCtrl').value;
      this.applicationOutcome.additionalLetterComment = this.outcomeFormGroup.get('additionalLetterCommentsCtrl').value;
      this.applicationOutcome.reasonToReject = this.outcomeFormGroup.get('rejectQuestionCtrl').value === 'Y' ? 33 : this.outcomeFormGroup.get('rejectQuestionCtrl').value === 'N' ? 34 : null;
      this.applicationOutcome.optionUserId = this.userData.optionUserId;      
      //Api call to save outcome form data
      this.outcomeService.saveApplicationOutcome(this.applicationOutcome)
        .subscribe(res => {
          if (res && res.isSaved) {
            if (this.isWithdrawn && path === 2) {
              this.determinationSideNavService.setIsSignOffFollowUpDisabled(false);
            }
            return;
          }
        });
    }
  }

  //Previous Determination Letter Tab
  previousDeterminationLetterTab() {
    this.tabSelectedIndex = this.tabSelectedIndex - 1;
  }

  //Next Determination Letter Tab
  nextDeterminationLetterTab() {
    this.determinationSideNavService.setIsSignOffFollowUpDisabled(false);
    this.router.navigate(['/ds/sign-off-follow-up', this.applicationDeterminationData.pactApplicationId]);
  }

  //Save Administrative Reject Event
  saveAdministrativeReject() {
    if (this.applicationRejectionDetailsArray.indexOf(8) !== -1
      || this.applicationRejectionDetailsArray.indexOf(16) !== -1
      || this.applicationRejectionDetailsArray.indexOf(24) !== -1
      || this.applicationRejectionDetailsArray.indexOf(32) !== -1) {
      if (this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value == null) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments is required.");
        }
        return;
      }
      if (!this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value.trim()) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments is required.");
        }
        return;
      }
      if (this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value.length == 1) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments cannot be less than 2 characters.");
        }
        return;
      }
    }
    if ((this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value &&
      this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value.length > 0)
      || this.applicationRejectionDetailsArray.length > 0) {
      this.saveRejectDetails();
      if (!this.message.currentlyActive) {
        this.message.success("Form saved successfully.");
      }
    }
    else {
      if (!this.message.currentlyActive) {
        this.message.error("Either missing documentation or missing one or more reason is required.");
      }
    }
  }

  //Save Administrative Rejection Details
  saveRejectDetails() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.saveApplicationRejection.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.saveApplicationRejection.rejectionDocumentsDetail = this.applicationRejectionDetailsArray.length > 0 ? this.applicationRejectionDetailsArray.join(',') : null;
      if (this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value && this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value.length > 0) {
        this.saveApplicationRejection.rejectionReasons = this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value.join(',');
      }
      else {
        this.saveApplicationRejection.rejectionReasons = null;
      }
      this.saveApplicationRejection.addtionalLetterComments = this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value ? this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value.trim() : null;
      this.saveApplicationRejection.optionUserId = this.userData.optionUserId;
      //Api call to save administrative reject form data
      this.outcomeService.saveAdministrativeRejection(this.saveApplicationRejection)
        .subscribe(res => {
          if (res && res.isSaved) {
            this.determinationSideNavService.setIsSignOffFollowUpDisabled(false);
            return;
          }
        });
    }
  }

  //Next Administrative Reject
  nextAdministrativeRejectTab() {
    if (this.applicationRejectionDetailsArray.indexOf(8) !== -1
      || this.applicationRejectionDetailsArray.indexOf(16) !== -1
      || this.applicationRejectionDetailsArray.indexOf(24) !== -1
      || this.applicationRejectionDetailsArray.indexOf(32) !== -1) {
      if (this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value == null) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments is required.");
        }
        return;
      }
      if (!this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value.trim()) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments is required.");
        }
        return;
      }
      if (this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value.length == 1) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments cannot be less than 2 characters.");
        }
        return;
      }
    }
    if ((this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value &&
      this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value.length > 0)
      || this.applicationRejectionDetailsArray.length > 0) {
      this.saveRejectDetails();
      this.showDeterminationLetterTab = true;
      this.getDeterminationLetterUrl();
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
    else {
      if (!this.message.currentlyActive) {
        this.message.error("Either missing documentation or missing one or more reason is required.");
      }
    }
  }

  //Previous Administrative Reject
  previousAdministrativeRejectTab() {
    if (this.applicationRejectionDetailsArray.indexOf(8) !== -1
      || this.applicationRejectionDetailsArray.indexOf(16) !== -1
      || this.applicationRejectionDetailsArray.indexOf(24) !== -1
      || this.applicationRejectionDetailsArray.indexOf(32) !== -1) {
      if (this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value == null) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments is required.");
        }
        return;
      }
      if (!this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value.trim()) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments is required.");
        }
        return;
      }
      if (this.administrativeRejectFormGroup.get('administrativeRejectAdditionalLetterCommentsCtrl').value.length == 1) {
        if (!this.message.currentlyActive) {
          this.message.error("Additional Letter Comments cannot be less than 2 characters.");
        }
        return;
      }
    }
    if ((this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value &&
      this.administrativeRejectFormGroup.get('administrativeRejectReasonCtrl').value.length > 0)
      || this.applicationRejectionDetailsArray.length > 0) {
      this.saveRejectDetails();
      this.showDeterminationLetterTab = true;
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
    else {
      if (!this.message.currentlyActive) {
        this.message.error("Either missing documentation or missing one or more reason is required.");
      }
    }
  }

  //On Application Summary
  onApplicationSummary(pactClientId: number, pactApplicationId: number) {
    if (pactApplicationId) {
      this.commonService.setIsOverlay(true);
      this.documentPackageDataSub = this.commonService.getDocumentPackage(pactApplicationId)
        .subscribe(result => {
          if (result) {
            this.documentPackage = result as iDocumentPackage[];
            if (this.documentPackage) {
              this.applicationSummaryReportData = this.documentPackage.find(d => { return d.documentType === 327 });
              if (this.applicationSummaryReportData) {
                if (this.applicationSummaryReportData.fileNetDocID) {
                  this.commonService.getFileNetDocumentLink(this.applicationSummaryReportData.fileNetDocID.toString()).subscribe(res => {
                    const data = res as iDocumentLink;
                    if (data && data.linkURL) {
                      this.commonService.OpenWindow(data.linkURL);
                      this.commonService.setIsOverlay(false);
                    }
                  });
                }
                else {
                  this.viewReportServerDocument(327, pactApplicationId);
                }
              }
            }
          }
        });
    }
  }

  //Get Clinical Pre Determination
  getClinicalPreDetermination() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.clinicalPreDeterminationInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.clinicalPreDeterminationInput.optionUserId = this.userData.optionUserId;
      this.outcomeService.getClinicalPreDetermination(this.clinicalPreDeterminationInput)
        .subscribe(res => {
          if (res) {
            const data = res as iClinicalPreDetermination;
            if (data) {
              this.populateClinicalPreDetermination(data);
            }
          }
        });
    }
  }

  //Populate Clinical Pre Determination Form
  populateClinicalPreDetermination(clinicalPreDetermination: iClinicalPreDetermination) {
    if (clinicalPreDetermination) {
      this.preDeterminationOutcomeText = clinicalPreDetermination.clinicalPreDeterminationOutcomeTypeDescription;
      this.approvedApplicationPast5YearsText = clinicalPreDetermination.approvedSHApplicationPast5YearsTypeDescription;
      this.haveSMIDiagnosisText = clinicalPreDetermination.haveSMIDiagnosisTypeDescription;
      this.hasSubstanceUseLast3MonthsText = clinicalPreDetermination.hasUsedSubstancesPast3MonthsTypeDescription;;
      this.hasHospitalizedText = clinicalPreDetermination.hasHospitalizedPast3YearsTypeDescription;
      //this.hasHospitalizedText = clinicalPreDetermination.hasHospitalizedDescription;
      if (clinicalPreDetermination.approvedSHApplicationPast5YearsType === 33) {
        this.clinicalPreDeterminationFormGroup.controls['approvedApplicationPast5YearsCtrl'].setValue('Y');
      }
      else if (clinicalPreDetermination.approvedSHApplicationPast5YearsType === 34) {
        this.clinicalPreDeterminationFormGroup.controls['approvedApplicationPast5YearsCtrl'].setValue('N');
      }
      if (clinicalPreDetermination.haveSMIDiagnosisType === 33) {
        this.clinicalPreDeterminationFormGroup.controls['haveSMIDiagnosisCtrl'].setValue('Y');
      }
      else if (clinicalPreDetermination.haveSMIDiagnosisType === 34) {
        this.clinicalPreDeterminationFormGroup.controls['haveSMIDiagnosisCtrl'].setValue('N');
      }
      if (clinicalPreDetermination.hasUsedSubstancesPast3MonthsType === 33) {
        this.clinicalPreDeterminationFormGroup.controls['hasSubstanceUseLast3MonthsCtrl'].setValue('Y');
      }
      else if (clinicalPreDetermination.hasUsedSubstancesPast3MonthsType === 34) {
        this.clinicalPreDeterminationFormGroup.controls['hasSubstanceUseLast3MonthsCtrl'].setValue('N');
      }
      if (clinicalPreDetermination.hasHospitalizedPast3YearsType === 33) {
        this.clinicalPreDeterminationFormGroup.controls['hasHospitalizedPast3YearsCtrl'].setValue('Y');
      }
      else if (clinicalPreDetermination.hasHospitalizedPast3YearsType === 34) {
        this.clinicalPreDeterminationFormGroup.controls['hasHospitalizedPast3YearsCtrl'].setValue('N');
      }
      if (clinicalPreDetermination.housingProgramSelectedByReviewerType) {
        if (clinicalPreDetermination.housingProgramSelectedByReviewerType === 669) {
          this.clinicalPreDeterminationFormGroup.controls['housingProgramSelectedCtrl'].setValue('S');
        }
        else if (clinicalPreDetermination.housingProgramSelectedByReviewerType === 670) {
          this.clinicalPreDeterminationFormGroup.controls['housingProgramSelectedCtrl'].setValue('G');
          this.isGenpop = true;
        }
      }
      else {
        if (clinicalPreDetermination.clinicalPreDeterminationOutcomeType === 669) {
          this.clinicalPreDeterminationFormGroup.controls['housingProgramSelectedCtrl'].setValue('S');
        }
        else if (clinicalPreDetermination.clinicalPreDeterminationOutcomeType === 670) {
          this.clinicalPreDeterminationFormGroup.controls['housingProgramSelectedCtrl'].setValue('G');
          this.isGenpop = true;
        }
      }
      if (clinicalPreDetermination.supportNotOfferedByGenPopType === 33) {
        this.clinicalPreDeterminationFormGroup.controls['requiresSupportCtrl'].setValue('Y');
        this.isGenpopExplain = true;
        this.clinicalPreDeterminationFormGroup.controls['requiresSupportExplainCtrl'].setValue(clinicalPreDetermination.supportNotOfferedByGenPopExplain);
      }
      else if (clinicalPreDetermination.supportNotOfferedByGenPopType === 34) {
        this.clinicalPreDeterminationFormGroup.controls['requiresSupportCtrl'].setValue('N');
      }
    }
  }

  //On Housing Program Change Event
  onHousingProgramChange($event: MatRadioChange) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.isMentalHealthConditionTabCompleted) {
      const title = 'Confirm Change';
      const primaryMessage = 'Changing the determination to "Supportive Housing" or "General Population" will restart the Housing Determination.';
      const secondaryMessage = 'Do you want to continue?';
      const confirmButtonName = 'Yes';
      const dismissButtonName = 'No';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          positiveResponse => {
            this.changeHousingProgram($event.value);
            this.applicationDeterminationData.isMentalHealthConditionTabCompleted = false;
          },
          negativeResponse => {
            if ($event.value === "S") {
              this.clinicalPreDeterminationFormGroup.controls['housingProgramSelectedCtrl'].setValue('G');
            }
            else if ($event.value === "G") {
              this.clinicalPreDeterminationFormGroup.controls['housingProgramSelectedCtrl'].setValue('S');
            }
          }
        );
    }
    else {
      this.changeHousingProgram($event.value);
    }
  }

  //Change Housing Program
  changeHousingProgram(response: string) {
    if (response === "G") {
      this.isGenpop = true;
    }
    else if (response === "S") {
      this.clinicalPreDeterminationFormGroup.controls["requiresSupportExplainCtrl"].reset();
      this.clinicalPreDeterminationFormGroup.controls["requiresSupportCtrl"].reset();
      this.isGenpop = false;
      this.isGenpopExplain = false;
    }
    this.determinationSideNavService.setAllDeterminationDisabled(true);
  }

  //On Require Support Change Event
  onRequireSupportChange($event: MatRadioChange) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.isMentalHealthConditionTabCompleted) {
      const title = 'Confirm Change';
      const primaryMessage = 'Changing the support required question, will restart the Housing Determination.';
      const secondaryMessage = 'Do you want to continue?';
      const confirmButtonName = 'Yes';
      const dismissButtonName = 'No';
      this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
        .then(
          positiveResponse => {
            this.changeRequireSupport($event.value);
            this.applicationDeterminationData.isMentalHealthConditionTabCompleted = false;
          },
          negativeResponse => {
            if ($event.value === "Y") {
              this.clinicalPreDeterminationFormGroup.controls['requiresSupportCtrl'].setValue('N');
            }
            else if ($event.value === "N") {
              this.clinicalPreDeterminationFormGroup.controls['requiresSupportCtrl'].setValue('Y');
            }
          }
        );
    }
    else {
      this.changeRequireSupport($event.value);
    }
  }

  //Change Require Support
  changeRequireSupport(response: string) {
    if (response === "Y") {
      this.isGenpopExplain = true;
    }
    else if (response === "N") {
      this.clinicalPreDeterminationFormGroup.controls["requiresSupportExplainCtrl"].reset();
      this.isGenpopExplain = false;
    }
    this.determinationSideNavService.setAllDeterminationDisabled(true);
  }

  //Previous Clinical Pre Determination Tab 
  previousClinicalPreDeterminationTab() {
    if (this.isGenpop && this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Please select the options for support required.");
      }
      return;
    }
    if (this.isGenpopExplain) {
      if (this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value == null) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain is required.");
        }
        return;
      }
      if (!this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.trim()) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain is required.");
        }
        return;
      }
      if (this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.length == 1) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain cannot be less than 2 characters.");
        }
        return;
      }
    }
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.saveClinicalPreDeterminationModel.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.saveClinicalPreDeterminationModel.clinicalPreDeterminationOutcomeType = null;
      this.saveClinicalPreDeterminationModel.housingProgramSelectedByReviewerType = this.clinicalPreDeterminationFormGroup.get('housingProgramSelectedCtrl').value === 'S' ? 669 : this.clinicalPreDeterminationFormGroup.get('housingProgramSelectedCtrl').value === 'G' ? 670 : null;
      this.saveClinicalPreDeterminationModel.supportNotOfferedByGenPopType = this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').value === 'Y' ? 33 : this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').value === 'N' ? 34 : null;
      this.saveClinicalPreDeterminationModel.supportNotOfferedByGenPopExplain = this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value ? this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.trim() : null;
      this.saveClinicalPreDeterminationModel.optionUserId = this.userData.optionUserId;
      this.saveClinicalPreDeterminationApi(this.saveClinicalPreDeterminationModel, 3);
    }
  }

  //Next Clinical Pre Determination Tab 
  nextClinicalPreDeterminationTab() {
    if (this.isGenpop && this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Please select the options for support required.");
      }
      return;
    }
    if (this.isGenpopExplain) {
      if (this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value == null) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain is required.");
        }
        return;
      }
      if (!this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.trim()) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain is required.");
        }
        return;
      }
      if (this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.length == 1) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain cannot be less than 2 characters.");
        }
        return;
      }
    }
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.saveClinicalPreDeterminationModel.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.saveClinicalPreDeterminationModel.clinicalPreDeterminationOutcomeType = null;
      this.saveClinicalPreDeterminationModel.housingProgramSelectedByReviewerType = this.clinicalPreDeterminationFormGroup.get('housingProgramSelectedCtrl').value === 'S' ? 669 : this.clinicalPreDeterminationFormGroup.get('housingProgramSelectedCtrl').value === 'G' ? 670 : null;
      this.saveClinicalPreDeterminationModel.supportNotOfferedByGenPopType = this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').value === 'Y' ? 33 : this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').value === 'N' ? 34 : null;
      this.saveClinicalPreDeterminationModel.supportNotOfferedByGenPopExplain = this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value ? this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.trim() : null;
      this.saveClinicalPreDeterminationModel.optionUserId = this.userData.optionUserId;
      this.saveClinicalPreDeterminationApi(this.saveClinicalPreDeterminationModel, 1);
    }
  }

  //Save Clinical Pre Determination
  saveClinicalPreDetermination() {
    if (this.isGenpop && this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Please select the options for support required.");
      }
      return;
    }
    if (this.isGenpopExplain) {
      if (this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value == null) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain is required.");
        }
        return;
      }
      if (!this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.trim()) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain is required.");
        }
        return;
      }
      if (this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.length == 1) {
        if (!this.message.currentlyActive) {
          this.message.error("Explain cannot be less than 2 characters.");
        }
        return;
      }
    }
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.saveClinicalPreDeterminationModel.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.saveClinicalPreDeterminationModel.clinicalPreDeterminationOutcomeType = null;
      this.saveClinicalPreDeterminationModel.housingProgramSelectedByReviewerType = this.clinicalPreDeterminationFormGroup.get('housingProgramSelectedCtrl').value === 'S' ? 669 : this.clinicalPreDeterminationFormGroup.get('housingProgramSelectedCtrl').value === 'G' ? 670 : null;
      this.saveClinicalPreDeterminationModel.supportNotOfferedByGenPopType = this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').value === 'Y' ? 33 : this.clinicalPreDeterminationFormGroup.get('requiresSupportCtrl').value === 'N' ? 34 : null;
      this.saveClinicalPreDeterminationModel.supportNotOfferedByGenPopExplain = this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value ? this.clinicalPreDeterminationFormGroup.get('requiresSupportExplainCtrl').value.trim() : null;
      this.saveClinicalPreDeterminationModel.optionUserId = this.userData.optionUserId;
      this.saveClinicalPreDeterminationApi(this.saveClinicalPreDeterminationModel, 2);
    }
  }

  //Api call to save clinical pre determination
  saveClinicalPreDeterminationApi(saveClinicalPreDeterminationModel: iSaveClinicalPreDeterminationModel, path: number) {
    this.outcomeService.saveClinicalPreDetermination(saveClinicalPreDeterminationModel)
      .subscribe(res => {
        if (res && res.isSaved) {
          if (path === 1) {
            if (this.applicationDeterminationData.isMatchSummaryTabCompleted
              || (this.applicationDeterminationData.isHousingProgramMatchTabCompleted
                && this.applicationDeterminationData.isDHSMatchTabCompleted
                && this.applicationDeterminationData.isHASAMatchTabCompleted
                && this.applicationDeterminationData.isMedicaidMatchTabCompleted
                && this.applicationDeterminationData.isSTARSMatchTabCompleted)) {
              this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
              this.determinationSideNavService.setIsClinicalReviewDisabled(false);
              this.router.navigate(['/ds/clinical-review', saveClinicalPreDeterminationModel.pactApplicationId]);
            }
            else {
              if (!this.message.currentlyActive) {
                this.message.error("To proceed with Housing Determination, please complete the client match!");
              }
            }
          }
          else if (path === 2) {
            if (!this.message.currentlyActive) {
              this.message.success("Form saved successfully.");
            }
          }
          else if (path === 3) {
            this.tabSelectedIndex = this.tabSelectedIndex - 1;
          }
        }
      });
  }

  //Get Document Package
  getDocumentPackage(applicationDeterminationData: iApplicationDeterminationData) {
    this.documentPackageDataSub = this.commonService.getDocumentPackage(applicationDeterminationData.pactApplicationId)
      .subscribe(result => {
        if (result) {
          this.documentPackage = result as iDocumentPackage[];
          if (this.documentPackage) {
            this.determinationLetterData = this.documentPackage.find(d => { return d.documentType === 326 });
          }
        }
      });
  }

  //Get Determination Letter URL
  getDeterminationLetterUrl() {
    if (this.determinationLetterData) {
      if (this.determinationLetterData.fileNetDocID) {
        this.commonService.getFileNetDocumentLink(this.determinationLetterData.fileNetDocID.toString()).subscribe(res => {
          const data = res as iDocumentLink;
          if (data && data.linkURL) {
            this.determinationLetterURL = this.sanitizer.bypassSecurityTrustResourceUrl(data.linkURL);
          }
        });
      }
      else {
        if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
          this.viewReportServerDocument(326, this.applicationDeterminationData.pactApplicationId);
        }
      }
    }
  }

  //View SSRS Document
  viewReportServerDocument(documentType: number, pactApplicationId: number) {
    switch (documentType) {
      case 326: {
        if (!this.determinationLetterGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "DeterminationLetter", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.determinationLetterGuid = res;
                  this.generateReport(res, "DeterminationLetter", documentType);
                }
              }
            );
        }
        else {
          this.generateReport(this.determinationLetterGuid, "DeterminationLetter", documentType);
        }
        break;
      }
      case 327: {
        if (!this.applicationSummaryGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PACTReportApplicationSummary", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.applicationSummaryGuid = res;
                  this.generateReport(res, "PACTReportApplicationSummary", documentType);
                }
              }
            );
        }
        else {
          this.generateReport(this.applicationSummaryGuid, "PACTReportApplicationSummary", documentType);
        }
        break;
      }
    }
  }

  //Generate Report
  generateReport(reportGuid: string, reportName: string, documentType: number) {
    this.reportParams = { reportParameterID: reportGuid, reportName: reportName, "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            if (documentType === 326) {
              this.determinationLetterURL = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(data));
            }
            else if (documentType === 327) {
              this.commonService.OpenWindow(URL.createObjectURL(data));
              this.commonService.setIsOverlay(false);
            }
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }

  //On Reason Change 
  onReasonChange(event: any) {
    this.showDeterminationLetterTab = false;
    this.determinationLetterURL = null;
    if (event.value == 759) {
      this.isAdditionalLetterCommentsRequired = true;
    }
    else {
      this.isAdditionalLetterCommentsRequired = false;
    }
  }

  //On Administrative Reject Reason Change
  onAdministrativeRejectReasonChange(event: any) {
    this.showDeterminationLetterTab = false;
    this.determinationLetterURL = null;
  }
}
