import { ClassGetter } from '@angular/compiler/src/output/output_ast';

export interface iApplicationOutcome {
    pactApplicationId: number;
    reasonToWithdraw: number;
    withdrawReasonCode: number;
    additionalLetterComment: string;
    reasonToReject: number;
    optionUserId: number;
}

export interface iApplicationRejectionDetails {
    applicationRejectionDetails: number;
}

export interface iSaveApplicationRejection {
    pactApplicationId: number;
    rejectionDocumentsDetail: string;
    rejectionReasons: string;
    addtionalLetterComments: string;
    optionUserId: number;
}

export interface iApplicationRejectionReasons {
    applicationRejectionReasons: number;
}

export interface iClinicalPreDetermination {
    clinicalPreDeterminationOutcomeType?: number;
    clinicalPreDeterminationOutcomeTypeDescription: string;
    housingProgramSelectedByReviewerType?: number;
    housingProgramSelectedByReviewerTypeDescription: string;
    clientCategoryType?: number;
    clientCategoryTypeDescription: string;
    approvedSHApplicationPast5YearsType?: number;
    approvedSHApplicationPast5YearsTypeDescription: string;
    haveSMIDiagnosisType?: number;
    haveSMIDiagnosisTypeDescription: string;
    hasUsedSubstancesPast3MonthsType?: number;
    hasUsedSubstancesPast3MonthsTypeDescription: string;
    hasHospitalizedPast3YearsType?: number;
    hasHospitalizedPast3YearsTypeDescription: string;
    hasHospitalizedDescription: string;
    supportNotOfferedByGenPopType?: number;
    supportNotOfferedByGenPopExplain: string;
}

export interface iClinicalPreDeterminationInput {
    pactApplicationId: number;
    optionUserId: number;
}

export interface iSaveClinicalPreDeterminationModel {
    pactApplicationId?: number;
    clinicalPreDeterminationOutcomeType?: number;
    housingProgramSelectedByReviewerType?: number;
    supportNotOfferedByGenPopType?: number;
    supportNotOfferedByGenPopExplain: string;
    optionUserId?: number;
}