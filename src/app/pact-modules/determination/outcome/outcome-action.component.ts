import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { MatDialog } from '@angular/material';
import { iHousingApplicationSupportingDocumentsData } from 'src/app/shared/housing-application-supporting-documents/housing-application-supporting-document.model';
import { DeterminationDocumentsDialogComponent } from './determination-documents-dialog.component'

@Component({
  selector: "outcome-action",
  template: `
    <mat-icon
      class="menu-icon"
      color="warn"
      [matMenuTriggerFor]="outcomeAction">
      more_vert
    </mat-icon>
    <mat-menu #outcomeAction="matMenu">
      <button mat-menu-item (click)="onApplicationSummary()" class="menu-button">Application Summary</button>
      <button mat-menu-item (click)="onDeterminationDocuments()" class="menu-button">Determination Documents</button>
    </mat-menu>
  `,
  styles: [
    `
      .menu-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 40px;
        width: 100%;
        height: 40px;
      }
    `
  ]
})

export class OutcomeActionComponent implements ICellRendererAngularComp {
  params: any;
  cell: any;

  housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null,
    expandSection: null
  }

  //Constrctor
  constructor(public dialog: MatDialog) {
  }

  //Init
  agInit(params: any) {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  //Refresh
  refresh(): boolean {
    return false;
  }

  //On Application Summary
  onApplicationSummary() {
    this.params.context.componentParent.onApplicationSummary(this.params.data.pactClientID, this.params.data.pactApplicationID);
  }

  //On Determination Documents
  onDeterminationDocuments() {
    this.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    this.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    this.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    this.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    this.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    this.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    this.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    this.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientID;
    this.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalTo;
    this.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
    this.housingApplicationSupportingDocumentsData.expandSection = 2;
    this.openDialog(this.housingApplicationSupportingDocumentsData);
  }

  //Open Housing Application Supporting Documents Dialog
  openDialog(housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData): void {
    this.dialog.open(DeterminationDocumentsDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: housingApplicationSupportingDocumentsData
    });
  }
}
