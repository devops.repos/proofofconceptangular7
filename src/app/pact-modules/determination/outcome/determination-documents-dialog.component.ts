import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { iHousingApplicationSupportingDocumentsData } from 'src/app/shared/housing-application-supporting-documents/housing-application-supporting-document.model';

@Component({
    selector: 'app-determination-documents-dialog',
    templateUrl: './determination-documents-dialog.html',
    styleUrls: ['./determination-documents-dialog.component.scss']
})

export class DeterminationDocumentsDialogComponent implements OnInit {
    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) public housingApplicationSupportingDocumentsData: iHousingApplicationSupportingDocumentsData,
        private dialogRef: MatDialogRef<DeterminationDocumentsDialogComponent>) {
    }

    //Close the dialog on close button
    CloseDialog() {
        this.dialogRef.close(true);
    }

    //On Init
    ngOnInit() {
    }
}