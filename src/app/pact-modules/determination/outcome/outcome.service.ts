import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

//Models
import { iApplicationOutcome, iSaveApplicationRejection, iClinicalPreDeterminationInput, iSaveClinicalPreDeterminationModel } from './outcome.model';

@Injectable({
    providedIn: 'root'
})

export class OutcomeService {
    saveApplicationOutcomeURL = environment.pactApiUrl + 'DETApplicationReview/SaveApplicationOutcome';
    getApplicationRejectionDetailsURL = environment.pactApiUrl + 'DETApplicationReview/GetApplicationRejectionDetails';
    saveAdministrativeRejectionURL = environment.pactApiUrl + 'DETApplicationReview/SaveAdministrativeRejection';
    getApplicationRejectionReasonsURL = environment.pactApiUrl + 'DETApplicationReview/GetApplicationRejectionReasons';
    getClinicalPreDeterminationURL = environment.pactApiUrl + 'DETApplicationReview/GetClinicalPreDetermination';
    saveClinicalPreDeterminationURL = environment.pactApiUrl + 'DETApplicationReview/SaveClinicalPreDetermination';
    getLastClientApplicationURL = environment.pactApiUrl + 'DETApplicationReview/GetLastClientApplication';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private httpClient: HttpClient) {
    }

    //Save Application Outcome
    saveApplicationOutcome(applicationOutcome: iApplicationOutcome): Observable<any> {
        if (applicationOutcome) {
            return this.httpClient.post(this.saveApplicationOutcomeURL, JSON.stringify(applicationOutcome), this.httpOptions);
        }
    }

    //Get Application Rejection Details
    getApplicationRejectionDetails(pactApplicationId: number): Observable<any> {
        if (pactApplicationId) {
            return this.httpClient.post(this.getApplicationRejectionDetailsURL, JSON.stringify(pactApplicationId), this.httpOptions);
        }
    }

    //Get Application Rejection Reasons
    getApplicationRejectionReasons(pactApplicationId: number): Observable<any> {
        if (pactApplicationId) {
            return this.httpClient.post(this.getApplicationRejectionReasonsURL, JSON.stringify(pactApplicationId), this.httpOptions);
        }
    }

    //Save Administrative Rejection
    saveAdministrativeRejection(saveApplicationRejection: iSaveApplicationRejection): Observable<any> {
        if (saveApplicationRejection) {
            return this.httpClient.post(this.saveAdministrativeRejectionURL, JSON.stringify(saveApplicationRejection), this.httpOptions);
        }
    }

    //Get Clinical Pre Determination
    getClinicalPreDetermination(clinicalPreDeterminationInput: iClinicalPreDeterminationInput): Observable<any> {
        if (clinicalPreDeterminationInput) {
            return this.httpClient.post(this.getClinicalPreDeterminationURL, JSON.stringify(clinicalPreDeterminationInput), this.httpOptions);
        }
    }

    //Save Clinical Pre Determination
    saveClinicalPreDetermination(saveClinicalPreDeterminationModel: iSaveClinicalPreDeterminationModel): Observable<any> {
        if (saveClinicalPreDeterminationModel) {
            return this.httpClient.post(this.saveClinicalPreDeterminationURL, JSON.stringify(saveClinicalPreDeterminationModel), this.httpOptions);
        }
    }

     //Get Last Client Application
     getLastClientApplication(pactClientId: number): Observable<any> {
        if (pactClientId) {
            return this.httpClient.post(this.getLastClientApplicationURL, JSON.stringify(pactClientId), this.httpOptions);
        }
    }
}