import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';

//Service
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';

//Models
import { DETTabTimeDuration } from 'src/app/models/determination-common.model';
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model';

export interface ClientData {
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
  pactClientId: number;
  pactApplicationId: number;
  referralDate: string;
};

export interface iClientDocumentsData {
  agencyNumber: string;
  siteNumber: string;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  genderTypeDescription: string;
  cin: string;
  pactClientId: number;
  approvalExpiryDate: string;
  pactApplicationId: number;
};

@Component({
  selector: 'app-client-case-folder',
  templateUrl: './client-case-folder.component.html',
  styleUrls: ['./client-case-folder.component.scss']
})

export class ClientCaseFolderComponent implements OnInit, OnDestroy {
  //Global Varaibles
  tabSelectedIndex: number = 0;
  currentTabIndex: number = 0;
  capsClientId: number = 0;
  optionUserId: number = 0;
  pactClientId: number = 0;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  clientData: ClientData = {
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null,
    pactClientId: null,
    pactApplicationId: null,
    referralDate: null
  };

  detTabTimeDuration: DETTabTimeDuration = {
    pactApplicationId: null,
    tabInName: null,
    tabOutName: null,
    optionUserId: null
  };

  clientDocumentsData: iClientDocumentsData = {
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null,
    pactClientId: null,
    approvalExpiryDate: null,
    pactApplicationId: null
  }

  //Constructor
  constructor(private route: ActivatedRoute,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private router: Router) {
  }

  //On Init
  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
            //User Data
            this.userDataSub = this.userService.getUserData().subscribe(res => {
              if (res) {
                this.userData = res;
                if (this.userData) {
                  this.optionUserId = this.userData.optionUserId;
                  //Getting the application details for determination from ApplicationDeterminationService
                  this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                    if (res) {
                      const data = res as iApplicationDeterminationData;
                      if (data) {
                        this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
                        this.applicationDeterminationData = data;
                        if (this.applicationDeterminationData) {
                          this.pactClientId = this.applicationDeterminationData.pactClientId;
                          //Get Client Data
                          this.clientData.firstName = this.applicationDeterminationData.clientFirstName;
                          this.clientData.lastName = this.applicationDeterminationData.clientLastName;
                          this.clientData.ssn = this.applicationDeterminationData.clientSSN;
                          this.clientData.dob = this.applicationDeterminationData.clientDOB;
                          this.clientData.genderType = this.applicationDeterminationData.clientGenderType;
                          this.clientData.genderTypeDescription = this.applicationDeterminationData.clientGenderDescription;
                          this.clientData.cin = this.applicationDeterminationData.clientCIN;
                          this.clientData.pactClientId = this.applicationDeterminationData.pactClientId;
                          this.clientData.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
                          this.clientData.referralDate = this.applicationDeterminationData.referralDate;
                          this.capsClientId = this.applicationDeterminationData.capsClientId;
                          //Housing Application Supporting Documents Data
                          this.clientDocumentsData.agencyNumber = this.applicationDeterminationData.agencyNumber;
                          this.clientDocumentsData.siteNumber = this.applicationDeterminationData.siteNumber;
                          this.clientDocumentsData.firstName = this.applicationDeterminationData.clientFirstName;
                          this.clientDocumentsData.lastName = this.applicationDeterminationData.clientLastName;
                          this.clientDocumentsData.ssn = this.applicationDeterminationData.clientSSN;
                          this.clientDocumentsData.dob = this.applicationDeterminationData.clientDOB;
                          this.clientDocumentsData.genderType = this.applicationDeterminationData.clientGenderType;
                          this.clientDocumentsData.genderTypeDescription = this.applicationDeterminationData.clientGenderDescription;
                          this.clientDocumentsData.cin = this.applicationDeterminationData.clientCIN;
                          this.clientDocumentsData.pactClientId = this.applicationDeterminationData.pactClientId;
                          this.clientDocumentsData.approvalExpiryDate = this.applicationDeterminationData.approvalTo;
                          this.clientDocumentsData.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
                          //Save Tab Time Duration
                          if (this.applicationDeterminationData.pactApplicationId) {
                            this.saveTabTimeDuration(this.applicationDeterminationData.pactApplicationId, null, "CaseNotes", this.userData.optionUserId);
                          }
                        }
                      }
                    }
                  });
                }
              }
            });
          }
          else {
            return;
          }
        }
      }
    });
  }

  //Set Tab Values For Tab Time Duration
  setTabTimeDuration() {
    let tabOutName: string;
    let tabInName: string;
    //Current Tab Index && TabOutName
    if (this.currentTabIndex === 0) {
      tabOutName = "CaseNotes"
    }
    else if (this.currentTabIndex === 1) {
      tabOutName = "CoordinatedAssessmentSurvey";
    }
    else if (this.currentTabIndex === 2) {
      tabOutName = "PriorApplicationsDeterminations"
    }
    else if (this.currentTabIndex === 3) {
      tabOutName = "ClientDocuments";
    }
    else if (this.currentTabIndex === 4) {
      tabOutName = "VCSReferralsPlacements"
    }
    // else if (this.currentTabIndex === 5) {
    //   tabOutName = "VPSReferrals";
    // }
    //Selected Tab Index && TabInName
    if (this.tabSelectedIndex === 0) {
      tabInName = "CaseNotes";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 1) {
      tabInName = "CoordinatedAssessmentSurvey";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 2) {
      tabInName = "PriorApplicationsDeterminations";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 3) {
      tabInName = "ClientDocuments";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 4) {
      tabInName = "VCSReferralsPlacements";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    // else if (this.tabSelectedIndex === 5) {
    //   tabInName = "VPSReferrals";
    //   this.currentTabIndex = this.tabSelectedIndex;
    // }
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.saveTabTimeDuration(this.applicationDeterminationData.pactApplicationId, tabOutName, tabInName, this.userData.optionUserId);
    }
  }

  //Save Tab Time Duration
  saveTabTimeDuration(pactApplicationId: number, tabOutName: string, tabInName: string, optionUserId: number) {
    this.detTabTimeDuration.pactApplicationId = pactApplicationId;
    this.detTabTimeDuration.tabOutName = tabOutName;
    this.detTabTimeDuration.tabInName = tabInName;
    this.detTabTimeDuration.optionUserId = optionUserId;
    this.commonService.saveTabTimeDuration(this.detTabTimeDuration).subscribe();
  }

  //Client Case Folder Tab Change Event
  clientCaseFolderTabChange() {
    this.setTabTimeDuration();
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.setTabTimeDuration();
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Next Button Click
  nextTab() {
    //if (this.tabSelectedIndex === 5 ) {
    if (this.tabSelectedIndex === 4 ) {
      this.router.navigate(['/ds/outcome', this.applicationDeterminationData.pactApplicationId]);
    }
    else {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  //Previous Button Click
  previousTab() {
    if (this.tabSelectedIndex === 0) {
      this.router.navigate(['/ds/application', this.applicationDeterminationData.pactApplicationId]);
    }
    else {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }
}