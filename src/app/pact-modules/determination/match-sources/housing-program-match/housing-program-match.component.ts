import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';
import { HousingProgramMatchActionComponent } from './housing-program-match-action.component';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { MatchSourcesService } from '../match-sources.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { iMatchedClients, ClientSearch, NewClient, CopyClient, HousingEpisodes, SourceSystemClient } from '../match-sources.model';

@Component({
  selector: 'app-housing-program-match',
  templateUrl: './housing-program-match.component.html',
  styleUrls: ['./housing-program-match.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})

export class HousingProgramMatchComponent implements OnInit, OnDestroy {
  //Global Varables
  housingEpisodesExpanded: boolean;
  isClientMatch: boolean = false;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  housingProgramGroup: FormGroup;
  genders: RefGroupDetails[];

  housingProgramClientColumnDefs = [];
  housingProgramClientRowData: iMatchedClients[];
  housingProgramClientDefaultColDef = {};
  housingProgramClientGridOptions: GridOptions;
  housingProgramClientOverlayNoRowsTemplate: string;
  housingProgramClientFrameworkComponents: any;
  housingProgramClientGridColumnApi: any;
  context: any;

  sourceSystemColumnDefs = [];
  sourceSystemRowData: SourceSystemClient[];
  sourceSystemDefaultColDef = {};
  sourceSystemGridOptions: GridOptions;
  sourceSystemOverlayNoRowsTemplate: string;

  episodesColumnDefs = [];
  episodesRowData: HousingEpisodes[];
  episodesDefaultColDef = {};
  episodesGridOptions: GridOptions;
  episodesOverlayNoRowsTemplate: string;

  clientSearchData: ClientSearch = {
    pactApplicationId: null,
    sourceType: null,
    clientId: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    cin: null,
    referralDate: null,
    optionUserId: null
  };

  newClient: NewClient = {
    pactApplicationId: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    cin: null,
    optionUserId: null,
    pactClientId: null
  }

  copyClient: CopyClient = {
    sourceSystemType: null,
    pactApplicationId: null,
    clientId: null,
    optionUserId: null,
    isSuccess: null
  }

  housingEpisodes: HousingEpisodes = {
    pactApplicationId: null,
    cth: null,
    fromDate: null,
    toDate: null,
    housingType: null,
    housingTypeDescription: null,
    facilityName: null,
    streetAddress: null,
    city: null,
    state: null,
    homelessStayDocumentedType: null,
    homelessStayDocumented: null,
    episodeSourceType: null,
    episodeSource: null,
    sourceDate: null,
    isEpisodeAddedPostTransmission: null
  }

  public sourceSystemClient: SourceSystemClient = {
    pactApplicationId: null,
    pactClientId: null,
    sourceSystemType: null,
    sourceSystemTypeDescription: null,
    sourceClientID: null,
    lastName: null,
    firstName: null,
    ssn: null,
    dob: null,
    age: null,
    genderType: null,
    genderTypeDescription: null,
    cin: null
  }

  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
  };

  //Constructor
  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private message: ToastrService,
    private datePipe: DatePipe,
    private matchSourcesService: MatchSourcesService,
    private dialogService: ConfirmDialogService) {
    //Housing Program Group Form Builder
    this.housingProgramGroup = this.formBuilder.group({
      clientNumberCtrl: [''],
      lastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      firstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      dobCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required],
      ssnCtrl: ['', [Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      cinCtrl: ['']
    });

    //Housing Program Client Grid Column Definitions
    this.housingProgramClientColumnDefs = [
      {
        headerName: 'Client #',
        field: 'clientId',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Last Name', field: 'lastName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactLastNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactLastNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'First Name', field: 'firstName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactFirstNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactFirstNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Social Security #', field: 'ssn', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactSSNMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactSSNMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Date of Birth', field: 'dob', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactDOBMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactDOBMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Age', field: 'age', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactAgeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactAgeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Gender', field: 'genderTypeDescription', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactGenderTypeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactGenderTypeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'CIN', field: 'cin', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactCINMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactCINMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.housingProgramClientDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.context = { componentParent: this };
    this.housingProgramClientFrameworkComponents = {
      actionRenderer: HousingProgramMatchActionComponent
    };

    //Source System Grid Column Definitions
    this.sourceSystemColumnDefs = [
      { headerName: 'Source System', field: 'sourceSystemTypeDescription', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Client #', field: 'sourceClientID', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Last Name', field: 'lastName', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'First Name', field: 'firstName', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Social Security #', field: 'ssn', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Date of Birth', field: 'dob', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Age', field: 'age', filter: 'agTextColumnFilter', width: 120 },
      { headerName: 'Gender', field: 'genderTypeDescription', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'CIN', field: 'cin', filter: 'agTextColumnFilter', width: 120 }
    ];
    this.sourceSystemDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    //Episodes Grid Column Definitions
    this.episodesColumnDefs = [
      {
        headerName: 'From Date',
        field: 'fromDate',
        filter: 'agTextColumnFilter',
        width: 125,
        comparator: this.commonService.dateComparator
      },
      {
        headerName: 'To Date',
        field: 'toDate',
        filter: 'agTextColumnFilter',
        width: 125,
        comparator: this.commonService.dateComparator
      },
      {
        headerName: 'Housing Type',
        field: 'housingTypeDescription',
        cellRenderer: function (params: { value: string; data: { housingTypeDescription: string; isEpisodeAddedPostTransmission: boolean }; }) {
          if (params.data.isEpisodeAddedPostTransmission) {
            return '<span style="color: red; font-size: large; vertical-align: middle">*</span>' + ' ' + params.data.housingTypeDescription;
          }
          else {
            return params.data.housingTypeDescription;
          }
        },
        filter: 'agTextColumnFilter',
        width: 300
      },
      { headerName: 'Name', field: 'facilityName', filter: 'agTextColumnFilter', width: 280 },
      { headerName: 'Street Address', field: 'streetAddress', filter: 'agTextColumnFilter', width: 260 },
      { headerName: 'City', field: 'city', filter: 'agTextColumnFilter', width: 120 },
      { headerName: 'State', field: 'state', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Doc', field: 'homelessStayDocumented', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Source Date', field: 'sourceDate', filter: 'agTextColumnFilter', width: 120 }
    ];
    this.episodesDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  //On Init
  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
          }
          else {
            return;
          }
        }
      }
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
      if (res) {
        const data = res as iApplicationDeterminationData;
        if (data) {
          this.applicationDeterminationData = data;
          if (this.applicationDeterminationData) {
            this.populateHousingProgramMatchForm(this.applicationDeterminationData);
          }
        }
      }
    });

    //Get Genders From ReFGroupDetails
    this.commonService.getRefGroupDetails("4")
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.genders = data;
        },
        error => {
          throw new Error(error.message);
        }
      );
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  //Validate Date of Birth
  validateDOB() {
    if (this.housingProgramGroup.get('dobCtrl').value && !moment(this.housingProgramGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid Date of Birth.");
      }
      this.housingProgramGroup.controls['dobCtrl'].setValue("");
      return;
    }
  }

  //Housing Program Client Grid Ready
  housingProgramClientGridReady(params: { api: { setDomLayout: (arg0: string) => void; }; columnApi: any; }) {
    params.api.setDomLayout('autoHeight');
    this.housingProgramClientGridColumnApi = params.columnApi;
    this.housingProgramClientOverlayNoRowsTemplate = '<span style="color: #337ab7">No Clients To Show</span>';
    if (this.applicationDeterminationData) {
      this.populateHousingProgramClientGrid(this.applicationDeterminationData);
      if (this.applicationDeterminationData.pactClientId && this.applicationDeterminationData.pactClientId > 0) {
        this.housingProgramClientGridColumnApi.setColumnVisible('action', false);
      }
    }
  }

  //Source System Grid Ready
  sourceSystemGridReady(params: { api: { setDomLayout: (arg0: string) => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.sourceSystemOverlayNoRowsTemplate = '<span style="color: #337ab7">No Clients To Show</span>';
    this.populateSourceSystemClientGrid();
  }

  //Popoulate Source System Client Grid
  populateSourceSystemClientGrid() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.sourceSystemClient.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      if (this.applicationDeterminationData.pactClientId) {
        this.sourceSystemClient.pactClientId = this.applicationDeterminationData.pactClientId;
        //Api call to get source system client data
        this.matchSourcesService.getSourceSystemClient(this.sourceSystemClient)
          .subscribe(res => {
            if (res) {
              const data = res as SourceSystemClient[];
              if (data && data.length > 0) {
                this.sourceSystemRowData = data;
              }
              else {
                this.sourceSystemRowData = null;
              }
            }
            else {
              this.sourceSystemRowData = null;
            }
          });
      }
      else {
        this.sourceSystemRowData = null;
      }
    }
  }

  //Episodes Grid Ready
  episodesGridReady(params: { api: { setDomLayout: (arg0: string) => void; sizeColumnsToFit: () => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.episodesOverlayNoRowsTemplate = '<span style="color: #337ab7">No Housing Episodes To Show</span>';
    this.populateHousingEpisodesGrid();
  }

  //Popoulate Housing Episodes Grid
  populateHousingEpisodesGrid() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.housingEpisodes.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.housingEpisodes.episodeSourceType = 367;
      //Api call to get housing episodes
      this.matchSourcesService.getHousingEpisodes(this.housingEpisodes)
        .subscribe(res => {
          if (res) {
            const data = res as HousingEpisodes[];
            if (data && data.length > 0) {
              this.episodesRowData = data;
            }
            else {
              this.episodesRowData = null;
            }
          }
          else {
            this.episodesRowData = null;
          }
        });
    }
  }

  //Populate Housing Program Match Form
  populateHousingProgramMatchForm(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      this.housingProgramGroup.controls['lastNameCtrl'].setValue(applicationDeterminationData.demographicLastName);
      this.housingProgramGroup.controls['firstNameCtrl'].setValue(applicationDeterminationData.demographicFirstName);
      this.housingProgramGroup.controls['dobCtrl'].setValue(applicationDeterminationData.demographicDOB);
      this.housingProgramGroup.controls['genderCtrl'].setValue(applicationDeterminationData.demographicGenderType);
      this.housingProgramGroup.controls['ssnCtrl'].setValue(applicationDeterminationData.demographicSSN);
      this.housingProgramGroup.controls['cinCtrl'].setValue(applicationDeterminationData.demographicCIN);
    }
  }

  //Populate Housing Program Client Grid 
  populateHousingProgramClientGrid(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData && applicationDeterminationData.pactClientId && this.applicationDeterminationData.pactClientId > 0) {
      this.isClientMatch = true;
      this.housingProgramGroup.controls['clientNumberCtrl'].setValue(applicationDeterminationData.pactClientId);
      this.housingProgramGroup.disable();
      this.housingProgramClientRowData = [
        {
          clientId: null,
          lastName: applicationDeterminationData.demographicLastName,
          firstName: applicationDeterminationData.demographicFirstName,
          ssn: applicationDeterminationData.demographicSSN,
          dob: applicationDeterminationData.demographicDOB,
          age: applicationDeterminationData.demographicAge,
          genderTypeDescription: applicationDeterminationData.demographicGenderDescription,
          cin: applicationDeterminationData.demographicCIN,
          isClientRow: false
        },
        {
          clientId: applicationDeterminationData.pactClientId,
          lastName: applicationDeterminationData.clientLastName,
          firstName: applicationDeterminationData.clientFirstName,
          ssn: applicationDeterminationData.clientSSN,
          dob: applicationDeterminationData.clientDOB,
          age: applicationDeterminationData.clientAge,
          genderTypeDescription: applicationDeterminationData.clientGenderDescription,
          cin: applicationDeterminationData.clientCIN,
          isClientRow: true,
          isExactLastNameMatch: applicationDeterminationData.demographicLastName === applicationDeterminationData.clientLastName ? true : false,
          isExactFirstNameMatch: applicationDeterminationData.demographicFirstName === applicationDeterminationData.clientFirstName ? true : false,
          isExactSSNMatch: applicationDeterminationData.demographicSSN === applicationDeterminationData.clientSSN ? true : false,
          isExactDOBMatch: applicationDeterminationData.demographicDOB === applicationDeterminationData.clientDOB ? true : false,
          isExactAgeMatch: applicationDeterminationData.demographicAge === applicationDeterminationData.clientAge ? true : false,
          isExactGenderTypeMatch: applicationDeterminationData.demographicGenderType === applicationDeterminationData.clientGenderType ? true : false,
          isExactCINMatch: applicationDeterminationData.demographicCIN === applicationDeterminationData.clientCIN ? true : false
        },
      ];
    }
    else {
      this.housingProgramClientRowData = null;
    }
  }

  //Search Client
  searchClient() {
    if (this.housingProgramGroup.valid) {
      if (this.applicationDeterminationData) {
        this.clientSearchData.sourceType = 367;
        this.clientSearchData.clientId = this.housingProgramGroup.get('clientNumberCtrl').value;
        this.clientSearchData.firstName = this.housingProgramGroup.get('firstNameCtrl').value.trim();
        this.clientSearchData.lastName = this.housingProgramGroup.get('lastNameCtrl').value.trim();
        this.clientSearchData.ssn = this.housingProgramGroup.get('ssnCtrl').value;
        this.clientSearchData.dob = this.datePipe.transform(this.housingProgramGroup.get('dobCtrl').value, 'MM/dd/yyyy');
        this.clientSearchData.genderType = this.housingProgramGroup.get('genderCtrl').value;
        this.clientSearchData.cin = this.housingProgramGroup.get('cinCtrl').value;
        //Api call to search client
        this.matchSourcesService.searchClient(this.clientSearchData)
          .subscribe(res => {
            if (res) {
              const data = res as iMatchedClients[];
              if (data && data.length > 0) {
                if (data.length >= 2) {
                  for (let i = 1; i < data.length; i++) {
                    data[i].isExactLastNameMatch = data[0].lastName === data[i].lastName ? true : false;
                    data[i].isExactFirstNameMatch = data[0].firstName === data[i].firstName ? true : false;
                    data[i].isExactSSNMatch = data[0].ssn === data[i].ssn ? true : false;
                    data[i].isExactDOBMatch = data[0].dob === data[i].dob ? true : false;
                    data[i].isExactAgeMatch = data[0].age === data[i].age ? true : false;
                    data[i].isExactGenderTypeMatch = data[0].genderType === data[i].genderType ? true : false;
                    data[i].isExactCINMatch = data[0].cin === data[i].cin ? true : false;
                  }
                }
                this.housingProgramClientRowData = data;
              }
              else {
                this.noClientMatchDialog(this.clientSearchData);
              }
            }
            else {
              this.noClientMatchDialog(this.clientSearchData);
            }
          });
      }
    }
    else {
      this.validationMessage();
    }
  }

  //No Client Match Dialog
  noClientMatchDialog(clientSearchData: ClientSearch) {
    this.dialogService.confirmDialog('No Match Found', 'Do you want to create a new client?', '', 'Create New Client', 'Cancel')
      .then(() => {
        if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
          this.newClient.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
          this.newClient.firstName = clientSearchData.firstName.trim();
          this.newClient.lastName = clientSearchData.lastName.trim();
          this.newClient.ssn = clientSearchData.ssn;
          this.newClient.dob = clientSearchData.dob;
          this.newClient.genderType = clientSearchData.genderType;
          this.newClient.cin = clientSearchData.cin == null ? null : clientSearchData.cin;
          this.newClient.optionUserId = this.userData.optionUserId;
          //Api call to create a new client 
          this.matchSourcesService.newClient(this.newClient)
            .subscribe(res => {
              if (res) {
                const data = res as NewClient
                if (data && data.pactClientId && data.pactClientId > 0) {
                  //Refresh the service call with the latest client information
                  this.applicationDeterminationService.setApplicationIdForDetermination(this.applicationDeterminationData.pactApplicationId);
                  //Reload the application determination data
                  this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                    if (res) {
                      this.applicationDeterminationData = res as iApplicationDeterminationData;;
                      this.populateHousingProgramClientGrid(res);
                      this.housingProgramClientGridColumnApi.setColumnVisible('action', false);
                      this.populateSourceSystemClientGrid();
                      this.populateHousingEpisodesGrid();
                      this.matchSourcesService.setHousingProgramMatchTabComplete(true);
                    }
                  });
                }
              }
            });
        }
        else {
          this.message.error("Unable to create new client.");
        }
      }, () => {
      });
  }

  //On Copy Client
  onCopyClient(clientId: number) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.copyClient.sourceSystemType = 367;
      this.copyClient.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.copyClient.clientId = clientId;
      this.copyClient.optionUserId = this.userData.optionUserId;
      this.copyClient.isSuccess = false;
      //Api call to copy the client to the pactapplication 
      this.matchSourcesService.copyClient(this.copyClient)
        .subscribe(res => {
          if (res) {
            const data = res as CopyClient
            if (data && data.isSuccess) {
              //Refresh the service call with the latest client information
              this.applicationDeterminationService.setApplicationIdForDetermination(this.applicationDeterminationData.pactApplicationId);
              //Reload the application determination data
              this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                if (res) {
                  this.applicationDeterminationData = res as iApplicationDeterminationData;;
                  this.populateHousingProgramClientGrid(res);
                  this.housingProgramClientGridColumnApi.setColumnVisible('action', false);
                  this.populateSourceSystemClientGrid();
                  this.populateHousingEpisodesGrid();
                  this.matchSourcesService.setHousingProgramMatchTabComplete(true);
                }
              });
            }
          }
        });
    }
  }

  //Validation Message
  validationMessage() {
    if (this.housingProgramGroup.get('firstNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("First Name is required.");
      }
    }
    else if (this.housingProgramGroup.get('lastNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Last Name is required.");
      }
    }
    else if (this.housingProgramGroup.get('ssnCtrl').errors && this.housingProgramGroup.get('ssnCtrl').errors.required) {
      if (!this.message.currentlyActive) {
        this.message.error("SSN is required.");
      }
    }
    else if (this.housingProgramGroup.get('ssnCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid SSN.");
      }
    }
    else if (this.housingProgramGroup.get('dobCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Date of Birth is required.");
      }
    }
    else if (this.housingProgramGroup.get('genderCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Gender is required.");
      }
    }
    else if (this.housingProgramGroup.get('cinCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("CIN# is formatted as follows: Two alpha characters followed by five numeric characters and then followed by one alpha character; e.g: AA12345A");
      }
    }
  }

  //Reset Housing Program Data
  refreshData() {
    if (this.applicationDeterminationData) {
      this.populateHousingProgramMatchForm(this.applicationDeterminationData);
    }
    this.housingProgramGroup.controls['clientNumberCtrl'].reset();
    this.housingProgramClientRowData = null;
  }
}
