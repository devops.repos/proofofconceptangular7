import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
  selector: "housing-program-match-action",
  template: `
    <div *ngIf="params.data.isClientRow">
    <mat-icon
      class="menu-icon"
      color="warn"
      [matMenuTriggerFor]="housingProgramClientMatchAction">
      more_vert
    </mat-icon>
    <mat-menu #housingProgramClientMatchAction="matMenu">
      <button mat-menu-item (click)="onCopyClient()" class="menu-button">Copy</button>
    </mat-menu>
    </div>
  `,
  styles: [
    `
      .menu-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 30px;
        width: 100%;
        height: 30px;
      }
    `
  ]
})
export class HousingProgramMatchActionComponent implements ICellRendererAngularComp {
  params: any;
  cell: any;

  //Constrctor
  constructor() {
  }

  //Init
  agInit(params: any) {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  //Refresh
  refresh(): boolean {
    return false;
  }

  //On Copy Client
  onCopyClient() {
    this.params.context.componentParent.onCopyClient(this.params.data.clientId);
  }
}
