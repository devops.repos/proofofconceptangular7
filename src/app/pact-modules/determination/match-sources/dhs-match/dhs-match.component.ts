import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';
import { DhsMatchActionComponent } from './dhs-match-action.component';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { MatchSourcesService } from '../match-sources.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { iMatchedClients, ClientSearch, CopyClient, HousingEpisodes, SetCompleteFlag } from '../match-sources.model';



@Component({
  selector: 'app-dhs-match',
  templateUrl: './dhs-match.component.html',
  styleUrls: ['./dhs-match.component.scss'],
  providers: [DatePipe]
})
export class DhsMatchComponent implements OnInit, OnDestroy {
  //Global Varables
  housingEpisodesExpanded: boolean;
  isClientMatch: boolean = false;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  dhsGroup: FormGroup;
  genders: RefGroupDetails[];

  dhsClientColumnDefs = [];
  dhsClientRowData: iMatchedClients[];
  dhsClientDefaultColDef = {};
  dhsClientGridOptions: GridOptions;
  dhsClientOverlayNoRowsTemplate: string;
  dhsClientFrameworkComponents: any;
  dhsClientGridColumnApi: any;
  context: any;

  episodesColumnDefs = [];
  episodesRowData: HousingEpisodes[];
  episodesDefaultColDef = {};
  episodesGridOptions: GridOptions;
  episodesOverlayNoRowsTemplate: string;

  clientSearchData: ClientSearch = {
    pactApplicationId: null,
    sourceType: null,
    clientId: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    cin: null,
    referralDate: null,
    optionUserId: null
  };

  copyClient: CopyClient = {
    sourceSystemType: null,
    pactApplicationId: null,
    clientId: null,
    optionUserId: null,
    isSuccess: null
  }

  setCompleteFlag: SetCompleteFlag = {
    pactApplicationId: null,
    sourceSystemType: null,
    isSuccess: null
  }

  housingEpisodes: HousingEpisodes = {
    pactApplicationId: null,
    cth: null,
    fromDate: null,
    toDate: null,
    housingType: null,
    housingTypeDescription: null,
    facilityName: null,
    streetAddress: null,
    city: null,
    state: null,
    homelessStayDocumentedType: null,
    homelessStayDocumented: null,
    episodeSourceType: null,
    episodeSource: null,
    sourceDate: null,
    isEpisodeAddedPostTransmission: null
  }

  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  //Constructor
  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private message: ToastrService,
    private datePipe: DatePipe,
    private matchSourcesService: MatchSourcesService,
    private dialogService: ConfirmDialogService) {
    //DHS Group Form Builder
    this.dhsGroup = this.formBuilder.group({
      caresIdCtrl: [''],
      lastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      firstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      dobCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required],
      ssnCtrl: ['', [Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      cinCtrl: ['']
    });

    //DHS Client Grid Column Definitions
    this.dhsClientColumnDefs = [
      {
        headerName: 'Cares ID',
        field: 'clientId',
        filter: 'agTextColumnFilter',
        width: 150,
      },
      {
        headerName: 'Last Name', field: 'lastName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactLastNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactLastNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'First Name', field: 'firstName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactFirstNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactFirstNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Social Security #', field: 'ssn', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactSSNMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactSSNMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Date of Birth', field: 'dob', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactDOBMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactDOBMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Age', field: 'age', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactAgeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactAgeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Gender', field: 'genderTypeDescription', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactGenderTypeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactGenderTypeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'CIN', field: 'cin', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.dhsClientDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.context = { componentParent: this };
    this.dhsClientFrameworkComponents = {
      actionRenderer: DhsMatchActionComponent
    };
    
    //Episodes Grid Column Definitions
    this.episodesColumnDefs = [
      { 
        headerName: 'From Date', 
        field: 'fromDate', 
        filter: 'agTextColumnFilter', 
        width: 125, 
        comparator: this.commonService.dateComparator 
      },
      { 
        headerName: 'To Date', 
        field: 'toDate', 
        filter: 'agTextColumnFilter', 
        width: 125, 
        comparator: this.commonService.dateComparator 
      },
      {
        headerName: 'Housing Type',
        field: 'housingTypeDescription',
        cellRenderer: function (params: { value: string; data: { housingTypeDescription: string; isEpisodeAddedPostTransmission: boolean }; }) {
          if (params.data.isEpisodeAddedPostTransmission) {
            return '<span style="color: red; font-size: large; vertical-align: middle">*</span>' + ' ' + params.data.housingTypeDescription;
          }
          else {
            return params.data.housingTypeDescription;
          }
        },
        filter: 'agTextColumnFilter',
        width: 300
      },
      { headerName: 'Name', field: 'facilityName', filter: 'agTextColumnFilter', width: 280 },
      { headerName: 'Street Address', field: 'streetAddress', filter: 'agTextColumnFilter', width: 260 },
      { headerName: 'City', field: 'city', filter: 'agTextColumnFilter', width: 120 },
      { headerName: 'State', field: 'state', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Doc', field: 'homelessStayDocumented', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Source Date', field: 'sourceDate', filter: 'agTextColumnFilter', width: 120 }
    ];
    this.episodesDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  //On Init
  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
          }
          else {
            return;
          }
        }
      }
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
      if (res) {
        const data = res as iApplicationDeterminationData;
        if (data) {
          this.applicationDeterminationData = data;
          this.populateDhsMatchForm(this.applicationDeterminationData);
        }
      }
    });

    //Get Genders From ReFGroupDetails
    this.commonService.getRefGroupDetails("4")
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.genders = data;
        },
        error => {
          throw new Error(error.message);
        }
      );
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  //Validate Date of Birth
  validateDOB() {
    if (this.dhsGroup.get('dobCtrl').value && !moment(this.dhsGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid Date of Birth.");
      }
      this.dhsGroup.controls['dobCtrl'].setValue("");
      return;
    }
  }

  //DHS Client Grid Ready
  dhsClientGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.dhsClientGridColumnApi = params.columnApi;
    this.dhsClientOverlayNoRowsTemplate = '<span style="color: #337ab7">No Clients To Show</span>';
    if (this.applicationDeterminationData) {
      this.populateDhsClientGrid(this.applicationDeterminationData);
      if (this.applicationDeterminationData.dhsClientID && this.applicationDeterminationData.dhsClientID > 0) {
        this.dhsClientGridColumnApi.setColumnVisible('action', false);
      }
    }
  }

  //Episodes Grid Ready
  episodesGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.episodesOverlayNoRowsTemplate = '<span style="color: #337ab7">No Episodes To Show</span>';
    this.populateHousingEpisodesGrid();
  }

  //Popoulate Housing Episodes Grid
  populateHousingEpisodesGrid() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.housingEpisodes.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.housingEpisodes.episodeSourceType = 368;
      //Api call to get housing episodes
      this.matchSourcesService.getHousingEpisodes(this.housingEpisodes)
        .subscribe(res => {
          if (res) {
            const data = res as HousingEpisodes[];
            if (data && data.length > 0) {
              this.episodesRowData = data;
            }
            else {
              this.episodesRowData = null;
            }
          }
          else {
            this.episodesRowData = null;
          }
        });
    }
  }

  //Populate DHS Form
  populateDhsMatchForm(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      this.dhsGroup.controls['lastNameCtrl'].setValue(applicationDeterminationData.demographicLastName);
      this.dhsGroup.controls['firstNameCtrl'].setValue(applicationDeterminationData.demographicFirstName);
      this.dhsGroup.controls['dobCtrl'].setValue(applicationDeterminationData.demographicDOB);
      this.dhsGroup.controls['genderCtrl'].setValue(applicationDeterminationData.demographicGenderType);
      this.dhsGroup.controls['ssnCtrl'].setValue(applicationDeterminationData.demographicSSN);
      this.dhsGroup.controls['cinCtrl'].setValue(applicationDeterminationData.demographicCIN);
    }
  }

  //Populate Dhs Client Grid 
  populateDhsClientGrid(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData && applicationDeterminationData.dhsClientID && applicationDeterminationData.dhsClientID > 0) {
      this.isClientMatch = true;
      this.dhsGroup.controls['caresIdCtrl'].setValue(applicationDeterminationData.dhsClientID);
      this.dhsGroup.disable();
      this.dhsClientRowData = [
        {
          clientId: null,
          lastName: applicationDeterminationData.demographicLastName,
          firstName: applicationDeterminationData.demographicFirstName,
          ssn: applicationDeterminationData.demographicSSN,
          dob: applicationDeterminationData.demographicDOB,
          age: applicationDeterminationData.demographicAge,
          genderTypeDescription: applicationDeterminationData.demographicGenderDescription,
          cin: applicationDeterminationData.demographicCIN,
          isClientRow: false
        },
        {
          clientId: applicationDeterminationData.dhsClientID,
          lastName: applicationDeterminationData.dhsLastName,
          firstName: applicationDeterminationData.dhsFirstName,
          ssn: applicationDeterminationData.dhsssn,
          dob: applicationDeterminationData.dhsdob,
          age: applicationDeterminationData.dhsAge,
          genderTypeDescription: applicationDeterminationData.dhsGenderDescription,
          cin: null,
          isClientRow: true,
          isExactLastNameMatch: applicationDeterminationData.demographicLastName === applicationDeterminationData.dhsLastName ? true : false,
          isExactFirstNameMatch: applicationDeterminationData.demographicFirstName === applicationDeterminationData.dhsFirstName ? true : false,
          isExactSSNMatch: applicationDeterminationData.demographicSSN === applicationDeterminationData.dhsssn ? true : false,
          isExactDOBMatch: applicationDeterminationData.demographicDOB === applicationDeterminationData.dhsdob ? true : false,
          isExactAgeMatch: applicationDeterminationData.demographicAge === applicationDeterminationData.dhsAge ? true : false,
          isExactGenderTypeMatch: applicationDeterminationData.demographicGenderType === applicationDeterminationData.dhsGenderType ? true : false
        },
      ];
    }
    else {
      this.dhsClientRowData = null;
    }
  }

  //Search Client
  searchClient() {
    if (this.dhsGroup.valid) {
      if (this.applicationDeterminationData) {
        this.clientSearchData.sourceType = 368;
        this.clientSearchData.clientId = this.dhsGroup.get('caresIdCtrl').value;
        this.clientSearchData.firstName = this.dhsGroup.get('firstNameCtrl').value.trim();
        this.clientSearchData.lastName = this.dhsGroup.get('lastNameCtrl').value.trim();
        this.clientSearchData.ssn = this.dhsGroup.get('ssnCtrl').value;
        this.clientSearchData.dob = this.datePipe.transform(this.dhsGroup.get('dobCtrl').value, 'MM/dd/yyyy');
        this.clientSearchData.genderType = this.dhsGroup.get('genderCtrl').value;
        this.clientSearchData.cin = this.dhsGroup.get('cinCtrl').value;
        //Api call to search client
        this.matchSourcesService.searchClient(this.clientSearchData)
          .subscribe(res => {
            if (res) {
              const data = res as iMatchedClients[];
              if (data && data.length > 0) {
                if (data.length >= 2) {
                  for (let i = 1; i < data.length; i++) {
                    data[i].isExactLastNameMatch = data[0].lastName === data[i].lastName ? true : false;
                    data[i].isExactFirstNameMatch = data[0].firstName === data[i].firstName ? true : false;
                    data[i].isExactSSNMatch = data[0].ssn === data[i].ssn ? true : false;
                    data[i].isExactDOBMatch = data[0].dob === data[i].dob ? true : false;
                    data[i].isExactAgeMatch = data[0].age === data[i].age ? true : false;
                    data[i].isExactGenderTypeMatch = data[0].genderType === data[i].genderType ? true : false;
                  }
                }
                this.dhsClientRowData = data;
              }
              else {
                this.noClientMatchDialog();
              }
            }
            else {
              this.noClientMatchDialog();
            }
          });
      }
    }
    else {
      this.validationMessage();
    }
  }

  //No Client Match Dialog
  noClientMatchDialog() {
    this.dialogService.confirmDialog('No Match Found', 'Do you want to continue without match?', '', 'Continue Without Match', 'Cancel')
      .then(() => {
        if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
          this.setCompleteFlag.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
          this.setCompleteFlag.sourceSystemType = 368;
          //Api call to create a new client 
          this.matchSourcesService.updateTabCompleteForMatchSummary(this.setCompleteFlag)
            .subscribe(res => {
              if (res) {
                const data = res as SetCompleteFlag
                if (data && data.isSuccess) {
                  this.matchSourcesService.setDhsMatchTabComplete(data.isSuccess);
                }
              }
            });
        }
        else {
          this.message.error("Unable to set complete flag.");
        }
      }, () => {
      });
  }

  //On Copy Client
  onCopyClient(clientId: number) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.copyClient.sourceSystemType = 368;
      this.copyClient.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.copyClient.clientId = clientId;
      this.copyClient.optionUserId = this.userData.optionUserId;
      this.copyClient.isSuccess = false;
      //Api call to copy the client to the match summary table 
      this.matchSourcesService.copyClient(this.copyClient)
        .subscribe(res => {
          if (res) {
            const data = res as CopyClient
            if (data && data.isSuccess) {
              //Refresh the service call with the latest client information
              this.applicationDeterminationService.setApplicationIdForDetermination(this.applicationDeterminationData.pactApplicationId);
              //Reload the application determination data
              this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                if (res) {
                  this.applicationDeterminationData = res as iApplicationDeterminationData;;
                  this.populateDhsClientGrid(res);
                  this.dhsClientGridColumnApi.setColumnVisible('action', false);
                  this.populateHousingEpisodesGrid();
                  this.matchSourcesService.setDhsMatchTabComplete(true);
                }
              });
            }
          }
        });
    }
  }

  //Validation Message
  validationMessage() {
    if (this.dhsGroup.get('firstNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("First Name is required.");
      }
    }
    else if (this.dhsGroup.get('lastNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Last Name is required.");
      }
    }
    else if (this.dhsGroup.get('ssnCtrl').errors && this.dhsGroup.get('ssnCtrl').errors.required) {
      if (!this.message.currentlyActive) {
        this.message.error("SSN is required.");
      }
    }
    else if (this.dhsGroup.get('ssnCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid SSN.");
      }
    }
    else if (this.dhsGroup.get('dobCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Date of Birth is required.");
      }
    }
    else if (this.dhsGroup.get('genderCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Gender is required.");
      }
    }
    else if (this.dhsGroup.get('cinCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("CIN# is formatted as follows: Two alpha characters followed by five numeric characters and then followed by one alpha character; e.g: AA12345A");
      }
    }
  }

  //Refresh Data
  refreshData() {
    if (this.applicationDeterminationData) {
      this.populateDhsMatchForm(this.applicationDeterminationData);
    }
    this.dhsGroup.controls['caresIdCtrl'].reset();
    this.dhsClientRowData = null;
  }
}



