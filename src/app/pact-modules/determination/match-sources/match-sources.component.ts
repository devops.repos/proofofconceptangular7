import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import { appStatusColor } from 'src/app/models/pact-enums.enum';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { MatchSourcesService } from './match-sources.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { iMatchSummaryDetails, iMatchSummaryDetailsInput } from './match-sources.model'
import { DETTabTimeDuration } from 'src/app/models/determination-common.model';

@Component({
  selector: 'app-match-sources',
  templateUrl: './match-sources.component.html',
  styleUrls: ['./match-sources.component.scss']
})

export class MatchSourcesComponent implements OnInit, OnDestroy {
  //Global Varaibles
  tabSelectedIndex: number = 0;
  currentTabIndex: number = 0;

  detTabTimeDuration: DETTabTimeDuration = {
    pactApplicationId: null,
    tabInName: null,
    tabOutName: null,
    optionUserId: null
  };

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  matchSummaryColumnDefs = [];
  matchSummaryRowData: iMatchSummaryDetails[];
  matchSummaryDefaultColDef = {};
  matchSummaryOverlayNoRowsTemplate: string;

  isMatchSummaryTabComplete: boolean = false;
  isHousingProgramMatchTabComplete: boolean = false;
  isDHSMatchTabComplete: boolean = false;
  isHASAMatchTabComplete: boolean = false;
  isMedicaidMatchTabComplete: boolean = false;
  isSTARSMatchTabComplete: boolean = false;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;

  matchSummaryDetailsSub: Subscription;
  matchSummaryDetails: iMatchSummaryDetails[];

  matchSummaryDetailsInput: iMatchSummaryDetailsInput = {
    pactApplicationId: null,
    optionUserId: null
  }

  //Constructor
  constructor(private route: ActivatedRoute,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private matchSourcesService: MatchSourcesService,
    private router: Router,
    private commonService: CommonService,
    private determinationSideNavService: DeterminationSideNavService) {
    //Match Summary Grid Column Definitions
    this.matchSummaryColumnDefs = [
      { headerName: 'Source System', field: 'sourceSystemTypeDescription', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Matched', field: 'matchedTypeDescription', filter: 'agTextColumnFilter', width: 145 },
      { headerName: 'Source Client #', field: 'sourceClientID', filter: 'agTextColumnFilter', width: 135 },
      { headerName: 'First Name', field: 'firstName', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Last Name', field: 'lastName', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Social Security #', field: 'ssn', filter: 'agTextColumnFilter', width: 135 },
      { headerName: 'Date of Birth', field: 'dob', filter: 'agTextColumnFilter', width: 120 },
      { headerName: 'Gender', field: 'genderTypeDescription', filter: 'agTextColumnFilter', width: 120 },
      { headerName: 'CIN', field: 'cin', filter: 'agTextColumnFilter', width: 120 }
    ];
    this.matchSummaryDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  //On Init
  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
          }
          else {
            return;
          }
        }
      }
    });

    //Get Housing Program Match Tab Compelete
    this.matchSourcesService.getHousingProgramMatchTabComplete().subscribe(res => {
      this.isHousingProgramMatchTabComplete = res;
      this.setMatchSummayTabStatus();
    });
    //Get Dhs Match Tab Compelete
    this.matchSourcesService.getDhsMatchTabComplete().subscribe(res => {
      this.isDHSMatchTabComplete = res;
      this.setMatchSummayTabStatus();
    });
    //Get Hasa Match Tab Compelete
    this.matchSourcesService.getHasaMatchTabComplete().subscribe(res => {
      this.isHASAMatchTabComplete = res;
      this.setMatchSummayTabStatus();
    });
    //Get Medicaid Match Tab Compelete
    this.matchSourcesService.getMedicaidMatchTabComplete().subscribe(res => {
      this.isMedicaidMatchTabComplete = res;
      this.setMatchSummayTabStatus();
    });
    //Get Stars Match Tab Compelete
    this.matchSourcesService.getStarsMatchTabComplete().subscribe(res => {
      this.isSTARSMatchTabComplete = res;
      this.setMatchSummayTabStatus();
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
      if (res) {
        const data = res as iApplicationDeterminationData;
        if (data) {
          this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
          this.applicationDeterminationData = data;
          if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
            this.matchSummaryDetailsInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
            this.matchSummaryDetailsInput.optionUserId = this.userData.optionUserId;
            this.matchSummaryDetailsSub = this.matchSourcesService.getMatchSummaryDetails(this.matchSummaryDetailsInput).subscribe(res => {
              if (res) {
                const data = res as iMatchSummaryDetails[];
                if (data && data.length > 0) {
                  this.matchSummaryDetails = data;
                  this.matchSummaryRowData = this.matchSummaryDetails;
                  this.setTabComplete(data);
                }
                else {
                  this.matchSummaryRowData = null;
                }
              }
            });
          }
        }
      }
    });
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.saveTabTimeDuration();
      this.applicationDeterminationDataSub.unsubscribe();
    }
    if (this.matchSummaryDetailsSub) {
      this.matchSummaryDetailsSub.unsubscribe();
    }
  }

  //Set Tab Complete
  setTabComplete(matchSummaryDetails: iMatchSummaryDetails[]) {
    let housingProgramMatchTab = matchSummaryDetails.find(({ sourceSystemType }) => sourceSystemType === 367);
    if (housingProgramMatchTab && housingProgramMatchTab.isHousingProgramMatchTabComplete) {
      this.isHousingProgramMatchTabComplete = true;
    }
    else {
      this.isHousingProgramMatchTabComplete = false;
    }
    let dhsMatchTab = matchSummaryDetails.find(({ sourceSystemType }) => sourceSystemType === 368);
    if (dhsMatchTab && dhsMatchTab.isDHSMatchTabComplete) {
      this.isDHSMatchTabComplete = true;
    }
    else {
      this.isDHSMatchTabComplete = false;
    }
    let hasaMatchTab = matchSummaryDetails.find(({ sourceSystemType }) => sourceSystemType === 366);
    if (hasaMatchTab && hasaMatchTab.isHASAMatchTabComplete) {
      this.isHASAMatchTabComplete = true;
    }
    else {
      this.isHASAMatchTabComplete = false;
    }
    let medicaidMatchTab = matchSummaryDetails.find(({ sourceSystemType }) => sourceSystemType === 577);
    if (medicaidMatchTab && medicaidMatchTab.isMedicaidMatchTabComplete) {
      this.isMedicaidMatchTabComplete = true;
    }
    else {
      this.isMedicaidMatchTabComplete = false;
    }
    let starsMatchTab = matchSummaryDetails.find(({ sourceSystemType }) => sourceSystemType === 576);
    if (starsMatchTab && starsMatchTab.isSTARSMatchTabComplete) {
      this.isSTARSMatchTabComplete = true;
    }
    else {
      this.isSTARSMatchTabComplete = false;
    }
    this.setMatchSummayTabStatus();
  }

  //Set Match Summay Tab Status
  setMatchSummayTabStatus() {
    if (this.isHousingProgramMatchTabComplete
      && this.isDHSMatchTabComplete
      && this.isHASAMatchTabComplete
      && this.isMedicaidMatchTabComplete
      && this.isSTARSMatchTabComplete) {
      this.isMatchSummaryTabComplete = true;
    }
    else {
      this.isMatchSummaryTabComplete = false;
    }
  }

  //Save Tab Time Duration
  saveTabTimeDuration() {
    let tabOutName: string;
    let tabInName: string;
    //Current Tab Index && TabOutName
    if (this.currentTabIndex === 0) {
      tabOutName = "MatchSummary"
    }
    else if (this.currentTabIndex === 1) {
      tabOutName = "HousingProgramMatch";
    }
    else if (this.currentTabIndex === 2) {
      tabOutName = "DHSMatch";
    }
    else if (this.currentTabIndex === 3) {
      tabOutName = "HASAMatch";
    }
    else if (this.currentTabIndex === 4) {
      tabOutName = "MedicaidMatch";
    }
    else if (this.currentTabIndex === 5) {
      tabOutName = "StarsMatch";
    }
    //Selected Tab Index && TabInName
    if (this.tabSelectedIndex === 0) {
      tabInName = "MatchSummary";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 1) {
      tabInName = "HousingProgramMatch";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 2) {
      tabInName = "DHSMatch";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 3) {
      tabInName = "HASAMatch";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 4) {
      tabInName = "MedicaidMatch";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 5) {
      tabInName = "StarsMatch";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.detTabTimeDuration.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.detTabTimeDuration.tabOutName = tabOutName;
      this.detTabTimeDuration.tabInName = tabInName;
      this.detTabTimeDuration.optionUserId = this.userData.optionUserId;
      this.commonService.saveTabTimeDuration(this.detTabTimeDuration).subscribe();
    }
  }

  //Match Sources Tab Change Event
  matchSourcesTabChange() {
    this.saveTabTimeDuration();
    if (this.tabSelectedIndex === 0) {
      if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
        this.matchSummaryDetailsInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
        this.matchSummaryDetailsInput.optionUserId = this.userData.optionUserId;
        this.matchSummaryDetailsSub = this.matchSourcesService.getMatchSummaryDetails(this.matchSummaryDetailsInput).subscribe(res => {
          if (res) {
            const data = res as iMatchSummaryDetails[];
            if (data && data.length > 0) {
              this.matchSummaryDetails = data;
              this.matchSummaryRowData = this.matchSummaryDetails;
              this.setTabComplete(data);
            }
          }
        });
      }
    }
  }

  //Match Summary Grid Ready
  matchSummaryGridReady(params: any) {
    if(params) {
      params.api.setDomLayout('autoHeight');
    }
    this.matchSummaryOverlayNoRowsTemplate = '<span style="color: #337ab7">No Matches To Show</span>';
    if (this.matchSummaryDetails && this.matchSummaryDetails.length > 0) {
      this.matchSummaryRowData = this.matchSummaryDetails;
    }
    else {
      this.matchSummaryRowData = null;
    }
  }

  //Next Button Click
  nextTab() {
    if (this.tabSelectedIndex === 5) {
      this.router.navigate(['/ds/application', this.applicationDeterminationData.pactApplicationId]);
    }
    else {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  //Previous Button Click
  previousTab() {
    if (this.tabSelectedIndex != 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }
}
