import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';
import { HasaMatchActionComponent } from './hasa-match-action.component';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { MatchSourcesService } from '../match-sources.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { iHasaMatchedClients, ClientSearch, CopyClient, HousingEpisodes, SetCompleteFlag } from '../match-sources.model';

@Component({
  selector: 'app-hasa-match',
  templateUrl: './hasa-match.component.html',
  styleUrls: ['./hasa-match.component.scss'],
  providers: [DatePipe]
})

export class HasaMatchComponent implements OnInit, OnDestroy {
  //Global Varables
  housingEpisodesExpanded: boolean;
  isClientMatch: boolean = false;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  hasaGroup: FormGroup;
  genders: RefGroupDetails[];

  hasaClientColumnDefs = [];
  hasaClientRowData: iHasaMatchedClients[];
  hasaClientDefaultColDef = {};
  hasaClientGridOptions: GridOptions;
  hasaClientOverlayNoRowsTemplate: string;
  hasaClientFrameworkComponents: any;
  hasaClientGridColumnApi: any;
  context: any;

  episodesColumnDefs = [];
  episodesRowData: HousingEpisodes[];
  episodesDefaultColDef = {};
  episodesGridOptions: GridOptions;
  episodesOverlayNoRowsTemplate: string;

  clientSearchData: ClientSearch = {
    pactApplicationId: null,
    sourceType: null,
    clientId: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    cin: null,
    referralDate: null,
    optionUserId: null
  };

  copyClient: CopyClient = {
    sourceSystemType: null,
    pactApplicationId: null,
    clientId: null,
    optionUserId: null,
    isSuccess: null
  }

  setCompleteFlag: SetCompleteFlag = {
    pactApplicationId: null,
    sourceSystemType: null,
    isSuccess: null
  }

  housingEpisodes: HousingEpisodes = {
    pactApplicationId: null,
    cth: null,
    fromDate: null,
    toDate: null,
    housingType: null,
    housingTypeDescription: null,
    facilityName: null,
    streetAddress: null,
    city: null,
    state: null,
    homelessStayDocumentedType: null,
    homelessStayDocumented: null,
    episodeSourceType: null,
    episodeSource: null,
    sourceDate: null,
    isEpisodeAddedPostTransmission: null
  }

  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  //Constructor
  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private message: ToastrService,
    private datePipe: DatePipe,
    private matchSourcesService: MatchSourcesService,
    private dialogService: ConfirmDialogService) {
    //hasa Group Form Builder
    this.hasaGroup = this.formBuilder.group({
      hasaClientIdCtrl: [''],
      lastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      firstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      dobCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required],
      ssnCtrl: ['', [Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      cinCtrl: ['']
    });

    //DHS Client Grid Column Definitions
    this.hasaClientColumnDefs = [
      {
        headerName: 'HASA Client #',
        field: 'clientId',
        filter: 'agTextColumnFilter',
        width: 150
      },
      {
        headerName: 'Last Name', field: 'lastName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactLastNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactLastNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'First Name', field: 'firstName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactFirstNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactFirstNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Social Security #', field: 'ssn', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactSSNMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactSSNMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Date of Birth', field: 'dob', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactDOBMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactDOBMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Age', field: 'age', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactAgeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactAgeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Gender', field: 'genderTypeDescription', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactGenderTypeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          if (params.data.isExactGenderTypeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'CIN', field: 'cin', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
        }
      },
      {
        headerName: 'Active HASA Case', field: 'isActiveHasaCase', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
        }
      },
      {
        headerName: 'Currently In Shelter', field: 'isCurrentlyInShelter', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.hasaClientDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.context = { componentParent: this };
    this.hasaClientFrameworkComponents = {
      actionRenderer: HasaMatchActionComponent
    };

    //Episodes Grid Column Definitions
    this.episodesColumnDefs = [
      { 
        headerName: 'From Date', 
        field: 'fromDate', 
        filter: 'agTextColumnFilter', 
        width: 125,
        comparator: this.commonService.dateComparator 
      },
      { 
        headerName: 'To Date', 
        field: 'toDate', 
        filter: 'agTextColumnFilter', 
        width: 125,
        comparator: this.commonService.dateComparator 
      },
      {
        headerName: 'Housing Type',
        field: 'housingTypeDescription',
        cellRenderer: function (params: { value: string; data: { housingTypeDescription: string; isEpisodeAddedPostTransmission: boolean }; }) {
          if (params.data.isEpisodeAddedPostTransmission) {
            return '<span style="color: red; font-size: large; vertical-align: middle">*</span>' + ' ' + params.data.housingTypeDescription;
          }
          else {
            return params.data.housingTypeDescription;
          }
        },
        filter: 'agTextColumnFilter',
        width: 300
      },
      { headerName: 'Name', field: 'facilityName', filter: 'agTextColumnFilter', width: 280 },
      { headerName: 'Street Address', field: 'streetAddress', filter: 'agTextColumnFilter', width: 260 },
      { headerName: 'City', field: 'city', filter: 'agTextColumnFilter', width: 120 },
      { headerName: 'State', field: 'state', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Doc', field: 'homelessStayDocumented', filter: 'agTextColumnFilter', width: 100 },
      { headerName: 'Source Date', field: 'sourceDate', filter: 'agTextColumnFilter', width: 120 }
    ];
    this.episodesDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  //On Init
  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
          }
          else {
            return;
          }
        }
      }
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
      if (res) {
        const data = res as iApplicationDeterminationData;
        if (data) {
          this.applicationDeterminationData = data;
          this.populateHasaMatchForm(this.applicationDeterminationData);
        }
      }
    });

    //Get Genders From ReFGroupDetails
    this.commonService.getRefGroupDetails("4")
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.genders = data;
        },
        error => {
          throw new Error(error.message);
        }
      );
  }
  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  //Validate Date of Birth
  validateDOB() {
    if (this.hasaGroup.get('dobCtrl').value && !moment(this.hasaGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid Date of Birth.");
      }
      this.hasaGroup.controls['dobCtrl'].setValue("");
      return;
    }
  }

  //hasa Client Grid Ready
  hasaClientGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.hasaClientGridColumnApi = params.columnApi;
    this.hasaClientOverlayNoRowsTemplate = '<span style="color: #337ab7">No Clients To Show</span>';
    if (this.applicationDeterminationData) {
      this.populateHasaClientGrid(this.applicationDeterminationData);
      if (this.applicationDeterminationData.hasaClientID && this.applicationDeterminationData.hasaClientID > 0) {
        this.hasaClientGridColumnApi.setColumnVisible('action', false);
      }
    }
  }

  //Episodes Grid Ready
  episodesGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.episodesOverlayNoRowsTemplate = '<span style="color: #337ab7">No Episodes To Show</span>';
    this.populateHousingEpisodesGrid();
  }

  //Popoulate Housing Episodes Grid
  populateHousingEpisodesGrid() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.housingEpisodes.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.housingEpisodes.episodeSourceType = 366;
      //Api call to get housing episodes
      this.matchSourcesService.getHousingEpisodes(this.housingEpisodes)
        .subscribe(res => {
          if (res) {
            const data = res as HousingEpisodes[];
            if (data && data.length > 0) {
              this.episodesRowData = data;
            }
            else {
              this.episodesRowData = null;
            }
          }
          else {
            this.episodesRowData = null;
          }
        });
    }
  }

  //Populate Hasa Form
  populateHasaMatchForm(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      this.hasaGroup.controls['lastNameCtrl'].setValue(applicationDeterminationData.demographicLastName);
      this.hasaGroup.controls['firstNameCtrl'].setValue(applicationDeterminationData.demographicFirstName);
      this.hasaGroup.controls['dobCtrl'].setValue(applicationDeterminationData.demographicDOB);
      this.hasaGroup.controls['genderCtrl'].setValue(applicationDeterminationData.demographicGenderType);
      this.hasaGroup.controls['ssnCtrl'].setValue(applicationDeterminationData.demographicSSN);
      this.hasaGroup.controls['cinCtrl'].setValue(applicationDeterminationData.demographicCIN);
    }
  }

  //Populate Dhs Client Grid 
  populateHasaClientGrid(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData && applicationDeterminationData.hasaClientID && applicationDeterminationData.hasaClientID > 0) {
      this.isClientMatch = true;
      this.hasaGroup.controls['hasaClientIdCtrl'].setValue(applicationDeterminationData.hasaClientID);
      this.hasaGroup.disable();
      this.hasaClientRowData = [
        {
          clientId: null,
          lastName: applicationDeterminationData.demographicLastName,
          firstName: applicationDeterminationData.demographicFirstName,
          ssn: applicationDeterminationData.demographicSSN,
          dob: applicationDeterminationData.demographicDOB,
          age: applicationDeterminationData.demographicAge,
          genderTypeDescription: applicationDeterminationData.demographicGenderDescription,
          cin: applicationDeterminationData.demographicCIN,
          isActiveHasaCase: null,
          isCurrentlyInShelter: null,
          isClientRow: false
        },
        {
          clientId: applicationDeterminationData.hasaClientID,
          lastName: applicationDeterminationData.hasaLastName,
          firstName: applicationDeterminationData.hasaFirstName,
          ssn: applicationDeterminationData.hasassn,
          dob: applicationDeterminationData.hasadob,
          age: applicationDeterminationData.hasaAge,
          genderTypeDescription: applicationDeterminationData.hasaGenderDescription,
          isActiveHasaCase: applicationDeterminationData.isActiveHasaCase,
          isCurrentlyInShelter: applicationDeterminationData.isCurrentlyInShelter,
          cin: null,
          isClientRow: true,
          isExactLastNameMatch: applicationDeterminationData.demographicLastName === applicationDeterminationData.hasaLastName ? true : false,
          isExactFirstNameMatch: applicationDeterminationData.demographicFirstName === applicationDeterminationData.hasaFirstName ? true : false,
          isExactSSNMatch: applicationDeterminationData.demographicSSN === applicationDeterminationData.hasassn ? true : false,
          isExactDOBMatch: applicationDeterminationData.demographicDOB === applicationDeterminationData.hasadob ? true : false,
          isExactAgeMatch: applicationDeterminationData.demographicAge === applicationDeterminationData.hasaAge ? true : false,
          isExactGenderTypeMatch: applicationDeterminationData.demographicGenderType === applicationDeterminationData.hasaGenderType ? true : false
        },
      ];
    }
    else {
      this.hasaClientRowData = null;
    }
  }

  //Search Client
  searchClient() {
    if (this.hasaGroup.valid) {
      if (this.applicationDeterminationData) {
        this.clientSearchData.sourceType = 366;
        this.clientSearchData.clientId = this.hasaGroup.get('hasaClientIdCtrl').value;
        this.clientSearchData.firstName = this.hasaGroup.get('firstNameCtrl').value.trim();
        this.clientSearchData.lastName = this.hasaGroup.get('lastNameCtrl').value.trim();
        this.clientSearchData.ssn = this.hasaGroup.get('ssnCtrl').value;
        this.clientSearchData.dob = this.datePipe.transform(this.hasaGroup.get('dobCtrl').value, 'MM/dd/yyyy');
        this.clientSearchData.genderType = this.hasaGroup.get('genderCtrl').value;
        this.clientSearchData.cin = this.hasaGroup.get('cinCtrl').value;
        //Api call to search client
        this.matchSourcesService.searchClient(this.clientSearchData)
          .subscribe(res => {
            if (res) {
              const data = res as iHasaMatchedClients[];
              if (data && data.length > 0) {
                if (data.length >= 2) {
                  for (let i = 1; i < data.length; i++) {
                    data[i].isExactLastNameMatch = data[0].lastName === data[i].lastName ? true : false;
                    data[i].isExactFirstNameMatch = data[0].firstName === data[i].firstName ? true : false;
                    data[i].isExactSSNMatch = data[0].ssn === data[i].ssn ? true : false;
                    data[i].isExactDOBMatch = data[0].dob === data[i].dob ? true : false;
                    data[i].isExactAgeMatch = data[0].age === data[i].age ? true : false;
                    data[i].isExactGenderTypeMatch = data[0].genderType === data[i].genderType ? true : false;
                  }
                }
                this.hasaClientRowData = data;
              }
              else {
                this.noClientMatchDialog();
              }
            }
            else {
              this.noClientMatchDialog();
            }
          });
      }
    }
    else {
      this.validationMessage();
    }
  }

  //No Client Match Dialog
  noClientMatchDialog() {
    this.dialogService.confirmDialog('No Match Found', 'Do you want to continue without match?', '', 'Continue Without Match', 'Cancel')
      .then(() => {
        if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
          this.setCompleteFlag.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
          this.setCompleteFlag.sourceSystemType = 366;
          //Api call to create a new client 
          this.matchSourcesService.updateTabCompleteForMatchSummary(this.setCompleteFlag)
            .subscribe(res => {
              if (res) {
                const data = res as SetCompleteFlag
                if (data && data.isSuccess) {
                  this.matchSourcesService.setHasaTabComplete(data.isSuccess);
                }
              }
            });
        }
        else {
          this.message.error("Unable to set complete flag.");
        }
      }, () => {
      });
  }

  //On Copy Client
  onCopyClient(clientId: number) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.copyClient.sourceSystemType = 366;
      this.copyClient.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.copyClient.clientId = clientId;
      this.copyClient.optionUserId = this.userData.optionUserId;
      this.copyClient.isSuccess = false;
      //Api call to copy the client to the match summary table 
      this.matchSourcesService.copyClient(this.copyClient)
        .subscribe(res => {
          if (res) {
            const data = res as CopyClient
            if (data && data.isSuccess) {
              //Refresh the service call with the latest client information
              this.applicationDeterminationService.setApplicationIdForDetermination(this.applicationDeterminationData.pactApplicationId);
              //Reload the application determination data
              this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                if (res) {
                  this.applicationDeterminationData = res as iApplicationDeterminationData;;
                  this.populateHasaClientGrid(res);
                  this.hasaClientGridColumnApi.setColumnVisible('action', false);
                  this.populateHousingEpisodesGrid();
                  this.matchSourcesService.setHasaTabComplete(true);
                }
              });
            }
          }
        });
    }
  }

  //Validation Message
  validationMessage() {
    if (this.hasaGroup.get('firstNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("First Name is required.");
      }
    }
    else if (this.hasaGroup.get('lastNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Last Name is required.");
      }
    }
    else if (this.hasaGroup.get('ssnCtrl').errors && this.hasaGroup.get('ssnCtrl').errors.required) {
      if (!this.message.currentlyActive) {
        this.message.error("SSN is required.");
      }
    }
    else if (this.hasaGroup.get('ssnCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid SSN.");
      }
    }
    else if (this.hasaGroup.get('dobCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Date of Birth is required.");
      }
    }
    else if (this.hasaGroup.get('genderCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Gender is required.");
      }
    }
    else if (this.hasaGroup.get('cinCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("CIN# is formatted as follows: Two alpha characters followed by five numeric characters and then followed by one alpha character; e.g: AA12345A");
      }
    }
  }

  //Refresh Data
  refreshData() {
    if (this.applicationDeterminationData) {
      this.populateHasaMatchForm(this.applicationDeterminationData);
    }
    this.hasaGroup.controls['hasaClientIdCtrl'].reset();
    this.hasaClientRowData = null;
  }
}
