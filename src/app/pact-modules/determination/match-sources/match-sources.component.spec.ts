import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchSourcesComponent } from './match-sources.component';

describe('MatchSourcesComponent', () => {
  let component: MatchSourcesComponent;
  let fixture: ComponentFixture<MatchSourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchSourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
