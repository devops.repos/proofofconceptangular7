import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment';

//Models
import {
    ClientSearch,
    NewClient,
    CopyClient,
    SourceSystemClient,
    SetCompleteFlag,
    MedicaidSpendingInput,
    iMatchSummaryDetailsInput,
    HousingEpisodes
} from './match-sources.model';

@Injectable({
    providedIn: 'root'
})

export class MatchSourcesService {
    isHousingProgramMatchTabComplete = new BehaviorSubject<boolean>(false);
    isDhsMatchTabComplete = new BehaviorSubject<boolean>(false);
    isHasaMatchTabComplete = new BehaviorSubject<boolean>(false);
    isMedicaidMatchTabComplete = new BehaviorSubject<boolean>(false);
    isStarsMatchTabComplete = new BehaviorSubject<boolean>(false);

    matchSummaryDetailsURL = environment.pactApiUrl + 'DETApplicationReview/GetMatchSummaryDetails';
    clientSearchURL = environment.pactApiUrl + 'DETApplicationReview/ClientSearch';
    newClientURL = environment.pactApiUrl + 'DETApplicationReview/NewClient';
    copyClientURL = environment.pactApiUrl + 'DETApplicationReview/CopyClient';
    housingEpisodesURL = environment.pactApiUrl + 'DETApplicationReview/GetHousingEpisodes';
    sourceSystemClientURL = environment.pactApiUrl + 'DETApplicationReview/GetSourceSystemClient';
    updateTabCompleteForMatchSummaryURL = environment.pactApiUrl + 'DETApplicationReview/UpdateTabCompleteForMatchSummary';
    medicaidClientSearchURL = environment.pactApiUrl + 'DETApplicationReview/MedicaidClientSearch';
    getMedicaidClaimsURL = environment.pactApiUrl + 'DETApplicationReview/GetMedicaidClaims';
    starsClientSearchURL = environment.pactApiUrl + 'DETApplicationReview/StarsClientSearch';
    getStarsAssignmentsURL = environment.pactApiUrl + 'DETApplicationReview/GetStarsAssignments';
    getMedicaidSpendingURL = environment.pactApiUrl + 'DETApplicationReview/GetMedicaidSpending';
    getInpatientAndEmergencyRoomVisitsURL = environment.pactApiUrl + 'DETApplicationReview/GetInpatientAndEmergencyRoomVisits';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private httpClient: HttpClient) {
    }

    setHousingProgramMatchTabComplete(value: boolean) {
        this.isHousingProgramMatchTabComplete.next(value);
    }
    getHousingProgramMatchTabComplete() {
        return this.isHousingProgramMatchTabComplete.asObservable();
    }
    setDhsMatchTabComplete(value: boolean) {
        this.isDhsMatchTabComplete.next(value);
    }
    getDhsMatchTabComplete() {
        return this.isDhsMatchTabComplete.asObservable();
    }
    setHasaTabComplete(value: boolean) {
        this.isHasaMatchTabComplete.next(value);
    }
    getHasaMatchTabComplete() {
        return this.isHasaMatchTabComplete.asObservable();
    }
    setMedicaidMatchTabComplete(value: boolean) {
        this.isMedicaidMatchTabComplete.next(value);
    }
    getMedicaidMatchTabComplete() {
        return this.isMedicaidMatchTabComplete.asObservable();
    }
    setStarsMatchTabComplete(value: boolean) {
        this.isStarsMatchTabComplete.next(value);
    }
    getStarsMatchTabComplete() {
        return this.isStarsMatchTabComplete.asObservable();
    }

    //Get Match Summary Details
    getMatchSummaryDetails(matchSummaryDetailsInput: iMatchSummaryDetailsInput): Observable<any> {
        if (matchSummaryDetailsInput) {
            return this.httpClient.post(this.matchSummaryDetailsURL, JSON.stringify(matchSummaryDetailsInput), this.httpOptions);
        }
    }

    //Client Search
    searchClient(clientSearchData: ClientSearch): Observable<any> {
        if (clientSearchData) {
            return this.httpClient.post(this.clientSearchURL, JSON.stringify(clientSearchData), this.httpOptions);
        }
    }

    //New Client
    newClient(newClient: NewClient): Observable<any> {
        if (newClient) {
            return this.httpClient.post(this.newClientURL, JSON.stringify(newClient), this.httpOptions);
        }
    }

    //Copy Client
    copyClient(copyClient: CopyClient): Observable<any> {
        if (copyClient) {
            return this.httpClient.post(this.copyClientURL, JSON.stringify(copyClient), this.httpOptions);
        }
    }

    //Get Housing Episodes
    getHousingEpisodes(housingEpisodes: HousingEpisodes): Observable<any> {
        if (housingEpisodes) {
            return this.httpClient.post(this.housingEpisodesURL, JSON.stringify(housingEpisodes), this.httpOptions);
        }
    }

    //Get Source System Client Data
    getSourceSystemClient(sourceSystemClient: SourceSystemClient): Observable<any> {
        if (sourceSystemClient) {
            return this.httpClient.post(this.sourceSystemClientURL, JSON.stringify(sourceSystemClient), this.httpOptions);
        }
    }

    //Update Tab Complete For Match Summary
    updateTabCompleteForMatchSummary(setCompleteFlag: SetCompleteFlag): Observable<any> {
        if (setCompleteFlag) {
            return this.httpClient.post(this.updateTabCompleteForMatchSummaryURL, JSON.stringify(setCompleteFlag), this.httpOptions);
        }
    }

    //Medicaid Client Search
    searchMedicaidClient(clientSearchData: ClientSearch): Observable<any> {
        if (clientSearchData) {
            return this.httpClient.post(this.medicaidClientSearchURL, JSON.stringify(clientSearchData), this.httpOptions);
        }
    }

    //Get Medicaid Claims
    getMedicaidClaims(clientSearchData: ClientSearch): Observable<any> {
        if (clientSearchData) {
            return this.httpClient.post(this.getMedicaidClaimsURL, JSON.stringify(clientSearchData), this.httpOptions);
        }
    }

    //Get Medicaid Spending
    getMedicaidSpending(medicaidSpendingInput: MedicaidSpendingInput): Observable<any> {
        if (medicaidSpendingInput) {
            return this.httpClient.post(this.getMedicaidSpendingURL, JSON.stringify(medicaidSpendingInput), this.httpOptions);
        }
    }

    //Get In Patient And Emergency Room Visits
    getInpatientAndEmergencyRoomVisits(pactApplicationId: number): Observable<any> {
        if (pactApplicationId) {
            return this.httpClient.post(this.getInpatientAndEmergencyRoomVisitsURL, JSON.stringify(pactApplicationId), this.httpOptions);
        }
    }

    //Stars Client Search
    searchStarsClient(clientSearchData: ClientSearch): Observable<any> {
        if (clientSearchData) {
            return this.httpClient.post(this.starsClientSearchURL, JSON.stringify(clientSearchData), this.httpOptions);
        }
    }

    //Get Stars Assignment
    getStarsAssignments(pactApplicationId: number): Observable<any> {
        if (pactApplicationId) {
            return this.httpClient.post(this.getStarsAssignmentsURL, JSON.stringify(pactApplicationId), this.httpOptions);
        }
    }
}
