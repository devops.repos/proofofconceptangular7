import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
  selector: "medicaid-match-action",
  template: `
    <div style="color:blue" *ngIf="params.data.isClientRow && params.data.isCopied">
      Copied
    </div>
    <div *ngIf="params.data.isClientRow && !params.data.isCopied">
      <mat-icon
        class="menu-icon"
        color="warn"
        [matMenuTriggerFor]="medicaidClientMatchAction">
        more_vert
      </mat-icon>
      <mat-menu #medicaidClientMatchAction="matMenu">
        <button mat-menu-item (click)="onCopyClient()" class="menu-button">Copy</button>
      </mat-menu>
    </div>
  `,
  styles: [
    `
      .menu-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 30px;
        width: 100%;
        height: 30px;
      }
    `
  ]
})
export class MedicaidMatchActionComponent implements ICellRendererAngularComp {
  params: any;
  cell: any;

  //Constrctor
  constructor() {
  }

  //Init
  agInit(params: any) {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  //Refresh
  refresh(): boolean {
    return false;
  }

  //On Copy Client
  onCopyClient() {
    this.params.context.componentParent.onCopyClient(
      this.params.data.firstName,
      this.params.data.lastName,
      this.params.data.ssn,
      this.params.data.dob,
      this.params.data.genderType,
      this.params.data.cin);
  }
}
