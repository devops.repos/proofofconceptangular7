import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';
import { MedicaidMatchActionComponent } from './medicaid-match-action.component';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { MatchSourcesService } from '../match-sources.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import {
  iMatchSummaryDetails,
  ClientSearch,
  SetCompleteFlag,
  CopyClient,
  MedicaidSpendingInput,
  MedicaidSpending,
  InPatientEmergencyRoom,
  iMatchSummaryDetailsInput,
  iMatchedMedicaidClients
} from '../match-sources.model'

@Component({
  selector: 'app-medicaid-match',
  templateUrl: './medicaid-match.component.html',
  styleUrls: ['./medicaid-match.component.scss'],
  providers: [DatePipe]
})

export class MedicaidMatchComponent implements OnInit, OnDestroy {
  //Global Varables
  inPatientStaysEmergencyRoomExpanded: boolean;
  medicaidSpendingExpanded: boolean;
  isClientMatch: boolean = false;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  medicaidGroup: FormGroup;
  genders: RefGroupDetails[];
  matchSummaryDetailsData: iMatchSummaryDetails[];

  medicaidClientColumnDefs = [];
  medicaidClientRowData: iMatchedMedicaidClients[];
  medicaidClientDefaultColDef = {};
  medicaidClientGridOptions: GridOptions;
  medicaidClientOverlayNoRowsTemplate: string;
  medicaidClientFrameworkComponents: any;
  medicaidClientGridColumnApi: any;
  medicaidClientSingle: iMatchedMedicaidClients;
  context: any;

  medicaidClientData: iMatchedMedicaidClients[];

  inPatientStaysEmergencyRoomColumnDefs = [];
  inPatientStaysEmergencyRoomRowData: InPatientEmergencyRoom[];
  inPatientStaysEmergencyRoomDefaultColDef = {};
  inPatientStaysEmergencyRoomGridOptions: GridOptions;
  inPatientStaysEmergencyRoomOverlayNoRowsTemplate: string;

  medicaidSpendingColumnDefs = [];
  medicaidSpendingRowData: MedicaidSpending[];
  medicaidSpendingDefaultColDef = {};
  medicaidSpendingGridOptions: GridOptions;
  medicaidSpendingOverlayNoRowsTemplate: string;

  clientSearchData: ClientSearch = {
    pactApplicationId: null,
    sourceType: null,
    clientId: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    cin: null,
    referralDate: null,
    optionUserId: null
  };

  setCompleteFlag: SetCompleteFlag = {
    pactApplicationId: null,
    sourceSystemType: null,
    isSuccess: null
  }

  copyClient: CopyClient = {
    sourceSystemType: null,
    pactApplicationId: null,
    clientId: null,
    optionUserId: null,
    isSuccess: null
  }

  medicaidSpendingInput: MedicaidSpendingInput = {
    pactApplicationId: null,
    numberOfMonth: null
  }

  matchSummaryDetailsInput: iMatchSummaryDetailsInput = {
    pactApplicationId: null,
    optionUserId: null
  }

  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  //Constructor
  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private message: ToastrService,
    private datePipe: DatePipe,
    private matchSourcesService: MatchSourcesService,
    private dialogService: ConfirmDialogService) {
    //Medicaid Group Form Builder
    this.medicaidGroup = this.formBuilder.group({
      lastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      firstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      dobCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required],
      ssnCtrl: ['', [Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      cinCtrl: ['']
    });

    //Medicaid Client Grid Column Definitions
    this.medicaidClientColumnDefs = [
      {
        headerName: 'Medicaid #', field: 'cin', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactCINMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactCINMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Last Name', field: 'lastName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactLastNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactLastNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'First Name', field: 'firstName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactFirstNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactFirstNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Social Security #', field: 'ssn', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactSSNMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactSSNMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Date of Birth', field: 'dob', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactDOBMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactDOBMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Age', field: 'age', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactAgeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactAgeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Gender', field: 'genderTypeDescription', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactGenderTypeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactGenderTypeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Action',
        field: 'action',
        width: 60,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer'
      }
    ];
    this.medicaidClientDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.context = { componentParent: this };
    this.medicaidClientFrameworkComponents = {
      actionRenderer: MedicaidMatchActionComponent
    };

    //In Patient Stays Emergency Room Grid Column Definitions
    this.inPatientStaysEmergencyRoomColumnDefs = [
      { headerName: 'Medicaid #', field: 'medicaidNumber', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Type', field: 'serviceType', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Admission Date', field: 'admitDate', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Discharge Date', field: 'dischargeDate', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Location', field: 'serviceLocation', filter: 'agTextColumnFilter', width: 550 },
      { headerName: 'Description', field: 'serviceDescription', filter: 'agTextColumnFilter', width: 550 }
    ];
    this.inPatientStaysEmergencyRoomDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    //Medicaid Spending Grid Column Definitions
    this.medicaidSpendingColumnDefs = [
      { headerName: 'Sequence #', field: 'sequenceNo', filter: 'agTextColumnFilter' },
      { headerName: 'Service Month', field: 'serviceMonthYear', filter: 'agTextColumnFilter' },
      {
        headerName: 'Managed Care',
        field: 'managedCare',
        cellRenderer: function (params: { value: string; data: { managedCare: string }; }) {
          if (params.data.managedCare) {
            return '$' + params.data.managedCare;
          }
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'FFS Claims',
        field: 'ffsAmount',
        cellRenderer: function (params: { value: string; data: { ffsAmount: string }; }) {
          if (params.data.ffsAmount) {
            return '$' + params.data.ffsAmount;
          }
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Total Claims',
        field: 'totalClaims',
        cellRenderer: function (params: { value: string; data: { totalClaims: string }; }) {
          if (params.data.totalClaims) {
            return '$' + params.data.totalClaims;
          }
        },
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Cumulative Claims',
        field: 'cumulativeClaims',
        cellRenderer: function (params: { value: string; data: { cumulativeClaims: string; sequenceNo: number }; }) {
          if (params.data.cumulativeClaims) {
            if (params.data.sequenceNo === 15 || params.data.sequenceNo === 27) {
              return '<span style="color: blue"><b>$' + params.data.cumulativeClaims + '</b></span>';
            }
            else {
              return '<b>$' + params.data.cumulativeClaims + '</b>';
            }
          }
        },
        filter: 'agTextColumnFilter'
      }
    ];
    this.medicaidSpendingDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  //On Init
  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
          }
          else {
            return;
          }
        }
      }
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
      if (res) {
        const data = res as iApplicationDeterminationData;
        if (data) {
          this.applicationDeterminationData = data;
          this.populateMedicaidMatchForm(this.applicationDeterminationData);
          if (this.applicationDeterminationData.pactApplicationId) {
            this.matchSummaryDetailsInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
            this.matchSummaryDetailsInput.optionUserId = this.userData.optionUserId;
            this.matchSourcesService.getMatchSummaryDetails(this.matchSummaryDetailsInput).subscribe(res => {
              if (res) {
                const data = res as iMatchSummaryDetails[];
                if (data && data.length > 0) {
                  this.matchSummaryDetailsData = data.filter(({ sourceSystemType, matchedType, matchOutcomeType }) => sourceSystemType === 577 && matchedType === 33 && matchOutcomeType === 753);
                  this.populateMedicaidClientGrid(this.applicationDeterminationData);
                }
              }
            });
          }
        }
      }
    });

    //Get Genders From ReFGroupDetails
    this.commonService.getRefGroupDetails("4")
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.genders = data;
        },
        error => {
          throw new Error(error.message);
        }
      );
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  //Validate Date of Birth
  validateDOB() {
    if (this.medicaidGroup.get('dobCtrl').value && !moment(this.medicaidGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid Date of Birth.");
      }
      this.medicaidGroup.controls['dobCtrl'].setValue("");
      return;
    }
  }

  //Medicaid Client Grid Ready
  medicaidClientGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.medicaidClientGridColumnApi = params.columnApi;
    this.medicaidClientOverlayNoRowsTemplate = '<span style="color: #337ab7">No Clients To Show</span>';
    if (this.applicationDeterminationData) {
      this.populateMedicaidClientGrid(this.applicationDeterminationData);
    }
  }

  //Populate Medicaid Client Grid
  populateMedicaidClientGrid(applicationDeterminationData: iApplicationDeterminationData) {
    if (this.matchSummaryDetailsData && this.matchSummaryDetailsData.length > 0) {
      this.medicaidClientGridColumnApi.setColumnVisible('action', false);
      this.isClientMatch = true;
      this.medicaidClientRowData = [
        {
          clientId: null,
          lastName: applicationDeterminationData.demographicLastName,
          firstName: applicationDeterminationData.demographicFirstName,
          ssn: applicationDeterminationData.demographicSSN,
          dob: applicationDeterminationData.demographicDOB,
          age: applicationDeterminationData.demographicAge,
          genderTypeDescription: applicationDeterminationData.demographicGenderDescription,
          cin: applicationDeterminationData.demographicCIN,
          isClientRow: false,
          isCopied: false
        }
      ];
      this.matchSummaryDetailsData.forEach(m => {
        this.medicaidClientSingle = {
          clientId: m.sourceClientID,
          lastName: m.lastName,
          firstName: m.firstName,
          ssn: m.ssn,
          dob: m.dob,
          age: m.age,
          genderTypeDescription: m.genderTypeDescription,
          cin: m.cin,
          isClientRow: true,
          isCopied: false,
          isExactLastNameMatch: applicationDeterminationData.demographicLastName === m.lastName ? true : false,
          isExactFirstNameMatch: applicationDeterminationData.demographicFirstName === m.firstName ? true : false,
          isExactSSNMatch: applicationDeterminationData.demographicSSN === m.ssn ? true : false,
          isExactDOBMatch: applicationDeterminationData.demographicDOB === m.dob ? true : false,
          isExactAgeMatch: applicationDeterminationData.demographicAge === m.age ? true : false,
          isExactGenderTypeMatch: applicationDeterminationData.demographicGenderDescription === m.genderTypeDescription ? true : false,
          isExactCINMatch: applicationDeterminationData.demographicCIN === m.cin ? true : false
        }
        this.medicaidClientRowData.push(this.medicaidClientSingle);
      });
      this.medicaidGroup.disable();
    }
    else {
      this.medicaidClientRowData = null;
    }
  }

  //In Patient Stays Emergency Room Grid Ready
  inPatientStaysEmergencyRoomGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.inPatientStaysEmergencyRoomOverlayNoRowsTemplate = '<span style="color: #337ab7">No In Patient Stays / Emergency Room Visits To Show</span>';
    this.populateInPatientStaysEmergencyRoomGrid();
    //params.api.sizeColumnsToFit();
  }

  //Populate InPatient and Emergency Room Visits
  populateInPatientStaysEmergencyRoomGrid() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      //Api call to get in patient and emergency room visits
      this.matchSourcesService.getInpatientAndEmergencyRoomVisits(this.applicationDeterminationData.pactApplicationId)
        .subscribe(res => {
          if (res) {
            const data = res as InPatientEmergencyRoom[];
            if (data && data.length > 0) {
              this.inPatientStaysEmergencyRoomRowData = data;
            }
            else {
              this.inPatientStaysEmergencyRoomRowData = null;
            }
          }
          else {
            this.inPatientStaysEmergencyRoomRowData = null;
          }
        });
    }
  }

  //Medicaid Spending Grid Ready
  medicaidSpendingGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.medicaidSpendingOverlayNoRowsTemplate = '<span style="color: #337ab7">No Medicaid Spending To Show</span>';
    this.populateMedicaidSpendingGrid();
    params.api.sizeColumnsToFit();
  }

  //Populate Medicaid Spending Grid
  populateMedicaidSpendingGrid() {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.medicaidSpendingInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.medicaidSpendingInput.numberOfMonth = 15;
      //Api call to get medicaid spending
      this.matchSourcesService.getMedicaidSpending(this.medicaidSpendingInput)
        .subscribe(res => {
          if (res) {
            const data = res as MedicaidSpending[];
            if (data && data.length > 0) {
              this.medicaidSpendingRowData = data;
            }
            else {
              this.medicaidSpendingRowData = null;
            }
          }
          else {
            this.medicaidSpendingRowData = null;
          }
        });
    }
  }

  //Populate Medicaid Match Form
  populateMedicaidMatchForm(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      this.medicaidGroup.controls['lastNameCtrl'].setValue(applicationDeterminationData.demographicLastName);
      this.medicaidGroup.controls['firstNameCtrl'].setValue(applicationDeterminationData.demographicFirstName);
      this.medicaidGroup.controls['dobCtrl'].setValue(applicationDeterminationData.demographicDOB);
      this.medicaidGroup.controls['genderCtrl'].setValue(applicationDeterminationData.demographicGenderType);
      this.medicaidGroup.controls['ssnCtrl'].setValue(applicationDeterminationData.demographicSSN);
      this.medicaidGroup.controls['cinCtrl'].setValue(applicationDeterminationData.demographicCIN);
    }
  }

  //Search Medicaid Client
  searchMedicaidClient() {
    if (this.medicaidGroup.valid) {
      if (this.applicationDeterminationData) {
        this.clientSearchData.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
        this.clientSearchData.sourceType = 577;
        this.clientSearchData.firstName = this.medicaidGroup.get('firstNameCtrl').value.trim().toUpperCase();
        this.clientSearchData.lastName = this.medicaidGroup.get('lastNameCtrl').value.trim().toUpperCase();
        this.clientSearchData.ssn = this.medicaidGroup.get('ssnCtrl').value;
        this.clientSearchData.dob = this.datePipe.transform(this.medicaidGroup.get('dobCtrl').value, 'MM/dd/yyyy');
        this.clientSearchData.genderType = this.medicaidGroup.get('genderCtrl').value;
        this.clientSearchData.cin = this.medicaidGroup.get('cinCtrl').value.toUpperCase();
        this.clientSearchData.referralDate = this.applicationDeterminationData.referralDate;
        this.clientSearchData.optionUserId = this.userData.optionUserId;
        //Api call to search medicaid client
        this.matchSourcesService.searchMedicaidClient(this.clientSearchData)
          .subscribe(res => {
            if (res) {
              const data = res as iMatchedMedicaidClients[];
              if (data && data.length > 0) {
                if (data.length >= 2) {
                  for (let i = 1; i < data.length; i++) {
                    data[i].isExactLastNameMatch = data[0].lastName === data[i].lastName ? true : false;
                    data[i].isExactFirstNameMatch = data[0].firstName === data[i].firstName ? true : false;
                    data[i].isExactSSNMatch = data[0].ssn === data[i].ssn ? true : false;
                    data[i].isExactDOBMatch = data[0].dob === data[i].dob ? true : false;
                    data[i].isExactAgeMatch = data[0].age === data[i].age ? true : false;
                    data[i].isExactGenderTypeMatch = data[0].genderTypeDescription === data[i].genderTypeDescription ? true : false;
                    data[i].isExactCINMatch = data[0].cin === data[i].cin ? true : false;
                  }
                }
                this.medicaidClientRowData = data;
                this.medicaidClientData = data;
              }
              else {
                this.noClientMatchDialog();
              }
            }
            else {
              this.noClientMatchDialog();
            }
          });
      }
    }
    else {
      this.validationMessage();
    }
  }

  //No Client Match Dialog
  noClientMatchDialog() {
    this.dialogService.confirmDialog('No Match Found', 'Do you want to continue without match?', '', 'Continue Without Match', 'Cancel')
      .then(() => {
        if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
          this.setCompleteFlag.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
          this.setCompleteFlag.sourceSystemType = 577;
          //Api call to create a new client 
          this.matchSourcesService.updateTabCompleteForMatchSummary(this.setCompleteFlag)
            .subscribe(res => {
              if (res) {
                const data = res as SetCompleteFlag
                if (data && data.isSuccess) {
                  this.matchSourcesService.setMedicaidMatchTabComplete(data.isSuccess);
                }
              }
            });
        }
        else {
          this.message.error("Unable to set complete flag.");
        }
      }, () => {
      });
  }

  //On Copy Client
  onCopyClient(firstName: string, lastName: string, ssn: string, dob: string, genderType: number, cin: string) {
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.clientSearchData.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.clientSearchData.sourceType = 577;
      this.clientSearchData.firstName = firstName.trim().toUpperCase();
      this.clientSearchData.lastName = lastName.trim().toUpperCase();
      if (ssn) {
        this.clientSearchData.ssn = ssn;
      }
      else {
        this.clientSearchData.ssn = this.medicaidGroup.get('ssnCtrl').value;
      }
      this.clientSearchData.dob = this.datePipe.transform(dob, 'MM/dd/yyyy');
      this.clientSearchData.genderType = genderType;
      this.clientSearchData.cin = cin.toUpperCase();
      this.clientSearchData.referralDate = this.applicationDeterminationData.referralDate;
      this.clientSearchData.optionUserId = this.userData.optionUserId;
      //Api call to copy the client to the match summary and get medicaid claims data from mdw
      this.matchSourcesService.getMedicaidClaims(this.clientSearchData)
        .subscribe(res => {
          if (res && res.isSaved) {
            if (this.medicaidClientData) {
              this.medicaidClientData.find(x => x.cin === cin && x.isClientRow === true).isCopied = true;
              this.medicaidClientRowData = this.medicaidClientData as iMatchedMedicaidClients[];
            }
            this.isClientMatch = true;
            this.medicaidGroup.disable();
            this.matchSourcesService.setMedicaidMatchTabComplete(true);
            this.populateInPatientStaysEmergencyRoomGrid();
            this.populateMedicaidSpendingGrid();
          }
        });
    }
  }

  //Validation Message
  validationMessage() {
    if (this.medicaidGroup.get('firstNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("First Name is required.");
      }
    }
    else if (this.medicaidGroup.get('lastNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Last Name is required.");
      }
    }
    else if (this.medicaidGroup.get('ssnCtrl').errors && this.medicaidGroup.get('ssnCtrl').errors.required) {
      if (!this.message.currentlyActive) {
        this.message.error("SSN is required.");
      }
    }
    else if (this.medicaidGroup.get('ssnCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid SSN.");
      }
    }
    else if (this.medicaidGroup.get('dobCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Date of Birth is required.");
      }
    }
    else if (this.medicaidGroup.get('genderCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Gender is required.");
      }
    }
    else if (this.medicaidGroup.get('cinCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("CIN# is formatted as follows: Two alpha characters followed by five numeric characters and then followed by one alpha character; e.g: AA12345A");
      }
    }
  }

  //Refresh Data
  refreshData() {
    if (this.applicationDeterminationData) {
      this.populateMedicaidMatchForm(this.applicationDeterminationData);
    }
    this.medicaidClientRowData = null;
  }
}

