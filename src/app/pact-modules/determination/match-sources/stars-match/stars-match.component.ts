import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { MatchSourcesService } from '../match-sources.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { iMatchSummaryDetails, ClientSearch, iMatchedClients, SetCompleteFlag, CopyClient, StarsAssignments, iMatchSummaryDetailsInput } from '../match-sources.model'

@Component({
  selector: 'app-stars-match',
  templateUrl: './stars-match.component.html',
  styleUrls: ['./stars-match.component.scss'],
  providers: [DatePipe]
})

export class StarsMatchComponent implements OnInit, OnDestroy {
  //Global Varables
  assignmentExpanded: boolean;
  isClientMatch: boolean = false;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;

  starsGroup: FormGroup;
  genders: RefGroupDetails[];
  matchSummaryDetailsData: iMatchSummaryDetails[];

  starsClientColumnDefs = [];
  starsClientRowData: iMatchedClients[];
  starsClientDefaultColDef = {};
  starsClientGridOptions: GridOptions;
  starsClientOverlayNoRowsTemplate: string;
  starsClientSingle: iMatchedClients;
  context: any;

  assignmentColumnDefs = [];
  assignmentRowData: StarsAssignments[];
  assignmentDefaultColDef = {};
  assignmentGridOptions: GridOptions;
  assignmentOverlayNoRowsTemplate: string;

  clientSearchData: ClientSearch = {
    pactApplicationId: null,
    sourceType: null,
    clientId: null,
    firstName: null,
    lastName: null,
    ssn: null,
    dob: null,
    genderType: null,
    cin: null,
    referralDate: null,
    optionUserId: null
  };

  setCompleteFlag: SetCompleteFlag = {
    pactApplicationId: null,
    sourceSystemType: null,
    isSuccess: null
  }

  copyClient: CopyClient = {
    sourceSystemType: null,
    pactApplicationId: null,
    clientId: null,
    optionUserId: null,
    isSuccess: null
  }

  starsAssignments: StarsAssignments = {
    assignmentId: null,
    admissionDate: null,
    assignmentCreatedDate: null,
    treatmentEndDate: null,
    treatmentEndActionDate: null,
    assignmentUpdatedDate: null,
    isResidential: null,
    siteCode: null,
    isCorrections: null,
  }

  matchSummaryDetailsInput: iMatchSummaryDetailsInput = {
    pactApplicationId: null,
    optionUserId: null
  }

  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  //Constructor
  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private message: ToastrService,
    private datePipe: DatePipe,
    private matchSourcesService: MatchSourcesService,
    private dialogService: ConfirmDialogService) {
    //Stars Group Form Builder
    this.starsGroup = this.formBuilder.group({
      caseIdCtrl: [''],
      lastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      firstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      dobCtrl: ['', Validators.required],
      genderCtrl: ['', Validators.required],
      ssnCtrl: ['', [Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      cinCtrl: ['']
    });

    //Stars Client Grid Column Definitions
    this.starsClientColumnDefs = [
      { headerName: 'Case #', field: 'clientId', filter: 'agTextColumnFilter', width: 150 },
      {
        headerName: 'Last Name', field: 'lastName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactLastNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactLastNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'First Name', field: 'firstName', filter: 'agTextColumnFilter', width: 200,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactFirstNameMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactFirstNameMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Social Security #', field: 'ssn', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactSSNMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactSSNMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Date of Birth', field: 'dob', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactDOBMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactDOBMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Age', field: 'age', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactAgeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactAgeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'Gender', field: 'genderTypeDescription', filter: 'agTextColumnFilter', width: 150,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactGenderTypeMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactGenderTypeMatch) {
            return { color: 'red' };
          }
        }
      },
      {
        headerName: 'CIN', field: 'cin', filter: 'agTextColumnFilter', width: 120,
        cellStyle: function (params: { data: { isClientRow: boolean; isExactCINMatch: boolean }; }) {
          if (!params.data.isClientRow) {
            return { color: 'blue' };
          }
          else if (params.data.isExactCINMatch) {
            return { color: 'red' };
          }
        }
      }
    ];
    this.starsClientDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    //Assignment Grid Column Definitions
    this.assignmentColumnDefs = [
      { headerName: 'Assignment ID', field: 'assignmentId', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Admission Date', field: 'admissionDate', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Site Code', field: 'siteCode', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'SU Residential Treatment / Detox', field: 'isResidential', filter: 'agTextColumnFilter', width: 250 },
      { headerName: 'Corrections / Legal', field: 'isCorrections', filter: 'agTextColumnFilter', width: 200 }
    ];
    this.assignmentDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  //On Init
  ngOnInit() {
    this.starsGroup.controls['caseIdCtrl'].disable();

    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
          }
          else {
            return;
          }
        }
      }
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
      if (res) {
        const data = res as iApplicationDeterminationData;
        if (data) {
          this.applicationDeterminationData = data;
          this.populateStarsMatchForm(this.applicationDeterminationData);
          if (this.applicationDeterminationData.pactApplicationId) {
            this.matchSummaryDetailsInput.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
            this.matchSummaryDetailsInput.optionUserId = this.userData.optionUserId;
            this.matchSourcesService.getMatchSummaryDetails(this.matchSummaryDetailsInput).subscribe(res => {
              if (res) {
                const data = res as iMatchSummaryDetails[];
                if (data && data.length > 0) {
                  this.matchSummaryDetailsData = data.filter(({ sourceSystemType, matchedType, matchOutcomeType }) => sourceSystemType === 576 && matchedType === 33 && matchOutcomeType === 753);
                  this.populateStarsClientGrid(this.applicationDeterminationData);
                }
              }
            });
          }
        }
      }
    });

    //Get Genders From ReFGroupDetails
    this.commonService.getRefGroupDetails("4")
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.genders = data;
        },
        error => {
          throw new Error(error.message);
        }
      );
  }
  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }

  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  //Validate Date of Birth
  validateDOB() {
    if (this.starsGroup.get('dobCtrl').value && !moment(this.starsGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid Date of Birth.");
      }
      this.starsGroup.controls['dobCtrl'].setValue("");
      return;
    }
  }

  //Stars Client Grid Ready
  starsClientGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.starsClientOverlayNoRowsTemplate = '<span style="color: #337ab7">No Clients To Show</span>';
    if (this.applicationDeterminationData) {
      this.populateStarsClientGrid(this.applicationDeterminationData);
    }
  }

  //Populate Stars Client Grid
  populateStarsClientGrid(applicationDeterminationData: iApplicationDeterminationData) {
    if (this.matchSummaryDetailsData && this.matchSummaryDetailsData.length > 0) {
      this.isClientMatch = true;
      this.starsClientRowData = [
        {
          clientId: null,
          lastName: applicationDeterminationData.demographicLastName,
          firstName: applicationDeterminationData.demographicFirstName,
          ssn: applicationDeterminationData.demographicSSN,
          dob: applicationDeterminationData.demographicDOB,
          age: applicationDeterminationData.demographicAge,
          genderType: applicationDeterminationData.demographicGenderType,
          genderTypeDescription: applicationDeterminationData.demographicGenderDescription,
          cin: applicationDeterminationData.demographicCIN,
          isClientRow: false
        }
      ];
      this.matchSummaryDetailsData.forEach(m => {
        this.starsClientSingle = {
          clientId: m.sourceClientID,
          lastName: m.lastName,
          firstName: m.firstName,
          ssn: m.ssn,
          dob: m.dob,
          age: m.age,
          genderType: null,
          genderTypeDescription: m.genderTypeDescription,
          cin: m.cin,
          isClientRow: true,
          isExactLastNameMatch: applicationDeterminationData.demographicLastName === m.lastName ? true : false,
          isExactFirstNameMatch: applicationDeterminationData.demographicFirstName === m.firstName ? true : false,
          isExactSSNMatch: applicationDeterminationData.demographicSSN === m.ssn ? true : false,
          isExactDOBMatch: applicationDeterminationData.demographicDOB === m.dob ? true : false,
          isExactAgeMatch: applicationDeterminationData.demographicAge === m.age ? true : false,
          isExactGenderTypeMatch: applicationDeterminationData.demographicGenderDescription === m.genderTypeDescription ? true : false,
          isExactCINMatch: applicationDeterminationData.demographicCIN === m.cin ? true : false
        }
        this.starsClientRowData.push(this.starsClientSingle);
      });
      this.starsGroup.controls['caseIdCtrl'].setValue(this.starsClientSingle.clientId);
      this.starsGroup.disable();
    }
    else {
      this.starsClientRowData = null;
    }
  }

  //Assignment Grid Ready
  assignmentGridReady(params: { api: { setDomLayout: (arg0: string) => void; sizeColumnsToFit: () => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.assignmentOverlayNoRowsTemplate = '<span style="color: #337ab7">No Assignment To Show</span>';
    if (this.applicationDeterminationData) {
      this.populateAssignmentGrid(this.applicationDeterminationData);
    }
    //params.api.sizeColumnsToFit();
  }

  //Poipulate Assignment Grid
  populateAssignmentGrid(applicationDeterminationData: iApplicationDeterminationData) {
    //Api call to get stars assignments
    this.matchSourcesService.getStarsAssignments(applicationDeterminationData.pactApplicationId)
      .subscribe(res => {
        if (res) {
          const data = res as StarsAssignments[];
          if (data && data.length > 0) {
            this.assignmentRowData = data;
          }
          else {
            this.assignmentRowData = null;
          }
        }
        else {
          this.assignmentRowData = null;
        }
      });
  }

  //Populate Stars Match Form
  populateStarsMatchForm(applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      this.starsGroup.controls['lastNameCtrl'].setValue(applicationDeterminationData.demographicLastName);
      this.starsGroup.controls['firstNameCtrl'].setValue(applicationDeterminationData.demographicFirstName);
      this.starsGroup.controls['dobCtrl'].setValue(applicationDeterminationData.demographicDOB);
      this.starsGroup.controls['genderCtrl'].setValue(applicationDeterminationData.demographicGenderType);
      this.starsGroup.controls['ssnCtrl'].setValue(applicationDeterminationData.demographicSSN);
      this.starsGroup.controls['cinCtrl'].setValue(applicationDeterminationData.demographicCIN);
    }
  }

  //Search Stars Client
  searchStarsClient() {
    if (this.starsGroup.valid) {
      if (this.applicationDeterminationData) {
        this.clientSearchData.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
        this.clientSearchData.sourceType = 576;
        this.clientSearchData.clientId = this.starsGroup.get('caseIdCtrl').value;
        this.clientSearchData.firstName = this.starsGroup.get('firstNameCtrl').value.trim();
        this.clientSearchData.lastName = this.starsGroup.get('lastNameCtrl').value.trim();
        this.clientSearchData.ssn = this.starsGroup.get('ssnCtrl').value;
        this.clientSearchData.dob = this.datePipe.transform(this.starsGroup.get('dobCtrl').value, 'MM/dd/yyyy');
        this.clientSearchData.genderType = this.starsGroup.get('genderCtrl').value;
        this.clientSearchData.cin = this.starsGroup.get('cinCtrl').value;
        this.clientSearchData.referralDate = this.applicationDeterminationData.referralDate;
        this.clientSearchData.optionUserId = this.userData.optionUserId;
        //Api call to searcr stars client
        this.matchSourcesService.searchStarsClient(this.clientSearchData)
          .subscribe(res => {
            if (res) {
              const data = res as iMatchedClients[];
              if (data && data.length > 0) {
                if (data.length >= 2) {
                  for (let i = 1; i < data.length; i++) {
                    data[i].isExactLastNameMatch = data[0].lastName === data[i].lastName ? true : false;
                    data[i].isExactFirstNameMatch = data[0].firstName === data[i].firstName ? true : false;
                    data[i].isExactSSNMatch = data[0].ssn === data[i].ssn ? true : false;
                    data[i].isExactDOBMatch = data[0].dob === data[i].dob ? true : false;
                    data[i].isExactAgeMatch = data[0].age === data[i].age ? true : false;
                    data[i].isExactGenderTypeMatch = data[0].genderTypeDescription === data[i].genderTypeDescription ? true : false;
                    data[i].isExactCINMatch = data[0].cin === data[i].cin ? true : false;
                  }
                }
                this.starsClientRowData = data;
                const sclientid = data.find(s => s.isClientRow === true).clientId;
                this.starsGroup.controls['caseIdCtrl'].setValue(sclientid);
                this.isClientMatch = true;
                this.starsGroup.disable();
                this.populateAssignmentGrid(this.applicationDeterminationData);
                this.matchSourcesService.setStarsMatchTabComplete(true);
              }
              else {
                this.noClientMatchDialog();
              }
            }
            else {
              this.noClientMatchDialog();
            }
          });
      }
    }
    else {
      this.validationMessage();
    }
  }

  //No Client Match Dialog
  noClientMatchDialog() {
    this.dialogService.confirmDialog('No Match Found', 'Do you want to continue without match?', '', 'Continue Without Match', 'Cancel')
      .then(() => {
        if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
          this.setCompleteFlag.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
          this.setCompleteFlag.sourceSystemType = 576;
          //Api call to create a new client 
          this.matchSourcesService.updateTabCompleteForMatchSummary(this.setCompleteFlag)
            .subscribe(res => {
              if (res) {
                const data = res as SetCompleteFlag
                if (data && data.isSuccess) {
                  this.matchSourcesService.setStarsMatchTabComplete(data.isSuccess);
                }
              }
            });
        }
        else {
          this.message.error("Unable to set complete flag.");
        }
      }, () => {
      });
  }

  //Validation Message
  validationMessage() {
    if (this.starsGroup.get('firstNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("First Name is required.");
      }
    }
    else if (this.starsGroup.get('lastNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Last Name is required.");
      }
    }
    else if (this.starsGroup.get('ssnCtrl').errors && this.starsGroup.get('ssnCtrl').errors.required) {
      if (!this.message.currentlyActive) {
        this.message.error("SSN is required.");
      }
    }
    else if (this.starsGroup.get('ssnCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid SSN.");
      }
    }
    else if (this.starsGroup.get('dobCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Date of Birth is required.");
      }
    }
    else if (this.starsGroup.get('genderCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Gender is required.");
      }
    }
    else if (this.starsGroup.get('cinCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("CIN# is formatted as follows: Two alpha characters followed by five numeric characters and then followed by one alpha character; e.g: AA12345A");
      }
    }
  }

  //Refresh Data
  refreshData() {
    if (this.applicationDeterminationData) {
      this.populateStarsMatchForm(this.applicationDeterminationData);
    }
    this.starsClientRowData = null;
  }
}

