export interface iMatchSummaryDetails {
    pactApplicationID?: number;
    isMatchSummaryTabComplete?: boolean;
    isHousingProgramMatchTabComplete?: boolean;
    isDHSMatchTabComplete?: boolean;
    isHASAMatchTabComplete?: boolean;
    isMedicaidMatchTabComplete?: boolean;
    isSTARSMatchTabComplete?: boolean;
    sourceSystemType?: number;
    sourceSystemTypeDescription: string;
    sourceSystemTypeLongDescription: string;
    matchedType?: number;
    matchedTypeDescription: string;
    matchOutcomeType?: number;
    matchOutcomeDescription: string;
    clientMatchedStageDescription: string;
    sourceClientID?: number;
    firstName: string;
    lastName: string;
    ssn: string;
    dob: string;
    age?: number;
    genderTypeDescription: string;
    cin: string;
}

export interface iMatchSummaryDetailsInput {
    pactApplicationId?: number;
    optionUserId?: number;
}

export interface iMatchedClients {
    clientId?: number;
    lastName: string;
    firstName: string;
    ssn: string;
    dob: string;
    age?: number;
    genderType?: number;
    genderTypeDescription: string;
    cin: string;
    isClientRow?: boolean;
    isExactLastNameMatch?: boolean;
    isExactFirstNameMatch?: boolean;
    isExactSSNMatch?: boolean;
    isExactDOBMatch?: boolean;
    isExactAgeMatch?: boolean;
    isExactGenderTypeMatch?: boolean;
    isExactCINMatch?: boolean;
}

export interface iMatchedMedicaidClients {
    clientId?: number;
    lastName: string;
    firstName: string;
    ssn: string;
    dob: string;
    age?: number;
    genderType?: number;
    genderTypeDescription: string;
    cin: string;
    isClientRow: boolean;
    isCopied: boolean;
    isExactLastNameMatch?: boolean;
    isExactFirstNameMatch?: boolean;
    isExactSSNMatch?: boolean;
    isExactDOBMatch?: boolean;
    isExactAgeMatch?: boolean;
    isExactGenderTypeMatch?: boolean;
    isExactCINMatch?: boolean;
}

export interface iHasaMatchedClients {
    clientId?: number;
    lastName: string;
    firstName: string;
    ssn: string;
    dob: string;
    age?: number;
    genderType?: number;
    genderTypeDescription: string;
    cin: string;
    isActiveHasaCase: string;
    isCurrentlyInShelter: string;
    isClientRow: boolean;
    isExactLastNameMatch?: boolean;
    isExactFirstNameMatch?: boolean;
    isExactSSNMatch?: boolean;
    isExactDOBMatch?: boolean;
    isExactAgeMatch?: boolean;
    isExactGenderTypeMatch?: boolean;
}

export interface ClientSearch {
    pactApplicationId?: number;
    sourceType: number;
    clientId?: number;
    firstName: string;
    lastName: string;
    ssn: string;
    dob: string;
    genderType: number;
    cin: string;
    referralDate: string;
    optionUserId: number;
}

export interface NewClient {
    pactApplicationId?: number;
    firstName: string;
    lastName: string;
    ssn: string;
    dob: string;
    genderType?: number;
    cin: string;
    optionUserId?: number;
    pactClientId?: number;
}

export interface CopyClient {
    sourceSystemType?: number;
    pactApplicationId?: number;
    clientId?: number;
    optionUserId?: number;
    isSuccess?: boolean;
}

export interface HousingEpisodes {
    pactApplicationId?: number
    cth: string;
    fromDate: string;
    toDate: string;
    housingType?: number;
    housingTypeDescription: string;
    facilityName: string;
    streetAddress: string;
    city: string;
    state: string;
    homelessStayDocumentedType?: number;
    homelessStayDocumented: string;
    episodeSourceType?: number;
    episodeSource: string;
    sourceDate: string;
    isEpisodeAddedPostTransmission?: boolean;
}

export interface SourceSystemClient {
    pactApplicationId?: number;
    pactClientId?: number;
    sourceSystemType?: number;
    sourceSystemTypeDescription: string;
    sourceClientID?: number;
    lastName: string;
    firstName: string;
    ssn: string;
    dob: string;
    age?: number;
    genderType?: number;
    genderTypeDescription: string;
    cin: string;
}

export interface SetCompleteFlag {
    pactApplicationId?: number;
    sourceSystemType?: number;
    isSuccess?: boolean;
}

export interface StarsAssignments {
    assignmentId?: number;
    admissionDate: string;
    assignmentCreatedDate: string;
    treatmentEndDate: string;
    treatmentEndActionDate: string;
    assignmentUpdatedDate: string;
    isResidential: string;
    siteCode: string;
    isCorrections: string;
}

export interface MedicaidSpendingInput {
    pactApplicationId: number;
    numberOfMonth: number;
}

export interface MedicaidSpending {
    sequenceNo?: number;
    serviceMonthYear: string;
    managedCare: string;
    ffsAmount: string;
    totalClaims: string;
    cumulativeClaims: string;
}

export interface InPatientEmergencyRoom {
    medicaidNumber: string;
    serviceType: string;
    admitDate: string;
    dischargeDate: string;
    serviceLocation: string;
    serviceDescription: string;
}