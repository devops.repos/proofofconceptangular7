export class DETDeterminationPopulationLevel {
    detDeterminationPopulationLevelID? : number;
    pactApplicationID? : number;
    agreementPopulationID? : number;
    agreementTypeName : string
    appliedForPopulation? : number;
    appliedForPopulationDescription : string;
    clinicalEligibility? : number;
    clinicalEligibilityDescription : string;
    homelessEligibility? : number;
    homelessEligibilityDescription : string;
    finalEligibility? : number;
    finalEligibilityDescription? : string;
    mrt? : number;
    mrtDescription : string;
    //userID?: number;
  }


export class DETSummary{
    detOutcomeID? : number;
	vulnerabilityType? : number;
	vulnerabilityTypeDescription : string;
	hudType ? : number;
	hudTypeDescription : string;
	determinationDate : string;
	reviewerName: string;
	approvalFrom: string;
    approvalTo: string;
    detDeterminationPopulationLevel : DETDeterminationPopulationLevel[];
}

export class DETSummaryInput{
    pactApplicationID? : number;
    userID?: number;
}

export class DETDeterminationConditionRecommendation {
    detDeterminationConditionRecommendationID? : number;
    pactApplicationID? : number;
    conditionRecommendationType? : number;
    conditionRecommendationTypeDescription : string
    refGroupID? : number;
    isCondition? : boolean;
    isRecommendation? : boolean;
}

export class DETDeterminationLevelApproved {
    detDeterminationLevelApprovedID? : number;
    pactApplicationID? : number;
    levelOfCareType? : number;
    levelOfCareTypeDescription : string;
    isChecked? : boolean;
    isVisible? : boolean
}

export class HousingTypes {
    detOutcomeID? : number;
    pactApplicationID? : number;
    housingType? : number;
    housingTypeDescription : string;
    isChecked? : boolean;
}

export class ConditionRecommendation
{
    detOutcomeID? : number;
    pactApplicationID? : number;
    isSufficientlyStabilized? : boolean;
    isSMI? : boolean;
    isNYC3? : boolean;
    isNYC1515? : boolean;
    isESSHI? : boolean;
    isNYC3PopEApproved? : boolean;
    isAnyPopulationApproved? : boolean;
    conditionOrRecommendationExists? : number;
    letterComment: string;
    detDeterminationLevelApproved : DETDeterminationLevelApproved[];
    housingTypes : HousingTypes[];
    detDeterminationConditionRecommendation : DETDeterminationConditionRecommendation[];
    userID?: number;
}