import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTabChangeEvent } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

//Ag-Grid References
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridOptions } from 'ag-grid-community';

//Model References
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model';
import { AuthData } from 'src/app/models/auth-data.model';
import {
  DETDeterminationPopulationLevel, DETSummary, DETSummaryInput
  , DETDeterminationConditionRecommendation, DETDeterminationLevelApproved
  , HousingTypes, ConditionRecommendation
} from './determination-summary.model';
import { DETTabTimeDuration } from 'src/app/models/determination-common.model';
//import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';

//Servicece';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { DeterminationSummaryService } from './determination-summary.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

@Component({
  selector: 'app-determination-summary',
  templateUrl: './determination-summary.component.html',
  styleUrls: ['./determination-summary.component.scss']
})

export class DeterminationSummaryComponent implements OnInit, OnDestroy {
  currentTab: number = 0;
  selectedTab: number = 0;
  activatedRouteSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;
  pactApplicationId: number;
  isSaveVisible: boolean = false;
  detTabTimeDuration: DETTabTimeDuration = {
    pactApplicationId: null, tabInName: null
    , tabOutName: null, optionUserId: null
  };

  detSummary: DETSummary;
  detSummaryInput: DETSummaryInput = { pactApplicationID: null, userID: null };
  vulnerabilityTypeDescription: string;
  hudTypeDescription: string;
  determinationDate: string;
  reviewerName: string;
  approvalFrom: string;
  approvalTo: string;

  @ViewChild('agGrid') agGrid: AgGridAngular;
  gridApi: any;
  gridColumnApi: any;
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;
  context: any;
  summaryColumnDefs: any;
  summaryRowSelection: any;
  summaryDefaultColDef: any;
  public summaryGridOptions: GridOptions;
  summaryRowData: DETDeterminationPopulationLevel[];
  rowClassRules: any;
  summaryContext: any;

  conditionRecommendation: ConditionRecommendation = {
    detOutcomeID: null,
    pactApplicationID: null, isSufficientlyStabilized: null, isSMI: null,
    isNYC3: null, isNYC1515: null, isESSHI: null, isNYC3PopEApproved: null, isAnyPopulationApproved : null,
    conditionOrRecommendationExists: null, letterComment: null, detDeterminationLevelApproved: null, housingTypes: null,
    detDeterminationConditionRecommendation: null, userID: null
  };
  isSMI: boolean;
  isNYC3: boolean;
  isNYC1515: boolean;
  isESSHI: boolean;
  isNYC3PopEApproved: boolean;
  isAnyPopulationApproved: boolean;
  isSufficientlyStabilized: boolean;
  IsConditionExist: boolean;
  letterComment: string;
  detDeterminationLevelApproved: DETDeterminationLevelApproved[];
  housingTypes: HousingTypes[];
  conditionRecommendationGroup: FormGroup;
  conditionRecommendationRowData: DETDeterminationConditionRecommendation[];
  onlyRecommendationRowData: DETDeterminationConditionRecommendation[];
  detDeterminationConditionRecommendation: DETDeterminationConditionRecommendation[];
  isConditionOrRecommendationExistsDisabled: boolean = false;

  determinationLetterGuid: string;
  determinationLetterURL: SafeResourceUrl = null;
  reportParams: PACTReportUrlParams;
  reportParameters: PACTReportParameters[] = [];

  //Constructor
  constructor(
    private route: ActivatedRoute
    , private router: Router
    , private applicationDeterminationService: ApplicationDeterminationService
    , private userService: UserService
    , private determinationSummaryService: DeterminationSummaryService
    , private commonService: CommonService
    , private formBuilder: FormBuilder
    , private toastrService: ToastrService
    , private determinationSideNavService: DeterminationSideNavService
    , private sanitizer: DomSanitizer

  ) { }

  //On Init
  ngOnInit() {
    //Form Control
    this.conditionRecommendationGroup = this.formBuilder.group({
      isSMI: [''],
      isNYC3: [''],
      isNYC1515: [''],
      isSufficientlyStabilized: [''],
      conditionOrRecommendationExists: [''],
      letterComment: [''],
    });

    // //User Data
    // this.userDataSub = this.userService.getUserData().subscribe(res => {
    //   if (res) {
    //     this.userData = res;
    //   }
    // });

    // //Side Navigation Service For Determination
    // this.activatedRouteSub = this.route.paramMap.subscribe(param => {
    //   if (param) {
    //     const paramApplicationId = param.get('applicationId');
    //     if (paramApplicationId) {
    //       const numericApplicationId = parseInt(paramApplicationId);
    //       if (isNaN(numericApplicationId)) {
    //         throw new Error("Invalid application number!");
    //       } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
    //         this.applicationDeterminationService.setApplicationDataForDeterminationWithApplicationId(numericApplicationId)
    //           .subscribe(data => {
    //             //console.log('Data - ', data);
    //             this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
    //             this.applicationDeterminationData = data as iApplicationDeterminationData;
    //             this.applicationDeterminationService.setApplicationDeterminationData(this.applicationDeterminationData);
    //             this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);

    //             if (this.applicationDeterminationData !== null)
    //               this.InitSummary();
    //           },
    //             error => {
    //               throw new Error("There is an error while getting Application Determination Data.");
    //             });
    //       }
    //       else {
    //         return;
    //       }
    //     }
    //   }
    // });

    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
            //Get User Data
            this.userDataSub = this.userService.getUserData().subscribe(res => {
              if (res) {
                this.userData = res;
                //Getting the application details for determination from ApplicationDeterminationService
                this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                  if (res) {
                    const data = res as iApplicationDeterminationData;
                    if (data) {
                      this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
                      this.applicationDeterminationData = data;
                      if (this.applicationDeterminationData) {
                        this.InitSummary();
                      }
                    }
                  }
                });
              }
            });
          }
          else {
            return;
          }
        }
      }
    });


  }

  InitSummary() {
    // //User Data
    // this.userDataSub = this.userService.getUserData().subscribe(res => {
    //   if (res) {
    //     this.userData = res;
    //   }
    // });
    this.pactApplicationId = this.applicationDeterminationData.pactApplicationId;

    //Enable Pending Review on Page Load
    this.determinationSideNavService.enablePendingReview(this.applicationDeterminationData);
    this.determinationSideNavService.setIsDeterminationSummaryDisabled(false);

    //Summary Grid Properties
    this.summaryGridOptions = {
      rowHeight: 30
    } as GridOptions;

    this.summaryColumnDefs = [
      {
        headerName: 'AgreementPopulationID',
        field: 'agreementPopulationID',
        hide: true
      },
      {
        headerName: 'Agreement Type',
        field: 'agreementTypeName',
        width: 300,
        cellStyle: {
          'white-space': 'normal'
        },
      },
      {
        headerName: 'Applied for Population',
        field: 'appliedForPopulationDescription',
        width: 160,
      },
      {
        headerName: 'Clinical Eligibility',
        field: 'clinicalEligibilityDescription',
        width: 130,
      },
      {
        headerName: 'Homeless Eligibility',
        field: 'homelessEligibilityDescription',
        width: 130,
      },
      {
        headerName: 'Final Eligibility',
        field: 'finalEligibility',
        hide: true
      },
      {
        headerName: 'Final Eligibility',
        field: 'finalEligibilityDescription',
        width: 130,
      },
      {
        headerName: 'Medicaid Redesign Team',
        field: 'mrtDescription',
        width: 180,
      }

    ];
    this.rowClassRules = {
      'green-background': function (params) {
        return params.data.finalEligibility === 831 ? true : false;
      },
    };

    this.summaryDefaultColDef = {
      filter: false,
      sortable: false,
      resizable: false,
      suppressMenu: true,
    };
    this.summaryContext = { componentParent: this };

  }

  //Tab Changes Event
  //tabChanged(tabChangeEvent: MatTabChangeEvent): void {
  tabChanged() {
    //console.log('Tab Index - ', tabChangeEvent.index);
    //this.selectedTab = tabChangeEvent.index;
    // console.log('Curent Tab - ', this.currentTab);
    // console.log('Selected Tab - ', this.selectedTab);

    switch (this.currentTab) {
      case 0:
        this.saveTabTimeDuration(this.currentTab);
        this.currentTab = this.selectedTab;
        this.isSaveVisible = this.selectedTab === 1 ? true : false;
        this.loadTabData();
        break;
      case 1:
        if (this.validateConditionRecommendation()) {
          this.saveConditionRecommendation(0);
          //this.saveTabTimeDuration(this.currentTab);
          this.currentTab = this.selectedTab;
          this.isSaveVisible = this.selectedTab === 1 ? true : false;
          this.loadTabData();
        }
        else {
          this.selectedTab = 1;
        }
        break;
      case 2:
        this.saveTabTimeDuration(this.currentTab);
        this.isSaveVisible = this.selectedTab === 1 ? true : false;
        this.currentTab = this.selectedTab;
        this.loadTabData();
        break;
    }
  }

  //Load Tab Data
  loadTabData() {
    if (this.selectedTab === 0)
      this.getSummaryData();
    else if (this.selectedTab === 1)
      this.getConditionRecommendationInit();
    else if (this.selectedTab === 2)
      this.getDeterminationLetter();
  }

  //Next Button Click
  nextTab() {
    //this.saveTabTimeDuration();
    this.saveTabData(1);
  }

  //Previous Button Click
  previousTab() {
    switch (this.selectedTab) {
      case 0:
        this.saveTabTimeDuration(this.currentTab);
        this.currentTab = this.selectedTab;
        if (this.applicationDeterminationData.isVulnerabilityAssessmentTabCompleted !== null)
          this.router.navigate(['/ds/vulnerability-assessment/', this.pactApplicationId]);
        else if (this.applicationDeterminationData.isMedicaidPrioritizationTabCompleted !== null)
          this.router.navigate(['/ds/medicaid-prioritization/', this.pactApplicationId]);
        else if (this.applicationDeterminationData.isHomeless1To4YearsTabCompleted !== null)
          this.router.navigate(['/ds/homeless-review/', this.pactApplicationId]);
        else if (this.applicationDeterminationData.isMentalHealthConditionTabCompleted !== null)
          this.router.navigate(['/ds/clinical-review/', this.pactApplicationId]);
        break;
      case 1:
        this.isSaveVisible = true;
        let isCompleted = this.validateConditionRecommendation();
        if (isCompleted) {
          this.saveConditionRecommendation(1);
          this.selectedTab = this.selectedTab - 1;
          this.currentTab = this.selectedTab;
        }
        break;
      case 2:
        this.saveTabTimeDuration(this.currentTab);
        this.selectedTab = this.selectedTab - 1;
        this.currentTab = this.selectedTab;
        this.isSaveVisible = false;
        break;

    }
  }

  // Value - 0 For Save, 1 For Save And Previous / Next
  saveTabData(value: number) {
    switch (this.currentTab) {
      case 0:
        this.isSaveVisible = false;
        if (value === 1) {
          this.saveTabTimeDuration(this.currentTab);
          this.selectedTab = this.selectedTab + 1;
          //this.currentTab = this.selectedTab;
        }
        break;
      case 1:
        this.isSaveVisible = true;
        let isCompleted = this.validateConditionRecommendation();
        if (isCompleted) {
          this.saveConditionRecommendation(value);
          if (value === 1) {
            //this.saveTabTimeDuration(this.currentTab);
            this.selectedTab = this.selectedTab + 1;
            //this.currentTab = this.selectedTab;
          }
        }
        break;
      case 2:
        this.isSaveVisible = false;
        if (value === 1) {
          this.saveTabTimeDuration(this.currentTab);
          this.determinationSideNavService.setIsSignOffFollowUpDisabled(false);
          this.router.navigate(['/ds/sign-off-follow-up', this.pactApplicationId]);
        }
        break;
    }
  }

  //Save Tab Time Duration
  saveTabTimeDuration(tabIndex: number) {
    let tabOutName: string;
    //let tabInName: string;

    if (tabIndex === 0) {
      tabOutName = "DeterminationSummary"
    }
    else if (tabIndex === 1) {
      tabOutName = "ConditionsRecommendations";
    }
    else if (tabIndex === 2) {
      tabOutName = "DeterminationLetter";
    }
    //Tab TimeIn
    // if (this.selectedTab === 0) {
    //   tabInName = "DeterminationSummary"
    // }
    // else if (this.selectedTab === 1) {
    //   tabInName = "ConditionsRecommendations";
    // }
    // else if (this.selectedTab === 2) {
    //   tabInName = "DeterminationLetter";
    // }

    if (this.applicationDeterminationData !== null) {
      this.detTabTimeDuration.pactApplicationId = this.pactApplicationId;
      this.detTabTimeDuration.tabOutName = tabOutName;
      //this.detTabTimeDuration.tabInName = tabInName;
      this.detTabTimeDuration.optionUserId = this.userData.optionUserId;
      //console.log('detTabTimeDuration - ', this.detTabTimeDuration);
      this.commonService.saveTabTimeDuration(this.detTabTimeDuration).subscribe();
    }
  }

  //#region Summary Tab 1
  onSummaryGridReady = (params: { api: any; columnApi: any }) => {

    /** API call to get the grid data */
    if (this.applicationDeterminationData !== null)
      this.getSummaryData();

    params.api.setDomLayout('autoHeight');
    params.api.sizeColumnsToFit();
  }

  getSummaryData() {
    // this.detSummaryInput = {
    //   pactApplicationID: this.applicationDeterminationData.pactApplicationId
    //   , userID: this.userData.optionUserId
    // };

    this.detSummaryInput.pactApplicationID = this.pactApplicationId;
    this.detSummaryInput.userID = this.userData.optionUserId;

    //console.log('DetSummaryInput - ',this.detSummaryInput);  
    this.determinationSummaryService.getDeterminationSummary(this.detSummaryInput)
      .subscribe(
        res => {
          //console.log('Summary Data -', res);
          this.detSummary = res;
          this.vulnerabilityTypeDescription = this.detSummary.vulnerabilityTypeDescription;
          this.hudTypeDescription = this.detSummary.hudTypeDescription;
          this.reviewerName = this.detSummary.reviewerName;
          this.determinationDate = this.detSummary.determinationDate;
          this.approvalFrom = this.detSummary.approvalFrom;
          this.approvalTo = this.detSummary.approvalTo;
          this.summaryRowData = this.detSummary.detDeterminationPopulationLevel;
        },
        error => {
          throw new Error(error.message);
        }
      );
  }

  //#endregion

  //#region Condition Recommendation Tab 2
  getConditionRecommendationInit() {
    // this.detSummaryInput = {
    //   pactApplicationID: this.applicationDeterminationData.pactApplicationId
    //   , userID: this.userData.optionUserId
    // };
    this.detSummaryInput.pactApplicationID = this.pactApplicationId;
    this.detSummaryInput.userID = this.userData.optionUserId;

    this.determinationSummaryService.getDeterminationConditionRecommendation(this.detSummaryInput)
      .subscribe(
        res => {
          //console.log('Condition Data -', res);
          if (res) {
            this.conditionRecommendation = res;
            this.isSMI = this.conditionRecommendation.isSMI;
            this.isNYC3 = this.conditionRecommendation.isNYC3;
            this.isNYC1515 = this.conditionRecommendation.isNYC1515;
            this.isESSHI = this.conditionRecommendation.isESSHI;
            this.isNYC3PopEApproved = this.conditionRecommendation.isNYC3PopEApproved;
            this.isAnyPopulationApproved = this.conditionRecommendation.isAnyPopulationApproved;
            this.isSufficientlyStabilized = this.conditionRecommendation.isSufficientlyStabilized;
            this.IsConditionExist = this.conditionRecommendation.conditionOrRecommendationExists === 33 ? true : false;
            this.detDeterminationLevelApproved = this.conditionRecommendation.detDeterminationLevelApproved;
            this.housingTypes = this.conditionRecommendation.housingTypes;
            this.detDeterminationConditionRecommendation = this.conditionRecommendation.detDeterminationConditionRecommendation;

            if (this.detDeterminationConditionRecommendation) {
              this.conditionRecommendationRowData = this.detDeterminationConditionRecommendation.filter(d => d.refGroupID === 109);
              this.onlyRecommendationRowData = this.detDeterminationConditionRecommendation.filter(d => d.refGroupID === 110);
            }

            this.populateConditionRecommendation(this.conditionRecommendation);
          }
        },
        error => {
          throw new Error(error.message);
        }
      );
  }

  populateConditionRecommendation(formData: ConditionRecommendation) {
    if (formData != null) {
      if (formData.conditionOrRecommendationExists !== null) {
        this.conditionRecommendationGroup.controls['conditionOrRecommendationExists'].setValue(formData.conditionOrRecommendationExists === 33 ? 'Y' : 'N');
      }
      else {
        this.conditionRecommendationGroup.controls['conditionOrRecommendationExists'].setValue(null);
      }

      //Disable Condition REcommendation for isNYC3PopEApproved 
      if(this.isNYC3PopEApproved)
      {
        this.conditionRecommendationGroup.controls['conditionOrRecommendationExists'].disable();
        this.isConditionOrRecommendationExistsDisabled = true;
        this.IsConditionExist = true;
      }
      else
      {
        this.conditionRecommendationGroup.controls['conditionOrRecommendationExists'].enable();
        this.isConditionOrRecommendationExistsDisabled = false;
        if (this.isAnyPopulationApproved)
          this.IsConditionExist = true;
        else
          this.IsConditionExist = false;
      }
      
      if (formData.letterComment !== null) {
        this.conditionRecommendationGroup.controls['letterComment'].setValue(formData.letterComment);
      }
      else {
        this.conditionRecommendationGroup.controls['letterComment'].setValue(null);
      }

    }
    else {
      this.conditionRecommendationGroup.controls['conditionOrRecommendationExists'].setValue(null);
      this.conditionRecommendationGroup.controls['letterComment'].setValue(null);
    }
  }



  //Levels of Approved For SMI CheckBox Change Event
  levelApprovedForSMIChange(event: { checked: boolean; }, item: DETDeterminationLevelApproved) {
    let index = this.detDeterminationLevelApproved.indexOf(item);
    item.isChecked = event.checked;
    this.detDeterminationLevelApproved[index] = item;
    this.set24HourSuperVisionVisibility();
  }

  // 24 Hour Supervision - 852 visibility logic
  set24HourSuperVisionVisibility() {
    let elementCommunityCare = this.detDeterminationLevelApproved.find(x => x.levelOfCareType === 849);
    let elementLevel2 = this.detDeterminationLevelApproved.find(x => x.levelOfCareType === 851);

    let element24HourSupervision = this.detDeterminationLevelApproved.find(x => x.levelOfCareType === 852);
    let index = this.detDeterminationLevelApproved.indexOf(element24HourSupervision);

    if (elementCommunityCare.isChecked) {
      element24HourSupervision.isVisible = false;
      element24HourSupervision.isChecked = false;
    }
    else {
      element24HourSupervision.isVisible = true;

      if (!elementLevel2.isChecked)
        element24HourSupervision.isChecked = false;
    }

    this.detDeterminationLevelApproved[index] = element24HourSupervision;
  }

  //Levels of Approved For SMI CheckBox Change Event
  housingTypeChange(event: { checked: boolean; }, item: HousingTypes) {
    let index = this.housingTypes.indexOf(item);
    item.isChecked = event.checked;
    this.housingTypes[index] = item;
  }

  conditionOrRecommendationExistsSelected(selectedValue: string) {
    if (selectedValue === 'Y') {
      this.IsConditionExist = true;

      this.detDeterminationConditionRecommendation.forEach((item) => {
        if (this.isNYC3PopEApproved && item.conditionRecommendationType === 854) {
          item.isCondition = true;
          item.isRecommendation = false;
        }
      });

      this.conditionRecommendationRowData.forEach((item) => {
        if (this.isNYC3PopEApproved && item.conditionRecommendationType === 854) {
          item.isCondition = true;
          item.isRecommendation = false;
        }
      });
    }
    else {
      this.IsConditionExist = false;
      this.detDeterminationConditionRecommendation.forEach((item) => {
        item.isCondition = false;
        item.isRecommendation = false;
      });

      this.conditionRecommendationRowData.forEach((item) => {
        item.isCondition = false;
        item.isRecommendation = false;
      });

      this.onlyRecommendationRowData.forEach((item) => {
        item.isCondition = false;
        item.isRecommendation = false;
      });
    }
  }

  //Condition Recommendation Grid CheckBox Change Event
  conditionRecommendationCellChange(event: { checked: boolean; }, item: DETDeterminationConditionRecommendation, cellType: string, gridType: string) {
    if (cellType === 'C') {
      item.isCondition = event.checked;
      item.isRecommendation = null;
    }
    else {
      item.isRecommendation = event.checked;
      item.isCondition = null;
    }

    if (gridType === 'CR') {
      let index = this.conditionRecommendationRowData.indexOf(item);
      this.conditionRecommendationRowData[index] = item;
    }
    else {
      let index = this.onlyRecommendationRowData.indexOf(item);
      this.onlyRecommendationRowData[index] = item;
    }

    let index = this.detDeterminationConditionRecommendation.indexOf(item);
    this.detDeterminationConditionRecommendation[index] = item;

    //console.log(this.detDeterminationConditionRecommendation);

  }

  validateConditionRecommendation() {
    //console.log(this.isSufficientlyStabilized);

    if (this.isSMI || this.isNYC3 || this.isNYC1515 || this.isESSHI || this.isAnyPopulationApproved) {

      if (this.isSMI) {
        let items = this.detDeterminationLevelApproved.filter(x => x.isChecked === true);

        if (typeof items === 'undefined' || items.length <= 0) {
          this.callToastrService("Levels Approved for SH for the SMI  is required.");
          return false;
        }
      }

      if (this.isNYC3 || this.isNYC1515 || this.isESSHI) {
        let items = this.housingTypes.filter(x => x.isChecked === true);

        if (typeof items === 'undefined' || items.length <= 0) {
          this.callToastrService("Housing Type is required.");
          return false;
        }
      }

      // if (this.isNYC3 || this.isNYC1515) {
      if (this.isAnyPopulationApproved) {  
        var conditionOrRecommendationExists = this.conditionRecommendationGroup.get('conditionOrRecommendationExists').value;
        if (conditionOrRecommendationExists === null) {
          this.callToastrService("Conditions of Approval and/or Recommendations is required.");
          return false;
        }
        else {
          if (conditionOrRecommendationExists === 'Y') {
            let items = this.detDeterminationConditionRecommendation.filter(x => x.isCondition === true || x.isRecommendation === true);

            if (typeof items === 'undefined' || items.length <= 0) {
              this.callToastrService("At least 1 Condition or Recommendation is required.");
              return false;
            }
          }
        }
      }

      var letterComment = this.conditionRecommendationGroup.get('letterComment').value;
      // if (letterComment === null) {
      //   this.callToastrService("Additional Letter Comments is required.");
      //   return false;
      // }
      // else if (letterComment !== null && letterComment.trim().length < 2)
      // {
      //   this.callToastrService("Additional Letter Comments should be at least 2 character long.");
      //   return false;
      // }
      //console.log('letterComment - ', letterComment);
      if ((letterComment !== null && letterComment !== '') && letterComment.trim().length < 2) {
        this.callToastrService("Additional Letter Comments should be at least 2 character long.");
        return false;
      }

      this.conditionRecommendationGroup.controls.letterComment.clearValidators();
      this.conditionRecommendationGroup.controls.letterComment.updateValueAndValidity();

    }


    return true;
  }


  saveConditionRecommendation(value: number) {
    // if (this.isNYC3 || this.isNYC1515) {
    if (this.isAnyPopulationApproved) {
      if (this.conditionRecommendationGroup.get('conditionOrRecommendationExists').value === 'Y')
        this.conditionRecommendation.conditionOrRecommendationExists = 33;
      else if (this.conditionRecommendationGroup.get('conditionOrRecommendationExists').value === 'N')
        this.conditionRecommendation.conditionOrRecommendationExists = 34;
      else
        this.conditionRecommendation.conditionOrRecommendationExists = null;
    }
    else
      this.conditionRecommendation.conditionOrRecommendationExists = null;

    if (this.conditionRecommendationGroup.get('letterComment').value !== null)
      this.conditionRecommendation.letterComment = this.conditionRecommendationGroup.get('letterComment').value;
    else
      this.conditionRecommendation.letterComment = null;

    this.conditionRecommendation.isSMI = this.isSMI;
    this.conditionRecommendation.pactApplicationID = this.pactApplicationId;
    this.conditionRecommendation.isNYC3 = this.isNYC3;
    this.conditionRecommendation.isNYC1515 = this.isNYC1515;
    this.conditionRecommendation.isESSHI = this.isESSHI;
    this.conditionRecommendation.isNYC3PopEApproved = this.isNYC3PopEApproved;
    this.conditionRecommendation.isAnyPopulationApproved = this.isAnyPopulationApproved;
    this.conditionRecommendation.isSufficientlyStabilized = this.isSufficientlyStabilized;
    this.conditionRecommendation.detDeterminationLevelApproved = this.detDeterminationLevelApproved;
    this.conditionRecommendation.housingTypes = this.housingTypes;
    this.conditionRecommendation.detDeterminationConditionRecommendation = this.detDeterminationConditionRecommendation;
    this.conditionRecommendation.userID = this.userData.optionUserId;

    //console.log('conditionRecommendation - ', this.conditionRecommendation);

    this.determinationSummaryService.saveDeterminationConditionRecommendation(this.conditionRecommendation)
      .subscribe(
        res => {
          if (value === 0) {
            if (!this.toastrService.currentlyActive)
              this.toastrService.success("Conditions OR Recommendations Data has been saved.");
          }
        },
        error => {
          throw new Error(error.message);
        }
      );
  }



  //#endregion

  //#region Determination Letter Tab 3

  getDeterminationLetter() {
    if (!this.determinationLetterGuid) {
      this.reportParameters = [
        { parameterName: 'applicationID', parameterValue: this.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
        { parameterName: 'reportName', parameterValue: "DeterminationLetter", CreatedBy: this.userData.optionUserId }
      ];
      this.commonService.generateGUID(this.reportParameters)
        .subscribe(
          res => {
            if (res) {
              this.determinationLetterGuid = res;
              this.generateReport(res, "DeterminationLetter");
            }
          }
        );
    }
    else {
      this.generateReport(this.determinationLetterGuid, "DeterminationLetter");
    }
  }

  //Generate Report
  generateReport(reportGuid: string, reportName: string) {
    this.reportParams = { reportParameterID: reportGuid, reportName: reportName, "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            this.determinationLetterURL = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(data));
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }

  //#endregion

  //Call Toaster Service Message
  callToastrService(message: string) {
    if (!this.toastrService.currentlyActive)
      this.toastrService.error(message);
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.saveTabData(0);
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }


}
