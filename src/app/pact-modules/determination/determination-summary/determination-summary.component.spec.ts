import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeterminationSummaryComponent } from './determination-summary.component';

describe('DeterminationSummaryComponent', () => {
  let component: DeterminationSummaryComponent;
  let fixture: ComponentFixture<DeterminationSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeterminationSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeterminationSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
