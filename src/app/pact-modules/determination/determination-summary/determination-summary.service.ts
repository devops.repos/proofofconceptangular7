import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

//Model References
import { DETSummaryInput, ConditionRecommendation } from './determination-summary.model';



@Injectable({
  providedIn: 'root'
})
export class DeterminationSummaryService {
  getDeterminationSummaryUrl = environment.pactApiUrl + 'DETSummary/GetDeterminationSummary';
  getDeterminationConditionRecommendationUrl = environment.pactApiUrl + 'DETSummary/GetDeterminationConditionRecommendation';
  saveDeterminationConditionRecommendationUrl = environment.pactApiUrl + 'DETSummary/SaveDeterminationConditionRecommendation';
    
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),
    response : "json",
  };
  
  constructor(private httpClient: HttpClient) { }

  //Get Determination Summary
  getDeterminationSummary(detSummaryInput : DETSummaryInput) : Observable<any>
  {
     return this.httpClient.post(this.getDeterminationSummaryUrl, JSON.stringify(detSummaryInput), this.httpOptions);
  }

  //Get Condition Recommendation
  getDeterminationConditionRecommendation(detSummaryInput : DETSummaryInput) : Observable<any>
  {
     return this.httpClient.post(this.getDeterminationConditionRecommendationUrl, JSON.stringify(detSummaryInput), this.httpOptions);
  }

  //Save Condition Recommendation
  saveDeterminationConditionRecommendation(conditionRecommendation : ConditionRecommendation) : Observable<any>
  {
     return this.httpClient.post(this.saveDeterminationConditionRecommendationUrl, JSON.stringify(conditionRecommendation), this.httpOptions);
  }

  

 
}

