import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { HttpEventType } from '@angular/common/http';
import { take } from 'rxjs/operators';

//Services
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { DocumentService } from 'src/app/shared/document/document.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { iDocumentPackage, iDocumentLink } from 'src/app/models/document-package.model'
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { DETTabTimeDuration } from 'src/app/models/determination-common.model';


@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})

export class ApplicationComponent implements OnInit, OnDestroy {
  //Global Varaibles
  tabSelectedIndex: number = 0;
  currentTabIndex: number = 0;

  detTabTimeDuration: DETTabTimeDuration = {
    pactApplicationId: null,
    tabInName: null,
    tabOutName: null,
    optionUserId: null
  };

  activatedRouteSub: Subscription;
  userDataSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  documentPackageDataSub: Subscription;

  userData: AuthData;
  applicationDeterminationData: iApplicationDeterminationData;
  documentPackage: iDocumentPackage[];
  applicationSummaryReportData: iDocumentPackage;

  reportParams: PACTReportUrlParams;
  reportParameters: PACTReportParameters[] = [];
  applicationSummaryReportURL: SafeResourceUrl = null;

  determinationLetterGuid: string;
  applicationSummaryGuid: string;
  mentalHealthReportGuid: string;
  psychiarticEvalReportGuid: string;
  psychosocialEvalReportGuid: string;
  vulnerabilityAssessmentReportGuid: string;
  housingHomelessGuid: string;
  determinationSummaryReportGuid: string;

  supportingDocumentsColumnDefs = [];
  supportingDocumentsRowData = [];
  supportingDocumentsDefaultColDef = {};
  supportingDocumentsOverlayNoRowsTemplate: string;

  //Constructor
  constructor(private route: ActivatedRoute,
    private userService: UserService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private sanitizer: DomSanitizer,
    private documentService: DocumentService,
    private message: ToastrService,
    private router: Router,
    private determinationSideNavService: DeterminationSideNavService) {
    //Supporting Documents Grid Column Definitions
    this.supportingDocumentsColumnDefs = [
      { headerName: 'Document Type', field: 'documentTypeDescription', filter: 'agTextColumnFilter', width: 250 },
      {
        headerName: 'Document Name',
        field: 'documentName',
        cellRenderer: (params: { value: string; data: { pactDocumentID: number; documentType: number; documentExtension: string; fileNetDocID: number; capsReportID: string }; }) => {
          var link = document.createElement('a');
          link.href = '#';
          link.innerText = params.value;
          link.addEventListener('click', (e) => {
            e.preventDefault();
            this.commonService.setIsOverlay(true);
            this.openDocument(params.data.pactDocumentID, params.data.documentType, params.data.documentExtension, params.data.fileNetDocID, params.data.capsReportID);
          });
          return link;
        },
        filter: 'agTextColumnFilter',
        width: 220
      },
      { headerName: 'Description', field: 'documentDescription', filter: 'agTextColumnFilter', width: 250 },
      { headerName: 'Clinician', field: 'clinicianName', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Clinician License #', field: 'clincianLicenseNumber', filter: 'agTextColumnFilter', width: 150 },
      { headerName: 'Collaborating MD', field: 'colloborationMDName', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Collaborating MD License #', field: 'colloborationMDLicenseNumber', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Attached Date-Time', field: 'createdDateTime', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Attached By', field: 'createdByName', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Agency/Site', field: 'agencySiteName', filter: 'agTextColumnFilter', width: 250 }
    ];
    this.supportingDocumentsDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
          }
          else {
            return;
          }
        }
      }
    });

    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });

    //Getting the application details for determination from ApplicationDeterminationService
    this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData()
      .pipe(take(1))
      .subscribe(res => {
        if (res) {
          const data = res as iApplicationDeterminationData;
          if (data) {
            this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
            this.applicationDeterminationData = data;
            //Get Document Package  
            if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
              this.saveTabTimeDuration(this.applicationDeterminationData.pactApplicationId, null, "Application", this.userData.optionUserId);
              this.documentPackageDataSub = this.commonService.getDocumentPackage(this.applicationDeterminationData.pactApplicationId)
                .subscribe(result => {
                  if (result) {
                    this.documentPackage = result as iDocumentPackage[];
                    if (this.documentPackage) {
                      this.applicationSummaryReportData = this.documentPackage.find(d => { return d.documentType === 327 });
                      //Open Document
                      if (this.applicationSummaryReportData) {
                        this.openDocument(this.applicationSummaryReportData.pactDocumentID, this.applicationSummaryReportData.documentType, this.applicationSummaryReportData.documentExtension, this.applicationSummaryReportData.fileNetDocID, this.applicationSummaryReportData.capsReportID);
                      }
                      else {
                        if (!this.message.currentlyActive) {
                          this.message.error("Missing Application Summary Report");
                        }
                      }
                    }
                  }
                });
            }
          }
        }
      });
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.setTabTimeDuration();
      this.applicationDeterminationDataSub.unsubscribe();
    }
    if (this.documentPackageDataSub) {
      this.documentPackageDataSub.unsubscribe();
    }
  }

  //Set Tab Values For Tab Time Duration
  setTabTimeDuration() {
    let tabOutName: string;
    let tabInName: string;
    //Current Tab Index && TabOutName
    if (this.currentTabIndex === 0) {
      tabOutName = "Application"
    }
    else if (this.currentTabIndex === 1) {
      tabOutName = "SupportingDocuments";
    }
    //Selected Tab Index && TabInName
    if (this.tabSelectedIndex === 0) {
      tabInName = "Application";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    else if (this.tabSelectedIndex === 1) {
      tabInName = "SupportingDocuments";
      this.currentTabIndex = this.tabSelectedIndex;
    }
    if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId) {
      this.saveTabTimeDuration(this.applicationDeterminationData.pactApplicationId, tabOutName, tabInName, this.userData.optionUserId);
    }
  }

  //Save Tab Time Duration
  saveTabTimeDuration(pactApplicationId: number, tabOutName: string, tabInName: string, optionUserId: number) {
    this.detTabTimeDuration.pactApplicationId = pactApplicationId;
    this.detTabTimeDuration.tabOutName = tabOutName;
    this.detTabTimeDuration.tabInName = tabInName;
    this.detTabTimeDuration.optionUserId = optionUserId;
    this.commonService.saveTabTimeDuration(this.detTabTimeDuration).subscribe();
  }

  //Application Tab Change Event
  applicationTabChange() {
    this.setTabTimeDuration();
  }

  //On Grid Ready
  supportingDocumentsOnGridReady(params: { api: { setDomLayout: (arg0: string) => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.supportingDocumentsOverlayNoRowsTemplate = '<span style="color: #337ab7">No Documents To Show</span>';
    if (this.documentPackage) {
      this.supportingDocumentsRowData = this.documentPackage.filter(d => { return d.documentType !== 326 && d.documentType !== 332 && d.documentType !== 334 && d.documentType !== 874 });
    }
  }

  //Open Document
  openDocument(pactDocumentId: number, documentType: number, documentExtension: string, fileNetDocID: number, capsReportID: string) {
    //File Net
    if (fileNetDocID && this.tabSelectedIndex === 0) {
      this.commonService.getFileNetDocumentLink(fileNetDocID.toString()).subscribe(res => {
        const data = res as iDocumentLink;
        if (data && data.linkURL) {
          this.applicationSummaryReportURL = this.sanitizer.bypassSecurityTrustResourceUrl(data.linkURL);
        }
      });
    } else if (fileNetDocID && this.tabSelectedIndex === 1) {
      this.commonService.getFileNetDocumentLink(fileNetDocID.toString()).subscribe(res => {
        const data = res as iDocumentLink;
        if (data && data.linkURL) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    }
    else {
      //SSRS
      if (documentType === 331) {
        if (capsReportID) {
          this.commonService.displaySurveySummaryReport(capsReportID, this.userData.optionUserId);
        }
        else {
          this.commonService.setIsOverlay(false);
          if (!this.message.currentlyActive) {
            this.message.error("There was an error in opening the document.", "Viewing Document Failed!");
          }
        }
      }
      else if (documentType === 326 || documentType === 327 || documentType === 328 || documentType === 329 || documentType === 330 || documentType === 332 || documentType === 334 || documentType === 874) {
        this.viewSSRSDocument(documentType);
      }
      else {
        //Storage MTC
        if (pactDocumentId && documentExtension) {
          var docName = pactDocumentId.toString() + "." + documentExtension;
          this.documentService.viewDocument(docName).subscribe(
            data => {
              switch (data.type) {
                case HttpEventType.Response:
                  const downloadedFile = new Blob([data.body], {
                    type: data.body.type
                  });
                  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(downloadedFile, docName);
                  } else {
                    this.commonService.OpenWindow(URL.createObjectURL(downloadedFile));
                    this.commonService.setIsOverlay(false);
                  }
                  break;
              }
            }, error => {
              if (!this.message.currentlyActive) {
                this.message.error('There was an error in opening the document.', 'Viewing Document Failed!');
              }
            });
        }
      }
    }
  }

  //View SSRS Document
  viewSSRSDocument(documentType: number) {
    switch (documentType) {
      case 326: {
        if (!this.determinationLetterGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "DeterminationLetter", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.determinationLetterGuid = res;
                  this.generateReport(res, "DeterminationLetter");
                }
              }
            );
        }
        else {
          this.generateReport(this.determinationLetterGuid, "DeterminationLetter");
        }
        break;
      }
      case 327: {
        if (!this.applicationSummaryGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PACTReportApplicationSummary", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.applicationSummaryGuid = res;
                  this.generateReport(res, "PACTReportApplicationSummary");
                }
              }
            );
        }
        else {
          this.generateReport(this.applicationSummaryGuid, "PACTReportApplicationSummary");
        }
        break;
      }
      case 328: {
        if (!this.mentalHealthReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "MentalHealthReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.mentalHealthReportGuid = res;
                  this.generateReport(res, "MentalHealthReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.mentalHealthReportGuid, "MentalHealthReport");
        }
        break;
      }
      case 329: {
        if (!this.psychiarticEvalReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PsychiarticEvalReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.psychiarticEvalReportGuid = res;
                  this.generateReport(res, "PsychiarticEvalReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.psychiarticEvalReportGuid, "PsychiarticEvalReport");
        }
        break;
      }
      case 330: {
        if (!this.psychosocialEvalReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "PsychosocialEvalReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.psychosocialEvalReportGuid = res;
                  this.generateReport(res, "PsychosocialEvalReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.psychosocialEvalReportGuid, "PsychosocialEvalReport");
        }
        break;
      }
      case 332: {
        if (!this.vulnerabilityAssessmentReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "SVAReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.vulnerabilityAssessmentReportGuid = res;
                  this.generateReport(res, "SVAReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.vulnerabilityAssessmentReportGuid, "SVAReport");
        }
        break;
      }
      case 334: {
        if (!this.housingHomelessGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "HHHReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.housingHomelessGuid = res;
                  this.generateReport(res, "HHHReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.housingHomelessGuid, "HHHReport");
        }
        break;
      }
      case 874: {
        if (!this.determinationSummaryReportGuid) {
          this.reportParameters = [
            { parameterName: 'applicationID', parameterValue: this.applicationDeterminationData.pactApplicationId.toString(), CreatedBy: this.userData.optionUserId },
            { parameterName: 'reportName', parameterValue: "DeterminationSummaryReport", CreatedBy: this.userData.optionUserId }
          ];
          this.commonService.generateGUID(this.reportParameters)
            .subscribe(
              res => {
                if (res) {
                  this.determinationSummaryReportGuid = res;
                  this.generateReport(res, "DeterminationSummaryReport");
                }
              }
            );
        }
        else {
          this.generateReport(this.determinationSummaryReportGuid, "DeterminationSummaryReport");
        }
        break;
      }
      default: {
        break;
      }
    }
  }

  //Generate Report
  generateReport(reportGuid: string, reportName: string) {
    this.reportParams = { reportParameterID: reportGuid, reportName: reportName, reportFormat : "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          if (data.size > 512) {
            if (this.tabSelectedIndex === 0) {
              this.applicationSummaryReportURL = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(data));
            }
            else {
              this.commonService.OpenWindow(URL.createObjectURL(data));
              this.commonService.setIsOverlay(false);
            }
          }
        },
        error => {
          throw new error(error.message);
        }
      );
  }

  //Next Button Click
  nextTab() {
    if (this.tabSelectedIndex === 1) {
      this.router.navigate(['/ds/client-case-folder', this.applicationDeterminationData.pactApplicationId]);
    }
    else {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  //Previous Button Click
  previousTab() {
    if (this.tabSelectedIndex === 0) {
      this.router.navigate(['/ds//match-sources', this.applicationDeterminationData.pactApplicationId]);
    }
    else {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }
}
