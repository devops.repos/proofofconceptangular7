import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router} from '@angular/router';
import { FormGroup, FormBuilder} from "@angular/forms";
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { FollowUpReason, DETFollowUp } from './sign-off-follow-up.model';
import { SignOffFollowUpService } from './sign-off-follow-up.service';


@Component({
    selector: 'follow-up-reason',
    templateUrl: './follow-up-reason.component.html',
    styleUrls: ['./follow-up-reason.component.scss']
})

export class DetFollowUpReasonComponent {
    followUpReasons : FollowUpReason[];
    userData: AuthData;
    detFollowUp : DETFollowUp = { detFollowUpID : null, pactApplicationID : null, sequenceNumber : null,
        startDate : null, endDate : null, reasonID : null, reasonDescription : null, status : null, 
        statusDescription : null, comment : null, assignedTo : null, assignedToName : null, userID : null };
    followUpReasonGroup : FormGroup;
    
    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any
        , private dialogRef: MatDialogRef<DetFollowUpReasonComponent>
        , private formBuilder: FormBuilder
        , private userService: UserService
        , private signOffFollowUpService : SignOffFollowUpService
        
        ) { 
           if (data)
           {
             this.followUpReasons = data;
           }
       }

    ngOnInit() {

        //User Data
        this.userService.getUserData().subscribe(res => {
            if (res) {
              this.userData = res;
            }
        });

        this.followUpReasonGroup = this.formBuilder.group({
            comment: ['']
        });
    }

    saveFollowUp()
    {
        if (this.followUpReasonGroup.get('comment').value !== null)
        {
            this.detFollowUp.comment = this.followUpReasonGroup.get('comment').value;
        }
        
        this.detFollowUp.reasonID = this.followUpReasons[0].concatenatedReasons;
        this.detFollowUp.status = 876;
        this.detFollowUp.pactApplicationID = this.followUpReasons[0].pactApplicationID;
        this.detFollowUp.userID = this.userData.optionUserId;

        console.log(this.detFollowUp);

        this.signOffFollowUpService.saveFollowUp(this.detFollowUp)
        .subscribe(data => {
            this.dialogRef.close(true);
        },
        error => {
            throw new Error(error.message);
        });
    }

    //Close the dialog on close button
    closeDialog() {
        this.dialogRef.close(false);
    }
    
}