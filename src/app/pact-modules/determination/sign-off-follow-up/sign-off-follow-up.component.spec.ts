import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignOffFollowUpComponent } from './sign-off-follow-up.component';

describe('SignOffFollowUpComponent', () => {
  let component: SignOffFollowUpComponent;
  let fixture: ComponentFixture<SignOffFollowUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignOffFollowUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignOffFollowUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
