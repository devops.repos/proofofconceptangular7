export class DETSignOffFollowUpInput{
    pactApplicationID? : number;
    userID?: number;
}

export class FollowUpReason{
    ruleNumber? : number;
    pactApplicationID? : number;
    reasonID?: number;
    reasonDescription: string;
    concatenatedReasons: string;
}

export class DETFollowUp{
    detFollowUpID? : number;
    pactApplicationID? : number;
    sequenceNumber : number;
    startDate : string;
    endDate : string;
    reasonID : string;
    reasonDescription: string;
    status? : number;
    statusDescription? : number;
    comment : string;
    assignedTo? : number;
    assignedToName : string;
    userID?: number;
}

export class AssignedTo
{
    optionUserID? : number;
    optionUserName : string;
    roleID? : number;
    roleName : string;
}

export class DETSignOff{
    detSignoffID? : number;
    pactApplicationID? : number;
    sequenceNumber? : number;
    signOff? : boolean;
    signOffDate : string;
    signOffBy? : number;
    signOffByName: string;
    signOffUpdatedDate : string;
    signOffUpdatedBy? : number;
    signOffUpdatedByName: string;
    signOffReason : string;
    signOffComment : string;
    approvalFrom : string;
    approvalTo : string;
    determinationDate : string;
    userID?: number;
}