import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

//Model References
import { DETSignOffFollowUpInput, DETFollowUp, DETSignOff} from './sign-off-follow-up.model';



@Injectable({
  providedIn: 'root'
})
export class SignOffFollowUpService {
  getDeterminationFollowUpReasonsUrl = environment.pactApiUrl + 'DETSignOffFollowUp/GetDeterminationFollowUpReasons';
  getFollowUpAssignedToListUrl = environment.pactApiUrl + 'DETSignOffFollowUp/GetFollowUpAssignedToList';
  getFollowUpHistoryUrl = environment.pactApiUrl + 'DETSignOffFollowUp/GetFollowUpHistory';
  saveFollowUpUrl = environment.pactApiUrl + 'DETSignOffFollowUp/SaveFollowUp';
  getDETSignOffDetailsUrl = environment.pactApiUrl + 'DETSignOffFollowUp/GetDETSignOffDetails';
  saveSignOffUrl = environment.pactApiUrl + 'DETSignOffFollowUp/SaveSignOff';
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),
    response : "json",
  };
  
  constructor(private httpClient: HttpClient) { }

  //Get Followup Reason
  getDeterminationFollowUpReasons(detSignOffFollowUpInput : DETSignOffFollowUpInput) : Observable<any>
  {
     return this.httpClient.post(this.getDeterminationFollowUpReasonsUrl, JSON.stringify(detSignOffFollowUpInput), this.httpOptions);
  }

   //Save Follow Up
  saveFollowUp(detFollowUp : DETFollowUp) : Observable<any>
  {
      return this.httpClient.post(this.saveFollowUpUrl, JSON.stringify(detFollowUp), this.httpOptions);
  }

   //Get Assigned To List
  getFollowUpAssignedToList(detSignOffFollowUpInput : DETSignOffFollowUpInput) : Observable<any>
  {
     return this.httpClient.post(this.getFollowUpAssignedToListUrl, JSON.stringify(detSignOffFollowUpInput), this.httpOptions);
  }

  //Get Followup History
  getFollowUpHistory(detSignOffFollowUpInput : DETSignOffFollowUpInput) : Observable<any>
  {
     return this.httpClient.post(this.getFollowUpHistoryUrl, JSON.stringify(detSignOffFollowUpInput), this.httpOptions);
  }

  //Get Sign Off Details
  getDETSignOffDetails(detSignOffFollowUpInput : DETSignOffFollowUpInput) : Observable<any>
  {
     return this.httpClient.post(this.getDETSignOffDetailsUrl, JSON.stringify(detSignOffFollowUpInput), this.httpOptions);
  }

   //Save Follow Up
  saveSignOff(detSignOff : DETSignOff) : Observable<any>
  {
      return this.httpClient.post(this.saveSignOffUrl, JSON.stringify(detSignOff), this.httpOptions);
  }

  
 
}

