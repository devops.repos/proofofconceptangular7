import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { MatDialog, MatTabChangeEvent } from '@angular/material';
import { DetFollowUpReasonComponent } from './follow-up-reason.component';

//Ag-Grid References
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridOptions } from 'ag-grid-community';

//Models
import { AuthData } from 'src/app/models/auth-data.model';
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model';
import { iHousingDeterminationDocumentsData } from 'src/app/models/determination-documents.model';
import { DETSignOffFollowUpInput, FollowUpReason, AssignedTo, DETFollowUp, DETSignOff } from './sign-off-follow-up.model';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';

//Services
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { SignOffFollowUpService } from './sign-off-follow-up.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { MyWorklistService } from 'src/app/pact-modules/determination/my-worklist/my-worklist.service';

@Component({
  selector: 'app-sign-off-follow-up',
  templateUrl: './sign-off-follow-up.component.html',
  styleUrls: ['./sign-off-follow-up.component.scss'],
  providers: [DatePipe]
})

export class SignOffFollowUpComponent implements OnInit, OnDestroy {
  //Global Varaibles
  currentTab: number = 0;
  tabSelectedIndex: number = 0;
  activatedRouteSub: Subscription;
  applicationDeterminationDataSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;
  signOffGroup: FormGroup;
  followUpReasonGroup: FormGroup;
  detSignOffFollowUpInput: DETSignOffFollowUpInput = { pactApplicationID: null, userID: null };
  followUpReasons: FollowUpReason[];
  assignedToReviewer: AssignedTo[];
  assignedToSupervisor: AssignedTo[];
  assignedToSupervisorID: number;
  assignedToSupervisorName: string;
  detSignOff: DETSignOff = {
    detSignoffID: null, pactApplicationID: null, sequenceNumber: null,
    signOff: null, signOffBy: null, signOffByName: null, signOffDate: null,
    signOffUpdatedBy: null, signOffUpdatedByName: null, signOffUpdatedDate: null,
    signOffComment: null, signOffReason: null, approvalFrom: null, approvalTo: null, determinationDate: null, userID: null,
  };
  isSignOffTabCompleted: boolean;
  isFollowUpTabCompleted: boolean;
  isFollowUpTabVisible: boolean = false;

  //Documents 
  signOffDocuments: iHousingDeterminationDocumentsData = { pactApplicationId: null, documentType: [] };

  isSignOffDisabled: boolean = true;
  signOffBy: number;
  signOffByName: string;
  signOffDate: string;
  signOffUpdatedBy: number;
  signOffUpdatedByName: string;
  signOffUpdatedDate: string;
  approvalFrom: string;
  approvalTo: string;
  determinationDate: string;
  sequenceNumber: number;

  //Follow Up Prop
  isSignOffSelected: boolean = false;
  isFollowUpSelected: boolean = false;
  isFollowUpAction: boolean = true;
  signOffDropDownReasons: RefGroupDetails[];
  followUpDropDownReasons: RefGroupDetails[];
  responseToFollowUpDropDownReasons: RefGroupDetails[];
  isResponseToFollowUp: boolean = false;
  followUpStatus: number;
  detFollowUp: DETFollowUp = {
    detFollowUpID: null, pactApplicationID: null, sequenceNumber: null,
    startDate: null, endDate: null, reasonID: null, reasonDescription: null, status: null,
    statusDescription: null, comment: null, assignedTo: null, assignedToName: null, userID: null
  };

  determinationDecisionBy: number;
  determinationDecisionByName: string;
  dateOfDetermination: string;
  followUpBy: number;
  followUpByName: string;
  responseTofollowUpBy: number;
  responseTofollowUpByName: string;
  dateAssigned: string;
  dateOfResponse: string;
  followUpHistSequence1: DETFollowUp[];


  @ViewChild('agGrid') agGrid: AgGridAngular;
  gridApi: any;
  gridColumnApi: any;
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;
  context: any;
  columnDefs: any;
  rowSelection: any;
  defaultColDef: any;
  public gridOptions: GridOptions;
  rowData: DETFollowUp[];


  //Constructor
  constructor(private route: ActivatedRoute
    , private applicationDeterminationService: ApplicationDeterminationService
    , private userService: UserService
    , private formBuilder: FormBuilder
    , private datePipe: DatePipe
    , private signOffFollowUpService: SignOffFollowUpService
    , public dialog: MatDialog
    , private commonService: CommonService
    , private myWorklistService: MyWorklistService
    , private router: Router
    , private toastrService: ToastrService
  ) { }

  //On Init
  ngOnInit() {
    //Form Control
    this.signOffGroup = this.formBuilder.group({
      signOff: [''],
      signoffDate: [''],
      signoffBy: [''],
      signoffByName: [''],
      signoffUpdatedDate: [''],
      signoffUpdatedBy: [''],
      signoffUpdatedByName: [''],
      approvalFrom: [''],
      approvalTo: [''],
      determinationDate: [''],
    });

    this.followUpReasonGroup = this.formBuilder.group({
      followUpAction: [''],
      signOffReasonID: [''],
      signOffComment: [''],
      followUpReasonID: [''],
      followUpAssignedTo: [''],
      followUpComment: [''],
      responseToFollowUpReasonID: [''],
      responseToFollowUpAssignedTo: [''],
      responseToFollowUpComment: ['']
    });

    // //User Data
    // this.userDataSub = this.userService.getUserData().subscribe(res => {
    //   if (res) {
    //     this.userData = res;
    //   }
    // });

    // //Side Navigation Service For Determination
    // this.activatedRouteSub = this.route.paramMap.subscribe(param => {
    //   if (param) {
    //     const paramApplicationId = param.get('applicationId');
    //     if (paramApplicationId) {
    //       const numericApplicationId = parseInt(paramApplicationId);
    //       if (isNaN(numericApplicationId)) {
    //         throw new Error("Invalid application number!");
    //       } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
    //         this.applicationDeterminationService.setApplicationDataForDeterminationWithApplicationId(numericApplicationId)
    //           .subscribe(data => {
    //             //console.log('Data - ', data.caseAssignedTo);
    //             //this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
    //             this.applicationDeterminationData = data as iApplicationDeterminationData;
    //             this.applicationDeterminationService.setApplicationDeterminationData(this.applicationDeterminationData);
    //             this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);

    //             if (this.applicationDeterminationData)
    //               this.InitSignOff();
    //           },
    //             error => {
    //               throw new Error("There is an error while getting Application Determination Data.");
    //             });
    //       }
    //       else {
    //         return;
    //       }
    //     }
    //   }
    // });

    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
            //Get User Data
            this.userDataSub = this.userService.getUserData().subscribe(res => {
              if (res) {
                this.userData = res;
                //Getting the application details for determination from ApplicationDeterminationService
                this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                  if (res) {
                    const data = res as iApplicationDeterminationData;
                    if (data) {
                      this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
                      this.applicationDeterminationData = data;
                      if (this.applicationDeterminationData) {
                        this.InitSignOff();
                      }
                    }
                  }
                });
              }
            });
          }
          else {
            return;
          }
        }
      }
    });
  }

  //#region Sign Off Tab
  InitSignOff() {
    this.isSignOffTabCompleted = this.applicationDeterminationData.isSignOffTabCompleted;
    this.isFollowUpTabCompleted = this.applicationDeterminationData.isFollowUpTabCompleted;

    //Get Refgroup Details
    var refGroupList = "91,92,112";
    this.commonService.getRefGroupDetails(refGroupList)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.signOffDropDownReasons = data.filter(d => { return d.refGroupID === 91 });
          this.followUpDropDownReasons = data.filter(d => { return d.refGroupID === 92 });
          this.responseToFollowUpDropDownReasons = data.filter(d => { return d.refGroupID === 112 });
        },
        error => {
          throw new Error('There is an error while fetching Clinician Titles Data.');
        }
      );

    //Get Documents - 326 -  Determination Letter , 332 - SVA , 334 - Housing, 874 - Determination Summary
    if (this.applicationDeterminationData.pactApplicationId > 0) {
      this.signOffDocuments.pactApplicationId = this.applicationDeterminationData.pactApplicationId;
      this.signOffDocuments.documentType = [326, 874, 334, 332];

      this.detSignOffFollowUpInput = {
        pactApplicationID: this.applicationDeterminationData.pactApplicationId
        , userID: this.userData.optionUserId
      };
    }

    //Get Sign Off Details
    this.loadSignOffData();

  }

  loadSignOffData() {
    this.signOffFollowUpService.getDETSignOffDetails(this.detSignOffFollowUpInput)
      .subscribe(
        res => {
          this.detSignOff = res as DETSignOff;
          //console.log('detSignOff-', this.detSignOff);
          if (this.detSignOff) {
            if (this.applicationDeterminationData.approvalFrom != null)
              this.approvalFrom = this.datePipe.transform(this.applicationDeterminationData.approvalFrom, 'MM/dd/yyyy');
            else
              this.approvalFrom = null;

            if (this.applicationDeterminationData.approvalTo != null)
              this.approvalTo = this.datePipe.transform(this.applicationDeterminationData.approvalTo, 'MM/dd/yyyy');
            else
              this.approvalTo = null;

            if (this.applicationDeterminationData.determinationDate != null)
              this.determinationDate = this.datePipe.transform(this.applicationDeterminationData.determinationDate, 'MM/dd/yyyy');
            else
              this.determinationDate = null;

            this.sequenceNumber = this.detSignOff.sequenceNumber === null ? 0 : this.detSignOff.sequenceNumber;

            //this.detSignOff = res as DETSignOff;
            //console.log('detSignOff', this.detSignOff.detSignoffID);
            if (this.detSignOff.detSignoffID > 0) {
              this.populateSignOffData(this.detSignOff);
              // Just to Set the Follow Up Tab Visbility
              this.getFollowUpHistoryRowData();
            }
          }
        },
        error => {
          throw new Error(error.message);
        });
  }

  populateSignOffData(formData: DETSignOff) {
    //console.log('SignOff Data - ', formData);
    if (formData != null) {
      if (formData.signOff !== null)
        this.signOffGroup.controls['signOff'].setValue(formData.signOff);
      else
        this.signOffGroup.controls['signOff'].setValue(null);

      if (formData.signOffDate !== null)
        this.signOffDate = this.datePipe.transform(formData.signOffDate, 'MM/dd/yyyy');
      else
        this.signOffDate = null;

      if (formData.signOffBy !== null) {
        this.signOffByName = formData.signOffByName;
        this.signOffBy = formData.signOffBy;
      }
      else {
        this.signOffByName = null;
        this.signOffBy = null;
      }

      if (formData.signOffUpdatedDate !== null)
        this.signOffUpdatedDate = this.datePipe.transform(formData.signOffUpdatedDate, 'MM/dd/yyyy');
      else
        this.signOffUpdatedDate = null;

      if (formData.signOffUpdatedBy !== null) {
        this.signOffUpdatedByName = formData.signOffUpdatedByName;
        this.signOffUpdatedBy = formData.signOffUpdatedBy;
      }
      else {
        this.signOffUpdatedByName = null;
        this.signOffUpdatedBy = null;
      }

      if (this.isSignOffTabCompleted && this.detSignOff.signOff)
        this.disableSignOffForm();
    }
    else {
      this.signOffGroup.controls['signOff'].setValue(null);
      this.signOffDate = null;
      this.signOffBy = null;
      this.signOffByName = null;
      this.signOffUpdatedDate = null;
      this.signOffUpdatedBy = null;
      this.signOffUpdatedByName = null;
      this.approvalFrom = null;
      this.approvalTo = null;
    }
  }

  disableSignOffForm() {
    this.isSignOffDisabled = true;
    this.signOffGroup.controls['signOff'].disable();
  }

  //navigation Value - 0 - Sign Off, 1 - Follow Up
  saveSignOff(navigationValue: number) {
    if (this.signOffGroup.get('signOff').value != null)
      this.detSignOff.signOff = this.signOffGroup.get('signOff').value;
    else
      this.detSignOff.signOff = null;

    //Sign Off Details  
    if (this.signOffDate != null) {
      this.detSignOff.signOffDate = this.signOffDate;
      //this.detSignOff.determinationDate = this.signOffDate;
    }
    else {
      this.detSignOff.signOffDate = null;
      //this.detSignOff.determinationDate = null;
    }

    if (this.signOffByName != null)
      this.detSignOff.signOffBy = this.signOffBy;
    else
      this.detSignOff.signOffBy = null;

    //Updated Sign Off Details  
    if (this.signOffUpdatedDate != null) {
      this.detSignOff.signOffUpdatedDate = this.signOffUpdatedDate;
      //this.detSignOff.determinationDate = this.signOffUpdatedDate;
    }
    else {
      this.detSignOff.signOffUpdatedDate = null;
      //this.detSignOff.determinationDate = null;
    }

    if (this.signOffUpdatedByName != null)
      this.detSignOff.signOffUpdatedBy = this.signOffUpdatedBy;
    else
      this.detSignOff.signOffUpdatedBy = null;

    if (this.approvalFrom != null)
      this.detSignOff.approvalFrom = this.approvalFrom;
    else
      this.detSignOff.approvalFrom = null;

    if (this.approvalTo != null)
      this.detSignOff.approvalTo = this.approvalTo;
    else
      this.detSignOff.approvalTo = null;

    if (this.determinationDate != null)
      this.detSignOff.determinationDate = this.determinationDate;
    else
      this.detSignOff.determinationDate = null;  

    this.detSignOff.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
    this.detSignOff.userID = this.userData.optionUserId;

    //console.log('Sign Off Data - ', this.detSignOff);

    this.signOffFollowUpService.saveSignOff(this.detSignOff)
      .subscribe(data => {
        if (navigationValue === 1) {
          this.myWorklistService.setTabIndex(0);
          this.router.navigate(['/ds/my-worklist/']);
        }
        else {
          this.myWorklistService.setTabIndex(2);
          this.router.navigate(['/ds/my-worklist/']);
        }
      },
        error => {
          throw new Error('There is an error while saving Sign Off Data.');
        });
  }

  //On Sign Off check change
  onSignOffChange(event: { checked: Boolean }) {
    if (event.checked) {
      this.signOffFollowUpService.getDeterminationFollowUpReasons(this.detSignOffFollowUpInput)
        .subscribe(
          res => {
            if (res) {
              this.followUpReasons = res;
              //console.log(this.followUpReasons);
              this.setSignOffDetails();
              if (this.followUpReasons !== null) {
                if (this.followUpReasons.length > 0)
                  this.openFollowUpReasonDialog(this.followUpReasons);
                else
                  this.isSignOffDisabled = false;
              }
              else
                this.isSignOffDisabled = false;
            }
          },
          error => {
            throw new Error(error.message);
          });
    }
    else {
      this.isSignOffDisabled = true;
      this.clearSignOffForm();
    }

  }

  //Populate Sign Off Details
  setSignOffDetails() {
    this.signOffGroup.controls['signOff'].setValue(true);
    //console.log('SequenceNumber - ', this.sequenceNumber);
    if (this.sequenceNumber <= 1) {
      this.signOffBy = this.userData.optionUserId;
      this.signOffByName = this.userData.firstName + " " + this.userData.lastName;
      this.signOffDate = this.datePipe.transform(new Date(), "yyyy-MM-dd  hh:mm:ss.sss");
    }
    else {
      this.signOffUpdatedBy = this.userData.optionUserId;
      this.signOffUpdatedByName = this.userData.firstName + " " + this.userData.lastName;
      this.signOffUpdatedDate = this.datePipe.transform(new Date(), "yyyy-MM-dd  hh:mm:ss.sss");
    }
  }

  //Open Dialog
  openFollowUpReasonDialog(data: FollowUpReason[]): void {
    this.dialog.open(DetFollowUpReasonComponent, {
      width: '800px',
      maxHeight: '450px',
      disableClose: true,
      autoFocus: false,
      data: data
    }).afterClosed().subscribe(result => {
      if (result) {
        this.saveSignOff(1);
      }
      else {
        this.clearSignOffForm();
      }
    });
  }

  clearSignOffForm() {
    if (this.sequenceNumber > 1) {
      this.signOffUpdatedBy = null;
      this.signOffUpdatedByName = null;
      this.signOffUpdatedDate = null;
    }
    else {
      this.signOffBy = null;
      this.signOffByName = null;
      this.signOffDate = null;
    }
    this.signOffGroup.controls['signOff'].setValue(false);
  }
  //#endregion

  //#region Follow Up Tab

  InitFollowUp() {
    //Get Assigned To
    this.signOffFollowUpService.getFollowUpAssignedToList(this.detSignOffFollowUpInput)
      .subscribe(
        res => {
          if (res) {
            const data = res as AssignedTo[];
            //console.log('Assigned To - ', data);
            //this.assignedToReviewer = data.filter(d => { return d.roleID === 1 });
            this.assignedToReviewer = data;
            this.assignedToSupervisor = data.filter(d => { return d.roleID === 2 });
          }
        },
        error => {
          throw new Error(error.message);
        });

    // History Grid
    this.gridOptions = {
      rowHeight: 30
    } as GridOptions;

    this.columnDefs = [
      {
        headerName: 'DETFollowUpID',
        field: 'detFollowUpID',
        hide: true
      },
      {
        headerName: 'Action #',
        field: 'sequenceNumber',
        width: 70,
      },
      {
        headerName: 'Action',
        field: 'statusDescription',
        width: 120,
        tooltipField: 'statusDescription',
        //autoHeight:true,
        cellStyle: {
          'white-space': 'normal'
        },
      },
      {
        headerName: 'Reason',
        field: 'reasonDescription',
        width: 200,
        tooltipField: 'reasonDescription',
        //autoHeight:true,
        cellStyle: {
          'white-space': 'normal'
        },
      },
      {
        headerName: 'Comments',
        field: 'comment',
        width: 300,
        tooltipField: 'comment',
        //autoHeight:true,
        cellStyle: {
          'white-space': 'normal'
        },
      },
      {
        headerName: 'Assigned To',
        field: 'assignedToName',
        width: 100,
        tooltipField: 'assignedToName',
        //autoHeight:true,
        cellStyle: {
          'white-space': 'normal'
        },
      },
      {
        headerName: 'Date',
        field: 'startDate',
        width: 100,
        cellStyle: {
          'white-space': 'normal'
        },
        cellRenderer: (data) => {
          return moment(data.startDate).format('MM/DD/YYYY')
        }
      }
    ];

    this.defaultColDef = {
      filter: false,
      sortable: false,
      resizable: false,
      suppressMenu: true,
    };
    this.context = { componentParent: this };
  }

  onGridReady = (params: { api: any; columnApi: any }) => {
    /** API call to get the grid data */
    if (this.followUpStatus <= 0)
      this.getFollowUpHistoryRowData();

    params.api.setDomLayout('autoHeight');
    params.api.sizeColumnsToFit();
  }

  //Get Follow Up History
  getFollowUpHistoryRowData() {
    this.signOffFollowUpService.getFollowUpHistory(this.detSignOffFollowUpInput)
      .subscribe(
        res => {
          if (res) {
            this.rowData = res as DETFollowUp[];
            if (this.rowData && this.rowData.length > 0) {
              this.followUpStatus = this.rowData[0].status;
              //console.log('followUpStatus - ',  this.followUpStatus);
              if (this.followUpStatus > 0) {
                this.isFollowUpTabVisible = true;

                //Special Condition To Disable Sign Off Tab
                this.disableSignOffForm();

                if (this.followUpStatus === 877) {
                  this.followUpActionSelected('responseToFollowUp');
                  //Get Supervisor
                  //console.log('followUp Data - ',  this.rowData[1]);
                  // this.assignedToSupervisorID = this.rowData[1].assignedTo;
                  // this.assignedToSupervisorName = this.rowData[1].assignedToName;
                  this.followUpHistSequence1 = this.rowData.filter(x => x.sequenceNumber === 1);
                  this.assignedToSupervisorID = this.followUpHistSequence1[0].assignedTo;
                  this.assignedToSupervisorName = this.followUpHistSequence1[0].assignedToName;
                }
              }
              else {
                this.isFollowUpTabVisible = false;
              }
            }
          }
        },
        error => {
          throw new Error(error.message);
        });
  }

  //Follow Up Action Selection
  followUpActionSelected(selectedValue: string) {
    if (selectedValue === 'signOff') {
      this.isSignOffSelected = true;
      this.isFollowUpSelected = false;
      this.isFollowUpAction = false;
      this.isResponseToFollowUp = false;
    }
    else if (selectedValue === 'followUp') {
      this.isSignOffSelected = false;
      this.isFollowUpSelected = true;
      this.isFollowUpAction = false;
      this.isResponseToFollowUp = false;
    }
    else if (selectedValue === 'responseToFollowUp') {
      this.isSignOffSelected = false;
      this.isFollowUpSelected = false;
      this.isFollowUpAction = false;
      this.isResponseToFollowUp = true;
    }

    this.setFollowUpDetails();

  }

  saveFollowUp() {
    if (this.validateFollowUpForm()) {
      if (this.isSignOffSelected) {
        if (this.followUpReasonGroup.get('signOffComment').value !== null)
          this.detFollowUp.comment = this.followUpReasonGroup.get('signOffComment').value;

        if (this.followUpReasonGroup.get('signOffReasonID').value !== null)
          this.detFollowUp.reasonID = this.followUpReasonGroup.get('signOffReasonID').value;

        this.detFollowUp.status = 879; // 879 - Supervisor Sign Off Review     
      }
      else if (this.isFollowUpSelected) {
        if (this.followUpReasonGroup.get('followUpComment').value !== null)
          this.detFollowUp.comment = this.followUpReasonGroup.get('followUpComment').value;

        if (this.followUpReasonGroup.get('followUpReasonID').value !== null)
          this.detFollowUp.reasonID = this.followUpReasonGroup.get('followUpReasonID').value;

        if (this.followUpReasonGroup.get('followUpAssignedTo').value !== null)
          this.detFollowUp.assignedTo = this.followUpReasonGroup.get('followUpAssignedTo').value;

        this.detFollowUp.status = 877; // 877 - Supervisor Responded to Follow-up  
      }
      else if (this.isResponseToFollowUp) {
        if (this.followUpReasonGroup.get('responseToFollowUpComment').value !== null)
          this.detFollowUp.comment = this.followUpReasonGroup.get('responseToFollowUpComment').value;

        if (this.followUpReasonGroup.get('responseToFollowUpReasonID').value !== null)
          this.detFollowUp.reasonID = this.followUpReasonGroup.get('responseToFollowUpReasonID').value;

        // if (this.followUpReasonGroup.get('responseToFollowUpAssignedTo').value !== null)
        //   this.detFollowUp.assignedTo = this.followUpReasonGroup.get('responseToFollowUpAssignedTo').value;

        this.detFollowUp.assignedTo = this.assignedToSupervisorID > 0 ? this.assignedToSupervisorID : null;
        this.detFollowUp.status = 878; // 878 - Reviewer Responded to Follow-up
      }

      this.detFollowUp.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
      this.detFollowUp.userID = this.userData.optionUserId;

      //console.log(this.detFollowUp);

      this.signOffFollowUpService.saveFollowUp(this.detFollowUp)
        .subscribe(data => {

          // if (this.isFollowUpSelected || this.isResponseToFollowUp) {
          //   this.myWorklistService.setTabIndex(1);
          //   this.router.navigate(['/ds/my-worklist/']);
          // }
          // else if (this.isSignOffSelected) {
          //   this.myWorklistService.setTabIndex(2);
          //   this.router.navigate(['/ds/my-worklist/']);
          // }

          this.myWorklistService.setTabIndex(1);
          this.router.navigate(['/ds/my-worklist/']);

        },
          error => {
            throw new Error(error.message);
          });
    }
  }

  //Populate Sign Off Details
  setFollowUpDetails() {
    this.determinationDecisionBy = this.followUpBy = this.responseTofollowUpBy = this.userData.optionUserId;
    this.determinationDecisionByName = this.followUpByName
      = this.responseTofollowUpByName = this.userData.firstName + " " + this.userData.lastName;
    this.dateOfDetermination = this.dateAssigned
      = this.dateOfResponse = this.datePipe.transform(new Date(), "MM/dd/yyyy");
  }

  //Validate Follow Up Tab
  validateFollowUpForm() {
    var followUpAction = this.followUpReasonGroup.get('followUpAction').value;
    if ((followUpAction === null || followUpAction === '')
        && (this.isSignOffSelected === false && this.isFollowUpSelected === false && this.isResponseToFollowUp === false))
    {
      this.callToastrService("Follow Up Action is required.");
      return false;
    }

    if (this.isSignOffSelected) {
      var signOffComment = this.followUpReasonGroup.get('signOffComment').value;
      var signOffReasonID = this.followUpReasonGroup.get('signOffReasonID').value;

      if (signOffReasonID === null || signOffReasonID === '') {
        this.callToastrService("Reason is required.");
        return false;
      }

      if (signOffComment === null || signOffComment === '') {
        this.callToastrService("Comment is required.");
        return false;
      }
      else {
        if (signOffComment.length < 2 && signOffComment != '') {
          this.callToastrService("Comment should be at least 2 character long.");
          return false;
        }
      }
    }
    else if (this.isFollowUpSelected) {

      var followUpComment = this.followUpReasonGroup.get('followUpComment').value;
      var followUpReasonID = this.followUpReasonGroup.get('followUpReasonID').value;
      var followUpAssignedTo = this.followUpReasonGroup.get('followUpAssignedTo').value;

      if (followUpReasonID === null || followUpReasonID === '') {
        this.callToastrService("Reason is required.");
        return false;
      }

      if (followUpAssignedTo === null || followUpAssignedTo === '') {
        this.callToastrService("Assigned To is required.");
        return false;
      }

      if (followUpComment === null || followUpComment === '') {
        this.callToastrService("Comment is required.");
        return false;
      }
      else {
        if (followUpComment.length < 2 && followUpComment != '') {
          this.callToastrService("Comment should be at least 2 character long.");
          return false;
        }
      }
    }
    else if (this.isResponseToFollowUp) {
      var responseToFollowUpComment = this.followUpReasonGroup.get('responseToFollowUpComment').value;
      var responseToFollowUpReasonID = this.followUpReasonGroup.get('responseToFollowUpReasonID').value;

      if (responseToFollowUpReasonID === null || responseToFollowUpReasonID === '') {
        this.callToastrService("Reason is required.");
        return false;
      }

      if (responseToFollowUpComment === null || responseToFollowUpComment === '') {
        this.callToastrService("Comment is required.");
        return false;
      }
      else {
        if (responseToFollowUpComment.length < 2 && responseToFollowUpComment != '') {
          this.callToastrService("Comment should be at least 2 character long.");
          return false;
        }
      }
    }

    return true;
  }

  //#endregion

  //Tab Changes Event
  //tabChanged(tabChangeEvent: MatTabChangeEvent): void {
  tabChanged() {
    //console.log('Tab Index - ', tabChangeEvent.index);
    //this.tabSelectedIndex = tabChangeEvent.index;

    switch (this.currentTab) {
      case 0:
        //this.InitSignOff();
        this.currentTab = this.tabSelectedIndex;
        this.loadTabData();
        break;
      case 1:
        if (this.validateFollowUpForm()) {
          this.saveFollowUp();
          this.currentTab = this.tabSelectedIndex;
          //this.InitFollowUp();
          this.loadTabData();
        }
        else {
          this.tabSelectedIndex = 1;
          return;
        }
        break;
    }
  }

  loadTabData() {
    if (this.tabSelectedIndex === 0)
      this.InitSignOff();
    else
      this.InitFollowUp();
  }

  //Next Button Click
  nextTab() {
    if (this.tabSelectedIndex != 1) {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  //Previous Button Click
  previousTab() {
    if (this.tabSelectedIndex != 0) {
      if (this.validateFollowUpForm()) {
        this.saveFollowUp();
        this.tabSelectedIndex = this.tabSelectedIndex - 1;
      }
    }
    else {
      if (this.applicationDeterminationData.isDeterminationSummaryTabCompleted !== null)
        this.router.navigate(['/ds/determination-summary/', this.applicationDeterminationData.pactApplicationId]);
      else if (this.applicationDeterminationData.isApplicationOutcomeTabCompleted !== null)
        this.router.navigate(['/ds/outcome/', this.applicationDeterminationData.pactApplicationId]);
    }

  }

  //Cancel
  cancel() {
    this.isSignOffSelected = false;
    this.isFollowUpSelected = false;
    if (this.followUpStatus === 877) {
      this.isFollowUpAction = false;
      this.isResponseToFollowUp = true;
    }
    else {
      this.isFollowUpAction = true;
      this.isResponseToFollowUp = false;
    }

    this.clearFollowUpForm();
  }

  clearFollowUpForm() {
    if (this.followUpStatus === 877) {
      //Response to Follow Up Controls
      this.followUpReasonGroup.controls['responseToFollowUpReasonID'].setValue(null);
      this.followUpReasonGroup.controls['responseToFollowUpComment'].setValue(null);
    }
    else {
      this.followUpReasonGroup.controls['followUpAction'].setValue(null);

      //Sign Off Controls
      this.followUpReasonGroup.controls['signOffReasonID'].setValue(null);
      this.followUpReasonGroup.controls['signOffComment'].setValue(null);

      //Follow Up Controls
      this.followUpReasonGroup.controls['followUpReasonID'].setValue(null);
      this.followUpReasonGroup.controls['followUpAssignedTo'].setValue(null);
      this.followUpReasonGroup.controls['followUpComment'].setValue(null);
    }
  }

  //Call Toaster Service Message
  callToastrService(message: string) {
    if (!this.toastrService.currentlyActive)
      this.toastrService.error(message);
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
    }
  }


}
