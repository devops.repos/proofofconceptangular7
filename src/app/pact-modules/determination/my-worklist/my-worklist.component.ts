import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
//import { MyWorklistGridComponent } from  './my-worklist-grid/my-worklist-grid.component';

//Services
import { MyWorklistService } from './my-worklist.service';
import { UserService } from 'src/app/services/helper-services/user.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

// Models
import { MyWorklistStats } from './my-worklist.model';

@Component({
  selector: 'app-my-worklist',
  templateUrl: './my-worklist.component.html',
  styleUrls: ['./my-worklist.component.scss']
})

export class MyWorklistComponent implements OnInit, OnDestroy {
  //Global Varaibles
  tabSelectedIndex: number;
  userData: AuthData;
  userDataSub: Subscription;
  myWorklistStatsRowData :  MyWorklistStats;
  pendingAppsCount: number;
  followUpAppsCount : number;
  completedAppsCount : number;

  constructor(
    private myWorklistService: MyWorklistService,
    private userService: UserService,
    private determinationSideNavService: DeterminationSideNavService
  ) { }

  ngOnInit() {
    //Set Tab Index
    this.myWorklistService.getTabIndex().subscribe(res => {
      this.tabSelectedIndex = res;
    });

    //Get User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
      }
    });
    this.determinationSideNavService.setAllDeterminationDisabled(true);

    this.getMyWoklistStats(this.userData.optionUserId);


  }

  getMyWoklistStats(optionUserId : number){
    this.myWorklistService.GetMyWoklistStats(optionUserId)
    .subscribe(
      res => {
        this.myWorklistStatsRowData = res as MyWorklistStats;
        //console.log('myWorklistStatsRowData - ',this.myWorklistStatsRowData[0].followUpAppsCount);
        this.pendingAppsCount = this.myWorklistStatsRowData[0].pendingAppsCount;
        this.followUpAppsCount = this.myWorklistStatsRowData[0].followUpAppsCount;
        this.completedAppsCount = this.myWorklistStatsRowData[0].completedAppsCount;

      },
      error => console.error('Error!', error)
    )

  }

  //Destroy 
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

  //On Tab Change
  myWorklistTabChange() {
    this.myWorklistService.setTabIndex(this.tabSelectedIndex);
  }

  //Next Button Click
  nextTab() {
    if (this.tabSelectedIndex != 2) {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  //Previous Button Click
  previousTab() {
    if (this.tabSelectedIndex != 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }
}
