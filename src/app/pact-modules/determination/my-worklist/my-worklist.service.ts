import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { MyWorklistPendingGridComponent } from './my-worklist-pending-grid/my-worklist-pending-grid.component';
import { StartReviewParameters } from './my-worklist.model';

@Injectable({
    providedIn: 'root'
})

export class MyWorklistService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type':  'application/json'
        }),
        response : "json",
      };

    tabIndex = new BehaviorSubject<number>(0);

    constructor(private httpClient: HttpClient) {  }

    setTabIndex(value: number) {
        this.tabIndex.next(value);
    }

    getTabIndex() {
        return this.tabIndex.asObservable();
    }

    //public startReviewParams: StartReviewParameters;
    getMyWorklistPendingListUrl = environment.pactApiUrl + 'DETMyWorklist/GetMyWorklistPendingList';
    getMyWorklistStatsUrl = environment.pactApiUrl + 'DETMyWorklist/GetMyWorklistStats';
    getMyWorklistFollowUpListUrl = environment.pactApiUrl + 'DETMyWorklist/GetMyWorklistFollowUpList';
    getMyWorklistCompletedListUrl = environment.pactApiUrl + 'DETMyWorklist/GetMyWorklistCompletedList';
    saveStartReviewUrl = environment.pactApiUrl + 'DETMyWorklist/SaveStartReview';
    saveUnAssignPendingAppUrl = environment.pactApiUrl + 'DETMyWorklist/SaveUnAssignPendingApp';
    saveUnAssignFollowUpAppUrl = environment.pactApiUrl + 'DETMyWorklist/SaveUnAssignFollowUpApp';
    saveFollowUpReviewAppUrl = environment.pactApiUrl + 'DETMyWorklist/SaveFollowUpReview';

    GetMyWoklistStats(optionUserID : number) : Observable<any> {
        if(optionUserID > 0) {
            return this.httpClient.post(this.getMyWorklistStatsUrl, JSON.stringify(optionUserID), this.httpOptions);
        }
    }
  
    //#region My Worklist Pending
    GetMyWorklistPendingList(optionUserID : number) : Observable<any> {
        if(optionUserID > 0) {
            return this.httpClient.post(this.getMyWorklistPendingListUrl, JSON.stringify(optionUserID), this.httpOptions);
        }
    }

    saveStartReview(startReviewParameters : StartReviewParameters) : Observable<any> {
        return this.httpClient.post(this.saveStartReviewUrl, JSON.stringify(startReviewParameters), this.httpOptions);
    }

    saveUnAssignPendingApp(pendingUnAssignParameters : StartReviewParameters) : Observable<any> {
        return this.httpClient.post(this.saveUnAssignPendingAppUrl, JSON.stringify(pendingUnAssignParameters), this.httpOptions);
    }
    //#endregion


    //#region My Worklist FollowUp  
    GetMyWorklistFollowUpList(optionUserID : number) : Observable<any> {
        if(optionUserID > 0) {
            return this.httpClient.post(this.getMyWorklistFollowUpListUrl, JSON.stringify(optionUserID), this.httpOptions);
        }
    }  

    saveUnAssignFollowUpApp(followUpUnAssignParameters : StartReviewParameters) : Observable<any> {
        return this.httpClient.post(this.saveUnAssignFollowUpAppUrl, JSON.stringify(followUpUnAssignParameters), this.httpOptions);
    }

    saveFollowUpReview(followUpReviewParameters : StartReviewParameters) : Observable<any> {
        return this.httpClient.post(this.saveFollowUpReviewAppUrl, JSON.stringify(followUpReviewParameters), this.httpOptions);
    }
    //#endregion

    //#region My Worklist Completed
    GetMyWorklistCompletedList(optionUserID : number) : Observable<any> {
        if(optionUserID > 0) {
            return this.httpClient.post(this.getMyWorklistCompletedListUrl, JSON.stringify(optionUserID), this.httpOptions);
        }
    } 
    //#endregion 
}