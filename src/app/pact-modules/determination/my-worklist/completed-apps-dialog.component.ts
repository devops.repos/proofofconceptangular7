import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CompletedAppsDialogData } from './my-worklist.model';

@Component({
    selector: 'app-completed-apps-dialog',
    templateUrl: './completed-apps-dialog.component.html',
    styleUrls: ['./completed-apps-dialog.component.scss']
})

export class CompletedApplicationsDialogComponent implements OnInit {
    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) public completedAppsDialogData = new CompletedAppsDialogData(),
        private dialogRef: MatDialogRef<CompletedApplicationsDialogComponent>) {
    }

    //Close the dialog on close button
    CloseDialog() {
        this.dialogRef.close(true);
    }

    //On Init
    ngOnInit() {
    }
}