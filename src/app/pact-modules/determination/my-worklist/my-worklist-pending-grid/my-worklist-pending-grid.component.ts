import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
//AG Grid 
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridOptions } from 'ag-grid-community';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { MyWorklistService } from '../my-worklist.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';
import { MyWorklistPendingActionComponent } from './my-worklist-pending-action.component';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { MyWorklistPendingList, StartReviewParameters, iDocumentLink } from '../my-worklist.model';


@Component({
  selector: 'app-my-worklist-pending-grid',
  templateUrl: './my-worklist-pending-grid.component.html',
  styleUrls: ['./my-worklist-pending-grid.component.scss']
})

export class MyWorklistPendingGridComponent implements OnInit {
  //Global Variables 
  @Output() getMyWoklistStats = new EventEmitter<any>();
  @Input() name: string;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  getRowHeight;
  pactApplicationId: number = 0;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  isSupervisor: boolean = false;
  rowSelection: any;
  context: any;
  startReviewParameters: StartReviewParameters;
  pendingUnAssignParameters : StartReviewParameters;


  myWorklistPendingColumnDefs = [];
  myWorklistPendingRowData: MyWorklistPendingList[];
  myWorklistPendingDefaultColDef = {};
  public myWorklistPendingGridOptions: GridOptions;
  myWorklistPendingOverlayNoRowsTemplate: string;
  myWorklistPendingFrameworkComponents: any;
  myWorklistPendingOverlayLoadingTemplate: string;

  applicationDeterminationData: iApplicationDeterminationData;

  constructor(private determinationSideNavService: DeterminationSideNavService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private router: Router,
    private myWorklistService: MyWorklistService,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private commonService: CommonService,
    private userService: UserService) {

    this.myWorklistPendingGridOptions = {
      rowHeight: 30
    } as GridOptions;


    //My Worklist Pending Grid Column Definitions
    this.myWorklistPendingColumnDefs = [
      //{ headerName: 'Application #', field: 'pactApplicationID', width: 160, filter: 'agTextColumnFilter' },
      {
        headerName: 'Application Number',
        field: 'pactApplicationID',
        cellRenderer: (params: { value: string; data: { pactApplicationID: number; applicationSummaryReportFileNetDocID: number}; }) => {
          var link = document.createElement('a');
          link.href = '#';
          link.innerText = params.value;
          link.addEventListener('click', (e) => {
            e.preventDefault();
            this.commonService.setIsOverlay(true);
            this.openApplicationSummaryReport(params.data.applicationSummaryReportFileNetDocID,params.data.pactApplicationID.toString(), this.userData.optionUserId);
          });
          return link;
        },
        width: 155,
        filter: 'agTextColumnFilter'
      },
      { headerName: 'Client #', field: 'clientNumber', width: 100, filter: 'agTextColumnFilter' },
      { headerName: 'Client Name (L,N)', field: 'clientName', width: 160, filter: 'agTextColumnFilter' },
      //Supervisor view
      {
        headerName: 'Reference Date/Time', field: 'referenceDate', width: 160, filter: 'agTextColumnFilter',
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        }
      },
      //Reviewer view
      {
        headerName: 'Referral Date', field: 'referralDate', width: 160, filter: 'agTextColumnFilter',
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        }
      },
      { headerName: 'Referral Agency/Site', field: 'referalAgencySite', width: 300, filter: 'agTextColumnFilter' },
      { headerName: 'Referred By Name (L,F)', field: 'referredByName', width: 160, filter: 'agTextColumnFilter' },
      // Supervisor view
      { headerName: 'Application Type (T)', field: 'applicationType', width: 160, filter: 'agTextColumnFilter' },
      { headerName: 'Pop Type', field: 'populationAppliedFor', width: 130, filter: 'agTextColumnFilter' },
      { headerName: 'Case Type', field: 'caseType', width: 150, filter: 'agTextColumnFilter', cellRenderer: this.hasOriginalReviewer, tooltipField: 'originalReviewer' },
      { headerName: 'Assigned To', field: 'assignedTo', width: 150, filter: 'agTextColumnFilter' },
      {
        headerName: 'Date Assigned', field: 'reviewAssignedDate', width: 160, filter: 'agTextColumnFilter',
        cellRenderer : this.PreviouslyAssignedTo,
        tooltipField: 'previouslyAssignedTo'
        // cellRenderer: (data: { value: string | number | Date; }) => {
        //   return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        // }
      },
      //Reviewer view
      {
        headerName: 'Review Start Date', field: 'reviewStartDate', width: 160, filter: 'agTextColumnFilter',
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        }
      },
      { headerName: 'Review Status', field: 'caseStatus', width: 160, filter: 'agTextColumnFilter' },
      { headerName: 'Assignment Comments', field: 'comments', width: 300, filter: 'agTextColumnFilter' },
      { headerName: 'Actions', field: 'action', width: 65, filter: false, sortable: false, resizable: false, pinned: 'left', suppressMenu: true, suppressSizeToFit: true, cellRenderer: 'actionRenderer' },
    ];
    this.myWorklistPendingDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    this.pagination = true;
    this.rowSelection = 'single';
    this.context = { componentParent: this };

    this.myWorklistPendingFrameworkComponents = {
      actionRenderer: MyWorklistPendingActionComponent
    };

    this.getRowHeight = function (params: any) {
      return params.data.originalReviewer != null ? 30 : 30;
    };
  }

  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        if (this.userData.roleId == 2) {
          this.isSupervisor = true;
        }
        //Getting my worklist pending list details
        this.getMyWorklistPendingList(this.userData.optionUserId);
      }
    });
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

  //My Worklist Pending Grid Ready
  myWorklistPendingGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.myWorklistPendingOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while loading.</span>';
    this.myWorklistPendingOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Data Available</span>';

    this.myWorklistPendingGridOptions.api.getColumnDef('comments').headerName = this.isSupervisor == true ? 'Comments' : 'Assignment Comments';
    if (this.isSupervisor) {
      this.myWorklistPendingGridOptions.columnApi.setColumnsVisible(['referenceDate', 'applicationType'], true);
      this.myWorklistPendingGridOptions.columnApi.setColumnsVisible(['referralDate', 'reviewStartDate'], false);
    } else {
      this.myWorklistPendingGridOptions.columnApi.setColumnsVisible(['referenceDate', 'applicationType'], false);
      this.myWorklistPendingGridOptions.columnApi.setColumnsVisible(['referralDate', 'reviewStartDate'], true);
    }
  }

  //Get My Pending Work List
  getMyWorklistPendingList(optionUserId: number) {
    this.myWorklistService.GetMyWorklistPendingList(optionUserId)
      .subscribe(
        res => {
          if (res) {
            this.myWorklistPendingRowData = res as MyWorklistPendingList[];
          }
        },
        error => console.error('Error!', error)
      );
  }

  //On Page Size Changed
  onPageSizeChanged(selectedPageSize: any) {
    this.myWorklistPendingGridOptions.api.paginationSetPageSize(Number(selectedPageSize));
    this.myWorklistPendingGridOptions.api.refreshHeader();
  }

  //Has Original Reviewer
  hasOriginalReviewer(params: any) {
    return params.data.caseType == "Resubmission" && params.data.originalReviewer != null ?  params.data.caseType + `<mat-icon class="mat-icon material-icons"  style="padding-left: 5px; font-size: 15px !important;" [matTooltip]="" matTooltipPosition="after">info</mat-icon>` :  params.data.caseType; }
  
  //Case PreviouslyAssignedTo  
  PreviouslyAssignedTo (params: any) {
    return params.data.sequenceId > 1 && params.data.previouslyAssignedTo != null ?  (new Date(params.data.reviewAssignedDate)).toLocaleDateString() + `<mat-icon class="mat-icon material-icons"  style="padding-left: 5px; font-size: 15px !important;" [matTooltip]="" matTooltipPosition="after">info</mat-icon>` :  (new Date(params.data.reviewAssignedDate)).toLocaleDateString() }
  

  //Save Start Review
  saveStartReview(pendingListdata: MyWorklistPendingList) {
    if (pendingListdata && pendingListdata.pactApplicationID) {
      this.startReviewParameters = { pactApplicationID: pendingListdata.pactApplicationID, optionUserID: this.userData.optionUserId };
      this.myWorklistService.saveStartReview(this.startReviewParameters)
        .subscribe(data => {
          if (data) {
            this.router.navigate(['/ds/match-sources', pendingListdata.pactApplicationID]);
          }
        },
          error => {
            throw new Error(error.message);
          }
        );
    }
  }

  //Save UnAssign for Pending Application
  saveUnAssignPendingApp (pendingListdata: MyWorklistPendingList) {
    const title = 'Confirm Unassign';
    const primaryMessage = 'The Application will be unassigned.';
    const secondaryMessage =
      'Are you sure you want to unassign the selected application?';
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService
      .confirmDialog(
        title,
        primaryMessage,
        secondaryMessage,
        confirmButtonName,
        dismissButtonName
      )
      .then( 
        positiveResponse => { 
            if (pendingListdata && pendingListdata.pactApplicationID) {
              this.pendingUnAssignParameters = { pactApplicationID: pendingListdata.pactApplicationID, optionUserID: this.userData.optionUserId };
              this.myWorklistService.saveUnAssignPendingApp(this.pendingUnAssignParameters)
                .subscribe(res => {
                  if (res) {
                    this.toastr.success('Application unassigned successfully.');
                    this.getMyWoklistStats.emit(this.userData.optionUserId);
                    this.getMyWorklistPendingList(this.userData.optionUserId);
                  }
                },
                  error => {
                    this.toastr.error(
                      'There was an error in unassigning the application.',
                      'Unassign application Failed!'
                    );
                  }
                );
            } else {
              this.toastr.error(
                'There was an error in unassigning the application.',
                'Unassign application Failed!'
              );
            }              
        },
        negativeResponse => console.log()
      );
  }

  //On Continue Review
  onContinueReview(pendingListData: MyWorklistPendingList) {
    this.determinationSideNavService.setAllDeterminationDisabled(true);
    if (pendingListData && pendingListData.pactApplicationID) {
      this.applicationDeterminationService.setApplicationDataForDeterminationWithApplicationId(pendingListData.pactApplicationID)
        .subscribe(res => {
          if (res) {
            const data = res as iApplicationDeterminationData;
            if (data) {
              this.applicationDeterminationData = data;
              this.applicationDeterminationService.setApplicationDeterminationData(this.applicationDeterminationData);
              this.navigatePath(pendingListData.pactApplicationID, this.applicationDeterminationData);
            }
          }
        })
    }
  }

  //On Client Case Folder
  onClientCaseFolder(data: MyWorklistPendingList) {
    if (data && data.pactApplicationID) {
      this.router.navigate(['/ds/client-case-folder', data.pactApplicationID]);
    }
  }

  //On Assignment History
  onAssignmentHistory(data: MyWorklistPendingList) {
    this.router.navigate(['/ds/assignment-history', data.pactApplicationID]);
  }

    //Open Application Summary Report
    openApplicationSummaryReport(applicationSummaryReportFileNetDocID: number, selectedApplicationID: string, optionUserId: number) {
      if (applicationSummaryReportFileNetDocID) {
        this.commonService.getFileNetDocumentLink(applicationSummaryReportFileNetDocID.toString()).subscribe(res => {
          const data = res as iDocumentLink;
          if (data && data.linkURL) {
            this.commonService.OpenWindow(data.linkURL);
            this.commonService.setIsOverlay(false);
          }
        });
      }
      else {
        this.commonService.displayApplicationSummaryReport(selectedApplicationID, optionUserId);
      }
    }

  //Navigate Path
  navigatePath(pactApplicationId: number, applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      var navigateTo: string = null;
      //Match Sources
      if (applicationDeterminationData.isMatchSummaryTabCompleted ||
        applicationDeterminationData.isHousingProgramMatchTabCompleted ||
        applicationDeterminationData.isDHSMatchTabCompleted ||
        applicationDeterminationData.isHASAMatchTabCompleted ||
        applicationDeterminationData.isMedicaidMatchTabCompleted ||
        applicationDeterminationData.isSTARSMatchTabCompleted) {
        navigateTo = "match-sources";
      }
      //Application
      if (applicationDeterminationData.isApplicationTabCompleted ||
        applicationDeterminationData.isSupportingDocumentsTabCompleted) {
        navigateTo = "application";
      }
      //Client Case Folder
      if (applicationDeterminationData.isCaseNotesTabCompleted ||
        applicationDeterminationData.isCoordinatedAssessmentSurveyTabCompleted ||
        applicationDeterminationData.isPriorApplicationsDeterminationsTabCompleted ||
        applicationDeterminationData.isClientDocumentsTabCompleted ||
        applicationDeterminationData.isVCSReferralsTabCompleted ||
        applicationDeterminationData.isVCSPlacementsTabCompleted ||
        applicationDeterminationData.isVPSReferralsTabCompleted) {
        navigateTo = "client-case-folder";
      }
      //Application Outcome
      if (applicationDeterminationData.isApplicationOutcomeTabCompleted ||
        applicationDeterminationData.isAdministrativeRejectTabCompleted ||
        applicationDeterminationData.isClinicalPreDeterminationTabCompleted ||
        applicationDeterminationData.isApplicationDeterminationLetterTabCompleted) {
        navigateTo = "outcome";
      }
      //Clinical Review
      if (applicationDeterminationData.isMentalHealthConditionTabCompleted ||
        applicationDeterminationData.isSubstanceUseTabCompleted ||
        applicationDeterminationData.isMedicalConditionsTabCompleted ||
        applicationDeterminationData.isClinicalAtRiskTabCompleted ||
        applicationDeterminationData.isClinicalSummaryTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsClinicalReviewDisabled(false);
        navigateTo = "clinical-review";
      }
      //Homeless Review
      if (applicationDeterminationData.isHousingHomelessTabCompleted ||
        applicationDeterminationData.isHomeless1To4YearsTabCompleted ||
        applicationDeterminationData.isResidentialTreatmentAndInstitutionalHistoryTabCompleted ||
        applicationDeterminationData.isHomelessAtRiskTabCompleted ||
        applicationDeterminationData.isHomelessSummaryTabCompleted ||
        applicationDeterminationData.isHousingHomelessReportTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsHomelessReviewDisabled(false);
        navigateTo = "homeless-review";
      }
      //Medicaid Prioritization
      if (applicationDeterminationData.isMedicaidClaimsTabCompleted ||
        applicationDeterminationData.isMedicaidPrioritizationTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(false);
        navigateTo = "medicaid-prioritization";
      }
      //Vulnerability Assessment
      if (applicationDeterminationData.isActivitiesOfDailyLivingTabCompleted ||
        applicationDeterminationData.isSVAReviewTabCompleted ||
        applicationDeterminationData.isVulnerabilityAssessmentTabCompleted ||
        applicationDeterminationData.isSVASummaryReportTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(false);
        navigateTo = "vulnerability-assessment";
      }
      //Determination Summary
      if (applicationDeterminationData.isDeterminationSummaryTabCompleted ||
        applicationDeterminationData.isConditionsRecommendationsTabCompleted ||
        applicationDeterminationData.isDeterminationLetterTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsDeterminationSummaryDisabled(false);
        navigateTo = "determination-summary";
      }
      //Sign Off
      if (applicationDeterminationData.isSignOffTabCompleted ||
        applicationDeterminationData.isFollowUpTabCompleted) {
        this.determinationSideNavService.setIsSignOffFollowUpDisabled(false);
        navigateTo = "sign-off-follow-up";
      }

      //Router Navigation 
      if (navigateTo) {
        this.router.navigate(['/ds/' + navigateTo, pactApplicationId]);
      }
      else {
        this.router.navigate(['/ds/match-sources', pactApplicationId]);
      }
    }
  }
}
