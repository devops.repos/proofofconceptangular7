import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
  selector: "my-worklist-pending-action",
  template: `
    <mat-icon
      class="myWorklistPendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="myWorklistPendingAction">
      more_vert
    </mat-icon>
    <mat-menu #myWorklistPendingAction="matMenu">
      <button mat-menu-item (click)="onAssignmentHistory()" class="menu-button">Assignment History</button>
      <button mat-menu-item (click)="onClientCaseFolder()" class="menu-button" [disabled]="!isClientCaseFolder">Client Case Folder</button>
      <button mat-menu-item (click)="saveUnAssignPendingApp()" class="menu-button">Un-Assign</button>   
      <div *ngIf="isStartReview">
        <button mat-menu-item (click)="saveStartReview()" class="menu-button">Start Review</button>
      </div>
      <div *ngIf="isContinueReview">
        <button mat-menu-item (click)="onContinueReview()" class="menu-button">Continue Review</button>    
      </div>
    </mat-menu>
  `,
  styles: [
    `
      .myWorklistPendingMenu-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 40px;
        width: 100%;
        height: 40px;
      }
    `
  ]
})

export class MyWorklistPendingActionComponent implements ICellRendererAngularComp {
  params: any;
  cell: any;
  isSupervisor: boolean;
  selectedPactApplicationID: any;
  optionsUserId: any;
  isStartReview: boolean;
  isContinueReview: boolean;
  isClientCaseFolder: boolean;

  //Constrctor
  constructor() {
  }

  //Init
  agInit(params: any) {
    this.params = params;
    this.selectedPactApplicationID = this.params.data.pactApplicationID;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    this.isSupervisor = this.params.context.componentParent.isSupervisor;
    this.selectedPactApplicationID = this.params.data.pactApplicationID;
    this.isStartReview = this.params.data.caseStatusTypeId === 813 ? true : false;
    this.isContinueReview = this.params.data.caseStatusTypeId === 814 ? true : false;
    this.isClientCaseFolder = this.params.data.caseStatusTypeId === 814 && this.params.data.clientNumber ? true : false;
  }

  //Refresh
  refresh(): boolean {
    return false;
  }

  //Save Start Review
  saveStartReview() {
    this.params.context.componentParent.saveStartReview(this.params.data);
  }

  //save UnAssing Pending App
  saveUnAssignPendingApp() {
    this.params.context.componentParent.saveUnAssignPendingApp(this.params.data);
  }

  //On Continue Review
  onContinueReview() {
    this.params.context.componentParent.onContinueReview(this.params.data);
  }

  //On Client Case Folder
  onClientCaseFolder() {
    this.params.context.componentParent.onClientCaseFolder(this.params.data);
  }

  //On Assignment History
  onAssignmentHistory() {
    this.params.context.componentParent.onAssignmentHistory(this.params.data);
  }
}

