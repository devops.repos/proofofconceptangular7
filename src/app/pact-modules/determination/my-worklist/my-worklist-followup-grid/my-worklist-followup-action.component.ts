import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
  selector: "my-worklist-followup-action",
  template: `
    <mat-icon
      class="myWorklistFollowUpMenu-icon"
      color="warn"
      [matMenuTriggerFor]="myWorklistFollowUpAction">
      more_vert
    </mat-icon>
    <mat-menu #myWorklistFollowUpAction="matMenu">
      <button mat-menu-item (click)="onAssignmentHistory()" class="menu-button">Assignment History</button>
      <button mat-menu-item (click)="onClientCaseFolder()" class="menu-button">Client Case Folder</button>
      <button mat-menu-item (click)="saveUnAssignFollowUpApp()"  class="menu-button">Un-Assign</button>   
      <button mat-menu-item (click)="saveFollowUpReview()" class="menu-button">Follow-Up / Continue Review</button>
    </mat-menu>
  `,
  styles: [
    `
      .myWorklistFollowUpMenu-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 40px;
        width: 100%;
        height: 40px;
      }
    `
  ]
})

export class MyWorklistFollowUpActionComponent implements ICellRendererAngularComp {
  params: any;
  cell: any;
  isSupervisor: boolean;
  selectedPactApplicationID: any;
//   optionsUserId: any;

  //Constrctor
  constructor() {
  }

  //Init
  agInit(params: any) {
    this.params = params;
    this.selectedPactApplicationID = this.params.data.pactApplicationID;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    // this.isSupervisor = this.params.context.componentParent.isSupervisor;
    // this.selectedPactApplicationID = this.params.data.pactApplicationID;
  }

  //Refresh
  refresh(): boolean {
    return false;
  }

  //save UnAssing FollowUp App
  saveUnAssignFollowUpApp() {
    this.params.context.componentParent.saveUnAssignFollowUpApp(this.params.data);
  }

  //save
  saveFollowUpReview() {
    this.params.context.componentParent.saveFollowUpReview(this.params.data);
  }

  //On Client Case Folder
  onClientCaseFolder() {
    this.params.context.componentParent.onClientCaseFolder(this.params.data);
  }

  //On Assignment History
  onAssignmentHistory() {
    this.params.context.componentParent.onAssignmentHistory(this.params.data);
  }
}

