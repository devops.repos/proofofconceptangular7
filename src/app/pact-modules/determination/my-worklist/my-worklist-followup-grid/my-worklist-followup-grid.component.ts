import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
//AG Grid 
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridOptions } from 'ag-grid-community';

//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { MyWorklistService } from '../my-worklist.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';
import { MyWorklistFollowUpActionComponent } from './my-worklist-followup-action.component';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { MyWorklistFollowUpList, StartReviewParameters } from '../my-worklist.model';

@Component({
  selector: 'app-my-worklist-followup-grid',
  templateUrl: './my-worklist-followup-grid.component.html',
  styleUrls: ['./my-worklist-followup-grid.component.scss']
})

export class MyWorklistFollowUpGridComponent implements OnInit {
  //Global Variables 
  @Output() getMyWoklistStats = new EventEmitter<any>();
  @Input() name: string;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  getRowHeight;
  pactApplicationId: number = 0;
  followUpUnAssignParameters : StartReviewParameters;
  followUpReviewParameters : StartReviewParameters;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  isSupervisor: boolean = false;
  rowSelection: any;
  context: any;
  //startReviewParameters: StartReviewParameters;

  myWorklistFollowUpColumnDefs = [];
  myWorklistFollowUpRowData: MyWorklistFollowUpList[];
  myWorklistFollowUpDefaultColDef = {};
  public myWorklistFollowUpGridOptions: GridOptions;
  myWorklistFollowUpOverlayNoRowsTemplate: string;
  myWorklistFollowUpFrameworkComponents: any;
  myWorklistFollowUpOverlayLoadingTemplate: string;

  applicationDeterminationData: iApplicationDeterminationData;

  constructor(private determinationSideNavService: DeterminationSideNavService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private router: Router,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private myWorklistService: MyWorklistService,
    private userService: UserService) {

    this.myWorklistFollowUpGridOptions = {
      rowHeight: 30
    } as GridOptions;


    //My Worklist FollowUp Grid Column Definitions
    this.myWorklistFollowUpColumnDefs = [
      { headerName: 'Application #', field: 'pactApplicationID', width: 160, filter: 'agTextColumnFilter' },
      { headerName: 'Client #', field: 'clientNumber', width: 100, filter: 'agTextColumnFilter' },
      { headerName: 'Client Name (L,N)', field: 'clientName', width: 160, filter: 'agTextColumnFilter' },
      {
        headerName: 'Referral Date', field: 'referralDate', width: 160, filter: 'agTextColumnFilter',
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        }
      },
      { headerName: 'Referral Agency/Site', field: 'referalAgencySite', width: 300, filter: 'agTextColumnFilter' },
      { headerName: 'Referred By Name (L,F)', field: 'referredByName', width: 160, filter: 'agTextColumnFilter' },
      { headerName: 'Eligibility', field: 'eligibility', width: 130, filter: 'agTextColumnFilter' },
      { headerName: 'Prioritization', field: 'prioritization', width: 150, filter: 'agTextColumnFilter' },
      { headerName: 'Service Needs', field: 'serviceNeeds', width: 150, filter: 'agTextColumnFilter' },
      { headerName: 'Approval Period', field: 'approvalPeriod', width: 150, filter: 'agTextColumnFilter' },
      { headerName: 'Assigned To', field: 'assignedTo', width: 150, filter: 'agTextColumnFilter' },
      {
        headerName: 'QA Date Assigned', field: 'reviewAssignedDate', width: 160, filter: 'agTextColumnFilter',
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        }
      },
      { headerName: 'Follow Up Reason', field: 'followUpReason', width: 160, filter: 'agTextColumnFilter' },
      { headerName: 'Follow Up Status', field: 'followUpStatus', width: 300, filter: 'agTextColumnFilter' },
      { headerName: 'Follow Up Comments', field: 'followUpComments', width: 300, filter: 'agTextColumnFilter' },
      { headerName: 'Actions', field: 'action', width: 65, filter: false, sortable: false, resizable: false, pinned: 'left', suppressMenu: true, suppressSizeToFit: true, cellRenderer: 'actionRenderer' },
    ];
    this.myWorklistFollowUpDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    this.pagination = true;
    this.rowSelection = 'single';
    this.context = { componentParent: this };

    this.myWorklistFollowUpFrameworkComponents = {
      actionRenderer: MyWorklistFollowUpActionComponent
    };

    this.getRowHeight = function (params: any) {
      return params.data.originalReviewer != null ? 30 : 30;
    };
  }

  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        if (this.userData.roleId == 2) {
          this.isSupervisor = true;
        }
        //Getting my worklist followup list details
        this.getMyWorklistFollowUpList(this.userData.optionUserId);
      }
    });
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

  //My Worklist Pending Grid Ready
  myWorklistFollowUpGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.myWorklistFollowUpOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while loading.</span>';
    this.myWorklistFollowUpOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Data Available</span>';

    // this.myWorklistFollowUpGridOptions.api.getColumnDef('comments').headerName = this.isSupervisor == true ? 'Comments' : 'Assignment Comments';
    // if (this.isSupervisor) {
    //   this.myWorklistFollowUpGridOptions.columnApi.setColumnsVisible(['referenceDate', 'applicationType'], true);
    //   this.myWorklistFollowUpGridOptions.columnApi.setColumnsVisible(['referralDate', 'reviewStartDate'], false);
    // } else {
    //   this.myWorklistFollowUpGridOptions.columnApi.setColumnsVisible(['referenceDate', 'applicationType'], false);
    //   this.myWorklistFollowUpGridOptions.columnApi.setColumnsVisible(['referralDate', 'reviewStartDate'], true);
    // }
  }

  //Get My FollowUp Work List
  getMyWorklistFollowUpList(optionUserId: number) {
    this.myWorklistService.GetMyWorklistFollowUpList(optionUserId)
      .subscribe(
        res => {
          if (res) {
            this.myWorklistFollowUpRowData = res as MyWorklistFollowUpList[];
          }
        },
        error => console.error('Error!', error)
      );
  }

  //On Page Size Changed
  onPageSizeChanged(selectedPageSize: any) {
    this.myWorklistFollowUpGridOptions.api.paginationSetPageSize(Number(selectedPageSize));
    this.myWorklistFollowUpGridOptions.api.refreshHeader();
  }

  //Has Original Reviewer
  hasOriginalReviewer(params: any) {
    return params.data.originalReviewer != null ? params.data.caseType + `<mat-icon class="mat-icon material-icons mat-icon-no-color" style="width: 50px;height: 50px;" matTooltip="C" matTooltipPosition="above">
    info</mat-icon>` : params.data.caseType;
  }

  //Save AssignTo value for FollowUp Application
  saveFollowUpReview (followUpListdata: MyWorklistFollowUpList) {
    if (followUpListdata && followUpListdata.pactApplicationID && (followUpListdata.assignedTo == null || followUpListdata.assignedTo == "" )) {
      this.followUpReviewParameters = { pactApplicationID: followUpListdata.pactApplicationID, optionUserID: this.userData.optionUserId };
      this.myWorklistService.saveFollowUpReview(this.followUpReviewParameters)
        .subscribe(res => {
          if (res) {
            this.router.navigate(['/ds/sign-off-follow-up', followUpListdata.pactApplicationID]);
          }
        },
          error => {
            throw new Error(error.message);
          }
        );
    } else {
      this.router.navigate(['/ds/sign-off-follow-up', followUpListdata.pactApplicationID]);
    }
  }

  //Save UnAssign for FollowUp Application
  saveUnAssignFollowUpApp (followUpListdata: MyWorklistFollowUpList) {
    const title = 'Confirm Unassign';
    const primaryMessage = 'The Application will be unassigned.';
    const secondaryMessage =
      'Are you sure you want to unassign the selected application?';
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService
      .confirmDialog(
        title,
        primaryMessage,
        secondaryMessage,
        confirmButtonName,
        dismissButtonName
      )
      .then( 
        positiveResponse => { 
              if (followUpListdata && followUpListdata.pactApplicationID) {
                this.followUpUnAssignParameters = { pactApplicationID: followUpListdata.pactApplicationID, optionUserID: this.userData.optionUserId };
                this.myWorklistService.saveUnAssignFollowUpApp(this.followUpUnAssignParameters)
                  .subscribe(res => {
                    if (res) {
                      this.toastr.success('Application unassigned successfully.');
                      this.getMyWoklistStats.emit(this.userData.optionUserId);
                      this.getMyWorklistFollowUpList(this.userData.optionUserId);
                    }
                  },
                    error => {
                      this.toastr.error(
                        'There was an error in unassigning the application.',
                        'Unassign application Failed!'
                      );
                    }
                  );
              } else  {              
                this.toastr.error(
                'There was an error in unassigning the application.',
                'Unassign application Failed!'
              );       
            }          
        },
        negativeResponse => console.log()
      );
  }

  //On Client Case Folder
  onClientCaseFolder(data: MyWorklistFollowUpList) {
    if (data && data.pactApplicationID) {
      this.router.navigate(['/ds/client-case-folder', data.pactApplicationID]);
    }
  }

  //On Assignment History
  onAssignmentHistory(data: MyWorklistFollowUpList) {
    this.router.navigate(['/ds/assignment-history', data.pactApplicationID]);
  }

  //Navigate Path
  navigatePath(pactApplicationId: number, applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      var navigateTo: string = null;
      //Match Sources
      if (applicationDeterminationData.isMatchSummaryTabCompleted ||
        applicationDeterminationData.isHousingProgramMatchTabCompleted ||
        applicationDeterminationData.isDHSMatchTabCompleted ||
        applicationDeterminationData.isHASAMatchTabCompleted ||
        applicationDeterminationData.isMedicaidMatchTabCompleted ||
        applicationDeterminationData.isSTARSMatchTabCompleted) {
        navigateTo = "match-sources";
      }
      //Application
      if (applicationDeterminationData.isApplicationTabCompleted ||
        applicationDeterminationData.isSupportingDocumentsTabCompleted) {
        navigateTo = "application";
      }
      //Client Case Folder
      if (applicationDeterminationData.isCaseNotesTabCompleted ||
        applicationDeterminationData.isCoordinatedAssessmentSurveyTabCompleted ||
        applicationDeterminationData.isPriorApplicationsDeterminationsTabCompleted ||
        applicationDeterminationData.isClientDocumentsTabCompleted ||
        applicationDeterminationData.isVCSReferralsTabCompleted ||
        applicationDeterminationData.isVCSPlacementsTabCompleted ||
        applicationDeterminationData.isVPSReferralsTabCompleted) {
        navigateTo = "client-case-folder";
      }
      //Application Outcome
      if (applicationDeterminationData.isApplicationOutcomeTabCompleted ||
        applicationDeterminationData.isAdministrativeRejectTabCompleted ||
        applicationDeterminationData.isClinicalPreDeterminationTabCompleted ||
        applicationDeterminationData.isApplicationDeterminationLetterTabCompleted) {
        navigateTo = "outcome";
      }
      //Clinical Review
      if (applicationDeterminationData.isMentalHealthConditionTabCompleted ||
        applicationDeterminationData.isSubstanceUseTabCompleted ||
        applicationDeterminationData.isMedicalConditionsTabCompleted ||
        applicationDeterminationData.isClinicalAtRiskTabCompleted ||
        applicationDeterminationData.isClinicalSummaryTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsClinicalReviewDisabled(false);
        navigateTo = "clinical-review";
      }
      //Homeless Review
      if (applicationDeterminationData.isHousingHomelessTabCompleted ||
        applicationDeterminationData.isHomeless1To4YearsTabCompleted ||
        applicationDeterminationData.isResidentialTreatmentAndInstitutionalHistoryTabCompleted ||
        applicationDeterminationData.isHomelessAtRiskTabCompleted ||
        applicationDeterminationData.isHomelessSummaryTabCompleted ||
        applicationDeterminationData.isHousingHomelessReportTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsHomelessReviewDisabled(false);
        navigateTo = "homeless-review";
      }
      //Medicaid Prioritization
      if (applicationDeterminationData.isMedicaidClaimsTabCompleted ||
        applicationDeterminationData.isMedicaidPrioritizationTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(false);
        navigateTo = "medicaid-prioritization";
      }
      //Vulnerability Assessment
      if (applicationDeterminationData.isActivitiesOfDailyLivingTabCompleted ||
        applicationDeterminationData.isSVAReviewTabCompleted ||
        applicationDeterminationData.isVulnerabilityAssessmentTabCompleted ||
        applicationDeterminationData.isSVASummaryReportTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(false);
        navigateTo = "vulnerability-assessment";
      }
      //Determination Summary
      if (applicationDeterminationData.isDeterminationSummaryTabCompleted ||
        applicationDeterminationData.isConditionsRecommendationsTabCompleted ||
        applicationDeterminationData.isDeterminationLetterTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsDeterminationSummaryDisabled(false);
        navigateTo = "determination-summary";
      }
      //Sign Off
      if (applicationDeterminationData.isSignOffTabCompleted ||
        applicationDeterminationData.isFollowUpTabCompleted) {
        this.determinationSideNavService.setIsSignOffFollowUpDisabled(false);
        navigateTo = "sign-off-follow-up";
      }
      //Router Navigation 
      if (navigateTo) {
        this.router.navigate(['/ds/' + navigateTo, pactApplicationId]);
      }
      else {
        this.router.navigate(['/ds/match-sources', pactApplicationId]);
      }
    }
  }
}
