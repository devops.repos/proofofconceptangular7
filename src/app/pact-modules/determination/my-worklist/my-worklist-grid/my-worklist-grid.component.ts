import { Component, OnInit, Input } from '@angular/core';

import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { MatDialog } from '@angular/material';
import { CaseAssignmentActionComponent } from '../../case-assignment/case-assignment-action/case-assignment-action.component';
import { CaseAssignmentModel } from '../../case-assignment/case-assignment.model';
@Component({
  selector: 'app-my-worklist-grid',
  templateUrl: './my-worklist-grid.component.html',
  styleUrls: ['./my-worklist-grid.component.scss']
})
export class MyWorklistGridComponent implements OnInit {

  @Input() name: string;

  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  public gridOptions: GridOptions;
  rowData: CaseAssignmentModel[];
  overlayLoadingTemplate: string;
  overlayNoRowsTemplate: string;

   caseList: CaseAssignmentModel[]; //= [
  //   {
  //     srNo: 331122,
  //     // clientNumber : number,
  //     // clientName : string,
  //     // referralDate: '08/19/2019 15:34',
  //     referalAgencySite: '2005-CAMBA / 001-Gather Place',
  //     referedBy: 'Ken Williams',
  //     populationAppliedFor: 'AB',
  //     caseType: 'New',
  //     reviewerAssignedTo: '', //'Select One',
  //     // reviewAssignedDate? : Date,
  //     caseStatus: 'Un-Assigned',
  //     comments: 'No Comments ... ... .. .. .......... ........... ... ..',
  //     reviewer: 0,

  //   },
  //   {
  //     srNo: 324400,
  //     clientNumber: 242112,
  //     clientName: 'Susan , Joe',
  //     referralDate: '08/19/2019 15:34',
  //     referalAgencySite: '2005-CAMBA/ 049- Landing',
  //     referedBy: 'Michael Bierra',
  //     populationAppliedFor: 'D',
  //     caseType: 'New',
  //     reviewerAssignedTo: '',
  //     // reviewAssignedDate? : Date,
  //     caseStatus: 'Un-Assigned',
  //     comments: 'Test Comments',
  //     reviewer: 0,
  //   },
  //   {
  //     srNo: 332255,
  //     clientNumber: 241123,
  //     clientName: 'Doe, John',
  //     // referralDate? : Date,
  //     referalAgencySite: '2005-CAMBA/ 001- Gathering Place',
  //     referedBy: 'Kenneth Johnson',
  //     populationAppliedFor: 'AE',
  //     caseType: 'New',
  //     reviewerAssignedTo: '',
  //     // reviewAssignedDate? : Date,
  //     caseStatus: 'Un-Assigned',
  //     comments: 'My Comments',
  //     reviewer: 0,
  //   },
  //   {
  //     srNo: 334411,
  //     clientNumber: 234411,
  //     clientName: 'Joe, Ram',
  //     referralDate: '09/20/2019 14:20',
  //     referalAgencySite: '2005-CAMBA/ 001- Gathering Place',
  //     referedBy: 'Ron Williams',
  //     populationAppliedFor: 'AH',
  //     caseType: 'Resubmission',
  //     reviewerAssignedTo: 'Elsa Stazesky',
  //     reviewAssignedDate: '09/10/2019 10:39',
  //     caseStatus: 'Pending Review',
  //     comments: '',
  //     reviewer: 0,
  //     originalReviewer: 'JAJ'
  //   },
  //   {
  //     srNo: 331133,
  //     // clientNumber : number,
  //     // clientName : string,
  //     // referralDate? : Date,
  //     referalAgencySite: '2005-CAMBA / 001-Gather Place',
  //     referedBy: 'Ken Williams',
  //     populationAppliedFor: 'AB',
  //     caseType: 'New',
  //     reviewerAssignedTo: '', //'Select One',
  //     // reviewAssignedDate? : Date,
  //     caseStatus: 'Un-Assigned',
  //     comments: 'No Comments',
  //     reviewer: 0,

  //   },
  //   {
  //     srNo: 324488,
  //     clientNumber: 242188,
  //     clientName: 'Susan , Joe',
  //     referralDate: '08/10/2019 09:10',
  //     referalAgencySite: '2005-CAMBA/ 049- Landing',
  //     referedBy: 'Michael Bierra',
  //     populationAppliedFor: 'D',
  //     caseType: 'New',
  //     reviewerAssignedTo: '',
  //     reviewAssignedDate: '09/10/2019 10:39',
  //     caseStatus: 'Un-Assigned',
  //     comments: 'Test Comments',
  //     reviewer: 0,
  //   },
  //   {
  //     srNo: 332275,
  //     clientNumber: 241173,
  //     clientName: 'Doe, John',
  //     referralDate: '10/10/2019 09:55',
  //     referalAgencySite: '2005-CAMBA/ 001- Gathering Place',
  //     referedBy: 'Kenneth Johnson',
  //     populationAppliedFor: 'AE',
  //     caseType: 'New',
  //     reviewerAssignedTo: '',
  //     reviewAssignedDate: '09/10/2019 10:39',
  //     caseStatus: 'Un-Assigned',
  //     comments: 'My Comments',
  //     reviewer: 0,
  //   },
  //   {
  //     srNo: 334611,
  //     clientNumber: 234611,
  //     clientName: 'Joe, Ram',
  //     referralDate: '09/10/2019 10:39',
  //     referalAgencySite: '2005-CAMBA/ 001- Gathering Place',
  //     referedBy: 'Ron Williams',
  //     populationAppliedFor: 'AH',
  //     caseType: 'Resubmission',
  //     reviewerAssignedTo: 'Elsa Stazesky',
  //     reviewAssignedDate: '09/10/2019 10:39',
  //     caseStatus: 'Pending Review',
  //     comments: '',
  //     reviewer: 0,
  //     originalReviewer: 'JAJ'
  //   }
  // ];



  constructor(public dialog: MatDialog) {
    this.gridOptions = {
      rowHeight: 30
    } as GridOptions;

    this.columnDefs = [
      {
        headerName: ' S.R.#',
        field: 'srNo',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        autoHeight: true
      },
      {
        headerName: 'Client #',
        field: 'clientNo',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        autoHeight: true,
      },
      {
        headerName: ' Client Name',
        field: 'clientName',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        autoHeight: true,
      },
      {
        headerName: 'Referral Date',
        field: 'referralDate',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        hide: false,
        autoHeight: true,
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        },
      },
      {
        headerName: 'Referral Agency/Site',
        field: 'referingAgengy',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        autoHeight: true
      },
      {
        headerName: 'Referral By',
        field: 'referedBy',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        autoHeight: true
      },
      {
        headerName: 'Pop Type',
        field: 'popType',
        width: 50,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        autoHeight: true
      },
      {
        headerName: 'Case Type',
        field: 'caseType',
        width: 50,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        hide: false
      },
      {
        headerName: 'Reviewer Assigned',
        field: 'reviewerAssigned',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        hide: false
      },
      {
        headerName: 'Date Assigned',
        field: 'dateAssigned',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        hide: true,
        suppressMenu: true
      },
      {
        headerName: 'Review Status',
        field: 'reviewStatus',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true

      },
      {
        headerName: 'Assignment Comments',
        field: 'comments',
        width: 150,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        hide: true,
      },
      {
        headerName: 'Actions',
        field: 'action',
        width: 65,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      },
      {
        headerName: 'Eligibility',
        field: 'eligibility',
        width: 150,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        hide: true
      },
      {
        headerName: 'Sign Off Date',
        field: 'signOffDate',
        width: 100,
        filter: 'agTextColumnFilter',
        sortable: false,
        resizable: false,
        suppressMenu: true,
        hide: true,
        autoHeight: true,
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        }
      }
    ];

    this.defaultColDef = {
      filter: true,
      flex: 1,
      resizable: true,
    };
    this.pagination = true;
    // this.context = { componentParent: this };

    this.frameworkComponents = {
      actionRenderer: CaseAssignmentActionComponent
    };

  }

  ngOnInit() {
    this.refreshDiagnosisList();
  }


  onGridReady = (params: { api: any; columnApi: any }) => {
    this.refreshDiagnosisList();
    this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while loading.</span>';
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Data Available</span>';

    this.gridOptions.api.refreshHeader();
    params.api.setDomLayout('autoHeight');
    this.gridOptions.columnApi.setColumnVisible('signOffDate', this.name == 'completed') //In that case we hide it
    this.gridOptions.columnApi.setColumnVisible('eligibility', this.name == 'followup') //In that case we hide it
    params.api.sizeColumnsToFit();
  }

  onPageSizeChanged(id: number) {

  }

  clear() {

  }
  refreshDiagnosisList() {
    if (this.caseList != undefined) {
      this.rowData = this.caseList;
    }
  }

}
