import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { MatDialog } from '@angular/material';
import { CompletedAppsDialogData } from '../my-worklist.model';
import { CompletedApplicationsDialogComponent } from '../completed-apps-dialog.component'

@Component({
  selector: "my-worklist-completed-action",
  template: `
    <mat-icon
      class="myWorklistCompletedMenu-icon"
      color="warn"
      [matMenuTriggerFor]="myWorklistCompletedAction">
      more_vert
    </mat-icon>
    <mat-menu #myWorklistCompletedAction="matMenu">
      <button mat-menu-item (click)="onAssignmentHistory()" class="menu-button">Assignment History</button>
      <button mat-menu-item (click)="onClientCaseFolder()" class="menu-button">Client Case Folder</button>
      <button mat-menu-item (click)="showClientDocuments()"> Client Documents</button>   
      <button mat-menu-item (click)="showApplcationPackage()"> Application Package </button>   
      <button mat-menu-item (click)="showDeterminationDocuments()">Determination Documents</button>
    </mat-menu>
  `,
  styles: [
    `
      .myWorklistCompletedMenu-icon {
        cursor: pointer;
      }
      .menu-button {
        line-height: 40px;
        width: 100%;
        height: 40px;
      }
    `
  ]
})

export class MyWorklistCompletedActionComponent implements ICellRendererAngularComp {
  params: any;
  cell: any;
  isSupervisor: boolean;
  selectedPactApplicationID: any;
//   optionsUserId: any;
//   isStartReview: boolean;
//   isContinueReview: boolean;

  //Constrctor
  constructor(
    public dialog: MatDialog
  ) {
  }

  //Init
  agInit(params: any) {
    this.params = params;
    this.selectedPactApplicationID = this.params.data.pactApplicationID;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    // this.isSupervisor = this.params.context.componentParent.isSupervisor;
    // this.selectedPactApplicationID = this.params.data.pactApplicationID;
    // this.isStartReview = this.params.data.caseStatusTypeId === 813 ? true : false;
    // this.isContinueReview = this.params.data.caseStatusTypeId === 814 ? true : false;
  }

  //Refresh
  refresh(): boolean {
    return false;
  }

  //On Client Case Folder
  onClientCaseFolder() {
    this.params.context.componentParent.onClientCaseFolder(this.params.data);
  }

  showClientDocuments() {
    let completedAppsDialogData = new CompletedAppsDialogData();
    completedAppsDialogData.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientId;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalExpiryDate ? (new Date(this.params.data.approvalExpiryDate)).toLocaleDateString() : '';
    completedAppsDialogData.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.expandSection = 4;
    completedAppsDialogData.pactApplicationId = 0;
    completedAppsDialogData.isReferralHistory = false;
    completedAppsDialogData.isCASAgency = true;
    this.openDialog(completedAppsDialogData);
  }  

  showApplcationPackage() {
    let completedAppsDialogData = new CompletedAppsDialogData();
    completedAppsDialogData.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientId;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalExpiryDate ? (new Date(this.params.data.approvalExpiryDate)).toLocaleDateString() : '';
    completedAppsDialogData.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.expandSection = 1;
    completedAppsDialogData.pactApplicationId = 0;
    completedAppsDialogData.isReferralHistory = false;
    completedAppsDialogData.isCASAgency = true;
    this.openDialog(completedAppsDialogData);
  }  

  showDeterminationDocuments() {
    let completedAppsDialogData = new CompletedAppsDialogData();
    completedAppsDialogData.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientId;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalExpiryDate ? (new Date(this.params.data.approvalExpiryDate)).toLocaleDateString() : '';
    completedAppsDialogData.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
    completedAppsDialogData.housingApplicationSupportingDocumentsData.expandSection = 2;
    completedAppsDialogData.pactApplicationId = 0;
    completedAppsDialogData.isReferralHistory = false;
    completedAppsDialogData.isCASAgency = true;
    this.openDialog(completedAppsDialogData);
  } 

   openDialog(completedAppsDialogData = new CompletedAppsDialogData()): void {
    this.dialog.open(CompletedApplicationsDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: completedAppsDialogData
    });
  }

  //On Assignment History
  onAssignmentHistory() {
    this.params.context.componentParent.onAssignmentHistory(this.params.data);
  }
}

