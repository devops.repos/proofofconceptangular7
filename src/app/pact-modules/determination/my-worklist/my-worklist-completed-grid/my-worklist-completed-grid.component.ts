import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
//AG Grid 
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridOptions } from 'ag-grid-community';
import * as moment from 'moment';
//import { DatePipe } from '@angular/common';
//Services
import { UserService } from 'src/app/services/helper-services/user.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
//import { ToastrService } from 'ngx-toastr';
import { MyWorklistService } from '../my-worklist.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';
import { MyWorklistCompletedActionComponent } from './my-worklist-completed-action.component';

//Models
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model'
import { MyWorklistCompletedList } from '../my-worklist.model';

@Component({
  selector: 'app-my-worklist-completed-grid',
  templateUrl: './my-worklist-completed-grid.component.html',
  styleUrls: ['./my-worklist-completed-grid.component.scss']
})

export class MyWorklistCompletedGridComponent implements OnInit {
  //Global Variables 
  @Input() name: string;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  defaultColDef: any;
  pagination: any;
  getRowHeight;
  pactApplicationId: number = 0;

  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;
  isSupervisor: boolean = false;
  rowSelection: any;
  context: any;

  myWorklistCompletedColumnDefs = [];
  myWorklistCompletedRowData: MyWorklistCompletedList[];
  myWorklistCompletedDefaultColDef = {};
  public myWorklistCompletedGridOptions: GridOptions;
  myWorklistCompletedOverlayNoRowsTemplate: string;
  myWorklistCompletedFrameworkComponents: any;
  myWorklistCompletedOverlayLoadingTemplate: string;

  applicationDeterminationData: iApplicationDeterminationData;

  constructor(private determinationSideNavService: DeterminationSideNavService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private router: Router,
    private commonService: CommonService,
    //private datePipe : DatePipe,
    private myWorklistService: MyWorklistService,
    private userService: UserService) {

    this.myWorklistCompletedGridOptions = {
      rowHeight: 30
    } as GridOptions;


    //My Worklist Completed Grid Column Definitions
    this.myWorklistCompletedColumnDefs = [
      { headerName: 'Application #', field: 'pactApplicationID', width: 160, filter: 'agTextColumnFilter' },
      { headerName: 'Client #', field: 'clientNumber', width: 100, filter: 'agTextColumnFilter' },
      { headerName: 'Client Name (L,N)', field: 'clientName', width: 160, filter: 'agTextColumnFilter' },
      {
        headerName: 'Referral Date', field: 'referralDate', width: 180, filter: 'agDateColumnFilter',
        // cellRenderer: (data: { value: string | number | Date; }) => {
        //   return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        // }
        comparator: this.commonService.dateComparator,
        filterParams: {
          // filterOptions: ['equals','inRange'],
          comparator(filterLocalDateAtMidnight, cellValue) {
            const dateAsString = cellValue;
            if (dateAsString == null) { return -1; }
            const dateParts = dateAsString.split('/');
            const day = Number(dateParts[1]);
            const month = Number(dateParts[0]) - 1;
            const year = Number(dateParts[2]);
            // console.log('day: ' + day + 'month: ' + month + 'year: ' + year);
            const cellDate = new Date(year, month, day);
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
      },
      { headerName: 'Referral Agency/Site', field: 'referalAgencySite', width: 300, filter: 'agTextColumnFilter' },
      { headerName: 'Referred By Name (L,F)', field: 'referredByName', width: 160, filter: 'agTextColumnFilter' },
      {
        headerName: 'Date Assigned', field: 'reviewAssignedDate', width: 160, filter: 'agTextColumnFilter',
        cellRenderer: (data: { value: string | number | Date; }) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : '';
        }
      },
      { headerName: 'Assigned To', field: 'assignedTo', width: 150, filter: 'agTextColumnFilter' },
      { headerName: 'Eligibility', field: 'eligibility', width: 130, filter: 'agTextColumnFilter' },
      { headerName: 'Prioritization', field: 'prioritization', width: 150, filter: 'agTextColumnFilter' },
      { headerName: 'Service Needs', field: 'serviceNeeds', width: 150, filter: 'agTextColumnFilter' },
      { headerName: 'Approval Period', field: 'approvalPeriod', width: 150, filter: 'agTextColumnFilter' },
      { headerName: 'Comments', field: 'comments', width: 300, filter: 'agTextColumnFilter' },
      // {
      //     headerName: 'Sign-off' Date, field: 'signOffDate', width: 160, filter: 'agTextColumnFilter',
      //     cellRenderer: (data: { value: string | number | Date; }) => {
      //       return data.value ? (new Date(data.value)).toLocaleDateString() : '';
      //     }
      // },
      { headerName: 'Actions', field: 'action', width: 65, filter: false, sortable: false, resizable: false, pinned: 'left', suppressMenu: true, suppressSizeToFit: true, cellRenderer: 'actionRenderer' },
    ];
    this.myWorklistCompletedDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true
    };

    this.pagination = true;
    this.rowSelection = 'single';
    this.context = { componentParent: this };

    this.myWorklistCompletedFrameworkComponents = {
      actionRenderer: MyWorklistCompletedActionComponent
    };

    this.getRowHeight = function (params: any) {
      return params.data.originalReviewer != null ? 30 : 30;
    };
  }

  ngOnInit() {
    //User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        if (this.userData.roleId == 2) {
          this.isSupervisor = true;
        }
        //Getting my worklist completed list details
        this.getMyWorklistCompletedList(this.userData.optionUserId);
      }
    });
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
  }

  //My Worklist Completed Grid Ready
  myWorklistCompletedGridReady(params: any) {
    params.api.setDomLayout('autoHeight');
    this.myWorklistCompletedOverlayLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while loading.</span>';
    this.myWorklistCompletedOverlayNoRowsTemplate = '<span style="color:#337ab7;">No Data Available</span>';

    // if (this.isSupervisor) {
    //   this.myWorklistFollowUpGridOptions.columnApi.setColumnsVisible(['referenceDate', 'applicationType'], true);
    //   this.myWorklistFollowUpGridOptions.columnApi.setColumnsVisible(['referralDate', 'reviewStartDate'], false);
    // } else {
    //   this.myWorklistFollowUpGridOptions.columnApi.setColumnsVisible(['referenceDate', 'applicationType'], false);
    //   this.myWorklistFollowUpGridOptions.columnApi.setColumnsVisible(['referralDate', 'reviewStartDate'], true);
    // }
  }

  //Get My Completed Work List
  getMyWorklistCompletedList(optionUserId: number) {
    this.myWorklistService.GetMyWorklistCompletedList(optionUserId)
      .subscribe(
        res => {
          if (res) {
            this.myWorklistCompletedRowData = res as MyWorklistCompletedList[];
          }
        },
        error => console.error('Error!', error)
      );
  }

  //On Page Size Changed
  onPageSizeChanged(selectedPageSize: any) {
    this.myWorklistCompletedGridOptions.api.paginationSetPageSize(Number(selectedPageSize));
    this.myWorklistCompletedGridOptions.api.refreshHeader();
  }

  //Has Original Reviewer
  hasOriginalReviewer(params: any) {
    return params.data.originalReviewer != null ? params.data.caseType + `<mat-icon class="mat-icon material-icons mat-icon-no-color" style="width: 50px;height: 50px;" matTooltip="C" matTooltipPosition="above">
    info</mat-icon>` : params.data.caseType;
  }

  //On Client Case Folder
  onClientCaseFolder(data: MyWorklistCompletedList) {
    if (data && data.pactApplicationID) {
      this.router.navigate(['/admin/client-case-folder', data.pactApplicationID]);
    }
  }

  //On Assignment History
  onAssignmentHistory(data: MyWorklistCompletedList) {
    if (data && data.pactApplicationID) {
     this.router.navigate(['/ds/assignment-history', data.pactApplicationID]);
    }
  }

  //Navigate Path
  navigatePath(pactApplicationId: number, applicationDeterminationData: iApplicationDeterminationData) {
    if (applicationDeterminationData) {
      var navigateTo: string = null;
      //Match Sources
      if (applicationDeterminationData.isMatchSummaryTabCompleted ||
        applicationDeterminationData.isHousingProgramMatchTabCompleted ||
        applicationDeterminationData.isDHSMatchTabCompleted ||
        applicationDeterminationData.isHASAMatchTabCompleted ||
        applicationDeterminationData.isMedicaidMatchTabCompleted ||
        applicationDeterminationData.isSTARSMatchTabCompleted) {
        navigateTo = "match-sources";
      }
      //Application
      if (applicationDeterminationData.isApplicationTabCompleted ||
        applicationDeterminationData.isSupportingDocumentsTabCompleted) {
        navigateTo = "application";
      }
      //Client Case Folder
      if (applicationDeterminationData.isCaseNotesTabCompleted ||
        applicationDeterminationData.isCoordinatedAssessmentSurveyTabCompleted ||
        applicationDeterminationData.isPriorApplicationsDeterminationsTabCompleted ||
        applicationDeterminationData.isClientDocumentsTabCompleted ||
        applicationDeterminationData.isVCSReferralsTabCompleted ||
        applicationDeterminationData.isVCSPlacementsTabCompleted ||
        applicationDeterminationData.isVPSReferralsTabCompleted) {
        navigateTo = "client-case-folder";
      }
      //Application Outcome
      if (applicationDeterminationData.isApplicationOutcomeTabCompleted ||
        applicationDeterminationData.isAdministrativeRejectTabCompleted ||
        applicationDeterminationData.isClinicalPreDeterminationTabCompleted ||
        applicationDeterminationData.isApplicationDeterminationLetterTabCompleted) {
        navigateTo = "outcome";
      }
      //Clinical Review
      if (applicationDeterminationData.isMentalHealthConditionTabCompleted ||
        applicationDeterminationData.isSubstanceUseTabCompleted ||
        applicationDeterminationData.isMedicalConditionsTabCompleted ||
        applicationDeterminationData.isClinicalAtRiskTabCompleted ||
        applicationDeterminationData.isClinicalSummaryTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsClinicalReviewDisabled(false);
        navigateTo = "clinical-review";
      }
      //Homeless Review
      if (applicationDeterminationData.isHousingHomelessTabCompleted ||
        applicationDeterminationData.isHomeless1To4YearsTabCompleted ||
        applicationDeterminationData.isResidentialTreatmentAndInstitutionalHistoryTabCompleted ||
        applicationDeterminationData.isHomelessAtRiskTabCompleted ||
        applicationDeterminationData.isHomelessSummaryTabCompleted ||
        applicationDeterminationData.isHousingHomelessReportTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsHomelessReviewDisabled(false);
        navigateTo = "homeless-review";
      }
      //Medicaid Prioritization
      if (applicationDeterminationData.isMedicaidClaimsTabCompleted ||
        applicationDeterminationData.isMedicaidPrioritizationTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(false);
        navigateTo = "medicaid-prioritization";
      }
      //Vulnerability Assessment
      if (applicationDeterminationData.isActivitiesOfDailyLivingTabCompleted ||
        applicationDeterminationData.isSVAReviewTabCompleted ||
        applicationDeterminationData.isVulnerabilityAssessmentTabCompleted ||
        applicationDeterminationData.isSVASummaryReportTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(false);
        navigateTo = "vulnerability-assessment";
      }
      //Determination Summary
      if (applicationDeterminationData.isDeterminationSummaryTabCompleted ||
        applicationDeterminationData.isConditionsRecommendationsTabCompleted ||
        applicationDeterminationData.isDeterminationLetterTabCompleted) {
        this.determinationSideNavService.setIsHousingDeterminationDisabled(false);
        this.determinationSideNavService.setIsDeterminationSummaryDisabled(false);
        navigateTo = "determination-summary";
      }
      //Sign Off
      if (applicationDeterminationData.isSignOffTabCompleted ||
        applicationDeterminationData.isFollowUpTabCompleted) {
        this.determinationSideNavService.setIsSignOffFollowUpDisabled(false);
        navigateTo = "sign-off-follow-up";
      }
      
      //Router Navigation 
      if (navigateTo) {
        this.router.navigate(['/ds/' + navigateTo, pactApplicationId]);
      }
      else {
        this.router.navigate(['/ds/match-sources', pactApplicationId]);
      }
    }
  }
}
