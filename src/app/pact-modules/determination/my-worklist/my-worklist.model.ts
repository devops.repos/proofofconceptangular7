export interface MyWorklistPendingList {
    srNo : number;
    pactApplicationID? : number;
    clientNumber? : number;
    clientName : string;
    referenceDate? : Date;
    referralDate? : Date;
    referalAgencySite : string;
    referredByName? : string;
    applicationType?: string; 
    populationAppliedFor? : string;
    caseType? : string;
    assignedTo : string;
    previouslyAssignedTo : string;
    sequenceId? : number
    reviewAssignedDate? : Date;
    reviewStartDate? : Date;
    caseStatus : string;
    caseStatusTypeId? : number;
    comments? : string;
    originalReviewer? : string;
    optionUserId: number;
}

export interface  StartReviewParameters{
    pactApplicationID : number;
    optionUserID: number;
}

export interface  MyWorklistStats{
    pendingAppsCount?: number;
    followUpAppsCount? : number;
    completedAppsCount? : number;
}

export interface MyWorklistFollowUpList {
    pactApplicationID : number;
    clientNumber? : number;
    clientName : string;
    referralDate? : Date;
    referalAgencySite : string;
    referredByName : string;
    assignedTo : string;
    reviewAssignedDate? : Date;
    caseType? : string;
    eligibility : string;
    prioritization : string;
    serviceNeeds : string;
    approvalPeriod : string;
    followUpStatus : string;
    followUpReason: string;
    followupComments: string;
    originalReviewer? : string;
    optionUserId: number;
}

export interface MyWorklistCompletedList {
    pactApplicationID : number;
    clientNumber? : number;
    clientName : string;
    referralDate : string;
    referalAgencySite : string;
    referredByName : string;
    reviewAssignedDate? : Date;
    assignedTo : string;
    caseType? : string;
    eligibility : string;
    prioritization : string;
    serviceNeeds : string;
    approvalPeriod : string;
    comments : string;
    signOffDate?: Date; 
    originalReviewer? : string;
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    approvalExpiryDate: string;
    optionUserId: number;
}

//Housing Application Supporting Documents - Class
export class HousingApplicationSupportingDocumentsData {
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    approvalExpiryDate: string;
    pactApplicationId: number;
    expandSection: number;
};

//Prior Supportive Housing Application Dialog Data
export class CompletedAppsDialogData {
    housingApplicationSupportingDocumentsData = new HousingApplicationSupportingDocumentsData();
    pactApplicationId: number;
    isReferralHistory: boolean;
    isCASAgency: boolean;
}

//Document Link
export interface iDocumentLink {
    linkURL: string;
  }
