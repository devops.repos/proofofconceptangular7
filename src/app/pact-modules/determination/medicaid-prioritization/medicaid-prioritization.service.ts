import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { MedicaidPrioritizationComponent } from './medicaid-prioritization.component';
import { MedicaidClaimsSummaryDetails, MedicaidClaimsSummaryInput } from './medicaid-prioritization.model';
import { environment } from '../../../../environments/environment';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class MedicaidPrioritizationService {
  getMedicaidClaimsSummaryDetailsUrl = environment.pactApiUrl + 'DETMedicaidPrioritization/GetMedicaidClaimsSummaryDetails'; 

  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    }),
    response : "json",
  };
    
  constructor(private httpClient: HttpClient) {
  }


      //Get Medicaid Claims Summary Details
  GetMedicaidClaimsSummaryDetails(medicaidClaimsSummaryInput : MedicaidClaimsSummaryInput): Observable<any> {
    if (medicaidClaimsSummaryInput){
      return this.httpClient.post(this.getMedicaidClaimsSummaryDetailsUrl, JSON.stringify(medicaidClaimsSummaryInput), this.httpOptions);
    }
  }


}