import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { GridOptions } from 'ag-grid-community';

//Service
import { UserService } from 'src/app/services/helper-services/user.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { MedicaidPrioritizationService } from './medicaid-prioritization.service';
import { ApplicationDeterminationService } from 'src/app/services/helper-services/application-determination.service';
import { DeterminationSideNavService } from 'src/app/core/sidenav-list/determination-sidenav-list/determination-sidenav-list.service';

//Models
import { DETTabTimeDuration } from 'src/app/models/determination-common.model';
import { MedicaidClaimsSummaryDetails, MedicaidClientSourceMatchSummary, MedicaidTotalSpendingRowData, InPatientStaysEmergencyRoom, MedicaidMonthlySpendingSummary, MedicaidClaimsSummaryInput } from './medicaid-prioritization.model';
import { iApplicationDeterminationData } from 'src/app/models/application-determination.model';
import { DETClinicalHomelessMedicaidReviewInput } from '../clinical-review/clinical-review.model';


@Component({
  selector: 'app-medicaid-prioritization',
  templateUrl: './medicaid-prioritization.component.html',
  styleUrls: ['./medicaid-prioritization.component.scss']
})

export class MedicaidPrioritizationComponent implements OnInit, OnDestroy {
  //Global Varaibles
  medicaidClaimsSummaryRowData: MedicaidClaimsSummaryDetails;
  inPatientStaysEmergencyRoomExpanded: boolean;
  medicaidMonthlySpendingExpanded: boolean;
  applicationDeterminationDataSub: Subscription;
  applicationDeterminationData: iApplicationDeterminationData;
  pactApplicationId: number = 0;
  detClinicalReviewInput: DETClinicalHomelessMedicaidReviewInput = {
    pactApplicationID: null, summaryType: null, clientCategoryType: null, userID: null
  };
  clientCategoryType: number;
  summaryType: number = 613; // Medicaid Prioritization

  isMedicaidClaimsTabCompleted: boolean = false;
  isMedicaidPrioritizationTabCompleted: boolean = false;
  tabSelectedIndex: number = 0;
  currentTabIndex: number = 0;
  activatedRouteSub: Subscription;
  userData: AuthData;
  userDataSub: Subscription;


  detTabTimeDuration: DETTabTimeDuration = {
    pactApplicationId: null,
    tabInName: null,
    tabOutName: null,
    optionUserId: null
  };

  medicaidClientSourceMatchColumnDefs = [];
  medicaidClientSourceMatchRowData: MedicaidClientSourceMatchSummary[];
  medicaidClientSourceMatchDefaultColDef = {};
  medicaidClientSourceMatchGridOptions: GridOptions;
  medicaidClientSourceMatchOverlayNoRowsTemplate: string;

  medicaidTotalSpendingColumnDefs = [];
  medicaidTotalSpendingRowData: MedicaidTotalSpendingRowData[];
  medicaidTotalSpendingDefaultColDef = {};
  medicaidTotalSpendingGridOptions: GridOptions;
  medicaidTotalSpendingOverlayNoRowsTemplate: string;

  inPatientStaysEmergencyRoomColumnDefs = [];
  inPatientStaysEmergencyRoomRowData: InPatientStaysEmergencyRoom[];
  inPatientStaysEmergencyRoomDefaultColDef = {};
  inPatientStaysEmergencyRoomGridOptions: GridOptions;
  inPatientStaysEmergencyRoomOverlayNoRowsTemplate: string;

  medicaidMonthlySpendingColumnDefs = [];
  medicaidMonthlySpendingRowData: MedicaidMonthlySpendingSummary[];
  medicaidMonthlySpendingDefaultColDef = {};
  medicaidMonthlySpendingGridOptions: GridOptions;
  medicaidMonthlySpendingOverlayNoRowsTemplate: string;

  //Constructor
  constructor(private router: Router,
    private route: ActivatedRoute,
    private determinationSideNavService: DeterminationSideNavService,
    private applicationDeterminationService: ApplicationDeterminationService,
    private commonService: CommonService,
    private medicaidPrioritizationService: MedicaidPrioritizationService,
    private userService: UserService) {
    //Medicaid Client Match Summary Grid Column Definitions
    this.medicaidClientSourceMatchColumnDefs = [
      { headerName: 'System', field: 'sourceSystemTypeDescription', filter: 'agTextColumnFilter', width: 250 },
      { headerName: 'Name (L, F)', field: 'clientName', filter: 'agTextColumnFilter', width: 300 },
      { headerName: 'Social Security #', field: 'ssn', filter: 'agTextColumnFilter', width: 300 },
      {
        headerName: 'Date of Birth', field: 'dob', filter: 'agTextColumnFilter', width: 250, cellRenderer: (data) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : ''
        }
      },
      { headerName: 'CIN', field: 'cin', filter: 'agTextColumnFilter', width: 250 }
    ];
    this.medicaidClientSourceMatchDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    //Medicaid Total Spending Grid Column Definitions
    this.medicaidTotalSpendingColumnDefs = [
      { headerName: 'Service Period', field: 'servicePeriod', filter: 'agTextColumnFilter', width: 350 },
      {
        headerName: 'Managed Care',
        field: 'managedCare',
        cellRenderer: function (params: { value: string; data: { managedCare: string }; }) {
          if (params.data.managedCare) {
            return '$' + params.data.managedCare;
          }
        },
        filter: 'agTextColumnFilter', width: 300
      },
      {
        headerName: 'FFS Claims',
        field: 'ffsClaims',
        cellRenderer: function (params: { value: string; data: { ffsClaims: string }; }) {
          if (params.data.ffsClaims) {
            return '$' + params.data.ffsClaims;
          }
        },
        filter: 'agTextColumnFilter', width: 300
      },
      {
        headerName: 'Total Claims',
        field: 'totalClaims',
        cellRenderer: function (params: { value: string; data: { totalClaims: string }; }) {
          if (params.data.totalClaims) {
            return '$' + params.data.totalClaims;
          }
        },
        filter: 'agTextColumnFilter', width: 300
      }
    ];
    this.medicaidTotalSpendingDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    //In Patient Stays Emergency Room Grid Column Definitions
    this.inPatientStaysEmergencyRoomColumnDefs = [
      { headerName: 'Medicaid #', field: 'medicaidID', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Type', field: 'type', filter: 'agTextColumnFilter', width: 200 },
      {
        headerName: 'Admission Date', field: 'admissionDate', filter: 'agTextColumnFilter', width: 200, cellRenderer: (data) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : ''
        }
      },
      {
        headerName: 'Discharge Date', field: 'dischargeDate', filter: 'agTextColumnFilter', width: 200, cellRenderer: (data) => {
          return data.value ? (new Date(data.value)).toLocaleDateString() : ''
        }
      },
      { headerName: 'Location', field: 'location', filter: 'agTextColumnFilter', width: 250 },
      { headerName: 'Description', field: 'description', filter: 'agTextColumnFilter', width: 350 }
    ];
    this.inPatientStaysEmergencyRoomDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    //Medicaid Monthly Spending Grid Column Definitions
    this.medicaidMonthlySpendingColumnDefs = [
      { headerName: 'Sequence #', field: 'sequenceID', filter: 'agTextColumnFilter', width: 200 },
      { headerName: 'Service Month', field: 'serviceMonth', filter: 'agTextColumnFilter', width: 200 },
      {
        headerName: 'Managed Care',
        field: 'managedCare',
        cellRenderer: function (params: { value: string; data: { managedCare: string }; }) {
          if (params.data.managedCare) {
            return '$' + params.data.managedCare;
          }
        },
        filter: 'agTextColumnFilter', width: 200
      },
      {
        headerName: 'FFS Claims',
        field: 'ffsClaims',
        cellRenderer: function (params: { value: string; data: { ffsClaims: string }; }) {
          if (params.data.ffsClaims) {
            return '$' + params.data.ffsClaims;
          }
        },
        filter: 'agTextColumnFilter', width: 250
      },
      {
        headerName: 'Total Claims',
        field: 'totalClaims',
        cellRenderer: function (params: { value: string; data: { totalClaims: string }; }) {
          if (params.data.totalClaims) {
            return '$' + params.data.totalClaims;
          }
        },
        filter: 'agTextColumnFilter', width: 250
      },
      {
        headerName: 'Cumulative Claims',
        field: 'cumulativeClaims',
        cellRenderer: function (params: { value: string; data: { cumulativeClaims: string }; }) {
          if (params.data.cumulativeClaims) {
            return '$' + params.data.cumulativeClaims;
          }
        },
        filter: 'agTextColumnFilter', width: 250
      }
    ];
    this.medicaidMonthlySpendingDefaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
  }

  //On Init
  ngOnInit() {
    //Side Navigation Service For Determination
    this.activatedRouteSub = this.route.paramMap.subscribe(param => {
      if (param) {
        const paramApplicationId = param.get('applicationId');
        if (paramApplicationId) {
          const numericApplicationId = parseInt(paramApplicationId);
          if (isNaN(numericApplicationId)) {
            throw new Error("Invalid application number!");
          } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
            this.applicationDeterminationService.setApplicationIdForDetermination(numericApplicationId);
            //User Data
            this.userDataSub = this.userService.getUserData().subscribe(res => {
              if (res) {
                this.userData = res;
                //Getting the application details for determination from ApplicationDeterminationService
                this.applicationDeterminationDataSub = this.applicationDeterminationService.getApplicationDeterminationData().subscribe(res => {
                  if (res) {
                    const data = res as iApplicationDeterminationData;
                    if (data) {
                      this.commonService.validateDetermination(data.caseAssignedTo, data.caseStatusType, this.userData.optionUserId);
                      this.applicationDeterminationData = data;
                      //Enable Pending Review on Page Load
                      if (this.applicationDeterminationData) {
                        this.determinationSideNavService.enablePendingReview(this.applicationDeterminationData);
                        this.determinationSideNavService.setIsMedicaidPrioritizationDisabled(false);
                      }
                      //Setting the local variables
                      if (this.applicationDeterminationData && this.applicationDeterminationData.clientCategoryType !== null)
                        this.clientCategoryType = this.applicationDeterminationData.clientCategoryType;

                      if (this.applicationDeterminationData && this.applicationDeterminationData.pactApplicationId !== null)
                        this.pactApplicationId = this.applicationDeterminationData.pactApplicationId;

                      //Clinical Review Input for all Tabs
                      if (this.pactApplicationId > 0) {
                        this.detClinicalReviewInput = {
                          pactApplicationID: this.pactApplicationId, summaryType: this.summaryType
                          , clientCategoryType: this.clientCategoryType, userID: this.userData.optionUserId
                        };
                      }
                      //Get Medicaid Claims Summary Details
                      this.getMedicaidClaimsSummaryDetails(this.applicationDeterminationData.pactApplicationId);
                    }
                  }
                });
              }
            });
          }
          else {
            return;
          }
        }
      }
    });
  }

  //On Destroy
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.applicationDeterminationDataSub) {
      this.applicationDeterminationDataSub.unsubscribe();
      this.saveTabTimeDuration();
    }
  }

  //Medicaid Client Grid Ready
  medicaidClientSourceMatchGridReady(params: { api: { setDomLayout: (arg0: string) => void; sizeColumnsToFit: () => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.medicaidClientSourceMatchOverlayNoRowsTemplate = '<span style="color: #337ab7">No Clients Source To Show</span>';
  }

  //Medicaid Total Spending Grid Ready
  medicaidTotalSpendingGridReady(params: { api: { setDomLayout: (arg0: string) => void; sizeColumnsToFit: () => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.medicaidTotalSpendingOverlayNoRowsTemplate = '<span style="color: #337ab7">No Medicaid Spending To Show</span>';
    //this.medicaidTotalSpendingRowData = null;
  }

  //In Patient Stays Emergency Room Grid Ready
  inPatientStaysEmergencyRoomGridReady(params: { api: { setDomLayout: (arg0: string) => void; sizeColumnsToFit: () => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.inPatientStaysEmergencyRoomOverlayNoRowsTemplate = '<span style="color: #337ab7">No In Patient Stays / Emergency Room Visits To Show</span>';
  }

  //Medicaid Monthly Spending Grid Ready
  medicaidMonthlySpendingGridReady(params: { api: { setDomLayout: (arg0: string) => void; sizeColumnsToFit: () => void; }; }) {
    params.api.setDomLayout('autoHeight');
    this.medicaidMonthlySpendingOverlayNoRowsTemplate = '<span style="color: #337ab7">No Medicaid Spending To Show</span>';
  }

  medicaidClaimsSummaryInput: MedicaidClaimsSummaryInput = {
    pactApplicationID: null,
    optionUserID: null
  }

  getMedicaidClaimsSummaryDetails = (pactApplicationId: number) => {
    if (pactApplicationId > 0) {
      this.medicaidClaimsSummaryInput.pactApplicationID = this.applicationDeterminationData.pactApplicationId;
      this.medicaidClaimsSummaryInput.optionUserID = this.userData.optionUserId;
      this.medicaidPrioritizationService.GetMedicaidClaimsSummaryDetails(this.medicaidClaimsSummaryInput)
        .subscribe(
          res => {
            this.medicaidClaimsSummaryRowData = res as MedicaidClaimsSummaryDetails;
            //console.log('medicaidClaimsSummaryRowData: ', this.medicaidClaimsSummaryRowData);
            this.medicaidClientSourceMatchRowData = this.medicaidClaimsSummaryRowData.medicaidClientSourceMatchSummary;
            this.inPatientStaysEmergencyRoomRowData = this.medicaidClaimsSummaryRowData.inPatientStaysEmergencyRoom;
            this.medicaidMonthlySpendingRowData = this.medicaidClaimsSummaryRowData.medicaidMonthlySpendingSummary;
            this.medicaidTotalSpendingRowData = [
              {
                servicePeriod: this.medicaidClaimsSummaryRowData.servicePeriod,
                managedCare: this.medicaidClaimsSummaryRowData.managedCare,
                ffsClaims: this.medicaidClaimsSummaryRowData.ffsClaims,
                totalClaims: this.medicaidClaimsSummaryRowData.totalClaims
              }];
            //Save Tab Time Duration
            // if (this.applicationDeterminationData.pactApplicationId) {
            //   this.saveTabTimeDuration();
            // }

          },
          error => console.error('Error!', error)
        );
    }
  }

  //Medicaid Prioritization Tab Change Event
  medicaidPrioritizationTabChange() {
    this.saveTabTimeDuration();
  }

  //Save Tab Time Duration
  saveTabTimeDuration() {
    let tabOutName: string;
    //debugger;
    if (this.currentTabIndex === 0) {
      tabOutName = "MedicaidClaims"
      this.currentTabIndex = this.currentTabIndex + 1;
    }
    else if (this.currentTabIndex === 1) {
      tabOutName = "MedicaidPrioritization";
      this.currentTabIndex = this.currentTabIndex - 1;
    }

    if (this.pactApplicationId) {

      this.detTabTimeDuration.pactApplicationId = this.pactApplicationId;
      this.detTabTimeDuration.tabOutName = tabOutName;
      this.detTabTimeDuration.optionUserId = this.userData.optionUserId;
      this.commonService.saveTabTimeDuration(this.detTabTimeDuration).subscribe();

    }
  }

  //Next Button Click
  nextTab() {
    //this.saveTabTimeDuration();
    if (this.tabSelectedIndex === 1) {
      if (this.applicationDeterminationData && this.applicationDeterminationData.siteType === '80') {
        this.determinationSideNavService.setIsDeterminationSummaryDisabled(false);
        this.router.navigate(['/ds/determination-summary', this.pactApplicationId]);
      }
      else {
        this.determinationSideNavService.setIsVulnerabilityAssessmentDisabled(false);
        this.router.navigate(['/ds/vulnerability-assessment', this.pactApplicationId]);
      }
    } else {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  //Previous Button Click
  previousTab() {
    //this.saveTabTimeDuration();
    if (this.tabSelectedIndex === 0) {
      this.router.navigate(['/ds/homeless-review', this.pactApplicationId]);
    } else {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }
}
