import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicaidPrioritizationComponent } from './medicaid-prioritization.component';

describe('MedicaidPrioritizationComponent', () => {
  let component: MedicaidPrioritizationComponent;
  let fixture: ComponentFixture<MedicaidPrioritizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicaidPrioritizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicaidPrioritizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
