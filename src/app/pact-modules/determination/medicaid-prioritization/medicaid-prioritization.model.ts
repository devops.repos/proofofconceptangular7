export interface MedicaidClaimsSummaryDetails {
    //Medicaid Program Enrollment Eligibility Details
    isProgramEnrollmentCriteria? : boolean;
    isServiceUtilization? : boolean;
    isMedicaidSpendingMH? : boolean;
    isMedicaidSpendingSUD? : boolean;
    currentHousingLocationType: string;
    inpatientStays? : number;
    emergencyRoomVisits?: number;
    totalClaims : string;
    tabTimeIn? : Date;
    tabTimeOut? : Date;

    //Medicaid Spending Summary (Total)
    servicePeriod : string;
    managedCare : string;
    ffsClaims : string;

    medicaidClientSourceMatchSummary: MedicaidClientSourceMatchSummary[];
    inPatientStaysEmergencyRoom: InPatientStaysEmergencyRoom[];
    medicaidMonthlySpendingSummary: MedicaidMonthlySpendingSummary[];
  }

  export interface MedicaidClientSourceMatchSummary
  {
    pactApplicationID? : number;
    sourceSystemTypeDescription : string;
    clientName : string;
    ssn : string;
    dob?: Date;
    cin: string;
  }

  export interface MedicaidTotalSpendingRowData
  {
    servicePeriod : string;
    managedCare : string;
    ffsClaims : string;
    totalClaims : string;
  }

  export interface InPatientStaysEmergencyRoom 
  {
      medicaidID : string;
      type : string;
      admissionDate? : Date; 
      dischargeDate? : Date; 
      location : string;
      description : string;
  }

  export interface MedicaidMonthlySpendingSummary
  {
      sequenceID? : number;
      serviceMonth : string;
      managedCare : string;
      ffsClaims : string;
      totalClaims : string;
      cumulativeClaims : string;
  }

  export interface MedicaidClaimsSummaryInput
  {
      pactApplicationID? : number;
      optionUserID? : number;
  }

