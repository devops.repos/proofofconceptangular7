import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';

import { CustomHeader } from '../case-assignment/custom-header.component';
//Service
import { UserService } from 'src/app/services/helper-services/user.service';

@Component({
  selector: 'app-auto-assignment',
  templateUrl: './auto-assignment.component.html',
  styleUrls: ['./auto-assignment.component.scss']
})

export class AutoAssignmentComponent implements OnInit, OnDestroy {
  
  @ViewChild('agGrid') agGrid: AgGridAngular;
  //Global Varaibles
  tabSelectedIndex: number = 0;
  userData: AuthData;
  userDataSub: Subscription;
  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  columnDefs;
  frameworkComponents;
  defaultColDef;
  rowData: [];
  isResub : boolean = false;
  isAuto : boolean = false;
  constructor(private userService: UserService) {
    
    this.columnDefs = [
      {
        field: 'athlete',
        suppressMenu: true,
        minWidth: 120,
      },
      {
        field: 'age',
        sortable: false,
        headerComponentParams: { menuIcon: 'fa-external-link-alt' },
      },
      {
        field: 'country',
        suppressMenu: true,
        minWidth: 120,
      },
      {
        field: 'year',
        sortable: false,
      },
      {
        field: 'date',
        suppressMenu: true,
      },
      {
        field: 'sport',
        sortable: false,
      },
      {
        field: 'gold',
        headerComponentParams: { menuIcon: 'fa-cog' },
        minWidth: 120,
      },
      {
        field: 'silver',
        sortable: false,
      },
      {
        field: 'bronze',
        suppressMenu: true,
        minWidth: 120,
      },
      {
        field: 'total',
        sortable: false,
      },
    ];
    
    this.frameworkComponents = { agColumnHeader: CustomHeader };
    this.defaultColDef = {
      editable: true,
      sortable: true,
      flex: 1,
      minWidth: 100,
      filter: true,
      resizable: true,
      headerComponentParams: { menuIcon: 'fa-bars' },
    };
  }

  onGridReady = (params: { api: any; columnApi: any }) => {    
    params.api.setDomLayout('autoHeight');
  }  

  ngOnInit() {
  }

  ngOnDestroy() {
  }
  
  nextTab() {
    if (this.tabSelectedIndex != 1) {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  previousTab() {
    if (this.tabSelectedIndex != 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }
  onChange($event) {
    if($event.value == 1){
      this.isAuto = true;
    }else this.isAuto = false;
  }

  assignCases() {
    alert("Assign Cases");
  }

  clear() {
    alert("Assign Cases");
  }

  bulkReassignCases() {
    this.isResub = this.isResub ? false : true;
  }   
  
}
