import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoAssignmentComponent } from './auto-assignment.component';

describe('AutoAssignmentComponent', () => {
  let component: AutoAssignmentComponent;
  let fixture: ComponentFixture<AutoAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
