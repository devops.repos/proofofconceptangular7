import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { TransmittedAppsList } from './transmitted-apps.model';
import { UserService } from '../../../services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { TransmittedApplicationsDialogComponent } from './transmitted-apps-dialog.component'
import { MatDialog } from '@angular/material';
import { CommonService } from '../../../services/helper-services/common.service';
import { TransmittedAppsDialogData } from './transmitted-apps.model'

@Component({
  selector: "transmitted-apps-action",
  template: `
    <mat-icon
      class="transmittedMenu-icon"
      color="warn"
      [matMenuTriggerFor]="transmittedAppsAction">
      more_vert
      btnClick ()
    </mat-icon>
    <mat-menu #transmittedAppsAction="matMenu">
      <div
      [matTooltip]="params.data.childApplicationID? 'You may not copy/resubmit this application. It is already awaiting to be transmitted [Service Request # : ' + params.data.childApplicationID + ']' : null"
      matTooltipPosition="right" matTooltipClass="pact-tooltip"
      [matTooltipDisabled]="params.data.isReSubmit">
        <button mat-menu-item id="btnReSubmit" (click)="onReSubmit()" [disabled]="!params.data.isReSubmit || isCASAgency == true ">
          ReSubmit
        </button>
      </div>
      <button mat-menu-item (click)="showAttachDocuments()" [disabled]="(isCASAgency == true || params.data.approvalStatusType !== 831)">
        Attach Documents
      </button>
      <button mat-menu-item (click)="showApplcationPackage()">
          Application Package
      </button>
      <button mat-menu-item (click)="showReferralHistory()">Referral History</button>
      <button mat-menu-item (click)="showDeterminationDocuments()" [disabled]="params.data.approvalStatusType !== 831 && params.data.approvalStatusType !== 832 && params.data.approvalStatusType !== 833 && params.data.approvalStatusType !== 835 ">Determination Documents</button>
    </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})

export class TransmittedAppsActionComponent implements ICellRendererAngularComp {
  //Global Variables
  params: any;
  public cell: any;
  applicationSelected: TransmittedAppsList;
  selectedApplicationID: any;
  clientName: string;
  isCASAgency: boolean;
  userData: AuthData;

  constructor(
    public dialog: MatDialog
    , private userService: UserService
    , private commonService: CommonService
  ) { }

  agInit(params: any): void {
    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });
    this.params = params;
    // console.log(this.params);
    this.selectedApplicationID = this.params.data.applicationNumber;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    this.isCASAgency = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
  }

  showAttachDocuments() {
    let transmittedAppsDialogData = new TransmittedAppsDialogData();
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientId;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalExpiryDate ? (new Date(this.params.data.approvalExpiryDate)).toLocaleDateString() : '';
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.applicationNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.expandSection = 3;
    transmittedAppsDialogData.pactApplicationId = 0;
    transmittedAppsDialogData.isReferralHistory = false;
    if (this.isCASAgency) {
      transmittedAppsDialogData.canShowPostApprovalUpload = false;
    }
    else {
      if (this.params.data.approvalStatusType === 831) {
        transmittedAppsDialogData.canShowPostApprovalUpload = true;
      }
      else {
        transmittedAppsDialogData.canShowPostApprovalUpload = false;
      }
    }
    this.openDialog(transmittedAppsDialogData);
  }

  showApplcationPackage() {
    let transmittedAppsDialogData = new TransmittedAppsDialogData();
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientId;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalExpiryDate ? (new Date(this.params.data.approvalExpiryDate)).toLocaleDateString() : '';
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.applicationNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.expandSection = 1;
    transmittedAppsDialogData.pactApplicationId = 0;
    transmittedAppsDialogData.isReferralHistory = false;
    if (this.isCASAgency) {
      transmittedAppsDialogData.canShowPostApprovalUpload = false;
    }
    else {
      if (this.params.data.approvalStatusType === 831) {
        transmittedAppsDialogData.canShowPostApprovalUpload = true;
      }
      else {
        transmittedAppsDialogData.canShowPostApprovalUpload = false;
      }
    }
    this.openDialog(transmittedAppsDialogData);
  }

  showDeterminationDocuments() {
    let transmittedAppsDialogData = new TransmittedAppsDialogData();
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientId;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalExpiryDate ? (new Date(this.params.data.approvalExpiryDate)).toLocaleDateString() : '';
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.applicationNumber;
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData.expandSection = 2;
    transmittedAppsDialogData.pactApplicationId = 0;
    transmittedAppsDialogData.isReferralHistory = false;
    if (this.isCASAgency) {
      transmittedAppsDialogData.canShowPostApprovalUpload = false;
    }
    else {
      if (this.params.data.approvalStatusType === 831) {
        transmittedAppsDialogData.canShowPostApprovalUpload = true;
      }
      else {
        transmittedAppsDialogData.canShowPostApprovalUpload = false;
      }
    }
    this.openDialog(transmittedAppsDialogData);
  }

  showReferralHistory() {
    let transmittedAppsDialogData = new TransmittedAppsDialogData();
    transmittedAppsDialogData.housingApplicationSupportingDocumentsData = null;
    transmittedAppsDialogData.pactApplicationId = this.params.data.applicationNumber;
    transmittedAppsDialogData.isReferralHistory = true;
    this.openDialog(transmittedAppsDialogData);
  }

  openDialog(transmittedAppsDialogData = new TransmittedAppsDialogData()): void {
    this.dialog.open(TransmittedApplicationsDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: transmittedAppsDialogData
    });
  }

  refresh(): boolean {
    return false;
  }

  //On Resubmit
  onReSubmit() {
    this.clientName = this.params.data.lastName + ", " + this.params.data.firstName;
    this.params.context.componentParent.onReSubmit(this.selectedApplicationID, this.clientName.toUpperCase());
  }
}
