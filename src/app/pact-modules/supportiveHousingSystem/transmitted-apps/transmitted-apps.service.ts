import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TransmittedAppsComponent } from './transmitted-apps.component';
import { TransmittedAppsList, transmittedApplicationListParameters } from './transmitted-apps.model';
import { environment } from '../../../../environments/environment';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransmittedAppsService {

  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    }),
    response : "json",
  };
    
  constructor(private httpClient: HttpClient) { }

  public transmittedAppParams: transmittedApplicationListParameters;
  getTransmittedApplicationListUrl = environment.pactApiUrl + 'TransmittedApplications/GetTransmittedApplicationList';
  

  getTransmittedApplicationList(optionUserID : number, selectedAgency : number) : Observable<any> 
  {
    this.transmittedAppParams = { optionUserID: optionUserID, agencyID: selectedAgency};
    if(optionUserID > 0) {
      return this.httpClient.post(this.getTransmittedApplicationListUrl, JSON.stringify(this.transmittedAppParams), this.httpOptions);
    }
     
  }

}
