export interface TransmittedAppsList{
    clientName : string;
    clientNumber : number;
    surveyNumber : number;
    applicationType: string;
    applicationNumber : number;
    applicationDate : Date;
    eligibility : string;
    approvalPeriod :string;
    serviceNeeds: string;
    prioritization : string;
    enteredBy : string;
    agency: string;
    site: string;
    approvalStatusDescription: string;
    approvalStatusType?: number;
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    approvalExpiryDate: string;
    expandSection: number;
    isReSubmit? : boolean;
    childApplicationID?: number;
    surveyReportFileNetDocID?: number;
    applicationSummaryReportFileNetDocID?: number;
    userId : number;
}


export interface AgencyData {
    agencyID:number;
    agencyNo:string;
    name:string;
    agencyType:string;
    agencyAddress:string;
    city:string;
    state:string;
    zip:string;
    userId?:number;
    userName: string;
  }

  export interface transmittedApplicationListParameters {
    optionUserID: number;
    agencyID: number;
  }
  
//Housing Application Supporting Documents - Class
export class HousingApplicationSupportingDocumentsData {
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    approvalExpiryDate: string;
    pactApplicationId: number;
    expandSection: number;
};

//Transmitted Apps Dialog Data
export class TransmittedAppsDialogData {
    housingApplicationSupportingDocumentsData = new HousingApplicationSupportingDocumentsData();
    pactApplicationId: number;
    isReferralHistory: boolean;
    canShowPostApprovalUpload: boolean;
}

//Document Link
export interface iDocumentLink {
  linkURL: string;
}