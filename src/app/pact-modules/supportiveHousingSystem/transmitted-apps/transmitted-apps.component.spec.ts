import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransmittedAppsComponent } from './transmitted-apps.component';

describe('TransmittedAppsComponent', () => {
  let component: TransmittedAppsComponent;
  let fixture: ComponentFixture<TransmittedAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransmittedAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransmittedAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
