import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TransmittedAppsDialogData } from './transmitted-apps.model'

@Component({
    selector: 'app-transmitted-apps-dialog',
    templateUrl: './transmitted-apps-dialog.component.html',
    styleUrls: ['./transmitted-apps-dialog.component.scss']
})

export class TransmittedApplicationsDialogComponent implements OnInit {
    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) public transmittedAppsDialogData = new TransmittedAppsDialogData(),
        private dialogRef: MatDialogRef<TransmittedApplicationsDialogComponent>) {
    }

    //Close the dialog on close button
    CloseDialog() {
        this.dialogRef.close(true);
    }

    //On Init
    ngOnInit() {
    }
}