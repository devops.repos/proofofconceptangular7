import { Component, OnInit, ViewChild } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';

//Ag-Grid References
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import * as moment from 'moment';

//Model References
import { TransmittedAppsList, AgencyData, iDocumentLink } from './transmitted-apps.model';

//Service References
import { UserService } from '../../../services/helper-services/user.service';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { TransmittedAppsService } from './transmitted-apps.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { SiteAdminService } from '../../vacancyControlSystem/agency-site-maintenance/site-admin.service';

//Component References
import { TransmittedAppsActionComponent } from './transmitted-apps-action.component';
import { CommonService } from '../../../services/helper-services/common.service';
import { ApplicantPreferencesComponent } from 'src/app/shared/applicant-preferences/applicant-preferences.component';

@Component({
  selector: 'app-transmitted-apps',
  templateUrl: './transmitted-apps.component.html',
  styleUrls: ['./transmitted-apps.component.scss']
})
export class TransmittedAppsComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  userData: AuthData;

  //Model Initialization
  trasmitttedAppsList: TransmittedAppsList = {
    clientName: null,
    clientNumber: null,
    surveyNumber: null,
    applicationType: null,
    applicationNumber: null,
    applicationDate: null,
    eligibility: null,
    approvalPeriod: null,
    serviceNeeds: null,
    prioritization: null,
    enteredBy: null,
    agency: null,
    site: null,
    approvalStatusDescription: null,
    approvalStatusType: null,
    agencyNumber: null,
    siteNumber: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    approvalExpiryDate: null,
    expandSection: null,
    isReSubmit: null,
    childApplicationID: null,
    surveyReportFileNetDocID: null,
    applicationSummaryReportFileNetDocID: null,
    userId: null
  }

  //ag-grid variables
  gridApi: any;
  gridColumnApi: any;
  pagination: any;
  rowSelection: any;
  defaultColDef: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  public gridOptions: GridOptions;
  context: any;
  widthSideBar: number;
  widthSideBarSub: Subscription;
  rowData: TransmittedAppsList[];
  transmittedData: TransmittedAppsList[];
  agencyList: AgencyData[];
  agencyInfo: AgencyData;
  agencyID;
  isCASAgency;
  searchStringCtrl = new FormControl();
  agencyGroup: FormGroup;
  filteredAgencyData: Observable<any[]>;

  //Global Variables
  transmittedAppsCount: number;
  agencyName: string;
  pactApplicationId: number;

  //ag-grid Columns
  columnDefs = [
    {
      headerName: 'Survey Number',
      field: 'surveyNumber',
      cellRenderer: (params: { value: string; data: { capsReportId: string; surveyReportFileNetDocID: number }; }) => {
        var link = document.createElement('a');
        link.href = '#';
        link.innerText = params.value;
        link.addEventListener('click', (e) => {
          e.preventDefault();
          if (params.data.capsReportId) {
            this.openSurveyReport(params.data.surveyReportFileNetDocID, params.data.capsReportId, this.userData.optionUserId);
          }
        });
        return link;
      },
      filter: 'agTextColumnFilter',
      width: 130
    },
    {
      headerName: 'Application Number',
      field: 'applicationNumber',
      cellRenderer: (params: { value: string; data: { applicationNumber: number; applicationSummaryReportFileNetDocID: number }; }) => {
        var link = document.createElement('a');
        link.href = '#';
        link.innerText = params.value;
        link.addEventListener('click', (e) => {
          e.preventDefault();
          this.commonService.setIsOverlay(true);
          this.openApplicationSummaryReport(params.data.applicationSummaryReportFileNetDocID, params.data.applicationNumber.toString(), this.userData.optionUserId);
        });
        return link;
      },
      width: 155,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Client Name (L,F)',
      field: 'clientName',
      width: 160,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'HRA Client ID',
      field: 'clientNumber',
      width: 130,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Application Type',
      field: 'applicationType',
      width: 150,
      filter: 'agTextColumnFilter',
      hide: true
    },
    {
      headerName: 'Transmit Date',
      field: 'applicationDate',
      width: 150,
      filter: 'agTextColumnFilter',
      cellRenderer: (data: { value: string | number | Date; }) => {
        return data.value ? moment(data.value).format('MM/DD/YYYY hh:mm A') : '';
      }
    },
    {
      headerName: 'Agency',
      field: 'agency',
      width: 230,
      filter: 'agTextColumnFilter',
      tooltipField: 'agency'
    },
    {
      headerName: 'Site',
      field: 'site',
      width: 230,
      filter: 'agTextColumnFilter',
      tooltipField: 'site'
    },
    {
      headerName: 'Entered By (L,N)',
      field: 'enteredBy',
      width: 130,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Approval Status',
      field: 'approvalStatusDescription',
      width: 130,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Eligibility',
      field: 'eligibility',
      width: 130,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Approval Period',
      field: 'approvalPeriod',
      width: 150,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Service Needs',
      field: 'serviceNeeds',
      width: 150,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Prioritization',
      field: 'prioritization',
      width: 130,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Actions',
      field: 'action',
      width: 70,
      filter: false,
      sortable: false,
      resizable: false,
      pinned: 'left',
      suppressMenu: true,
      suppressSizeToFit: true,
      cellRenderer: 'actionRenderer',
    }
  ];

  constructor(private transmittedAppsService: TransmittedAppsService
    , private userService: UserService
    , private siteAdminservice: SiteAdminService
    , private route: ActivatedRoute
    , private confirmDialogService: ConfirmDialogService
    , private navService: NavService
    , private sidenavStatusService: SidenavStatusService
    , private commonService: CommonService
    , private router: Router
    , private formBuilder: FormBuilder
  ) {

    this.agencyGroup = this.formBuilder.group({
      searchStringCtrl: ['']
    });

    this.gridOptions = {
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.rowSelection = 'single';
    this.pagination = true;
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: TransmittedAppsActionComponent
    };
  }

  ngOnInit() {
    /** Setting the sidenav Status for the application with the given ID from URL router parameter **/
    /** This is the case for page refresh or reload **/
    // this.sidenavStatusService._sidenavStatusReload(this.route);

    this.userService.getUserData().subscribe(res => { this.userData = res });

    this.agencyName = this.userData.agencyName + "/" + this.userData.agencyNo;

    /** Getting the width of the sidenav to reflect the changes to nav toggle */
    this.widthSideBarSub = this.navService.getWidthSideBar().subscribe(res => {
      this.widthSideBar = res;
    });
  }

  onGridReady(params: { api: { setDomLayout: (arg0: string) => void; }; columnApi: any; }) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;

    // var allColumnIds = [];
    // this.gridColumnApi.getAllColumns().forEach(function (column: { colId: string; }) {
    //   if (column.colId !== 'action') {
    //     if (column.colId !== 'eligibility') {
    //       allColumnIds.push(column.colId);
    //     }
    //   }
    // });
    //this.gridColumnApi.autoSizeColumns(allColumnIds);
    this.isCASAgency = false;
    this.agencyInfo = {} as AgencyData;

    this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        if (this.userData.siteCategoryType.length > 0) {
          this.isCASAgency = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
        }
        if (this.userData.agencyId > 0) {
          this.agencyID = this.userData.agencyId;
        }

        // If the user is from CAS Agency, Load the ag grid based on the drop down selected value
        // If the user is not from CAS Agency, we will load the Grid with all the applications the User has access i.e applications created by him & his subordinates
        if (this.isCASAgency == true) {
          this.agencyID = 0
        }
        this.siteAdminservice.getAgencyList(null, null)
          .subscribe(
            res => {
              this.agencyList = res.body as AgencyData[];
              if (this.agencyList.length > 0) {
                this.filteredAgencyData = this.searchStringCtrl.valueChanges.pipe(
                  startWith(''),
                  map(value => this._filter(value))
                );
              }
            },
            error => console.error('Error!', error)
          );
      }

      /** API call to get the grid data */
      this.transmittedAppsService.getTransmittedApplicationList(this.userData.optionUserId, this.agencyID)
        .subscribe(
          res => {
            if (res) {
              this.transmittedData = res as TransmittedAppsList[];
              this.transmittedAppsCount = this.transmittedData.length;
              //Get Pact Application ID From Router Parameter
              this.route.paramMap.subscribe(params => {
                const pactAppId = +params.get('applicationID');
                if (pactAppId) {
                  this.pactApplicationId = pactAppId;
                  this.rowData = res.filter((x: { applicationNumber: number; }) => x.applicationNumber === this.pactApplicationId) as TransmittedAppsList[];
                  setTimeout(() => {
                    const instance = this.gridApi.getFilterInstance('applicationNumber');
                    instance.setModel({
                      type: 'equals',
                      filter: this.pactApplicationId
                    });
                    this.gridApi.onFilterChanged();
                  }, 0);
                }
                else {
                  this.rowData = res as TransmittedAppsList[];
                }
              });
            }
          },
          error => console.error('Error!', error)
        );  //Api call ends

    });
  }

  private _filter(value: string): AgencyData[] {
    const filterValue = value.toLowerCase();
    return this.agencyList.filter(agency => agency.name.toLowerCase().includes(filterValue) || agency.agencyNo.includes(filterValue));
  }

  onPageSizeChanged(selectedPageSize) {
    this.gridOptions.api.paginationSetPageSize(Number(selectedPageSize));
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
    this.rowData = this.transmittedData as TransmittedAppsList[];
  }

  updateAgGrid(value: AgencyData) {

    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    /** API call to get the grid data */
    this.transmittedAppsService.getTransmittedApplicationList(this.userData.optionUserId, value.agencyID)
      .subscribe(
        res => {
          if (res) {
            this.transmittedData = res as TransmittedAppsList[];
            this.transmittedAppsCount = this.transmittedData.length;
            //Get Pact Application ID From Router Parameter
            this.route.paramMap.subscribe(params => {
              const pactAppId = +params.get('applicationID');
              if (pactAppId) {
                this.pactApplicationId = pactAppId;
                this.rowData = res.filter((x: { applicationNumber: number; }) => x.applicationNumber === this.pactApplicationId) as TransmittedAppsList[];
                setTimeout(() => {
                  const instance = this.gridApi.getFilterInstance('applicationNumber');
                  instance.setModel({
                    type: 'equals',
                    filter: this.pactApplicationId
                  });
                  this.gridApi.onFilterChanged();
                }, 0);
              }
              else {
                this.rowData = res as TransmittedAppsList[];
              }
            });
          }
        },
        error => console.error('Error!', error)
      );  //Api call ends
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');
    const params = {
      fileName: 'transmittedApplicationList-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.gridApi.exportDataAsExcel(params);
  }

  //On ReSubmit
  onReSubmit(selectedApplicationID: number, clientName: string) {
    this.confirmDialogService.confirmDialog('Confirm ReSubmit', '', 'Wish to Resubmit/Copy the Application Package for client ' + clientName + '?', 'Yes', 'No')
      .then(() => {
        this.router.navigate(['/shs/newApp/consent-search', selectedApplicationID]);
      }, () => {
      });
  }

  //Open Survey Report
  openSurveyReport(surveyReportFileNetDocID: number, capsReportId: string, optionUserId: number) {
    if (surveyReportFileNetDocID) {
      this.commonService.getFileNetDocumentLink(surveyReportFileNetDocID.toString()).subscribe(res => {
        const data = res as iDocumentLink;
        if (data && data.linkURL) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    }
    else {
      this.commonService.displaySurveySummaryReport(capsReportId, optionUserId);
    }
  }

  //Open Application Summary Report
  openApplicationSummaryReport(applicationSummaryReportFileNetDocID: number, selectedApplicationID: string, optionUserId: number) {
    if (applicationSummaryReportFileNetDocID) {
      this.commonService.getFileNetDocumentLink(applicationSummaryReportFileNetDocID.toString()).subscribe(res => {
        const data = res as iDocumentLink;
        if (data && data.linkURL) {
          this.commonService.OpenWindow(data.linkURL);
          this.commonService.setIsOverlay(false);
        }
      });
    }
    else {
      this.commonService.displayApplicationSummaryReport(selectedApplicationID, optionUserId);
    }
  }
}
