import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ClinicalAssessmentModel } from './clinical-assessment.model'
// import { PACTClinicalMHRDiagnosisDetailsInput } from '../../../shared/diagnosis/diagnosis.model';

@Injectable({
  providedIn: 'root'
})
export class ClinicalAssessmentService {
  saveClinicalAssessmentURL = environment.pactApiUrl + 'ClinicalAssessment/SaveAssessment';
  getClinicalAssessmentURL = environment.pactApiUrl + 'ClinicalAssessment/GetAssessment';
  // saveDiagnosisURL = environment.pactApiUrl + 'ClinicalAssessment/AddDiagnosis';
  // deleteDiagnosisURL = environment.pactApiUrl + 'ClinicalAssessment/DeleteDiagnosis';
  getAOTProgramURL = environment.pactApiUrl + 'ClinicalAssessment/GetAOTProgram';
  getRefGroupDetailsURL = environment.pactApiUrl + 'Common';
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }
  
  getAssessment(pactapplicationID : number) : Observable<any>
  {
    return this.httpClient.get(this.getClinicalAssessmentURL+"/"+pactapplicationID, this.httpOptions);
  }

  getAOTPrograms() : Observable<any>
  {
    return this.httpClient.get(this.getAOTProgramURL, this.httpOptions);
  }

  saveAssessment(clinicalAssessment : ClinicalAssessmentModel) : Observable<any>
  {
    return this.httpClient.post(this.saveClinicalAssessmentURL, JSON.stringify(clinicalAssessment), this.httpOptions);
  }
  
  // saveDiagnosis(diagnosis : PACTClinicalMHRDiagnosisDetailsInput) : Observable<any>
  // {
  //   return this.httpClient.post(this.saveDiagnosisURL, JSON.stringify(diagnosis), this.httpOptions);
  // }
  
  // deleteDiagnosis(diagnosis : PACTClinicalMHRDiagnosisDetailsInput) : Observable<any>
  // {
  //   return this.httpClient.post(this.deleteDiagnosisURL, JSON.stringify(diagnosis), this.httpOptions);
  // }
}

