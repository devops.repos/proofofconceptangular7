import { PACTClinicalMHRDiagnosisDetails } from '../../../shared/diagnosis/diagnosis.model'
export interface ClinicalAssessmentModel {
    id : number;
    hasMedicalDiagnosis : boolean;
    hasPrincipalDiagnosis : boolean;
    hasOtherDiagnosis : boolean;
    hasLimitedDailyActivities : string;
    hasHIVDiagnosis : number;
    isReceivingServices : number;
    hasAOT : number;
    aotProgramType : number;
    aotPhoneNumber: string;
    hasACT : number;
    actProgramName : string;
    actPhoneNumber : string;
    isMedicalDiagnosisCompleted : boolean;
    isPsychiatricDiagnosisCompleted : boolean;
    isAOTACTCompleted : boolean;
    isACTCompleted : boolean;
    userID? : number;
    diagnosisList : PACTClinicalMHRDiagnosisDetails[];
    // medicalDiagnosisList : DiagnosisDetails[];
    // psychiatricDiagnosisList : DiagnosisDetails[];
    // otherDiagnosisList : DiagnosisDetails[];
  }
  export interface IAOTPrograms{
    programID : number;
    programName : string; 
    phoneNumber : string;
  }