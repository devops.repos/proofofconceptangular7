import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClinicalAssessmentService } from './clinical-assessment.service';
import { CommonService } from '../../../services/helper-services/common.service';
import { RefGroupDetails } from '../../../models/refGroupDetailsDropDown.model';
import { PACTClinicalMHRDiagnosisDetails, PACTClinicalMHRDiagnosisDetailsInput } from '../../../shared/diagnosis/diagnosis.model';
import { ClinicalAssessmentModel } from './clinical-assessment.model'
import { IAOTPrograms } from './clinical-assessment.model'
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { Router } from '@angular/router';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { DiagnosisService } from 'src/app/shared/diagnosis/diagnosis.service';

@Component({
  selector: 'app-clinical-assessment',
  templateUrl: './clinical-assessment.component.html',
  styleUrls: ['./clinical-assessment.component.scss']
})
export class ClinicalAssessmentComponent implements OnInit, OnDestroy {
  userData: AuthData;
  applicationID: number;
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  tab1Status: number = 2;
  tab2Status: number = 2;
  tab3Status: number = 2;
  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;
  isLoading: boolean = false;
  riskOf: boolean = false;
  historyOf: boolean = false;
  hasHIVDiagnosis: boolean = false;
  isReceivingServices: boolean = false;
  isReceivingServicesVisible: boolean = false;

  responseItem: RefGroupDetails[];
  responseItemChk: RefGroupDetails[];
  limitActivitie: RefGroupDetails[];
  aOTPrograms: IAOTPrograms[];
  caDetails: ClinicalAssessmentModel;

  medicalDiagnosisGroup: FormGroup;
  psychiatricDiagnosisGroup: FormGroup;
  oACTOCTGroup: FormGroup;
  hasAOTProgram: boolean;
  hasACTProgram: boolean;
  selectedAOTNo: number;
  selectedAOT: IAOTPrograms;
  mdList: PACTClinicalMHRDiagnosisDetails[] = [];
  pdList: PACTClinicalMHRDiagnosisDetails[] = [];
  odList: PACTClinicalMHRDiagnosisDetails[] = [];

  diagnosisList: PACTClinicalMHRDiagnosisDetails[] = [];
  diagnosisSearchList: PACTClinicalMHRDiagnosisDetails[] = [];
  medicalDiagnosisList: PACTClinicalMHRDiagnosisDetails[] = [];
  psychiatricDiagnosisList: PACTClinicalMHRDiagnosisDetails[] = [];
  otherDiagnosisList: PACTClinicalMHRDiagnosisDetails[] = [];

  medicDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 48, historyOf: true, riskOf: true, provisional: false };
  principalDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 49, historyOf: true, riskOf: true, provisional: true };
  otherDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 51, historyOf: false, riskOf: true, provisional: false };

  hasOtherDiagnosis: boolean;
  hasPrincipalDiagnosis: boolean;
  hasMedicalDiagnosis: boolean;
  message: string;
  tabSelectedIndex: number = 0;
  isDiagnosisTouched: boolean = false;
  routeSub: any;

  constructor(
    private formBuilder: FormBuilder,
    private cinicalAssessmentService: ClinicalAssessmentService,
    private commonService: CommonService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private sidenavStatusService: SidenavStatusService,
    private userService: UserService,
    private diagnosisService: DiagnosisService

  ) {
    this.medicalDiagnosisGroup = this.formBuilder.group({
      diagnosisHIVCtrl: [''],
      receivingServicesCtrl: [''],
      limitedDailyActivitiesCtrl: ['']
    });
    this.psychiatricDiagnosisGroup = this.formBuilder.group({});
    this.oACTOCTGroup = this.formBuilder.group({
      aOTProgramCtrl: [''],
      aCTProgramCtrl: [''],
      aOTProgramNameCtrl: [''],
      aCTProgramNameCtrl: ['', [CustomValidators.requiredCharLength(2), CustomValidators.blankspaceValidator]],
      aCTPhoneNumberCtrl: ['']
    });
  }

  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
    this.caDetails = null;
  }

  @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
    if (this.medicalDiagnosisGroup.dirty) {
      this.onSave(false);
    }
  }

  ngOnChanges() {
    this.oACTOCTGroup.get("limitedDailyActivitiesCtrl").setValue(this.caDetails.hasLimitedDailyActivities);
    if (this.mdList.length < 1) {
      this.medicalDiagnosisGroup.get("limitedDailyActivitiesCtrl").setValue(34);
      this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').disable();
    } else {
      this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').enable();
    }
  }

  ngOnInit() {
    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.applicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);

          this.OnInit();
        }
      }
    });

    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.onSave(false);
      }
    });
  }

  OnInit() {
    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    this.getclinicalAssessment();
    this.getRefGroupItems();
  }

  getRefGroupItems() {
    this.commonService.getRefGroupDetails('7').subscribe(
      res => {
        const data = res.body as RefGroupDetails[];
        this.responseItem = data.filter(d => {
          return (
            (d.refGroupID === 7 && d.refGroupDetailID == 33) ||
            d.refGroupDetailID == 34 ||
            d.refGroupDetailID == 335
          );
        });
        this.responseItemChk = data.filter(d => {
          return (
            (d.refGroupID === 7 && d.refGroupDetailID == 33) ||
            d.refGroupDetailID == 34
          );
        });
        this.limitActivitie = data.filter(d => {
          return (
            (d.refGroupID === 7 && d.refGroupDetailID == 33) ||
            d.refGroupDetailID == 34
          );
        });
      },
      error => console.error('Error!', error)
    );
    this.cinicalAssessmentService.getAOTPrograms().subscribe(
      data => {
        if (data != null) {
          this.aOTPrograms = data;
        }
      },
      error => console.error('Error!', error)
    );

    this.commonService.getRefDiagnosis().subscribe(
      res => {
        const data = res as PACTClinicalMHRDiagnosisDetails[];
        this.diagnosisSearchList = data;
      },
      error => console.error('Error!', error)
    );
  }

  initTabStatus() {
    this.mdTabStatus();
    this.pdTabStatus();
    this.aotTabStaus();
  }

  mdTabStatus() {
    if (this.caDetails.hasHIVDiagnosis === 33) {
      this.isReceivingServicesVisible = true;
      if (this.caDetails.hasMedicalDiagnosis && this.caDetails.isReceivingServices !== null && this.caDetails.hasHIVDiagnosis !== null) {
        this.tab1Status = 1;
      } else {
        this.tab1Status =
          this.mdList.length > 0 &&
            this.caDetails.hasLimitedDailyActivities !== null && this.caDetails.isReceivingServices !== null && this.caDetails.hasHIVDiagnosis !== null
            ? 1
            : 0;
      }
    }
    else {
      this.isReceivingServicesVisible = false;
      if (this.caDetails.hasMedicalDiagnosis && this.caDetails.hasHIVDiagnosis !== null) {
        this.tab1Status = 1;
      } else {
        this.tab1Status =
          this.mdList.length > 0 &&
            this.caDetails.hasLimitedDailyActivities !== null && this.caDetails.hasHIVDiagnosis !== null
            ? 1
            : 0;
      }
    }
  }

  pdTabStatus() {
    if (this.caDetails.hasPrincipalDiagnosis && this.caDetails.hasOtherDiagnosis) {
      this.tab2Status = 1;
    }
    else if (!this.caDetails.hasPrincipalDiagnosis && this.pdList.length > 0 && !this.caDetails.hasOtherDiagnosis && this.odList.length > 0) {
      this.tab2Status = 1;
    }
    else if (!this.caDetails.hasPrincipalDiagnosis && this.pdList.length < 1 && !this.caDetails.hasOtherDiagnosis && this.odList.length < 1) {
      this.tab2Status = 0;
    }
    else if (!this.caDetails.hasPrincipalDiagnosis && this.pdList.length > 0 && this.caDetails.hasOtherDiagnosis) {
      this.tab2Status = 1;
    }
    else if (!this.caDetails.hasOtherDiagnosis && this.odList.length > 0 && this.caDetails.hasPrincipalDiagnosis) {
      this.tab2Status = 1;
    }
    else if (!this.caDetails.hasPrincipalDiagnosis && this.pdList.length > 0 && !this.caDetails.hasOtherDiagnosis) {
      this.tab2Status = 0;
    }
    else if (!this.caDetails.hasOtherDiagnosis && this.odList.length > 0 && !this.caDetails.hasPrincipalDiagnosis) {
      this.tab2Status = 0;
    }
    else if (!this.caDetails.hasOtherDiagnosis && this.odList.length < 1 && this.caDetails.hasPrincipalDiagnosis) {
      this.tab2Status = 0;
    }
    else if (!this.caDetails.hasPrincipalDiagnosis && this.pdList.length < 1 && this.caDetails.hasOtherDiagnosis) {
      this.tab2Status = 0;
    }
  }
  aotTabStaus() {
    if (this.caDetails.hasAOT === 33 && this.caDetails.hasACT === 33) {
      if (this.caDetails.aotProgramType !== 0 && this.caDetails.actProgramName !== '') {
        this.tab3Status = 1;
      }
      else {
        this.tab3Status = 0;
      }
      return;
    }

    if (this.caDetails.hasAOT === 0 && this.caDetails.hasACT === 0) {
      this.tab3Status = 0; //2?
      return;
    }

    if (this.caDetails.hasAOT > 33 && this.caDetails.hasACT > 33) {
      this.tab3Status = 1;
      return;
    }

    if (this.caDetails.hasAOT === 33 && this.caDetails.hasACT > 33) {
      if (this.caDetails.aotProgramType !== 0 && this.caDetails.aotProgramType !== null) {
        this.tab3Status = 1;
      }
      else {
        this.tab3Status = 0;
      }
      return;
    }

    if (this.caDetails.hasAOT > 33 && this.caDetails.hasACT === 33) {
      if (this.caDetails.actProgramName !== '') {
        this.tab3Status = 1;
      }
      else {
        this.tab3Status = 0;
      }
      return;
    }

    if (this.caDetails.hasAOT > 33 && this.caDetails.hasACT === 33) {
      if (this.caDetails.actProgramName !== '') {
        this.tab3Status = 1;
      } else {
        this.tab3Status = 0;
      }
    }

    if (this.caDetails.hasAOT === 0 || this.caDetails.hasACT === 0) {
      this.tab3Status = 0;
      return;
    }

    if (this.caDetails.hasAOT === null || this.caDetails.hasACT === null) {
      this.caDetails.hasAOT === null ? 0 : this.caDetails.hasAOT;
      this.caDetails.hasACT === null ? 0 : this.caDetails.hasACT;
      this.tab3Status = 0;
    }
  }

  getclinicalAssessment() {
    this.cinicalAssessmentService.getAssessment(this.applicationID).subscribe(
      res => {
        const data = res as ClinicalAssessmentModel;
        if (data !== null) {
          this.caDetails = data;
          //console.log('GEt ',data.diagnosisList);
          this.diagnosisList = data.diagnosisList;
          this.refreshDiagnosisList();
          this.medicalDiagnosisGroup.setValue({
            limitedDailyActivitiesCtrl:
              this.caDetails.hasLimitedDailyActivities === null
                ? 0
                : this.caDetails.hasLimitedDailyActivities,
            diagnosisHIVCtrl: this.caDetails.hasHIVDiagnosis,
            receivingServicesCtrl: this.caDetails.isReceivingServices
          });

          if (this.caDetails.id != 0) {
            this.initTabStatus();
          }

          this.oACTOCTGroup.setValue({
            aOTProgramCtrl:
              this.caDetails.hasAOT === null ? 0 : this.caDetails.hasAOT,
            aCTProgramCtrl:
              this.caDetails.hasACT === null ? 0 : this.caDetails.hasACT,
            aOTProgramNameCtrl:
              this.caDetails.aotProgramType === null
                ? 0
                : this.caDetails.aotProgramType,
            aCTProgramNameCtrl: this.caDetails.actProgramName,
            aCTPhoneNumberCtrl: this.caDetails.actPhoneNumber
          });

          this.hasAOTProgram = this.caDetails.hasAOT == 33;
          this.hasACTProgram = this.caDetails.hasACT == 33;

          if (this.caDetails.hasMedicalDiagnosis) {
            this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').disable();
          } else {
            this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').enable();
          }

          if (this.mdList.length < 1) {
            this.medicalDiagnosisGroup.get("limitedDailyActivitiesCtrl").setValue(34);
            this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').disable();
          } else {
            this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').enable();
          }

          this.hasMedicalDiagnosis = this.caDetails.hasMedicalDiagnosis;
          this.hasPrincipalDiagnosis = this.caDetails.hasPrincipalDiagnosis;
          this.hasOtherDiagnosis = this.caDetails.hasOtherDiagnosis;
        }
      },
      error => console.error('Error!', error)
    );
  }

  msg: string;
  res: boolean;

  refreshDiagnosisList() {
    this.mdList = this.diagnosisList.filter(
      x => x.diagnosisTypeID == 48 && x.isActive == true
    );
    this.pdList = this.diagnosisList.filter(
      x => x.diagnosisTypeID == 49 && x.isActive == true
    );
    this.odList = this.diagnosisList.filter(
      x => x.diagnosisTypeID == 51 && x.isActive == true
    );
  }

  addDiagnosisList(diagnosis: PACTClinicalMHRDiagnosisDetails): void {
    this.diagnosisList.push(diagnosis);

    let newItem = {
      userID: this.userData.optionUserId,
      pactApplicationID: this.applicationID,
      pactClinicalMHRDiagnosisDetails: diagnosis,
    }
    this.saveDiagnosis(newItem);
    this.refreshDiagnosisList();
    if (this.mdList.length > 0) {
      this.medicalDiagnosisGroup.enable();
    }
    this.initTabStatus();
  }

  removeDiagnosisList(diagnosis: PACTClinicalMHRDiagnosisDetails): void {
    const index = this.diagnosisList.filter(x => x.diagnosisID == diagnosis.diagnosisID).length;
    //console.log('Index', index);
    if (index > 0) {
      let removedItem = {
        // userId : this.userData.optionUserId,
        // id : this.caDetails.id,
        // diagnosisDetails : diagnosis,
        userID: this.userData.optionUserId,
        pactApplicationID: this.applicationID,
        pactClinicalMHRDiagnosisDetails: diagnosis,
      }
      this.deleteDiagnosis(removedItem);
      if (this.mdList.length < 1) {
        this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').disable();
      }
      this.initTabStatus();
    }
  }

  saveDiagnosis(diagnosis: PACTClinicalMHRDiagnosisDetailsInput) {
    //this.cinicalAssessmentService.saveDiagnosis(diagnosis).subscribe(
    this.diagnosisService.savePACTClinicalMHRDiagnosisDetails(diagnosis).subscribe(
      res => {
        this.getclinicalAssessment();
        if (this.applicationID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
        }
      },
      error => {
        //console.log(error);
        this.getclinicalAssessment();
      });
  }

  deleteDiagnosis(diagnosis: PACTClinicalMHRDiagnosisDetailsInput) {
    //console.log(diagnosis);
    //this.cinicalAssessmentService.deleteDiagnosis(diagnosis).subscribe(
    this.diagnosisService.deletePACTClinicalMHRDiagnosisDetails(diagnosis).subscribe(
      res => {
        if (this.applicationID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
        }
        this.getclinicalAssessment();
      },
      error => {
        //console.log(error);
        this.getclinicalAssessment();
      });
  }

  diagnosisChanged(itm: any): void {
    if (itm['diagnosisID'] === 48) {
      this.caDetails.hasMedicalDiagnosis = itm['value'];
      this.hasMedicalDiagnosis = this.caDetails.hasMedicalDiagnosis;
      if (this.hasMedicalDiagnosis) {
        this.caDetails.diagnosisList.filter(x => x.diagnosisTypeID === 48).forEach(x => x.isActive = false);
        this.refreshDiagnosisList();
      }

      this.tab1Status = 0;
      if (this.caDetails.hasMedicalDiagnosis) {
        this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').disable();
      } else {
        if (this.mdList.length > 0) {
          this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').enable();
        }
      }
    }
    else if (itm['diagnosisID'] === 49) {
      this.caDetails.hasPrincipalDiagnosis = itm['value'];
      this.hasPrincipalDiagnosis = this.caDetails.hasPrincipalDiagnosis;
      if (this.hasPrincipalDiagnosis) {
        this.caDetails.diagnosisList
          .filter(x => x.diagnosisTypeID === 49)
          .forEach(x => (x.isActive = false));
      }
      this.tab2Status = 0;
    }
    else if (itm['diagnosisID'] === 51) {
      this.caDetails.hasOtherDiagnosis = itm['value'];
      this.hasOtherDiagnosis = this.caDetails.hasOtherDiagnosis;
      if (this.hasPrincipalDiagnosis) {
        this.caDetails.diagnosisList
          .filter(x => x.diagnosisTypeID === 51)
          .forEach(x => (x.isActive = false));
      }
      this.tab3Status = 0;
    }
    this.initTabStatus();
    this.isDiagnosisTouched = true;
    this.onSave(false);
  }

  setLimitedDailyActivitiesDDL() {
    if (this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').disabled) {
      this.caDetails.hasLimitedDailyActivities = null;
    } else {
      this.caDetails.hasLimitedDailyActivities =
        this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').value === 0
          ? null
          : this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').value;
    }
    this.caDetails.hasLimitedDailyActivities =
      this.diagnosisList.filter(
        x => x.diagnosisTypeID == 48 && x.isActive == true
      ).length > 0
        ? this.caDetails.hasLimitedDailyActivities
        : null;
  }

  dataMapping() {
    this.caDetails.id = this.applicationID;
    this.caDetails.diagnosisList = this.diagnosisList;
    this.caDetails.hasHIVDiagnosis = this.medicalDiagnosisGroup.get("diagnosisHIVCtrl").value;
    this.caDetails.isReceivingServices = this.medicalDiagnosisGroup.get("receivingServicesCtrl").value;
    this.setLimitedDailyActivitiesDDL();
    this.caDetails.hasAOT =
      this.oACTOCTGroup.get("aOTProgramCtrl").value === 0
        ? null
        : this.oACTOCTGroup.get("aOTProgramCtrl").value;
    this.caDetails.aotProgramType =
      this.oACTOCTGroup.get("aOTProgramNameCtrl").value === 0
        ? null
        : this.oACTOCTGroup.get("aOTProgramNameCtrl").value;
    this.caDetails.hasACT =
      this.oACTOCTGroup.get("aCTProgramCtrl").value === 0
        ? null
        : this.oACTOCTGroup.get("aCTProgramCtrl").value;
    this.caDetails.actProgramName = this.oACTOCTGroup.get("aCTProgramNameCtrl").value;
    this.caDetails.actPhoneNumber = this.oACTOCTGroup.get("aCTPhoneNumberCtrl").value;
    this.caDetails.userID = this.userData.optionUserId;
    this.caDetails.isMedicalDiagnosisCompleted = this.tab1Status == 1;
    this.caDetails.isPsychiatricDiagnosisCompleted = this.tab2Status == 1;
    this.caDetails.isAOTACTCompleted = this.tab3Status == 1;
  }

  onSave(isSave: boolean): void {
    if (this.caDetails != null) {
      if (this.medicalDiagnosisGroup.touched || this.oACTOCTGroup.touched || this.isDiagnosisTouched) {
        this.dataMapping();
        if (this.caDetails.actPhoneNumber != null && this.caDetails.actPhoneNumber.length > 0 && this.caDetails.actPhoneNumber.length < 10) {
          this.toastr.error('Entered ACT phone number is invalid!', 'Error');
          return
        }
        //console.log(this.caDetails);
        this.cinicalAssessmentService.saveAssessment(this.caDetails).subscribe(
          result => {
            if (isSave && result > 0) {
              this.message = 'Clinical assessment has been saved successfully!';
              this.toastr.success(this.message, 'Saved..');
              this.getclinicalAssessment();
            }
            if (this.applicationID) {
              this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
            }
          },
          error => {
            //console.log(error);
            this.toastr.error("There is an error while saving Clinical Assessment.");
          }
        );
      }
    }
  }

  limitedActivitiesChanged(): void {
    this.caDetails.hasLimitedDailyActivities = this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').value == 0 ? null : this.medicalDiagnosisGroup.get('limitedDailyActivitiesCtrl').value;
    this.mdTabStatus();
  }
  diagnosisHIVChange($event): void {
    this.caDetails.hasHIVDiagnosis = $event.value;
    this.mdTabStatus();

    if ($event.value === 33)
      this.isReceivingServicesVisible = true;
    else {
      this.isReceivingServicesVisible = false;
      this.medicalDiagnosisGroup.controls['receivingServicesCtrl'].setValue(null);
    }
  }
  receivingServiceChange($event): void {
    this.caDetails.isReceivingServices = $event.value;
    this.mdTabStatus();
  }
  aOTprogramNameCtrlChanged(): void {
    this.selectedAOTNo = this.oACTOCTGroup.get('aOTProgramNameCtrl').value;
    this.caDetails.aotProgramType = this.selectedAOTNo;
    if (this.selectedAOTNo > 0) {
      this.selectedAOT = this.aOTPrograms.filter(
        x => x.programID == this.selectedAOTNo
      )[0];
      this.caDetails.aotPhoneNumber = this.selectedAOT.phoneNumber;
    } else {
      this.caDetails.aotPhoneNumber = null;
    }
    this.aotTabStaus();
  }

  aOTprogramCtrlChanged(): void {
    this.caDetails.hasAOT = this.oACTOCTGroup.get('aOTProgramCtrl').value;
    if (this.oACTOCTGroup.get('aOTProgramCtrl').value == 33) {
      this.caDetails.aotProgramType = 0;
      this.oACTOCTGroup.get('aOTProgramNameCtrl').setValue(0);
      this.caDetails.aotPhoneNumber = null;
      this.hasAOTProgram = true;
    } else {
      this.hasAOTProgram = false;
    }
    this.aotTabStaus();
  }

  aCTprogramCtrlChanged(): void {
    this.caDetails.hasACT = this.oACTOCTGroup.get('aCTProgramCtrl').value;
    this.oACTOCTGroup.get('aCTProgramNameCtrl').setValue('');
    this.oACTOCTGroup.get('aCTPhoneNumberCtrl').setValue('');
    this.caDetails.actPhoneNumber = '';
    this.caDetails.actProgramName = '';
    if (this.oACTOCTGroup.get('aCTProgramCtrl').value == 33) {
      this.hasACTProgram = true;
    } else {
      this.hasACTProgram = false;
    }
    this.aotTabStaus();
  }

  aCTProgramNameName() {
    this.caDetails.actProgramName = this.oACTOCTGroup.get('aCTProgramNameCtrl').value;
    this.aotTabStaus();
  }
  nextTab() {
    this.onSave(false);
    if (this.tabSelectedIndex == 3) {
      this.router.navigateByUrl('shs/newApp/adls/' + this.applicationID);
    } else {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    }
  }

  previousTab() {
    this.onSave(false);
    if (this.tabSelectedIndex != 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    } else if (this.tabSelectedIndex == 0) {
      this.sidenavStatusService.routeToPreviousPage(this.router, this.route);
    }
  }
}
