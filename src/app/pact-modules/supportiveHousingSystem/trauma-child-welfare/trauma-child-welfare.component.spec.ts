import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraumaChildWelfareComponent } from './trauma-child-welfare.component';

describe('TraumaChildWelfareComponent', () => {
  let component: TraumaChildWelfareComponent;
  let fixture: ComponentFixture<TraumaChildWelfareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraumaChildWelfareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraumaChildWelfareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
