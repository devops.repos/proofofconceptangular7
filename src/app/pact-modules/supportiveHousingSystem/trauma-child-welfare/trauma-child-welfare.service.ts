import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { PACTTraumaAndChildWelfare } from './trauma-child-welfare.model'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
  })

  export class TraumaService {
    
    pactTraumaAndChildWelfare = new PACTTraumaAndChildWelfare();


    
    SERVER_URL = environment.pactApiUrl;
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    
    constructor(private httpClient: HttpClient) { }
  
    saveTrauma(traumaAndChildWelfare:PACTTraumaAndChildWelfare): Observable<any> 
    {
        //const saveTraumaURL = `${this.SERVER_URL}TraumaAndChildWelfare/SaveTraumaAndChildWelfare`;
       const saveUrl = environment.pactApiUrl + 'TraumaAndChildWelfare/SaveTraumaAndChildWelfare';              
        return this.httpClient.post(saveUrl, traumaAndChildWelfare, this.httpOptions);
       
    }

    getApplicationByID(applicationID: number) {
        const getUrl = environment.pactApiUrl + 'TraumaAndChildWelfare/GetTraumaAndChildWelfareByID/';
        return this.httpClient.get(getUrl + applicationID);

    }

    getCWDquestionStatus(applicationID: number) {
      const getUrl = environment.pactApiUrl + 'TraumaAndChildWelfare/GetCWDquestionStatus/';
      return this.httpClient.get(getUrl + applicationID);
    }

      
  }