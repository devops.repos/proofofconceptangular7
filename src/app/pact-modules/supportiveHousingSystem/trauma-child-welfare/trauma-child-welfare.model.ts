export class PACTTraumaAndChildWelfare {
  pactTraumaAndChildWelfareID: number;
  pactApplicationID: number;
  isDomesticViolenceVictim: number;
  dvHowLongAgoType: number;
  hasReceivedDVServices: number;
  recentProviderName: string;
  contactName: string;
  contactPhoneNo: number;
  isAtSeriousRiskDV: number;
  seriousRiskDVDescription: string; 
  hasGenderBasedViolence: number;
  gbvHowLongAgoType: number;
  gbvDescription: string;
  hasHistoryOfCommercialSexualActivity: number;
  csaHowLongAgoType: number;
  csaDescription: string;
  hasAdoptiveFamilyPlacement: number; 
  afpDescription: string;
  hasHealthIssue: number;
  hiDescription: string;
  hasOpenChildProtectiveInvestigation: number;
  ocpiDescription: string;
  isActive: boolean;
  createdBy: number;
  updatedBy: number;
  isDomesticViolenceTraumaTabComplete: boolean;
  isChildWelfareDevelopmentTabComplete: boolean;
}
export class CWDquestionStatus {

  qOne: boolean;
  qTwo: boolean;

}