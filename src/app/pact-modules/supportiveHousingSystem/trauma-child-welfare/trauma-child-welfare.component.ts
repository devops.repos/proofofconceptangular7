import { Component, OnInit, OnDestroy, HostListener } from "@angular/core";
import { Subscription } from "rxjs";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { PACTTraumaAndChildWelfare, CWDquestionStatus } from "./trauma-child-welfare.model";
import { MatRadioChange, MatTabChangeEvent, MatSelectChange } from "@angular/material";
import { RefGroupDetails } from "../../../models/refGroupDetailsDropDown.model";
import { CommonService } from "../../../services/helper-services/common.service";
import { TraumaService } from "./trauma-child-welfare.service";
import { UserService } from "src/app/services/helper-services/user.service";
import { ClientApplicationService } from "src/app/services/helper-services/client-application.service";
import { AuthData } from "src/app/models/auth-data.model";
import { ToastrService } from "ngx-toastr";
import { SidenavStatusService } from "src/app/core/sidenav-list/sidenav-status.service";
import { ActivatedRoute, Router, NavigationStart } from "@angular/router";
import { appStatusColor } from "src/app/models/pact-enums.enum";
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

@Component({
  selector: "app-trauma-child-welfare",
  templateUrl: "./trauma-child-welfare.component.html",
  styleUrls: ["./trauma-child-welfare.component.scss"]
})
export class TraumaChildWelfareComponent implements OnInit, OnDestroy {
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;
  commonServiceSub: Subscription;
  saveTraumeSub: Subscription;
  checkQStatusSub: Subscription;
  userDataSub: Subscription;
  getTraumaSub: Subscription;
  routeSub: Subscription; //Subscription to route observer


  dvGroup: FormGroup;
  pactTraumaAndChildWelfare: PACTTraumaAndChildWelfare = new PACTTraumaAndChildWelfare();
  cwdQuestionStatus: CWDquestionStatus = new CWDquestionStatus();
  domesticShow: Boolean;
  genderShow: Boolean;
  commercialShow: Boolean;
  adoptShow: Boolean;
  healthShow: Boolean;
  investigationShow: Boolean;
  dvProviderShow: Boolean;
  riskDVShow: Boolean;
  riskDVDescShow: Boolean;

  dvTime: number;

  timeLength: RefGroupDetails[];
  responseItems: RefGroupDetails[];

  applicationID: number;
  user: AuthData;

  tab1Status: number = 2; // 0 = false; 1 = true; 2 = null
  tab2Status: number = 2;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  selectedTab = 0;

  //Save Data on page refresh
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (this.dvGroup.dirty) {
      this.onSave(false);
    }
    // event.returnValue = false;
  }

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private commonService: CommonService,
    private traumaService: TraumaService,
    private route: ActivatedRoute,
    private sidenavStatusService: SidenavStatusService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    /** To Load the Sidenav Completion Status for all the Application related Pages */
    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.applicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
          this.getApplicationByID(this.applicationID);
          this.checkCWDquestionStatus(this.applicationID);
          this.buildForm();
        }
      }
    });

    this.commonServiceSub = this.commonService.getRefGroupDetails("7,23").subscribe(
      res => {
        const data = res.body as RefGroupDetails[];

        this.timeLength = data.filter(d => {
          return d.refGroupID === 23;
        });
        this.responseItems = data.filter(d => {
          return d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34);
        });
      },
      error => console.error("Error!", error)
    );

   /*  this.clientApplicationService.getPactApplicationID().subscribe(appID => {
      this.applicationID = appID;
      this.getApplicationByID(this.applicationID);
      this.checkCWDquestionStatus(this.applicationID);
      this.buildForm();
    }); */

    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.dvGroup.touched && event.navigationTrigger != "popstate") {
          this.onSave(false);
        }
        if (event.navigationTrigger == "popstate" && this.dvGroup.dirty) {
          // Handles forward and backward browser clicks
          this.onSave(false);
        }
      }
    });

    this.domesticShow = false;
    this.genderShow = false;
    this.commercialShow = false;
    this.adoptShow = false;
    this.healthShow = false;
    this.investigationShow = false;
    this.dvProviderShow = false;
    this.riskDVShow = false;
    this.riskDVDescShow = false;

    this.userDataSub = this.userService.getUserData().subscribe(res => (this.user = res));
  }

  buildForm() {
    this.dvGroup = this.formBuilder.group({
      isDomesticViolenceVictim: [""],
      hasGenderBasedViolence: [""],
      hasHistoryOfCommercialSexualActivity: [""],
      dvHowLongAgoType: [0],
      isAtSeriousRiskDV: [""],
      seriousRiskDVDescription: ["",[CustomValidators.minimunCharLength(2)]],
      hasReceivedDVServices: [""],
      recentProviderName: ["",[CustomValidators.minimunCharLength(2)]],
      contactName: ["", [Validators.pattern("^[a-zA-Z ]*$"), CustomValidators.minimunCharLength(2)]],
      contactPhoneNo: [""], //, { validators: [Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(10)], updateOn: "blur" }],
      gbvHowLongAgoType: [0],
      gbvDescription: ["",[CustomValidators.minimunCharLength(2)]],
      csaHowLongAgoType: [0],
      csaDescription: ["",[CustomValidators.minimunCharLength(2)]],
      hasAdoptiveFamilyPlacement: [""],
      afpDescription: ["",[CustomValidators.minimunCharLength(2)]],
      hasHealthIssue: [""],
      hiDescription: ["",[CustomValidators.minimunCharLength(2)]],
      hasOpenChildProtectiveInvestigation: [""],
      ocpiDescription: ["",[CustomValidators.minimunCharLength(2)]]
    });
  }

  onSave(showToast: boolean) {
    var invalidData = false;
    if (this.dvGroup.invalid) {
      invalidData = true;
      //this.toastr.warning("Form has invalid data.", "Invalid data was not saved");
      this.findInvalidControls();
      /* const invalidControls = this.findInvalidControls();
      invalidControls.forEach(ctr => {
        this.dvGroup.controls[ctr].setValue("");
      }); */
    } 
      this.pactTraumaAndChildWelfare = this.dvGroup.value;
      this.pactTraumaAndChildWelfare.pactApplicationID = this.applicationID;
      this.pactTraumaAndChildWelfare.isActive = true;
      this.pactTraumaAndChildWelfare.createdBy = this.pactTraumaAndChildWelfare.updatedBy = this.user.optionUserId;
      if (this.pactTraumaAndChildWelfare.dvHowLongAgoType == 0) {
        this.pactTraumaAndChildWelfare.dvHowLongAgoType = null;
      }
      if (this.pactTraumaAndChildWelfare.gbvHowLongAgoType == 0) {
        this.pactTraumaAndChildWelfare.gbvHowLongAgoType = null;
      }
      if (this.pactTraumaAndChildWelfare.csaHowLongAgoType == 0) {
        this.pactTraumaAndChildWelfare.csaHowLongAgoType = null;
      }

      this.pactTraumaAndChildWelfare.isDomesticViolenceTraumaTabComplete = this.isDVTTabComplete();
      this.pactTraumaAndChildWelfare.isChildWelfareDevelopmentTabComplete = this.isCWDTabComplete();

      this.saveTraumeSub = this.traumaService.saveTrauma(this.pactTraumaAndChildWelfare).subscribe(
        res => {
          if (showToast) {
            if(invalidData){
              this.toastr.warning("Some inputs are not valid", "Valid data was saved successfully.");
            } else {
              this.toastr.success("The data was saved successfully.", "");
            }
          }
        },
        error =>
          this.toastr.error(
            "The information was not saved.",
            "Error while saving"
          )
      );

      this.tab1Status =
        this.pactTraumaAndChildWelfare.isDomesticViolenceTraumaTabComplete ==
        true
          ? 1
          : 0;
      this.tab2Status =
        this.pactTraumaAndChildWelfare.isChildWelfareDevelopmentTabComplete ==
        true
          ? 1
          : 0;

      //this.sidenavStatusService._sidenavStatusReload(this.route);
      if (this.applicationID) {
        this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
      }

    
  }

  findInvalidControls() {
    const invalid = [];
    const controls = this.dvGroup.controls;
    for (const name in controls) {
        if (controls[name].invalid) {
          controls[name].setValue("");
        }
    }
    //return invalid;
}

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    //Saving Data Between Tab Navigation
    if (this.dvGroup.touched || this.dvGroup.dirty) {
      this.onSave(false);
    }
    this.selectedTab = tabChangeEvent.index;
  }

  //Next button click
  nextPage() {
    if (this.selectedTab < 1) {
      this.selectedTab = this.selectedTab + 1;
    } else if (this.selectedTab == 1) {
      // if (this.router.url.includes("newApp")) {
      //   this.router.navigate([
      //     "/shs/newApp/symptoms-substance-use/" + this.applicationID
      //   ]);
      // } else {
      //   this.router.navigate([
      //     "/shs/pendingApp/symptoms-substance-use/" + this.applicationID
      //   ]);
      // }
      this.sidenavStatusService.routeToNextPage(this.router, this.route);
    }
  }

  //Previous button click
  previousPage() {
    if (this.selectedTab != 0) {
      this.selectedTab = this.selectedTab - 1;
    } else if (this.selectedTab == 0) {
      // if (this.router.url.includes('newApp')){
      //   this.router.navigate(['/shs/newApp/medications-providers-hospitalization/' + this.applicationID]);
      // }
      // else {
      //   this.router.navigate(['/shs/pendingApp/medications-providers-hospitalization/' + this.applicationID]);
      // }
      this.sidenavStatusService.routeToPreviousPage(this.router, this.route);
    }
  }

  getApplicationByID(id: number) {
    this.getTraumaSub = this.traumaService
      .getApplicationByID(id)
      .subscribe(res => {
        this.pactTraumaAndChildWelfare = res as PACTTraumaAndChildWelfare;

        if (this.pactTraumaAndChildWelfare != null) {
          this.tab1Status =
            this.pactTraumaAndChildWelfare
              .isDomesticViolenceTraumaTabComplete == true
              ? 1
              : this.pactTraumaAndChildWelfare
                  .isDomesticViolenceTraumaTabComplete == false
              ? 0
              : 2;
          this.tab2Status =
            this.pactTraumaAndChildWelfare
              .isChildWelfareDevelopmentTabComplete == true
              ? 1
              : this.pactTraumaAndChildWelfare
                  .isChildWelfareDevelopmentTabComplete == false
              ? 0
              : 2;
          this.dvGroup.patchValue(this.pactTraumaAndChildWelfare);

          if ( this.pactTraumaAndChildWelfare.isDomesticViolenceVictim == 33)
          {
            this.domesticShow = true;
            if (!this.pactTraumaAndChildWelfare.dvHowLongAgoType) {
              this.dvGroup.controls["dvHowLongAgoType"].setValue(0);
            }

            if (this.pactTraumaAndChildWelfare.hasReceivedDVServices == 33)
            {
              this.dvProviderShow = true;
            }

            if (this.pactTraumaAndChildWelfare.dvHowLongAgoType == 151) {
              this.riskDVShow = true;

              if (this.pactTraumaAndChildWelfare.isAtSeriousRiskDV == 33 )
              {
                this.riskDVDescShow = true;
              }
            }
          }
          if (this.pactTraumaAndChildWelfare.hasGenderBasedViolence == 33)
          {
            this.genderShow = true;

            if (!this.pactTraumaAndChildWelfare.gbvHowLongAgoType) {
              this.dvGroup.controls["gbvHowLongAgoType"].setValue(0);
            }
          }
          if ( this.pactTraumaAndChildWelfare.hasHistoryOfCommercialSexualActivity == 33 )
          {
            this.commercialShow = true;

            if (!this.pactTraumaAndChildWelfare.csaHowLongAgoType) {
              this.dvGroup.controls["csaHowLongAgoType"].setValue(0);
            }
          }
          if (this.pactTraumaAndChildWelfare.hasAdoptiveFamilyPlacement == 33)
          {
            this.adoptShow = true;
          }
          if ( this.pactTraumaAndChildWelfare.hasHealthIssue == 33 ) {
            this.healthShow = true;
          }
          if ( this.pactTraumaAndChildWelfare.hasOpenChildProtectiveInvestigation == 33 )
          {
            this.investigationShow = true;
          }
        }
      });
  }

  phoneValidate() {
    this.dvGroup.controls.contactPhoneNo.hasError('Mask error') ? this.toastr.error("Invalid Phone number. Please enter 10 digit phone number") : "";
  }

  checkCWDquestionStatus(id: number){

    this.checkQStatusSub = this.traumaService.getCWDquestionStatus(id).subscribe(res => {

    this.cwdQuestionStatus = res as CWDquestionStatus;
    });
  }

  isDVTTabComplete(): boolean {
    var isDomesticViolenceTraumaTabComplete = true;

    //top level 3 questions
    if (
      !this.dvGroup.controls["isDomesticViolenceVictim"].value ||
      !this.dvGroup.controls["hasGenderBasedViolence"].value ||
      !this.dvGroup.controls["hasHistoryOfCommercialSexualActivity"].value
    ) {
      isDomesticViolenceTraumaTabComplete = false;
    } else {
      //DVV - yes
      if (
        this.checkThreeInputs(
          "isDomesticViolenceVictim",
          "dvHowLongAgoType",
          "hasReceivedDVServices"
        )
      ) {
        isDomesticViolenceTraumaTabComplete = false;
      } else if ( this.dvGroup.controls["isDomesticViolenceVictim"].value == 33 ) {
        if (
          this.dvGroup.controls["hasReceivedDVServices"].value == 33 &&
          (!this.dvGroup.controls["recentProviderName"].value ||
            !this.dvGroup.controls["contactName"].value ||
            !this.dvGroup.controls["contactPhoneNo"].value)
        ) {
          isDomesticViolenceTraumaTabComplete = false;
        }
        if (
          this.dvGroup.controls["dvHowLongAgoType"].value == 151 &&
          !this.dvGroup.controls["isAtSeriousRiskDV"].value
        ) {
          isDomesticViolenceTraumaTabComplete = false;
        }
        if (
          this.checkTwoInputs("isAtSeriousRiskDV", "seriousRiskDVDescription")
        ) {
          isDomesticViolenceTraumaTabComplete = false;
        }
      }

      //GBV - yes
      if (
        this.checkThreeInputs(
          "hasGenderBasedViolence",
          "gbvHowLongAgoType",
          "gbvDescription"
        )
      ) {
        isDomesticViolenceTraumaTabComplete = false;
      }

      //CSA - yes
      if (
        this.checkThreeInputs(
          "hasHistoryOfCommercialSexualActivity",
          "csaHowLongAgoType",
          "csaDescription"
        )
      ) {
        isDomesticViolenceTraumaTabComplete = false;
      }
    }
    return isDomesticViolenceTraumaTabComplete;
  }

  isCWDTabComplete(): boolean {
    var isChildWelfareDevelopmentTabComplete = true;
    //if all 3 questions are displayed
    if(this.cwdQuestionStatus.qOne && this.cwdQuestionStatus.qTwo) {
      //top level 3 questions
      if (!this.dvGroup.controls["hasAdoptiveFamilyPlacement"].value ||
          !this.dvGroup.controls["hasHealthIssue"].value ||
          !this.dvGroup.controls["hasOpenChildProtectiveInvestigation"].value) {
        isChildWelfareDevelopmentTabComplete = false;
      }
    } else if (this.cwdQuestionStatus.qOne) {
      if (!this.dvGroup.controls["hasAdoptiveFamilyPlacement"].value) {
        isChildWelfareDevelopmentTabComplete = false;
      }
    } else if (this.cwdQuestionStatus.qTwo) {
      if (
        !this.dvGroup.controls["hasHealthIssue"].value ||
        !this.dvGroup.controls["hasOpenChildProtectiveInvestigation"].value
      ) {
        isChildWelfareDevelopmentTabComplete = false;
      }
    }

    //AFP - yes
    if (this.checkTwoInputs("hasAdoptiveFamilyPlacement", "afpDescription")) {
      isChildWelfareDevelopmentTabComplete = false;
    }

    //HI - yes
    if (this.checkTwoInputs("hasHealthIssue", "hiDescription")) {
      isChildWelfareDevelopmentTabComplete = false;
    }

    //OCPI - yes
    if (this.checkTwoInputs("hasOpenChildProtectiveInvestigation", "ocpiDescription")) {
        isChildWelfareDevelopmentTabComplete = false;
    }

    return isChildWelfareDevelopmentTabComplete;
  }

  checkTwoInputs(ctrl1: string, ctrl2: string): boolean {
    if (
      this.dvGroup.controls[ctrl1].value == 33 &&
      (!this.dvGroup.controls[ctrl2].value ||
        this.dvGroup.controls[ctrl2].value == "" ||
        this.dvGroup.controls[ctrl2].value == 0)
    ) {
      return true;
    } else {
      return false;
    }
  }

  checkThreeInputs(ctrl1: string, ctrl2: string, ctrl3: string): boolean {
    if (
      this.dvGroup.controls[ctrl1].value == 33 &&
      (this.dvGroup.controls[ctrl2].value == 0 ||
        !this.dvGroup.controls[ctrl3].value ||
        this.dvGroup.controls[ctrl3].value == "")
    ) {
      return true;
    } else {
      return false;
    }
  }

  domesticChange(event: MatRadioChange) {
    if (event.value == 33) {
      this.domesticShow = true;
      this.dvGroup.controls["dvHowLongAgoType"].setValue(0);
    } else {
      this.domesticShow = false;
      this.riskDVShow = false;
      this.dvProviderShow = false;
      this.riskDVDescShow = false;
      this.dvGroup.controls["dvHowLongAgoType"].setValue("");
      this.dvGroup.controls["hasReceivedDVServices"].setValue("");
      this.dvGroup.controls["recentProviderName"].setValue("");
      this.dvGroup.controls["contactName"].setValue("");
      this.dvGroup.controls["contactPhoneNo"].setValue("");
      this.dvGroup.controls["isAtSeriousRiskDV"].setValue("");
      this.dvGroup.controls["seriousRiskDVDescription"].setValue("");
    }
  }

  dvProviderChange(event: MatRadioChange) {
    if (event.value == 33) {
      this.dvProviderShow = true;
    } else {
      this.dvProviderShow = false;
      this.dvGroup.controls["recentProviderName"].setValue("");
      this.dvGroup.controls["contactName"].setValue("");
      this.dvGroup.controls["contactPhoneNo"].setValue("");
    }
  }

  dvPeriodChange(event: MatSelectChange) {
    if (event.value == 151) {
      this.riskDVShow = true;
    } else {
      this.riskDVShow = false;
      this.dvGroup.controls["isAtSeriousRiskDV"].setValue("");
      this.dvGroup.controls["seriousRiskDVDescription"].setValue("");
    }
  }

  riskDVChange(event: MatRadioChange) {
    if (event.value == 33) {
      this.riskDVDescShow = true;
    } else {
      this.riskDVDescShow = false;
      this.dvGroup.controls["seriousRiskDVDescription"].setValue("");
    }
  }
  genderChange(event: MatRadioChange) {
    if (event.value == 33) {
      this.genderShow = true;
      this.dvGroup.controls["gbvHowLongAgoType"].setValue(0);
    } else {
      this.genderShow = false;
      this.dvGroup.controls["gbvHowLongAgoType"].setValue("");
      this.dvGroup.controls["gbvDescription"].setValue("");
    }
  }

  commercialChange(event: MatRadioChange) {
    if (event.value == 33) {
      this.commercialShow = true;
      this.dvGroup.controls["csaHowLongAgoType"].setValue(0);
    } else {
      this.commercialShow = false;
      this.dvGroup.controls["csaHowLongAgoType"].setValue("");
      this.dvGroup.controls["csaDescription"].setValue("");
    }
  }
  adoptChange(event: MatRadioChange) {
    if (event.value == 33) {
      this.adoptShow = true;
    } else {
      this.adoptShow = false;
      this.dvGroup.controls["afpDescription"].setValue("");
    }
  }
  healthChange(event: MatRadioChange) {
    if (event.value == 33) {
      this.healthShow = true;
    } else {
      this.healthShow = false;
      this.dvGroup.controls["hiDescription"].setValue("");
    }
  }
  investigationChange(event: MatRadioChange) {
    if (event.value == 33) {
      this.investigationShow = true;
    } else {
      this.investigationShow = false;
      this.dvGroup.controls["ocpiDescription"].setValue("");
    }
  }

  //get contactPhoneNo() { return this.dvGroup.get('contactPhoneNo'); }

  get contactName() {
    return this.dvGroup.get("contactName");
  }

  ngOnDestroy() {

    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
     if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.getTraumaSub) {
      this.getTraumaSub.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.commonServiceSub) {
      this.commonServiceSub.unsubscribe();
    }
    if (this.saveTraumeSub) {
      this.saveTraumeSub.unsubscribe();
    }
    if (this.checkQStatusSub) {
      this.checkQStatusSub.unsubscribe();
    }
  }
}
