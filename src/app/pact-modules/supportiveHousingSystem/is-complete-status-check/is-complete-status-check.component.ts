import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import {
  ClientApplicationService,
  ClientApplication
} from 'src/app/services/helper-services/client-application.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import {
  MatDatepickerInputEvent,
  MatDatepicker,
  MatDatepickerInput
} from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-is-complete-status-check',
  templateUrl: './is-complete-status-check.component.html',
  styleUrls: ['./is-complete-status-check.component.scss']
})
export class IsCompleteStatusCheckComponent implements OnInit {
  tab1Status = 2; // 0 = false; 1 = true; 2 = null
  tab2Status = 2;
  tab3Status = 2;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;
  clientApplicationData: ClientApplication;

  housingForm: FormGroup;
  fromDateTriggered = false;

  @ViewChild('fromDatePicker') fromDatePicker;
  @ViewChild('toDatePicker') toDatePicker;

  pactApplicationID: number;
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;
  homelessServiceSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private sidenavStatusService: SidenavStatusService,
    private clientApplicationService: ClientApplicationService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.housingForm = this.fb.group({
      fromDateCtrl: ['', [Validators.required, CustomValidators.dropdownRequired()] ],
      toDateCtrl: ['', [Validators.required, CustomValidators.dropdownRequired()] ]
    });
    /** Setting the sidenav Status for the application with the given ID from URL router parameter
     * This is the case for page refresh or reload
     */
    // this.sidenavStatusService._sidenavStatusReload(this.route);
    this.activatedRouteSub = this.route.paramMap.subscribe((params) => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.pactApplicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);
        }
      }
    });
    this.clientApplicationService.getClientApplicationData().subscribe(res => {
      if (res) {
        this.clientApplicationData = res;
        // console.log(this.clientApplicationData);
      }
    });
  }

  // #region buttons for tab level status icon/color change
  onTab1FormFieldUnTouched() {
    this.tab1Status = 2;
  }
  onTab1FormFieldTouched() {
    this.tab1Status = 0;
  }
  onTab1FormFieldCompleted() {
    this.tab1Status = 1;
  }

  onTab2FormFieldUnTouched() {
    this.tab2Status = 2;
  }
  onTab2FormFieldTouched() {
    this.tab2Status = 0;
  }
  onTab2FormFieldCompleted() {
    this.tab2Status = 1;
  }

  onTab3FormFieldUnTouched() {
    this.tab3Status = 2;
  }
  onTab3FormFieldTouched() {
    this.tab3Status = 0;
  }
  onTab3FormFieldCompleted() {
    this.tab3Status = 1;
  }
  // #endregion buttons for tab level status icon/color change

  // #region datepicker popup dynamically on date changed dynamically
  onFromDateTriggered() {
    this.housingForm.get('fromDateCtrl').setValue(new Date('12/22/1998'));
    this.fromDatePicker.open();
  }

  onToDateTriggered() {
    this.housingForm.get('toDateCtrl').setValue(new Date('01/13/2019'));
    this.toDatePicker.open();
  }
  // #endregion datepicker popup dynamically on date changed dynamically
}
