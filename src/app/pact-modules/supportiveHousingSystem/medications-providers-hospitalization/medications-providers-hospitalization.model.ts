
export interface MedicationsProvidersHospitalizationModel {
    admissionDate: Date;
    isApplicantPsychiatricallyHospitalized: number;
    createdBy: number;
    createdDate: Date;
    everTakenForPsychiatricAssessment: number;
    everTakenToEmergencyServices: number;    
    hhAgencyProgramName: string;    
    hhAgencyContactNameAddress: string;
    hhAgencyContactPhoneNo: string;
    hospitalName: string;
    isActive: boolean;
    isCareCoordinationTabComplete?: boolean;
    isComplete?: boolean;
    isCrisisInterventionTabComplete?: boolean;
    isCurrentlyHospitalized: number;
    isCurrentMedicationsTabComplete?: boolean;
    isCurrentTreatmentServiceProvidersTabComplete?: boolean;
    isEnrolledInHealthHome: number;
    isEnrolledInMLTC: number;
    isHospitalizationsTabComplete?: boolean;
    levelOfSupportType: number;
    medicationsDetails: MedicationDetailModel[],
    mltcAgencyProgramName: string;    
    mltcProviderContactNameAddress: string;
    mltcProviderContactPhoneNo: number;
    noOfPsychiatricHospitalizationInPast3Yrs: number;
    pactApplicationID: number;
    pactMedicationsProvidersHospitalizationID: number;
    recentDischargeDate: Date;
    recentEmergencyServiceDate: Date;
    recentPhychAssessmentDate: Date;
    serviceType: number;
    treatmentServiceProvidersDetails: TreatmentServiceProviderDetailModel[]
    updatedBy: number;
    updatedDate: Date;
}

export interface TreatmentServiceProviderDetailModel {
    agencyProgramName: string;
    providerContactName: string;
    contactPhoneNo: string;
    treatmentType: number;
    treatmentTypeDescription: number;
    modalityType: number;
    modalityTypeDescription:number;
    isActive: boolean;
    createdBy: number;
    createdDate: Date;
    updatedBy: number;
    updatedDate: Date;
    pactTreatmentServiceProvidersDetailsID: number;
    pactMedicationsProvidersHospitalizationID: number;
}

export interface MedicationDetailModel {
    medicationName: string;
    medicationType: number;
    medicationTypeDescription: string;
    isActive: boolean;
    createdBy: number;
    createdDate: Date;
    updatedBy: number;
    updatedDate : Date;
    pactMedicationsDetailsID: number;
    pactMedicationsProvidersHospitalizationID: number;
}
