import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicationsProvidersHospitalizationComponent } from './medications-providers-hospitalization.component';

describe('MedicationsProvidersHospitalizationComponent', () => {
  let component: MedicationsProvidersHospitalizationComponent;
  let fixture: ComponentFixture<MedicationsProvidersHospitalizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicationsProvidersHospitalizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationsProvidersHospitalizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
