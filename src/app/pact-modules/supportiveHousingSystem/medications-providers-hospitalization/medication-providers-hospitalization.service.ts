import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MedicationsProvidersHospitalizationModel, MedicationDetailModel, TreatmentServiceProviderDetailModel } from './medications-providers-hospitalization.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MedicationProvidersHospitalizationService{
SERVER_URL =environment.pactApiUrl;

getMedicationProviderHospitalizationUrl = environment.pactApiUrl + 'MedicationsProvidersHospitalization/GetByPACTApplicationID';
getMedicationDetailsUrl = environment.pactApiUrl+'MedicationsProvidersHospitalization/GetPACTMedicationDetails';
getTreatmentServiceProvidersDetailsUrl = environment.pactApiUrl+'MedicationsProvidersHospitalization/GetPACTTreatmentServiceProvidersDetails';
saveMedicationProviderHospitalizationUrl = environment.pactApiUrl + 'MedicationsProvidersHospitalization/SaveMedicationsProvidersHospitalization';
saveMedicationDetailUrl = environment.pactApiUrl + 'MedicationsProvidersHospitalization/SaveMedicationDetail';
saveTreatmentServiceProvidersDetailUrl = environment.pactApiUrl + 'MedicationsProvidersHospitalization/SaveTreatmentServiceProvidersDetail';

httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':'application/json'
    }),
    response : "json",
};

constructor(private httpClient: HttpClient) {}


public getMedicationDetailsList<T>(pactMedicationsProvidersHospitalizationID: number) : Observable<T> {
    return this.httpClient.post<T>(
      this.getMedicationDetailsUrl,
      JSON.stringify(pactMedicationsProvidersHospitalizationID),
      this.httpOptions
    );
  }

public getTreatmentServiceProviderDetailsList<T> (pactMedicationsProvidersHospitalizationID : number): Observable<T> {
  return this.httpClient.post<T>(
    this.getTreatmentServiceProvidersDetailsUrl,
    JSON.stringify(pactMedicationsProvidersHospitalizationID),
    this.httpOptions
  );
}

getMedicationProviderHospitalizationDetail<MedicationsProvidersHospitalizationModel>(pactApplicationID : number) :Observable<MedicationsProvidersHospitalizationModel> {
  return this.httpClient.post<MedicationsProvidersHospitalizationModel>(
    this.getMedicationProviderHospitalizationUrl,
    JSON.stringify(pactApplicationID),
    this.httpOptions
  );
}

public saveMedicationProviderHospitalization = (medProvHospData: MedicationsProvidersHospitalizationModel) => {
  return this.httpClient.post(
    this.saveMedicationProviderHospitalizationUrl,
    JSON.stringify(medProvHospData),
    this.httpOptions
  );
}

public saveMedicationDetail = (medicationData : MedicationDetailModel) => {
  return this.httpClient.post(
    this.saveMedicationDetailUrl,
    JSON.stringify(medicationData),
    this.httpOptions
  );
}

public saveTreatmentServiceProvidersDetail = (treatmentSerProvData: TreatmentServiceProviderDetailModel) => {
  return this.httpClient.post(
    this.saveTreatmentServiceProvidersDetailUrl,
    JSON.stringify(treatmentSerProvData),
    this.httpOptions
  );
}

}