import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { TreatmentServiceProviderDetailModel} from './medications-providers-hospitalization.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "treatmentService-action",
  template: ` <mat-icon  matTooltip='Edit Treatment Service' (click)="onEdit()" class="doc-action-icon" color="primary">edit</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete Treatment Service' class="doc-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class TreatmentServiceActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private treatmentServiceSelected : TreatmentServiceProviderDetailModel;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onEdit(){
      this.treatmentServiceSelected = this.modelFromParams();

      this.params.context.componentParent.treatmentServiceViewParent(this.treatmentServiceSelected);
  }

  onDelete() {
     this.treatmentServiceSelected = this.modelFromParams();
     // alert(JSON.stringify(this.docSelected));
     // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
     this.params.context.componentParent.treatmentServiceDeleteParent(this.treatmentServiceSelected);
     // this.c2vService.setClientAwaitingPlacementSelected(this.docSelected);
     // todo
  }

  modelFromParams()
  {
      return {
        pactMedicationsProvidersHospitalizationID : this.params.data.pactMedicationsProvidersHospitalizationID,
        pactTreatmentServiceProvidersDetailsID :this.params.data.pactTreatmentServiceProvidersDetailsID,
        agencyProgramName : this.params.data.agencyProgramName,
        providerContactName: this.params.data.providerContactName,
        contactPhoneNo: this.params.data.contactPhoneNo,
        treatmentType: this.params.data.treatmentType,
        treatmentTypeDescription : null,
        modalityType: this.params.data.modalityType,
        modalityTypeDescription: null,
        isActive: this.params.data.isActive,
        createdBy: this.params.data.createdBy,
        createdDate:this.params.data.createdDate,
        updatedBy:this.params.data.updatedBy,
        updatedDate:this.params.data.updatedDate,
      } as TreatmentServiceProviderDetailModel
  }

  refresh(): boolean {
    return false;
  }
}
