import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormGroupName, FormControl, AbstractControl } from '@angular/forms';
import { MedicationDetailModel, MedicationsProvidersHospitalizationModel, TreatmentServiceProviderDetailModel } from './medications-providers-hospitalization.model';
import { GridOptions, GridApi, Module } from 'ag-grid-community';

import { CommonService } from 'src/app/services/helper-services/common.service';
import { RefGroupDetails } from '../../../models/refGroupDetailsDropDown.model';//'src/app/models/refGroupDetailsDropDown.module';

//import { DocActionComponent } from '../../../shared/document/doc-action.component';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { MedicationProvidersHospitalizationService } from './medication-providers-hospitalization.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { AuthData } from 'src/app/models/auth-data.model';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/helper-services/user.service';
import { MedicationActionComponent } from './medication-action.component';
import { TreatmentServiceActionComponent } from './treatmentService-action.component';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { ActivatedRoute, Router, NavigationStart, ɵangular_packages_router_router_b } from '@angular/router';
import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';

@Component({
  selector: 'app-medications-providers-hospitalization',
  templateUrl: './medications-providers-hospitalization.component.html',
  styleUrls: ['./medications-providers-hospitalization.component.scss']
})

export class MedicationsProvidersHospitalizationComponent implements OnInit, OnDestroy {

  //#region Properties
  medicationsForm: FormGroup;
  treatmentServicesFormGroup: FormGroup;
  hospitalizationsFormGroup: FormGroup;
  appID: number;
  medicationDetailFormGroup: FormGroup;
  careCoordinationFormGroup: FormGroup;
  crisisInterventionFormGroup: FormGroup;
  tabSelectedIndex: number = 0;
  tab1Status = 2;
  tab2Status = 2;
  tab3Status = 2;
  tab4Status = 2;
  tab5Status = 2;
  refGroupIDs: any = [32, 33, 34, 35, 36, 7];
  public refGroupYesID: number = 33;
  public refGroupNoID: number = 34;
  now = new Date();
  minDate = this.minDateCalculate();
  saveToster: boolean = false;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  //medicationDetailsFromArray: FormArray;
  medicationDetailsData: MedicationDetailModel;
  medicationsProvidersHospitalizationData: MedicationsProvidersHospitalizationModel;
  treatmentServiceProviderDetail: TreatmentServiceProviderDetailModel;
  private medicationGridApi: any;
  private medicationGridColumnApi: any;
  private medicationColumnDefs: any;
  private rowModelType: any;
  private rowSelection: any;
  pagination: any;
  overlayMedicationDetailsLoadingTemplate: string;
  overlayMedicationDetailsNoRowsTemplate: string;

  medicationDetailsRowData: MedicationDetailModel[];
  public gridOptions: GridOptions;
  public treatmentServicesGridOptions: GridOptions;
  defaultColDef: any;
  context: any;
  frameworkComponents: any;
  treatmentServicesframeworkComponents: any;
  overlayTreatmentServicesLoadingTemplate: string;
  overlayTreatmentServicesNoRowsTemplate: string;


  private treatmentServicesGridApi: any;
  private treatmentServicesGridColumnApi: any;
  private treatmentServicesColumnDefs: any;
  treatmentServicesDetailsRowData: TreatmentServiceProviderDetailModel[];
  isCurrentTreatmentServiceProvidersTouched: boolean = false;

  medicationType: RefGroupDetails[];
  levelOfSupports: RefGroupDetails[];
  treatmentServiceTypes: RefGroupDetails[];
  filteredtreatmentServiceModalities: RefGroupDetails[];
  treatmentServiceModalities: RefGroupDetails[];
  applCurrHospServiceType: RefGroupDetails[];
  applCurrHospType: RefGroupDetails[];
  radioBtnYesNo: RefGroupDetails[];
  isappCurrHospRequired: boolean;

  preSelectedlevelOfSupport: number = 0;
  pactAppID = 0;
  userData: AuthData;
  userDataSub: Subscription;
  //routeSub: any; // Subscription to route observer
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;


  //#endregion

  //#region Validation
  readonly validationMssg = {
    isApplicantPsychiatricallyHospitalizedCtrl: {
      pattern: '0-9 digits only',
      max: 'Age of first Psychiatric Hospitalization must be a number between 1-118',
    },
    medicationNameCtrl: {
      required: 'Medication Name is required',
    },
    medicationTypeDataCtrl: {
      radioGroupRequired: 'Medication Type is required',
    },
    levelOfSupportCtrl: {
      dropdownRequired: 'Level of Support is required.',
    },
    treatmentServiceTypeCtrl: {
      dropdownRequired: 'Type is required.',
    },
    treatmentServiceModalitiesCtrl: {
      dropdownRequired: 'Modality is required.',
    },
    agencyProgramNameCtrl: {
      required: 'Agency/Program Name is required',
    },
    providerContactNameCtrl: {
      charLength: 'Provider Contact Name should be atleast 3 characters',
    },
    noOfPsychiatricHospitalizationInPast3YrsCtrl: {
      max: 'Psychiatric hospitalization in past 3 years must be a number between 1-999',
    },
    healthHomeAgencyProgPhoneCtrl: {
      'Mask error': 'Invalid Phone number. Please enter date 10 digit phone number',
    },
    mltcAgencyProgramPhoneCtrl: {
      'Mask error': 'Invalid Phone number. Please enter date 10 digit phone number',
      required: 'mltc Agency program phone is required',
    },
    phoneCtrl: {
      'Mask error': 'Invalid Phone number. Please enter date 10 digit phone number',
    },
    recentDischargeDateCtrl: {
      'matDatepickerMax': 'Recent Date cannot be future',
    },
    admissionDateCtrl: {
      'matDatepickerMax': 'Admission Date cannot be future',
      required: 'Admission date is required',
      max: 'test',
    },
    recentPhychAssessmentDateCtrl: {
      'matDatepickerMax': 'Admission Date cannot be future',
    },
    recentEmergencyServicesDateCtrl: {
      'matDatepickerMax': 'Admission Date cannot be future',
      max: ' test',
    },
    hospitalNameCtrl: {
      required: 'Hospital Name is required',
      charLength: 'Hospital Name should be atleast 3 characters',
    },
    serviceTypeCtrl: {
      required: 'Service Type is required',
    },
    mltcAgencyProgramNameCtrl: {
      required: 'mltc Agency program name is required',
      charLength: 'Agency Program Name should be atleast 3 characters',
    },
    mltcAgencyProgramAddressCtrl: {
      required: 'mltc Agency program address is required',
      charLength: 'Agency Program Name should be atleast 3 characters',
    },
    healthHomeAgencyProgNameCtrl: {
      required: 'Health home agency program name is required',
      charLength: 'Health home agency Program Name should be atleast 3 characters',
    },
    healthHomeAgencyProgAddressCtrl: {
      required: 'Health home agency program address is required',
      charLength: 'Health home agency Program Name should be atleast 3 characters',
    }
  };

  formErrors = {
    isApplicantPsychiatricallyHospitalizedCtrl: '',
    //medicationNameCtrl: '',
    //medicationTypeDataCtrl: '',
    levelOfSupportCtrl: '',
    treatmentServiceTypeCtrl: '',
    treatmentServiceModalitiesCtrl: '',
    //agencyProgramNameCtrl: '',
    healthHomeAgencyProgPhoneCtrl: '',
    mltcAgencyProgramPhoneCtrl: '',
    phoneCtrl: '',
    recentEmergencyServicesDateCtrl: '',
    serviceTypeCtrl: '',
    hospitalNameCtrl: '',
    mltcAgencyProgramNameCtrl: '',
    mltcAgencyProgramAddressCtrl: '',
    healthHomeAgencyProgNameCtrl: '',
    healthHomeAgencyProgAddressCtrl: '',

  };



  //#endregion

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private userService: UserService,
    private medProvHospService: MedicationProvidersHospitalizationService,
    private confirmDialogService: ConfirmDialogService,
    private sidenavStatusService: SidenavStatusService,
    private route: ActivatedRoute,
    private clientApplicationService: ClientApplicationService,
    private router: Router,
    private toastr: ToastrService,
  ) {
    this.gridOptions = {
      rowHeight: 35,
      context: { componentParent: this }
    } as GridOptions;
    this.treatmentServicesGridOptions = {
      rowHeight: 35,
      context: { componentParent: this }
    } as GridOptions;
    this.medicationColumnDefs = [
      {
        field: "medicationName",
        headerName: "Medication Name"
      },
      {
        field: "medicationTypeDescription",
        headerName: "Medication Type"
      },
      {
        headerName: 'Actions',
        field: 'action',
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];
    this.treatmentServicesColumnDefs = [
      {
        field: "agencyProgramName",
        headerName: "Agency/Program Name",
        width: 100,
      },
      {
        field: "providerContactName",
        headerName: "Provider Contact Name",
        width: 100,
      },
      {
        field: "contactPhoneNo",
        headerName: "Phone",
        width: 100,
      },
      {
        field: "treatmentTypeDescription",
        headerName: "Type",
        width: 150,
      },
      {
        field: "modalityTypeDescription",
        headerName: "Modality",
        width: 150,
      },
      {
        headerName: 'Actions',
        field: 'action',
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ]
    //this.medicationDetailsListData = [];
    this.rowSelection = "single";
    this.rowModelType = "serverSide";
    this.pagination = true;
    this.context = { componentParent: this };
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.frameworkComponents = {
      actionRenderer: MedicationActionComponent
    };
    this.treatmentServicesframeworkComponents = {
      actionRenderer: TreatmentServiceActionComponent
    }
  }

  ngOnInit() {
    //this.sidenavStatusService._sidenavStatusReload(this.route);
    this.buildForm();
    this.onChanges();
    this.medicationsForm.valueChanges.subscribe(data => {
      this.logValidationErrors(this.medicationsForm);
    });

    // this.clientApplicationService.getPactApplicationID().subscribe(appID => {
    //   this.pactAppID = appID;
    //   if (this.pactAppID > 0) {
    //     this.loadRequiredData();
    //   }
    // });

    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.pactAppID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactAppID);
          this.loadRequiredData();
        }
      }
    });



    this.activatedRouteSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.medicationsForm.touched && event.navigationTrigger != "popstate") {
          this.saveMedicationProvidersHospitalization();
        }
        if (event.navigationTrigger == "popstate" && this.medicationsForm.dirty) // Handles forward and backward browser clicks
        {
          this.saveMedicationProvidersHospitalization();
        }

      };
    });
  }

  loadRequiredData() {
    this.loadRefGroupDetails();
    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });
    this.getMedicationProvidersHospitalization();
  }

  //#region Form Controls Builder

  buildForm(): void {

    this.medicationDetailFormGroup = this.formBuilder.group({
      medicationNameCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
      medicationTypeDataCtrl: ['', [CustomValidators.radioGroupRequired()]]
    })

    this.treatmentServicesFormGroup = this.formBuilder.group({
      treatmentServiceTypeCtrl: [0, [CustomValidators.dropdownRequired()]],
      treatmentServiceModalitiesCtrl: [0, [CustomValidators.dropdownRequired()]],
      agencyProgramNameCtrl: ['', [Validators.required, CustomValidators.requiredCharLength(2)]],
      providerContactNameCtrl: ['', [CustomValidators.minimunCharLength(2)]],
      phoneCtrl: ['', null],
    })

    this.hospitalizationsFormGroup = this.formBuilder.group({
      //isApplicantPsychiatricallyHospitalizedCtrl: ['', [Validators.pattern('^[0-9]*$'), Validators.max(118), Validators.min(0)]],
      isApplicantPsychiatricallyHospitalizedCtrl:[{value :0},null],
      noOfPsychiatricHospitalizationInPast3YrsCtrl: [{ value: '' }, [Validators.pattern('^[0-9]*$'), Validators.max(999), Validators.min(0)]],
      recentDischargeDateCtrl: [{ value: '' }, null],
      isCurrentHospitalizedCtrl: [{ value: 0 }, Validators.required],
      admissionDateCtrl: [{ value: '' }, null],
      serviceTypeCtrl: [{ value: 0 }, null],
      hospitalNameCtrl: ['', null],
    })

    this.careCoordinationFormGroup = this.formBuilder.group({
      mltcPlanCtrl: ['', null],
      mltcAgencyProgramNameCtrl: ['', null],
      mltcAgencyProgramAddressCtrl: ['', null],
      mltcAgencyProgramPhoneCtrl: [''],
      healthHomeCtrl: ['', null],
      healthHomeAgencyProgNameCtrl: ['', null],
      healthHomeAgencyProgAddressCtrl: ['', null],
      healthHomeAgencyProgPhoneCtrl: [''],
    })

    this.crisisInterventionFormGroup = this.formBuilder.group({
      everTakenForPsychiatricAssessmentCtrl: ['', null],
      recentPhychAssessmentDateCtrl: ['', null],
      everTakenToEmergencyServicesCtrl: ['', null],
      recentEmergencyServicesDateCtrl: [''],
    })

    this.medicationsForm = this.formBuilder.group({
      medicationGroupForm: this.medicationDetailFormGroup,
      levelOfSupportCtrl: [0, [CustomValidators.dropdownRequired()]],
      treatmentServiceGroupForm: this.treatmentServicesFormGroup,
      hospitalizationsForm: this.hospitalizationsFormGroup,
      careCoordinationForm: this.careCoordinationFormGroup,
      crisisInterventionForm: this.crisisInterventionFormGroup,
    });


    //this.medicationsForm.valueChanges.subscribe(data => this.logValidationErrors(this.medicationsForm));
  }

  logValidationErrors = (group: FormGroup): void => {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      } else {
        this.formErrors[key] = '';
        if (abstractControl && !abstractControl.valid && (abstractControl.touched || abstractControl.dirty)) {
          const messages = this.validationMssg[key];
          for (const errorKey in abstractControl.errors) {
            if (errorKey) {
              this.formErrors[key] += messages[errorKey] + ' ';
            }
          }
        }
      }
    });
  }

  getMedicationProvidersHospitalization(): void {
    this.medProvHospService.getMedicationProviderHospitalizationDetail<MedicationsProvidersHospitalizationModel>(this.pactAppID).subscribe(res => {
      this.medicationsProvidersHospitalizationData = res as MedicationsProvidersHospitalizationModel;
      if (this.isControlValueNotNull(this.medicationsProvidersHospitalizationData.medicationsDetails) && this.medicationsProvidersHospitalizationData.medicationsDetails.length > 0) {
        this.medicationDetailsRowData = this.medicationsProvidersHospitalizationData.medicationsDetails;
      }
      if (this.isControlValueNotNull(this.medicationsProvidersHospitalizationData.treatmentServiceProvidersDetails) && this.medicationsProvidersHospitalizationData.treatmentServiceProvidersDetails.length > 0) {
        this.treatmentServicesDetailsRowData = this.medicationsProvidersHospitalizationData.treatmentServiceProvidersDetails;
      }

      this.medicationDetailFormGroup.controls.medicationNameCtrl.setErrors(null);
      this.treatmentServicesFormGroup.controls.agencyProgramNameCtrl.setErrors(null);
      this.treatmentServicesFormGroup.controls.treatmentServiceTypeCtrl.setErrors(null);
      this.treatmentServicesFormGroup.controls.treatmentServiceModalitiesCtrl.setErrors(null);

      this.tab1Status = this.medicationsProvidersHospitalizationData.isCurrentMedicationsTabComplete ? 1 : 0;
      this.tab2Status = this.medicationsProvidersHospitalizationData.isCurrentTreatmentServiceProvidersTabComplete ? 1 : 0;
      this.tab3Status = this.medicationsProvidersHospitalizationData.isHospitalizationsTabComplete ? 1 : 0;
      this.tab4Status = this.medicationsProvidersHospitalizationData.isCareCoordinationTabComplete ? 1 : 0;
      this.tab5Status = this.medicationsProvidersHospitalizationData.isCrisisInterventionTabComplete ? 1 : 0;

      this.mapToControls();

      this.isCurrentTreatmentServiceProvidersTouched = this.medicationsProvidersHospitalizationData.isCurrentTreatmentServiceProvidersTabComplete;

      if (!this.isControlValueNotNull(this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID)) {
        // console.log("Initial Save");
        this.initialSave();
      }
    });
  }

  initialSave() {
    this.medProvHospService.saveMedicationProviderHospitalization(this.mapFromControls()).subscribe(data => {
      this.medProvHospService.getMedicationProviderHospitalizationDetail<MedicationsProvidersHospitalizationModel>(this.pactAppID).subscribe(res => {
        this.medicationsProvidersHospitalizationData = res as MedicationsProvidersHospitalizationModel;
      });
    });
  }


  saveMedicationProvidersHospitalization(): void {
    if (this.medicationDetailFormGroup.touched)
      this.addMedications(false);
    if (this.treatmentServicesFormGroup.touched)
      this.addTreatmentServices(false);
    this.medProvHospService.saveMedicationProviderHospitalization(this.mapFromControls())
      .subscribe(
        data => {
          if (this.saveToster) {
            if (this.isCurrentMedicationTabComplete() &&
              this.isCurrentTreatmentServiceProvidersTabComplete() &&
              this.isHospitalizationsTabComplete() &&
              this.isCareCoordinationTabComplete() &&
              this.isCrisisInterventionTabComplete()) {
              this.toastr.success('Medication Providers Hospitalization is Completed', ' ');
            }
            else {
              this.toastr.info('Medication Providers Hospitalization is Partially Completed', '');
            }
            this.saveToster = false;
          }

          this.getMedicationProvidersHospitalization();
          // this.sidenavStatusService._sidenavStatusReload(this.route);
          if (this.pactAppID) {
            this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactAppID);
          }
          this.medicationDetailFormGroup.reset();
          this.treatmentServicesFormGroup.reset({ treatmentServiceModalitiesCtrl: 0, treatmentServiceTypeCtrl: 0 });
          //console.log(1);
        }
      )
  }
  //#endregion

  //#region Current medication pactMedicationsProvidersHospitalizationID
  addMedications(isErrorMessageShown: boolean): void {
    if (this.isMedicationFormValid()) {
      var medicationName: string = this.medicationDetailFormGroup.controls.medicationNameCtrl.value.trim();
      var medicationTypeId: number = +this.medicationDetailFormGroup.controls.medicationTypeDataCtrl.value.toString().trim();
      if (this.isControlValueNotNull(medicationName) && this.isControlValueNotNull(medicationTypeId)) {
        if (this.isControlValueNotNull(this.medicationsProvidersHospitalizationData.medicationsDetails) && this.medicationsProvidersHospitalizationData.medicationsDetails.length > 0) {
          if (!this.medicationsProvidersHospitalizationData.medicationsDetails.some(item => item.medicationName === medicationName)) {
            this.medicationDetailsData = {
              pactMedicationsProvidersHospitalizationID: this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID,
              medicationName: medicationName,
              medicationType: medicationTypeId,
              medicationTypeDescription: null,
              createdBy: this.userData.optionUserId,
              updatedBy: this.userData.optionUserId,
              isActive: true,
              createdDate: new Date(),
              updatedDate: new Date()
            } as MedicationDetailModel;
            this.saveMedications();
            this.medicationDetailFormGroup.reset();
          }
          else {
            this.toastr.error("Record already exists.");
          }
        }
        else {
          this.medicationDetailsData = {
            pactMedicationsProvidersHospitalizationID: this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID,
            medicationName: medicationName,
            medicationType: medicationTypeId,
            medicationTypeDescription: null,
            createdBy: this.userData.optionUserId,
            updatedBy: this.userData.optionUserId,
            isActive: true,
            createdDate: new Date(),
            updatedDate: new Date()
          } as MedicationDetailModel;
          this.saveMedications();
          this.medicationDetailFormGroup.reset();
        }
      }
      else {
        if (isErrorMessageShown) {
          this.markFormGroupTouched(this.medicationDetailFormGroup);
          this.toastr.error("Both Medication Name and Medication Type are required fields.");
        }
      }
    }
    else {
      if (isErrorMessageShown) {
        this.markFormGroupTouched(this.medicationDetailFormGroup);
        this.toastr.error("Both Medication Name and Medication Type are required fields.");
      }
    }
  }

  isMedicationFormValid(): boolean {
    var returnval: boolean = false;
    if (this.medicationDetailFormGroup.valid) {
      if (this.isControlValueNotNull(this.medicationDetailFormGroup.controls.medicationNameCtrl.value)
        && this.isControlValueNotNull(this.medicationDetailFormGroup.controls.medicationTypeDataCtrl.value)) {
        returnval = true;
      }
    }
    return returnval;
  }

  saveMedications(): void {
    this.medProvHospService.saveMedicationDetail(this.medicationDetailsData)
      .subscribe(
        data => {
          this.getPactMedciationDetails();
        }
      )
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady = (params) => {
    this.medicationGridApi = params.api;
    this.medicationGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.medicationGridColumnApi.getAllColumns().forEach(function (column) {
      // tslint:disable-next-line: max-line-length
      if (column.colId !== 'medicationName') {
        allColumnIds.push(column.colId);
      }
    });
    this.medicationGridColumnApi.autoSizeColumns(allColumnIds);

    // params.api.expandAll();

    this.getPactMedciationDetails();

    params.api.sizeColumnsToFit();
    params.api.setDomLayout('autoHeight');
    this.overlayMedicationDetailsLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the medication details are loading.</span>';
    this.overlayMedicationDetailsNoRowsTemplate = '<span style="color:#337ab7;">No medications Available</span>';

  }

  getPactMedciationDetails = () => {

    if (this.medicationsProvidersHospitalizationData != null && this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID != null
      && this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID != 0) {
      this.medProvHospService.getMedicationDetailsList<MedicationDetailModel[]>(this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID).subscribe(res => {
        this.medicationDetailsRowData = res as MedicationDetailModel[];
        this.medicationsProvidersHospitalizationData.medicationsDetails = res;
        this.medicationGridApi.sizeColumnsToFit();
      });
    }
    else {
      this.medicationDetailsRowData = [];
    }

  }

  medicationDeleteParent = (cell: MedicationDetailModel) => {
    const title = 'Confirm Delete';
    const primaryMessage = `Wish to remove the selected medication ?`;
    const secondaryMessage = ``;
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      (positiveResponse) => {
        this.medicationDetailsData = cell;
        this.medicationDetailsData.updatedBy = this.userData.optionUserId;
        this.medicationDetailsData.updatedDate = new Date();
        this.medicationDetailsData.isActive = false;
        this.saveMedications();
      },
      (negativeResponse) => console.log(),
    );

  }

  //#endregion

  //#region Treatment Services
  addTreatmentServices(isErrorMessageShown: boolean): void {
    if (this.isTreatmentServicesFormGroupValid()) {
      if (this.isControlValueNotNull(this.treatmentServicesFormGroup.controls.agencyProgramNameCtrl.value)) {
        var agencyProgramName: string = this.treatmentServicesFormGroup.controls.agencyProgramNameCtrl.value.trim();
        if (this.isControlValueNotNull(this.treatmentServiceProviderDetail) && this.isControlValueNotNull(this.treatmentServiceProviderDetail.pactTreatmentServiceProvidersDetailsID)) {
          this.treatmentServiceProviderDetail.agencyProgramName = this.treatmentServicesFormGroup.controls.agencyProgramNameCtrl.value;
          this.treatmentServiceProviderDetail.providerContactName = this.treatmentServicesFormGroup.controls.providerContactNameCtrl.value;
          this.treatmentServiceProviderDetail.contactPhoneNo = this.treatmentServicesFormGroup.controls.phoneCtrl.value;
          this.treatmentServiceProviderDetail.treatmentType = this.treatmentServicesFormGroup.controls.treatmentServiceTypeCtrl.value;
          this.treatmentServiceProviderDetail.modalityType = this.treatmentServicesFormGroup.controls.treatmentServiceModalitiesCtrl.value;
          this.treatmentServiceProviderDetail.updatedBy = this.userData.optionUserId;
          this.treatmentServiceProviderDetail.updatedDate = new Date();
          this.saveTreatmentServiceProviderDetail();
          this.treatmentServicesFormGroup.reset({ treatmentServiceModalitiesCtrl: 0, treatmentServiceTypeCtrl: 0 });
        }
        else {
          if (!this.medicationsProvidersHospitalizationData.treatmentServiceProvidersDetails.some(
            item => item.agencyProgramName === this.treatmentServicesFormGroup.controls.agencyProgramNameCtrl.value &&
              item.treatmentType === +this.treatmentServicesFormGroup.controls.treatmentServiceTypeCtrl.value &&
              item.modalityType === +this.treatmentServicesFormGroup.controls.treatmentServiceModalitiesCtrl.value)) {
            this.treatmentServiceProviderDetail = this.getTreatmentServiceFromControl();
            this.saveTreatmentServiceProviderDetail();
            this.treatmentServicesFormGroup.reset({ treatmentServiceModalitiesCtrl: 0, treatmentServiceTypeCtrl: 0 });
          }
          else {
            this.toastr.error("Record already exists.");
          }
        }
      }
      else {
        if (isErrorMessageShown) {
          this.markFormGroupTouched(this.treatmentServicesFormGroup);
          this.toastr.error("Agency/Program Name, Type and Modality are required fields.");
        }
      }

    }
    else {
      if (isErrorMessageShown) {
        this.markFormGroupTouched(this.treatmentServicesFormGroup);
        this.toastr.error("Agency/Program Name, Type and Modality are required fields.");
      }
    }
  }

  isTreatmentServicesFormGroupValid(): boolean {
    var returnval: boolean = false;
    if (this.treatmentServicesFormGroup.valid) {
      if (this.isControlValueNotNull(this.treatmentServicesFormGroup.controls.agencyProgramNameCtrl.value)
        && (this.treatmentServicesFormGroup.controls.agencyProgramNameCtrl.value.toString().length >= 2)
        && this.isControlValueNotNull(this.treatmentServicesFormGroup.controls.treatmentServiceTypeCtrl.value)
        && this.isControlValueNotNull(this.treatmentServicesFormGroup.controls.treatmentServiceModalitiesCtrl.value)) {
        returnval = true;
      }
      if (this.isControlValueNotNull(this.treatmentServicesFormGroup.controls.providerContactNameCtrl.value) && returnval) {
        if (this.treatmentServicesFormGroup.controls.providerContactNameCtrl.value.toString().length >= 2) {
          returnval = true;
        }
        else { returnval = false; }
      }
      if (returnval && this.isControlValueNotNull(this.treatmentServicesFormGroup.controls.phoneCtrl.value)) {
        if (this.treatmentServicesFormGroup.controls.phoneCtrl.value.toString().length == 10) {
          returnval = true;
        }
        else { returnval = false; }
      }
    }
    return returnval;
  }

  getTreatmentServiceFromControl() {
    return {
      pactMedicationsProvidersHospitalizationID: this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID,
      agencyProgramName: this.treatmentServicesFormGroup.controls.agencyProgramNameCtrl.value.trim(),
      providerContactName: this.trimValue(this.treatmentServicesFormGroup.controls.providerContactNameCtrl.value),
      contactPhoneNo: this.treatmentServicesFormGroup.controls.phoneCtrl.value,
      treatmentType: +this.treatmentServicesFormGroup.controls.treatmentServiceTypeCtrl.value,
      treatmentTypeDescription: null,
      modalityType: this.treatmentServicesFormGroup.controls.treatmentServiceModalitiesCtrl.value,
      modalityTypeDescription: null,
      createdBy: this.userData.optionUserId,
      updatedBy: this.userData.optionUserId,
      isActive: true,
      createdDate: new Date(),
      updatedDate: new Date()
    } as TreatmentServiceProviderDetailModel;
  }


  saveTreatmentServiceProviderDetail(): void {
    this.medProvHospService.saveTreatmentServiceProvidersDetail(this.treatmentServiceProviderDetail)
      .subscribe(
        data => {
          this.treatmentServicesFormGroup.reset({ treatmentServiceModalitiesCtrl: 0, treatmentServiceTypeCtrl: 0 });
          this.getPactTreatmentServicesDetails();
          this.treatmentServiceProviderDetail = null;
        }
      )
  }
  onTreatmentServicesGridReady = (params) => {
    this.treatmentServicesGridApi = params.api;
    this.treatmentServicesGridColumnApi = params.columnApi;

    const allColumnIds = [];
    this.treatmentServicesGridColumnApi.getAllColumns().forEach(function (column) {
      // tslint:disable-next-line: max-line-length
      if (column.colId !== 'medicationName') {
        allColumnIds.push(column.colId);
      }
    });
    this.treatmentServicesGridColumnApi.autoSizeColumns(allColumnIds);

    // params.api.expandAll();

    this.getPactTreatmentServicesDetails();

    params.api.sizeColumnsToFit();
    params.api.setDomLayout('autoHeight');
    this.overlayTreatmentServicesLoadingTemplate = '<span class="ag-overlay-loading-center">Please wait while the Treatment services are loading.</span>';
    this.overlayTreatmentServicesNoRowsTemplate = '<span style="color:#337ab7;">No Treatment Services Available</span>';
  }

  getPactTreatmentServicesDetails = () => {

    if (this.medicationsProvidersHospitalizationData != null && this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID != null
      && this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID != 0) {
      this.medProvHospService.getTreatmentServiceProviderDetailsList<TreatmentServiceProviderDetailModel[]>(this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID).subscribe(res => {
        this.treatmentServicesDetailsRowData = res as TreatmentServiceProviderDetailModel[];
        this.medicationsProvidersHospitalizationData.treatmentServiceProvidersDetails = res;
        this.treatmentServicesGridApi.sizeColumnsToFit();
      });
    }
    else {
      this.treatmentServicesDetailsRowData = [];
    }

  }
  treatmentServiceDeleteParent = (cell: TreatmentServiceProviderDetailModel) => {
    this.treatmentServiceProviderDetail = null;
    const title = 'Confirm Delete';
    const primaryMessage = `Wish to remove the selected `;
    const secondaryMessage = `Service Provider information ?`;
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
      (positiveResponse) => {
        this.treatmentServiceProviderDetail = cell;
        this.treatmentServiceProviderDetail.updatedBy = this.userData.optionUserId;
        this.treatmentServiceProviderDetail.updatedDate = new Date();
        this.treatmentServiceProviderDetail.isActive = false;
        this.saveTreatmentServiceProviderDetail();

      },
      (negativeResponse) => console.log(),
    );

  }

  treatmentServiceViewParent = (cell: TreatmentServiceProviderDetailModel) => {
    this.treatmentServiceProviderDetail = null;
    this.treatmentServiceProviderDetail = cell;
    this.medicationsForm.get('treatmentServiceGroupForm').get('agencyProgramNameCtrl').setValue(this.treatmentServiceProviderDetail.agencyProgramName);
    this.medicationsForm.get('treatmentServiceGroupForm').get('treatmentServiceTypeCtrl').setValue(this.treatmentServiceProviderDetail.treatmentType);
    this.medicationsForm.get('treatmentServiceGroupForm').get('treatmentServiceModalitiesCtrl').setValue(this.treatmentServiceProviderDetail.modalityType);
    this.medicationsForm.get('treatmentServiceGroupForm').get('providerContactNameCtrl').setValue(this.treatmentServiceProviderDetail.providerContactName);
    this.medicationsForm.get('treatmentServiceGroupForm').get('phoneCtrl').setValue(this.treatmentServiceProviderDetail.contactPhoneNo);
  }
  //#endregion

  //#region Events
  onChanges() {

    this.treatmentServicesFormGroup.controls.treatmentServiceTypeCtrl.valueChanges.subscribe(selectedValue =>{
      this.filteredtreatmentServiceModalities = this.treatmentServiceModalities.filter(d => { return d.parentRefGroupDetailID === selectedValue });
    });
    this.hospitalizationsFormGroup.controls.isCurrentHospitalizedCtrl.valueChanges.subscribe(selectedValue => {
      if (selectedValue != this.refGroupYesID) {
        this.hospitalizationsFormGroup.controls.admissionDateCtrl.setValue(null);
        this.hospitalizationsFormGroup.controls.serviceTypeCtrl.setValue(0);
        this.hospitalizationsFormGroup.controls.hospitalNameCtrl.setValue(null);
        //this.hospitalizationsFormGroup.controls.admissionDateCtrl.setValidators(null);
        this.hospitalizationsFormGroup.controls.serviceTypeCtrl.clearValidators();
        this.hospitalizationsFormGroup.controls.hospitalNameCtrl.clearValidators();
      }
      else if (selectedValue == this.refGroupYesID) {
        //this.hospitalizationsFormGroup.controls.admissionDateCtrl.setValidators([Validators.required]);
        this.hospitalizationsFormGroup.controls.serviceTypeCtrl.setValidators([Validators.required]);
        this.hospitalizationsFormGroup.controls.hospitalNameCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);
      }
    });

    this.hospitalizationsFormGroup.controls.isApplicantPsychiatricallyHospitalizedCtrl.valueChanges.subscribe(selectedValue => {
      //if (this.isControlValueNotNull(this.hospitalizationsFormGroup.controls.isApplicantPsychiatricallyHospitalizedCtrl.value) && this.hospitalizationsFormGroup.controls.isApplicantPsychiatricallyHospitalizedCtrl.value <= 110) {
        if (selectedValue != this.refGroupYesID) {
        this.hospitalizationsFormGroup.controls.noOfPsychiatricHospitalizationInPast3YrsCtrl.setValue(null);
        this.hospitalizationsFormGroup.controls.recentDischargeDateCtrl.setValue(null);
      }
    });

    this.careCoordinationFormGroup.controls.mltcPlanCtrl.valueChanges.subscribe(selectedValue => {
      if (selectedValue != this.refGroupYesID) {
        this.careCoordinationFormGroup.controls.mltcAgencyProgramNameCtrl.clearValidators();
        this.careCoordinationFormGroup.controls.mltcAgencyProgramAddressCtrl.clearValidators(); 
        this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.clearValidators();
        this.careCoordinationFormGroup.controls.mltcAgencyProgramNameCtrl.setValue(null);
        this.careCoordinationFormGroup.controls.mltcAgencyProgramAddressCtrl.setValue(null);
        this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.setValue(null);
      }
      else if (selectedValue == this.refGroupYesID) {
        this.careCoordinationFormGroup.controls.mltcAgencyProgramNameCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);
        this.careCoordinationFormGroup.controls.mltcAgencyProgramAddressCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);
        this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.setValidators([Validators.required]);
        this.careCoordinationFormGroup.controls.mltcAgencyProgramNameCtrl.markAsUntouched();
        this.careCoordinationFormGroup.controls.mltcAgencyProgramAddressCtrl.markAsUntouched();
        this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.markAsUntouched();
      }
    });

    this.careCoordinationFormGroup.controls.healthHomeCtrl.valueChanges.subscribe(selectedValue => {
      if (selectedValue != this.refGroupYesID) {
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgNameCtrl.setValue(null);
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgAddressCtrl.setValue(null);
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.setValue(null);
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgNameCtrl.clearValidators();
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgAddressCtrl.clearValidators();
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.clearValidators();
      }
      else if (selectedValue == this.refGroupYesID) {
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgNameCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgAddressCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(2)]);
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.setValidators([Validators.required]);
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgNameCtrl.markAsUntouched();
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgAddressCtrl.markAsUntouched();
        this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.markAsUntouched();
      }
    });

    this.crisisInterventionFormGroup.controls.everTakenForPsychiatricAssessmentCtrl.valueChanges.subscribe(selectedValue => {
      if (selectedValue != this.refGroupYesID) {
        this.crisisInterventionFormGroup.controls.recentPhychAssessmentDateCtrl.setValue(null);
      }
    });

    this.crisisInterventionFormGroup.controls.everTakenToEmergencyServicesCtrl.valueChanges.subscribe(selectedValue => {
      if (selectedValue != this.refGroupYesID) {
        this.crisisInterventionFormGroup.controls.recentEmergencyServicesDateCtrl.setValue(null);
      }
    });


    this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.markAsTouched();

  }

  onTabChanged($event) {
    let clickedIndex = $event.index;
    if (clickedIndex == 1) {
      this.isCurrentTreatmentServiceProvidersTouched = true;
    }
    this.saveMedicationProvidersHospitalization();
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      //control.touched =true;
      control.markAsTouched();
      control.updateValueAndValidity();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  isAgeOfFirstPsychHospValid() {
    //if (this.isControlValueNotNull(this.hospitalizationsFormGroup.controls.isApplicantPsychiatricallyHospitalizedCtrl.value) && this.hospitalizationsFormGroup.controls.isApplicantPsychiatricallyHospitalizedCtrl.value <= 110) {
      if (this.hospitalizationsFormGroup.controls.isApplicantPsychiatricallyHospitalizedCtrl.value == this.refGroupYesID) {
      return true;
    }
    return false;
  }

  onSubmit() {
    this.saveToster = true;
    this.saveMedicationProvidersHospitalization();
  }

  PreviousPage() {
    if (this.tabSelectedIndex == 0) {
      this.sidenavStatusService.routeToPreviousPage(this.router, this.route);
    }
    else {
      this.tabSelectedIndex--;
    }
  }

  NextPage() {
    if (this.tabSelectedIndex == 4) {
      this.sidenavStatusService.routeToNextPage(this.router, this.route);
    }
    else {
      this.tabSelectedIndex++;
    }
  }
  //#endregion

  //#region Common functions
  loadRefGroupDetails = () => {
    //var value = "32,33,34,35,36,7";
    this.commonService.getRefGroupDetails(this.refGroupIDs.toString())
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.medicationType = data.filter(d => { return d.refGroupID === 32 });
          this.levelOfSupports = data.filter(d => { return d.refGroupID === 33 });
          this.treatmentServiceTypes = data.filter(d => { return d.refGroupID === 34 });
          this.treatmentServiceModalities = data.filter(d => { return d.refGroupID === 35 });
          this.applCurrHospServiceType = data.filter(d => { return d.refGroupID === 36 });
          this.applCurrHospType = data.filter(d => { return d.refGroupID === 7 && (d.refGroupDetailDescription == "Yes" || d.refGroupDetailDescription == "No") });
          this.radioBtnYesNo = data.filter(d => { return d.refGroupID === 7 && (d.refGroupDetailDescription == "Yes" || d.refGroupDetailDescription == "No") });
        },
        error => console.error('Error!', error)
      );
  }

  mapToControls() {
    this.medicationsForm.controls.levelOfSupportCtrl.setValue(this.medicationsProvidersHospitalizationData.levelOfSupportType);

    this.hospitalizationsFormGroup.controls.isApplicantPsychiatricallyHospitalizedCtrl.setValue(this.medicationsProvidersHospitalizationData.isApplicantPsychiatricallyHospitalized);
    this.hospitalizationsFormGroup.controls.noOfPsychiatricHospitalizationInPast3YrsCtrl.setValue(this.medicationsProvidersHospitalizationData.noOfPsychiatricHospitalizationInPast3Yrs);
    this.hospitalizationsFormGroup.controls.recentDischargeDateCtrl.setValue(this.FormatDatetoCtrl(this.medicationsProvidersHospitalizationData.recentDischargeDate));
    this.hospitalizationsFormGroup.controls.isCurrentHospitalizedCtrl.setValue(this.medicationsProvidersHospitalizationData.isCurrentlyHospitalized);
    this.hospitalizationsFormGroup.controls.admissionDateCtrl.setValue(this.FormatDatetoCtrl(this.medicationsProvidersHospitalizationData.admissionDate));
    this.hospitalizationsFormGroup.controls.serviceTypeCtrl.setValue(this.medicationsProvidersHospitalizationData.serviceType);
    this.hospitalizationsFormGroup.controls.hospitalNameCtrl.setValue(this.medicationsProvidersHospitalizationData.hospitalName);
    this.careCoordinationFormGroup.controls.mltcPlanCtrl.setValue(this.medicationsProvidersHospitalizationData.isEnrolledInMLTC);
    this.careCoordinationFormGroup.controls.mltcAgencyProgramNameCtrl.setValue(this.medicationsProvidersHospitalizationData.mltcAgencyProgramName);
    this.careCoordinationFormGroup.controls.mltcAgencyProgramAddressCtrl.setValue(this.medicationsProvidersHospitalizationData.mltcProviderContactNameAddress);
    this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.setValue(this.medicationsProvidersHospitalizationData.mltcProviderContactPhoneNo);
    this.careCoordinationFormGroup.controls.healthHomeCtrl.setValue(this.medicationsProvidersHospitalizationData.isEnrolledInHealthHome);
    this.careCoordinationFormGroup.controls.healthHomeAgencyProgNameCtrl.setValue(this.medicationsProvidersHospitalizationData.hhAgencyProgramName);
    this.careCoordinationFormGroup.controls.healthHomeAgencyProgAddressCtrl.setValue(this.medicationsProvidersHospitalizationData.hhAgencyContactNameAddress);
    this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.setValue(this.medicationsProvidersHospitalizationData.hhAgencyContactPhoneNo);
    this.crisisInterventionFormGroup.controls.everTakenForPsychiatricAssessmentCtrl.setValue(this.medicationsProvidersHospitalizationData.everTakenForPsychiatricAssessment);
    this.crisisInterventionFormGroup.controls.recentPhychAssessmentDateCtrl.setValue(this.FormatDatetoCtrl(this.medicationsProvidersHospitalizationData.recentPhychAssessmentDate));
    this.crisisInterventionFormGroup.controls.everTakenToEmergencyServicesCtrl.setValue(this.medicationsProvidersHospitalizationData.everTakenToEmergencyServices);
    this.crisisInterventionFormGroup.controls.recentEmergencyServicesDateCtrl.setValue(this.FormatDatetoCtrl(this.medicationsProvidersHospitalizationData.recentEmergencyServiceDate));

    if(this.medicationsProvidersHospitalizationData.mltcProviderContactPhoneNo != null && this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.value.toString().length != 10)
    {
      this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.setErrors({'Mask error':true});
    }

    if(this.medicationsProvidersHospitalizationData.hhAgencyContactPhoneNo != null && this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.value.toString().length != 10)
    {
      this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.setErrors({'Mask error':true});
    }
  }

  mapFromControls() {

    var medProvHospData: MedicationsProvidersHospitalizationModel = {
      pactApplicationID: this.medicationsProvidersHospitalizationData.pactApplicationID,
      pactMedicationsProvidersHospitalizationID: this.medicationsProvidersHospitalizationData.pactMedicationsProvidersHospitalizationID,
      levelOfSupportType: this.medicationsForm.controls.levelOfSupportCtrl.value,
      isApplicantPsychiatricallyHospitalized: this.hospitalizationsFormGroup.controls.isApplicantPsychiatricallyHospitalizedCtrl.value,
      noOfPsychiatricHospitalizationInPast3Yrs: this.hospitalizationsFormGroup.controls.noOfPsychiatricHospitalizationInPast3YrsCtrl.value,
      recentDischargeDate: this.isValidDate(this.hospitalizationsFormGroup.controls.recentDischargeDateCtrl.value) ? this.hospitalizationsFormGroup.controls.recentDischargeDateCtrl.value : null,
      isCurrentlyHospitalized: this.hospitalizationsFormGroup.controls.isCurrentHospitalizedCtrl.value,
      admissionDate: this.isValidDate(this.hospitalizationsFormGroup.controls.admissionDateCtrl.value) ? this.hospitalizationsFormGroup.controls.admissionDateCtrl.value : null,
      serviceType: this.hospitalizationsFormGroup.controls.serviceTypeCtrl.value,
      hospitalName: this.trimValue(this.hospitalizationsFormGroup.controls.hospitalNameCtrl.value),
      isEnrolledInMLTC: this.careCoordinationFormGroup.controls.mltcPlanCtrl.value,
      mltcAgencyProgramName: this.trimValue(this.careCoordinationFormGroup.controls.mltcAgencyProgramNameCtrl.value),
      mltcProviderContactNameAddress: this.trimValue(this.careCoordinationFormGroup.controls.mltcAgencyProgramAddressCtrl.value),
      mltcProviderContactPhoneNo: this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.value,
      isEnrolledInHealthHome: this.careCoordinationFormGroup.controls.healthHomeCtrl.value,
      hhAgencyProgramName: this.trimValue(this.careCoordinationFormGroup.controls.healthHomeAgencyProgNameCtrl.value),
      hhAgencyContactNameAddress: this.trimValue(this.careCoordinationFormGroup.controls.healthHomeAgencyProgAddressCtrl.value),
      hhAgencyContactPhoneNo: this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.value,
      everTakenForPsychiatricAssessment: this.crisisInterventionFormGroup.controls.everTakenForPsychiatricAssessmentCtrl.value,
      recentPhychAssessmentDate: this.isValidDate(this.crisisInterventionFormGroup.controls.recentPhychAssessmentDateCtrl.value) ? this.crisisInterventionFormGroup.controls.recentPhychAssessmentDateCtrl.value : null,
      everTakenToEmergencyServices: this.crisisInterventionFormGroup.controls.everTakenToEmergencyServicesCtrl.value,
      recentEmergencyServiceDate: this.isValidDate(this.crisisInterventionFormGroup.controls.recentEmergencyServicesDateCtrl.value) ? this.crisisInterventionFormGroup.controls.recentEmergencyServicesDateCtrl.value : null,
      createdBy: this.userData.optionUserId,
      updatedBy: this.userData.optionUserId,
      isActive: true,
      createdDate: new Date(),
      updatedDate: new Date(),
      isCareCoordinationTabComplete: this.isCareCoordinationTabComplete(),
      isCrisisInterventionTabComplete: this.isCrisisInterventionTabComplete(),
      isCurrentMedicationsTabComplete: this.isCurrentMedicationTabComplete(),
      isCurrentTreatmentServiceProvidersTabComplete: this.isCurrentTreatmentServiceProvidersTabComplete(),
      isHospitalizationsTabComplete: this.isHospitalizationsTabComplete(),
      medicationsDetails: null,
      treatmentServiceProvidersDetails: null
    };

    medProvHospData.isComplete = (medProvHospData.isCurrentMedicationsTabComplete && medProvHospData.isCurrentTreatmentServiceProvidersTabComplete && medProvHospData.isHospitalizationsTabComplete && medProvHospData.isCareCoordinationTabComplete && medProvHospData.isCrisisInterventionTabComplete);
    return medProvHospData;
  }

  isCurrentMedicationTabComplete(): boolean {
    if (this.medicationsForm.controls.levelOfSupportCtrl.value) {
      return true;
    }
    return false;
  }

  isCurrentTreatmentServiceProvidersTabComplete(): boolean {
    if (this.isControlValueNotNull(this.medicationsProvidersHospitalizationData.treatmentServiceProvidersDetails) && this.medicationsProvidersHospitalizationData.treatmentServiceProvidersDetails.length > 0) {
      return true;
    }
    return false;
    //return this.isCurrentTreatmentServiceProvidersTouched;
  }

  isHospitalizationsTabComplete(): boolean {
    if (this.hospitalizationsFormGroup.valid) {
      if (this.hospitalizationsFormGroup.controls.isCurrentHospitalizedCtrl.value == this.refGroupYesID) {
        if (this.isControlValueNotNull(this.hospitalizationsFormGroup.controls.admissionDateCtrl.value) &&
          this.isValidDate(this.hospitalizationsFormGroup.controls.admissionDateCtrl.value) &&
          this.isControlValueNotNull(this.hospitalizationsFormGroup.controls.serviceTypeCtrl.value) &&
          this.isControlValueNotNull(this.hospitalizationsFormGroup.controls.hospitalNameCtrl.value)) {
          return true;
        }
        else { return false; }
      }
      else if (this.hospitalizationsFormGroup.controls.isCurrentHospitalizedCtrl.value == this.refGroupNoID) {
        return true;
      }
    }
    return false;
  }

  isCareCoordinationTabComplete(): boolean {
    var isMLTCComplete: boolean = false;
    var isHealthHomeComplete: boolean = false;
    if (this.careCoordinationFormGroup.valid) {
      if (this.isControlValueNotNull(this.careCoordinationFormGroup.controls.mltcPlanCtrl.value)) {
        if (this.careCoordinationFormGroup.controls.mltcPlanCtrl.value == this.refGroupYesID) {
          if (this.isControlValueNotNull(this.careCoordinationFormGroup.controls.mltcAgencyProgramNameCtrl.value) &&
            this.isControlValueNotNull(this.careCoordinationFormGroup.controls.mltcAgencyProgramAddressCtrl.value) &&
            this.isControlValueNotNull(this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.value) &&
            this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.value.toString().length == 10) {
            isMLTCComplete = true;
          }
        }
        else if (this.careCoordinationFormGroup.controls.mltcPlanCtrl.value == this.refGroupNoID) {
          isMLTCComplete = true;
        }
      }
      if (this.isControlValueNotNull(this.careCoordinationFormGroup.controls.healthHomeCtrl.value)) {
        if (this.careCoordinationFormGroup.controls.healthHomeCtrl.value == this.refGroupYesID) {
          if (this.isControlValueNotNull(this.careCoordinationFormGroup.controls.healthHomeAgencyProgNameCtrl.value) &&
            this.isControlValueNotNull(this.careCoordinationFormGroup.controls.healthHomeAgencyProgAddressCtrl.value) &&
            this.isControlValueNotNull(this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.value) &&
            this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.value.toString().length == 10) {
            isHealthHomeComplete = true;
          }
        }
        else if (this.careCoordinationFormGroup.controls.healthHomeCtrl.value == this.refGroupNoID) {
          isHealthHomeComplete = true;
        }
      }
    }
    return (isMLTCComplete && isHealthHomeComplete);
  }

  isCrisisInterventionTabComplete(): boolean {
    var isPsychiatricAssessmentComplete: boolean = false;
    var isEmergencyServicesComplete: boolean = false;
    if (this.crisisInterventionFormGroup.valid) {
      if (this.isControlValueNotNull(this.crisisInterventionFormGroup.controls.everTakenForPsychiatricAssessmentCtrl.value)) {
        if (this.crisisInterventionFormGroup.controls.everTakenForPsychiatricAssessmentCtrl.value == this.refGroupYesID) {
          if (this.isControlValueNotNull(this.crisisInterventionFormGroup.controls.recentPhychAssessmentDateCtrl.value)
            && this.isValidDate(this.crisisInterventionFormGroup.controls.recentPhychAssessmentDateCtrl.value)) {
            isPsychiatricAssessmentComplete = true;
          }
        }
        else if (this.crisisInterventionFormGroup.controls.everTakenForPsychiatricAssessmentCtrl.value == this.refGroupNoID) {
          isPsychiatricAssessmentComplete = true;
        }
      }
      if (this.isControlValueNotNull(this.crisisInterventionFormGroup.controls.everTakenToEmergencyServicesCtrl.value)) {
        if (this.crisisInterventionFormGroup.controls.everTakenToEmergencyServicesCtrl.value == this.refGroupYesID) {
          if (this.isControlValueNotNull(this.crisisInterventionFormGroup.controls.recentEmergencyServicesDateCtrl.value)
            && this.isValidDate(this.crisisInterventionFormGroup.controls.recentEmergencyServicesDateCtrl.value)) {
            isEmergencyServicesComplete = true;
          }
        }
        else if (this.crisisInterventionFormGroup.controls.everTakenToEmergencyServicesCtrl.value == this.refGroupNoID) {
          isEmergencyServicesComplete = true;
        }
      }
    }
    return (isPsychiatricAssessmentComplete && isEmergencyServicesComplete);
  }

  isControlValueNotNull(value: any): boolean {
    if (typeof value === 'string') {
      value = value.trim();
      if (value != null && value != '' && value != '0') {
        return true;
      }
    }
    else if (value != null && value != '' && value != '0') {
      return true;
    }
    return false;
  }
  trimValue(value: any): string {
    if (typeof value === 'string') {
      return value.trim();
    }
  }

  FormatDate(iDate: Date) {
    if (this.isControlValueNotNull(iDate)) {
      var inputDate = new Date(iDate);
      var formattedDate = ('0' + (inputDate.getMonth() + 1)).slice(-2) + '/' + ('0' + inputDate.getDate()).slice(-2) + '/' +
        inputDate.getFullYear();
      return formattedDate;
    }
    return null;
  }

  FormatDatetoCtrl(iDate: Date) {
    if (this.isControlValueNotNull(iDate)) {
      var inputDate = new Date(iDate);
      var formattedDate = inputDate.getFullYear() + '-' + ('0' + (inputDate.getMonth() + 1)).slice(-2) + '-' +
        ('0' + inputDate.getDate()).slice(-2);
      return formattedDate;
    }
    return null;
  }

  minDateCalculate(){
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    return new Date(year-3, month, day);
};


  isValidDate(dateString: Date) {
    var inputDate = this.FormatDate(dateString);
    // First check for the pattern
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(inputDate))
      return false;

    // Parse the date parts to integers
    var parts = inputDate.split("/");
    var day = parseInt(parts[1], 10);
    var month = parseInt(parts[0], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1752 || year > 3000 || month == 0 || month > 12)
      return false;

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
      monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
  };
  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
  }

  getLevelofSupportTooltip() {    
    
    var levelofSupport  = this.medicationsForm.controls.levelOfSupportCtrl.value;
    if(levelofSupport != null && levelofSupport != 0)
    {
      return this.levelOfSupports.find(d=> d.refGroupDetailID === levelofSupport).refGroupDetailDescription.toString();
    }
  }

  //#region validate message

  mltcAgencyProgramPhoneValidate() {
    if (this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.value.toString().length != 10) {
      this.toastr.error("Invalid Phone number. Please enter date 10 digit phone number");
    }
    else
      this.careCoordinationFormGroup.controls.mltcAgencyProgramPhoneCtrl.hasError('Mask error') ? this.toastr.error("Invalid Phone number. Please enter date 10 digit phone number") : "";
  }
  healthHomeAgencyProgPhoneValidate() {
    if (this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.value.toString().length != 10) {
      this.toastr.error("Invalid Phone number. Please enter date 10 digit phone number");
    }
    else
      this.careCoordinationFormGroup.controls.healthHomeAgencyProgPhoneCtrl.hasError('Mask error') ? this.toastr.error("Invalid Phone number. Please enter date 10 digit phone number") : "";
  }

  phoneValidate() {
    this.treatmentServicesFormGroup.controls.phoneCtrl.hasError('Mask error') ? this.toastr.error("Invalid Phone number. Please enter date 10 digit phone number") : "";
  }

  recentEmergencyServicesDateValidate() {
    if (!this.isValidDate(this.crisisInterventionFormGroup.controls.recentEmergencyServicesDateCtrl.value)) {
      this.toastr.error("Recent emergency service date is invalid.")
    }
    this.crisisInterventionFormGroup.controls.recentEmergencyServicesDateCtrl.hasError('matDatepickerMax') ? this.toastr.error("Recent Emergency service date cannot be of future date") : "";
  }

  recentPhychAssessmentDateValidate() {
    if (!this.isValidDate(this.crisisInterventionFormGroup.controls.recentPhychAssessmentDateCtrl.value)) {
      this.toastr.error("Recent psychiatric assessment date is invalid.")
    }
    this.crisisInterventionFormGroup.controls.recentPhychAssessmentDateCtrl.hasError('matDatepickerMax') ? this.toastr.error("Recent psychiatric assessment date cannot be of future date") : "";
  }

  admissionDateValidate() {
    if (!this.isValidDate(this.hospitalizationsFormGroup.controls.admissionDateCtrl.value)) {
      this.toastr.error("Admission date is invalid.")
    }
    this.hospitalizationsFormGroup.controls.admissionDateCtrl.hasError('matDatepickerMax') ? this.toastr.error("Admission date cannot be of future date") : "";
  }

  recentDischargeValidate() {
    if (!this.isValidDate(this.hospitalizationsFormGroup.controls.recentDischargeDateCtrl.value)) {
      this.toastr.error("Recent discharge date is invalid.")
    }
    this.hospitalizationsFormGroup.controls.recentDischargeDateCtrl.hasError('matDatepickerMax') ? this.toastr.error("Recent discharge date cannot be of future date") : "";
    this.hospitalizationsFormGroup.controls.recentDischargeDateCtrl.hasError('matDatepickerMin') ? this.toastr.error("Recent discharge date should be within the past 3 yrs") : "";
  }

  noOfPsychiatricHospitalizationInPast3YrsValidate(){
    this.hospitalizationsFormGroup.controls.noOfPsychiatricHospitalizationInPast3YrsCtrl.hasError('max') ? this.toastr.error("Psychiatric hospitalization in past 3 years must be a number between 1-999"):"";
  }


  //#endregion

  //#endregion
}
