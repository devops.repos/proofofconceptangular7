import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MedicationDetailModel} from './medications-providers-hospitalization.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "medication-action",
  template: ` <mat-icon (click)="onDelete()" matTooltip='Delete Medication' class="doc-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class MedicationActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private medicationSelected : MedicationDetailModel;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onDelete() {
     this.medicationSelected = {
       pactMedicationsDetailsID: this.params.data.pactMedicationsDetailsID, 
       medicationName: this.params.data.medicationName,
       medicationType : this.params.data.medicationType,
       medicationTypeDescription: null,
       isActive:this.params.data.isActive,
       createdBy : this.params.data.createdBy,
       createdDate :this.params.data.createdDate,
       updatedBy:this.params.data.updatedBy,
       updatedDate :this.params.data.updatedDate,
       pactMedicationsProvidersHospitalizationID:this.params.data.pactMedicationsProvidersHospitalizationID       
     };
     // alert(JSON.stringify(this.docSelected));
     // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
     this.params.context.componentParent.medicationDeleteParent(this.medicationSelected);
     // this.c2vService.setClientAwaitingPlacementSelected(this.docSelected);
     // todo
  }

  refresh(): boolean {
    return false;
  }
}
