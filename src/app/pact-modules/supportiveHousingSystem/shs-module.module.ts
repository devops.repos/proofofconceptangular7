

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../shared/material/material.module';
import { ErrorsModule } from '../../shared/errors-handling/errors.module';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { ToastrModule } from 'ngx-toastr';
import { ShsRoutingModule } from './shs-routing.module';
import { ContentBannerModule } from '../../shared/content-banner/content-banner.module';
import { PactCalendarModule } from '../../shared/calendar/pact-calendar.module';
import { DocumentModule } from 'src/app/shared/document/document.module';
import { TextMaskModule } from 'angular2-text-mask';
import { ClientBannerModule } from 'src/app/shared/client-banner/client-banner.module';
import { ConfirmDialogModule } from 'src/app/shared/confirm-dialog/confirm-dialog.module';
import { NgxMaskModule } from 'ngx-mask';
import { NavFooterModule } from 'src/app/shared/nav-footer/nav-footer.module';
import { DiagnosisModule } from 'src/app/shared/diagnosis/diagnosis.module';
import { RecommendedServicesModule } from 'src/app/shared/recommended-services/recommended-services.module';
import { ApplicantPreferencesModule } from 'src/app/shared/applicant-preferences/applicant-preferences.module';
import { ClientDocumentsModule } from 'src/app/shared/client-documents/client-documents.module';
import { ClientDocumentsGridModule } from 'src/app/shared/client-documents-grid/client-documents-grid.module';
import { ClientDocumentsDialogModule } from 'src/app/shared/client-documents-dialog/client-documents-dialog.module';
import { HousingApplicationSupportingDocumentsModule } from 'src/app/shared/housing-application-supporting-documents/housing-application-supporting-documents.module';
import {SharedADLModule} from 'src/app/shared/shared-adl/shared-adl.module';


/*Components Goes Below*/

import { NewAppComponent } from './new-app/new-app.component';
import { PendingAppsComponent } from './pending-apps/pending-apps.component';
import { TransmittedAppsComponent } from './transmitted-apps/transmitted-apps.component';
import { ConsentSearchComponent } from './consent-search/consent-search.component';
import { ConsentFormsComponent } from './consent-search/consent-forms/consent-forms.component';
import { PriorSupportiveHousingApplicationsComponent } from './consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications.component';
import { DemographicsComponent } from './demographics/demographics.component';
import { HousingHomelessComponent } from './housing-homeless/housing-homeless.component';
import { ClinicalAssessmentComponent } from './clinical-assessment/clinical-assessment.component';
import { AdlsComponent } from './adls/adls.component';
import { MedicationsProvidersHospitalizationComponent } from './medications-providers-hospitalization/medications-providers-hospitalization.component';
import { TraumaChildWelfareComponent } from './trauma-child-welfare/trauma-child-welfare.component';
import { SymptomsSubstanceUseComponent } from './symptoms-substance-use/symptoms-substance-use.component';
import { HousingPreferencesComponent } from './housing-preferences/housing-preferences.component';
import { PsychiatricPsychosocialMhrComponent } from './psychiatric-psychosocial-mhr/psychiatric-psychosocial-mhr.component';
import { DocumentsComponent } from './documents/documents.component';
import { SummaryTransmitComponent } from './summary-transmit/summary-transmit.component';
import { IsCompleteStatusCheckComponent } from './is-complete-status-check/is-complete-status-check.component';
import { ContactsActionComponent } from './demographics/demographic-contacts-action.component';
import { PriorAppContactsActionComponent } from './demographics/demographic-priorapp-contacts-action.component';
import { FamilyCompositionActionComponent } from './demographics/demographic-familycomposition-action.component';
import { PriorAppFamilyCompositionActionComponent } from './demographics/demographic-priorapp-familycomposition-action.component';
import { MhrComponent } from './psychiatric-psychosocial-mhr/mhr/mhr.component';
import { PsychiatricComponent } from './psychiatric-psychosocial-mhr/psychiatric/psychiatric.component';
import { PsychosocialComponent } from './psychiatric-psychosocial-mhr/psychosocial/psychosocial.component';
import { PendingAppComponent } from './pending-app/pending-app.component';
import { MedicationActionComponent} from './medications-providers-hospitalization/medication-action.component';
import { TreatmentServiceActionComponent} from './medications-providers-hospitalization/treatmentService-action.component';
import { PendingAppsActionComponent } from './pending-apps/pending-apps-action.component';
import { TransmittedAppsActionComponent } from './transmitted-apps/transmitted-apps-action.component';
import { PriorSupportiveHousingApplicationsActionComponent } from './consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications-action.component';
import { BoroughPreferencesComponent } from './housing-preferences/borough-preferences/borough-preferences.component';
import { ApartmentPreferencesComponent } from './housing-preferences/apartment-preferences/apartment-preferences.component';
import { HousingLevelComponent } from './housing-preferences/housing-level/housing-level.component';
import { ReportViewerComponent } from './psychiatric-psychosocial-mhr/reportViewer/reportViewer.component';
import { HomelessHistoryModule } from './../../shared/homeless-history/homeless-history.module';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { PriorSupportiveHousingApplicationsDialogComponent } from './consent-search/prior-supportive-housing-applications/prior-supportive-housing-applications-dialog.component';
import { TransmittedApplicationsDialogComponent } from './transmitted-apps/transmitted-apps-dialog.component';
import { TransmissionStatusComponent } from './summary-transmit/transmission-status.component';
import { ReferralHistoryGridModule } from 'src/app/shared/referral-history-grid/referral-history.module';
import { TutorialModule } from 'src/app/shared/tutorial/tutorial.module';
import { DhsHasaPendingAppsComponent } from './dhs-hasa-pending-apps/dhs-hasa-pending-apps.component';

@NgModule({
  declarations: [
    NewAppComponent,
    PendingAppsComponent,
    TransmittedAppsComponent,
    ConsentSearchComponent,
    ConsentFormsComponent,
    PriorSupportiveHousingApplicationsDialogComponent,
    PriorSupportiveHousingApplicationsComponent,
    DemographicsComponent,
    HousingHomelessComponent,
    ClinicalAssessmentComponent,
    AdlsComponent,
    MedicationsProvidersHospitalizationComponent,
    TraumaChildWelfareComponent,
    SymptomsSubstanceUseComponent,
    HousingPreferencesComponent,
    PsychiatricPsychosocialMhrComponent,
    DocumentsComponent,
    SummaryTransmitComponent,
    IsCompleteStatusCheckComponent,
    ContactsActionComponent,
    PriorAppContactsActionComponent,
    FamilyCompositionActionComponent,
    PriorAppFamilyCompositionActionComponent,
    MhrComponent,
    PsychiatricComponent,
    PsychosocialComponent,
    PendingAppComponent,
    MedicationActionComponent,
    TreatmentServiceActionComponent,
    PendingAppsActionComponent,
    TransmittedApplicationsDialogComponent,
    TransmittedAppsActionComponent,
    PriorSupportiveHousingApplicationsActionComponent,
    BoroughPreferencesComponent,
    ApartmentPreferencesComponent,
    HousingLevelComponent,
    ReportViewerComponent,
    TransmissionStatusComponent,
    DhsHasaPendingAppsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ContentBannerModule,
    ClientBannerModule,
    ClientDocumentsModule,
    ClientDocumentsGridModule,
    ClientDocumentsDialogModule,
    ReferralHistoryGridModule,
    HousingApplicationSupportingDocumentsModule,
    DocumentModule,
    PactCalendarModule,
    ShsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    MaterialModule,
    ErrorsModule,
    NgxHmCarouselModule,
    TextMaskModule,
    ConfirmDialogModule,
    NavFooterModule,
    DiagnosisModule,
    RecommendedServicesModule, 
    ApplicantPreferencesModule,
    HomelessHistoryModule,
    SharedADLModule,
    DirectivesModule,
    TutorialModule,
    ToastrModule.forRoot({
      progressBar: true,
      progressAnimation: 'increasing'
    }),
    AgGridModule.withComponents([ContactsActionComponent, PriorAppContactsActionComponent
        , FamilyCompositionActionComponent, PriorAppFamilyCompositionActionComponent, MedicationActionComponent,
        TreatmentServiceActionComponent,
        PendingAppsActionComponent, TransmittedAppsActionComponent,
        PriorSupportiveHousingApplicationsActionComponent
      ]),
    NgxMaskModule.forRoot()
  ],
  entryComponents: [ConsentFormsComponent, TransmittedApplicationsDialogComponent, PriorSupportiveHousingApplicationsComponent, ReportViewerComponent, PriorSupportiveHousingApplicationsDialogComponent, TransmissionStatusComponent ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShsModule { }
