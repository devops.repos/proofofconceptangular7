import { Component, OnInit, ViewEncapsulation, ViewChild, EventEmitter, Output, Input, HostListener, OnDestroy } from '@angular/core';
import { MasterDropdown } from 'src/app/models/masterDropdown.module';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DatePipe, DecimalPipe } from '@angular/common';
import {
  PACTDemographicsData, PACTDemographicsContacts, PACTDemographicFamilyComposition,
  PACTDemographicsFinancialBenefits
} from './demographics.model';
import { DemographicsService } from './demographic.service';
import * as moment from 'moment';
import { CommonService } from '../../../services/helper-services/common.service';
import { RefGroupDetails } from '../../../models/refGroupDetailsDropDown.model';
import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { GridOptions } from 'ag-grid-community';
import { DocActionComponent } from 'src/app/shared/document/doc-action.component';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { Subscription } from 'rxjs';
import { AuthData } from 'src/app/models/auth-data.model';
//  import { UserService } from 'src/app/services/helper-services/user.service';
import { ContactsActionComponent } from './demographic-contacts-action.component';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { parse } from 'date-fns';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { ToastrService } from 'ngx-toastr';
import { HttpEventType } from '@angular/common/http';
import { PriorAppContactsActionComponent } from './demographic-priorapp-contacts-action.component';
import { MatTabChangeEvent } from '@angular/material';
import { FamilyCompositionActionComponent } from './demographic-familycomposition-action.component';
import { PriorAppFamilyCompositionActionComponent } from './demographic-priorapp-familycomposition-action.component';
import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { ReturnStatement } from '@angular/compiler';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';


@Component({
  selector: 'app-demographics',
  templateUrl: './demographics.component.html',
  styleUrls: ['./demographics.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class DemographicsComponent implements OnInit, OnDestroy {

  //#region varibles declaration


  //  Tab Status : Pending = 0 ; Complete = 1; Blank = 2;
  demographicDataTabStatus = 2;
  financialBenefitsTabStatus = 2;
  contactsTabStatus = 2;
  familyCompositionTabStatus = 2;
  medicalDocumentsTabStatus = 2;
  displayContactTab = 1; // If value : 0 meaning Hide the Tab and If value : 1 meaning Show the Tab
  displayFamilyTab = 0; // If value : 0 meaning Hide the Tab and If value : 1 meaning Show the Tab
  applicationType = 46; // Whether this application Type is Individual : 46 or Family : 47

  YearlyTotalDisplay: any;
  amountValue: number;
  frequencyValue: number;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  //  userData: AuthData;
  //  userDataSub: Subscription;
  demoGraphicGroup: FormGroup;
  financialBenifitGroup: FormGroup;
  demographicsContactsGroup: FormGroup;
  demographicsFamilyCompositionGroup: FormGroup;

  pactDemographicsData: PACTDemographicsData = {
    pactDemographicID: null, pactApplicationID: null, firstName: null, lastName: null, akaFirstName: null,
    akaLastName: null, ssn: null, dob: null, age: null, genderType: null, cin: null, address: null, aptNo: null, city: null, state: null,
    zip: null, boroughType: null, maritalStatusType: null, wasEmployedIn2Yrs: null, noOfMonthsEmployedIn2Yrs: null, ethnicityType: null,
    primaryLanguageType: null, veteranType: null, dischargeType: null, educationType: null, isPregnant: null, expectedDeliveryDate: null,
    isDemographicChanged: null, isComplete: null, isDemographicDataTabComplete: null, isFinancialBenefitsTabComplete: null,
    isContactsTabComplete: null, isFamilyCompositionTabComplete: null, isMedicalDocumentsTabComplete: null, isActive: null,
    createdBy: null, createdDate: null, updatedBy: null, updatedDate: null
  };

  pactDemographicsFinancialBenefits: PACTDemographicsFinancialBenefits = {
    pactDemographicFinancialBenefitsID: null, pactDemographicID: null, employmentSalaryType: null,
    employmentSalaryAmount: null, employmentSalaryFrequencyType: null, publicAssistanceType: null, publicAssistanceAmount: null,
    publicAssistanceFrequencyType: null, ssdType: null, ssdAmount: null, ssdFrequencyType: null, veteransGIBillType: null,
    veteransGIBillAmount: null, veteransGIBillFrequencyType: null, veteransServiceType: null, veteransServiceAmount: null,
    veteransServiceFrequencyType: null, socialSecurityType: null, socialSecurityAmount: null, socialSecurityFrequencyType: null,
    pensionRetirementType: null, pensionRetirementAmount: null, pensionRetirementFrequencyType: null, unemploymentType: null,
    unemploymentAMount: null, unemploymentFrequencyType: null, medicaidType: null, medicaidID: null, medicareType: null, snapType: null,
    hivServiceType: null, hivServiceAmount: null, hivServiceFrequencyType: null, otherCompensationType: null, otherCompensationAmount: null,
    otherCompensationFrequencyType: null, isActive: null, createdBy: null, createdDate: null, updatedBy: null, updatedDate: null
  };

  pactDemographicsContacts: PACTDemographicsContacts = {
    pactDemographicContactsID: null, pactDemographicID: null, firstName: null, lastName: null, relationshipType: null,
    refGroupDetailID: null, description: null, phone: null, alternatePhone: null, isActive: null,
    createdBy: null, createdDate: null, updatedBy: null, updatedDate: null
  };

  pactDemographicFamilyComposition: PACTDemographicFamilyComposition = {
    pactDemographicFamilyCompositionID: null, pactDemographicID: null, firstName: null, lastName: null, relationshipType: null,
    refGroupDetailID: null, description: null, dob: null, age: null, comments: null, isActive: null, createdBy: null,
    createdDate: null, updatedBy: null, updatedDate: null
  };

  applicationCreatedDate = new Date();
  //  Min-Max Date for Date Picker
  now = new Date();
  minDOB = new Date(1900, 1, 1);
  currentDate = this.datePipe.transform(new Date(), 'MM/dd/yyyy hh:mm:ss a');
  minDate =  new Date(this.now.getFullYear(), this.now.getMonth()-2, this.now.getDay());
  //minDate = new Date(Math.abs(this.now.getFullYear() - 119), this.now.getMonth(), this.now.getDay());
  maxDate = new Date(this.now.getFullYear()+1, this.now.getMonth()+1, this.now.getDay());

  routeSub: any;
  //  Masking SSN
  public mask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  };

  wasEmployedIn2YrsSelectedValue: string;
  IsVateranSelectedValue: string;
  IsPregnantSelectedValue: string;
  genderType: RefGroupDetails[];
  boroughType: RefGroupDetails[];
  maritalStatusType: RefGroupDetails[];
  wasEmployedIn2Yrs: RefGroupDetails[];
  ethnicityType: RefGroupDetails[];
  primaryLanguageType: RefGroupDetails[];
  veteranType: RefGroupDetails[];
  dischargeType: RefGroupDetails[];
  educationType: RefGroupDetails[];
  relationshipType: RefGroupDetails[];

  contactRelationshipType: RefGroupDetails[];


  empSalaryTypeSelected: string;
  publicAssistanceTypeSelected: string;
  ssdTypeSelected: string;
  veteransGIBillTypeSelected: string;
  veteransServicesTypeSelected: string;
  socialSecurityTypeSelected: string;
  pensionTypeSelected: string;
  unemploymentTypeSelected: string;
  medicaidTypeSelected: string;
  medicareTypeSelected: string;
  snapTypeSelected: string;
  hivServicesTypeSelected: string;
  otherCompTypeSelected: string;

  empSalaryType: RefGroupDetails[];
  empFrequencyType: RefGroupDetails[];
  publicAssistanceType: RefGroupDetails[];
  publicFrequencyType: RefGroupDetails[];
  ssdType: RefGroupDetails[];
  ssdFrequencyType: RefGroupDetails[];
  veteransGIBillType: RefGroupDetails[];
  veteransGIBillFrequencyType: RefGroupDetails[];
  veteransServicesType: RefGroupDetails[];
  veteransServicesFrequencyType: RefGroupDetails[];
  socialSecurityType: RefGroupDetails[];
  socialSecurityFrequencyType: RefGroupDetails[];
  pensionType: RefGroupDetails[];
  pensionFrequencyType: RefGroupDetails[];
  unemploymentType: RefGroupDetails[];
  unemploymentFrequencyType: RefGroupDetails[];
  medicaidType: RefGroupDetails[];
  medicaidFrequencyType: RefGroupDetails[];
  medicareType: RefGroupDetails[];
  medicareFrequencyType: RefGroupDetails[];
  snapType: RefGroupDetails[];
  snapFrequencyType: RefGroupDetails[];
  hivServicesType: RefGroupDetails[];
  hivServicesFrequencyType: RefGroupDetails[];
  otherCompType: RefGroupDetails[];
  otherCompFrequencyType: RefGroupDetails[];

  contactDataObj: PACTDemographicsContacts[];
  priorAppContactDataObj: PACTDemographicsContacts[];
  familyCompositionDataObj: PACTDemographicFamilyComposition[];
  priorAppFamilyCompositionDataObj: PACTDemographicFamilyComposition[];

  contactDataCount = 0;

  gridApi: any;
  gridColumnApi: any;
  defaultColDef: any;
  pagination: any;
  rowSelection: any;
  context: any;

  columnDefs: any;
  frameworkComponents: any;
  frameworkComponentsPriorAppContact: any;

  columnDefsFamily: any;
  frameworkComponentsFamily: any;
  frameworkComponentsPriorAppFamily: any;

  public gridOptions: GridOptions;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @Input() preSelectedRelationshipType: number;
  @Output() uploadFinished = new EventEmitter();

  selectedApplicationID = 0;
  PactDemographID: string;
  message: string;
  selectedTab = 0;
  familyMemberGridCount: number = 0;

  applicationID: number;
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  //#endregion

  constructor(
    private sidenavService: NavService,
    private router: Router,
    private datePipe: DatePipe,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    // private userService: UserService,
    private commonService: CommonService,
    private demographicsService: DemographicsService,
    private confirmDialogService: ConfirmDialogService,
    private sidenavStatusService: SidenavStatusService,
    private clientApplicationService: ClientApplicationService,
  ) {

    //  route.paramMap.subscribe(params => {
    //    if (params.get("applicationID") != null) {
    //      this.selectedApplicationID = parseInt(params.get("applicationID"));
    //    }
    //  })

    //  this.clientApplicationService.getPactApplicationID().subscribe(appID => {
    //    this.selectedApplicationID = appID;
    //  });

    this.gridOptions = {
      rowHeight: 35
    } as GridOptions;

    this.rowSelection = 'multiple';
    this.pagination = true;
    this.context = { componentParent: this };
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };

    // #region Demographics Important Contacts Tab - Grid variable

    this.columnDefs = [
      {
        headerName: 'DemographicsContactID',
        field: 'pactDemographicContactsID',
        hide: true
      },
      {
        headerName: 'DemographicsID',
        field: 'pactDemographicID',
        hide: true
      },
      {
        headerName: 'refGroupDetailID',
        field: 'refGroupDetailID',
        hide: true
      },
      {
        headerName: 'RelationshipType',
        field: 'relationshipType',
        hide: true
      },
      {
        headerName: 'First Name',
        field: 'firstName',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Last Name',
        field: 'lastName',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Relationship Type',
        field: 'description',
        width: 150,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Phone',
        field: 'phone',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Alternate Phone',
        field: 'alternatePhone',
        width: 100,
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Actions',
        field: 'action',
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];

    this.frameworkComponents = {
      actionRenderer: ContactsActionComponent
    };
    this.frameworkComponentsPriorAppContact = {
      actionRenderer: PriorAppContactsActionComponent
    };

    // #endregion

    // #region Demographics Family Composition Tab - Grid variable

    this.columnDefsFamily = [
      {
        headerName: 'DemographicFamilyCompositionID',
        field: 'pactDemographicFamilyCompositionID',
        hide: true
      },
      {
        headerName: 'DemographicsID',
        field: 'pactDemographicID',
        hide: true
      },
      {
        headerName: 'refGroupDetailID',
        field: 'refGroupDetailID',
        hide: true
      },
      {
        headerName: 'RelationshipType',
        field: 'relationshipType',
        hide: true
      },
      {
        headerName: 'First Name',
        field: 'firstName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Last Name',
        field: 'lastName',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Relationship Type',
        field: 'description',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Date Of Birth',
        field: 'dob',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Age',
        field: 'age',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Comments',
        field: 'comments',
        filter: 'agTextColumnFilter'
      },
      {
        headerName: 'Actions',
        field: 'action',
        width: 80,
        filter: false,
        sortable: false,
        resizable: false,
        pinned: 'left',
        suppressMenu: true,
        suppressSizeToFit: true,
        cellRenderer: 'actionRenderer',
      }
    ];

    this.frameworkComponentsFamily = {
      actionRenderer: FamilyCompositionActionComponent
    };
    this.frameworkComponentsPriorAppFamily = {
      actionRenderer: PriorAppFamilyCompositionActionComponent
    };

    // #endregion
  }

  //  Save Data on page refresh
  @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
    this.saveAllDemographicsTabs();
  }


  ngOnInit() {

    /** To Load the Sidenav Completion Status for all the Application related Pages */
    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.applicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
          /** If you have any logic that requires applicationID and if the Logic should be implemented in ngOnInit() (on component load). DO IT HERE */
          /** async logic that require applicationID, DO IT HERE */
          if (this.applicationID) {
            this.getDemographicData();
          }
        }
      }
    });

    this.clientApplicationService.getPactApplicationID().subscribe(appID => {
      this.applicationID = appID;
      //  Get Demographics Tab Data on Page Load
      if (this.applicationID) {
        this.getDemographicData();
      }
    });

    this.clientApplicationService.getClientApplicationData().subscribe(app => {
      if (app) {
        // console.log("app.applicationType : ", app.applicationType);
        if (app.applicationType) {
          this.applicationType = app.applicationType;
        }
        if (app.applicationCreatedDate) {
          this.applicationCreatedDate = new Date(app.applicationCreatedDate);
        }
      }
    })

    // this.clientApplicationService.getClientApplicationData().subscribe(appType => {
    //   if (appType) {
    //     this.applicationType = appType.applicationType;
    //     this.applicationID = this.selectedApplicationID;
    //     // console.log(" Application Type Inside: " + this.applicationType);
    //     // this.displayHideContactFamilyTab(appType.applicationType);
    //     this.getDemographicData();
    //   }
    // });

    //this.sidenavStatusService._sidenavStatusReload(this.route);


    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.saveAllDemographicsTabs_popstate(event);
      };
    });


    // #region Form Group Declaration

    this.demoGraphicGroup = this.formBuilder.group({
      akaLastNameCtrl: [''],
      akaFirstNameCtrl: [''],
      dobCtrl: [''],
      ageCtrl: [''],
      ssnCtrl: [''],
      addressCtrl: [''],
      aptNoCtrl: [''],
      cityCtrl: [''],
      stateCtrl: [''],
      zipCtrl: [''],
      genderTypeCtrl: [''],
      boroughTypeCtrl: [0],
      maritalStatusTypeCtrl: [0],
      wasEmployedIn2YrsCtrl: [0],
      noOfMonthsEmployedIn2YrsCtrl: [''],
      ethnicityTypeCtrl: [0],
      primaryLanguageTypeCtrl: [0],
      veteranTypeCtrl: [0],
      dischargeTypeCtrl: [0],
      educationTypeCtrl: [0],
    });

    this.demographicsContactsGroup = this.formBuilder.group({
      hdnDemographicsContactIDCtrl: [0],
      hdnDemographicsIDCtrl: [0],
      firstNameCtrl: [''],
      lastNameCtrl: [''],
      relationshipTypeCtrl: [0],
      phoneCtrl: [''],
      alternatePhoneCtrl: [''],
    });

    this.demographicsFamilyCompositionGroup = this.formBuilder.group({
      hdnDemographicFamilyCompositionIDCtrl: [0],
      hdnDemographicsIDCtrl: [0],
      firstNameCtrl: [''],
      lastNameCtrl: [''],
      relationshipTypeCtrl: [0],
      dobCtrl: [''],
      ageCtrl: [''],
      commentsCtrl: [''],
      isPregnantCtrl: [''],
      pregnancyDateCtrl: ['']
    });

    this.financialBenifitGroup = this.formBuilder.group({
      hdnDemographicFinancialBenefitsIDCtrl: [''],

      empSalaryTypeCtrl: [0],
      empFrequencyTypeCtrl: [0],
      empAmoutCtrl: [''],
      empYearlyTotalCtrl: [''],

      publicAssistanceTypeCtrl: [0],
      publicFrequencyTypeCtrl: [0],
      publicAmoutCtrl: [''],
      publicYearlyTotalCtrl: [''],

      ssdTypeCtrl: [0],
      ssdFrequencyTypeCtrl: [0],
      ssdAmoutCtrl: [''],
      ssdYearlyTotalCtrl: [''],

      veteransGIBillTypeCtrl: [0],
      veteransGIBillFrequencyTypeCtrl: [0],
      veteransGIBillAmoutCtrl: [''],
      veteransGIBillYearlyTotalCtrl: [''],

      veteransServicesTypeCtrl: [0],
      veteransServicesFrequencyTypeCtrl: [0],
      veteransServicesAmoutCtrl: [''],
      veteransServicesYearlyTotalCtrl: [''],

      socialSecurityTypeCtrl: [0],
      socialSecurityFrequencyTypeCtrl: [0],
      socialSecurityAmoutCtrl: [''],
      socialSecurityYearlyTotalCtrl: [''],

      pensionTypeCtrl: [0],
      pensionFrequencyTypeCtrl: [0],
      pensionAmoutCtrl: [''],
      pensionYearlyTotalCtrl: [''],

      unemploymentTypeCtrl: [0],
      unemploymentFrequencyTypeCtrl: [0],
      unemploymentAmoutCtrl: [''],
      unemploymentYearlyTotalCtrl: [''],

      medicaidTypeCtrl: [0],
      medicaidIdCtrl: [''],
      //  medicaidFrequencyTypeCtrl: [''],
      //  medicaidAmoutCtrl: [''],
      //  medicaidYearlyTotalCtrl: [''],

      medicareTypeCtrl: [0],
      //  medicareFrequencyTypeCtrl: [''],
      //  medicareAmoutCtrl: [''],
      //  medicareYearlyTotalCtrl: [''],

      snapTypeCtrl: [0],
      snapFrequencyTypeCtrl: [0],
      snapAmoutCtrl: [''],
      snapYearlyTotalCtrl: [''],

      hivServicesTypeCtrl: [0],
      hivServicesFrequencyTypeCtrl: [0],
      hivServicesAmoutCtrl: [''],
      hivServicesYearlyTotalCtrl: [''],

      otherCompTypeCtrl: [0],
      otherCompFrequencyTypeCtrl: [0],
      otherCompAmoutCtrl: [''],
      otherCompYearlyTotalCtrl: ['']

    });

    // #endregion

    // #region Dropdown values Assignment

    //  4	  Gender
    //  5	  Marital Status
    //  6	  Borough
    //  7	  Response Item
    //  8 	Ethnicity
    //  18	Language
    //  19	Veteran Discharge
    //  20	Education
    //  21  Frequency
    //  22	Relationship
    //  28	Financial Benefit Tab Questions

    const value = ' 4,5,6,7,8,13,18,19,20,21,22 ';

    this.commonService.getRefGroupDetails(value)
      .subscribe(
        res => {
          if (res.body) {

            const data = res.body as RefGroupDetails[];
            this.genderType = data.filter(d => d.refGroupID === 4);
            this.maritalStatusType = data.filter(d => d.refGroupID === 5);
            this.boroughType = data.filter(d => d.refGroupID === 6 && (d.refGroupDetailID === 27 || d.refGroupDetailID === 28 || d.refGroupDetailID === 29
              || d.refGroupDetailID === 30 || d.refGroupDetailID === 31 || d.refGroupDetailID === 32));

            this.wasEmployedIn2Yrs = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34 || d.refGroupDetailID === 35));

            this.ethnicityType = data.filter(d => d.refGroupID === 8);
            this.primaryLanguageType = data.filter(d => d.refGroupID === 18);
            this.veteranType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34 || d.refGroupDetailID === 35));

            this.dischargeType = data.filter(d => d.refGroupID === 19);
            this.educationType = data.filter(d => d.refGroupID === 20);
            this.relationshipType = data.filter(d => d.refGroupID === 22);
            this.contactRelationshipType = data.filter(d => d.refGroupID === 13);

            this.empSalaryType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.empSalaryType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.empFrequencyType = data.filter(d => d.refGroupID === 21);

            this.publicAssistanceType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34 || d.refGroupDetailID === 272));
            this.publicFrequencyType = data.filter(d => d.refGroupID === 21);

            this.ssdType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34
              || d.refGroupDetailID === 272));
            this.ssdFrequencyType = data.filter(d => d.refGroupID === 21);

            this.veteransGIBillType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34
              || d.refGroupDetailID === 272));
            this.veteransGIBillFrequencyType = data.filter(d => d.refGroupID === 21);

            this.veteransServicesType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34
              || d.refGroupDetailID === 272));
            this.veteransServicesFrequencyType = data.filter(d => d.refGroupID === 21);

            this.socialSecurityType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.socialSecurityFrequencyType = data.filter(d => d.refGroupID === 21);

            this.pensionType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.pensionFrequencyType = data.filter(d => d.refGroupID === 21);

            this.unemploymentType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.unemploymentFrequencyType = data.filter(d => d.refGroupID === 21);

            this.medicaidType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.medicaidFrequencyType = data.filter(d => d.refGroupID === 21);

            this.medicareType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.medicareFrequencyType = data.filter(d => d.refGroupID === 21);

            this.snapType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.snapFrequencyType = data.filter(d => d.refGroupID === 21);

            this.hivServicesType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.hivServicesFrequencyType = data.filter(d => d.refGroupID === 21);

            this.otherCompType = data.filter(d => d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34));
            this.otherCompFrequencyType = data.filter(d => d.refGroupID === 21);
          }
        },
        error => console.error('Error in retrieving the data for dropdowns...!', error)
      );
    // #endregion

  }

  // #region Demographics Data Tab

  getDemographicData() {

    this.demographicsService.getPACTDemographicsDataByApplicationID<PACTDemographicsData>(this.applicationID).subscribe(
      res => {
        //if (res.body) {
        if (res) {

          // console.log(res);
          // console.log(res[0]);
          // const value = res.body;
          // this.pactDemographicsData = value as PACTDemographicsData;
          this.pactDemographicsData = res as PACTDemographicsData;
          this.PactDemographID = this.pactDemographicsData.pactDemographicID.toString();
          // console.log('pactDemographicId :' + this.PactDemographID);
          // console.log(JSON.stringify(this.pactDemographicsData));

          if (this.pactDemographicsData.akaFirstName != null) {
            this.demoGraphicGroup.get('akaFirstNameCtrl').setValue(this.pactDemographicsData.akaFirstName);
          }
          if (this.pactDemographicsData.akaLastName != null) {
            this.demoGraphicGroup.get('akaLastNameCtrl').setValue(this.pactDemographicsData.akaLastName);
          }
          if (this.pactDemographicsData.ssn != null) {
            this.demoGraphicGroup.get('ssnCtrl').setValue(this.pactDemographicsData.ssn);
          }
          if (this.pactDemographicsData.genderType != null) {
            this.demoGraphicGroup.get('genderTypeCtrl').setValue(this.pactDemographicsData.genderType);
          }
          if (this.pactDemographicsData.dob != null) {
            this.datePipe.transform(this.demoGraphicGroup.get('dobCtrl').setValue(new Date(this.pactDemographicsData.dob)));
          }
          if (this.pactDemographicsData.age != null) {
            this.demoGraphicGroup.get('ageCtrl').setValue(this.pactDemographicsData.age);
          }
          if (this.pactDemographicsData.address != null) {
            this.demoGraphicGroup.get('addressCtrl').setValue(this.pactDemographicsData.address);
          }
          if (this.pactDemographicsData.aptNo != null) {
            this.demoGraphicGroup.get('aptNoCtrl').setValue(this.pactDemographicsData.aptNo);
          }
          if (this.pactDemographicsData.city != null) {
            this.demoGraphicGroup.get('cityCtrl').setValue(this.pactDemographicsData.city);
          }
          if (this.pactDemographicsData.state != null) {
            this.demoGraphicGroup.get('stateCtrl').setValue(this.pactDemographicsData.state);
          }
          if (this.pactDemographicsData.zip != null) {
            this.demoGraphicGroup.get('zipCtrl').setValue(this.pactDemographicsData.zip);
          }
          if (this.pactDemographicsData.boroughType != null) {
            this.demoGraphicGroup.get('boroughTypeCtrl').setValue(this.pactDemographicsData.boroughType);
          }
          if (this.pactDemographicsData.maritalStatusType != null) {
            this.demoGraphicGroup.get('maritalStatusTypeCtrl').setValue(this.pactDemographicsData.maritalStatusType);
          }
          if (this.pactDemographicsData.wasEmployedIn2Yrs != null) {
            this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').setValue(this.pactDemographicsData.wasEmployedIn2Yrs);
            this.wasEmployedIn2YrsSelectedValue = this.pactDemographicsData.wasEmployedIn2Yrs.toString();
            // console.log("Last 2 year Emp : " + this.wasEmployedIn2YrsSelectedValue)
          }
          if (this.pactDemographicsData.noOfMonthsEmployedIn2Yrs != null) {
            this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').setValue(this.pactDemographicsData.noOfMonthsEmployedIn2Yrs);
          }
          if (this.pactDemographicsData.ethnicityType != null) {
            this.demoGraphicGroup.get('ethnicityTypeCtrl').setValue(this.pactDemographicsData.ethnicityType);
          }
          if (this.pactDemographicsData.primaryLanguageType != null) {
            this.demoGraphicGroup.get('primaryLanguageTypeCtrl').setValue(this.pactDemographicsData.primaryLanguageType);
          }
          if (this.pactDemographicsData.veteranType != null) {
            this.demoGraphicGroup.get('veteranTypeCtrl').setValue(this.pactDemographicsData.veteranType);
            this.IsVateranSelectedValue = this.pactDemographicsData.veteranType.toString();
            // console.log(" is Vatern : " + this.IsVateranSelectedValue);
          }
          if (this.pactDemographicsData.dischargeType != null) {
            this.demoGraphicGroup.get('dischargeTypeCtrl').setValue(this.pactDemographicsData.dischargeType);
          }
          if (this.pactDemographicsData.educationType != null) {
            this.demoGraphicGroup.get('educationTypeCtrl').setValue(this.pactDemographicsData.educationType);
          }
          //console.log('this.pactDemographicsData - ', this.pactDemographicsData);
          if (this.pactDemographicsData.isPregnant != null) {
            if (this.pactDemographicsData.isPregnant === true) {
              this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').setValue('1');
              this.IsPregnantSelectedValue = '1';
            } else {
              this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').setValue('0');
              this.IsPregnantSelectedValue = '0';
            }
          }
          if (this.pactDemographicsData.expectedDeliveryDate != null) {
            this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl')
              .setValue(new Date(this.pactDemographicsData.expectedDeliveryDate)));
          }
          if (this.pactDemographicsData.isDemographicDataTabComplete != null) {
            this.demographicDataTabStatus = this.pactDemographicsData.isDemographicDataTabComplete == true ? 1 : 0;
          }
          if (this.pactDemographicsData.isFinancialBenefitsTabComplete != null) {
            this.financialBenefitsTabStatus = this.pactDemographicsData.isFinancialBenefitsTabComplete == true ? 1 : 0;
          }
          //console.log("this.pactDemographicsData.isFinancialBenefitsTabComplete : ", this.pactDemographicsData.isFinancialBenefitsTabComplete);
          //console.log("this.financialBenefitsTabStatus : ", this.financialBenefitsTabStatus);
          // if (this.pactDemographicsData.isContactsTabComplete != null) {
          //   this.contactsTabStatus = this.pactDemographicsData.isContactsTabComplete == true ? 1 : 0;
          // } // Keep this tab Completed as a default all the time
          this.contactsTabStatus =  1;
          if (this.pactDemographicsData.isFamilyCompositionTabComplete != null) {
            this.familyCompositionTabStatus = this.pactDemographicsData.isFamilyCompositionTabComplete == true ? 1 : 0;
          }

          //  Get Finanical Benifits Tab Data on Page Load
          this.getFinancialBenifits(this.PactDemographID);

          //get the Family Compotion total count
          this.RetrivetheFamilyCompositionByDemographicsID(this.PactDemographID);

        } else {
          // console.log('DemographicData not found');
        }

      },
      error => console.error('Error in retrieving the data on Demographics Tab on page load...!', error)
    );

  }

  setValidatorsForDemographicsDataTab = () => {

    this.demoGraphicGroup.controls.dobCtrl.setValidators([Validators.required]);
    this.demoGraphicGroup.controls.ssnCtrl.setValidators([Validators.required]);
    this.demoGraphicGroup.controls.genderTypeCtrl.setValidators([Validators.required]);
    this.demoGraphicGroup.controls.maritalStatusTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired]);
    this.demoGraphicGroup.controls.ethnicityTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired]);
    this.demoGraphicGroup.controls.primaryLanguageTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired]);
    this.demoGraphicGroup.controls.veteranTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired]);
    this.demoGraphicGroup.controls.educationTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired]);

    this.demoGraphicGroup.controls.dobCtrl.updateValueAndValidity();
    this.demoGraphicGroup.controls.ssnCtrl.updateValueAndValidity();
    this.demoGraphicGroup.controls.genderTypeCtrl.updateValueAndValidity();
    this.demoGraphicGroup.controls.maritalStatusTypeCtrl.updateValueAndValidity();
    this.demoGraphicGroup.controls.ethnicityTypeCtrl.updateValueAndValidity();
    this.demoGraphicGroup.controls.primaryLanguageTypeCtrl.updateValueAndValidity();
    this.demoGraphicGroup.controls.veteranTypeCtrl.updateValueAndValidity();
    this.demoGraphicGroup.controls.educationTypeCtrl.updateValueAndValidity();

  }

  saveDemographicData() {
    // this.setValidatorsForDemographicsDataTab();

    //  if (this.demoGraphicGroup.invalid) {
    //    return;
    //  }

    // if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != "") {
    //   this.pactDemographicsData.expectedDeliveryDate = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value, 'MM/dd/yyyy');
    //   if (new Date(this.pactDemographicsData.expectedDeliveryDate) < new Date()) {
    //     return;
    //   }
    // }

    //console.log("this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value", this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value);
    if (this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value == 33 && (this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value != ""
      || this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value != null)) {
      if (this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value == 0) {
        this.message = 'Total number of months employed must be between 1 to 24.';
        this.toastr.warning(this.message, 'Save');
        //this.pactDemographicsData.isDemographicDataTabComplete = false;
        this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').setValue(null);
        //return;
      }
      if (this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value > 24) {
        this.message = 'Total number of months employed must be between 1 to 24.';
        this.toastr.warning(this.message, 'Save');
        this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').setValue(null);
        //this.pactDemographicsData.isDemographicDataTabComplete = false;
        // return;
      }
    }

    this.pactDemographicsData.pactApplicationID = this.applicationID; //  1;

    this.pactDemographicsData.akaFirstName = this.demoGraphicGroup.get('akaFirstNameCtrl').value;
    this.pactDemographicsData.akaLastName = this.demoGraphicGroup.get('akaLastNameCtrl').value;
    this.pactDemographicsData.ssn = this.demoGraphicGroup.get('ssnCtrl').value;
    this.pactDemographicsData.dob = this.datePipe.transform(this.demoGraphicGroup.get('dobCtrl').value, 'MM/dd/yyyy');
    this.pactDemographicsData.age = this.demoGraphicGroup.get('ageCtrl').value;
    this.pactDemographicsData.genderType = this.demoGraphicGroup.get('genderTypeCtrl').value;
    // this.pactDemographicsData.cin = "";
    this.pactDemographicsData.address = this.demoGraphicGroup.get('addressCtrl').value;
    this.pactDemographicsData.aptNo = this.demoGraphicGroup.get('aptNoCtrl').value;
    this.pactDemographicsData.city = this.demoGraphicGroup.get('cityCtrl').value;
    this.pactDemographicsData.state = this.demoGraphicGroup.get('stateCtrl').value;
    this.pactDemographicsData.zip = this.demoGraphicGroup.get('zipCtrl').value;
    if (this.demoGraphicGroup.get('boroughTypeCtrl').value != 0) {
      this.pactDemographicsData.boroughType = this.demoGraphicGroup.get('boroughTypeCtrl').value;
    } else {
      this.pactDemographicsData.boroughType = null;
    }
    if (this.demoGraphicGroup.get('maritalStatusTypeCtrl').value != 0) {
      this.pactDemographicsData.maritalStatusType = this.demoGraphicGroup.get('maritalStatusTypeCtrl').value;
    } else {
      this.pactDemographicsData.maritalStatusType = null;
    }
    if (this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value != 0) {
      this.pactDemographicsData.wasEmployedIn2Yrs = this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value;
      this.pactDemographicsData.noOfMonthsEmployedIn2Yrs = this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value;
    } else {
      this.pactDemographicsData.wasEmployedIn2Yrs = null;
      this.pactDemographicsData.noOfMonthsEmployedIn2Yrs = null;
    }
    if (this.demoGraphicGroup.get('ethnicityTypeCtrl').value != 0) {
      this.pactDemographicsData.ethnicityType = this.demoGraphicGroup.get('ethnicityTypeCtrl').value;
    } else {
      this.pactDemographicsData.ethnicityType = null;
    }
    if (this.demoGraphicGroup.get('primaryLanguageTypeCtrl').value != 0) {
      this.pactDemographicsData.primaryLanguageType = this.demoGraphicGroup.get('primaryLanguageTypeCtrl').value;
    } else {
      this.pactDemographicsData.primaryLanguageType = null;
    }
    if (this.demoGraphicGroup.get('veteranTypeCtrl').value != 0) {
      this.pactDemographicsData.veteranType = this.demoGraphicGroup.get('veteranTypeCtrl').value;
    } else {
      this.pactDemographicsData.veteranType = null;
    }
    if (this.demoGraphicGroup.get('dischargeTypeCtrl').value != 0) {
      this.pactDemographicsData.dischargeType = this.demoGraphicGroup.get('dischargeTypeCtrl').value;
    } else {
      this.pactDemographicsData.dischargeType = null;
    }
    if (this.demoGraphicGroup.get('educationTypeCtrl').value != 0) {
      this.pactDemographicsData.educationType = this.demoGraphicGroup.get('educationTypeCtrl').value;
    } else {
      this.pactDemographicsData.educationType = null;
    }
    if (this.applicationType == 47) {
      this.pactDemographicsData.isFamilyCompositionTabComplete = this.IsFamilyCompositionTabCompleteCheck();
    }
    else {
      this.pactDemographicsData.isFamilyCompositionTabComplete = true;
    }
   // console.log('save stars demographicsFamilyCompositionGroup.get(isPregnantCtrl).value - ',this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value)
    if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value != null  && this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value != '' ) {
      if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value == 1) {
        this.pactDemographicsData.isPregnant = true;

        if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != null) {
          if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != '') {
            this.pactDemographicsData.expectedDeliveryDate = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value, 'MM/dd/yyyy');
          }
          else { this.pactDemographicsData.expectedDeliveryDate = null; }
        }
        else { this.pactDemographicsData.expectedDeliveryDate = null; }

      } else {
        this.pactDemographicsData.isPregnant = false;
       // console.log('save step pactDemographicsData.isPregnant false -',this.pactDemographicsData.isPregnant );
      }
  }

    this.pactDemographicsData.isDemographicChanged = false;
    this.pactDemographicsData.isDemographicDataTabComplete = this.IsDemographicsTabCompleteCheck(); // true;
    this.pactDemographicsData.isFinancialBenefitsTabComplete = this.IsFinancialBenifitTabCompleteCheck();
    this.pactDemographicsData.isContactsTabComplete = this.checkContactInfoTabListCount(); //false;
    //  this.pactDemographicsData.isFinancialBenefitsTabComplete = false;
    //  this.pactDemographicsData.isFamilyCompositionTabComplete = false;

    this.pactDemographicsData.isMedicalDocumentsTabComplete = false;
    this.pactDemographicsData.isActive = true;

    //  console.log(JSON.stringify(this.pactDemographicsData));

    this.demographicsService.savePACTDemographicsData(this.pactDemographicsData)
      .subscribe(
        data => {
          this.message = 'Demographics data saved successfully.';
          this.toastr.success(this.message, 'Save');

          //this.sidenavStatusService._sidenavStatusReload(this.route);
          if (this.applicationID) {
            this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
          }
          this.getDemographicData();
        },
        //  console.log('Success!', data)
        error => {
          this.message = 'Demographics data did not save.';
          this.toastr.error(this.message, 'Save');
        }//  console.error('Error!', error)
      );
  }

  saveDemographicData_Novalidation() {

    // if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != "") {
    //   this.pactDemographicsData.expectedDeliveryDate = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value, 'MM/dd/yyyy');
    //   if (new Date(this.pactDemographicsData.expectedDeliveryDate) < new Date()) {
    //     return;
    //   }
    // }

    if (this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value == 33 && (this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value != ""
      || this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value != null)) {

      if (this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value < 0) {
        this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').setValue(null);
      }
      if (this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value == 0) {
        this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').setValue(null);
      }
      if (this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value > 24) {
        this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').setValue(null);
      }
    }

    this.pactDemographicsData.pactApplicationID = this.applicationID;
    this.pactDemographicsData.akaFirstName = this.demoGraphicGroup.get('akaFirstNameCtrl').value;
    this.pactDemographicsData.akaLastName = this.demoGraphicGroup.get('akaLastNameCtrl').value;
    this.pactDemographicsData.ssn = this.demoGraphicGroup.get('ssnCtrl').value;
    this.pactDemographicsData.dob = this.datePipe.transform(this.demoGraphicGroup.get('dobCtrl').value, 'MM/dd/yyyy');
    this.pactDemographicsData.age = this.demoGraphicGroup.get('ageCtrl').value;
    this.pactDemographicsData.genderType = this.demoGraphicGroup.get('genderTypeCtrl').value;
    //  this.pactDemographicsData.cin = "";
    this.pactDemographicsData.address = this.demoGraphicGroup.get('addressCtrl').value;
    this.pactDemographicsData.aptNo = this.demoGraphicGroup.get('aptNoCtrl').value;
    this.pactDemographicsData.city = this.demoGraphicGroup.get('cityCtrl').value;
    this.pactDemographicsData.state = this.demoGraphicGroup.get('stateCtrl').value;
    this.pactDemographicsData.zip = this.demoGraphicGroup.get('zipCtrl').value;
    if (this.demoGraphicGroup.get('boroughTypeCtrl').value != 0) {
      this.pactDemographicsData.boroughType = this.demoGraphicGroup.get('boroughTypeCtrl').value;
    } else {
      this.pactDemographicsData.boroughType = null;
    }
    if (this.demoGraphicGroup.get('maritalStatusTypeCtrl').value != 0) {
      this.pactDemographicsData.maritalStatusType = this.demoGraphicGroup.get('maritalStatusTypeCtrl').value;
    } else {
      this.pactDemographicsData.maritalStatusType = null;
    }
    if (this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value != 0) {
      this.pactDemographicsData.wasEmployedIn2Yrs = this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value;
      this.pactDemographicsData.noOfMonthsEmployedIn2Yrs = this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value;
    } else {
      this.pactDemographicsData.wasEmployedIn2Yrs = null;
      this.pactDemographicsData.noOfMonthsEmployedIn2Yrs = null;
    }
    if (this.demoGraphicGroup.get('ethnicityTypeCtrl').value != 0) {
      this.pactDemographicsData.ethnicityType = this.demoGraphicGroup.get('ethnicityTypeCtrl').value;
    } else {
      this.pactDemographicsData.ethnicityType = null;
    }
    if (this.demoGraphicGroup.get('primaryLanguageTypeCtrl').value != 0) {
      this.pactDemographicsData.primaryLanguageType = this.demoGraphicGroup.get('primaryLanguageTypeCtrl').value;
    } else {
      this.pactDemographicsData.primaryLanguageType = null;
    }
    if (this.demoGraphicGroup.get('veteranTypeCtrl').value != 0) {
      this.pactDemographicsData.veteranType = this.demoGraphicGroup.get('veteranTypeCtrl').value;
    } else {
      this.pactDemographicsData.veteranType = null;
    }
    if (this.demoGraphicGroup.get('dischargeTypeCtrl').value != 0) {
      this.pactDemographicsData.dischargeType = this.demoGraphicGroup.get('dischargeTypeCtrl').value;
    } else {
      this.pactDemographicsData.dischargeType = null;
    }
    if (this.demoGraphicGroup.get('educationTypeCtrl').value != 0) {
      this.pactDemographicsData.educationType = this.demoGraphicGroup.get('educationTypeCtrl').value;
    } else {
      this.pactDemographicsData.educationType = null;
    }
    if (this.applicationType == 47) {
      this.pactDemographicsData.isFamilyCompositionTabComplete = this.IsFamilyCompositionTabCompleteCheck();
    }
    else {
      this.pactDemographicsData.isFamilyCompositionTabComplete = true;
    }
    //console.log('no validation save - isPregnantCtrl value - ', this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value );
    if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value != null && this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value != ''  ) {
      if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value == 1) {
        this.pactDemographicsData.isPregnant = true;
        console.log('no validation -  true ', this.pactDemographicsData.isPregnant);

        if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != null) {
          if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != '') {
            this.pactDemographicsData.expectedDeliveryDate = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value, 'MM/dd/yyyy');
          }
          else { this.pactDemographicsData.expectedDeliveryDate = null; }
        }
        else { this.pactDemographicsData.expectedDeliveryDate = null; }
      } else {
        this.pactDemographicsData.isPregnant = false;
        //console.log('no validation -  false ', this.pactDemographicsData.isPregnant);
      }
    }

    this.pactDemographicsData.isComplete = false;
    this.pactDemographicsData.isDemographicChanged = false;
    this.pactDemographicsData.isDemographicDataTabComplete = this.IsDemographicsTabCompleteCheck();
    this.pactDemographicsData.isFinancialBenefitsTabComplete = this.IsFinancialBenifitTabCompleteCheck();
    this.pactDemographicsData.isContactsTabComplete = this.checkContactInfoTabListCount();
    //   this.pactDemographicsData.isContactsTabComplete = false;

    this.pactDemographicsData.isMedicalDocumentsTabComplete = true;
    this.pactDemographicsData.isActive = true;
    //   this.pactDemographicsData.updatedBy = this.userData.optionUserId;;

    //  console.log(JSON.stringify(this.pactDemographicsData));

    this.demographicsService.savePACTDemographicsData(this.pactDemographicsData)
      .subscribe(
        data => {
          console.log('Demographics data saved successfully.!', data);

          //this.sidenavStatusService._sidenavStatusReload(this.route);
          if (this.applicationID) {
            this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
          }
          this.getDemographicData();
        },
        error =>
          console.error('Demographics data did not save.!', error)
      );
  }


  // IsNumericValueCheck(event: { value: number; }): boolean {
  //   console.log(event.value);
  //   if (event.value >= 0 && event.value <= 9) {
  //     return true;
  //   }
  //   else if (event.value.toString() == ".") { return true; }
  //   return false;
  // }

  IsDemographicsTabCompleteCheck(): boolean {

    if (this.pactDemographicsData.ssn && this.pactDemographicsData.dob && this.pactDemographicsData.age
      && this.pactDemographicsData.genderType && this.pactDemographicsData.boroughType && this.pactDemographicsData.maritalStatusType
      && this.pactDemographicsData.wasEmployedIn2Yrs && this.pactDemographicsData.ethnicityType && this.pactDemographicsData.primaryLanguageType
      && this.pactDemographicsData.veteranType && this.pactDemographicsData.educationType
    ) {

      // check the condition for Employment of last 2 years is not yes
      if (this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value != 33) {

        // check the condition for Veterns
        if (this.demoGraphicGroup.get('veteranTypeCtrl').value != 33) {
          return true;
        } else if (this.demoGraphicGroup.get('veteranTypeCtrl').value == 33 && this.demoGraphicGroup.get('dischargeTypeCtrl').value == 0) {
          return false;
        } else if (this.demoGraphicGroup.get('veteranTypeCtrl').value == 33 && this.demoGraphicGroup.get('dischargeTypeCtrl').value != 0) {
          return true;
        }
      } else if (this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value == 33 && (this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value == ""
        || this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value == null || this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value < 0
        || this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value > 24)) {
        return false;
      }
      // check the condition for Employment of last 2 years is  yes
      else if (this.demoGraphicGroup.get('wasEmployedIn2YrsCtrl').value == 33 && this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').value != "") {

        // check the condition for Veterns
        if (this.demoGraphicGroup.get('veteranTypeCtrl').value != 33) {
          return true;
        } else if (this.demoGraphicGroup.get('veteranTypeCtrl').value == 33 && this.demoGraphicGroup.get('dischargeTypeCtrl').value == 0) {
          return false;
          // tslint:disable-next-line: triple-equals
        } else if (this.demoGraphicGroup.get('veteranTypeCtrl').value == 33 && this.demoGraphicGroup.get('dischargeTypeCtrl').value != 0) {
          return true;
        }
      }
    }
    return false;
  }

  calculateAge(event: MatDatepickerInputEvent<Date>) {
    const dob = this.datePipe.transform(event.value, 'MM/dd/yyyy');
    if (dob) {
      const timeDiff = Math.abs(Date.now() - new Date(dob).getTime());
      this.demoGraphicGroup.patchValue({ ageCtrl: Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25) });
    }
  }

  wasEmployedIn2YrsSelectedValueChange(event: { value: string; }) {
    this.wasEmployedIn2YrsSelectedValue = event.value;
    // tslint:disable-next-line: triple-equals
    if (event.value == '33') {
      this.demoGraphicGroup.controls.noOfMonthsEmployedIn2YrsCtrl.setValidators([Validators.required, Validators.min(1), Validators.max(24)]);
      this.demoGraphicGroup.controls.noOfMonthsEmployedIn2YrsCtrl.updateValueAndValidity();
    } else {
      this.demoGraphicGroup.get('noOfMonthsEmployedIn2YrsCtrl').setValue('');
      this.demoGraphicGroup.controls.noOfMonthsEmployedIn2YrsCtrl.clearValidators();
    }
  }

  IsVeteran(event: { value: string; }) {
    this.IsVateranSelectedValue = event.value;
    // tslint:disable-next-line: triple-equals
    if (event.value == '33') {
      this.demoGraphicGroup.controls.dischargeTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.demoGraphicGroup.controls.dischargeTypeCtrl.updateValueAndValidity();
    } else {
      this.demoGraphicGroup.get('dischargeTypeCtrl').setValue(0);
      this.demoGraphicGroup.controls.dischargeTypeCtrl.clearValidators();
    }
  }

  //  #endregion

  //  #region Financial Benifits Tab

  getFinancialBenifits(PactDemographID: string) {

    this.demographicsService.getPACTDemographicsFinancialBenifitsByDemographicsID(PactDemographID).subscribe(
      res => {
        if (res.body) {
          this.pactDemographicsFinancialBenefits = res.body as PACTDemographicsFinancialBenefits;
          // console.log(JSON.stringify(this.pactDemographicsFinancialBenefits));

          if (this.pactDemographicsFinancialBenefits.pactDemographicFinancialBenefitsID != null) {
            this.financialBenifitGroup.get('hdnDemographicFinancialBenefitsIDCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.pactDemographicFinancialBenefitsID);
          }
          if (this.pactDemographicsFinancialBenefits.employmentSalaryType != null) {
            this.financialBenifitGroup.get('empSalaryTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.employmentSalaryType);
            this.empSalaryTypeSelected = this.pactDemographicsFinancialBenefits.employmentSalaryType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.employmentSalaryAmount != null) {
            this.financialBenifitGroup.get('empAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.employmentSalaryAmount);
          }
          if (this.pactDemographicsFinancialBenefits.employmentSalaryFrequencyType) {
            this.financialBenifitGroup.get('empFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.employmentSalaryFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.employmentSalaryAmount,
              this.pactDemographicsFinancialBenefits.employmentSalaryFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('empYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('empYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.publicAssistanceType != null) {
            this.financialBenifitGroup.get('publicAssistanceTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.publicAssistanceType);
            this.publicAssistanceTypeSelected = this.pactDemographicsFinancialBenefits.publicAssistanceType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.publicAssistanceAmount) {
            this.financialBenifitGroup.get('publicAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.publicAssistanceAmount);
          }
          if (this.pactDemographicsFinancialBenefits.publicAssistanceFrequencyType != null) {
            this.financialBenifitGroup.get('publicFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.publicAssistanceFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.publicAssistanceAmount,
              this.pactDemographicsFinancialBenefits.publicAssistanceFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('publicYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('publicYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.ssdType != null) {
            this.financialBenifitGroup.get('ssdTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.ssdType);
            this.ssdTypeSelected = this.pactDemographicsFinancialBenefits.ssdType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.ssdAmount != null) {
            this.financialBenifitGroup.get('ssdAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.ssdAmount);
          }
          if (this.pactDemographicsFinancialBenefits.ssdFrequencyType != null) {
            this.financialBenifitGroup.get('ssdFrequencyTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.ssdFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.ssdAmount,
              this.pactDemographicsFinancialBenefits.ssdFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('ssdYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('ssdYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.veteransGIBillType != null) {
            this.financialBenifitGroup.get('veteransGIBillTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.veteransGIBillType);
            this.veteransGIBillTypeSelected = this.pactDemographicsFinancialBenefits.veteransGIBillType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.veteransGIBillAmount != null) {
            this.financialBenifitGroup.get('veteransGIBillAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.veteransGIBillAmount);
          }
          if (this.pactDemographicsFinancialBenefits.veteransGIBillFrequencyType != null) {
            this.financialBenifitGroup.get('veteransGIBillFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.veteransGIBillFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.veteransGIBillAmount,
              this.pactDemographicsFinancialBenefits.veteransGIBillFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('veteransGIBillYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('veteransGIBillYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.veteransServiceType != null) {
            this.financialBenifitGroup.get('veteransServicesTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.veteransServiceType);
            this.veteransServicesTypeSelected = this.pactDemographicsFinancialBenefits.veteransServiceType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.veteransServiceAmount != null) {
            this.financialBenifitGroup.get('veteransServicesAmoutCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.veteransServiceAmount);
          }
          if (this.pactDemographicsFinancialBenefits.veteransServiceFrequencyType != null) {
            this.financialBenifitGroup.get('veteransServicesFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.veteransServiceFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.veteransServiceAmount,
              this.pactDemographicsFinancialBenefits.veteransServiceFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('veteransServicesYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('veteransServicesYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.socialSecurityType != null) {
            this.financialBenifitGroup.get('socialSecurityTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.socialSecurityType);
            this.socialSecurityTypeSelected = this.pactDemographicsFinancialBenefits.socialSecurityType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.socialSecurityAmount != null) {
            this.financialBenifitGroup.get('socialSecurityAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.socialSecurityAmount);
          }
          if (this.pactDemographicsFinancialBenefits.socialSecurityFrequencyType != null) {
            this.financialBenifitGroup.get('socialSecurityFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.socialSecurityFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.socialSecurityAmount,
              this.pactDemographicsFinancialBenefits.socialSecurityFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('socialSecurityYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('socialSecurityYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.pensionRetirementType != null) {
            this.financialBenifitGroup.get('pensionTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.pensionRetirementType);
            this.pensionTypeSelected = this.pactDemographicsFinancialBenefits.pensionRetirementType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.pensionRetirementAmount != null) {
            this.financialBenifitGroup.get('pensionAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.pensionRetirementAmount);
          }
          if (this.pactDemographicsFinancialBenefits.pensionRetirementFrequencyType != null) {
            this.financialBenifitGroup.get('pensionFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.pensionRetirementFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.pensionRetirementAmount,
              this.pactDemographicsFinancialBenefits.pensionRetirementFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('pensionYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('pensionYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.unemploymentType != null) {
            this.financialBenifitGroup.get('unemploymentTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.unemploymentType);
            this.unemploymentTypeSelected = this.pactDemographicsFinancialBenefits.unemploymentType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.unemploymentAMount != null) {
            this.financialBenifitGroup.get('unemploymentAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.unemploymentAMount);
          }
          if (this.pactDemographicsFinancialBenefits.unemploymentFrequencyType != null) {
            this.financialBenifitGroup.get('unemploymentFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.unemploymentFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.unemploymentAMount,
              this.pactDemographicsFinancialBenefits.unemploymentFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('unemploymentYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('unemploymentYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.medicaidType != null) {
            this.financialBenifitGroup.get('medicaidTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.medicaidType);
            this.medicaidTypeSelected = this.pactDemographicsFinancialBenefits.medicaidType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.medicaidID != null) {
            this.financialBenifitGroup.get('medicaidIdCtrl').setValue(this.pactDemographicsFinancialBenefits.medicaidID);
          }
          if (this.pactDemographicsFinancialBenefits.medicareType != null) {
            this.financialBenifitGroup.get('medicareTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.medicareType);
            this.medicareTypeSelected = this.pactDemographicsFinancialBenefits.medicareType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.snapType != null) {
            this.financialBenifitGroup.get('snapTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.snapType);
            this.snapTypeSelected = this.pactDemographicsFinancialBenefits.snapType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.hivServiceType != null) {
            this.financialBenifitGroup.get('hivServicesTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.hivServiceType);
            this.hivServicesTypeSelected = this.pactDemographicsFinancialBenefits.hivServiceType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.hivServiceAmount != null) {
            this.financialBenifitGroup.get('hivServicesAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.hivServiceAmount);
          }
          if (this.pactDemographicsFinancialBenefits.hivServiceFrequencyType != null) {
            this.financialBenifitGroup.get('hivServicesFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.hivServiceFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.hivServiceAmount,
              this.pactDemographicsFinancialBenefits.hivServiceFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('hivServicesYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('hivServicesYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
          if (this.pactDemographicsFinancialBenefits.otherCompensationType != null) {
            this.financialBenifitGroup.get('otherCompTypeCtrl').setValue(this.pactDemographicsFinancialBenefits.otherCompensationType);
            this.otherCompTypeSelected = this.pactDemographicsFinancialBenefits.otherCompensationType.toString();
          }
          if (this.pactDemographicsFinancialBenefits.otherCompensationAmount != null) {
            this.financialBenifitGroup.get('otherCompAmoutCtrl').setValue(this.pactDemographicsFinancialBenefits.otherCompensationAmount);
          }
          if (this.pactDemographicsFinancialBenefits.otherCompensationFrequencyType != null) {
            this.financialBenifitGroup.get('otherCompFrequencyTypeCtrl')
              .setValue(this.pactDemographicsFinancialBenefits.otherCompensationFrequencyType);
            this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.pactDemographicsFinancialBenefits.otherCompensationAmount,
              this.pactDemographicsFinancialBenefits.otherCompensationFrequencyType);
            if (this.YearlyTotalDisplay == "0.00") { this.financialBenifitGroup.get('otherCompYearlyTotalCtrl').setValue(''); }
            else { this.financialBenifitGroup.get('otherCompYearlyTotalCtrl').setValue(this.YearlyTotalDisplay.toFixed(2).toString()); }
          }
        } else {
          // console.log('Financial Benefits not found');
        }
      },
      error => console.error('Error in retrieving the data for Financial Benifit Tab on page load...!', error)
    );
  }

  saveDemographicFinancialBenifits() {

    this.getFinancialBenifitsValuesFromUI();
    //this.pactDemographicsData.isFinancialBenefitsTabComplete = this.IsFinancialBenifitTabCompleteCheck();
    this.demographicsService.savePACTDemographicsFinancialBenifits(this.pactDemographicsFinancialBenefits)
      .subscribe(
        data => {
          this.message = 'Financial Benefits data saved successfully.';
          this.toastr.success(this.message, 'Save');
        },
        error => {
          this.message = 'Financial Benefits data did not save.';
          this.toastr.error(this.message, 'Save');
          // console.error('Error!', error);
        }
      );
  }

  saveDemographicFinancialBenifits_NoValidation() {

    this.getFinancialBenifitsValuesFromUI();
    //this.pactDemographicsData.isFinancialBenefitsTabComplete = this.IsFinancialBenifitTabCompleteCheck();

    // console.log(JSON.stringify(this.pactDemographicsFinancialBenefits));
    this.demographicsService.savePACTDemographicsFinancialBenifits(this.pactDemographicsFinancialBenefits)
      .subscribe(
        data => {
          this.message = 'Financial Benefits data saved successfully.';
          // console.log(this.message, data);
        },
        error => {
          this.message = 'Financial Benefits data did not save.';
          console.error(this.message, error);
        }
      );
  }

  getFinancialBenifitsValuesFromUI() {

    if (this.financialBenifitGroup.get('hdnDemographicFinancialBenefitsIDCtrl').value == '') {
      this.pactDemographicsFinancialBenefits.pactDemographicFinancialBenefitsID = 0;
    } else {
      this.pactDemographicsFinancialBenefits.pactDemographicFinancialBenefitsID =
        this.financialBenifitGroup.get('hdnDemographicFinancialBenefitsIDCtrl').value;
    }
    // tslint:disable-next-line: radix
    this.pactDemographicsFinancialBenefits.pactDemographicID = parseInt(this.PactDemographID);

    if (this.financialBenifitGroup.get('empSalaryTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.employmentSalaryType = this.financialBenifitGroup.get('empSalaryTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.employmentSalaryType = null;
    }
    if (this.financialBenifitGroup.get('empAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.employmentSalaryAmount = this.financialBenifitGroup.get('empAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.employmentSalaryAmount = null;
    }
    if (this.financialBenifitGroup.get('empFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.employmentSalaryFrequencyType = this.financialBenifitGroup.get('empFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.employmentSalaryFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('publicAssistanceTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.publicAssistanceType = this.financialBenifitGroup.get('publicAssistanceTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.publicAssistanceType = null;
    }
    if (this.financialBenifitGroup.get('publicAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.publicAssistanceAmount = this.financialBenifitGroup.get('publicAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.publicAssistanceAmount = null;
    }
    if (this.financialBenifitGroup.get('publicFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.publicAssistanceFrequencyType = this.financialBenifitGroup.get('publicFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.publicAssistanceFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('ssdTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.ssdType = this.financialBenifitGroup.get('ssdTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.ssdType = null;
    }
    if (this.financialBenifitGroup.get('ssdAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.ssdAmount = this.financialBenifitGroup.get('ssdAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.ssdAmount = null;
    }
    if (this.financialBenifitGroup.get('ssdFrequencyTypeCtrl').value) {
      this.pactDemographicsFinancialBenefits.ssdFrequencyType = this.financialBenifitGroup.get('ssdFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.ssdFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('veteransGIBillTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.veteransGIBillType = this.financialBenifitGroup.get('veteransGIBillTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.veteransGIBillType = null;
    }
    if (this.financialBenifitGroup.get('veteransGIBillAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.veteransGIBillAmount = this.financialBenifitGroup.get('veteransGIBillAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.veteransGIBillAmount = null;
    }
    if (this.financialBenifitGroup.get('veteransGIBillFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.veteransGIBillFrequencyType =
        this.financialBenifitGroup.get('veteransGIBillFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.veteransGIBillFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('veteransServicesTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.veteransServiceType = this.financialBenifitGroup.get('veteransServicesTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.veteransServiceType = null;
    }
    if (this.financialBenifitGroup.get('veteransServicesAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.veteransServiceAmount = this.financialBenifitGroup.get('veteransServicesAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.veteransServiceAmount = null;
    }
    if (this.financialBenifitGroup.get('veteransServicesFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.veteransServiceFrequencyType =
        this.financialBenifitGroup.get('veteransServicesFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.veteransServiceFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('socialSecurityTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.socialSecurityType = this.financialBenifitGroup.get('socialSecurityTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.socialSecurityType = null;
    }
    if (this.financialBenifitGroup.get('socialSecurityAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.socialSecurityAmount = this.financialBenifitGroup.get('socialSecurityAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.socialSecurityAmount = null;
    }
    if (this.financialBenifitGroup.get('socialSecurityFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.socialSecurityFrequencyType =
        this.financialBenifitGroup.get('socialSecurityFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.socialSecurityFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('pensionTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.pensionRetirementType = this.financialBenifitGroup.get('pensionTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.pensionRetirementType = null;
    }
    if (this.financialBenifitGroup.get('pensionAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.pensionRetirementAmount = this.financialBenifitGroup.get('pensionAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.pensionRetirementAmount = null;
    }
    if (this.financialBenifitGroup.get('pensionFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.pensionRetirementFrequencyType =
        this.financialBenifitGroup.get('pensionFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.pensionRetirementFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('unemploymentTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.unemploymentType = this.financialBenifitGroup.get('unemploymentTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.unemploymentType = null;
    }
    if (this.financialBenifitGroup.get('unemploymentAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.unemploymentAMount = this.financialBenifitGroup.get('unemploymentAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.unemploymentAMount = null;
    }
    if (this.financialBenifitGroup.get('unemploymentFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.unemploymentFrequencyType =
        this.financialBenifitGroup.get('unemploymentFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.unemploymentFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('medicaidTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.medicaidType = this.financialBenifitGroup.get('medicaidTypeCtrl').value;
      if (this.financialBenifitGroup.get('medicaidIdCtrl').value.length == 8) {
        this.pactDemographicsFinancialBenefits.medicaidID = this.financialBenifitGroup.get('medicaidIdCtrl').value;
      } else { this.pactDemographicsFinancialBenefits.medicaidID = null; }
    } else {
      this.pactDemographicsFinancialBenefits.medicaidType = null;
      this.pactDemographicsFinancialBenefits.medicaidID = null;
    }

    if (this.financialBenifitGroup.get('medicareTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.medicareType = this.financialBenifitGroup.get('medicareTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.medicareType = null;
    }
    if (this.financialBenifitGroup.get('snapTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.snapType = this.financialBenifitGroup.get('snapTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.snapType = null;
    }

    if (this.financialBenifitGroup.get('hivServicesTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.hivServiceType = this.financialBenifitGroup.get('hivServicesTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.hivServiceType = null;
    }
    if (this.financialBenifitGroup.get('hivServicesAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.hivServiceAmount = this.financialBenifitGroup.get('hivServicesAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.hivServiceAmount = null;
    }
    if (this.financialBenifitGroup.get('hivServicesFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.hivServiceFrequencyType = this.financialBenifitGroup.get('hivServicesFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.hivServiceFrequencyType = null;
    }

    if (this.financialBenifitGroup.get('otherCompTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.otherCompensationType = this.financialBenifitGroup.get('otherCompTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.otherCompensationType = null;
    }
    if (this.financialBenifitGroup.get('otherCompAmoutCtrl').value != "") {
      this.pactDemographicsFinancialBenefits.otherCompensationAmount = this.financialBenifitGroup.get('otherCompAmoutCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.otherCompensationAmount = null;
    }
    if (this.financialBenifitGroup.get('otherCompFrequencyTypeCtrl').value != 0) {
      this.pactDemographicsFinancialBenefits.otherCompensationFrequencyType =
        this.financialBenifitGroup.get('otherCompFrequencyTypeCtrl').value;
    } else {
      this.pactDemographicsFinancialBenefits.otherCompensationFrequencyType = null;
    }

    this.pactDemographicsFinancialBenefits.isActive = true;

  }

  IsFinancialBenifitTabCompleteCheck(): boolean {
    if (this.pactDemographicsFinancialBenefits) {

      if (this.financialBenifitGroup.get('empSalaryTypeCtrl').value != 0 && this.financialBenifitGroup.get('publicAssistanceTypeCtrl').value != 0
        && this.financialBenifitGroup.get('ssdTypeCtrl').value != 0 && this.financialBenifitGroup.get('veteransGIBillTypeCtrl').value != 0
        && this.financialBenifitGroup.get('veteransServicesTypeCtrl').value != 0 && this.financialBenifitGroup.get('socialSecurityTypeCtrl').value != 0
        && this.financialBenifitGroup.get('pensionTypeCtrl').value != 0 && this.financialBenifitGroup.get('unemploymentTypeCtrl').value != 0
        && this.financialBenifitGroup.get('medicaidTypeCtrl').value != 0 && this.financialBenifitGroup.get('medicareTypeCtrl').value != 0
        && this.financialBenifitGroup.get('snapTypeCtrl').value != 0 && this.financialBenifitGroup.get('hivServicesTypeCtrl').value != 0
        && this.financialBenifitGroup.get('otherCompTypeCtrl').value != 0) {

        // console.log("this.financialBenifitGroup.get('empAmoutCtrl').value : ", this.financialBenifitGroup.get('empAmoutCtrl').value);
        // console.log("this.financialBenifitGroup.get('empFrequencyTypeCtrl').value : ", this.financialBenifitGroup.get('empFrequencyTypeCtrl').value);
        // if Employment Salary: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('empSalaryTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('empAmoutCtrl').value == "" || this.financialBenifitGroup.get('empAmoutCtrl').value == null
            || this.financialBenifitGroup.get('empFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if Public Assistance (Recurring grant): Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('publicAssistanceTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('publicAmoutCtrl').value == "" || this.financialBenifitGroup.get('publicAmoutCtrl').value == null
            || this.financialBenifitGroup.get('publicFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if SSD/SSI: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('ssdTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('ssdAmoutCtrl').value == "" || this.financialBenifitGroup.get('ssdAmoutCtrl').value == null
            || this.financialBenifitGroup.get('ssdFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if Veterans Assistance - GI Bill: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('veteransGIBillTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('veteransGIBillAmoutCtrl').value == "" || this.financialBenifitGroup.get('veteransGIBillAmoutCtrl').value == null
            || this.financialBenifitGroup.get('veteransGIBillFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if Veterans Assistance - Services Connected: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('veteransServicesTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('veteransServicesAmoutCtrl').value == "" || this.financialBenifitGroup.get('veteransServicesAmoutCtrl').value == null
            || this.financialBenifitGroup.get('veteransServicesFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if Social Security: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('socialSecurityTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('socialSecurityAmoutCtrl').value == "" || this.financialBenifitGroup.get('socialSecurityAmoutCtrl').value == null
            || this.financialBenifitGroup.get('socialSecurityFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if Pension/Retirement: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('pensionTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('pensionAmoutCtrl').value == "" || this.financialBenifitGroup.get('pensionAmoutCtrl').value == null
            || this.financialBenifitGroup.get('pensionFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if Unemployment Compensation: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('unemploymentTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('unemploymentAmoutCtrl').value == "" || this.financialBenifitGroup.get('unemploymentAmoutCtrl').value == null
            || this.financialBenifitGroup.get('unemploymentFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if Medicaid: Yes then Medicaid# check
        if (this.financialBenifitGroup.get('medicaidTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('medicaidIdCtrl').value == "" || this.financialBenifitGroup.get('medicaidIdCtrl').value == null || this.financialBenifitGroup.get('medicaidIdCtrl').value.length < 8) {
            return false;
          }
        }

        // if HIV/AIDS Services Adm: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('hivServicesTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('hivServicesAmoutCtrl').value == "" || this.financialBenifitGroup.get('hivServicesAmoutCtrl').value == null
            || this.financialBenifitGroup.get('hivServicesFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }

        // if Other Compensation: Yes then Amount OR Frequency selection check
        if (this.financialBenifitGroup.get('otherCompTypeCtrl').value == "33") {
          if (this.financialBenifitGroup.get('otherCompAmoutCtrl').value == "" || this.financialBenifitGroup.get('otherCompAmoutCtrl').value == null
            || this.financialBenifitGroup.get('otherCompFrequencyTypeCtrl').value == "0") {
            return false;
          }
        }
        return true;
      }
    } else { return false; }

  }

  toUpperValue(event: { value: string; }) {
    event.value = event.value.toUpperCase();
  }

  // #region Financial Benifits Tab - dropdown change event

  YearlyAmountCalculationDisplay(amountCtrl: string, frequencyCtrl: string, yearlyTotalCtrl: string) {

    // tslint:disable-next-line: radix
    this.amountValue = parseFloat(this.financialBenifitGroup.get(amountCtrl).value);
    // tslint:disable-next-line: radix
    this.frequencyValue = parseInt(this.financialBenifitGroup.get(frequencyCtrl).value);
    if (this.amountValue != null && this.frequencyValue != null) {
      this.YearlyTotalDisplay = this.YearlyAmountCalculation(this.amountValue, this.frequencyValue).toFixed(2);
      if (this.YearlyTotalDisplay == "0.00") {
        // this.financialBenifitGroup.get(yearlyTotalCtrl).setValue('');
        this.YearlyTotalDisplay = "";
      }
      this.financialBenifitGroup.get(yearlyTotalCtrl).setValue(this.YearlyTotalDisplay.toString());
      //  else {
      //   this.financialBenifitGroup.get(yearlyTotalCtrl).setValue(this.YearlyTotalDisplay.toString());
      // }
    } else {
      this.financialBenifitGroup.get(yearlyTotalCtrl).setValue('');
    }
  }

  YearlyAmountCalculation(amount: any, frequency: number): number {

    //  Frequency :	123	Weekly
    if (frequency == 123) {
      return amount = amount * 52;

      //  Frequency :  124	Semi-Monthly
    } else if (frequency == 124) {
      return amount = amount * 24;

      //  Frequency :  125	Bi-Weekly
    } else if (frequency == 125) {
      return amount = amount * 26;

      //  Frequency :	126	Monthly
    } else if (frequency == 126) {
      return amount = amount * 12;

      //  Frequency :	127	Quarterly
    } else if (frequency == 127) {
      return amount = amount * 4;
    }
    return 0;
  }

  EmpSalarySelectedValueChange(event: { value: string; }) {

    this.empSalaryTypeSelected = event.value;

    if (event.value == '33') {
      this.financialBenifitGroup.controls.empAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.empAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.empFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.empFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('empAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('empFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('empYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.empAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.empFrequencyTypeCtrl.clearValidators();
    }
  }

  publicAssistanceTypeSelectedValueChange(event: { value: string; }) {
    this.publicAssistanceTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.publicAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.publicAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.publicFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.publicFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('publicAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('publicFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('publicYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.publicAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.publicFrequencyTypeCtrl.clearValidators();
    }
  }

  ssdTypeSelectedValueChange(event: { value: string; }) {
    this.ssdTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.ssdAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.ssdAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.ssdFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.ssdFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('ssdAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('ssdFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('ssdYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.ssdAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.ssdFrequencyTypeCtrl.clearValidators();
    }
  }

  veteransGIBillTypeSelectedValueChange(event: { value: string; }) {
    this.veteransGIBillTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.veteransGIBillAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.veteransGIBillAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.veteransGIBillFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.veteransGIBillFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('veteransGIBillAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('veteransGIBillFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('veteransGIBillYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.veteransGIBillAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.veteransGIBillFrequencyTypeCtrl.clearValidators();
    }
  }

  veteransServicesTypeSelectedValueChange(event: { value: string; }) {
    this.veteransServicesTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.veteransServicesAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.veteransServicesAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.veteransServicesFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.veteransServicesFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('veteransServicesAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('veteransServicesFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('veteransServicesYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.veteransServicesAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.veteransServicesFrequencyTypeCtrl.clearValidators();
    }
  }

  socialSecurityTypeSelectedValueChange(event: { value: string; }) {
    this.socialSecurityTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.socialSecurityAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.socialSecurityAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.socialSecurityFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.socialSecurityFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('socialSecurityAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('socialSecurityFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('socialSecurityYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.socialSecurityAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.socialSecurityFrequencyTypeCtrl.clearValidators();
    }
  }

  pensionTypeSelectedValueChange(event: { value: string; }) {
    this.pensionTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.pensionAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.pensionAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.pensionFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.pensionFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('pensionAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('pensionFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('pensionYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.pensionAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.pensionFrequencyTypeCtrl.clearValidators();
    }
  }

  unemploymentTypeSelectedValueChange(event: { value: string; }) {
    this.unemploymentTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.unemploymentAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.unemploymentAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.unemploymentFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.unemploymentFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('unemploymentAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('unemploymentFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('unemploymentYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.unemploymentAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.unemploymentFrequencyTypeCtrl.clearValidators();
    }
  }

  medicaidTypeSelectedValueChange(event: { value: string; }) {
    this.medicaidTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.medicaidIdCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.medicaidIdCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('medicaidIdCtrl').setValue('');

      this.financialBenifitGroup.controls.medicaidIdCtrl.clearValidators();
    }
  }

  medicareTypeSelectedValueChange(value: string) {
    this.medicareTypeSelected = value;
  }

  snapTypeSelectedValueChange(value: string) {
    this.snapTypeSelected = value;
  }

  hivServicesTypeSelectedValueChange(event: { value: string; }) {
    this.hivServicesTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.hivServicesAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.hivServicesAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.hivServicesFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.hivServicesFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('hivServicesAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('hivServicesFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('hivServicesYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.hivServicesAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.hivServicesFrequencyTypeCtrl.clearValidators();
    }
  }

  otherCompTypeSelectedValueChange(event: { value: string; }) {
    this.otherCompTypeSelected = event.value;
    if (event.value == '33') {
      this.financialBenifitGroup.controls.otherCompAmoutCtrl.setValidators([Validators.required]);
      this.financialBenifitGroup.controls.otherCompAmoutCtrl.updateValueAndValidity();
      this.financialBenifitGroup.controls.otherCompFrequencyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.financialBenifitGroup.controls.otherCompFrequencyTypeCtrl.updateValueAndValidity();
    } else {
      this.financialBenifitGroup.get('otherCompAmoutCtrl').setValue('');
      this.financialBenifitGroup.get('otherCompFrequencyTypeCtrl').setValue(0);
      this.financialBenifitGroup.get('otherCompYearlyTotalCtrl').setValue('');

      this.financialBenifitGroup.controls.otherCompAmoutCtrl.clearValidators();
      this.financialBenifitGroup.controls.otherCompFrequencyTypeCtrl.clearValidators();
    }
  }
  // #endregion

  // #endregion

  // #region Demographics Important Contacts Tab

  RetrivetheContactsByDemographicsID(DemographicsID: string) {
    this.demographicsService.getPACTDemographicsContactsByDemographicID(DemographicsID)
      .subscribe(
        res => {
          if (res.body) {
            this.contactDataObj = res.body as PACTDemographicsContacts[];
            // console.log(JSON.stringify(this.contactDataObj));
          }
        },
        error => console.error('Error in retrieving the Contact Data ...!', error)
      );
  }



  saveDemographicsContact() {

    // this.setValidatorsForContactTab();

    if (!this.checkValidatorsForContactInfoTab()) {
      return;
    }

    if (this.demographicsContactsGroup.invalid) {
      return;
    }
    this.pactDemographicsContacts.pactDemographicContactsID = this.demographicsContactsGroup.get('hdnDemographicsContactIDCtrl').value;

    if (this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').value == '0') {
      // tslint:disable-next-line: radix
      this.pactDemographicsContacts.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      this.pactDemographicsContacts.pactDemographicID = this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').value;
    }
    if (this.PactDemographID != '') {
      // tslint:disable-next-line: radix
      this.pactDemographicsContacts.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      this.pactDemographicsContacts.pactDemographicID = this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').value;
    }
    this.pactDemographicsContacts.firstName = this.demographicsContactsGroup.get('firstNameCtrl').value;
    this.pactDemographicsContacts.lastName = this.demographicsContactsGroup.get('lastNameCtrl').value;
    if (this.demographicsContactsGroup.get('relationshipTypeCtrl').value != 0) {
      this.pactDemographicsContacts.relationshipType = this.demographicsContactsGroup.get('relationshipTypeCtrl').value;
      this.pactDemographicsContacts.refGroupDetailID = this.demographicsContactsGroup.get('relationshipTypeCtrl').value;
    } else {
      this.pactDemographicsContacts.relationshipType = null;
      this.pactDemographicsContacts.refGroupDetailID = null;
    }
    this.pactDemographicsContacts.phone = this.demographicsContactsGroup.get('phoneCtrl').value;
    this.pactDemographicsContacts.alternatePhone = this.demographicsContactsGroup.get('alternatePhoneCtrl').value;
    this.pactDemographicsContacts.description = '';
    this.pactDemographicsContacts.isActive = true;
    //this.pactDemographicsData.isContactsTabComplete = this.IsImportantContactsTabCompleteCheck();

    if (this.demographicsContactsGroup.get('firstNameCtrl').value != null && this.demographicsContactsGroup.get('firstNameCtrl').value != "" && 
        this.demographicsContactsGroup.get('lastNameCtrl').value != null && this.demographicsContactsGroup.get('lastNameCtrl').value != "" &&
        this.demographicsContactsGroup.get('relationshipTypeCtrl').value != 0) {

      if (this.pactDemographicsContacts.pactDemographicContactsID == 0) {

        this.demographicsService.checkPACTDemographicsContactsExist(this.PactDemographID, this.pactDemographicsContacts.firstName, this.pactDemographicsContacts.lastName,
          this.pactDemographicsContacts.relationshipType.toString()).subscribe(result => {
            // If Record Found
            if (result.body) {
              const tmp = result.body as PACTDemographicsContacts[];

              if (tmp.length > 0) {
                this.message = 'Contact already Exist';
                this.toastr.warning(this.message, 'Save');
              }
              else { // If Record Doesn't Exist... then save the Contact Information
                this.demographicsService.savePACTDemographicsContacts(this.pactDemographicsContacts)
                  .subscribe(
                    data => {
                      this.message = 'Contact saved successfully.';
                      this.toastr.success(this.message, 'Save');

                      // // display the tab status icon
                      // if (this.pactDemographicsData.isContactsTabComplete != null) {
                      //   this.contactsTabStatus = this.pactDemographicsData.isContactsTabComplete == true ? 1 : 0;
                      // }
                      //this.sidenavStatusService._sidenavStatusReload(this.route);
                      if (this.applicationID) {
                        this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
                      }
                      this.getDemographicData();
                      this.getContactsList();
                      this.clearValidatorsForContactTab();
                      this.clearContactFields();
                    }, // console.log('Success!', data),
                    error => {
                      this.message = 'Contact did not Save.';
                      this.toastr.error(this.message, 'Save');
                    }
                    // console.error('Error!', error)
                  );

              }
            }
          });

      } else {
        // Update the Data
        this.demographicsService.savePACTDemographicsContacts(this.pactDemographicsContacts)
          .subscribe(
            data => {
              this.message = 'Contact saved successfully.';
              this.toastr.success(this.message, 'Save');

              // // display the tab status icon
              // if (this.pactDemographicsData.isContactsTabComplete != null) {
              //   this.contactsTabStatus = this.pactDemographicsData.isContactsTabComplete == true ? 1 : 0;
              // }
              //this.sidenavStatusService._sidenavStatusReload(this.route);
              if (this.applicationID) {
                this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
              }
              this.getDemographicData();
              this.getContactsList();
              this.clearValidatorsForContactTab();
              this.clearContactFields();
            }, // console.log('Success!', data),
            error => {
              this.message = 'Contact did not Save.';
              this.toastr.error(this.message, 'Save');
            }
            // console.error('Error!', error)
          );
      }

    } else {
      this.message = 'Please enter First name, Last name and Relationship';
      this.toastr.warning(this.message, 'Save');
    }
  }

  saveDemographicsContact_NoValidation() {

    this.pactDemographicsContacts.pactDemographicContactsID = this.demographicsContactsGroup.get('hdnDemographicsContactIDCtrl').value;

    if (this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').value == '0') {
      // tslint:disable-next-line: radix
      this.pactDemographicsContacts.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      this.pactDemographicsContacts.pactDemographicID = this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').value;
    }
    if (this.PactDemographID != '') {
      // tslint:disable-next-line: radix
      this.pactDemographicsContacts.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      this.pactDemographicsContacts.pactDemographicID = this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').value;
    }
    this.pactDemographicsContacts.firstName = this.demographicsContactsGroup.get('firstNameCtrl').value;
    this.pactDemographicsContacts.lastName = this.demographicsContactsGroup.get('lastNameCtrl').value;
    if (this.demographicsContactsGroup.get('relationshipTypeCtrl').value != 0) {
      this.pactDemographicsContacts.relationshipType = this.demographicsContactsGroup.get('relationshipTypeCtrl').value;
      this.pactDemographicsContacts.refGroupDetailID = this.demographicsContactsGroup.get('relationshipTypeCtrl').value;
    } else {
      this.pactDemographicsContacts.relationshipType = null;
      this.pactDemographicsContacts.refGroupDetailID = null;
    }
    this.pactDemographicsContacts.phone = this.demographicsContactsGroup.get('phoneCtrl').value;
    this.pactDemographicsContacts.alternatePhone = this.demographicsContactsGroup.get('alternatePhoneCtrl').value;
    this.pactDemographicsContacts.description = '';
    this.pactDemographicsContacts.isActive = true;

    //this.pactDemographicsData.isContactsTabComplete = this.IsImportantContactsTabCompleteCheck();
    // Contact save when first name, last name and  relationship is entered
    if (this.demographicsContactsGroup.get('firstNameCtrl').value != null && this.demographicsContactsGroup.get('firstNameCtrl').value != "" && 
    this.demographicsContactsGroup.get('lastNameCtrl').value != null && this.demographicsContactsGroup.get('lastNameCtrl').value != "" &&
    this.demographicsContactsGroup.get('relationshipTypeCtrl').value != 0) {
      this.demographicsService.savePACTDemographicsContacts(this.pactDemographicsContacts)
        .subscribe(
          data => {
            // console.log('Contact saved successfully.!', data);

            // // display the tab status icon  
            // if (this.pactDemographicsData.isContactsTabComplete != null) {
            //   this.contactsTabStatus = this.pactDemographicsData.isContactsTabComplete == true ? 1 : 0;
            // }
            //this.sidenavStatusService._sidenavStatusReload(this.route);
            if (this.applicationID) {
              this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
            }
            this.getDemographicData();
            this.getContactsList();
            this.clearContactFields();
          },
          error => {
            // console.log('Contact did not Save : ' + error);
          }
        );
     }// else {
    //   this.message = 'Please enter First name, Last name and Relationship';
    //   this.toastr.warning(this.message, 'Save');
    // }

  }

  IsImportantContactsTabCompleteCheck(): boolean {
    if (this.demographicsContactsGroup.get('firstNameCtrl').value != "" && this.demographicsContactsGroup.get('lastNameCtrl').value != ""
      && this.demographicsContactsGroup.get('relationshipTypeCtrl').value != 0) {
      return true;
    } else { return false; }
  }

  clearContactFields() {
    this.demographicsContactsGroup.get('hdnDemographicsContactIDCtrl').setValue(0);
    this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').setValue(0);
    this.demographicsContactsGroup.get('firstNameCtrl').setValue('');
    this.demographicsContactsGroup.get('lastNameCtrl').setValue('');
    this.demographicsContactsGroup.get('relationshipTypeCtrl').setValue(0);
    this.demographicsContactsGroup.get('phoneCtrl').setValue('');
    this.demographicsContactsGroup.get('alternatePhoneCtrl').setValue('');
  }

  clearValidatorsForContactTab = () => {
    this.demographicsContactsGroup.controls.firstNameCtrl.clearValidators();
    this.demographicsContactsGroup.controls.lastNameCtrl.clearValidators();
    this.demographicsContactsGroup.controls.relationshipTypeCtrl.clearValidators();
    this.demographicsContactsGroup.controls.phoneCtrl.clearValidators();
    this.demographicsContactsGroup.controls.alternatePhoneCtrl.clearValidators();
  }

  setValidatorsForContactTab = () => {
    this.demographicsContactsGroup.controls.firstNameCtrl.setValidators([Validators.required]);
    this.demographicsContactsGroup.controls.lastNameCtrl.setValidators([Validators.required]);
    this.demographicsContactsGroup.controls.relationshipTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);

    this.demographicsContactsGroup.controls.firstNameCtrl.updateValueAndValidity();
    this.demographicsContactsGroup.controls.lastNameCtrl.updateValueAndValidity();
    this.demographicsContactsGroup.controls.relationshipTypeCtrl.updateValueAndValidity();
  }


  checkContactInfoTabListCount(): boolean {
    if (this.contactDataCount == 0) {
      this.contactsTabStatus = 1;
      this.pactDemographicsData.isContactsTabComplete = true;
      return true;
    }
    return false;
  }


  checkValidatorsForContactInfoTab(): boolean {
    // if (this.demographicsContactsGroup.get('firstNameCtrl').value == "") {
    //   this.demographicsContactsGroup.controls.firstNameCtrl.setValidators([Validators.required]);
    //   this.demographicsContactsGroup.controls.firstNameCtrl.updateValueAndValidity();
    //   this.message = 'First Name can not be blank';
    //   this.toastr.warning(this.message, 'Save');
    //   return false;
    // } else {
    if (this.demographicsContactsGroup.get('firstNameCtrl').value.length == 1) {
      this.message = 'First Name can not be less than 2 characters';
      this.toastr.warning(this.message, 'Save');
      this.demographicsContactsGroup.controls.firstNameCtrl.setValidators([Validators.required]);
      this.demographicsContactsGroup.controls.firstNameCtrl.updateValueAndValidity();
      return false;
    }
    else {
      this.demographicsContactsGroup.controls.firstNameCtrl.clearValidators();
    }
    // }

    // if (this.demographicsContactsGroup.get('lastNameCtrl').value == "") {
    //   this.demographicsContactsGroup.controls.lastNameCtrl.setValidators([Validators.required]);
    //   this.demographicsContactsGroup.controls.lastNameCtrl.updateValueAndValidity();
    //   this.message = 'Last Name can not be blank';
    //   this.toastr.warning(this.message, 'Save');
    //   return false;
    // } else {
    if (this.demographicsContactsGroup.get('lastNameCtrl').value.length == 1) {
      this.message = 'Last Name can not be less than 2 characters';
      this.toastr.warning(this.message, 'Save');
      this.demographicsContactsGroup.controls.lastNameCtrl.setValidators([Validators.required]);
      this.demographicsContactsGroup.controls.lastNameCtrl.updateValueAndValidity();
      return false;
    } else {
      this.demographicsContactsGroup.controls.lastNameCtrl.clearValidators();
    }
    //}

    if (this.demographicsContactsGroup.get('relationshipTypeCtrl').value == "0") {
      this.demographicsContactsGroup.controls.relationshipTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.demographicsContactsGroup.controls.relationshipTypeCtrl.updateValueAndValidity();
      this.message = 'Please select Relationship';
      this.toastr.warning(this.message, 'Save');
      return false;
    } else {
      this.demographicsContactsGroup.controls.relationshipTypeCtrl.clearValidators();
    }

    return true;
  }
  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady = (params: { api: any; columnApi: any }) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    // tslint:disable-next-line: only-arrow-functions
    this.gridColumnApi.getAllColumns().forEach(function (column) {

      if (column.colId !== 'action' && column.colId !== 'firstName' && column.colId !== 'lastName'
        && column.colId !== 'phone' && column.colId !== 'alternatePhone') {
        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);
      }

    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    /** API call to get the grid data */
    this.getContactsList();

  }

  getContactsList = () => {

    this.demographicsService.getPACTDemographicsContactsByDemographicID(this.PactDemographID)
      .subscribe(
        res => {
          if (res.body) {
            this.contactDataObj = res.body as PACTDemographicsContacts[];
            this.contactDataCount = this.contactDataObj.length;
            //  if (this.contactDataObj) {
            //    if (this.contactDataObj.length > 0) {
            //      this.PactDemographID = this.contactDataObj[0].pactDemographicID.toString();
            //    }
            //  }
            // console.log(JSON.stringify(this.contactDataObj));
            if (this.contactDataCount == 0) {
              this.contactsTabStatus = 1;
              this.pactDemographicsData.isContactsTabComplete = true;
              //this.saveDemographicData_Novalidation();
              // if (this.applicationID) {
              //   this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
              // }
              // this.getDemographicData();
            }
          }

        },
        error => console.error('Error in retrieving the Contact Data By ID...!', error)
      );
    // if (this.contactDataCount == 0) {
    //   this.contactsTabStatus = 1;
    //   this.pactDemographicsData.isContactsTabComplete = true;
    // }
  }

  onGridReadyPriorAppContact = (params: { api: any; columnApi: any }) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    // tslint:disable-next-line: only-arrow-functions
    this.gridColumnApi.getAllColumns().forEach(function (column) {

      if (column.colId !== 'action' && column.colId !== 'firstName' && column.colId !== 'lastName'
        && column.colId !== 'phone' && column.colId !== 'alternatePhone') {
        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);
      }

    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    /** API call to get the grid data */
    this.getPriorAppContactsList();

  }

  getPriorAppContactsList = () => {

    this.demographicsService.getPACTDemographicsPriorAppContactsByDemographicID(this.PactDemographID)
      .subscribe(
        res => {
          if (res.body) {
            this.priorAppContactDataObj = res.body as PACTDemographicsContacts[];
            //  if (this.priorAppContactDataObj) {
            //    if (this.priorAppContactDataObj.length > 0) {
            //      this.PactDemographID = this.priorAppContactDataObj[0].pactDemographicID.toString();
            //    }
            //  }
            // console.log(JSON.stringify(this.priorAppContactDataObj));
          }
        },
        error => console.error('Error in retrieving the Prior App Contact Data By ID...!', error)
      );

  }

  contatDeleteParent = (cell: PACTDemographicsContacts) => {

    const title = 'Confirm Delete';
    const primaryMessage = '';
    const secondaryMessage = `Are you sure you want to delete the selected contact?`;
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => this.deleteContact(cell),
        (negativeResponse) => console.log(),
      );

  }

  deleteContact = (dataSelected: PACTDemographicsContacts) => {
    //  dataSelected.updatedBy = this.userData.optionUserId;

    this.demographicsService.deletePACTDemographicsContacts(dataSelected).subscribe(
      res => {
        this.message = 'Contact deleted successfully.';
        this.toastr.success(this.message, 'Delete');
        // console.error('Success!', res);

        this.getContactsList();

        //this.sidenavStatusService._sidenavStatusReload(this.route);
        if (this.applicationID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
        }

        // this.contactsTabStatus = 1;
        // this.pactDemographicsData.isContactsTabComplete = true;
        //this.getDemographicData();

        this.clearValidatorsForContactTab();
        this.clearContactFields();
      },
      error => {
        this.message = 'Contact did not delete.';
        this.toastr.error(this.message, 'Delete');
        // console.error('Error!', error);
      }
    );
  }

  contactViewParent = (cell: PACTDemographicsContacts) => {
    // console.log(JSON.stringify(cell));
    if (cell.pactDemographicContactsID != null) {
      this.demographicsContactsGroup.get('hdnDemographicsContactIDCtrl').setValue(cell.pactDemographicContactsID);
    }
    if (cell.pactDemographicID != null) {
      this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').setValue(cell.pactDemographicID);
    }
    if (cell.firstName != null) {
      this.demographicsContactsGroup.get('firstNameCtrl').setValue(cell.firstName);
    }
    if (cell.lastName != null) {
      this.demographicsContactsGroup.get('lastNameCtrl').setValue(cell.lastName);
    }
    if (cell.relationshipType != null) {
      this.demographicsContactsGroup.get('relationshipTypeCtrl').setValue(cell.relationshipType);
    }
    if (cell.phone != null) {
      this.demographicsContactsGroup.get('phoneCtrl').setValue(cell.phone);
    }
    if (cell.alternatePhone != null) {
      this.demographicsContactsGroup.get('alternatePhoneCtrl').setValue(cell.alternatePhone);
    }
  }

  priorContactInsertParent = (dataSelected: PACTDemographicsContacts) => {
    //  dataSelected.createdBy = this.userData.optionUserId;
    dataSelected.pactDemographicContactsID = 0;
    if (this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').value == '0') {
      // tslint:disable-next-line: radix
      dataSelected.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      dataSelected.pactDemographicID = this.demographicsContactsGroup.get('hdnDemographicsIDCtrl').value;
    }

    this.demographicsService.checkPACTDemographicsContactsExist(dataSelected.pactDemographicID.toString(),
      dataSelected.firstName, dataSelected.lastName, dataSelected.relationshipType.toString()).subscribe(result => {
        // If Record Found
        if (result.body) {
          const tmp = result.body as PACTDemographicsContacts[];
          if (tmp.length > 0) {
            this.message = 'Contact already Exist';
            this.toastr.warning(this.message, 'Save');
          }
          else {
            // If Record Doesn't Exist... then save the Contact Information
            this.demographicsService.savePACTDemographicsContacts(dataSelected).subscribe(
              res => {
                this.message = 'Prior Application Contact Inserted successfully.';
                this.toastr.success(this.message, 'Save');
                // update the contactlist grid
                this.getContactsList();
                this.contactsTabStatus = 1;
                //this.getDemographicData();
              },
              error => {
                this.message = 'Prior Application Contact did not Insert.';
                this.toastr.error(this.message, 'Save');
                // console.error('Error!', error);
              }
            );

          }
        }
      });

  }

  onDocDataChanged = (event: string) => {
    if (this.applicationID) {
      this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
    }
  }

  // #endregion

  // #region Demographics Family Composition Tab

  RetrivetheFamilyCompositionByDemographicsID(DemographicsID: string) {
    this.demographicsService.getPACTDemographicsFamilyCompositionByDemographicID(DemographicsID)
      .subscribe(
        res => {
          if (res.body) {
            this.familyCompositionDataObj = res.body as PACTDemographicFamilyComposition[];
            this.familyMemberGridCount = this.familyCompositionDataObj.length;
            // console.log("Family Contact : ", JSON.stringify(this.familyCompositionDataObj));
          }
        },
        error => console.error('Error in retrieving the Family Composition Data ...!', error)
      );
  }

  saveDemographicsFamilyComposition() {

    //this.setValidatorsForFamilyCompositionTab();

    if (!this.checkValidatorsForFamilyCompositionTab()) {
      return;
    }

    if (this.applicationType == 47) {
      this.pactDemographicsData.isFamilyCompositionTabComplete = this.IsFamilyCompositionTabCompleteCheck();
    }
    else {
      this.pactDemographicsData.isFamilyCompositionTabComplete = true;
    }

    this.pactDemographicFamilyComposition.pactDemographicFamilyCompositionID =
      this.demographicsFamilyCompositionGroup.get('hdnDemographicFamilyCompositionIDCtrl').value;

    if (this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value == '0') {
      // tslint:disable-next-line: radix
      this.pactDemographicFamilyComposition.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      this.pactDemographicFamilyComposition.pactDemographicID = this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value;
    }
    if (this.PactDemographID != '') {
      // tslint:disable-next-line: radix
      this.pactDemographicFamilyComposition.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      this.pactDemographicFamilyComposition.pactDemographicID = this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value;
    }
    this.pactDemographicFamilyComposition.firstName = this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value;
    this.pactDemographicFamilyComposition.lastName = this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value;
    if (this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value != 0) {
      this.pactDemographicFamilyComposition.relationshipType = this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value;
      this.pactDemographicFamilyComposition.refGroupDetailID = this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value;
    } else {
      this.pactDemographicFamilyComposition.relationshipType = null;
      this.pactDemographicFamilyComposition.refGroupDetailID = null;
    }
    this.pactDemographicFamilyComposition.dob =
      this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('dobCtrl').value, 'MM/dd/yyyy');

    this.pactDemographicFamilyComposition.age = this.demographicsFamilyCompositionGroup.get('ageCtrl').value;

    this.pactDemographicFamilyComposition.description = '';
    this.pactDemographicFamilyComposition.comments = this.demographicsFamilyCompositionGroup.get('commentsCtrl').value;
    this.pactDemographicFamilyComposition.isActive = true;



    if (this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value !="" && this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value != null &&
    this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value !="" && this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value != null &&
    this.demographicsFamilyCompositionGroup.get('dobCtrl').value != null && this.demographicsFamilyCompositionGroup.get('dobCtrl').value != "" &&
      this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value != 0) {

      if (this.pactDemographicFamilyComposition.pactDemographicFamilyCompositionID == 0 && this.pactDemographicFamilyComposition.dob != null) {

        this.demographicsService.checkPACTDemographicsFamilyCompositionExist(this.pactDemographicFamilyComposition.pactDemographicID.toString(),
          this.pactDemographicFamilyComposition.firstName, this.pactDemographicFamilyComposition.lastName, this.pactDemographicFamilyComposition.dob,
          this.pactDemographicFamilyComposition.relationshipType.toString()).subscribe(result => {
            // If Record Found
            if (result.body) {
              const tmp = result.body as PACTDemographicFamilyComposition[];
              // console.log(JSON.stringify(tmp));
              if (tmp.length > 0) {
                this.message = 'Family member already Exist';
                this.toastr.warning(this.message, 'Save');
              }
              else { // If Record Doesn't Exist... then save the family member

                this.demographicsService.savePACTDemographicsFamilyComposition(this.pactDemographicFamilyComposition)
                  .subscribe(
                    data => {
                      this.message = 'Family member saved successfully.';
                      this.toastr.success(this.message, 'Save');
                      this.getFamilyCompositionList();
                      this.clearValidatorsForFamilyCompositionTab();
                      this.clearFamilyCompositionFields();
                      // display the tab status icon
                      if (this.pactDemographicsData.isFamilyCompositionTabComplete != null) {
                        this.familyCompositionTabStatus = this.pactDemographicsData.isFamilyCompositionTabComplete == true ? 1 : 0;
                      }
                      //this.sidenavStatusService._sidenavStatusReload(this.route);
                      if (this.applicationID) {
                        this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
                      }
                    }, // console.log('Success!', data),
                    error => {
                      this.message = 'Family member did not Save.';
                      this.toastr.error(this.message, 'Save');
                    }
                    // console.error('Error!', error)
                  );

              }
            }
          });

      } else {
        // update the record
        this.demographicsService.savePACTDemographicsFamilyComposition(this.pactDemographicFamilyComposition)
          .subscribe(
            data => {
              this.message = 'Family member saved successfully.';
              this.toastr.success(this.message, 'Save');
              this.getFamilyCompositionList();
              this.clearValidatorsForFamilyCompositionTab();
              this.clearFamilyCompositionFields();
              // display the tab status icon
              if (this.pactDemographicsData.isFamilyCompositionTabComplete != null) {
                this.familyCompositionTabStatus = this.pactDemographicsData.isFamilyCompositionTabComplete == true ? 1 : 0;
              }
              //this.sidenavStatusService._sidenavStatusReload(this.route);
              if (this.applicationID) {
                this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
              }
            }, // console.log('Success!', data),
            error => {
              this.message = 'Family member did not Save.';
              this.toastr.error(this.message, 'Save');
            }
            // console.error('Error!', error)
          );
      }

    } else {
      this.message = 'Please enter Firstname, Lastname, Relationship and DOB';
      this.toastr.warning(this.message, 'Save');
    }

  }

  saveDemographicsFamilyComposition_NoValidation() {

    if (!this.checkValidatorsForFamilyCompositionTab()) {
      return;
    }

    if (this.applicationType == 47) {
      this.pactDemographicsData.isFamilyCompositionTabComplete = this.IsFamilyCompositionTabCompleteCheck();
    }
    else {
      this.pactDemographicsData.isFamilyCompositionTabComplete = true;
    }

    this.pactDemographicFamilyComposition.pactDemographicFamilyCompositionID =
      this.demographicsFamilyCompositionGroup.get('hdnDemographicFamilyCompositionIDCtrl').value;

    if (this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value == '0') {
      // tslint:disable-next-line: radix
      this.pactDemographicFamilyComposition.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      this.pactDemographicFamilyComposition.pactDemographicID = this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value;
    }
    if (this.PactDemographID != '') {
      // tslint:disable-next-line: radix
      this.pactDemographicFamilyComposition.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      this.pactDemographicFamilyComposition.pactDemographicID = this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value;
    }


    this.pactDemographicFamilyComposition.firstName = this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value;
    this.pactDemographicFamilyComposition.lastName = this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value;
    if (this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value != 0) {
      this.pactDemographicFamilyComposition.relationshipType = this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value;
      this.pactDemographicFamilyComposition.refGroupDetailID = this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value;
    } else {
      this.pactDemographicFamilyComposition.relationshipType = null;
      this.pactDemographicFamilyComposition.refGroupDetailID = null;
    }
    this.pactDemographicFamilyComposition.dob =
      this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('dobCtrl').value, 'MM/dd/yyyy');
    this.pactDemographicFamilyComposition.age = this.demographicsFamilyCompositionGroup.get('ageCtrl').value;

    this.pactDemographicFamilyComposition.description = '';
    this.pactDemographicFamilyComposition.comments = this.demographicsFamilyCompositionGroup.get('commentsCtrl').value;
    this.pactDemographicFamilyComposition.isActive = true;


    //  only save record if Relationship is selected
    if (this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value !="" && this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value != null &&
    this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value !="" && this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value != null &&
    this.demographicsFamilyCompositionGroup.get('dobCtrl').value != null && this.demographicsFamilyCompositionGroup.get('dobCtrl').value != "" &&
      this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value != 0 ) {
      this.demographicsService.savePACTDemographicsFamilyComposition(this.pactDemographicFamilyComposition)
        .subscribe(
          data => {
            // console.log('Family member saved successfully.', data);
            this.getFamilyCompositionList();
            this.clearFamilyCompositionFields();
            // display the tab status icon
            if (this.pactDemographicsData.isFamilyCompositionTabComplete != null) {
              this.familyCompositionTabStatus = this.pactDemographicsData.isFamilyCompositionTabComplete == true ? 1 : 0;
            }
            //this.sidenavStatusService._sidenavStatusReload(this.route);
            if (this.applicationID) {
              this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
            }
          },
          error => {
            // console.log(' Family member did not Save : ', error);
          }
        );
    }

  }

  IsFamilyCompositionTabCompleteCheck(): boolean {
    var returnValue: boolean;
    returnValue = false;

    // if (this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value != "" && this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value != ""
    //   && this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value != 0 && this.demographicsFamilyCompositionGroup.get('dobCtrl').value != "") {

    //   //check if is pregnant selected or not
    //   if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value != "") {

    //     // is pregnant selected : true
    //     if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value == "1") {

    //       // delivery date selected check
    //       // console.log("this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value : ", this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value);

    //       if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != null) {

    //         if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != '') {

    //           // console.log("this.applicationCreatedDate.toString() :", this.applicationCreatedDate);
    //           // console.log("this.pactDemographicsData.expectedDeliveryDate :", this.pactDemographicsData.expectedDeliveryDate);

    //           //console.log("new Date(this.applicationCreatedDate.toString().slice(0, 12) :", new Date(this.applicationCreatedDate.setMonth(this.applicationCreatedDate.getMonth() + 12)));

    //           this.pactDemographicsData.expectedDeliveryDate = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value, 'MM/dd/yyyy');
    //           if (this.applicationCreatedDate > new Date(this.pactDemographicsData.expectedDeliveryDate)) {

    //             this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').setValue(null);
    //             this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.setValidators([Validators.required]);
    //             this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.updateValueAndValidity();
    //             // this.message = 'Expected Delivery date must be within 12 months from the application created date.';
    //             // this.toastr.warning(this.message, 'Save');
    //             returnValue = false;

    //           } else if (new Date(this.pactDemographicsData.expectedDeliveryDate) > new Date(this.applicationCreatedDate.setMonth(this.applicationCreatedDate.getMonth() + 12))) {


    //             this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').setValue(null);
    //             this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.setValidators([Validators.required]);
    //             this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.updateValueAndValidity();
    //             // this.message = 'Expected Delivery date must be within 12 months from the application created date.';
    //             // this.toastr.warning(this.message, 'Save');
    //             returnValue = false;

    //           } else {
    //             returnValue = true;
    //             this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.clearValidators();
    //           }
    //         } else { returnValue = false; }
    //       } else { returnValue = false; }

    //       // is pregnant selected : false
    //     } else if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value == "0") {
    //       returnValue = true;
    //     }

    //   } else { returnValue = false; }

    // } else {
      //check if is pregnant selected or not
      if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value != "") {
        // is pregnant selected : true
        if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value == "1") {

          // delivery date selected check 
          //console.log("this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value : ", this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value);

          if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != null) {

            if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != '') {

              //  console.log("this.applicationCreatedDate.toString() :", this.applicationCreatedDate);
              //  console.log("this.pactDemographicsData.expectedDeliveryDate :", this.pactDemographicsData.expectedDeliveryDate);

              this.pactDemographicsData.expectedDeliveryDate = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value, 'MM/dd/yyyy');
              if (this.applicationCreatedDate > new Date(this.pactDemographicsData.expectedDeliveryDate)) {

                this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').setValue(null);
                this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.setValidators([Validators.required]);
                this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.updateValueAndValidity();
                // this.message = 'Expected Delivery date must be within 12 months from the application created date.';
                // this.toastr.warning(this.message, 'Save');
                returnValue = false;

              } else if (new Date(this.pactDemographicsData.expectedDeliveryDate) > new Date(this.applicationCreatedDate.setMonth(this.applicationCreatedDate.getMonth() + 12))) {


                this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').setValue(null);
                this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.setValidators([Validators.required]);
                this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.updateValueAndValidity();
                // this.message = 'Expected Delivery date must be within 12 months from the application created date.';
                // this.toastr.warning(this.message, 'Save');
                returnValue = false;

              } else {
                returnValue = true;
                this.demographicsFamilyCompositionGroup.controls.pregnancyDateCtrl.clearValidators();
              }
            } else { returnValue = false; }
          } else { returnValue = false; }

          // check if grid has value and not Young Adult
          if (this.pactDemographicsData.age >= 26){
            if (this.familyMemberGridCount > 0 && returnValue != false ) {  
              returnValue = true;
            } else if (this.familyMemberGridCount == 0 ) {
              returnValue = false;
            }
          }

          // is pregnant selected : false
        } else if (this.demographicsFamilyCompositionGroup.get('isPregnantCtrl').value == "0") {

          // check if grid has value and Young Adult
            if (this.familyMemberGridCount > 0  ) {  // this.pactDemographicsData.age < 26)
              returnValue = true;
            } else if (this.familyMemberGridCount == 0 ) {
              returnValue = false;
            }
        }


      } else { returnValue = false; }


    return returnValue;
  }

  clearFamilyCompositionFields() {
    this.demographicsFamilyCompositionGroup.get('hdnDemographicFamilyCompositionIDCtrl').setValue(0);
    this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').setValue(0);
    this.demographicsFamilyCompositionGroup.get('firstNameCtrl').setValue('');
    this.demographicsFamilyCompositionGroup.get('lastNameCtrl').setValue('');
    this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').setValue(0);
    this.demographicsFamilyCompositionGroup.get('dobCtrl').setValue('');
    this.demographicsFamilyCompositionGroup.get('ageCtrl').setValue('');
    this.demographicsFamilyCompositionGroup.get('commentsCtrl').setValue('');
  }

  clearValidatorsForFamilyCompositionTab = () => {
    this.demographicsFamilyCompositionGroup.controls.firstNameCtrl.clearValidators();
    this.demographicsFamilyCompositionGroup.controls.lastNameCtrl.clearValidators();
    this.demographicsFamilyCompositionGroup.controls.relationshipTypeCtrl.clearValidators();
    this.demographicsFamilyCompositionGroup.controls.dobCtrl.clearValidators();
  }

  setValidatorsForFamilyCompositionTab = () => {
    this.demographicsFamilyCompositionGroup.controls.firstNameCtrl.setValidators([Validators.required]);
    this.demographicsFamilyCompositionGroup.controls.lastNameCtrl.setValidators([Validators.required]);
    this.demographicsFamilyCompositionGroup.controls.relationshipTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.demographicsFamilyCompositionGroup.controls.dobCtrl.setValidators([Validators.required]);

    this.demographicsFamilyCompositionGroup.controls.firstNameCtrl.updateValueAndValidity();
    this.demographicsFamilyCompositionGroup.controls.lastNameCtrl.updateValueAndValidity();
    this.demographicsFamilyCompositionGroup.controls.relationshipTypeCtrl.updateValueAndValidity();
    this.demographicsFamilyCompositionGroup.controls.dobCtrl.updateValueAndValidity();
  }

  checkValidatorsForFamilyCompositionTab(): boolean {
    // if (this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value == "") {
    //   this.demographicsFamilyCompositionGroup.controls.firstNameCtrl.setValidators([Validators.required]);
    //   this.demographicsFamilyCompositionGroup.controls.firstNameCtrl.updateValueAndValidity();
    //   this.message = 'First Name can not be blank';
    //   this.toastr.warning(this.message, 'Save');
    //   return false;
    // } else {
    if (this.demographicsFamilyCompositionGroup.get('firstNameCtrl').value.length == "1") {
      this.message = 'First Name can not be less than 2 characters';
      this.toastr.warning(this.message, 'Save');
      this.demographicsFamilyCompositionGroup.controls.firstNameCtrl.setValidators([Validators.required]);
      this.demographicsFamilyCompositionGroup.controls.firstNameCtrl.updateValueAndValidity();
      return false;
    }
    else {
      this.demographicsFamilyCompositionGroup.controls.firstNameCtrl.clearValidators();
    }
    //}

    // if (this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value == "") {
    //   this.demographicsFamilyCompositionGroup.controls.lastNameCtrl.setValidators([Validators.required]);
    //   this.demographicsFamilyCompositionGroup.controls.lastNameCtrl.updateValueAndValidity();
    //   this.message = 'Last Name can not be blank';
    //   this.toastr.warning(this.message, 'Save');
    //   return false;
    // } else {
    if (this.demographicsFamilyCompositionGroup.get('lastNameCtrl').value.length == "1") {
      this.message = 'Last Name can not be less than 2 characters';
      this.toastr.warning(this.message, 'Save');
      this.demographicsFamilyCompositionGroup.controls.lastNameCtrl.setValidators([Validators.required]);
      this.demographicsFamilyCompositionGroup.controls.lastNameCtrl.updateValueAndValidity();
      return false;
    } else {
      this.demographicsFamilyCompositionGroup.controls.lastNameCtrl.clearValidators();
    }
    //}

    if (this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').value == "0") {
      this.demographicsFamilyCompositionGroup.controls.relationshipTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.demographicsFamilyCompositionGroup.controls.relationshipTypeCtrl.updateValueAndValidity();
      this.message = 'Please select Relationship';
      this.toastr.warning(this.message, 'Save');
      return false;
    } else {
      this.demographicsFamilyCompositionGroup.controls.relationshipTypeCtrl.clearValidators();
    }

    if (this.demographicsFamilyCompositionGroup.get('dobCtrl').value == "") {
      this.demographicsFamilyCompositionGroup.controls.dobCtrl.setValidators([Validators.required]);
      this.demographicsFamilyCompositionGroup.controls.dobCtrl.updateValueAndValidity();
      this.message = 'Please select Date of Birth';
      this.toastr.warning(this.message, 'Save');
      return false;
    } else {
      if (this.demographicsFamilyCompositionGroup.get('dobCtrl').value != "") {
        this.pactDemographicFamilyComposition.dob = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('dobCtrl').value, 'MM/dd/yyyy');
        if (new Date(this.pactDemographicFamilyComposition.dob) > new Date()) {
          this.demographicsFamilyCompositionGroup.get('dobCtrl').setValue(null);
          this.demographicsFamilyCompositionGroup.get('ageCtrl').setValue(null);
          this.demographicsFamilyCompositionGroup.controls.dobCtrl.setValidators([Validators.required]);
          this.demographicsFamilyCompositionGroup.controls.dobCtrl.updateValueAndValidity();
          this.message = 'Date Of Birth can not be greater than today date';
          this.toastr.warning(this.message, 'Save');
          return false;
        } else if (new Date(this.pactDemographicFamilyComposition.dob) < this.minDOB) {
          this.demographicsFamilyCompositionGroup.get('dobCtrl').setValue(null);
          this.demographicsFamilyCompositionGroup.get('ageCtrl').setValue(null);
          this.demographicsFamilyCompositionGroup.controls.dobCtrl.setValidators([Validators.required]);
          this.demographicsFamilyCompositionGroup.controls.dobCtrl.updateValueAndValidity();
          this.message = 'Date Of Birth can not be less than 02/01/1900';
          this.toastr.warning(this.message, 'Save');
          return false;
        } else {
          this.demographicsFamilyCompositionGroup.controls.dobCtrl.clearValidators();
        }
      } else {
        this.demographicsFamilyCompositionGroup.controls.dobCtrl.clearValidators();
      }
    }

    return true;
  }

  onGridReadyFamilyComposition = (params: { api: any; columnApi: any }) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    // tslint:disable-next-line: only-arrow-functions
    this.gridColumnApi.getAllColumns().forEach(function (column) {

      if (column.colId !== 'action' && column.colId !== 'firstName' && column.colId !== 'lastName') {
        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);
      }

    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    /** API call to get the grid data */
    this.getFamilyCompositionList();

  }

  getFamilyCompositionList = () => {

    this.demographicsService.getPACTDemographicsFamilyCompositionByDemographicID(this.PactDemographID)
      .subscribe(
        res => {
          if (res.body) {
            this.familyCompositionDataObj = res.body as PACTDemographicFamilyComposition[];
            // console.log(JSON.stringify(this.familyCompositionDataObj));
            // console.log("Family Contact on Grid Ready : " ,JSON.stringify(this.familyCompositionDataObj));
            this.familyMemberGridCount = this.familyCompositionDataObj.length;
          }
        },
        error => console.error('Error in retrieving the Family Member Data By Demographics ID...!', error)
      );
  }

  onGridReadyPriorAppFamilyComposition = (params: { api: any; columnApi: any }) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    const allColumnIds = [];
    // tslint:disable-next-line: only-arrow-functions
    this.gridColumnApi.getAllColumns().forEach(function (column) {

      if (column.colId !== 'action' && column.colId !== 'firstName' && column.colId !== 'lastName') {
        // console.log('column.colID: ' + column.colId);
        allColumnIds.push(column.colId);
      }

    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);

    /** API call to get the grid data */
    this.getPriorAppFamilyCompositionList();

  }

  getPriorAppFamilyCompositionList = () => {

    this.demographicsService.getPACTDemographicsPriorAppFamilyCompositionByDemographicID(this.PactDemographID)
      .subscribe(
        res => {
          if (res.body) {
            this.priorAppFamilyCompositionDataObj = res.body as PACTDemographicFamilyComposition[];
            // console.log(JSON.stringify(this.priorAppFamilyCompositionDataObj));
            // console.log("Prior Family Contact on Grid Ready : " ,JSON.stringify(this.priorAppFamilyCompositionDataObj));
          }
        },
        error => console.error('Error in retrieving the Prior Application Family Member Data By Demographics ID...!', error)
      );
  }

  familyCompositionViewParent = (cell: PACTDemographicFamilyComposition) => {
    // console.log(JSON.stringify(cell));
    if (cell.pactDemographicFamilyCompositionID != null) {
      this.demographicsFamilyCompositionGroup.get('hdnDemographicFamilyCompositionIDCtrl')
        .setValue(cell.pactDemographicFamilyCompositionID);
    }
    if (cell.pactDemographicID != null) {
      this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').setValue(cell.pactDemographicID);
    }
    if (cell.firstName != null) {
      this.demographicsFamilyCompositionGroup.get('firstNameCtrl').setValue(cell.firstName);
    }
    if (cell.lastName != null) {
      this.demographicsFamilyCompositionGroup.get('lastNameCtrl').setValue(cell.lastName);
    }
    if (cell.relationshipType != null) {
      this.demographicsFamilyCompositionGroup.get('relationshipTypeCtrl').setValue(cell.relationshipType);
    }
    if (cell.dob != null) {
      // this.demographicsContactsGroup.get('dobCtrl').setValue(cell.dob);
      this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('dobCtrl').setValue(new Date(cell.dob)));
    }
    if (cell.age != null) {
      this.demographicsFamilyCompositionGroup.get('ageCtrl').setValue(cell.age);
    }
    if (cell.comments != null) {
      this.demographicsFamilyCompositionGroup.get('commentsCtrl').setValue(cell.comments);
    }
  }

  familyMemberDeleteParent = (cell: PACTDemographicFamilyComposition) => {

    const title = 'Confirm Delete';
    const primaryMessage = '';
    const secondaryMessage = `Are you sure you want to delete the selected Family member?`;
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName)
      .then(
        (positiveResponse) => this.familyCompositionDeleteParent(cell),
        (negativeResponse) => console.log(),
      );
  }

  familyCompositionDeleteParent = (dataSelected: PACTDemographicFamilyComposition) => {
    //  dataSelected.updatedBy = this.userData.optionUserId;
    this.demographicsService.deletePACTDemographicsFamilyComposition(dataSelected).subscribe(
      res => {
        this.message = 'Family member deleted successfully.';
        this.toastr.success(this.message, 'Delete');

        // this.sidenavStatusService._sidenavStatusReload(this.route);
        if (this.applicationID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
        }
        // update the Family Member grid
        this.getFamilyCompositionList();
        this.getDemographicData();
        this.clearValidatorsForFamilyCompositionTab();
        this.clearFamilyCompositionFields();
      },
      error => {
        this.message = 'Family member did not delete.';
        this.toastr.error(this.message, 'Delete');
        // console.error('Error!', error);
      }
    );
  }

  priorfamilyCompositionInsertParent = (dataSelected: PACTDemographicFamilyComposition) => {
    //   dataSelected.createdBy = this.userData.optionUserId;
    dataSelected.pactDemographicFamilyCompositionID = 0;

    if (this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value == '0') {
      // tslint:disable-next-line: radix
      dataSelected.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      dataSelected.pactDemographicID = this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value;
    }
    if (this.PactDemographID != '') {
      // tslint:disable-next-line: radix
      dataSelected.pactDemographicID = parseInt(this.PactDemographID);
    } else {
      dataSelected.pactDemographicID = this.demographicsFamilyCompositionGroup.get('hdnDemographicsIDCtrl').value;
    }

    //Check If Family Composition Exist
    this.demographicsService.checkPACTDemographicsFamilyCompositionExist(dataSelected.pactDemographicID.toString(),
      dataSelected.firstName, dataSelected.lastName, dataSelected.dob, dataSelected.relationshipType.toString()).subscribe(
        result => {
          // If Record Found
          if (result.body) {
            const tmp = result.body as PACTDemographicFamilyComposition[];
            // console.log(JSON.stringify(tmp));
            if (tmp.length > 0) {
              this.message = 'Family member already Exist';
              this.toastr.warning(this.message, 'Save');
            }
            else { // If Record Doesn't Exist... then save the family member

              this.demographicsService.savePACTDemographicsFamilyComposition(dataSelected).subscribe(
                res => {
                  this.message = 'Prior Application Family member Inserted successfully.';
                  this.toastr.success(this.message, 'Save');
                  // update the Family Member grid
                  this.getFamilyCompositionList();
                  this.familyCompositionTabStatus = 1;
                },
                error => {
                  this.message = 'Prior Application Family member did not Insert.';
                  this.toastr.error(this.message, 'Save');
                  // console.error('Error!', error);
                }
              );
            }
          }
        }
      );

  }

  calculateFamilyAge(event: MatDatepickerInputEvent<Date>) {
    const dob = this.datePipe.transform(event.value, 'MM/dd/yyyy');
    if (dob) {
      const timeDiff = Math.abs(Date.now() - new Date(dob).getTime());
      this.demographicsFamilyCompositionGroup.patchValue({ ageCtrl: Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25) });

    }
  }

  setPregnancyRadio(value: string) {
    this.IsPregnantSelectedValue = value;
    if (value == '0') {
      this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').setValue(null);
    }
  }

  // #endregion

  // #region  Common Methods of Component

  displayHideContactFamilyTab(applicationType: number) {
    if (this.applicationType == 46) {
      this.displayContactTab = 1;
      this.displayFamilyTab = 0;
    } else if (this.applicationType == 47) {
      this.displayContactTab = 0;
      this.displayFamilyTab = 1;
    }
  }

  dateValidator(AC: AbstractControl) {
    if (AC && AC.value && !moment(AC.value, 'MM/DD/YYYY', true).isValid()) {
      return { dateValidator: true };
    }
    return null;
  }

  ngOnDestroy = () => {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
    //  this.userDataSub.unsubscribe();
  }

  onTab1FormFieldUnTouched() {
    // this.demographicDataTabStatus = 2;
  }

  onTab1FormFieldTouched() {
    // this.demographicDataTabStatus = 0;
  }

  onTab1FormFieldCompleted() {
    // this.demographicDataTabStatus = 1;
  }

  // // Next button click
  // nextTab(event: Event) {

  //   if (this.selectedTab < 5) {
  //     if (this.selectedTab == 0) {
  //       this.saveDemographicData_Novalidation();
  //     } else if (this.selectedTab == 1) { //  Financial / Benefits Tab When selectedTab == 1
  //       this.saveDemographicFinancialBenifits_NoValidation();
  //       // save demographics Tab due Financial Benifits Tab Status
  //       this.saveDemographicData_Novalidation();
  //     } else if (this.selectedTab == 2) { //  Important Contact Tab When selectedTab == 2
  //       this.saveDemographicsContact_NoValidation();
  //     } else if (this.selectedTab == 3) { //  Family Member Tab When selectedTab == 3
  //       this.saveDemographicsFamilyComposition_NoValidation();
  //       // save demographics Tab due to delivery option
  //       this.saveDemographicData_Novalidation();
  //     } else if (this.selectedTab == 4) { //  Medical Document Tab When selectedTab == 4
  //       // if tab index is 4 then redirect user to next vertical Menu Item
  //       this.router.navigate(['/shs/newApp/housing-homeless/', this.selectedApplicationID]);
  //     }
  //     this.selectedTab = this.selectedTab + 1;
  //     // console.log(" Next " + this.selectedTab);
  //   }
  // }

  // // Previous button click
  // previousTab(event: Event) {
  //   // if tab index is 0 then redirect user to previous vertical Menu Item
  //   if (this.selectedTab == 0) {
  //     this.router.navigate(['/shs/newApp/consent-search/', this.selectedApplicationID]);
  //   } else if (this.selectedTab != 0) {
  //     //  Demographics Data Tab When selectedTab == 0
  //     if (this.selectedTab == 0) {
  //       this.saveDemographicData_Novalidation();
  //     } else if (this.selectedTab == 1) { //  Financial / Benefits Tab When selectedTab == 1
  //       this.saveDemographicFinancialBenifits_NoValidation();
  //       // save demographics Tab due Financial Benifits Tab Status
  //       this.saveDemographicData_Novalidation();
  //     } else if (this.selectedTab == 2) { //  Important Contact Tab When selectedTab == 2
  //       this.saveDemographicsContact_NoValidation();
  //     } else if (this.selectedTab == 3) { //  Family Member Tab When selectedTab == 3
  //       this.saveDemographicsFamilyComposition_NoValidation();
  //       // save demographics Tab due to delivery option
  //       this.saveDemographicData_Novalidation();
  //     } else if (this.selectedTab == 4) { //  Medical Document Tab When selectedTab == 4
  //     }
  //     this.selectedTab = this.selectedTab - 1;
  //     // console.log(" Previous " + this.selectedTab);
  //   }
  // }


  saveAllDemographicsTabs_popstate(event: NavigationStart) {

    //  Demographics Data Tab When selectedTab == 0
    if (this.selectedTab == 0) {
      if (this.demoGraphicGroup) {
        if (this.demoGraphicGroup.touched && event.navigationTrigger != "popstate") {
          this.saveDemographicData_Novalidation();
        }
      }
    } else if (this.selectedTab == 1) {
      if (this.financialBenifitGroup) {
        if (this.financialBenifitGroup.touched && event.navigationTrigger != "popstate") {
          //  Financial / Benefits Tab When selectedTab == 1
          this.saveDemographicFinancialBenifits_NoValidation();
          // save demographics Tab due Financial Benifits Tab Status
          this.saveDemographicData_Novalidation();
        }
      }
    } else if (this.selectedTab == 2) {
      // Application Type : 46 : Individual
      // if (this.applicationType == 46) {
      //  Important Contact Tab When selectedTab == 2
      if (this.demographicsContactsGroup) {
        if (this.demographicsContactsGroup.touched && event.navigationTrigger != "popstate") {
          this.saveDemographicsContact_NoValidation();
        }
      }
      // }
    } else if (this.selectedTab == 3) {
      // Application Type : 47 : Family (Includes pregnant youth)
      if (this.applicationType == 47) {
        if (this.demographicsFamilyCompositionGroup) {
          if (this.demographicsFamilyCompositionGroup.touched && event.navigationTrigger != "popstate") {
            // if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != "") {
            //   this.pactDemographicsData.expectedDeliveryDate = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value, 'MM/dd/yyyy');
            //   if (new Date(this.pactDemographicsData.expectedDeliveryDate) < this.now) {
            //     this.message = 'expected delivery date can not be less than today date';
            //     this.toastr.warning(this.message, 'Save');
            //     return;
            //   }
            // }
            //  Family Member Tab When selectedTab == 3
            // this.saveDemographicsFamilyComposition_NoValidation();
            // save demographics Tab due to delivery option
            this.saveDemographicData_Novalidation();
          }
        }
      }
    }
  }

  saveAllDemographicsTabs() {

    //  Demographics Data Tab When selectedTab == 0
    if (this.selectedTab == 0) {
      this.saveDemographicData();
    } else if (this.selectedTab == 1) {
      //  Financial / Benefits Tab When selectedTab == 1
      this.saveDemographicFinancialBenifits();
      // save demographics Tab due Financial Benifits Tab Status
      this.saveDemographicData_Novalidation();
    } else if (this.selectedTab == 2) {
      // Application Type : 46 : Individual
      // if (this.applicationType == 46) {
      //  Important Contact Tab When selectedTab == 2
      this.saveDemographicsContact_NoValidation();
      // }
    } else if (this.selectedTab == 3) {
      // Application Type : 47 : Family (Includes pregnant youth)
      if (this.applicationType == 47) {
        // if (this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value != "") {
        //   this.pactDemographicsData.expectedDeliveryDate = this.datePipe.transform(this.demographicsFamilyCompositionGroup.get('pregnancyDateCtrl').value, 'MM/dd/yyyy');
        //   if (new Date(this.pactDemographicsData.expectedDeliveryDate) < this.now) {
        //     this.message = 'expected delivery date can not be less than today date';
        //     this.toastr.warning(this.message, 'Save');
        //     return;
        //   }
        //   //   else if (new Date(this.pactDemographicsData.expectedDeliveryDate) > new Date(this.now.setMonth(this.now.getMonth() + 12))) {
        //   //     this.message = 'expected delivery date can not be greater than 12 months';
        //   //     this.toastr.warning(this.message, 'Save');
        //   //     return;
        //   //   }
        // }

        //  Family Member Tab When selectedTab == 3
        // this.saveDemographicsFamilyComposition_NoValidation();
        // save demographics Tab due to delivery option
        this.saveDemographicData_Novalidation();
      }
      //  Medical Document Tab When selectedTab == 3
    }
  }

  // tab changed
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    // Saving Data Between Tab Navigation
    if (this.selectedTab == 0) {
      this.saveDemographicData_Novalidation();
    } else if (this.selectedTab == 1) {
      //  Financial / Benefits Tab When selectedTab == 1
      this.saveDemographicFinancialBenifits_NoValidation();
      // save demographics Tab due Financial Benifits Tab Status
      this.saveDemographicData_Novalidation();
    } else if (this.selectedTab == 2) {
      // Application Type : 46 : Individual
      if (this.applicationType == 46) {
        //  Important Contact Tab When selectedTab == 2
        // Temporary commented
        // this.saveDemographicsContact_NoValidation();
      }
    } else if (this.selectedTab == 3) {
      // Application Type : 47 : Family (Includes pregnant youth)
      if (this.applicationType == 47) {
        //  Family Member Tab When selectedTab == 3
        // Temporary commented
        // this.saveDemographicsFamilyComposition_NoValidation();
        // save demographics Tab due to delivery option
        this.saveDemographicData_Novalidation();
      }
    }
    this.selectedTab = tabChangeEvent.index;
  }

  // SAVE, PREVIOUS, NEXT button footer implemented consuming the shared component
  PreviousPage() {
    // if tab index is 0 then redirect user to previous vertical Menu Item
    if (this.selectedTab == 0) {
      // this.router.navigate(['/shs/newApp/consent-search/', this.selectedApplicationID]);
      this.sidenavStatusService.routeToPreviousPage(this.router, this.route);
    } else if (this.selectedTab != 0) {
      //  Demographics Data Tab When selectedTab == 0
      if (this.selectedTab == 0) {
        this.saveDemographicData_Novalidation();
      } else if (this.selectedTab == 1) {
        //  Financial / Benefits Tab When selectedTab == 1
        this.saveDemographicFinancialBenifits_NoValidation();
        // save demographics Tab due Financial Benifits Tab Status
        this.saveDemographicData_Novalidation();
      } else if (this.selectedTab == 2) {
        // Application Type : 46 : Individual
        if (this.applicationType == 46) {
          //  Important Contact Tab When selectedTab == 2
          // Temporary commented
          // this.saveDemographicsContact_NoValidation();
        }
      } else if (this.selectedTab == 3) {
        // Application Type : 47 : Family (Includes pregnant youth)
        if (this.applicationType == 47) {
          //  Family Member Tab When selectedTab == 3
          // Temporary commented
          // this.saveDemographicsFamilyComposition_NoValidation();
          // save demographics Tab due to delivery option
          this.saveDemographicData_Novalidation();
        }
      }
      this.selectedTab = this.selectedTab - 1;
      // console.log(" Previous " + this.selectedTab);
    }
  }

  NextPage() {
    if (this.selectedTab < 5) {
      if (this.selectedTab == 0) {
        this.saveDemographicData_Novalidation();
      } else if (this.selectedTab == 1) {
        //  Financial / Benefits Tab When selectedTab == 1
        this.saveDemographicFinancialBenifits_NoValidation();
        // save demographics Tab due Financial Benifits Tab Status
        this.saveDemographicData_Novalidation();
      } else if (this.selectedTab == 2) {
        // Application Type : 46 : Individual
        if (this.applicationType == 46) {
          //  Important Contact Tab When selectedTab == 2
          // Temporary commented
          // this.saveDemographicsContact_NoValidation();
        }

      } else if (this.selectedTab == 3) {
        // Application Type : 47 : Family (Includes pregnant youth)
        if (this.applicationType == 47) {
          //  Family Member Tab When selectedTab == 3
          // Temporary commented
          // this.saveDemographicsFamilyComposition_NoValidation();
          // save demographics Tab due to delivery option
          this.saveDemographicData_Novalidation();
        } else {
          // if tab index is 4 then redirect user to next vertical Menu Item
          // this.router.navigate(['/shs/newApp/housing-homeless/', this.selectedApplicationID]);
          this.sidenavStatusService.routeToNextPage(this.router, this.route);
        }
        //  Medical Document Tab When selectedTab == 3
      } else if (this.selectedTab == 4) {
        this.sidenavStatusService.routeToNextPage(this.router, this.route);
      }
      this.selectedTab = this.selectedTab + 1;
      // console.log(" Next " + this.selectedTab);
    }
  }

  // #endregion

}
