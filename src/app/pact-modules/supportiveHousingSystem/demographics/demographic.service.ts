import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
    PACTDemographicsData, PACTDemographicsContacts, PACTDemographicFamilyComposition, PACTDemographicsFinancialBenefits
} from './demographics.model';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DemographicsService {

    getRefGroupDetailsURL = environment.pactApiUrl + 'Common';

    getDemographicsDataURL = environment.pactApiUrl + 'Demographics/GetDemographicsDataByApplicationID';
    saveDemographicsContactURL = environment.pactApiUrl + 'Demographics/SaveDemographicsContacts';

    getDemographicsFinancialBenifitsURL = environment.pactApiUrl + 'Demographics/GetDemographicsFinancialBenifitsByDemographicID';
    saveDemographicsFinancialBenifitsURL = environment.pactApiUrl + 'Demographics/SaveDemographicsFinancialBenifits';

    getDemographicsContactURL = environment.pactApiUrl + 'Demographics/GetDemographicsContactsByDemographicsID';
    saveDemographicsDataURL = environment.pactApiUrl + 'Demographics/SaveDemographicsData';
    deleteDemographicsContactURL = environment.pactApiUrl + 'Demographics/DeleteDemographicsContacts';
    getDemographicsPriorAppContactURL = environment.pactApiUrl + 'Demographics/GetDemographicsPriorAppContactsByDemographicsID';
    getDemographicsContactsExistURL = environment.pactApiUrl + 'Demographics/CheckDemographicsContactsExist';

    getDemographicsFamilyCompositionURL = environment.pactApiUrl + 'Demographics/GetDemographicsFamilyCompositionByDemographicID';
    saveDemographicsFamilyCompositionURL = environment.pactApiUrl + 'Demographics/SaveDemographicsFamilyComposition';
    deleteDemographicsFamilyCompositionURL = environment.pactApiUrl + 'Demographics/DeleteDemographicsFamilyComposition';
    getDemographicsPriorAppFamilyCompositionURL = environment.pactApiUrl + 'Demographics/GetDemographicsPriorAppFamilyCompositionByDemographicID';
    getDemographicsFamilyCompositionExistURL = environment.pactApiUrl + 'Demographics/CheckDemographicsFamilyCompositionExist';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private httpClient: HttpClient) { }


    //#region Demographics - Data Tab

    // getPACTDemographicsDataByApplicationID(ApplicationID: string) {
    //      return this.httpClient.get(this.getDemographicsDataURL, { params: { applicationID: ApplicationID }, observe: 'response' });

    // }

    getPACTDemographicsDataByApplicationID<T>(applicationID: number): Observable<T> {
        return this.httpClient.post<T>(this.getDemographicsDataURL, JSON.stringify(applicationID), this.httpOptions);

        //return this.httpClient.post(this.getADLURL, JSON.stringify(applicationID),observe : 'response' });
    }

    savePACTDemographicsData(pactDemographicsData: PACTDemographicsData): Observable<any> {
        return this.httpClient.post(this.saveDemographicsDataURL, JSON.stringify(pactDemographicsData), this.httpOptions);
    }

    //#endregion


    //#region Demographics - Financial Benifits Tab

    getPACTDemographicsFinancialBenifitsByDemographicsID(DemographicID: string) {
        return this.httpClient.get(this.getDemographicsFinancialBenifitsURL, { params: { PACTDemographicID: DemographicID }, observe: 'response' });
    }

    savePACTDemographicsFinancialBenifits(pactDemographicsFinancialBenefits: PACTDemographicsFinancialBenefits): Observable<any> {
        //console.log(JSON.stringify(pactDemographicsFinancialBenefits));
        return this.httpClient.post(this.saveDemographicsFinancialBenifitsURL, JSON.stringify(pactDemographicsFinancialBenefits), this.httpOptions);
    }

    //#endregion


    //#region Demographics - Important Contact Tab

    getPACTDemographicsContactsByDemographicID(DemographicID: string) {
        return this.httpClient.get(this.getDemographicsContactURL, { params: { PACTDemographicID: DemographicID }, observe: 'response' });
    }

    savePACTDemographicsContacts(pactDemographicsContacts: PACTDemographicsContacts): Observable<any> {
        // console.log(JSON.stringify(pactDemographicsContacts));
        return this.httpClient.post(this.saveDemographicsContactURL, JSON.stringify(pactDemographicsContacts), this.httpOptions);
    }

    deletePACTDemographicsContacts(pactDemographicsContacts: PACTDemographicsContacts): Observable<any> {
        // console.log(JSON.stringify(pactDemographicsContacts));
        return this.httpClient.post(this.deleteDemographicsContactURL, JSON.stringify(pactDemographicsContacts), this.httpOptions);
    }

    getPACTDemographicsPriorAppContactsByDemographicID(DemographicID: string) {
        return this.httpClient.get(this.getDemographicsPriorAppContactURL, { params: { PACTDemographicID: DemographicID }, observe: 'response' });
    }

    checkPACTDemographicsContactsExist(DemographicID: string, Firstname: string, LastName: string, RelationshipType: string) {
        return this.httpClient.get(this.getDemographicsContactsExistURL, { params: { PACTDemographicID: DemographicID, FirstName: Firstname, LastName: LastName, RelationshipType: RelationshipType }, observe: 'response' });
    }
    //#endregion


    //#region Demographics - Family Composition Tab

    getPACTDemographicsFamilyCompositionByDemographicID(DemographicID: string) {
        return this.httpClient.get(this.getDemographicsFamilyCompositionURL, { params: { PACTDemographicID: DemographicID }, observe: 'response' });
    }

    savePACTDemographicsFamilyComposition(pactDemographicsContacts: PACTDemographicFamilyComposition): Observable<any> {
        // console.log(JSON.stringify(pactDemographicsContacts));
        return this.httpClient.post(this.saveDemographicsFamilyCompositionURL, JSON.stringify(pactDemographicsContacts), this.httpOptions);
    }

    deletePACTDemographicsFamilyComposition(pactDemographicsContacts: PACTDemographicFamilyComposition): Observable<any> {
        // console.log(JSON.stringify(pactDemographicsContacts));
        return this.httpClient.post(this.deleteDemographicsFamilyCompositionURL, JSON.stringify(pactDemographicsContacts), this.httpOptions);
    }

    getPACTDemographicsPriorAppFamilyCompositionByDemographicID(DemographicID: string) {
        return this.httpClient.get(this.getDemographicsPriorAppFamilyCompositionURL, { params: { PACTDemographicID: DemographicID }, observe: 'response' });
    }

    checkPACTDemographicsFamilyCompositionExist(DemographicID: string, Firstname: string, LastName: string, DOB: string, RelationshipType: string) {
        return this.httpClient.get(this.getDemographicsFamilyCompositionExistURL, { params: { PACTDemographicID: DemographicID, FirstName: Firstname, LastName: LastName, DOB: DOB, RelationshipType: RelationshipType }, observe: 'response' });
    }
    //#endregion

}


