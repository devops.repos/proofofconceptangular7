import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PACTDemographicFamilyComposition } from './demographics.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: "doc-action",
    template: `<mat-icon  matTooltip='Insert the Prior Application family member to Current Application' (click)="onInsert()" class="doc-action-icon" color="primary">eject</mat-icon>`,
    styles: [
        `
      .doc-action-icon {
        cursor: pointer;
      }
    `
    ],
})
export class PriorAppFamilyCompositionActionComponent implements ICellRendererAngularComp {
    private params: any;
    public cell: any;
    private familySelected: PACTDemographicFamilyComposition;

    constructor() { }

    agInit(params: any): void {
        this.params = params;
        this.cell = { row: params.node.data, col: params.colDef.headerName };
    }

    onInsert() {

        this.familySelected = {
            pactDemographicFamilyCompositionID: this.params.data.pactDemographicFamilyCompositionID,
            pactDemographicID: this.params.data.pactDemographicID,
            firstName: this.params.data.firstName,
            lastName: this.params.data.lastName,
            relationshipType: this.params.data.relationshipType,
            refGroupDetailID: this.params.data.refGroupDetailID,
            description: this.params.data.description,
            dob: this.params.data.dob,
            age: this.params.data.age,
            comments: this.params.data.comments,
            isActive: this.params.data.isActive,
            createdBy: this.params.data.createdBy,
            createdDate: this.params.data.createdDate,
            updatedBy: this.params.data.updatedBy,
            updatedDate: this.params.data.updatedDate,
        };
        this.params.context.componentParent.priorfamilyCompositionInsertParent(this.familySelected);
    }

    refresh(): boolean {
        return false;
    }
}
