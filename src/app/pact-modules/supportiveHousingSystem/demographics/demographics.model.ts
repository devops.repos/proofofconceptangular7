export interface PACTDemographicsData {

    pactDemographicID: number;
    pactApplicationID: number;
    firstName: string;
    lastName: string;
    akaFirstName: string;
    akaLastName: string;
    ssn: string;
    dob: string;
    age: number;
    genderType: number;
    cin: string;
    address: string;
    aptNo: string;
    city: string;
    state: string;
    zip: string;
    boroughType: number;
    maritalStatusType: number;
    wasEmployedIn2Yrs: number;
    noOfMonthsEmployedIn2Yrs: number;
    ethnicityType: number;
    primaryLanguageType: number;
    veteranType: number;
    dischargeType: number;
    educationType: number;
    isPregnant?: boolean;
    expectedDeliveryDate: string;
    isDemographicChanged: boolean;
    isComplete: boolean;
    isDemographicDataTabComplete: boolean;
    isFinancialBenefitsTabComplete: boolean;
    isContactsTabComplete: boolean;
    isFamilyCompositionTabComplete: boolean;
    isMedicalDocumentsTabComplete: boolean;
    isActive: boolean;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;

}

export interface PACTDemographicsFinancialBenefits {

    pactDemographicFinancialBenefitsID: number;
    pactDemographicID: number;
    employmentSalaryType: number;
    employmentSalaryAmount: number;
    employmentSalaryFrequencyType: number;
    publicAssistanceType: number;
    publicAssistanceAmount: number;
    publicAssistanceFrequencyType: number;
    ssdType: number;
    ssdAmount: number;
    ssdFrequencyType: number;
    veteransGIBillType: number;
    veteransGIBillAmount: number;
    veteransGIBillFrequencyType: number;
    veteransServiceType: number;
    veteransServiceAmount: number;
    veteransServiceFrequencyType: number;
    socialSecurityType: number;
    socialSecurityAmount: number;
    socialSecurityFrequencyType: number;
    pensionRetirementType: number;
    pensionRetirementAmount: number;
    pensionRetirementFrequencyType: number;
    unemploymentType: number;
    unemploymentAMount: number;
    unemploymentFrequencyType: number;
    medicaidType: number;
    medicaidID: string;
    medicareType: number;
    snapType: number;
    hivServiceType: number;
    hivServiceAmount: number;
    hivServiceFrequencyType: number;
    otherCompensationType: number;
    otherCompensationAmount: number;
    otherCompensationFrequencyType: number;
    isActive: boolean;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;
}


export interface PACTDemographicsContacts {

    pactDemographicContactsID: number;
    pactDemographicID: number;
    firstName: string;
    lastName: string;
    relationshipType: number;
    refGroupDetailID: number;
    description: string;
    phone: string;
    alternatePhone: string;
    isActive: boolean;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;

}

export interface PACTDemographicFamilyComposition {

    pactDemographicFamilyCompositionID: number;
    pactDemographicID: number;
    firstName: string;
    lastName: string;
    relationshipType: number;
    refGroupDetailID: number;
    description: string;
    dob: string;
    age: number;
    comments: string;
    isActive: boolean;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;

}
