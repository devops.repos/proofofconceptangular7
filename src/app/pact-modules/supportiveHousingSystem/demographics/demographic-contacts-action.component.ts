import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { PACTDemographicsContacts } from './demographics.model';

// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
  selector: "doc-action",
  template: `<mat-icon  matTooltip='Edit Contact' (click)="onView()" class="doc-action-icon" color="primary">edit</mat-icon>
  <mat-icon (click)="onDelete()" matTooltip='Delete Contact' class="doc-action-icon" color="warn">delete_forever</mat-icon>`,
  styles: [
    `
      .doc-action-icon {
        cursor: pointer;
      }
    `
  ],
})
export class ContactsActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  //private docSelected: PACTDemographicsContacts;
  private contactSelected: PACTDemographicsContacts;
  constructor() { }

  agInit(params: any): void {
    this.params = params;
    // this.cell = {row: params.value, col: params.colDef.headerName};
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  onView() {

    this.contactSelected = {
      pactDemographicContactsID: this.params.data.pactDemographicContactsID,
      pactDemographicID: this.params.data.pactDemographicID,
      firstName: this.params.data.firstName,
      lastName: this.params.data.lastName,
      relationshipType: this.params.data.relationshipType,
      refGroupDetailID: this.params.data.refGroupDetailID,
      description: this.params.data.description,
      phone: this.params.data.phone,
      alternatePhone: this.params.data.alternatePhone,
      isActive: this.params.data.isActive,
      createdBy: this.params.data.createdBy,
      createdDate: this.params.data.createdDate,
      updatedBy: this.params.data.updatedBy,
      updatedDate: this.params.data.updatedDate,
    };

    this.params.context.componentParent.contactViewParent(this.contactSelected);
  }

  onDelete() {

    this.contactSelected = {
      pactDemographicContactsID: this.params.data.pactDemographicContactsID,
      pactDemographicID: this.params.data.pactDemographicID,
      firstName: this.params.data.firstName,
      lastName: this.params.data.lastName,
      relationshipType: this.params.data.relationshipType,
      refGroupDetailID: this.params.data.refGroupDetailID,
      description: this.params.data.description,
      phone: this.params.data.phone,
      alternatePhone: this.params.data.alternatePhone,
      isActive: this.params.data.isActive,
      createdBy: this.params.data.createdBy,
      createdDate: this.params.data.createdDate,
      updatedBy: this.params.data.updatedBy,
      updatedDate: this.params.data.updatedDate,
    };

    this.params.context.componentParent.contatDeleteParent(this.contactSelected);
  }

  refresh(): boolean {
    return false;
  }
}
