export class TransmissionCompletionData {
    pactApplicationID: number;
    clientFirstName: string;
    clientLastName: string;
    srNo: number;
    applicationDate: string;
    userID : number;
}

export class ApplicationCompleteStatus {
    tabName : string;
    missingFields : string;
    isCompleted : boolean;
    pactApplicationID : number;
}


  