import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router} from '@angular/router';
import { TransmissionCompletionData, ApplicationCompleteStatus } from './summary-transmit.model';

const pages = [
    'consent-search',
    'demographics',
    'housing-homeless',
    'clinical-assessment',
    'adls',
    'medications-providers-and-hospitalization',
    'trauma-child-welfare',
    'symptoms-substance-use',
    'housing-preferences',
    'psychiatric-psychosocial-mhr'
  ];

@Component({
    selector: 'app-transmit-complete',
    templateUrl: './transmission-status.component.html',
    styleUrls: ['./transmission-status.component.scss']
})

export class TransmissionStatusComponent {
    transmissionCompletionData : TransmissionCompletionData;
    applicationCompleteStatus : ApplicationCompleteStatus;
    istransmissionCompletion : boolean = false;
    isYoungAdultToAdult : boolean = false;
    youngAdultToAdultMessage : string;
    isapplicationCompleteStatus : boolean = false;
    pactApplicationId : number;

    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any
        , private dialogRef: MatDialogRef<TransmissionStatusComponent>
        , private router : Router
        ) { 
            if (data) {
                this.transmissionCompletionData = data.transmissionCompletionData;
                this.applicationCompleteStatus = data.applicationCompleteStatus;

                if(this.transmissionCompletionData === null)
                {
                    this.isapplicationCompleteStatus = true;
                    this.pactApplicationId = this.applicationCompleteStatus[0].pactApplicationID;
                    this.isYoungAdultToAdult = this.applicationCompleteStatus[0].tabName === 'YAToA' ? true : false;

                    if (this.isYoungAdultToAdult)
                        this.youngAdultToAdultMessage = this.applicationCompleteStatus[0].missingFields;
                }

                if(this.applicationCompleteStatus === null)
                {
                    this.istransmissionCompletion = true;
                }
            }

        }

    ngOnInit() {

    }

    //Print Transmission Status 
    printTransmissionStatus() {
        var windowFeatures = "menubar=no,location=no,resizable=no,scrollbars=no,status=no,titlebar=no,height=465,width=700,left=100,top=100";
        var printWindow = window.open('', 'PRINT', windowFeatures);

        printWindow.document.write('<html>');
        printWindow.document.write('<head><title>PACTWeb - Transmission Status</title></head>');
        printWindow.document.write('<body>');
        printWindow.document.write(document.getElementById('matDialogContent').innerHTML);
        printWindow.document.write('</body></html>');

        printWindow.document.close(); // necessary for IE >= 10
        printWindow.focus(); // necessary for IE >= 10*/
        printWindow.print();
        printWindow.close();
    }

    //Exit and Route to Transmitted List Page
    exit() {

        this.router.navigate(['/shs/transmittedApps']);
        this.dialogRef.close(true);
    }

    //Close the dialog on close button
    closeDialog() {
        this.dialogRef.close(true);
    }

    navigateTo(tabName : string)
    {
        switch(tabName)
        {
            case 'Consent / Search':
                this.router.navigate(['/shs/pendingApp/' + pages[0] + '/' + this.pactApplicationId]);
            break;
            case 'Demographics':
                this.router.navigate(['/shs/pendingApp/' + pages[1] + '/' + this.pactApplicationId]);
            break;
            case 'Housing / Homeless':
                this.router.navigate(['/shs/pendingApp/' + pages[2] + '/' + this.pactApplicationId]);
            break;
            case 'Clinical Assessment':
                this.router.navigate(['/shs/pendingApp/' + pages[3] + '/' + this.pactApplicationId]);
            break;
            case 'ADLs':
                this.router.navigate(['/shs/pendingApp/' + pages[4] + '/' + this.pactApplicationId]);
            break;
            case 'Medications, Providers and Hospitalization':
                this.router.navigate(['/shs/pendingApp/' + pages[5] + '/' + this.pactApplicationId]);
            break;
            case 'Trauma Child Welfare and Development':
                this.router.navigate(['/shs/pendingApp/' + pages[6] + '/' + this.pactApplicationId]);
            break;
            case 'Symptoms and Substance Use':
                this.router.navigate(['/shs/pendingApp/' + pages[7] + '/' + this.pactApplicationId]);
            break;
            case 'Housing Preferences':
                this.router.navigate(['/shs/pendingApp/' + pages[8] + '/' + this.pactApplicationId]);
            break;
            case 'Psychiatric / Psychosocial / MHR':
                this.router.navigate(['/shs/pendingApp/' + pages[9] + '/' + this.pactApplicationId]);
            break;

        }

        this.dialogRef.close(true);
    }

    
}