import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryTransmitComponent } from './summary-transmit.component';

describe('SummaryTransmitComponent', () => {
  let component: SummaryTransmitComponent;
  let fixture: ComponentFixture<SummaryTransmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryTransmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryTransmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
