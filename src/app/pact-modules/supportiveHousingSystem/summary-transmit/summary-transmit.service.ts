import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { TransmissionCompletionData } from './summary-transmit.model';

@Injectable({
  providedIn: 'root'
})
export class SummaryTransmitService {
  selectedTab = new BehaviorSubject<number>(0);

  //Get Urls
  getApplicationCompletionStatusUrl = environment.pactApiUrl + 'SummaryTransmit/GetApplicationCompletionStatus';
  transmitApplicationUrl = environment.pactApiUrl + 'SummaryTransmit/TransmitApplication';


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    response: "json",
  };

  constructor(private httpClient: HttpClient) { }

  //Get Methods
  getApplicationCompletionStatus(applicationID: number): Observable<any> {
    return this.httpClient.post(this.getApplicationCompletionStatusUrl, JSON.stringify(applicationID), this.httpOptions);
  }

  transmitApplication(transmissionCompletionData: TransmissionCompletionData): Observable<any> {
    return this.httpClient.post(this.transmitApplicationUrl, JSON.stringify(transmissionCompletionData), this.httpOptions);
  }

  setSelectedTab(value: number) {
    this.selectedTab.next(value);
  }

  getSelectedTab() {
    return this.selectedTab.asObservable();
  }
}

