import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { TransmissionStatusComponent } from './transmission-status.component';
import { MatDialog } from '@angular/material';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

//Models
import { AuthData } from 'src/app/models/auth-data.model';
import { siteDemographic } from '../../vacancyControlSystem/agency-site-maintenance/agency-site-model';
import { PACTPsychiatricPsychoMHR } from '../psychiatric-psychosocial-mhr/psychiatric-psychosocial-mhr.model';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { TransmissionCompletionData, ApplicationCompleteStatus } from './summary-transmit.model';
import { AssessmentApplication } from '../consent-search/consent-search.model';

//Services
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { UserService } from '../../../services/helper-services/user.service';
import { ClientApplicationService } from './../../../services/helper-services/client-application.service';
import { ToastrService } from 'ngx-toastr';
import { SiteAdminService } from '../../vacancyControlSystem/agency-site-maintenance/site-admin.service';
import { PsychiatricPsychosocialMHRService } from '../psychiatric-psychosocial-mhr/psychiatric-psychosocial-mhr.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { SummaryTransmitService } from './summary-transmit.service';
import { ConsentService } from '../consent-search/consent-search.service';

@Component({
  selector: 'app-summary-transmit',
  templateUrl: './summary-transmit.component.html',
  styleUrls: ['./summary-transmit.component.scss'],
  providers: [DatePipe]
})
export class SummaryTransmitComponent implements OnInit, OnDestroy {

  userData: AuthData;
  siteDemographics: siteDemographic;
  pactPsychiatricPsychoMHR = new PACTPsychiatricPsychoMHR();
  transmissionCompletionData: TransmissionCompletionData;
  routeSub: any;
  applicationCompleteStatus: ApplicationCompleteStatus;
  assessmentApplication: AssessmentApplication = { assessmentId: null, systemId: null, systemValue: null, systemFlag: null, userId: null, agcyNo: null, siteNo: null, pendingSRNO: null, savestatus: null };
  appReviewTransmitGroup: FormGroup;
  selectedTab = 0;

  isHoliday: boolean = false;
  isSummaryVisible: boolean = false;
  isPsychiatricVisible: boolean = false;
  isPsychosocialVisible: boolean = false;
  isMHRVisible: boolean = false;
  isTransmitVisible: boolean = false;
  isValid: boolean = false;
  isReportLoaded: boolean = false;
  canTransmit: boolean = false;

  pactApplicationID: number;
  siteID: string;
  clientFirstName: string;
  clientLastName: string;

  referringAgency: string;
  referringSite: string;
  typeSite: string;
  address: string;
  city: string;
  state: string;
  zip: string;

  workerName: string;
  title: string;
  officePhone: string;
  officeExt: string;
  email: string;

  pactReportParameters: PACTReportParameters[] = [];
  reportParams: PACTReportUrlParams;
  reportName: string = 'PACTReportApplicationSummary';
  guidSummary: string;
  guidPsychiatric: string;
  guidPsychosocial: string;
  guidMHR: string;
  guid: string;

  reportUrl: string;
  sanitizedReportUrl: SafeResourceUrl = null;
  srNo: number;

  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  constructor(private formBuilder: FormBuilder
    , private router: Router
    , private activatedRoute: ActivatedRoute
    , private datePipe: DatePipe
    , private sidenavStatusService: SidenavStatusService
    , private userService: UserService
    , private clientApplicationService: ClientApplicationService
    , private toastrService: ToastrService
    , private siteAdminService: SiteAdminService
    , private psychiatricPsychosocialMHRService: PsychiatricPsychosocialMHRService
    , private commonService: CommonService
    , private sanitizer: DomSanitizer
    , private confirmDialogService: ConfirmDialogService
    , public dialog: MatDialog
    , private summaryTransmitService: SummaryTransmitService
    , private consentService: ConsentService
  ) { }

  //Save Data on page refresh
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);
    if (this.pactApplicationID) {
      this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);
    }
    this.setButtonVisibility(this.pactPsychiatricPsychoMHR);
  };


  ngOnInit() {
    this.summaryTransmitService.getSelectedTab().subscribe(res => {
      this.selectedTab = res;
    });
    /** Setting the sidenav Status for the application with the given ID from URL router parameter
     * This is the case for page refresh or reload
     */
    //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);
    /** To Load the Sidenav Completion Status for all the Application related Pages */
    this.activatedRouteSub = this.activatedRoute.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.pactApplicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);

        }
      }
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.appReviewTransmitGroup.touched && event.navigationTrigger != "popstate") { }
        if (event.navigationTrigger == "popstate" && this.appReviewTransmitGroup.dirty) // Handles forward and backward browser clicks
        { }

      };
    });

    //Form Group
    this.appReviewTransmitGroup = this.formBuilder.group({
      verificationConsentCtrl: ['']
    });

    //Get User Data
    this.userService.getUserData().subscribe(res => {
      this.userData = res;
      var canTransmitFunction = this.userData.userFunctions.filter(x => x.functionId === 33);
      //console.log(canTransmitFunction);

      if (canTransmitFunction != null && canTransmitFunction.length > 0)
        this.canTransmit = true;
    });

    //Get Client Application Data
    this.getClientApplicationDetails();

  }

  getClientApplicationDetails() {
    this.clientApplicationService.getClientApplicationData().pipe(take(1)).subscribe(res => {
      if (res && res.pactApplicationId > 0) {
        this.isHoliday = res.isHoliday;
        this.pactApplicationID = res.pactApplicationId;
        this.siteID = res.siteId > 0 ? res.siteId.toString() : null;
        this.clientFirstName = res.firstName;
        this.clientLastName = res.lastName;

        //console.log(this.pactApplicationID);
        //console.log(this.siteID);

        //Get Referring Agency Information
        if (this.siteID != null)
          this.getReferringAgencyDetails();
        else
          this.callToastrService("There is an error while getting Application Data.");

        //Get Psychiatric / Psychosocial / MHR Report Status
        if (this.pactApplicationID > 0)
          this.getPsychiatricPsychosocialMHRDetails();
        else
          this.callToastrService("There is an error while getting Application Data.");

        //Get Summary Report and display
        if (this.pactApplicationID > 0)
          this.displaySummaryReport(this.reportName);
        else
          this.callToastrService("There is an error while getting Application Data.");
      }
    },
      error => {
        //this.callToastrService("There is an error while getting Application Data.");
        throw new Error('There is an error while getting Application Data.');
      });
  }

  callToastrService(message: string) {
    if (!this.toastrService.currentlyActive)
      this.toastrService.error(message);
  }

  getReferringAgencyDetails() {
    this.siteAdminService.getSiteDemogramics(this.siteID).subscribe(res => {
      //console.log(res.body);
      this.siteDemographics = res.body as siteDemographic;

      if (this.siteDemographics != null) {
        this.referringAgency = this.siteDemographics.agencyNo + ' - ' + this.siteDemographics.agencyName;
        this.referringSite = this.siteDemographics.siteNo + ' - ' + this.siteDemographics.name;
        this.typeSite = this.siteDemographics.siteTypeDesc;
        this.address = this.siteDemographics.address;
        this.city = this.siteDemographics.city;
        this.state = this.siteDemographics.state;
        this.zip = this.siteDemographics.zip;
      }

    },
      error => {
        //this.callToastrService("There is an error while getting Application Data.");
        throw new Error('There is an error while getting Application Data.');
      });
  }

  getPsychiatricPsychosocialMHRDetails() {
    this.psychiatricPsychosocialMHRService.getPACTPsychiatricPsychoMHR(this.pactApplicationID)
      .subscribe(
        res => {
          this.pactPsychiatricPsychoMHR = res;

          //Set Button Visibility
          this.setButtonVisibility(this.pactPsychiatricPsychoMHR);

        });
  }

  //On Consent check change
  onIsConsentCheckChange(event: { checked: Boolean }) {
    if (event.checked) {
      this.appReviewTransmitGroup.controls['verificationConsentCtrl'].setValue(true);
      this.workerName = this.userData.firstName + " " + this.userData.lastName;
      this.title = this.userData.roleName;
      this.officePhone = this.userData.officePhone;
      this.officeExt = this.userData.officeExt;
      this.email = this.userData.email;

    }
    else {
      this.appReviewTransmitGroup.controls['verificationConsentCtrl'].setValue(false);
      this.workerName = "";
      this.title = "";
      this.officePhone = "";
      this.officeExt = "";
      this.email = "";
    }
  }

  //Tab Changes Event
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab = tabChangeEvent.index;

    //Set Button Visibility
    this.setButtonVisibility(this.pactPsychiatricPsychoMHR);

    if (this.selectedTab == 0) {
      this.displaySummaryReport(this.reportName);
    }
  }

  //Next button click
  nextTab() {

    if (this.selectedTab < 1) {
      this.selectedTab = this.selectedTab + 1;
    }
    else if (this.selectedTab >= 1) {
      //Transmit Button Enable
    }

    //Set Button Visibility
    this.setButtonVisibility(this.pactPsychiatricPsychoMHR);

  }

  //Previous button click
  previousTab() {
    if (this.selectedTab != 0) {
      this.selectedTab = this.selectedTab - 1;
      //Next Button Enable

      //Display Report in Tab
      this.displaySummaryReport(this.reportName);

    }
    else if (this.selectedTab <= 0) {
      this.sidenavStatusService.routeToPreviousPage(this.router, this.activatedRoute);
    }

    //Set Button Visibility
    this.setButtonVisibility(this.pactPsychiatricPsychoMHR);

  }

  setButtonVisibility(pactPsychiatricPsychoMHR: PACTPsychiatricPsychoMHR) {
    if (this.selectedTab > 0) {
      this.isTransmitVisible = true;

      this.isSummaryVisible = false;
      this.isPsychiatricVisible = false;
      this.isPsychosocialVisible = false;
      this.isMHRVisible = false;
    }
    else {
      this.isTransmitVisible = false;

      this.isSummaryVisible = true;
      //console.log('Transmit - Button Visibility ', this.pactPsychiatricPsychoMHR);
      if (this.pactPsychiatricPsychoMHR != null) {
        this.isPsychiatricVisible = pactPsychiatricPsychoMHR.hasPsychiatricReport == true
          && pactPsychiatricPsychoMHR.hasPsychiatricAttachmentORDataEnter == 357 ? true : false;

        this.isPsychosocialVisible = pactPsychiatricPsychoMHR.hasPsychosocialReport == true
          && pactPsychiatricPsychoMHR.hasPsychosocialAttachmentORDataEnter == 357 ? true : false;

        this.isMHRVisible = pactPsychiatricPsychoMHR.isMHRYes;
      }
      else {
        this.isPsychiatricVisible = false;
        this.isPsychosocialVisible = false;
        this.isMHRVisible = false;
      }
    }

  }

  displaySummaryReport(reportName: string) {
    //console.log('inside displaySummaryReport');
    this.isReportLoaded = false;
    this.sanitizedReportUrl = null;

    //console.log('DisplaySummaryReport - AppID ', this.pactApplicationID);
    if (this.pactApplicationID > 0) {
      this.pactReportParameters = [
        { parameterName: 'applicationID', parameterValue: this.pactApplicationID.toString(), CreatedBy: this.userData.optionUserId },
        { parameterName: 'reportName', parameterValue: reportName, CreatedBy: this.userData.optionUserId },
        { parameterName: 'source', parameterValue: this.router.url, CreatedBy: this.userData.optionUserId },
      ];

      this.commonService.generateGUID(this.pactReportParameters)
        .subscribe(
          res => {
            if (res !== null) {
              this.guidSummary = res;
              this.guid = res;
              this.reportParams = { reportParameterID: this.guid, reportName: reportName, "reportFormat": "PDF" };
              this.commonService.generateReport(this.reportParams)
                .subscribe(
                  res => {
                    var data = new Blob([res.body], { type: 'application/pdf' });
                    if (data.size > 512) {
                      //console.log('Blob - ', data);
                      var fileURL = URL.createObjectURL(data);
                      this.sanitizedReportUrl = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                      this.isReportLoaded = true;
                    }
                  },
                  error => {
                    //this.callToastrService("There is an error while generating report.");
                    throw new Error('There is an error while generating Application Summary Report.');
                  });
            }
          },
          error => {
            //this.callToastrService("There is an error while generating report.");
            throw new Error('There is an error while generating Application Summary Report.');
          });
    }

  }

  openReport(reportName: string) {

    switch (reportName) {
      case 'PsychiarticEvalReport':
        this.guid = this.guidPsychiatric != null ? this.guidPsychiatric : null;
        break;
      case 'PsychosocialEvalReport':
        this.guid = this.guidPsychosocial != null ? this.guidPsychosocial : null;
        break;
      case 'MentalHealthReport':
        this.guid = this.guidMHR != null ? this.guidMHR : null;
        break;
      case 'PACTReportApplicationSummary':
        this.guid = this.guidSummary != null ? this.guidSummary : null;
        break;
    }

    //console.log(this.guid);
    if (this.guid === null) {
      if (this.pactApplicationID > 0) {
        this.pactReportParameters = [
          { parameterName: 'applicationID', parameterValue: this.pactApplicationID.toString(), CreatedBy: this.userData.optionUserId },
          { parameterName: 'reportName', parameterValue: reportName, CreatedBy: this.userData.optionUserId },
          { parameterName: 'source', parameterValue: this.router.url, CreatedBy: this.userData.optionUserId },
        ];

        this.commonService.generateGUID(this.pactReportParameters)
          .subscribe(
            res => {
              if (res !== null) {

                switch (reportName) {
                  case 'PsychiarticEvalReport':
                    this.guidPsychiatric = res;
                    break;
                  case 'PsychosocialEvalReport':
                    this.guidPsychosocial = res;
                    break;
                  case 'MentalHealthReport':
                    this.guidMHR = res;
                    break;
                  case 'PACTReportApplicationSummary':
                    this.guidSummary = res;
                    break;
                }

                this.guid = res;
                this.reportParams = { reportParameterID: this.guid, reportName: reportName, "reportFormat": "PDF" };
                this.generateReportURL(this.reportParams);
              }
            },
            error => {
              //this.callToastrService("There is an error while generating report.");
              throw new Error('There is an error while generating Application Summary Report.');
            }
          );
      }

    }
    else {
      this.reportParams = { reportParameterID: this.guid, reportName: reportName, "reportFormat": "PDF" };
      this.generateReportURL(this.reportParams);
    }
  }

  generateReportURL(reportparameters: PACTReportUrlParams) {
    this.commonService.generateReport(reportparameters)
      .subscribe(
        res => {
          var data = new Blob([res.body], { type: 'application/pdf' });
          //console.log(data);
          if (data.size > 512) {
            var fileURL = URL.createObjectURL(data);
            const printContent = window.open(fileURL, "_blank");
            if (printContent != null) {
              printContent.focus();
              printContent.print();
            }
          }
        },
        error => {
          //this.callToastrService("There is an error while generating report.");
          throw new Error('There is an error while generating Application Summary Report.');
        });
  }

  transmit() {
    if (this.isHoliday || !this.commonService.isBusinessHours8AMto8PM()) {
      this.callToastrService("Application transmission restricted to 8:00am to 8:00pm, Monday through Friday.");
      return;
    }
    
    var verificationConsent = this.appReviewTransmitGroup.get('verificationConsentCtrl').value;

    if (verificationConsent) {
      //Get Application Completion Status
      this.summaryTransmitService.getApplicationCompletionStatus(this.pactApplicationID)
        .subscribe(res => {
          //console.log(res);
          if (res.length === 0) {
            //Users Allowed to Transmit Application
            if (this.canTransmit) {
              const title = 'Transmission Status';
              const primaryMessage = `The Application Package is complete.`;
              const secondaryMessage = `Do you want to transmit the application?`;
              const confirmButtonName = 'Yes';
              const dismissButtonName = 'No';

              this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                (positiveResponse) => {
                  this.transmissionCompletionData = {
                    pactApplicationID: this.pactApplicationID
                    , clientFirstName: null, clientLastName: null
                    , srNo: null, applicationDate: null, userID: this.userData.optionUserId
                  };

                  //Tranmit Application Call
                  this.summaryTransmitService.transmitApplication(this.transmissionCompletionData)
                    .subscribe(res => {
                      if (res !== null) {
                        var applicationDate = this.datePipe.transform(res.applicationDate, 'MM/dd/yyyy');
                        this.srNo = res.srNo;
                        this.applicationCompleteStatus = null;

                        this.transmissionCompletionData = {
                          pactApplicationID: this.pactApplicationID
                          , clientFirstName: this.clientFirstName, clientLastName: this.clientLastName
                          , srNo: this.srNo, applicationDate: applicationDate
                          , userID: this.userData.optionUserId
                        }
                        if (res.assessmentID) {
                          this.createAssessmentApplication(res.assessmentID, res.pactApplicationID, res.siteNo);
                        }
                      }
                      this.openTransmitCompleteDialog();
                    },
                      error => {
                        //this.callToastrService("There is an error while transmitting application.");
                        throw new Error('There is an error while transmitting the Application.');
                      });
                },
                (negativeResponse) => { },
              );
            }
            else {
              const title = 'Transmission Status';
              const primaryMessage = `The Application Package (# ` + this.pactApplicationID + `) is complete and ready for transmission.`;
              const secondaryMessage = `User unauthorized to transmit. Please work with Site Supervisor/System Administrator to transmit the application.`;
              const confirmButtonName = 'Print';
              const dismissButtonName = 'Exit';

              this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName).then(
                (positiveResponse) => {
                  this.printCantTransmission(title, primaryMessage, secondaryMessage);
                },
                (negativeResponse) => { },
              );
            }


          }
          else {
            //Application Status Incomplete
            this.applicationCompleteStatus = res;
            this.transmissionCompletionData = null;

            this.openTransmitCompleteDialog();
          }
        },
          error => {
            //this.callToastrService("There is an error while transmitting application.");
            throw new Error('There is an error while transmitting the Application.');
          }
        );
    }
    else {
      this.callToastrService("Verification Consent Check is required.");
    }

  }

  //Open Cosent Forms Popup Dialog
  openTransmitCompleteDialog(): void {
    // console.log('Tran Data',this.transmissionCompletionData);
    // console.log('AppComp Data',this.applicationCompleteStatus);

    this.dialog.open(TransmissionStatusComponent, {
      width: '65%',
      disableClose: true,
      data: {
        transmissionCompletionData: this.transmissionCompletionData,
        applicationCompleteStatus: this.applicationCompleteStatus,
      },
      closeOnNavigation: true
    });
  }

  printCantTransmission(title: string, primaryMessage: string, secondaryMessage: string) {
    var windowFeatures = "menubar=no,location=no,resizable=no,scrollbars=no,status=no,titlebar=no,height=465,width=700,left=100,top=100";
    var printWindow = window.open('', 'PRINT', windowFeatures);

    printWindow.document.write('<html>');
    printWindow.document.write('<head><title>PACTWeb - ' + title + '</title></head>');
    printWindow.document.write('<body>');
    printWindow.document.write('<div style="text-align: center"><b>' + title + '</b></div><br/>');
    printWindow.document.write('<HR></HR><br/>');
    printWindow.document.write('<div>' + primaryMessage + '</div><br/>');
    printWindow.document.write('<div>' + secondaryMessage + '</div><br/>');
    printWindow.document.write('</body></html>');
    printWindow.document.close(); // necessary for IE >= 10
    printWindow.focus(); // necessary for IE >= 10*/
    printWindow.print();
    printWindow.close();
  }

  //Dispose on Destroy
  ngOnDestroy = () => {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }

    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }

    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

  //Create Assessment Application
  createAssessmentApplication(assessmentID: number, pactApplicationId: number, siteNo: string) {
    this.assessmentApplication.assessmentId = assessmentID.toString();
    this.assessmentApplication.systemId = 131;
    this.assessmentApplication.systemValue = pactApplicationId.toString();
    this.assessmentApplication.systemFlag = "T";
    this.assessmentApplication.userId = this.userData.lanId;
    this.assessmentApplication.agcyNo = this.userData.agencyNo;
    this.assessmentApplication.siteNo = siteNo;
    this.assessmentApplication.pendingSRNO = pactApplicationId.toString();
    this.consentService.createAssessmentApplication(this.assessmentApplication).subscribe(res => {
      if (!res.isUpdated) {
        throw new Error("Unable to create assessment application.");
      }
    });
  }
}
