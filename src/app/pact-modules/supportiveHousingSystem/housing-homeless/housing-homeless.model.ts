export interface HousingTabStatus {
    pactApplicationID?: number;
    isHomelessHistoryTabComplete?: boolean;
    isHousingDocumentsTabComplete?: boolean;
}