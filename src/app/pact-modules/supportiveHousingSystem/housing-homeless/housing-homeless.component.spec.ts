import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HousingHomelessComponent } from './housing-homeless.component';

describe('HousingHomelessComponent', () => {
  let component: HousingHomelessComponent;
  let fixture: ComponentFixture<HousingHomelessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HousingHomelessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousingHomelessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
