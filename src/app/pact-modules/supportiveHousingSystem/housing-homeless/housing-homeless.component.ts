import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { HousingHomelessService } from './housing-homeless.service';
import { HousingTabStatus } from './housing-homeless.model';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-housing-homeless',
  templateUrl: './housing-homeless.component.html',
  styleUrls: ['./housing-homeless.component.scss'],
  providers: [DatePipe]
})
export class HousingHomelessComponent implements OnInit, OnDestroy {
  tabSelectedIndex = 0;
  HHHStatus = 2; // 0 = Partial; 1 = Complete; 2 = null
  HDStatus = 2; // 0 = Partial; 1 = Complete; 2 = null
  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;
  pactApplicationID: number;
  housingTabStatus: HousingTabStatus;
  panelExpanded = false;

  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;
  homelessServiceSub: Subscription;

  constructor(
    private sidenavStatusService: SidenavStatusService,
    private route: ActivatedRoute,
    private router: Router,
    private housingHomelessService: HousingHomelessService,
    private confirmDialogService: ConfirmDialogService
  ) { }

  ngOnInit() {
    // this.sidenavStatusService._sidenavStatusReload(this.route);
    /** To Load the Sidenav Completion Status for all the Application related Pages */
    this.activatedRouteSub = this.route.paramMap.subscribe((params) => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.pactApplicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);
        }
      }
    });

  }

  getHousingTabStatus = () => {
    this.homelessServiceSub = this.housingHomelessService.getHousingTabStatus(this.pactApplicationID).subscribe((res: HousingTabStatus) => {
      this.housingTabStatus = res;
      if (this.housingTabStatus) {
        this.HHHStatus =
          this.housingTabStatus.isHomelessHistoryTabComplete === true ? 1 : 0;
        this.HDStatus =
          this.housingTabStatus.isHousingDocumentsTabComplete === true
            ? 1
            : 0;
      } else {
        this.HHHStatus = 2;
        this.HDStatus = 2;
      }
      this.showHomelessCompleteMessage();
      if (this.pactApplicationID) {
        this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);
      }
    });
  }

  showHomelessCompleteMessage = () => {
    if (this.HHHStatus === 1 && this.tabSelectedIndex === 0) {
      const title = 'Homeless History';
      const primaryMessage = 'The housing/homeless history has been completed.';
      const secondaryMessage =
        'Review to ensure the accuracy of the information.';
      const confirmButtonName = 'OK';
      const dismissButtonName = '';

      this.confirmDialogService.confirmDialog(
        title,
        primaryMessage,
        secondaryMessage,
        confirmButtonName,
        dismissButtonName
      );
    }
  }

  onDocDataChanged = (event: any) => {
    if (this.pactApplicationID > 0) {
      this.getHousingTabStatus();
    }
  }

  onHomelessHistoryChange = (event: any) => {
    if (this.pactApplicationID > 0) {
      this.getHousingTabStatus();
    }
  }

  PreviousPage = () => {
    if (this.tabSelectedIndex === 0) {
      this.sidenavStatusService.routeToPreviousPage(this.router, this.route);
    } else {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }

  NextPage = () => {
    if (this.tabSelectedIndex === 0) {
      this.tabSelectedIndex = this.tabSelectedIndex + 1;
    } else if (this.tabSelectedIndex === 1) {
      this.sidenavStatusService.routeToNextPage(this.router, this.route);
    }
  }

  onPanelChange = (event: boolean) => {
    this.panelExpanded = event;
  }

  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
    if (this.homelessServiceSub) {
      this.homelessServiceSub.unsubscribe();
    }
  }
}
