import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { HousingTabStatus } from './housing-homeless.model';

@Injectable({
  providedIn: 'root'
})
export class HousingHomelessService {

  SERVER_URL = environment.pactApiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  getHousingTabStatus = (PACTApplicationID: number) => {
    const getHousingTabStatusURL = `${this.SERVER_URL}HousingHomeless/GetHousingTabStatus`;
    return this.httpClient.post(
      getHousingTabStatusURL,
      PACTApplicationID,
      this.httpOptions
    );
  }

}
