import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http'
import { PACTADL } from './adls.model'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ADLService {
  saveADLURL = environment.pactApiUrl + 'ADL/SaveApplicantADL';
  getADLURL = environment.pactApiUrl + 'ADL/GetApplicantADL';
  getRefGroupDetailsURL = environment.pactApiUrl + 'Common';
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),     
    response : "json",
     
  };
  
  constructor(private httpClient: HttpClient) { }

   
  saveADL(adl : PACTADL) : Observable<any>
  {
    return this.httpClient.post(this.saveADLURL, JSON.stringify(adl), this.httpOptions);
  }

  getADL<T>(applicationID : number) : Observable<T>
  {
     return this.httpClient.post<T>(this.getADLURL, JSON.stringify(applicationID),this.httpOptions);

    //return this.httpClient.post(this.getADLURL, JSON.stringify(applicationID),observe : 'response' });
  }
}