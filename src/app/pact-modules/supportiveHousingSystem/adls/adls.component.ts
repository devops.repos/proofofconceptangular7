  import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
  import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
  import { RefGroupDetails } from '../../../models/refGroupDetailsDropDown.model';
  import { ADLService } from './adls.service';
  import { Subscription } from 'rxjs';
  import { PACTADL } from './adls.model';
  import { CommonService } from '../../../services/helper-services/common.service';
  import { UserService } from 'src/app/services/helper-services/user.service';
  import { AuthData } from 'src/app/models/auth-data.model';
  import { appStatusColor } from 'src/app/models/pact-enums.enum';
  import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
  import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
  import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
  import { ToastrService } from 'ngx-toastr';
  import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';


  @Component({
    selector: 'app-adls',
    templateUrl: './adls.component.html',
    styleUrls: ['./adls.component.scss']
  })
  export class AdlsComponent implements OnInit, OnDestroy {

     tab1Status :string = "2"; // 0 = Partial; 1 = Complete; 2 = null


    pendingColor = appStatusColor.pendingColor;
    completedColor = appStatusColor.completedColor;
    noColor = appStatusColor.noColor;


    ngOnInit() {


    }


    ngOnDestroy() {

    }


    changeTabStatus(data:any)
    {
      this.tab1Status = data;
      // console.log( this.tab1Status);
    }
  }
