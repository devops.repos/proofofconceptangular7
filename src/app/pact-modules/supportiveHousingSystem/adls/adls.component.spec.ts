import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdlsComponent } from './adls.component';

describe('AdlsComponent', () => {
  let component: AdlsComponent;
  let fixture: ComponentFixture<AdlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
