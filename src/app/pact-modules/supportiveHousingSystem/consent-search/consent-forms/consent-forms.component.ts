import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RefGroupDetails } from '../../../../models/refGroupDetailsDropDown.model';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-consent-forms',
    templateUrl: './consent-forms.html',
    styleUrls: ['./consent-forms.component.scss'],
})

export class ConsentFormsComponent {
    //Global Variables
    private consentFormNamesArray = new Array();
    supportiveHousingConsentForms: RefGroupDetails[];

    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any,
        private dialogRef: MatDialogRef<ConsentFormsComponent>, 
        private message: ToastrService) {
        if (data) {
            this.consentFormNamesArray.splice(0, this.consentFormNamesArray.length);
            this.supportiveHousingConsentForms = data;
        }
    }

    //Close the dialog on close button
    CloseDialog() {
        this.dialogRef.close(true);
    }

    //On Consent Forms Check Change Action method
    onConsentFormsCheckChange(event: { checked: boolean; }, description: string) {
        if (event.checked) {
            this.consentFormNamesArray.push(description);
        }
        else {
            this.consentFormNamesArray.splice(this.consentFormNamesArray.indexOf(description), 1);
        }
    }

    //Open Consent Form method
    openConsentForms() {
        if (this.consentFormNamesArray.length > 0) {
            this.consentFormNamesArray.forEach((element) => {
                this.openDocument(element);
            });
        }
        else {
            if (!this.message.currentlyActive) {
                this.message.error("Please select atleast one language to print.","");
            }
        }
    }
    
    //Open PDF document in a new window with print dialog
    private openDocument(filename: string) {
        const printContent = window.open("./assets/documents/" + filename, "_blank");
        if (printContent != null) {
            printContent.focus();
            printContent.print();
        }
        return false;
    }
}