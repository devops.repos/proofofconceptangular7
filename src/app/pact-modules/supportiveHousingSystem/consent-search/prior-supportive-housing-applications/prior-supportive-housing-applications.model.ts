//Prior Referrals
export interface iPriorReferrals {
    pactApplicationID?: number;
    pactClientID?: number;
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    approvalStatusType?: number;
    referralDate?: string;
    referringAgencySite: string;
    eligibility: string;
    prioritization: string;
    serviceNeeds: string;
    type: string;
    approvalFrom: string;
    approvalTo: string;
    approvalPeriod: string;
    placementAgencySite: string;
    moveInMoveOut: string;
    reasonMoved: string;
}

//Housing Application Supporting Documents - Interface
export interface iHousingApplicationSupportingDocumentsData {
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    approvalExpiryDate: string;
    pactApplicationId: number;
    expandSection: number;
};

//Housing Application Supporting Documents - Class
export class HousingApplicationSupportingDocumentsData {
    agencyNumber: string;
    siteNumber: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    approvalExpiryDate: string;
    pactApplicationId: number;
    expandSection: number;
};

//Prior Supportive Housing Application Dialog Data
export class PriorSupportiveHousingApplicationsDialogData {
    housingApplicationSupportingDocumentsData = new HousingApplicationSupportingDocumentsData();
    pactApplicationId: number;
    isReferralHistory: boolean;
}
