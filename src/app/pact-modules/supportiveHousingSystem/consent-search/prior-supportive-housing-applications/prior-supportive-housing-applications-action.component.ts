import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { MatDialog } from '@angular/material';
import { PriorSupportiveHousingApplicationsDialogComponent } from './prior-supportive-housing-applications-dialog.component';
import { PriorSupportiveHousingApplicationsDialogData } from './prior-supportive-housing-applications.model'

@Component({
  selector: "prior-supportive-housing-applications-action",
  templateUrl: './prior-supportive-housing-applications-action.html',
  styleUrls: ['./prior-supportive-housing-applications-action.component.scss'],
})

export class PriorSupportiveHousingApplicationsActionComponent implements ICellRendererAngularComp {
  params: any;
  cell: any;

  constructor(public dialog: MatDialog) {
  }

  //On Init
  agInit(params: any) {
    this.params = params;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
  }

  refresh(): boolean {
    return false;
  }

  // //Show Attached Documents
  // showAttachDocuments() {
  //   let priorSupportiveHousingApplicationsDialogData = new PriorSupportiveHousingApplicationsDialogData();
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientID;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalTo;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
  //   priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.expandSection = 3;
  //   priorSupportiveHousingApplicationsDialogData.pactApplicationId = 0;
  //   priorSupportiveHousingApplicationsDialogData.isReferralHistory = false;
  //   this.openDialog(priorSupportiveHousingApplicationsDialogData);
  // }

  //Show Application Package
  showApplcationPackage() {
    let priorSupportiveHousingApplicationsDialogData = new PriorSupportiveHousingApplicationsDialogData();
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.agencyNumber = this.params.data.agencyNumber;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.siteNumber = this.params.data.siteNumber;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.firstName = this.params.data.firstName;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.lastName = this.params.data.lastName;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.dob = this.params.data.dob;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.cin = this.params.data.cin;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.ssn = this.params.data.ssn;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.pactClientId = this.params.data.pactClientID;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.approvalExpiryDate = this.params.data.approvalTo;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.pactApplicationId = this.params.data.pactApplicationID;
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData.expandSection = 1;
    priorSupportiveHousingApplicationsDialogData.pactApplicationId = 0;
    priorSupportiveHousingApplicationsDialogData.isReferralHistory = false;
    this.openDialog(priorSupportiveHousingApplicationsDialogData);
  }

  //Show Referral History
  showReferralHistory() {
    let priorSupportiveHousingApplicationsDialogData = new PriorSupportiveHousingApplicationsDialogData();
    priorSupportiveHousingApplicationsDialogData.housingApplicationSupportingDocumentsData = null;
    priorSupportiveHousingApplicationsDialogData.pactApplicationId = this.params.data.pactApplicationID;
    priorSupportiveHousingApplicationsDialogData.isReferralHistory = true;
    this.openDialog(priorSupportiveHousingApplicationsDialogData);
  }

  //Open Housing Application Supporting Documents Dialog
  openDialog(priorSupportiveHousingApplicationsDialogData = new PriorSupportiveHousingApplicationsDialogData()): void {
    this.dialog.open(PriorSupportiveHousingApplicationsDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: priorSupportiveHousingApplicationsDialogData
    });
  }
}
