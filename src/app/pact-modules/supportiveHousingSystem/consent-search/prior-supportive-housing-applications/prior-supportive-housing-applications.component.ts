import { Component, Input } from '@angular/core';
import { ConsentService } from '../consent-search.service';
import { GridOptions } from 'ag-grid-community';
import { PriorSupportiveHousingApplicationsActionComponent } from './prior-supportive-housing-applications-action.component';
import { iPriorReferrals } from './prior-supportive-housing-applications.model'

@Component({
  selector: 'app-prior-supportive-housing-applications',
  templateUrl: './prior-supportive-housing-applications.html',
  styleUrls: ['./prior-supportive-housing-applications.component.scss'],
})

export class PriorSupportiveHousingApplicationsComponent {
  @Input() priorSupportiveHousingApplicationInput: {
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    pactClientSearchAuditId: number;
    optionUserId: number;
  };

  //Variables
  fullName: string;
  defaultColDef = {};
  gridOptions: GridOptions;
  rowData: iPriorReferrals[];
  overlayNoRowsTemplate: string;
  frameworkComponents: any;

  //Grid Columns
  columnDefs = [
    {
      headerName: 'Referral Date',
      field: 'referralDate',
      width: 150,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Referring Agency/Site',
      field: 'referringAgencySite',
      width: 400,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Eligibility',
      field: 'eligibility',
      width: 200,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Prioritization',
      field: 'prioritization',
      width: 150,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Service Needs',
      field: 'serviceNeeds',
      width: 200,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Type',
      field: 'type',
      width: 150,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Approval Period',
      field: 'approvalPeriod',
      width: 200,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Placement Agency/Site',
      field: 'placementAgencySite',
      width: 250,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Move In/Move Out',
      field: 'moveInMoveOut',
      width: 200,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Reason Moved',
      field: 'reasonMoved',
      width: 250,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Action',
      field: 'action',
      width: 60,
      filter: false,
      sortable: false,
      resizable: false,
      pinned: 'left',
      suppressMenu: true,
      suppressSizeToFit: true,
      cellRenderer: 'actionRenderer',
    }
  ];

  //Constructor
  constructor(private consentService: ConsentService) {
    //Default Column Definitions
    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    //Action Component Intialization
    this.frameworkComponents = {
      actionRenderer: PriorSupportiveHousingApplicationsActionComponent
    };
  }

  //On Init
  ngOnInit() {
    if (this.priorSupportiveHousingApplicationInput
      && this.priorSupportiveHousingApplicationInput.lastName != null
      && this.priorSupportiveHousingApplicationInput.firstName != null) {
      this.fullName = this.priorSupportiveHousingApplicationInput.lastName + ", " + this.priorSupportiveHousingApplicationInput.firstName;
    }
  }

  //On Grid Ready
  onGridReady(params: { api: any; columnApi: any; }) {
    params.api.setDomLayout('autoHeight');
    this.overlayNoRowsTemplate = '<span style="color:#337ab7;">No Prior Supportive Housing Applications</span>';
    this.populateGrid();
  }

  //API call to get the prior referrals data
  private populateGrid() {
    if (this.priorSupportiveHousingApplicationInput.pactClientId > 0) {
      this.consentService.getPriorReferrals(this.priorSupportiveHousingApplicationInput.pactClientId)
        .subscribe(res => {
          if (res) {
            const data = res as iPriorReferrals[];
            if (data && data.length > 0) {
              this.rowData = data;
            }
            else {
              this.rowData = null;
            }
          }
          else {
            this.rowData = null;
          }
        });
    }
    else {
      this.rowData = null;
    }
  }
}