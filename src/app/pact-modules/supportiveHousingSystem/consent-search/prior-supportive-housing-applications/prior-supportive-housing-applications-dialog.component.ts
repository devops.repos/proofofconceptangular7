import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PriorSupportiveHousingApplicationsDialogData } from './prior-supportive-housing-applications.model'

@Component({
    selector: 'app-prior-supportive-housing-applications-dialog',
    templateUrl: './prior-supportive-housing-applications-dialog.html',
    styleUrls: ['./prior-supportive-housing-applications-dialog.component.scss']
})

export class PriorSupportiveHousingApplicationsDialogComponent implements OnInit {
    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) public priorSupportiveHousingApplicationsDialogData = new PriorSupportiveHousingApplicationsDialogData(),
        private dialogRef: MatDialogRef<PriorSupportiveHousingApplicationsDialogComponent>) {
    }

    //Close the dialog on close button
    CloseDialog() {
        this.dialogRef.close(true);
    }

    //On Init
    ngOnInit() {
    }
}