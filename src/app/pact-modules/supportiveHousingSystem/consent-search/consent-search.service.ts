import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { ClientSearch, ApplicationData, ConsentSearch, AssessmentApplication } from './consent-search.model'
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ConsentService {
  clientSearchURL = environment.pactApiUrl + 'ConsentSearch/SearchClient';
  getSiteListURL = environment.pactApiUrl + 'ConsentSearch/GetSiteList';
  getRefGroupDetailsURL = environment.pactApiUrl + 'Common';
  priorReferralsURL = environment.pactApiUrl + 'ConsentSearch/GetPriorReferrals';
  create2010eApplicationURL = environment.pactApiUrl + 'ConsentSearch/Create2010eApplication';
  getConsentSearchURL = environment.pactApiUrl + 'ConsentSearch/GetConsentSearch';
  getAssessmentDetailsURL = environment.pactApiUrl + 'ConsentSearch/GetAssessmentDetails';
  createAssessmentApplicationURL = environment.pactApiUrl + 'ConsentSearch/CreateAssessmentApplication';
  isTransmittedApplication = new BehaviorSubject<boolean>(false);

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) {
  }

  //Get Sites List
  getSiteList(LanId: string): Observable<any> {
    return this.httpClient.post(this.getSiteListURL, JSON.stringify(LanId), this.httpOptions);
  }

  //Client Search and Save Client Search Audit 
  searchClient(clientSearch: ClientSearch): Observable<any> {
    return this.httpClient.post(this.clientSearchURL, JSON.stringify(clientSearch), this.httpOptions);
  }

  //Get Prior Referrals
  getPriorReferrals(PACTClientId: number): Observable<any> {
    return this.httpClient.post(this.priorReferralsURL, JSON.stringify(PACTClientId), this.httpOptions);
  }

  //Create new application
  create2010eApplication(applicationData: ApplicationData): Observable<any> {
    return this.httpClient.post(this.create2010eApplicationURL, JSON.stringify(applicationData), this.httpOptions);
  }

  //Get Consent Search
  getConsentSearch(consentSearch: ConsentSearch): Observable<any> {
    return this.httpClient.post(this.getConsentSearchURL, JSON.stringify(consentSearch), this.httpOptions);
  }

  //Get aAsessment Details
  getAssessmentDetails(assessmentId: number): Observable<any> {
    return this.httpClient.post(this.getAssessmentDetailsURL, JSON.stringify(assessmentId), this.httpOptions);
  }

  //Create Assessment Application
  createAssessmentApplication(assessmentApplication: AssessmentApplication): Observable<any> {
    return this.httpClient.post(this.createAssessmentApplicationURL, JSON.stringify(assessmentApplication), this.httpOptions);
  }

  //Set isTransmitted flag
  setIsTransmittedApplication(value: boolean) {
    this.isTransmittedApplication.next(value);
  }

  //Get isTransmitted flag
  getIsTransmittedApplication() {
    return this.isTransmittedApplication.asObservable();
  }
}
