import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ClientSearch, SiteList, ApplicationData, ConsentSearchData, ClientDocumentsData, ConsentSearch, PriorSupportiveHousingApplicationInput, AssessmentApplication } from './consent-search.model'
import * as moment from 'moment';
import { ConsentService } from './consent-search.service';
import { CommonService } from '../../../services/helper-services/common.service';
import { RefGroupDetails } from '../../../models/refGroupDetailsDropDown.model';
import { AuthData } from 'src/app/models/auth-data.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { ConsentFormsComponent } from './consent-forms/consent-forms.component';
import { ToastrService } from 'ngx-toastr';
import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';
import { SocialSecurityPattern } from 'src/app/models/pact-enums.enum';

@Component({
  selector: 'app-consent-search',
  templateUrl: './consent-search.component.html',
  styleUrls: ['./consent-search.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})

//Consent Search Class
export class ConsentSearchComponent implements OnInit, OnDestroy {
  //Global Variables
  selectedSiteType: string = null;
  capsReportId: string = null;
  capsAssessmentId: number = null;
  capsReportURL: SafeResourceUrl = null;
  assessmentDetails: any;
  tabSelectedIndex: number = 0;
  userData: AuthData;
  userDataSub: Subscription;
  consentGroup: FormGroup;
  applicationTypeGroup: FormGroup;
  supportiveHousingTypesArray = new Array();
  isSearch: boolean = false;
  isApplication: boolean = false;
  isConsent: boolean = false;
  isAssessment: boolean = false;
  pactClientId: number = 0;
  pactClientSearchAuditId: number = 0;
  applicationTypeValue: number = 0;
  clientAgeInYears: number;
  clientAgeInMonths: number;
  agencyName: string;
  enteredBy: string;
  consentBy: string;
  housingPrograms: RefGroupDetails[];
  genders: RefGroupDetails[];
  applicationTypes: RefGroupDetails[];
  supportiveHousingTypes: RefGroupDetails[];
  supportiveHousingConsentForms: RefGroupDetails[];
  responseItems: RefGroupDetails[];
  sites: SiteList[];
  now = new Date();
  currentDate = this.datePipe.transform(new Date(), "MM/dd/yyyy hh:mm a");
  consentMinDate = new Date();
  clientSearchData: ClientSearch = { firstName: null, lastName: null, dob: null, cin: null, ssn: null, createdBy: null, genderType: null, agencyId: null, siteId: null };
  applicationData: ApplicationData = { optionUserId: null, age: null, siteId: null, housingProgramType: null, applicationType: null, pactClientId: null, pactClientSearchAuditId: null, consentDate: null, consentLocation: null, supportiveHousingType: null, transmittedApplicationId: 0, assessmentDetails: null };
  clientDocumentsData: ClientDocumentsData = { firstName: null, lastName: null, dob: null, cin: null, ssn: null, pactClientId: 0, pactClientSearchAuditId: 0, optionUserId: null };
  priorSupportiveHousingApplicationInput: PriorSupportiveHousingApplicationInput = { firstName: null, lastName: null, dob: null, cin: null, ssn: null, pactClientId: 0, pactClientSearchAuditId: 0, optionUserId: null };
  pactApplicationId: number;
  isSupportiveHousingTypeDisabled: boolean = false;
  showCreateandClearButton: boolean = true;
  isIah: boolean = false;
  consentSearch: ConsentSearch = { pactApplicationId: 0, optionUserId: 0 };
  assessmentApplication: AssessmentApplication = { assessmentId: null, systemId: null, systemValue: null, systemFlag: null, userId: null, agcyNo: null, siteNo: null, pendingSRNO: null, savestatus: null };
  reportParams: PACTReportUrlParams;
  //reportLoaded: boolean = false;
  isTransmittedApplication: boolean = false;
  dateEntered: string = null;
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  //Masking DOB
  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  //Constructor
  constructor(
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private consentService: ConsentService,
    private commonService: CommonService,
    private userService: UserService,
    public dialog: MatDialog,
    private message: ToastrService,
    private clientApplicationService: ClientApplicationService,
    private router: Router,
    private route: ActivatedRoute,
    private sidenavStatusService: SidenavStatusService,
    private confirmDialogService: ConfirmDialogService,
    private sanitizer: DomSanitizer
  ) {
    //Consent Form Group Form Builder
    this.consentGroup = this.formBuilder.group({
      referringSiteCtrl: ['', Validators.required],
      housingProgramsCtrl: ['', Validators.required],
      isConsentCtrl: ['', Validators.requiredTrue],
      consentDateCtrl: ['', Validators.compose([Validators.required, this.dateValidator])],
      locationKeptCtrl: ['', [Validators.required, this.whitespaceValidator]],
      lastNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      firstNameCtrl: ['', [Validators.required, this.whitespaceValidator]],
      dobCtrl: ['', Validators.required],
      ageCtrl: [''],
      genderCtrl: ['', Validators.required],
      ssnCtrl: ['', [Validators.required, Validators.pattern(SocialSecurityPattern.Pattern)]],
      cinMedicaidCtrl: ['']
    });

    //Application Type Group Form Builder
    this.applicationTypeGroup = this.formBuilder.group({
      applicationTypeCtrl: ['', Validators.required]
    });
  }

  //On Init
  ngOnInit() {
    //Side Navigation Status
    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const paramApplicationId = params.get('applicationID');
      if (paramApplicationId) {
        const numericApplicationId = parseInt(paramApplicationId);
        if (isNaN(numericApplicationId)) {
          throw new Error('Invalid Application Number!');
        } else if (numericApplicationId > 0 && numericApplicationId < 2147483647) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(numericApplicationId);
        }
      } else {
        return;
      }
    });

    //Get User Data
    this.userDataSub = this.userService.getUserData().subscribe(res => {
      if (res) {
        this.userData = res;
        this.agencyName = this.userData.agencyNo + " - " + this.userData.agencyName;
        this.enteredBy = this.userData.firstName + " " + this.userData.lastName;
        this.consentBy = this.userData.firstName + " " + this.userData.lastName;
      }
    });

    //Date Entered
    this.dateEntered = this.currentDate;

    //Get Refgroup Details
    var refGroupList = "4,9,7,10,17,27";
    this.commonService.getRefGroupDetails(refGroupList)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.genders = data.filter(d => { return d.refGroupID === 4 });
          this.housingPrograms = data.filter(d => { return d.refGroupID === 9 });
          this.responseItems = data.filter(d => { return d.refGroupID === 7 && (d.refGroupDetailID === 33 || d.refGroupDetailID === 34) });
          this.applicationTypes = data.filter(d => { return d.refGroupID === 10 });
          this.supportiveHousingTypes = data.filter(d => { return d.refGroupID === 17 });
          this.supportiveHousingConsentForms = data.filter(d => { return d.refGroupID === 27 });
        },
        error => {
          throw new Error(error.message);
        }
      );

    //Get Sites List
    this.consentService.getSiteList(this.userData.lanId)
      .subscribe(
        response => {
          const data = response as SiteList[];
          this.sites = data;
        }, error => {
          throw new Error(error.message);
        }
      );

    //Setting The Min Date For Consent Date
    this.consentMinDate.setDate(this.now.getDate() - 181);

    //Default Value For Housing Program.
    this.consentGroup.controls['housingProgramsCtrl'].setValue(44);

    //Get Pact Application ID From Router Parameter
    this.route.paramMap.subscribe(params => {
      const selectedApplicationID = +params.get('applicationID');
      if (selectedApplicationID) {
        this.pactApplicationId = selectedApplicationID;
        this.getConsentSearch(this.pactApplicationId, this.userData.optionUserId);
      }
    });

    //Get Assessment ID From Router Parameter
    this.route.paramMap.subscribe(params => {
      const assessmentId = +params.get('assessmentId');
      if (assessmentId) {
        //Get Assessment Details
        this.consentService.getAssessmentDetails(assessmentId).subscribe(response => {
          if (response) {
            this.assessmentDetails = response;
            this.populateConsentSearchFormFromAssessment();
          }
          else {
            return;
          }
        }, error => {
          throw new Error(error.message);
        }
        );
      }
    });

    //set the local isTransmittedApplication value from the service
    this.consentService.getIsTransmittedApplication().subscribe(res => {
      this.isTransmittedApplication = res;
    })
  }

  //Destroy
  ngOnDestroy() {
    if (this.userDataSub) {
      this.userDataSub.unsubscribe();
    }
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
  }

  //Open Consent Forms Popup Dialog
  openConsentFormsDialog(): void {
    this.dialog.open(ConsentFormsComponent, {
      width: '25%',
      disableClose: true,
      data: this.supportiveHousingConsentForms
    });
  }

  //Calculate Age Of The Client In Years and Months
  calculateAge() {
    if (this.consentGroup.get('dobCtrl').value && !moment(this.consentGroup.get('dobCtrl').value, 'MM/DD/YYYY', true).isValid()) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid Date of Birth.");
      }
      this.consentGroup.controls['dobCtrl'].setValue("");
      return;
    }
    if (this.consentGroup.get('dobCtrl').value) {
      const starts = moment(this.consentGroup.get('dobCtrl').value, 'MM/DD/YYYY');
      const ends = moment(Date.now());
      const years = ends.diff(starts, 'year');
      starts.add(years, 'years');
      const months = ends.diff(starts, 'months');
      starts.add(months, 'months');
      this.clientAgeInYears = years;
      this.clientAgeInMonths = months;
    }
  }

  //Validate White Space
  whitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  //Validate Date
  dateValidator(AC: AbstractControl) {
    if (AC && AC.value && !moment(AC.value, 'MM/DD/YYYY', true).isValid()) {
      return { 'dateValidator': true };
    }
    return null;
  }

  //Next Button Click
  nextTab() {
    this.tabSelectedIndex = this.tabSelectedIndex + 1;
  }

  //Previous Button Click
  previousTab() {
    if (this.tabSelectedIndex != 0) {
      this.tabSelectedIndex = this.tabSelectedIndex - 1;
    }
  }

  //On Consent Check Change
  onIsConsentCheckChange(event: { checked: Boolean }) {
    if (event.checked) {
      this.isConsent = true;
    }
    else {
      this.isConsent = false;
    }
  }

  //on Referring Site Change Action Event
  onReferringSiteChange() {
    const selected = this.consentGroup.get("referringSiteCtrl").value;
    if (selected) {
      if (this.isCapsSurveyMandate(selected)) {
        this.confirmDialogService.confirmDialog(
          'CAPS Survey Mandate',
          'A CAPS survey is required for the Application.',
          'Please complete a CAPS survey before starting the Application.',
          'Ok',
          ''
        );
      }
    }
  }

  //Is Caps Survey Mandate To Create Application
  isCapsSurveyMandate(targetedSiteId: number) {
    if (this.sites && this.sites.length > 0) {
      const selectedSite = this.sites.find(({ siteId }) => siteId === targetedSiteId);
      if (selectedSite) {
        if (selectedSite.isCapsMandate && !this.isAssessment) {
          return true;
        }
      }
    }
    return false;
  }

  //On Consent Forms Check Change Action Method
  onSupportiveHousingTypesCheckChange(event: { checked: boolean; }, refGroupDetailId: number) {
    if (event.checked) {
      if (refGroupDetailId === 63) {
        this.removeItem(64);
      }
      if (refGroupDetailId === 64) {
        this.removeItem(63);
      }
      if (refGroupDetailId === 69) {
        this.removeItem(70);
      }
      if (refGroupDetailId === 70) {
        this.removeItem(69);
      }
      this.supportiveHousingTypesArray.push(refGroupDetailId);
    }
    else {
      this.removeItem(refGroupDetailId);
    }
  }

  //Remove Item From SupportiveHousingTypesArray
  removeItem(refId: number) {
    const index: number = this.supportiveHousingTypesArray.indexOf(refId);
    if (index !== -1) {
      this.supportiveHousingTypesArray.splice(index, 1);
    }
  }

  //Filered Supportive Housing Type
  get filteredSupportiveHousingType() {
    if (this.selectedSiteType) {
      if (this.selectedSiteType === '10' || this.selectedSiteType === '50' || this.selectedSiteType === '84' || this.selectedSiteType === '85') {
        if (this.clientAgeInYears > 25) {
          return this.supportiveHousingTypes.filter(s => s.parentRefGroupDetailID === this.applicationTypeValue && s.refGroupDetailID !== 66 && s.refGroupDetailID !== 67 && s.refGroupDetailID !== 71);
        }
        else {
          return this.supportiveHousingTypes.filter(s => s.parentRefGroupDetailID === this.applicationTypeValue);
        }
      }
      else {
        if (this.clientAgeInYears > 25) {
          return this.supportiveHousingTypes.filter(s => s.parentRefGroupDetailID === this.applicationTypeValue && s.refGroupDetailID !== 66 && s.refGroupDetailID !== 67 && s.refGroupDetailID !== 68 && s.refGroupDetailID !== 71);
        }
        else {
          return this.supportiveHousingTypes.filter(s => s.parentRefGroupDetailID === this.applicationTypeValue && s.refGroupDetailID !== 68);
        }
      }
    }
    else {
      return this.supportiveHousingTypes.filter(s => s.parentRefGroupDetailID === this.applicationTypeValue);
    }
  }

  //On Application Type Selection Change
  onApplicationTypeChange(event: { value: number; }) {
    if (event.value > 0) {
      while (this.supportiveHousingTypesArray.length !== 0) {
        this.supportiveHousingTypesArray.splice(0);
      }
      this.applicationTypeValue = event.value;
    }
  }

  //Clear Search Action Method
  clearSearch() {
    this.confirmDialogService.confirmDialog('Confirm Delete', '', 'Are you sure you want to clear search?', 'Yes', 'No')
      .then(() => {
        this.isAssessment = false;
        this.assessmentDetails = null;
        this.capsReportId = null;
        this.pactClientId = 0;
        this.isSearch = false;
        this.isTransmittedApplication = false;
        this.consentGroup.enable();
        while (this.supportiveHousingTypesArray.length !== 0) {
          this.supportiveHousingTypesArray.splice(0);
        }
        this.applicationTypeValue = 0;
        this.applicationTypeGroup.reset();
        this.applicationTypeGroup.enable();
        this.isIah = false;
        //Reset Client Search data
        this.consentGroup.controls['firstNameCtrl'].reset();
        this.consentGroup.controls['lastNameCtrl'].reset();
        this.consentGroup.controls['ssnCtrl'].reset();
        this.consentGroup.controls['dobCtrl'].reset();
        this.consentGroup.controls['ageCtrl'].reset();
        this.clientAgeInYears = null;
        this.consentGroup.controls['genderCtrl'].reset();
        this.consentGroup.controls['cinMedicaidCtrl'].reset();
        this.clientSearchData.firstName = null;
        this.clientSearchData.lastName = null;
        this.clientSearchData.ssn = null;
        this.clientSearchData.dob = null;
        this.clientSearchData.genderType = null;
        this.clientSearchData.cin = null;
        //Reset Clients Document Data
        this.clientDocumentsData.firstName = null;
        this.clientDocumentsData.lastName = null;;
        this.clientDocumentsData.dob = null;
        this.clientDocumentsData.cin = null;
        this.clientDocumentsData.ssn = null;
        this.clientDocumentsData.optionUserId = 0;
        this.clientDocumentsData.pactClientId = 0;
        this.clientDocumentsData.pactClientSearchAuditId = 0;
        //Reset Prior Supportive Housing Application Data
        this.priorSupportiveHousingApplicationInput.firstName = null;
        this.priorSupportiveHousingApplicationInput.lastName = null;
        this.priorSupportiveHousingApplicationInput.dob = null;
        this.priorSupportiveHousingApplicationInput.cin = null;
        this.priorSupportiveHousingApplicationInput.ssn = null;
        this.priorSupportiveHousingApplicationInput.optionUserId = 0;
        this.priorSupportiveHousingApplicationInput.pactClientId = 0;
        this.priorSupportiveHousingApplicationInput.pactClientSearchAuditId = 0;
      }, () => {
      });
  }

  //Validate Client Age
  validateAge() {
    if (this.clientAgeInYears < 17) {
      if (!this.message.currentlyActive) {
        this.message.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
      }
      return false;
    }
    if (this.clientAgeInYears === 17 && this.clientAgeInMonths < 8) {
      if (!this.message.currentlyActive) {
        this.message.error("Applicant must be at least 17 years and 8 months of age to apply for supportive housing.");
      }
      return false;
    }
    if (this.clientAgeInYears > 119) {
      if (!this.message.currentlyActive) {
        this.message.error("Client is over 119 years old. Please check the DOB or Age.");
      }
      return false;
    }
    return true;
  }

  //Client Search Action Method
  clientSearch() {
    if (this.consentGroup.valid) {
      if (!this.validateAge()) {
        return;
      }
      this.pactClientId = 0;
      this.clientSearchData.firstName = this.consentGroup.get('firstNameCtrl').value.trim();
      this.clientSearchData.lastName = this.consentGroup.get('lastNameCtrl').value.trim();
      this.clientSearchData.dob = this.datePipe.transform(this.consentGroup.get('dobCtrl').value, 'MM/dd/yyyy');
      this.clientSearchData.genderType = this.consentGroup.get('genderCtrl').value;
      this.clientSearchData.agencyId = this.userData.agencyId;
      this.clientSearchData.siteId = this.consentGroup.get('referringSiteCtrl').value;
      this.clientSearchData.ssn = this.consentGroup.get('ssnCtrl').value.trim();
      this.clientSearchData.cin = this.consentGroup.get('cinMedicaidCtrl').value == null ? "" : this.consentGroup.get('cinMedicaidCtrl').value.trim();
      this.clientSearchData.createdBy = this.userData.optionUserId;
      //Iah
      if (this.sites && this.sites.length > 0) {
        this.selectedSiteType = this.sites.find(({ siteId }) => siteId === this.consentGroup.get('referringSiteCtrl').value).siteType;
      }
      if (this.selectedSiteType && this.selectedSiteType === '80') {
        this.applicationTypeGroup.controls['applicationTypeCtrl'].setValue(46);
        this.applicationTypeValue = 46;
        this.supportiveHousingTypesArray.push(62);
        this.applicationTypeGroup.disable();
        this.isIah = true;
      }
      //Client Documents Data
      this.clientDocumentsData.firstName = this.consentGroup.get('firstNameCtrl').value.trim();
      this.clientDocumentsData.lastName = this.consentGroup.get('lastNameCtrl').value.trim();
      this.clientDocumentsData.dob = this.datePipe.transform(this.consentGroup.get('dobCtrl').value, 'MM/dd/yyyy');
      this.clientDocumentsData.cin = this.consentGroup.get('cinMedicaidCtrl').value == null ? "" : this.consentGroup.get('cinMedicaidCtrl').value.trim();
      this.clientDocumentsData.ssn = this.consentGroup.get('ssnCtrl').value.trim();
      this.clientDocumentsData.optionUserId = this.userData.optionUserId
      //Prior Supportive Housing Application Input
      this.priorSupportiveHousingApplicationInput.firstName = this.consentGroup.get('firstNameCtrl').value.trim();
      this.priorSupportiveHousingApplicationInput.lastName = this.consentGroup.get('lastNameCtrl').value.trim();
      this.priorSupportiveHousingApplicationInput.dob = this.datePipe.transform(this.consentGroup.get('dobCtrl').value, 'MM/dd/yyyy');
      this.priorSupportiveHousingApplicationInput.cin = this.consentGroup.get('cinMedicaidCtrl').value == null ? "" : this.consentGroup.get('cinMedicaidCtrl').value.trim();
      this.priorSupportiveHousingApplicationInput.ssn = this.consentGroup.get('ssnCtrl').value.trim();
      this.priorSupportiveHousingApplicationInput.optionUserId = this.userData.optionUserId;
      //Api Call
      this.consentService.searchClient(this.clientSearchData)
        .subscribe(
          response => {
            if (response) {
              if (response.pactClientId > 0) {
                this.pactClientId = response.pactClientId;
                this.clientDocumentsData.pactClientId = response.pactClientId;
                this.priorSupportiveHousingApplicationInput.pactClientId = response.pactClientId;
              }
              if (response.pactClientSearchAuditId > 0) {
                this.pactClientSearchAuditId = response.pactClientSearchAuditId;
                this.clientDocumentsData.pactClientSearchAuditId = response.pactClientSearchAuditId;
                this.priorSupportiveHousingApplicationInput.pactClientSearchAuditId = response.pactClientSearchAuditId;
                this.isSearch = true;
                this.consentGroup.disable();
                this.tabSelectedIndex = (this.tabSelectedIndex + 1);
              }
              if (response.cin != null) {
                this.clientSearchData.cin = response.cin;
                this.consentGroup.controls['cinMedicaidCtrl'].setValue(response.cin);
              }
            }
            else {
              this.message.error("Error in inserting record in pact client search audit table!");
            }
          }, error => {
            throw new Error(error.message);
          }
        );
    }
    else {
      this.validationMessage();
    }
  }

  //Create New Application Action Method
  createApplication() {
    if (this.isCapsSurveyMandate(this.consentGroup.get("referringSiteCtrl").value)) {
      if (!this.message.currentlyActive) {
        this.message.error("A CAPS survey is required for the Application. Please complete a CAPS survey before starting the Application.");
      }
      return;
    }
    if (!this.applicationTypeGroup.get('applicationTypeCtrl').invalid) {
      if (this.supportiveHousingTypesArray.length > 0) {
        this.applicationData.optionUserId = this.userData.optionUserId;
        this.applicationData.age = this.clientAgeInYears;
        this.applicationData.siteId = this.consentGroup.get("referringSiteCtrl").value;
        this.applicationData.housingProgramType = this.consentGroup.get("housingProgramsCtrl").value;
        this.applicationData.applicationType = this.applicationTypeGroup.get("applicationTypeCtrl").value;
        this.applicationData.pactClientId = this.pactClientId;
        this.applicationData.pactClientSearchAuditId = this.pactClientSearchAuditId;
        this.applicationData.consentDate = this.consentGroup.get("consentDateCtrl").value;
        this.applicationData.consentLocation = this.consentGroup.get("locationKeptCtrl").value.trim();
        this.applicationData.supportiveHousingType = this.supportiveHousingTypesArray.join(',');
        if (this.isTransmittedApplication) {
          this.applicationData.transmittedApplicationId = this.pactApplicationId;
        }
        if (this.isAssessment && this.assessmentDetails) {
          this.applicationData.assessmentDetails = this.assessmentDetails;
        }
        //Api Call
        this.consentService.create2010eApplication(this.applicationData)
          .subscribe(
            response => {
              if (response.pactApplicationId < 0 && response.validationMessage != null) {
                this.message.error(response.validationMessage);
              }
              else if (response.pactApplicationId > 0) {
                this.clientApplicationService.setClientApplicationData(response);
                this.sidenavStatusService.setIsNewApplicationFlag();
                this.pactApplicationId = response.pactApplicationId;
                this.isApplication = true;
                if (this.isAssessment && this.assessmentDetails) {
                  if (this.assessmentDetails.assessment) {
                    this.createAssessmentApplication(this.assessmentDetails.assessment.assessmentID, response.pactApplicationId, response.pactApplicationId, this.consentGroup.get("referringSiteCtrl").value);
                  }
                }
                if (this.isAssessment && this.isTransmittedApplication && this.capsAssessmentId && this.applicationData.transmittedApplicationId) {
                  this.createAssessmentApplication(this.capsAssessmentId, response.pactApplicationId, this.applicationData.transmittedApplicationId, this.consentGroup.get("referringSiteCtrl").value);
                }
                //this.consentService.setIsTransmittedApplication(false);
                this.nextTab();
                this.getConsentSearch(this.pactApplicationId, this.userData.optionUserId);
              }
              else {
                throw new Error("Error in creating application details!");
              }
            }, error => {
              throw new Error(error.message);
            }
          );
      }
      else {
        if (!this.message.currentlyActive) {
          this.message.error("Supportive Housing Type is required.");
        }
      }
    } else {
      if (!this.message.currentlyActive) {
        this.message.error("Application Type is required.");
      }
    }
  }

  //Validation Message
  validationMessage() {
    if (this.consentGroup.get('referringSiteCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Referring Site is required.");
      }
    }
    else if (this.consentGroup.get('housingProgramsCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Housing Program is required.");
      }
    }
    else if (this.consentGroup.get('isConsentCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Consent is required.");
      }
    }
    else if (this.consentGroup.get('consentDateCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Consent Date is required.");
      }
    }
    else if (this.consentGroup.get('locationKeptCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Consent Location is required.");
      }
    }
    else if (this.consentGroup.get('firstNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("First Name is required.");
      }
    }
    else if (this.consentGroup.get('lastNameCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Last Name is required.");
      }
    }
    else if (this.consentGroup.get('ssnCtrl').errors.required) {
      if (!this.message.currentlyActive) {
        this.message.error("SSN is required.");
      }
    }
    else if (this.consentGroup.get('ssnCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Invalid SSN.");
      }
    }
    else if (this.consentGroup.get('dobCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Date of Birth is required.");
      }
    }
    else if (this.consentGroup.get('genderCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("Gender is required.");
      }
    }
    else if (this.consentGroup.get('cinMedicaidCtrl').invalid) {
      if (!this.message.currentlyActive) {
        this.message.error("CIN / Medicaid # is formatted as follows: Two alpha characters followed by five numeric characters and then followed by one alpha character; e.g: AA12345A");
      }
    }
  }

  //Get Consent Search
  getConsentSearch(pactApplicationId: number, optionUserId: number) {
    this.consentSearch.pactApplicationId = pactApplicationId;
    this.consentSearch.optionUserId = optionUserId;
    //Api Call to get the consent search
    this.consentService.getConsentSearch(this.consentSearch).subscribe(res => {
      const data = res as ConsentSearchData;
      if (data) {
        if (data.pactApplicationId < 0 && data.validationMessage != null) {
          throw new Error(data.validationMessage);
        }
        else if (data.isTransmitted) {
          this.isTransmittedApplication = true;
          this.consentService.setIsTransmittedApplication(true);
          this.populateConsentSearchForm(data);
          this.consentBy = this.userData.firstName + " " + this.userData.lastName;
          this.consentGroup.controls['referringSiteCtrl'].disable();
          this.consentGroup.controls['housingProgramsCtrl'].disable();
          this.consentGroup.controls['firstNameCtrl'].disable();
          this.consentGroup.controls['lastNameCtrl'].disable();
          this.consentGroup.controls['ssnCtrl'].disable();
          this.consentGroup.controls['dobCtrl'].disable();
          this.consentGroup.controls['ageCtrl'].disable();
          this.consentGroup.controls['genderCtrl'].disable();
          this.consentGroup.controls['cinMedicaidCtrl'].disable();
        }
        else {
          this.isSearch = true;
          this.consentGroup.disable();
          this.applicationTypeGroup.disable();
          this.isSupportiveHousingTypeDisabled = true;
          this.showCreateandClearButton = false;
          this.isConsent = true;
          this.isApplication = true;
          this.populateConsentSearchForm(data);
        }
      }
      else {
        return;
      }
    }, error => {
      throw new Error(error.message);
    }
    );
  }

  //Populate Consent Search Form Objects
  populateConsentSearchForm(consentSearchData: ConsentSearchData) {
    //Populate Client Search data object
    this.clientSearchData.firstName = consentSearchData.firstName;
    this.clientSearchData.lastName = consentSearchData.lastName;
    this.clientSearchData.dob = this.datePipe.transform(consentSearchData.dob, 'MM/dd/yyyy');
    this.clientSearchData.genderType = consentSearchData.genderType;
    this.clientSearchData.siteId = consentSearchData.siteId;
    this.clientSearchData.ssn = consentSearchData.ssn;
    this.clientSearchData.cin = consentSearchData.cin;
    //Populate Client Documents data
    this.clientDocumentsData.firstName = consentSearchData.firstName;
    this.clientDocumentsData.lastName = consentSearchData.lastName;
    this.clientDocumentsData.dob = this.datePipe.transform(consentSearchData.dob, 'MM/dd/yyyy');
    this.clientDocumentsData.cin = consentSearchData.cin === null ? "" : consentSearchData.cin;
    this.clientDocumentsData.ssn = consentSearchData.ssn;
    this.clientDocumentsData.pactClientId = consentSearchData.pactClientId;
    this.clientDocumentsData.optionUserId = this.userData.optionUserId;
    this.clientDocumentsData.pactClientSearchAuditId = this.pactClientSearchAuditId;
    //Prior Supportive Housing Application Input
    this.priorSupportiveHousingApplicationInput.firstName = consentSearchData.firstName;
    this.priorSupportiveHousingApplicationInput.lastName = consentSearchData.lastName;
    this.priorSupportiveHousingApplicationInput.dob = this.datePipe.transform(consentSearchData.dob, 'MM/dd/yyyy');
    this.priorSupportiveHousingApplicationInput.cin = consentSearchData.cin;
    this.priorSupportiveHousingApplicationInput.ssn = consentSearchData.ssn;
    this.priorSupportiveHousingApplicationInput.pactClientId = consentSearchData.pactClientId;
    this.priorSupportiveHousingApplicationInput.optionUserId = this.userData.optionUserId;
    this.priorSupportiveHousingApplicationInput.pactClientSearchAuditId = this.pactClientSearchAuditId;
    //Populate form objects
    this.enteredBy = consentSearchData.enteredBy;
    this.consentBy = consentSearchData.enteredBy;
    this.dateEntered = consentSearchData.dateEntered;
    this.pactClientId = consentSearchData.pactClientId;
    if (consentSearchData.capsReportId && consentSearchData.capsAssessmentId) {
      this.isAssessment = true;
      this.capsReportId = consentSearchData.capsReportId;
      this.capsAssessmentId = consentSearchData.capsAssessmentId;
    }
    if (this.sites && this.sites.length > 0) {
      this.selectedSiteType = this.sites.find(({ siteId }) => siteId === consentSearchData.siteId).siteType;
    }
    this.consentGroup.controls['referringSiteCtrl'].setValue(consentSearchData.siteId);
    this.consentGroup.controls['housingProgramsCtrl'].setValue(consentSearchData.housingProgramType);
    if (!consentSearchData.isTransmitted) {
      this.consentGroup.controls['isConsentCtrl'].setValue(consentSearchData.isConsent);
      this.consentGroup.controls['consentDateCtrl'].setValue(consentSearchData.consentStartDate);
      this.consentGroup.controls['locationKeptCtrl'].setValue(consentSearchData.consentLocation);
    }
    this.consentGroup.controls['lastNameCtrl'].setValue(consentSearchData.lastName);
    this.consentGroup.controls['firstNameCtrl'].setValue(consentSearchData.firstName);
    this.consentGroup.controls['dobCtrl'].setValue(consentSearchData.dob);
    this.consentGroup.controls['ageCtrl'].setValue(consentSearchData.age);
    this.clientAgeInYears = consentSearchData.age;
    this.consentGroup.controls['genderCtrl'].setValue(consentSearchData.genderType);
    this.consentGroup.controls['ssnCtrl'].setValue(consentSearchData.ssn);
    this.consentGroup.controls['cinMedicaidCtrl'].setValue(consentSearchData.cin);
    this.applicationTypeGroup.controls['applicationTypeCtrl'].setValue(consentSearchData.applicationType);
    this.applicationTypeValue = consentSearchData.applicationType;
    consentSearchData.supportiveHousing.forEach((element) => {
      this.supportiveHousingTypesArray.push(element.supportiveHousingType);
    });
    return;
  }

  //Navigate To Demographics Page.
  navigateToDemographics() {
    if (this.pactApplicationId) {
      this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationId);
    }
    this.consentService.setIsTransmittedApplication(false);
    this.router.navigate(['/shs/newApp/demographics', this.pactApplicationId]);
  }

  //Populate Consent Search Form From Assessment
  populateConsentSearchFormFromAssessment() {
    if (this.assessmentDetails && this.assessmentDetails.assessment) {
      this.isAssessment = true;
      this.capsReportId = this.assessmentDetails.assessment.reportID;
      if (this.sites && this.sites.length > 0) {
        const surveySelectedSite = this.sites.find(({ siteNo, name }) => siteNo + ' - ' + name === this.assessmentDetails.assessment.site);
        if (surveySelectedSite) {
          this.consentGroup.controls['referringSiteCtrl'].setValue(surveySelectedSite.siteId);
          this.consentGroup.controls['referringSiteCtrl'].disable();
        }
        else {
          this.consentGroup.controls['referringSiteCtrl'].reset();
          this.consentGroup.controls['referringSiteCtrl'].enable();
        }
      }
      this.consentGroup.controls['firstNameCtrl'].setValue(this.assessmentDetails.assessment.firstName);
      this.consentGroup.controls['lastNameCtrl'].setValue(this.assessmentDetails.assessment.lastName);
      this.consentGroup.controls['ssnCtrl'].setValue(this.assessmentDetails.assessment.ssn);
      this.consentGroup.controls['dobCtrl'].setValue(moment(this.assessmentDetails.assessment.dob).format('MM/DD/YYYY'));
      this.consentGroup.controls['ageCtrl'].setValue(this.assessmentDetails.assessment.age);
      this.clientAgeInYears = this.assessmentDetails.assessment.age;
      if (this.genders && this.genders.length > 0) {
        this.consentGroup.controls['genderCtrl'].setValue(this.genders.find(({ refGroupDetailDescription }) => refGroupDetailDescription === this.assessmentDetails.assessment.gender).refGroupDetailID);
      }
      this.consentGroup.controls['cinMedicaidCtrl'].setValue(this.assessmentDetails.assessment.cin);
      if (this.assessmentDetails.demographic) {
        if (this.assessmentDetails.demographic.householdCompositionId === 102) {
          if (this.assessmentDetails.demographic.isPregnant === 'Y') {
            this.applicationTypeGroup.controls['applicationTypeCtrl'].setValue(47);
            this.applicationTypeValue = 47;
          }
          else {
            this.applicationTypeGroup.controls['applicationTypeCtrl'].setValue(46);
            this.applicationTypeValue = 46;
          }
        }
        else if (this.assessmentDetails.demographic.householdCompositionId === 103 || this.assessmentDetails.demographic.householdCompositionId === 104) {
          this.applicationTypeGroup.controls['applicationTypeCtrl'].setValue(47);
          this.applicationTypeValue = 47;
        }
      }
      this.consentGroup.controls['housingProgramsCtrl'].disable();
      this.consentGroup.controls['firstNameCtrl'].disable();
      this.consentGroup.controls['lastNameCtrl'].disable();
      if (this.assessmentDetails.assessment.ssn !== "") {
        this.consentGroup.controls['ssnCtrl'].disable();
      }
      this.consentGroup.controls['dobCtrl'].disable();
      this.consentGroup.controls['ageCtrl'].disable();
      this.consentGroup.controls['genderCtrl'].disable();
      this.consentGroup.controls['cinMedicaidCtrl'].disable();
      this.applicationTypeGroup.controls['applicationTypeCtrl'].disable();
    }
    if (this.assessmentDetails && this.assessmentDetails.potentialEligibleInfo && this.assessmentDetails.potentialEligibleInfo.length > 0) {
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => (x.refGroupDetailID === 6) || (x.refGroupDetailID === 7) || (x.refGroupDetailID === 35) || (x.refGroupDetailID === 36))) {
        this.supportiveHousingTypesArray.push(62);
      }
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => x.refGroupDetailID === 37)) {
        this.supportiveHousingTypesArray.push(66);
      }
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => x.refGroupDetailID === 38)) {
        this.supportiveHousingTypesArray.push(69);
      }
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => x.refGroupDetailID === 39)) {
        this.supportiveHousingTypesArray.push(63);
      }
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => x.refGroupDetailID === 40)) {
        this.supportiveHousingTypesArray.push(64);
      }
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => x.refGroupDetailID == 41)) {
        this.supportiveHousingTypesArray.push(70);
      }
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => x.refGroupDetailID === 42)) {
        this.supportiveHousingTypesArray.push(65);
      }
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => (x.refGroupDetailID === 43) || (x.refGroupDetailID === 193))) {
        this.supportiveHousingTypesArray.push(67);
      }
      if (this.assessmentDetails.potentialEligibleInfo.find((x: { refGroupDetailID: number; }) => x.refGroupDetailID === 201)) {
        this.supportiveHousingTypesArray.push(68);
      }
    }
  }

  //Create Assessment Application
  createAssessmentApplication(assessmentID: number, pactApplicationId: number, pendingApplicationID: number, siteId: number) {
    this.assessmentApplication.assessmentId = assessmentID.toString();
    this.assessmentApplication.systemId = 131;
    this.assessmentApplication.systemValue = pactApplicationId.toString();
    this.assessmentApplication.systemFlag = "P";
    this.assessmentApplication.userId = this.userData.lanId;
    this.assessmentApplication.agcyNo = this.userData.agencyNo;
    this.assessmentApplication.siteNo = this.sites.filter(s => s.siteId === siteId)[0].siteNo;
    this.assessmentApplication.pendingSRNO = pendingApplicationID.toString();
    this.consentService.createAssessmentApplication(this.assessmentApplication).subscribe(res => {
      if (!res.isUpdated) {
        throw new Error("Unable to create assessment application.");
      }
    });
  }

  //Tab Change Event
  consentSearchTabChange() {
    if (this.tabSelectedIndex === 1 && this.isAssessment && this.capsReportId) {
      this.capsReportURL = null;
      this.displaySurveyReport(this.capsReportId);
    }
  }

  //Display Survey Report
  displaySurveyReport(capsReportId: string) {
    this.reportParams = { reportParameterID: capsReportId, reportName: "AssessmentSurveyReport", "reportFormat": "PDF" };
    this.commonService.generateReport(this.reportParams)
      .subscribe(
        res => {
          if (res) {
            var data = new Blob([res.body], { type: 'application/pdf' });
            if (data.size > 512) {
              this.capsReportURL = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(data));
            }
          }
        },
        error => {
          throw new Error("Unable to generate report due to invalid survey number!");
        }
      );
  }

  //Get Site For ToolTip
  getSiteForToolTip() {
    const selected = this.consentGroup.get("referringSiteCtrl").value;
    if (selected) {
      if (this.sites && this.sites.length > 0) {
        const selectedSite = this.sites.find(({ siteId }) => siteId === selected);
        if (selectedSite) {
          return selectedSite.siteNo + " - " + selectedSite.name;
        }
      }
    }
  }
}
