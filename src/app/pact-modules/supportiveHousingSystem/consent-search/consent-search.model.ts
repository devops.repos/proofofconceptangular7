export interface ClientSearch {
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  genderType: number;
  cin: string;
  agencyId: number;
  siteId: number;
  createdBy: number;
};

export interface SiteList {
  siteId: number;
  siteNo: string;
  name: string;
  agencyId: number;
  siteType: string;
  isCapsMandate: boolean;
  isActive: boolean;
  id: number;
};

export interface ApplicationData {
  optionUserId: number;
  age: number;
  siteId: number;
  housingProgramType: number;
  applicationType: number;
  pactClientId: number;
  pactClientSearchAuditId: number;
  consentDate: Date;
  consentLocation: string;
  supportiveHousingType: string;
  transmittedApplicationId: number;
  assessmentDetails: any;
};

export interface ConsentSearchData {
  pactApplicationId: number;
  siteId: number;
  siteNo: string;
  housingProgramType: number;
  isConsent: boolean;
  consentType: number;
  consentStartDate: string;
  consentLocation: string;
  firstName: string;
  lastName: string;
  ssn: string;
  dob: string;
  age: number;
  genderType: number;
  cin: string;
  applicationType: number;
  enteredBy: string;
  pactClientId: number;
  capsReportId: string;
  capsAssessmentId: number;
  isTransmitted: boolean;
  dateEntered: string;
  supportiveHousing: SupportiveHousing[];
  validationMessage: string;
};

export interface SupportiveHousing {
  supportiveHousingType: number;
};

export interface ClientDocumentsData {
  firstName: string;
  lastName: string;
  dob: string;
  cin: string;
  ssn: string;
  pactClientId: number;
  pactClientSearchAuditId: number;
  optionUserId: number;
};

export interface ConsentSearch {
  pactApplicationId: number;
  optionUserId: number;
};

export interface PriorSupportiveHousingApplicationInput {
  firstName: string;
  lastName: string;
  dob: string;
  cin: string;
  ssn: string;
  pactClientId: number;
  pactClientSearchAuditId: number;
  optionUserId: number;
};

export interface AssessmentApplication {
  assessmentId: string;
  systemId: number;
  systemValue: string
  systemFlag: string;
  userId: string;
  agcyNo: string;
  siteNo: string;
  pendingSRNO: string;
  savestatus: string;
};