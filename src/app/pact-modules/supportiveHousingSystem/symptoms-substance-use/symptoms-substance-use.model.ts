
export interface PACTSymtomsAndSubstance {
    pactSymptomsSubstanceID: number;
    pactApplicationID: number;
    homicidal: number;
    suicidal: number;
    violent: number;
    disruptiveBehavior: number;
    criminalActivity: number;
    arson: number;
    cognitiveImpairment: number;
    hallucinations: number;
    delusions: number;
    thoughtDisorders: number;
    clinicalDepression: number;
    mania: number;
    substanceUsedRecently: boolean;
    substanceUsedRecentlyPatternType: number;
    substanceUsedRecentOtherSpecify: string;
    substanceUsedInPast: boolean;
    substanceUsedInPastPatternType: number;
    substanceUsedInPastOtherSpecify: string;
    pastAlcoholSobriety: number;
    pastAlcoholSobrietyDate: string;
    pastDrugSobriety: number;
    pastDrugSobrietyDate: string;
    isCurrentlyTreatmentProgram: boolean;
    currentTreatmentProgramName: string;
    currentTreatmentProgramModalityType: number;
    currentTreatmentProgramAdmissionDate: string;
    isActivelyParticipating: boolean;
    explainNonParticipation: string;
    hasCompletedTreatmentProgram: boolean;
    completedProgramName: string;
    completedTreatmentProgramModalityType: number;
    completedProgramAdmissionDate: string;
    isSymptomsBehaviorsTabComplete: boolean;
    isSubstanceUseTabComplete: boolean;
    isTreatmentProgramsTabComplete: boolean;
    isSubstanceDocumentsTabComplete: boolean;
    isActive: boolean;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;
}

export interface PACTSubstancePastRecentUse {
    pactSubstancePastRecentUseID: number;
    pactSymptomsSubstanceID: number;
    recentORPastType: number;
    substanceType: number;
    substanceUsedRecently: boolean;
    recentSubstanceUseArray: string;
    substanceUsedInPast: boolean;
    pastSubstanceUseArray: string;
    createdBy: string;
    createdDate: string;
    updatedBy: string;
    updatedDate: string;

}