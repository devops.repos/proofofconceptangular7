import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PACTSymtomsAndSubstance, PACTSubstancePastRecentUse } from './symptoms-substance-use.model';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class SymptomsSubstanceService {

    // Symptoms & Behaviors Tab
    getSymptomsSubstanceURL = environment.pactApiUrl + 'SymtomsAndSubstance/GetSymptomsSubstanceByApplicationID';
    saveSymptomsBhaviorsURL = environment.pactApiUrl + 'SymtomsAndSubstance/SaveSymptomsSubstanceBehaviors';

    // Substance Use Tab
    getSubstanceUseURL = environment.pactApiUrl + 'SymtomsAndSubstance/GetSubstanceUseBySymptomsSubstanceID';
    saveSubstanceUseURL = environment.pactApiUrl + 'SymtomsAndSubstance/SaveSymptomsSubstanceUse';


    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        }),
        response: "json",
    };

    constructor(private httpClient: HttpClient) { }

    //#region Symptoms & Behaviors Tab

    getPACTSymptomsSubstanceByApplicationID(ApplicationID: string) {
        return this.httpClient.get(this.getSymptomsSubstanceURL, { params: { applicationID: ApplicationID }, observe: 'response' });
    }

    savePACTSymptomsSubstanceData(pactSympSub: PACTSymtomsAndSubstance): Observable<any> {
        // console.log("Save Data : ",JSON.stringify(pactSympSub));
        return this.httpClient.post(this.saveSymptomsBhaviorsURL, JSON.stringify(pactSympSub), this.httpOptions);
    }

    //#endregion

    //#region Substance Use Tab

    getPACTSubstanceUseBySymptomsSubstanceID(SymptomsSubstanceID: string) {
        return this.httpClient.get(this.getSubstanceUseURL, { params: { symptomsSubstanceID: SymptomsSubstanceID }, observe: 'response' });
    }

    savePACTSubstanceUseData(pactSubUse: PACTSubstancePastRecentUse): Observable<any> {
        // console.log("Save Data : ",JSON.stringify(pactSubUse));
        return this.httpClient.post(this.saveSubstanceUseURL, JSON.stringify(pactSubUse), this.httpOptions);
    }

    //#endregion

}