import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SymptomsSubstanceUseComponent } from './symptoms-substance-use.component';

describe('SymptomsSubstanceUseComponent', () => {
  let component: SymptomsSubstanceUseComponent;
  let fixture: ComponentFixture<SymptomsSubstanceUseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SymptomsSubstanceUseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SymptomsSubstanceUseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
