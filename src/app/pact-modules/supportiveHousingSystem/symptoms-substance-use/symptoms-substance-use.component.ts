import { Component, OnInit, ViewEncapsulation, HostListener, OnDestroy } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { PACTSymtomsAndSubstance, PACTSubstancePastRecentUse } from './symptoms-substance-use.model';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { DatePipe } from '@angular/common';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { SymptomsSubstanceService } from './symptoms-substance-use.service';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-symptoms-substance-use',
  templateUrl: './symptoms-substance-use.component.html',
  styleUrls: ['./symptoms-substance-use.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class SymptomsSubstanceUseComponent implements OnInit, OnDestroy {

  //#region varibles declaration

  //  Tab Status : Pending = 0 ; Complete = 1; Blank = 2;
  symptomsTabStatus = 2;  // 0 = false; 1 = true; 2 = null
  substanceTabStatus = 2; // 0 : Pending ; 1 = complete ; 2 = blank
  treatmentTabStatus = 2;
  documentTabStatus = 2;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;
  //applicationID: number;
  symptomsSubstanceID: number;
  message: string;
  selectedTab = 0;

  symptomsGroup: FormGroup;
  substanceGroup: FormGroup;
  treatmentGroup: FormGroup;

  homicidalType: RefGroupDetails[];
  suicidalType: RefGroupDetails[];
  violentType: RefGroupDetails[];
  disruptiveBehaviorType: RefGroupDetails[];
  criminalActivityType: RefGroupDetails[];
  arsonType: RefGroupDetails[];
  cognitiveImpairmentType: RefGroupDetails[];
  hallucinationsType: RefGroupDetails[];
  delusionsType: RefGroupDetails[];
  thoughtDisordersType: RefGroupDetails[];
  clinicalDepressionType: RefGroupDetails[];
  maniaType: RefGroupDetails[];
  subUseRecentlyType: RefGroupDetails[];
  subUseInPastType: RefGroupDetails[];
  substanceUsedRecentlyPatternType: RefGroupDetails[];
  substanceUsedInPastPatternType: RefGroupDetails[];
  currentTreatmentProgramModalityType: RefGroupDetails[];
  completedTreatmentProgramModalityType: RefGroupDetails[];

  substanceUsedRecentlyPatternTypeArray = new Array();
  substanceUsedRecentlyPatternTypeDisplayArray: PACTSubstancePastRecentUse[];
  substanceUsedInPastPatternTypeArray = new Array();
  substanceUsedInPastPatternTypeDisplayArray: PACTSubstancePastRecentUse[];
  IsSubstanceUsedRecentlySelectedValue: string;
  IsSubstanceUsedPastSelectedValue: string;
  IsSubstanceUsedRecentlyPatternOtherSelected: string;
  IsSubstanceUsedInPastPatternOtherSelected: string;
  IsSubstanceUsedInPastPatternAlcoholSelected: string;
  IsSubstanceUsedInPastPatternDrugsSelected: string;

  pastAlcoholSobrietyType: RefGroupDetails[];
  pastDrugSobrietyType: RefGroupDetails[];

  IsCurrentlyTreatmentProgramSelected: string;
  IsHasCompletedTreatmentProgramSelected: string;
  IsActivelyParticipatingSelected: string;

  pactSymtomsAndSubstance: PACTSymtomsAndSubstance =
    {
      pactSymptomsSubstanceID: null, pactApplicationID: null, homicidal: null, suicidal: null, violent: null, disruptiveBehavior: null,
      criminalActivity: null, arson: null, cognitiveImpairment: null, hallucinations: null, delusions: null, thoughtDisorders: null,
      clinicalDepression: null, mania: null, substanceUsedRecently: null, substanceUsedRecentlyPatternType: null,
      substanceUsedRecentOtherSpecify: null, substanceUsedInPast: null, substanceUsedInPastPatternType: null, substanceUsedInPastOtherSpecify: null,
      pastAlcoholSobriety: null, pastAlcoholSobrietyDate: null, pastDrugSobriety: null, pastDrugSobrietyDate: null, isCurrentlyTreatmentProgram: null,
      currentTreatmentProgramName: null, currentTreatmentProgramModalityType: null, currentTreatmentProgramAdmissionDate: null, isActivelyParticipating: null,
      explainNonParticipation: null, hasCompletedTreatmentProgram: null, completedProgramName: null, completedTreatmentProgramModalityType: null,
      completedProgramAdmissionDate: null, isSymptomsBehaviorsTabComplete: null, isSubstanceUseTabComplete: null, isTreatmentProgramsTabComplete: null,
      isSubstanceDocumentsTabComplete: null, isActive: null, createdBy: null, createdDate: null, updatedBy: null, updatedDate: null
    }

  pactSubstancePastRecentUse: PACTSubstancePastRecentUse =
    {
      pactSubstancePastRecentUseID: null, pactSymptomsSubstanceID: null, recentORPastType: null, substanceType: null, substanceUsedRecently: null, recentSubstanceUseArray: null,
      substanceUsedInPast: null, pastSubstanceUseArray: null, createdBy: null, createdDate: null, updatedBy: null, updatedDate: null,
    }

  //  Min-Max Date for Date Picker
  now = new Date();
  minDate =  new Date(2005, 1, 1);
  currentDate = this.datePipe.transform(new Date(), 'MM/dd/yyyy hh:mm:ss a');
  //minDate = new Date(Math.abs(this.now.getFullYear() - 119), this.now.getMonth(), this.now.getDay());
  maxDate = new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDay());
  routeSub: any;

  applicationID: number;
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;
  public mask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  };
  //#endregion

  constructor(
    private router: Router,
    private datePipe: DatePipe,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private activatedRoute: ActivatedRoute,
    private sidenavStatusService: SidenavStatusService,
    private confirmDialogService: ConfirmDialogService,
    private symptomsSubstanceService: SymptomsSubstanceService,
    private clientApplicationService: ClientApplicationService,
    private route: ActivatedRoute
  ) { }

  ngOnDestroy = () => {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
    //  this.userDataSub.unsubscribe();
  }

  //  Save Data on page refresh
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.onSave();
  }

  ngOnInit() {
    // Side Nav Status
    //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);

    /** To Load the Sidenav Completion Status for all the Application related Pages */
    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.applicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
          /** If you have any logic that requires applicationID and if the Logic should be implemented in ngOnInit() (on component load). DO IT HERE */
          /** async logic that require applicationID, DO IT HERE */
          //this.retrieveSymptomsBehaviorsData();
        }
      }
    });

    // this.clientApplicationService.getPactApplicationID().subscribe(appID => {
    //   this.applicationID = appID;
    //   // remaining logic which uses this applicationID to be called below
    //   this.retrieveSymptomsBehaviorsData();
    // });

    this.clientApplicationService.getClientApplicationData().subscribe(res => {
      if (res && res.pactApplicationId > 0) {
        this.applicationID = res.pactApplicationId;
      }

      this.retrieveSymptomsBehaviorsData();
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {

        this.onSave_popstate(event)

        // if (event.navigationTrigger != "popstate" && this.symptomsGroup.touched) {
        //   this.onSave_popstate()
        // }
        // else if (event.navigationTrigger != "popstate" && this.symptomsGroup.dirty) {
        //   this.onSave_popstate()
        // }
      };
    });

    // #region Form Group Declaration

    this.symptomsGroup = this.formBuilder.group({
      hdnpactSymptomsSubstanceIDCtrl: [''],
      //pactSymptomsSubstanceIDCtrl: [''],
      pactApplicationIDCtrl: [''],
      homicidalCtrl: [0],
      suicidalCtrl: [0],
      violentCtrl: [0],
      disruptiveBehaviorCtrl: [0],
      criminalActivityCtrl: [0],
      arsonCtrl: [0],
      cognitiveImpairmentCtrl: [0],
      hallucinationsCtrl: [0],
      delusionsCtrl: [0],
      thoughtDisordersCtrl: [0],
      clinicalDepressionCtrl: [0],
      maniaCtrl: [0],
      substanceUsedRecentlyCtrl: [''],
      substanceUsedRecentlyPatternTypeCtrl: [0],
      substanceUsedRecentOtherSpecifyCtrl: [''],
      substanceUsedInPastCtrl: [''],
      substanceUsedInPastPatternTypeCtrl: [0],
      substanceUsedInPastOtherSpecifyCtrl: [''],

      pastAlcoholSobrietyTypeCtrl: [0],
      pastAlcoholSobrietyDateCtrl: [''],
      pastDrugSobrietyTypeCtrl: [0],
      pastDrugSobrietyDateCtrl: [''],

      isCurrentlyTreatmentProgramCtrl: [''],
      currentTreatmentProgramNameCtrl: [''],
      currentTreatmentProgramModalityTypeCtrl: [0],
      currentTreatmentProgramAdmissionDateCtrl: [''],

      isActivelyParticipatingCtrl: [''],
      explainNonParticipationCtrl: [''],

      hasCompletedTreatmentProgramCtrl: [''],
      completedProgramNameCtrl: [''],
      completedTreatmentProgramModalityTypeCtrl: [0],
      completedProgramAdmissionDateCtrl: ['']
    });

    this.substanceGroup = this.formBuilder.group({
      pactSubstancePastRecentUseIDCtrl: [''],
      pactSymptomsSubstanceIDCtrl: [''],
      RecentORPastTypeCtrl: [''],
      substanceTypeCtrl: [''],

      substanceUsedRecentlyCtrl: [''],
      substanceUsedRecentlyPatternTypeCtrl: [0],
      substanceUsedRecentOtherSpecifyCtrl: [''],

      subUseInPastTypeCtrl: [''],
      substanceUsedInPastCtrl: [''],
      substanceUsedInPastPatternTypeCtrl: [0],
      substanceUsedInPastOtherSpecifyCtrl: [''],

      pastAlcoholSobrietyTypeCtrl: [0],
      pastAlcoholSobrietyDateCtrl: [''],
      pastDrugSobrietyTypeCtrl: [0],
      pastDrugSobrietyDateCtrl: ['']
    });

    //#endregion

    // #region Dropdown values Assignment

    // 45 : Symptoms and Behavior =	Never, History, Current, Both
    // 46	: Substance Use Pattern
    // 47	: Substance Use Sobriety
    // 48	: Treatment Modality Type
    // 49	: Substance Type
    // 50	: Recent OR Past Type

    const value = ' 45,46,47,48,49,50 ';

    this.commonService.getRefGroupDetails(value)
      .subscribe(
        res => {
          if (res.body) {

            const data = res.body as RefGroupDetails[];

            this.homicidalType = data.filter(d => d.refGroupID === 45);
            this.suicidalType = data.filter(d => d.refGroupID === 45);
            this.violentType = data.filter(d => d.refGroupID === 45);
            this.disruptiveBehaviorType = data.filter(d => d.refGroupID === 45);
            this.criminalActivityType = data.filter(d => d.refGroupID === 45);
            this.arsonType = data.filter(d => d.refGroupID === 45);
            this.cognitiveImpairmentType = data.filter(d => d.refGroupID === 45);
            this.hallucinationsType = data.filter(d => d.refGroupID === 45);
            this.delusionsType = data.filter(d => d.refGroupID === 45);
            this.thoughtDisordersType = data.filter(d => d.refGroupID === 45);
            this.clinicalDepressionType = data.filter(d => d.refGroupID === 45);
            this.maniaType = data.filter(d => d.refGroupID === 45);
            this.subUseRecentlyType = data.filter(d => d.refGroupID === 49);
            this.subUseInPastType = data.filter(d => d.refGroupID === 49);
            this.substanceUsedRecentlyPatternType = data.filter(d => d.refGroupID === 46);
            this.substanceUsedInPastPatternType = data.filter(d => d.refGroupID === 46);
            this.currentTreatmentProgramModalityType = data.filter(d => d.refGroupID === 48);
            this.completedTreatmentProgramModalityType = data.filter(d => d.refGroupID === 48);
            this.pastAlcoholSobrietyType = data.filter(d => d.refGroupID === 47);
            this.pastDrugSobrietyType = data.filter(d => d.refGroupID === 47);
          }
        },
        error => console.error('Error in retrieving the data for dropdowns...!', error)
      );

    // #endregion

  }

  //#region Sysmptoms & behavior Tab & Treatment Programs Tab

  retrieveSymptomsBehaviorsData() {

    this.symptomsSubstanceService.getPACTSymptomsSubstanceByApplicationID(this.applicationID.toString()).subscribe(
      res => {
        if (res.body) {
          this.pactSymtomsAndSubstance = res.body as PACTSymtomsAndSubstance;
          this.setSymptomsBehaviorsValuesToUI(this.pactSymtomsAndSubstance);
          this.retrieveSubstancePastRecentUseData();

        } else {
          console.log('Sysmptoms & behavior data not found');
        }
      },
      error => console.error('Error in retrieving the data for Sysmptoms & behavior Tab ...!', error)
    );
  }

  setSymptomsBehaviorsValuesToUI(pactSymtomsAndSubstance: PACTSymtomsAndSubstance) {

    if (this.pactSymtomsAndSubstance.pactSymptomsSubstanceID != null) {
      this.symptomsGroup.get('hdnpactSymptomsSubstanceIDCtrl').setValue(this.pactSymtomsAndSubstance.pactSymptomsSubstanceID);
      this.symptomsSubstanceID = this.pactSymtomsAndSubstance.pactSymptomsSubstanceID;
    }
    if (this.pactSymtomsAndSubstance.homicidal != null) {
      this.symptomsGroup.get('homicidalCtrl').setValue(this.pactSymtomsAndSubstance.homicidal);
    }
    if (this.pactSymtomsAndSubstance.suicidal != null) {
      this.symptomsGroup.get('suicidalCtrl').setValue(this.pactSymtomsAndSubstance.suicidal);
    }
    if (this.pactSymtomsAndSubstance.violent != null) {
      this.symptomsGroup.get('violentCtrl').setValue(this.pactSymtomsAndSubstance.violent);
    }
    if (this.pactSymtomsAndSubstance.disruptiveBehavior != null) {
      this.symptomsGroup.get('disruptiveBehaviorCtrl').setValue(this.pactSymtomsAndSubstance.disruptiveBehavior);
    }
    if (this.pactSymtomsAndSubstance.criminalActivity != null) {
      this.symptomsGroup.get('criminalActivityCtrl').setValue(this.pactSymtomsAndSubstance.criminalActivity);
    }
    if (this.pactSymtomsAndSubstance.arson != null) {
      this.symptomsGroup.get('arsonCtrl').setValue(this.pactSymtomsAndSubstance.arson);
    }
    if (this.pactSymtomsAndSubstance.cognitiveImpairment != null) {
      this.symptomsGroup.get('cognitiveImpairmentCtrl').setValue(this.pactSymtomsAndSubstance.cognitiveImpairment);
    }
    if (this.pactSymtomsAndSubstance.hallucinations != null) {
      this.symptomsGroup.get('hallucinationsCtrl').setValue(this.pactSymtomsAndSubstance.hallucinations);
    }
    if (this.pactSymtomsAndSubstance.delusions != null) {
      this.symptomsGroup.get('delusionsCtrl').setValue(this.pactSymtomsAndSubstance.delusions);
    }
    if (this.pactSymtomsAndSubstance.thoughtDisorders != null) {
      this.symptomsGroup.get('thoughtDisordersCtrl').setValue(this.pactSymtomsAndSubstance.thoughtDisorders);
    }
    if (this.pactSymtomsAndSubstance.clinicalDepression != null) {
      this.symptomsGroup.get('clinicalDepressionCtrl').setValue(this.pactSymtomsAndSubstance.clinicalDepression);
    }
    if (this.pactSymtomsAndSubstance.mania != null) {
      this.symptomsGroup.get('maniaCtrl').setValue(this.pactSymtomsAndSubstance.mania);
    }
    if (this.pactSymtomsAndSubstance.isSymptomsBehaviorsTabComplete != null) {
      this.symptomsTabStatus = this.pactSymtomsAndSubstance.isSymptomsBehaviorsTabComplete == true ? 1 : 0;
    } else { this.symptomsTabStatus = null; }

    if (this.pactSymtomsAndSubstance.isSubstanceUseTabComplete != null) {
      this.substanceTabStatus = this.pactSymtomsAndSubstance.isSubstanceUseTabComplete == true ? 1 : 0;
    } else { this.substanceTabStatus = null; }

    if (this.pactSymtomsAndSubstance.isTreatmentProgramsTabComplete != null) {
      this.treatmentTabStatus = this.pactSymtomsAndSubstance.isTreatmentProgramsTabComplete == true ? 1 : 0;
    } else { this.treatmentTabStatus = null; }

    // Substance Use Tab Values

    if (this.pactSymtomsAndSubstance.substanceUsedRecently != null) {
      if (this.pactSymtomsAndSubstance.substanceUsedRecently === true) {
        this.substanceGroup.get('substanceUsedRecentlyCtrl').setValue('1');
        this.IsSubstanceUsedRecentlySelectedValue = "1";
      } else {
        this.substanceGroup.get('substanceUsedRecentlyCtrl').setValue('0');
        this.IsSubstanceUsedRecentlySelectedValue = "0";
      }
    }
    if (this.pactSymtomsAndSubstance.substanceUsedRecentOtherSpecify != null) {
      this.substanceGroup.get('substanceUsedRecentOtherSpecifyCtrl').setValue(this.pactSymtomsAndSubstance.substanceUsedRecentOtherSpecify);
    }
    if (this.pactSymtomsAndSubstance.substanceUsedRecentlyPatternType != null) {
      this.substanceGroup.get('substanceUsedRecentlyPatternTypeCtrl').setValue(this.pactSymtomsAndSubstance.substanceUsedRecentlyPatternType);
    }
    if (this.pactSymtomsAndSubstance.substanceUsedInPast != null) {
      if (this.pactSymtomsAndSubstance.substanceUsedInPast === true) {
        this.substanceGroup.get('substanceUsedInPastCtrl').setValue('1');
        this.IsSubstanceUsedPastSelectedValue = "1";
      } else {
        this.substanceGroup.get('substanceUsedInPastCtrl').setValue('0');
        this.IsSubstanceUsedPastSelectedValue = "0";
      }
    }
    if (this.pactSymtomsAndSubstance.substanceUsedInPastOtherSpecify != null) {
      this.substanceGroup.get('substanceUsedInPastOtherSpecifyCtrl').setValue(this.pactSymtomsAndSubstance.substanceUsedInPastOtherSpecify);
    }
    if (this.pactSymtomsAndSubstance.substanceUsedInPastPatternType != null) {
      this.substanceGroup.get('substanceUsedInPastPatternTypeCtrl').setValue(this.pactSymtomsAndSubstance.substanceUsedInPastPatternType);
    }
    if (this.pactSymtomsAndSubstance.pastAlcoholSobriety != null) {
      this.substanceGroup.get('pastAlcoholSobrietyTypeCtrl').setValue(this.pactSymtomsAndSubstance.pastAlcoholSobriety);
    }
    if (this.pactSymtomsAndSubstance.pastAlcoholSobrietyDate != null) {
      // this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').setValue(new Date(this.pactSymtomsAndSubstance.pastDrugSobrietyDate));
      this.datePipe.transform(this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').setValue(new Date(this.pactSymtomsAndSubstance.pastAlcoholSobrietyDate)));
    }
    if (this.pactSymtomsAndSubstance.pastDrugSobriety != null) {
      this.substanceGroup.get('pastDrugSobrietyTypeCtrl').setValue(this.pactSymtomsAndSubstance.pastDrugSobriety);
    }
    if (this.pactSymtomsAndSubstance.pastDrugSobrietyDate != null) {
      // this.substanceGroup.get('pastDrugSobrietyDateCtrl').setValue(new Date(this.pactSymtomsAndSubstance.pastDrugSobrietyDate));
      this.datePipe.transform(this.substanceGroup.get('pastDrugSobrietyDateCtrl').setValue(new Date(this.pactSymtomsAndSubstance.pastDrugSobrietyDate)));
    }


    // Treatment Programs Tab Values

    if (this.pactSymtomsAndSubstance.isCurrentlyTreatmentProgram != null) {
      if (this.pactSymtomsAndSubstance.isCurrentlyTreatmentProgram === true) {
        this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').setValue('1');
        this.IsCurrentlyTreatmentProgramSelected = "1";
      } else {
        this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').setValue('0');
        this.IsCurrentlyTreatmentProgramSelected = "0";
      }
    }
    if (this.pactSymtomsAndSubstance.currentTreatmentProgramName != null) {
      this.symptomsGroup.get('currentTreatmentProgramNameCtrl').setValue(this.pactSymtomsAndSubstance.currentTreatmentProgramName);
    }
    if (this.pactSymtomsAndSubstance.currentTreatmentProgramModalityType != null) {
      this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').setValue(this.pactSymtomsAndSubstance.currentTreatmentProgramModalityType);
    }
    if (this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate != null) {
      this.datePipe.transform(this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').setValue(new Date(this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate)));
    }
    if (this.pactSymtomsAndSubstance.isActivelyParticipating != null) {
      if (this.pactSymtomsAndSubstance.isActivelyParticipating === true) {
        this.symptomsGroup.get('isActivelyParticipatingCtrl').setValue('1');
        this.IsActivelyParticipatingSelected = "1";
      } else {
        this.symptomsGroup.get('isActivelyParticipatingCtrl').setValue('0');
        this.IsActivelyParticipatingSelected = "0";
      }
    }
    if (this.pactSymtomsAndSubstance.explainNonParticipation != null) {
      this.symptomsGroup.get('explainNonParticipationCtrl').setValue(this.pactSymtomsAndSubstance.explainNonParticipation);
    }
    if (this.pactSymtomsAndSubstance.hasCompletedTreatmentProgram != null) {
      if (this.pactSymtomsAndSubstance.hasCompletedTreatmentProgram === true) {
        this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').setValue('1');
        this.IsHasCompletedTreatmentProgramSelected = "1";
      } else {
        this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').setValue('0');
        this.IsHasCompletedTreatmentProgramSelected = "0";
      }
    }
    if (this.pactSymtomsAndSubstance.completedProgramName != null) {
      this.symptomsGroup.get('completedProgramNameCtrl').setValue(this.pactSymtomsAndSubstance.completedProgramName);
    }
    if (this.pactSymtomsAndSubstance.completedTreatmentProgramModalityType != null) {
      this.symptomsGroup.get('completedTreatmentProgramModalityTypeCtrl').setValue(this.pactSymtomsAndSubstance.completedTreatmentProgramModalityType);
    }
    if (this.pactSymtomsAndSubstance.completedProgramAdmissionDate != null) {
      this.datePipe.transform(this.symptomsGroup.get('completedProgramAdmissionDateCtrl').setValue(new Date(this.pactSymtomsAndSubstance.completedProgramAdmissionDate)));
    }

  }

  setValidatorsForSymptomsBehaviorsTab = () => {

    this.symptomsGroup.controls.homicidalCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.suicidalCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.violentCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.disruptiveBehaviorCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.criminalActivityCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.arsonCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.cognitiveImpairmentCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.hallucinationsCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.delusionsCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.thoughtDisordersCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.clinicalDepressionCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
    this.symptomsGroup.controls.maniaCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);

    this.symptomsGroup.controls.homicidalCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.suicidalCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.violentCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.disruptiveBehaviorCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.criminalActivityCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.arsonCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.cognitiveImpairmentCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.hallucinationsCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.delusionsCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.thoughtDisordersCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.clinicalDepressionCtrl.updateValueAndValidity();
    this.symptomsGroup.controls.maniaCtrl.updateValueAndValidity();

  }

  saveSymptomsBehaviorsData() {

    //set the validation on control
    this.setValidatorsForSymptomsBehaviorsTab();

    // get the values from the UI controls
    this.getSymptomsBehaviorsValuesFromUI();

    if (!this.SubstanceUseTabDateValidationCheck_NoMessage()) {
      return;
    }

    if (!this.TreatmentProgramTabDateValidationCheck()) {
      return;
    }

    this.symptomsSubstanceService.savePACTSymptomsSubstanceData(this.pactSymtomsAndSubstance).subscribe(
      data => {
        //this.message = 'Symptoms And Behaviors data saved successfully.';
        this.message = 'Data Successfully Saved.';
        this.toastr.success(this.message, 'Save');

        //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);
        if (this.applicationID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
        }

        this.retrieveSymptomsBehaviorsData();
      },
      error => {
        //this.message = 'Symptoms And Behaviors data did not save.';
        this.message = 'Data did not save.';
        this.toastr.error(this.message, 'Save');
      }
    );
  }

  saveSymptomsBehaviorsData_NoValidation() {

    // get the values from the UI controls
    this.getSymptomsBehaviorsValuesFromUI();

    if (!this.SubstanceUseTabDateValidationCheck_NoMessage()) {
      return;
    }

    if (!this.TreatmentProgramTabDateValidationCheck_NoMessage()) {
      return;
    }

    this.symptomsSubstanceService.savePACTSymptomsSubstanceData(this.pactSymtomsAndSubstance).subscribe(
      data => {
        //this.message = 'Symptoms And Behaviors data saved successfully.';
        this.message = 'Data Successfully Saved.';

        //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);
        if (this.applicationID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
        }

        this.retrieveSymptomsBehaviorsData();
      },
      error => {
        //this.message = 'Symptoms And Behaviors data did not save.';
        this.message = 'Data did not save.';
        console.error(this.message, error)
      }
    );
  }

  TreatmentProgramTabDateValidationCheck(): boolean {

    if (this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate = this.datePipe.transform(this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value, 'MM/dd/yyyy');

      if (new Date(this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate) > new Date()) {
        this.message = 'Current program admission date can not be greater than today date';
        this.toastr.warning(this.message, 'Save');
        return false;
      }
    }

    if (this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.completedProgramAdmissionDate = this.datePipe.transform(this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value, 'MM/dd/yyyy');

      if (new Date(this.pactSymtomsAndSubstance.completedProgramAdmissionDate) > new Date()) {
        this.message = 'Completed program admission date can not be greater than today date';
        this.toastr.warning(this.message, 'Save');
        return false;
      }
    }
    return true;
  }

  TreatmentProgramTabDateValidationCheck_NoMessage(): boolean {
    if (this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate = this.datePipe.transform(this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value, 'MM/dd/yyyy');

      if (new Date(this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate) > new Date()) {
        return false;
      }
    }

    if (this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.completedProgramAdmissionDate = this.datePipe.transform(this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value, 'MM/dd/yyyy');

      if (new Date(this.pactSymtomsAndSubstance.completedProgramAdmissionDate) > new Date()) {
        return false;
      }
    }
    return true;
  }

  getSymptomsBehaviorsValuesFromUI() {

    this.pactSymtomsAndSubstance.pactApplicationID = this.applicationID;

    if (this.symptomsGroup.get('hdnpactSymptomsSubstanceIDCtrl') != null) {
      this.pactSymtomsAndSubstance.pactSymptomsSubstanceID = this.symptomsGroup.get('hdnpactSymptomsSubstanceIDCtrl').value;
    }

    if (this.symptomsGroup.get('homicidalCtrl').value != 0) {
      this.pactSymtomsAndSubstance.homicidal = this.symptomsGroup.get('homicidalCtrl').value;
    } else { this.pactSymtomsAndSubstance.homicidal = null; }

    if (this.symptomsGroup.get('suicidalCtrl').value != 0) {
      this.pactSymtomsAndSubstance.suicidal = this.symptomsGroup.get('suicidalCtrl').value;
    } else { this.pactSymtomsAndSubstance.suicidal = null; }

    if (this.symptomsGroup.get('violentCtrl').value != 0) {
      this.pactSymtomsAndSubstance.violent = this.symptomsGroup.get('violentCtrl').value;
    } else { this.pactSymtomsAndSubstance.violent = null; }

    if (this.symptomsGroup.get('disruptiveBehaviorCtrl').value != 0) {
      this.pactSymtomsAndSubstance.disruptiveBehavior = this.symptomsGroup.get('disruptiveBehaviorCtrl').value;
    } else { this.pactSymtomsAndSubstance.disruptiveBehavior = null; }

    if (this.symptomsGroup.get('criminalActivityCtrl').value != 0) {
      this.pactSymtomsAndSubstance.criminalActivity = this.symptomsGroup.get('criminalActivityCtrl').value;
    } else { this.pactSymtomsAndSubstance.criminalActivity = null; }

    if (this.symptomsGroup.get('arsonCtrl').value != 0) {
      this.pactSymtomsAndSubstance.arson = this.symptomsGroup.get('arsonCtrl').value;
    } else { this.pactSymtomsAndSubstance.arson = null; }

    if (this.symptomsGroup.get('cognitiveImpairmentCtrl').value != 0) {
      this.pactSymtomsAndSubstance.cognitiveImpairment = this.symptomsGroup.get('cognitiveImpairmentCtrl').value;
    } else { this.pactSymtomsAndSubstance.cognitiveImpairment = null; }

    if (this.symptomsGroup.get('hallucinationsCtrl').value != 0) {
      this.pactSymtomsAndSubstance.hallucinations = this.symptomsGroup.get('hallucinationsCtrl').value;
    } else { this.pactSymtomsAndSubstance.hallucinations = null; }

    if (this.symptomsGroup.get('delusionsCtrl').value != 0) {
      this.pactSymtomsAndSubstance.delusions = this.symptomsGroup.get('delusionsCtrl').value;
    } else { this.pactSymtomsAndSubstance.delusions = null; }

    if (this.symptomsGroup.get('thoughtDisordersCtrl').value != 0) {
      this.pactSymtomsAndSubstance.thoughtDisorders = this.symptomsGroup.get('thoughtDisordersCtrl').value;
    } else { this.pactSymtomsAndSubstance.thoughtDisorders = null; }

    if (this.symptomsGroup.get('clinicalDepressionCtrl').value != 0) {
      this.pactSymtomsAndSubstance.clinicalDepression = this.symptomsGroup.get('clinicalDepressionCtrl').value;
    } else { this.pactSymtomsAndSubstance.clinicalDepression = null; }

    if (this.symptomsGroup.get('maniaCtrl').value != 0) {
      this.pactSymtomsAndSubstance.mania = this.symptomsGroup.get('maniaCtrl').value;
    } else { this.pactSymtomsAndSubstance.mania = null; }

    //Substance Use Tab

    if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value != null) {
      if (this.symptomsGroup.get('substanceUsedRecentlyCtrl').value == "1") {
        this.pactSymtomsAndSubstance.substanceUsedRecently = true;
      } else if (this.symptomsGroup.get('substanceUsedRecentlyCtrl').value == "0") {
        this.pactSymtomsAndSubstance.substanceUsedRecently = false;
      }
    } else { this.pactSymtomsAndSubstance.substanceUsedRecently = null; }

    if (this.substanceGroup.get('substanceUsedInPastCtrl').value != null) {
      if (this.symptomsGroup.get('substanceUsedInPastCtrl').value == "1") {
        this.pactSymtomsAndSubstance.substanceUsedInPast = true;
      } else if (this.symptomsGroup.get('substanceUsedInPastCtrl').value == "0") {
        this.pactSymtomsAndSubstance.substanceUsedInPast = false;
      }
    } else { this.pactSymtomsAndSubstance.substanceUsedInPast = null; }

    // Treatment Programs Tab Values

    if (this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').value != null) {
      if (this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').value == "1") {
        this.pactSymtomsAndSubstance.isCurrentlyTreatmentProgram = true;
      } else if (this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').value == "0") {
        this.pactSymtomsAndSubstance.isCurrentlyTreatmentProgram = false;
      }
    } else { this.pactSymtomsAndSubstance.isCurrentlyTreatmentProgram = null; }

    if (this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value != null) {
      this.pactSymtomsAndSubstance.currentTreatmentProgramName = this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value;
    } else { this.pactSymtomsAndSubstance.currentTreatmentProgramName = null; }

    if (this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').value != 0) {
      this.pactSymtomsAndSubstance.currentTreatmentProgramModalityType = this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').value;
    } else { this.pactSymtomsAndSubstance.currentTreatmentProgramModalityType = null; }

    if (this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate = this.datePipe.transform(this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value, 'MM/dd/yyyy');
    } else { this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate = null; }

    if (this.symptomsGroup.get('isActivelyParticipatingCtrl').value != null) {
      if (this.symptomsGroup.get('isActivelyParticipatingCtrl').value == "1") {
        this.pactSymtomsAndSubstance.isActivelyParticipating = true;
      } else if (this.symptomsGroup.get('isActivelyParticipatingCtrl').value == "0") {
        this.pactSymtomsAndSubstance.isActivelyParticipating = false;
      }
    } else { this.pactSymtomsAndSubstance.isActivelyParticipating = null; }

    if (this.symptomsGroup.get('explainNonParticipationCtrl').value != null) {
      this.pactSymtomsAndSubstance.explainNonParticipation = this.symptomsGroup.get('explainNonParticipationCtrl').value;
    } else { this.pactSymtomsAndSubstance.explainNonParticipation = null; }

    if (this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').value != null) {
      if (this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').value == "1") {
        this.pactSymtomsAndSubstance.hasCompletedTreatmentProgram = true;
      } else if (this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').value == "0") {
        this.pactSymtomsAndSubstance.hasCompletedTreatmentProgram = false;
      }
    } else { this.pactSymtomsAndSubstance.hasCompletedTreatmentProgram = null; }

    if (this.symptomsGroup.get('completedProgramNameCtrl').value != null) {
      this.pactSymtomsAndSubstance.completedProgramName = this.symptomsGroup.get('completedProgramNameCtrl').value;
    } else { this.pactSymtomsAndSubstance.completedProgramName = null; }

    if (this.symptomsGroup.get('completedTreatmentProgramModalityTypeCtrl').value != 0) {
      this.pactSymtomsAndSubstance.completedTreatmentProgramModalityType = this.symptomsGroup.get('completedTreatmentProgramModalityTypeCtrl').value;
    } else { this.pactSymtomsAndSubstance.completedTreatmentProgramModalityType = null; }

    if (this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.completedProgramAdmissionDate = this.datePipe.transform(this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value, 'MM/dd/yyyy');
    } else { this.pactSymtomsAndSubstance.completedProgramAdmissionDate = null; }

    this.pactSymtomsAndSubstance.isActive = true;
    this.pactSymtomsAndSubstance.isSymptomsBehaviorsTabComplete = this.IsSymptomsBehaviorsTabCompleteCheck();
    this.pactSymtomsAndSubstance.isSubstanceUseTabComplete = this.IsSubstanceUseTabCompleteCheck();
    this.pactSymtomsAndSubstance.isTreatmentProgramsTabComplete = this.IsTreatmentProgramsTabCompleteCheck();

  }

  IsSymptomsBehaviorsTabCompleteCheck(): boolean {
    if (this.symptomsGroup) {
      if (this.symptomsGroup.get('homicidalCtrl').value != 0 && this.symptomsGroup.get('suicidalCtrl').value != 0
        && this.symptomsGroup.get('violentCtrl').value != 0 && this.symptomsGroup.get('disruptiveBehaviorCtrl').value != 0
        && this.symptomsGroup.get('criminalActivityCtrl').value != 0 && this.symptomsGroup.get('arsonCtrl').value != 0
        && this.symptomsGroup.get('cognitiveImpairmentCtrl').value != 0 && this.symptomsGroup.get('hallucinationsCtrl').value != 0
        && this.symptomsGroup.get('delusionsCtrl').value != 0 && this.symptomsGroup.get('thoughtDisordersCtrl').value != 0
        && this.symptomsGroup.get('clinicalDepressionCtrl').value != 0 
        && this.symptomsGroup.get('maniaCtrl').value != 0) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  IsSubstanceUseTabCompleteCheck(): boolean {

    var returnValue: boolean = false;

    if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value != null && this.substanceGroup.get('substanceUsedInPastCtrl').value != null) {

      // if both radio button selected value in NO then return true
      if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value == "0" && this.substanceGroup.get('substanceUsedInPastCtrl').value == "0") {
        returnValue = true;
      }
      // if Recent selected : YES && Past selected NO then
      else if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value == "1" && this.substanceGroup.get('substanceUsedInPastCtrl').value == "0") {

        // Recently Use Substance Use Pattern is not equal to "0"
        if (this.substanceGroup.get('substanceUsedRecentlyPatternTypeCtrl').value != "0") {

          // If any checkbox is selected
          if (this.substanceUsedRecentlyPatternTypeArray.length > 0) {

            // Checkbox Other : Checked
            if (this.substanceUsedRecentlyPatternTypeArray.filter(x => x == 464).length > 0) {

              // Other Specify Textbox is not null
              if (this.substanceGroup.get('substanceUsedRecentOtherSpecifyCtrl').value != "") {
                returnValue = true;
              } else { returnValue = false; return; }
            } else { returnValue = true; }
          } else { returnValue = false; return; }
        } else { returnValue = false; return; }

      }
      // if Recent selected : NO && Past selected YES then
      else if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value == "0" && this.substanceGroup.get('substanceUsedInPastCtrl').value == "1") {

        // Past Used Substance Use Pattern is not equal to "0"
        if (this.substanceGroup.get('substanceUsedInPastPatternTypeCtrl').value != "0") {

          // If any checkbox is selected
          if (this.substanceUsedInPastPatternTypeArray.length > 0) {

            // Checkbox Other : Checked
            if (this.substanceUsedInPastPatternTypeArray.filter(x => x == 464).length > 0) {

              // Other Specify Textbox is not null
              if (this.substanceGroup.get('substanceUsedInPastOtherSpecifyCtrl').value != "") {
                returnValue = true;
              }
            } else { returnValue = true; }

            // Checkbox Alcohol : Checked
            if (this.substanceUsedInPastPatternTypeArray.filter(x => x == 455).length > 0) {

              // Alcohol : Sobriety : Selected
              if (this.substanceGroup.get('pastAlcoholSobrietyTypeCtrl').value != "0") {
                returnValue = true;
              } else { returnValue = false; return; }
            }

            // Checkbox Drugs(Opiates ,Cannibis ,Cocaine/Crack, Stimulants, Benzodiazepine, Sedatives/Hypnotics) : Checked
            if (this.substanceUsedInPastPatternTypeArray.filter(x => x == 456).length > 0 || this.substanceUsedInPastPatternTypeArray.filter(x => x == 457).length > 0
              || this.substanceUsedInPastPatternTypeArray.filter(x => x == 458).length > 0 || this.substanceUsedInPastPatternTypeArray.filter(x => x == 459).length > 0
              || this.substanceUsedInPastPatternTypeArray.filter(x => x == 460).length > 0 || this.substanceUsedInPastPatternTypeArray.filter(x => x == 461).length > 0
            ) {

              // Drugs : Sobriety : Selected
              if (this.substanceGroup.get('pastDrugSobrietyTypeCtrl').value != "0") {
                returnValue = true;
              } else { returnValue = false; return; }
            }

          }
        }

      }
      // if Recent selected : YES && Past selected YES then
      else if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value == "1" && this.substanceGroup.get('substanceUsedInPastCtrl').value == "1") {

        // Recent selected : YES

        // Recently Use Substance Use Pattern is not equal to "0"
        if (this.substanceGroup.get('substanceUsedRecentlyPatternTypeCtrl').value != "0") {

          // If any checkbox is selected
          if (this.substanceUsedRecentlyPatternTypeArray.length > 0) {

            // Checkbox Other : Checked
            if (this.substanceUsedRecentlyPatternTypeArray.filter(x => x == 464).length > 0) {

              // Other Specify Textbox is not null
              if (this.substanceGroup.get('substanceUsedRecentOtherSpecifyCtrl').value != "") {
                returnValue = true;
              } else { returnValue = false; return; }
            } else { returnValue = true; }
          } else { returnValue = false; return; }
        } else { returnValue = false; return; }

        // Past selected YES

        // Past Used Substance Use Pattern is not equal to "0"
        if (this.substanceGroup.get('substanceUsedInPastPatternTypeCtrl').value != "0") {

          // If any checkbox is selected
          if (this.substanceUsedInPastPatternTypeArray.length > 0) {

            // Checkbox Other : Checked
            if (this.substanceUsedInPastPatternTypeArray.filter(x => x == 464).length > 0) {

              // Other Specify Textbox is not null
              if (this.substanceGroup.get('substanceUsedInPastOtherSpecifyCtrl').value != "") {
                returnValue = true;
              } else { returnValue = false; return; }
            } else { returnValue = true; }

            // Checkbox Alcohol : Checked
            if (this.substanceUsedInPastPatternTypeArray.filter(x => x == 455).length > 0) {

              // Alcohol : Sobriety : Selected
              if (this.substanceGroup.get('pastAlcoholSobrietyTypeCtrl').value != "0") {
                returnValue = true;
              } else { returnValue = false; return; }
            }

            // Checkbox Drugs(Opiates ,Cannibis ,Cocaine/Crack, Stimulants, Benzodiazepine, Sedatives/Hypnotics) : Checked
            if (this.substanceUsedInPastPatternTypeArray.filter(x => x == 456).length > 0 || this.substanceUsedInPastPatternTypeArray.filter(x => x == 457).length > 0
              || this.substanceUsedInPastPatternTypeArray.filter(x => x == 458).length > 0 || this.substanceUsedInPastPatternTypeArray.filter(x => x == 459).length > 0
              || this.substanceUsedInPastPatternTypeArray.filter(x => x == 460).length > 0 || this.substanceUsedInPastPatternTypeArray.filter(x => x == 461).length > 0
            ) {

              // Drugs : Sobriety : Selected
              if (this.substanceGroup.get('pastDrugSobrietyTypeCtrl').value != "0") {
                returnValue = true;
              } else { returnValue = false; return; }
            }
          } else { returnValue = false; return; }
        } else { returnValue = false; return; }
      }

    }
    return returnValue;
  }

  IsTreatmentProgramsTabCompleteCheck(): boolean {

    // trim all fields if it's having any space value
    if (this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value != null) {
      // if (this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value.length == 1) {
      //   this.symptomsGroup.get('currentTreatmentProgramNameCtrl').setValue(null);
      // } else if (this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value.length > 1) {
      this.symptomsGroup.get('currentTreatmentProgramNameCtrl').setValue(this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value.trim());
      //}
    }
    if (this.symptomsGroup.get('explainNonParticipationCtrl').value != null) {
      // if (this.symptomsGroup.get('explainNonParticipationCtrl').value == 1) {
      //   this.symptomsGroup.get('explainNonParticipationCtrl').setValue(null);
      // } else if (this.symptomsGroup.get('explainNonParticipationCtrl').value > 1) {
      this.symptomsGroup.get('explainNonParticipationCtrl').setValue(this.symptomsGroup.get('explainNonParticipationCtrl').value.trim());
      //}
    }
    if (this.symptomsGroup.get('completedProgramNameCtrl').value != null) {
      // if (this.symptomsGroup.get('completedProgramNameCtrl').value == 1) {
      //   this.symptomsGroup.get('completedProgramNameCtrl').setValue(null);
      // } else if (this.symptomsGroup.get('completedProgramNameCtrl').value > 1) {
      this.symptomsGroup.get('completedProgramNameCtrl').setValue(this.symptomsGroup.get('completedProgramNameCtrl').value.trim());
      //}
    }

    // Condition Checks with different scenarios
    if (this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').value != null && this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').value != null) {

      // Current Program : No && Past : No
      if (this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').value == "0" && this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').value == "0") {
        return true;
      }
      // Current Program : Yes && Past : No
      if (this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').value == "1" && this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').value == "0") {

        //Check if is the Applicant actively participating is null or not
        if (this.symptomsGroup.get('isActivelyParticipatingCtrl').value != null) {

          //Check if is the Applicant actively participating is YES
          if (this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value != "" && this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').value != "0"
            // && this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value != null
            && this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate != null
            && this.symptomsGroup.get('isActivelyParticipatingCtrl').value == "1") {
            return true;
          }
          //Check if is the Applicant actively participating is NO && explaination is not null
          if (this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value != "" && this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').value != "0"
            // && this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value != null
            && this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate != null
            && this.symptomsGroup.get('isActivelyParticipatingCtrl').value == "0"
            && this.symptomsGroup.get('explainNonParticipationCtrl').value != "") {
            return true;
          }
        }
      }
      // Current Program : NO && Past : YES
      if (this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').value == "0" && this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').value == "1") {

        //Check if every thing is not null
        if (this.symptomsGroup.get('completedProgramNameCtrl').value != "" && this.symptomsGroup.get('completedTreatmentProgramModalityTypeCtrl').value != "0"
          // && this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value != null
          && this.pactSymtomsAndSubstance.completedProgramAdmissionDate != null) {
          return true;
        }
      }

      // Current Program : YES && Past : YES
      if (this.symptomsGroup.get('isCurrentlyTreatmentProgramCtrl').value == "1" && this.symptomsGroup.get('hasCompletedTreatmentProgramCtrl').value == "1") {
        //Check if is the Applicant actively participating is null or not
        if (this.symptomsGroup.get('isActivelyParticipatingCtrl').value != null) {

          // Check if current program everything is not null and is the Applicant actively participating is YES  && Past everything is not null
          if (this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value != "" && this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').value != "0"
            // && this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value != null
            && this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate != null
            && this.symptomsGroup.get('isActivelyParticipatingCtrl').value == "1"
            && this.symptomsGroup.get('completedProgramNameCtrl').value != "" && this.symptomsGroup.get('completedTreatmentProgramModalityTypeCtrl').value != "0"
            // && this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value != null
            && this.pactSymtomsAndSubstance.completedProgramAdmissionDate != null) {
            return true;
          }
          // Check if current program everything is not null and is the Applicant actively participating is No and Explanation is not null  && Past everything is not null
          if (this.symptomsGroup.get('currentTreatmentProgramNameCtrl').value != "" && this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').value != "0"
            // && this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').value != null
            && this.pactSymtomsAndSubstance.currentTreatmentProgramAdmissionDate != null
            && this.symptomsGroup.get('isActivelyParticipatingCtrl').value == "0"
            && this.symptomsGroup.get('explainNonParticipationCtrl').value != ""
            && this.symptomsGroup.get('completedProgramNameCtrl').value != "" && this.symptomsGroup.get('completedTreatmentProgramModalityTypeCtrl').value != "0"
            // && this.symptomsGroup.get('completedProgramAdmissionDateCtrl').value != null
            && this.pactSymtomsAndSubstance.completedProgramAdmissionDate != null) {
            return true;
          }
        }
      }
    }
    return false;
  }

  setCurrentlyTreatmentProgramRadio(value: string) {
    this.IsCurrentlyTreatmentProgramSelected = value;
    if (value == '1') {
      // set the validation
      this.symptomsGroup.controls.currentTreatmentProgramNameCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(1)]);
      this.symptomsGroup.controls.currentTreatmentProgramModalityTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.symptomsGroup.controls.currentTreatmentProgramAdmissionDateCtrl.setValidators([Validators.required]);
      //this.symptomsGroup.controls.isActivelyParticipatingCtrl.setValidators([CustomValidators.radioGroupRequired]);
      // this.symptomsGroup.controls.explainNonParticipationCtrl.setValidators([Validators.required]);
      this.symptomsGroup.get('isActivelyParticipatingCtrl').setValue('1');

      this.symptomsGroup.controls.currentTreatmentProgramNameCtrl.updateValueAndValidity();
      this.symptomsGroup.controls.currentTreatmentProgramModalityTypeCtrl.updateValueAndValidity();
      this.symptomsGroup.controls.currentTreatmentProgramAdmissionDateCtrl.updateValueAndValidity();
      //this.symptomsGroup.controls.isActivelyParticipatingCtrl.updateValueAndValidity();
      // this.symptomsGroup.controls.explainNonParticipationCtrl.updateValueAndValidity();

    }
    else if (value == '0') {

      this.symptomsGroup.controls.currentTreatmentProgramNameCtrl.clearValidators();
      this.symptomsGroup.controls.currentTreatmentProgramModalityTypeCtrl.clearValidators();
      this.symptomsGroup.controls.currentTreatmentProgramAdmissionDateCtrl.clearValidators();
      // this.symptomsGroup.controls.isActivelyParticipatingCtrl.clearValidators();
      // this.symptomsGroup.controls.explainNonParticipationCtrl.clearValidators();

      this.symptomsGroup.get('currentTreatmentProgramNameCtrl').setValue('');
      this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').setValue(0);
      this.symptomsGroup.get('currentTreatmentProgramAdmissionDateCtrl').setValue(null);
      //this.symptomsGroup.get('isActivelyParticipatingCtrl').setValue(null);
      this.symptomsGroup.get('explainNonParticipationCtrl').setValue('');

    }
  }

  setIsActivelyParticipatingCtrlRadio(value: string) {
    this.IsActivelyParticipatingSelected = value;
    if (value == '0') {
      this.symptomsGroup.controls.explainNonParticipationCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(1)]);
      this.symptomsGroup.controls.explainNonParticipationCtrl.updateValueAndValidity();
    }
    else if (value == '1') {
      this.symptomsGroup.controls.explainNonParticipationCtrl.clearValidators();
      this.symptomsGroup.get('explainNonParticipationCtrl').setValue('');
    }
  }

  setHasCompletedTreatmentProgramRadio(value: string) {
    this.IsHasCompletedTreatmentProgramSelected = value;
    if (value == '1') {
      this.symptomsGroup.controls.completedProgramNameCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(1)]);
      this.symptomsGroup.controls.completedTreatmentProgramModalityTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.symptomsGroup.controls.completedProgramAdmissionDateCtrl.setValidators([Validators.required]);

      this.symptomsGroup.controls.completedProgramNameCtrl.updateValueAndValidity();
      this.symptomsGroup.controls.currentTreatmentProgramModalityTypeCtrl.updateValueAndValidity();
      this.symptomsGroup.controls.completedProgramAdmissionDateCtrl.updateValueAndValidity();
    }
    if (value == '0') {

      this.symptomsGroup.controls.completedProgramNameCtrl.clearValidators();
      this.symptomsGroup.controls.currentTreatmentProgramModalityTypeCtrl.clearValidators();
      this.symptomsGroup.controls.completedProgramAdmissionDateCtrl.clearValidators();

      this.symptomsGroup.get('completedProgramNameCtrl').setValue('');
      this.symptomsGroup.get('completedTreatmentProgramModalityTypeCtrl').setValue(0);
      this.symptomsGroup.get('completedProgramAdmissionDateCtrl').setValue(null);
    }
  }

  //#endregion


  //#region Substance Use Tab

  retrieveSubstancePastRecentUseData() {

    this.symptomsSubstanceService.getPACTSubstanceUseBySymptomsSubstanceID(this.symptomsSubstanceID.toString()).subscribe(
      res => {
        if (res.body) {

          const data1 = res.body as PACTSubstancePastRecentUse[];

          this.substanceUsedRecentlyPatternTypeArray = [];
          this.substanceUsedInPastPatternTypeArray = [];

          this.substanceUsedRecentlyPatternTypeDisplayArray = data1.filter(d => d.recentORPastType === 465);

          if (this.substanceUsedRecentlyPatternTypeDisplayArray.length == 0) {
            this.IsSubstanceUsedRecentlyPatternOtherSelected = "0";
          }
          this.substanceUsedRecentlyPatternTypeDisplayArray.forEach((element) => {
            this.substanceUsedRecentlyPatternTypeArray.push(element.substanceType);
            if (element.substanceType == 464) {
              this.IsSubstanceUsedRecentlyPatternOtherSelected = "1";
            }
          });

          this.substanceUsedInPastPatternTypeDisplayArray = data1.filter(d => d.recentORPastType === 466);

          if (this.substanceUsedInPastPatternTypeDisplayArray.length == 0) {
            this.IsSubstanceUsedInPastPatternOtherSelected = "0";
          }

          this.substanceUsedInPastPatternTypeDisplayArray.forEach((element) => {
            this.substanceUsedInPastPatternTypeArray.push(element.substanceType);
            if (element.substanceType == 464) {
              this.IsSubstanceUsedInPastPatternOtherSelected = "1";
            }
            else if (element.substanceType == 455) {
              this.IsSubstanceUsedInPastPatternAlcoholSelected = "1";
            }
            else if (element.substanceType == 456 || element.substanceType == 457 || element.substanceType == 458 || element.substanceType == 459
              || element.substanceType == 460 || element.substanceType == 461) {
              this.IsSubstanceUsedInPastPatternDrugsSelected = "1";
            }
          });

        } else {
          console.log('Substance Use data not found...');
        }
      },
      error => console.error('Error in retrieving the Substance Use data ...!', error)
    );
  }

  SubstanceUseTabDateValidationCheck(): boolean {
    if (this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.pastAlcoholSobrietyDate = this.datePipe.transform(this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').value, 'MM/dd/yyyy');

      if (new Date(this.pactSymtomsAndSubstance.pastAlcoholSobrietyDate) > new Date()) {
        this.message = 'Past Alcohol Sobriety date can not be greater than today date';
        this.toastr.warning(this.message, 'Save');
        return false;
      }
    }

    if (this.substanceGroup.get('pastDrugSobrietyDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.pastDrugSobrietyDate = this.datePipe.transform(this.substanceGroup.get('pastDrugSobrietyDateCtrl').value, 'MM/dd/yyyy');

      if (new Date(this.pactSymtomsAndSubstance.pastDrugSobrietyDate) > new Date()) {
        this.message = 'Past drugs Sobriety date can not be greater than today date';
        this.toastr.warning(this.message, 'Save');
        return false;
      }
    }
    return true;
  }

  SubstanceUseTabDateValidationCheck_NoMessage(): boolean {
    if (this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.pastAlcoholSobrietyDate = this.datePipe.transform(this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').value, 'MM/dd/yyyy');

      if (new Date(this.pactSymtomsAndSubstance.pastAlcoholSobrietyDate) > new Date()) {
        return false;
      }
    }

    if (this.substanceGroup.get('pastDrugSobrietyDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.pastDrugSobrietyDate = this.datePipe.transform(this.substanceGroup.get('pastDrugSobrietyDateCtrl').value, 'MM/dd/yyyy');

      if (new Date(this.pactSymtomsAndSubstance.pastDrugSobrietyDate) > new Date()) {
        return false;
      }
    }
    return true;
  }

  saveSubstanceUseData() {

    this.GetSubstanceUseDataFromUI();

    if (!this.SubstanceUseTabDateValidationCheck()) {
      return;
    }

    this.symptomsSubstanceService.savePACTSubstanceUseData(this.pactSubstancePastRecentUse).subscribe(
      data => {
        this.message = 'Substance Use data saved successfully.';
        this.toastr.success(this.message, 'Save');
        if (this.applicationID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
        }
      },
      error => {
        this.message = 'Substance Use  data did not save.';
        this.toastr.warning(this.message, 'Save');
      }
    );

  }

  saveSubstanceUseData_NoValidation() {

    this.GetSubstanceUseDataFromUI();

    if (!this.SubstanceUseTabDateValidationCheck_NoMessage()) {
      return;
    }

    this.symptomsSubstanceService.savePACTSubstanceUseData(this.pactSubstancePastRecentUse).subscribe(
      data => {
        this.message = 'Substance Use data saved successfully.';
        if (this.applicationID) {
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
        }
      },
      error => {
        this.message = 'Substance Use  data did not save.';
        // this.toastr.warning(this.message, 'Save');
        console.error(this.message, error)
      }
    );

  }

  GetSubstanceUseDataFromUI() {

    if (this.symptomsGroup.get('hdnpactSymptomsSubstanceIDCtrl').value != null) {
      this.pactSubstancePastRecentUse.pactSymptomsSubstanceID = this.symptomsGroup.get('hdnpactSymptomsSubstanceIDCtrl').value;
    }

    if (this.substanceUsedRecentlyPatternTypeArray.length > 0) {
      this.pactSubstancePastRecentUse.recentSubstanceUseArray = this.substanceUsedRecentlyPatternTypeArray.join(',');
    } else { this.pactSubstancePastRecentUse.recentSubstanceUseArray = null; }

    if (this.substanceUsedInPastPatternTypeArray.length > 0) {
      this.pactSubstancePastRecentUse.pastSubstanceUseArray = this.substanceUsedInPastPatternTypeArray.join(',');
    } else { this.pactSubstancePastRecentUse.pastSubstanceUseArray = null; }

    // assign value to Symptoms & behaviors Form Control Group
    // Recent Substabce Use
    if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value != null) {
      if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value == "1") {
        this.pactSymtomsAndSubstance.substanceUsedRecently = true;
        this.pactSubstancePastRecentUse.substanceUsedRecently = true;
      } else if (this.substanceGroup.get('substanceUsedRecentlyCtrl').value == "0") {
        this.pactSymtomsAndSubstance.substanceUsedRecently = false;
        this.pactSubstancePastRecentUse.substanceUsedRecently = false;
      }
    } else {
      this.pactSymtomsAndSubstance.substanceUsedRecently = null;
      this.pactSubstancePastRecentUse.substanceUsedRecently = null;
    }

    if (this.substanceGroup.get('substanceUsedRecentOtherSpecifyCtrl').value != null) {
      this.pactSymtomsAndSubstance.substanceUsedRecentOtherSpecify = this.substanceGroup.get('substanceUsedRecentOtherSpecifyCtrl').value;
    }
    if (this.substanceGroup.get('substanceUsedRecentlyPatternTypeCtrl').value != 0) {
      this.pactSymtomsAndSubstance.substanceUsedRecentlyPatternType = this.substanceGroup.get('substanceUsedRecentlyPatternTypeCtrl').value;
    } else { this.pactSymtomsAndSubstance.substanceUsedRecentlyPatternType = null; }

    // Past Substabce Use
    if (this.substanceGroup.get('substanceUsedInPastCtrl').value != null) {
      if (this.substanceGroup.get('substanceUsedInPastCtrl').value == "1") {
        this.pactSymtomsAndSubstance.substanceUsedInPast = true;
        this.pactSubstancePastRecentUse.substanceUsedInPast = true;
      } else if (this.substanceGroup.get('substanceUsedInPastCtrl').value == "0") {
        this.pactSymtomsAndSubstance.substanceUsedInPast = false;
        this.pactSubstancePastRecentUse.substanceUsedInPast = false;
      }
    } else {
      this.pactSymtomsAndSubstance.substanceUsedInPast = null;
      this.pactSubstancePastRecentUse.substanceUsedInPast = null;
    }

    if (this.substanceGroup.get('substanceUsedInPastOtherSpecifyCtrl').value != null) {
      this.pactSymtomsAndSubstance.substanceUsedInPastOtherSpecify = this.substanceGroup.get('substanceUsedInPastOtherSpecifyCtrl').value;
    }
    if (this.substanceGroup.get('substanceUsedInPastPatternTypeCtrl').value != 0) {
      this.pactSymtomsAndSubstance.substanceUsedInPastPatternType = this.substanceGroup.get('substanceUsedInPastPatternTypeCtrl').value;
    } else { this.pactSymtomsAndSubstance.substanceUsedInPastPatternType = null; }

    if (this.substanceGroup.get('pastAlcoholSobrietyTypeCtrl').value != 0) {
      this.pactSymtomsAndSubstance.pastAlcoholSobriety = this.substanceGroup.get('pastAlcoholSobrietyTypeCtrl').value;
    } else { this.pactSymtomsAndSubstance.pastAlcoholSobriety = null; }

    if (this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.pastAlcoholSobrietyDate = this.datePipe.transform(this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').value, 'MM/dd/yyyy');
    } else {
      this.pactSymtomsAndSubstance.pastAlcoholSobrietyDate = null;
    }

    if (this.substanceGroup.get('pastDrugSobrietyTypeCtrl').value != 0) {
      this.pactSymtomsAndSubstance.pastDrugSobriety = this.substanceGroup.get('pastDrugSobrietyTypeCtrl').value;
    } else { this.pactSymtomsAndSubstance.pastDrugSobriety = null; }

    if (this.substanceGroup.get('pastDrugSobrietyDateCtrl').value != null) {
      this.pactSymtomsAndSubstance.pastDrugSobrietyDate = this.datePipe.transform(this.substanceGroup.get('pastDrugSobrietyDateCtrl').value, 'MM/dd/yyyy');
    } else { this.pactSymtomsAndSubstance.pastDrugSobrietyDate = null; }
  }

  //Recently Used Radio Button
  setSubstanceUseRecentlyRadio(value: string) {
    this.IsSubstanceUsedRecentlySelectedValue = value;
    // If Value : Yes
    if (value == '1') {
      this.substanceGroup.controls.substanceUsedRecentlyPatternTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.substanceGroup.controls.substanceUsedRecentlyPatternTypeCtrl.updateValueAndValidity();

      // If Value : No
    } else if (value == '0') {
      this.substanceGroup.controls.substanceUsedRecentlyPatternTypeCtrl.clearValidators();
      this.substanceGroup.get('substanceUsedRecentlyPatternTypeCtrl').setValue(0);

      // Reset the checkbox
      while (this.substanceUsedRecentlyPatternTypeArray.length !== 0) {
        this.substanceUsedRecentlyPatternTypeArray.splice(0);
      }

      //Other Specify Textbox clear
      this.IsSubstanceUsedRecentlyPatternOtherSelected = "0";
      this.substanceGroup.controls.substanceUsedRecentOtherSpecifyCtrl.clearValidators();
      this.substanceGroup.get('substanceUsedRecentOtherSpecifyCtrl').setValue("");
    }
    this.reSetTreatmentProgram();
  }

  //Past Used Radio Button
  setSubstanceUsePastRadio(value: string) {
    this.IsSubstanceUsedPastSelectedValue = value;
    // If Value : Yes
    if (value == '1') {
      this.substanceGroup.controls.substanceUsedInPastPatternTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
      this.substanceGroup.controls.substanceUsedInPastPatternTypeCtrl.updateValueAndValidity();

      // If Value : No
    } else if (value == '0') {
      this.substanceGroup.controls.substanceUsedInPastPatternTypeCtrl.clearValidators();
      this.substanceGroup.get('substanceUsedInPastPatternTypeCtrl').setValue(0);

      // Reset the checkbox
      while (this.substanceUsedInPastPatternTypeArray.length !== 0) {
        this.substanceUsedInPastPatternTypeArray.splice(0);
      }

      //Other Specify Checkbox : false , Textbox hide clear
      this.IsSubstanceUsedInPastPatternOtherSelected = "0";
      this.substanceGroup.controls.substanceUsedInPastOtherSpecifyCtrl.clearValidators();
      this.substanceGroup.get('substanceUsedInPastOtherSpecifyCtrl').setValue("");

      // Alcohol checkbox : true , textkbox and dropdown hide and clear
      this.IsSubstanceUsedInPastPatternAlcoholSelected = "0";
      this.substanceGroup.controls.pastAlcoholSobrietyTypeCtrl.clearValidators();
      this.substanceGroup.controls.pastAlcoholSobrietyDateCtrl.clearValidators();
      this.substanceGroup.get('pastAlcoholSobrietyTypeCtrl').setValue(0);
      this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').setValue(null);


      // Past Drug Checkbox : False , Textbox hide and clear
      this.IsSubstanceUsedInPastPatternDrugsSelected = "0";
      this.substanceGroup.controls.pastDrugSobrietyTypeCtrl.clearValidators();
      this.substanceGroup.controls.pastDrugSobrietyDateCtrl.clearValidators();
      this.substanceGroup.get('pastDrugSobrietyTypeCtrl').setValue(0);
      this.substanceGroup.get('pastDrugSobrietyDateCtrl').setValue(null);

    }
    this.reSetTreatmentProgram();
  }

  //Reset Teatment Pogram 
  reSetTreatmentProgram() {
    if (this.IsSubstanceUsedRecentlySelectedValue == "0" && this.IsSubstanceUsedPastSelectedValue == "0") {
      this.treatmentTabStatus = 0;
      this.symptomsGroup.controls['isCurrentlyTreatmentProgramCtrl'].clearValidators();
      this.symptomsGroup.controls['isCurrentlyTreatmentProgramCtrl'].reset();
      this.IsCurrentlyTreatmentProgramSelected = '0';
      this.symptomsGroup.controls.currentTreatmentProgramNameCtrl.clearValidators();
      this.symptomsGroup.controls.currentTreatmentProgramModalityTypeCtrl.clearValidators();
      this.symptomsGroup.controls.currentTreatmentProgramAdmissionDateCtrl.clearValidators();
      this.symptomsGroup.controls['currentTreatmentProgramNameCtrl'].reset();
      this.symptomsGroup.get('currentTreatmentProgramModalityTypeCtrl').setValue(0);
      this.symptomsGroup.controls['currentTreatmentProgramAdmissionDateCtrl'].reset();
      this.IsActivelyParticipatingSelected = "0";
      this.symptomsGroup.controls.isActivelyParticipatingCtrl.clearValidators();
      this.symptomsGroup.controls.explainNonParticipationCtrl.clearValidators();
      this.symptomsGroup.controls['isActivelyParticipatingCtrl'].reset();
      this.symptomsGroup.controls['explainNonParticipationCtrl'].reset();
      this.symptomsGroup.controls['hasCompletedTreatmentProgramCtrl'].reset();
      this.IsHasCompletedTreatmentProgramSelected = "0";
      this.symptomsGroup.controls.completedProgramNameCtrl.clearValidators();
      this.symptomsGroup.controls.currentTreatmentProgramModalityTypeCtrl.clearValidators();
      this.symptomsGroup.controls.completedProgramAdmissionDateCtrl.clearValidators();
      this.symptomsGroup.controls['completedProgramNameCtrl'].reset();
      this.symptomsGroup.get('completedTreatmentProgramModalityTypeCtrl').setValue(0);
      this.symptomsGroup.controls['completedProgramAdmissionDateCtrl'].reset();
    }
  }

  // On Checkbox Change Action method - Recent Use
  onsubstanceUsedRecentlyPatternTypesCheckChange(event: { checked: boolean; }, refGroupDetailId: string) {
    if (event.checked) {
      this.substanceUsedRecentlyPatternTypeArray.push(refGroupDetailId);
      this.onsubstanceUsedRecentlyPatternTypesCheckChangeValidation(true, refGroupDetailId);
    }
    else {
      // this.substanceUsedRecentlyPatternTypeArray.splice(this.substanceUsedRecentlyPatternTypeArray.indexOf(refGroupDetailId), 1);
      this.removeRecentItem(parseInt(refGroupDetailId));
      this.onsubstanceUsedRecentlyPatternTypesCheckChangeValidation(false, refGroupDetailId);
    }
  }

  //Remove Item From Array
  removeRecentItem(refId: number) {
    const index: number = this.substanceUsedRecentlyPatternTypeArray.indexOf(refId);
    if (index !== -1) {
      this.substanceUsedRecentlyPatternTypeArray.splice(index, 1);
    }
  }

  onsubstanceUsedRecentlyPatternTypesCheckChangeValidation(checked: boolean, refGroupDetailId: string) {

    if (checked) {

      // Other checkbox : true , textkbox show
      if (refGroupDetailId == "464") {
        this.IsSubstanceUsedRecentlyPatternOtherSelected = "1";
        this.substanceGroup.controls.substanceUsedRecentOtherSpecifyCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(1)]);
        this.substanceGroup.controls.substanceUsedRecentOtherSpecifyCtrl.updateValueAndValidity();
      }
    } else {

      // Other checkbox : false , textkbox hide
      if (refGroupDetailId == "464") {
        this.IsSubstanceUsedRecentlyPatternOtherSelected = "0";
        this.substanceGroup.controls.substanceUsedRecentOtherSpecifyCtrl.clearValidators();
        this.substanceGroup.get('substanceUsedRecentOtherSpecifyCtrl').setValue("");
      }
    }
  }

  // On Checkbox Change Action method - Past Use
  onsubstanceUsedPastPatternTypesCheckChange(event: { checked: boolean; }, refGroupDetailId: string) {
    if (event.checked) {
      this.substanceUsedInPastPatternTypeArray.push(refGroupDetailId);
      this.onsubstanceUsedPastPatternTypesCheckChangeValidation(true, refGroupDetailId);
    }
    else {
      // this.substanceUsedInPastPatternTypeArray.splice(this.substanceUsedInPastPatternTypeArray.indexOf(refGroupDetailId), 1);
      this.removePastItem(parseInt(refGroupDetailId));
      this.onsubstanceUsedPastPatternTypesCheckChangeValidation(false, refGroupDetailId);
    }
  }

  //Remove Item From Array
  removePastItem(refId: number) {
    const index: number = this.substanceUsedInPastPatternTypeArray.indexOf(refId);
    if (index !== -1) {
      this.substanceUsedInPastPatternTypeArray.splice(index, 1);
    }
  }

  onsubstanceUsedPastPatternTypesCheckChangeValidation(checked: boolean, refGroupDetailId: string) {

    if (checked) {

      // Alcohol checkbox : true , textkbox and dropdown show
      if (refGroupDetailId == "455") {
        this.IsSubstanceUsedInPastPatternAlcoholSelected = "1";

        this.substanceGroup.controls.pastAlcoholSobrietyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
        this.substanceGroup.controls.pastAlcoholSobrietyDateCtrl.setValidators([Validators.required]);
      }
      // Drugs checkbox : true , textkbox and dropdown show
      else if (refGroupDetailId == "456" || refGroupDetailId == "457" || refGroupDetailId == "458" || refGroupDetailId == "459"
        || refGroupDetailId == "460" || refGroupDetailId == "461") {
        this.IsSubstanceUsedInPastPatternDrugsSelected = "1";
        this.substanceGroup.controls.pastDrugSobrietyTypeCtrl.setValidators([Validators.required, CustomValidators.dropdownRequired()]);
        this.substanceGroup.controls.pastDrugSobrietyDateCtrl.setValidators([Validators.required]);
      }
      // Other checkbox : true , textkbox show
      else if (refGroupDetailId == "464") {
        this.IsSubstanceUsedInPastPatternOtherSelected = "1";
        this.substanceGroup.controls.substanceUsedInPastOtherSpecifyCtrl.setValidators([Validators.required, CustomValidators.requiredCharLength(1)]);
        this.substanceGroup.controls.substanceUsedInPastOtherSpecifyCtrl.updateValueAndValidity();
      }

    } else {

      // Alcohol checkbox : true , textkbox and dropdown hide and clear
      if (refGroupDetailId == "455") {
        this.IsSubstanceUsedInPastPatternAlcoholSelected = "0";
        this.substanceGroup.controls.pastAlcoholSobrietyTypeCtrl.clearValidators();
        this.substanceGroup.controls.pastAlcoholSobrietyDateCtrl.clearValidators();
        this.substanceGroup.get('pastAlcoholSobrietyTypeCtrl').setValue(0);
        this.substanceGroup.get('pastAlcoholSobrietyDateCtrl').setValue(null);
      }

      // Other checkbox : false , textkbox hide and clear
      else if (refGroupDetailId == "464") {
        this.IsSubstanceUsedInPastPatternOtherSelected = "0";
        this.substanceGroup.controls.substanceUsedInPastOtherSpecifyCtrl.clearValidators();
        this.substanceGroup.get('substanceUsedInPastOtherSpecifyCtrl').setValue("");
      }
      // Past Drug Checkbox : False , Textbox hide and clear
      else if (this.pastDrugsConditionCheck() == false) {
        this.IsSubstanceUsedInPastPatternDrugsSelected = "0";
        this.substanceGroup.controls.pastDrugSobrietyTypeCtrl.clearValidators();
        this.substanceGroup.controls.pastDrugSobrietyDateCtrl.clearValidators();
        this.substanceGroup.get('pastDrugSobrietyTypeCtrl').setValue(0);
        this.substanceGroup.get('pastDrugSobrietyDateCtrl').setValue(null);
      }
    }
  }

  pastDrugsConditionCheck(): boolean {

    var tmpReturn: boolean = false;

    this.substanceUsedInPastPatternTypeArray.forEach((element) => {

      if (element === 456) {
        tmpReturn = true;
      }
      else if (element === 457) {
        tmpReturn = true;
      }
      else if (element === 458) {
        tmpReturn = true;
      }
      else if (element === 459) {
        tmpReturn = true;
      }
      else if (element === 460) {
        tmpReturn = true;
      }
      else if (element === 461) {
        tmpReturn = true;
      }
    });

    return tmpReturn;
  }

  //#endregion


  // #region  Common Methods of Component

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    if (this.IsSubstanceUsedRecentlySelectedValue == "0" && this.IsSubstanceUsedPastSelectedValue == "0") {
      if (this.selectedTab == 0) {
        // Save the Symptoms and behaviors data withouth validation
        this.saveSymptomsBehaviorsData_NoValidation();
      }
      else if (this.selectedTab == 1) {
        this.saveSubstanceUseData_NoValidation();
        this.saveSymptomsBehaviorsData_NoValidation();
      }
    }
    else {
      if (this.selectedTab == 0 || this.selectedTab == 2) {
        // Save the Symptoms and behaviors data withouth validation
        this.saveSymptomsBehaviorsData_NoValidation();
      }
      else if (this.selectedTab == 1) {
        this.saveSubstanceUseData_NoValidation();
        this.saveSymptomsBehaviorsData_NoValidation();
      }
    }
    this.selectedTab = tabChangeEvent.index;
  }

  onSave_popstate(event: NavigationStart) {
    if (this.selectedTab < 4) {
      if (this.selectedTab == 0 || this.selectedTab == 2) {
        // Save the Symptoms and behaviors data
        if (this.symptomsGroup) {
          if (event.navigationTrigger != "popstate" && this.symptomsGroup.touched) {
            this.saveSymptomsBehaviorsData_NoValidation();
          }
          else if (event.navigationTrigger != "popstate" && this.symptomsGroup.dirty) {
            this.saveSymptomsBehaviorsData_NoValidation();
          }
          if (this.treatmentGroup) {
            if (event.navigationTrigger != "popstate" && this.treatmentGroup.touched) {
              this.saveSymptomsBehaviorsData_NoValidation();
            }
            else if (event.navigationTrigger != "popstate" && this.treatmentGroup.dirty) {
              this.saveSymptomsBehaviorsData_NoValidation();
            }
          }
        }
      }
      else if (this.selectedTab == 1) {
        // Save the Substance Use Tab Data
        // if (!this.SubstanceUseTabDateValidationCheck()) {
        //   return;
        // }

        if (this.substanceGroup) {
          if (event.navigationTrigger != "popstate" && this.substanceGroup.touched) {
            this.saveSubstanceUseData_NoValidation();
            this.saveSymptomsBehaviorsData_NoValidation();
          }
          else if (event.navigationTrigger != "popstate" && this.substanceGroup.dirty) {
            this.saveSubstanceUseData_NoValidation();
            this.saveSymptomsBehaviorsData_NoValidation();
          }
        }
      }
    }
  }

  onSave() {
    if (this.selectedTab < 4) {
      if (this.selectedTab == 0 || this.selectedTab == 2) {
        // Save the Symptoms and behaviors data
        this.saveSymptomsBehaviorsData();
      }
      else if (this.selectedTab == 1) {
        // Save the Substance Use Tab Data
        // if (!this.SubstanceUseTabDateValidationCheck()) {
        //   return;
        // }
        this.saveSubstanceUseData();
        this.saveSymptomsBehaviorsData_NoValidation();
      }
    }
  }

  // Next button click
  nextPage() {
    if (this.IsSubstanceUsedRecentlySelectedValue == "0" && this.IsSubstanceUsedPastSelectedValue == "0") {
      if (this.selectedTab == 0) {
        this.saveSymptomsBehaviorsData_NoValidation();
      }
      else if (this.selectedTab == 1) {
        this.saveSubstanceUseData_NoValidation();
        this.saveSymptomsBehaviorsData_NoValidation();
      }
      else if (this.selectedTab == 2) {
        this.sidenavStatusService.routeToNextPage(this.router, this.activatedRoute);
      }
    }
    else {
      if (this.selectedTab == 0 || this.selectedTab == 2) {
        this.saveSymptomsBehaviorsData_NoValidation();
      }
      else if (this.selectedTab == 1) {
        this.saveSubstanceUseData_NoValidation();
        this.saveSymptomsBehaviorsData_NoValidation();
      }
      else if (this.selectedTab == 3) {
        this.sidenavStatusService.routeToNextPage(this.router, this.activatedRoute);
      }
    }
    this.selectedTab = this.selectedTab + 1;
  }

  // Previous button click
  previousPage() {
    if (this.selectedTab > 0) {
      if (this.IsSubstanceUsedRecentlySelectedValue == "0" && this.IsSubstanceUsedPastSelectedValue == "0") {
        if (this.selectedTab == 0) {
          this.saveSymptomsBehaviorsData_NoValidation();
          this.sidenavStatusService.routeToPreviousPage(this.router, this.activatedRoute);
        }
        else if (this.selectedTab == 1) {
          this.saveSubstanceUseData_NoValidation();
          this.saveSymptomsBehaviorsData_NoValidation();
        }
      }
      else {
        if (this.selectedTab == 0 || this.selectedTab == 2) {
          this.saveSymptomsBehaviorsData_NoValidation();
          if (this.selectedTab == 0) {
            this.sidenavStatusService.routeToPreviousPage(this.router, this.activatedRoute);
          }
        }
        else if (this.selectedTab == 1) {
          this.saveSubstanceUseData_NoValidation();
          this.saveSymptomsBehaviorsData_NoValidation();
        }
      }
      this.selectedTab = this.selectedTab - 1;
    }
    else {
      if (this.selectedTab == 0) {
        // Save the Symptoms and behaviors data withouth validation
        this.saveSymptomsBehaviorsData_NoValidation();
        // redirect the user to previous menu item
        this.sidenavStatusService.routeToPreviousPage(this.router, this.activatedRoute);
      }
    }
  }

  onUploadFinish = (event: string) => {
    //  this.uploadFinishMessage = event;
    // console.log(event);
  }
  // #endregion
}
