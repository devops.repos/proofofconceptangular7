import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit, OnDestroy {
  uploadFinishMessage: string;
  pactApplicationID: number;

  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private sidenavStatusService: SidenavStatusService,
    private router: Router,
  ) { }

  ngOnInit() {
    /** Setting the sidenav Status for the application with the given ID from URL router parameter
     * This is the case for page refresh or reload
     */
    this.activatedRouteSub = this.route.paramMap.subscribe((params) => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.pactApplicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);
        }
      }
    });
  }

  onDocDataChanged = (event: string) => {
    this.uploadFinishMessage = event;
    if (this.pactApplicationID) {
      this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);
    }
  }

  PreviousPage = () => {
    this.sidenavStatusService.routeToPreviousPage(this.router, this.route);
  }

  NextPage = () => {
    this.sidenavStatusService.routeToNextPage(this.router, this.route);
  }

  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
  }

}
