import { Component, OnInit, HostListener } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { MatTabChangeEvent } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { appStatusColor } from 'src/app/models/pact-enums.enum';
import { RefGroupDetails } from '../../../models/refGroupDetailsDropDown.model';
import { HousingPreferenceService } from '../housing-preferences/housing-preferences.service';
import { HousingPreferencesModel, HousingLevelItem, Items, RecommendedServices } from './housing-preferences.model';
import { ApplicantPreference } from 'src/app/shared/applicant-preferences/applicant-preferences.model';
import { UserService } from 'src/app/services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';

@Component({
  selector: 'app-housing-preferences',
  templateUrl: './housing-preferences.component.html',
  styleUrls: ['./housing-preferences.component.scss']
})

export class HousingPreferencesComponent implements OnInit {

  applicationID: number;
  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  tab1Status: number = 2;
  tab2Status: number = 2;
  tab3Status: number = 2;
  userData: AuthData;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  selectedTab = 0;

  recommServiceGroup: FormGroup;
  responseItem: RefGroupDetails[];
  housingLevels: RefGroupDetails[];
  communityCare : HousingLevelItem[]
  levelI : HousingLevelItem[]
  levelII : HousingLevelItem[]
  housingPreferences : HousingPreferencesModel;
  psychosocialSummary  : HousingLevelItem[]
  appPreference : ApplicantPreference;
  boroughOptions : RefGroupDetails[]=[]
  apartmentOptions : RefGroupDetails[]
  recommOther : boolean
  routeSub: any;
  isPageTouched : boolean = false;
  showCtrl : boolean
  selectedItem : string

  constructor(
    private router: Router,
    private sidenavStatusService: SidenavStatusService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private housingPreferencesService: HousingPreferenceService,
    private userService: UserService
    ) { }

   @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
    if (this.isPageTouched) {
     this.save(false);
    }
  }
  ngOnInit() {
    this.activatedRouteSub = this.route.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.applicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);

          this.OnInit();
        }
      }
    });

    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.save(false);
        this.isPageTouched = false;
      }
    });
  }


  ngOnDestroy() {
    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }
    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }
    this.housingPreferences = null;
  }

  OnInit() {
    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });
    this.getHousingPreferences();
    this.getApplicantPrefernces();
  };

  onChange (itm: any): void {
    this.isPageTouched = true;
    this.initTab2Status();
  }

  onApplicantPrefChange (appPref : ApplicantPreference) {
    this.isPageTouched = true;
    this.setApplicantPreferences(appPref);
  }

  onRecommendedValueChange (lst: RecommendedServices): void {
    this.isPageTouched = true;
    this.housingPreferences.housingPreference.recommendedServices = lst;
    this.initTab3Status();
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.save(false);
    this.selectedTab = tabChangeEvent.index;
  }

  onSave() {
    this.save(true);
  }

  ctrlChanged() {
    this.isPageTouched =true;
  }

  nextPage() {
    if (this.selectedTab < 2) {
      this.selectedTab = this.selectedTab + 1;
    } else if (this.selectedTab == 2) {
      this.sidenavStatusService.routeToNextPage(this.router, this.route);
    }
     this.save(false);
  }

  previousPage() {
    if (this.selectedTab != 0) {
      this.selectedTab = this.selectedTab - 1;
    } else if (this.selectedTab == 0) {
      this.sidenavStatusService.routeToPreviousPage(this.router, this.route);
    }
     this.save(false);
  }
  getApplicantPrefernces() {
    if(this.housingPreferences  != undefined) {
      this.appPreference = {
        preference1 : this.housingPreferences.housingPreference.applicantPreference.borough[0].preference,
        preference2 : this.housingPreferences.housingPreference.applicantPreference.borough[1].preference,
        excludePreference: this.housingPreferences.housingPreference.applicantPreference.borough[2].preference,
        serviceLocated : this.housingPreferences.housingPreference.applicantPreference.borough[3].preference,
        excludeComments : this.housingPreferences.housingPreference.applicantPreference.borough[2].comments,

        aptQuestion1 : this.housingPreferences.housingPreference.applicantPreference.apartment[0].preference,
        aptQuestion2 : this.housingPreferences.housingPreference.applicantPreference.apartment[1].preference,
        aptQuestion3 : this.housingPreferences.housingPreference.applicantPreference.apartment[2].preference,
        aptQuestion4 : this.housingPreferences.housingPreference.applicantPreference.apartment[3].preference,
        aptQuestion5 : this.housingPreferences.housingPreference.applicantPreference.apartment[4].preference,
        aptQuestion6 : this.housingPreferences.housingPreference.applicantPreference.apartment[5].preference,
        aptQuestion7 : this.housingPreferences.housingPreference.applicantPreference.apartment[6].preference,
        aptQuestion8 : this.housingPreferences.housingPreference.applicantPreference.apartment[7].preference,
        aptQuestion1Comment : this.housingPreferences.housingPreference.applicantPreference.apartment[0].comments,
        aptQuestion8Comment : this.housingPreferences.housingPreference.applicantPreference.apartment[7].comments,
        primaryLanguage : this.housingPreferences.housingPreference.primaryLanguage.length > 1 ?
        '('+this.housingPreferences.housingPreference.primaryLanguage+')' : ''
    }
  };
}

  setApplicantPreferences (appPreference : ApplicantPreference) {
    this.housingPreferences.housingPreference.applicantPreference.borough[0].preference = appPreference.preference1;
    this.housingPreferences.housingPreference.applicantPreference.borough[1].preference = appPreference.preference2;
    this.housingPreferences.housingPreference.applicantPreference.borough[2].preference = appPreference.excludePreference;
    this.housingPreferences.housingPreference.applicantPreference.borough[3].preference = appPreference.serviceLocated;
    this.housingPreferences.housingPreference.applicantPreference.borough[2].comments = appPreference.excludeComments;

    this.housingPreferences.housingPreference.applicantPreference.apartment[0].preference = appPreference.aptQuestion1;
    this.housingPreferences.housingPreference.applicantPreference.apartment[1].preference = appPreference.aptQuestion2;
    this.housingPreferences.housingPreference.applicantPreference.apartment[2].preference = appPreference.aptQuestion3;
    this.housingPreferences.housingPreference.applicantPreference.apartment[3].preference = appPreference.aptQuestion4;
    this.housingPreferences.housingPreference.applicantPreference.apartment[4].preference = appPreference.aptQuestion5;
    this.housingPreferences.housingPreference.applicantPreference.apartment[5].preference = appPreference.aptQuestion6;
    this.housingPreferences.housingPreference.applicantPreference.apartment[6].preference = appPreference.aptQuestion7;
    this.housingPreferences.housingPreference.applicantPreference.apartment[7].preference = appPreference.aptQuestion8;
    this.housingPreferences.housingPreference.applicantPreference.apartment[0].comments = appPreference.aptQuestion1Comment;
    this.housingPreferences.housingPreference.applicantPreference.apartment[7].comments = appPreference.aptQuestion8Comment;
    this.initTab1Status();
  }
  getHousingPreferences() {
      this.housingPreferencesService.getHousingPreference(this.applicationID).subscribe(
      res => {
        const data = res as HousingPreferencesModel;
        this.housingPreferences = data;
        this.getApplicantPrefernces();
        if(this.boroughOptions.length < 1){
          this.housingPreferences.boroughOptions.filter(x => x.refGroupDetailID == 29).forEach(x => this.boroughOptions.push(x))
          this.housingPreferences.boroughOptions.filter(x => x.refGroupDetailID != 29).forEach(x => this.boroughOptions.push(x));
        }
        this.apartmentOptions = this.housingPreferences.apartmentOptions;

        this.communityCare = this.housingPreferences.housingPreference.housingLevel.communityCare;
        this.levelI = this.housingPreferences.housingPreference.housingLevel.levelI;
        this.levelII = this.housingPreferences.housingPreference.housingLevel.levelII;
        this.psychosocialSummary = this.housingPreferences.housingPreference.recommendedServices.psychosocialSummary.filter(x => x.typeId != 390);
        this.recommOther = this.housingPreferences.housingPreference.recommendedServices.psychosocialSummary.filter(x => x.typeId === 390 && x.id === 0).length > 0 ? false : true;
         if(this.housingPreferences.housingPreference.id != 0){
          this.initTab1Status();
          this.initTab2Status();
          this.initTab3Status();
         }
      },
      error => {
        console.log(error);
      }
    );
  }

  save(isSave : boolean) {
    if (this.isPageTouched === true) {
        this.housingPreferences.housingPreference.isApplicantPreferenceTabComplete = this.tab1Status == 1 ? 1 : 0;
        this.housingPreferences.housingPreference.isHousingLevelTabComplete = this.tab2Status == 1 ? 1 : 0;
        this.housingPreferences.housingPreference.isRecommendedServicesTabComplete = this.tab3Status == 1 ? 1 : 0;
        if (this.housingPreferences.housingPreference.id == 0){
          this.housingPreferences.housingPreference.id = this.applicationID
        }
        this.housingPreferences.housingPreference.userId = this.userData.optionUserId;

        this.housingPreferencesService.saveHousingPreference(this.housingPreferences.housingPreference).subscribe(
          result => {
            if (this.applicationID) {
              this.sidenavStatusService.setApplicationIDForSidenavStatus(this.applicationID);
            }

            if (isSave && result > 0){
              this.toastr.success('Housing Preferences has been saved successfully!', 'Saved..');
              this.isPageTouched = false;
              this.getHousingPreferences();
            }
          },
          error => {
          console.log(error);
          this.isPageTouched = false;
          });
    }
  }

  initTab1Status() {
    this.tab1Status = this.housingPreferences.housingPreference.applicantPreference.borough.filter(x => x.preference == null || x.preference == 0).length == 0 &&
    this.housingPreferences.housingPreference.applicantPreference.apartment.filter(x => x.preference == null || x.preference == 0).length == 0 ? 1 : 0
    if((this.appPreference.excludePreference != 0 && this.appPreference.excludePreference != null
      && this.appPreference.excludePreference != 503) && (this.appPreference.excludeComments == null
      || this.appPreference.excludeComments.length < 2)){
        this.tab1Status = 0;
      }

    if(this.appPreference.aptQuestion1 == 34 && (this.appPreference.aptQuestion1Comment == null
      || this.appPreference.aptQuestion1Comment.length < 2))
      {
        this.tab1Status = 0;
      }

    if(this.appPreference.aptQuestion8 == 33 && (this.appPreference.aptQuestion8Comment == null
      || this.appPreference.aptQuestion8Comment.length < 2))
      {
        this.tab1Status = 0;
      }
  }

  initTab2Status() {
    if (this.communityCare.filter(x => x.id != 0).length > 0 ||
        this.levelI.filter(x => x.id != 0).length > 0 ||
        this.levelII.filter(x => x.id != 0).length > 0 ){
          this.tab2Status = 1;
          if (this.communityCare.filter(x => x.typeId ==393 && x.id != 0 && (x.other == null || x.other.length < 2)).length > 0
          || this.levelI.filter(x => x.typeId ==396 && x.id != 0 && (x.other == null || x.other.length < 2)).length > 0
          || this.levelII.filter(x => x.typeId ==405 && x.id != 0 && (x.other == null || x.other.length < 2)).length > 0){
            this.tab2Status = 0;
          }
    }else{
      this.tab2Status = 0;
    }
  }

  initTab3Status() {
    var itm =  this.housingPreferences.housingPreference.recommendedServices.psychosocialSummary.find(x => x.typeId == 390) as HousingLevelItem

    if (this.psychosocialSummary.filter( x => x.id != 0).length > 0){
      this.tab3Status = 1;
    }
    else{
      this.tab3Status = 0;
    }

    if(itm.id != 0 && (itm.other == null || itm.other.length < 2 )){
      this.tab3Status = 0;
    }else if(itm.id != 0 && (itm.other != null || itm.other.length > 1 )){
      this.tab3Status = 1;
    }
  }
  displayItem (i : number): string{
    if(this.housingPreferences.housingPreference.recommendedServices.psychosocialSummary.filter(z => z.typeId == i).length > 0){
      return this.housingPreferences.housingPreference.recommendedServices.psychosocialSummary.filter(z => z.typeId == i)[0].description;
    }
  }
}
