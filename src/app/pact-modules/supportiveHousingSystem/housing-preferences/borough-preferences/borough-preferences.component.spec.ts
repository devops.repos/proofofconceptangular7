import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoroughPreferencesComponent } from './borough-preferences.component';

describe('BoroughPreferencesComponent', () => {
  let component: BoroughPreferencesComponent;
  let fixture: ComponentFixture<BoroughPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoroughPreferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoroughPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
