import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Items } from '../housing-preferences.model';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

@Component({
  selector: 'app-borough-preferences',
  templateUrl: './borough-preferences.component.html',
  styleUrls: ['./borough-preferences.component.scss']
})
export class BoroughPreferencesComponent implements OnInit {

  @Input()  id : string;
  @Input()  item : any;
  @Input()  showComments : boolean;
  @Output()
  onValueChange : EventEmitter<any> = new EventEmitter<any>();

  
  boroughGroup : FormGroup;
  constructor(
    private formBuilder: FormBuilder) {
    this.boroughGroup = this.formBuilder.group({
      rdoCtrl: [''],
      // txtareaCtrl: ['']
      txtareaCtrl: ['',[CustomValidators.requiredCharLength(2)]]
    });
   }

  ngOnInit() {
    console.log('borough init triggered');
    this.boroughGroup.get('rdoCtrl').setValue(this.item.preference)
    this.boroughGroup.get('txtareaCtrl').setValue(this.item.comments)
  }

  ngOnChanges() {
    console.log('ngOnChanges');
  }

  onChange (item : any, selectedItem: number) {
    console.log("radio button clicked!");
    console.log(this.id);
    console.log(item);
    console.log(selectedItem);
    const itm : any = {
      id : this.id,
      item : item,
      selectedItem : selectedItem
    }
    this.onValueChange.emit(itm);
  }
  
  onTextChange () {
    console.log("text value changed!");
    console.log(this.boroughGroup.get('txtareaCtrl').value);  
    this.item.comments = this.boroughGroup.get('txtareaCtrl').value;
    this.onValueChange.emit(this.item);
  }
}
