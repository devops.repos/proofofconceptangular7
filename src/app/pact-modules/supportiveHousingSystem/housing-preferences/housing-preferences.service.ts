import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HousingPreference } from './housing-preferences.model'

@Injectable({
  providedIn: 'root'
})
export class HousingPreferenceService {
  saveHousingPreferenceURL = environment.pactApiUrl + 'HousingPreferences/Save';
  getHousingPreferenceURL = environment.pactApiUrl + 'HousingPreferences/GetHousingPreferences';
  getRefGroupDetailsURL = environment.pactApiUrl + 'Common';
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private httpClient: HttpClient) { }
  
  getHousingPreference(id : number) : Observable<any>
  {
    return this.httpClient.get(this.getHousingPreferenceURL+"/"+id, this.httpOptions);
  }

  saveHousingPreference(housingPreference : HousingPreference) : Observable<any>
  {
    return this.httpClient.post(this.saveHousingPreferenceURL, JSON.stringify(housingPreference), this.httpOptions);
  }  
}

