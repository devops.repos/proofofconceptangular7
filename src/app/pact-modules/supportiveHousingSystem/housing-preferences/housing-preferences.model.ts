import { RefGroupDetails } from '../../../models/refGroupDetailsDropDown.model';

 export interface Items {
    id : number
    name : string
    comments : string
    preference : number
  }

  export interface HousingPreference {
    applicantPreference :ApplicantPreference
    housingLevel :HousingLevel
    recommendedServices : RecommendedServices    
    id : number
    userId? : number;
    primaryLanguage : string
    isApplicantPreferenceTabComplete : number
    isHousingLevelTabComplete : number
    isRecommendedServicesTabComplete : number
  }

  export interface HousingPreferencesModel {
    boroughOptions : RefGroupDetails[]
    boroughExcludeOptions : RefGroupDetails[]
    apartmentOptions : RefGroupDetails[]
    apartmentNoPreferenceOptions : RefGroupDetails[]

    housingPreference : HousingPreference
  }

  export interface ApplicantPreference {
    borough : Items[]
    apartment : Items[]
    housing : Items[]
    housingLevel : HousingLevel
  }
  export interface ApplicantPref {
    preference1? : number,
    preference2? : number,
    excludePreference? : number,
    excludeComments? : string,
    serviceLocated?: number,
    aptQuestion1? : number,
    aptQuestion1Comment? : string,    
    aptQuestion2? : number,
    aptQuestion3? : number,
    aptQuestion4? : number,
    aptQuestion5? : number,
    aptQuestion6? : number,
    aptQuestion7? : number,
    aptQuestion8? : number,
    aptQuestion8Comment? : string

  }
  export interface HousingLevel {
    communityCare : HousingLevelItem[]
    levelI : HousingLevelItem[]
    levelII : HousingLevelItem[]
  }

  export interface HousingLevelItem {
    id : number
    typeId : number
    description : string
    other : string
  }

  export interface RecommendedServices {
    psychosocialSummary : HousingLevelItem[]
  }