import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApartmentPreferencesComponent } from './apartment-preferences.component';

describe('ApartmentPreferencesComponent', () => {
  let component: ApartmentPreferencesComponent;
  let fixture: ComponentFixture<ApartmentPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApartmentPreferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApartmentPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
