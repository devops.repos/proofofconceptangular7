import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-apartment-preferences',
  templateUrl: './apartment-preferences.component.html',
  styleUrls: ['./apartment-preferences.component.scss']
})
export class ApartmentPreferencesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  item : any[] = ['Yes','No','No Preference'];
}
