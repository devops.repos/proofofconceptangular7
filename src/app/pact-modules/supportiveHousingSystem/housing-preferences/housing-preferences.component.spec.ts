import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HousingPreferencesComponent } from './housing-preferences.component';

describe('HousingPreferencesComponent', () => {
  let component: HousingPreferencesComponent;
  let fixture: ComponentFixture<HousingPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HousingPreferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousingPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
