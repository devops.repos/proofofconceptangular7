import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CustomValidators } from 'src/app/shared/custom-validator/custom.validators';

@Component({
  selector: 'app-housing-level',
  templateUrl: './housing-level.component.html',
  styleUrls: ['./housing-level.component.scss']
})
export class HousingLevelComponent implements OnInit {

  @Input()  id : string;
  @Input()  item : any;
  @Output()
  onValueChange : EventEmitter<any> = new EventEmitter<any>();

  housingLevelGroup : FormGroup;
  showCtrl : boolean 
  constructor(
    private formBuilder: FormBuilder) {
    this.housingLevelGroup = this.formBuilder.group({
      chkboxCtrl: [''],
      txtareaCtrl: ['',[CustomValidators.requiredCharLength(2)]]
    });
   }

  ngOnInit() {
    this.housingLevelGroup.get('chkboxCtrl').setValue(this.item.id != 0);
    if (this.item.id != 0) {
      this.housingLevelGroup.get('txtareaCtrl').enable();
      this.housingLevelGroup.get('txtareaCtrl').setValue(this.item.other);
      this.showCtrl = true;
    }else {      
        this.housingLevelGroup.get('txtareaCtrl').disable();
        this.showCtrl = false;
    }
  }

  onChange ($event) {
    if ($event.checked){
      this.item.id = this.item.typeId;
      this.housingLevelGroup.get('txtareaCtrl').enable();
      this.showCtrl = true;
    } else{
      this.item.id = 0;
      this.housingLevelGroup.get('txtareaCtrl').setValue('');
      this.housingLevelGroup.get('txtareaCtrl').disable();
      this.showCtrl = false;
        if(this.item.typeId == 393 || this.item.typeId == 396 || this.item.typeId == 405){
          this.item.other = ''
        }
    }
    this.onValueChange.emit(this.item);
  }

  onTextChange () { 
    this.item.other = this.housingLevelGroup.get('txtareaCtrl').value;
    this.onValueChange.emit(this.item);
  }
}
