import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HousingLevelComponent } from './housing-level.component';

describe('HousingLevelComponent', () => {
  let component: HousingLevelComponent;
  let fixture: ComponentFixture<HousingLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HousingLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousingLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
