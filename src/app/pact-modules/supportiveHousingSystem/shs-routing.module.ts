import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PactLandingPageComponent } from 'src/app/core/pact-landing-page/pact-landing-page.component';
import { NewAppComponent } from './new-app/new-app.component';
import { ConsentSearchComponent } from './consent-search/consent-search.component';
import { DemographicsComponent } from './demographics/demographics.component';
import { HousingHomelessComponent } from './housing-homeless/housing-homeless.component';
import { ClinicalAssessmentComponent } from './clinical-assessment/clinical-assessment.component';
import { AdlsComponent } from './adls/adls.component';
// tslint:disable-next-line: max-line-length
import { MedicationsProvidersHospitalizationComponent } from './medications-providers-hospitalization/medications-providers-hospitalization.component';
import { TraumaChildWelfareComponent } from './trauma-child-welfare/trauma-child-welfare.component';
import { SymptomsSubstanceUseComponent } from './symptoms-substance-use/symptoms-substance-use.component';
import { HousingPreferencesComponent } from './housing-preferences/housing-preferences.component';
import { PsychiatricPsychosocialMhrComponent } from './psychiatric-psychosocial-mhr/psychiatric-psychosocial-mhr.component';
import { DocumentsComponent } from './documents/documents.component';
import { SummaryTransmitComponent } from './summary-transmit/summary-transmit.component';
import { PendingAppsComponent } from './pending-apps/pending-apps.component';
import { TransmittedAppsComponent } from './transmitted-apps/transmitted-apps.component';
import { SiteTypeMasterAuthGuard } from 'src/app/services/auth/site-type-auth-guards/site-type-master-auth.guard';
import { SiteTypeGuard, MixedAuthGuard } from 'src/app/models/pact-enums.enum';
import { IsCompleteStatusCheckComponent } from './is-complete-status-check/is-complete-status-check.component';
import { PendingAppComponent } from './pending-app/pending-app.component';
// import { NotificationResolver } from 'src/app/services/resolvers/notification.resolver';

const shsRoutes: Routes = [
  {
    path: '',
    component: PactLandingPageComponent,
    // resolve: { notifications: NotificationResolver},
    children: [
      {
        path: 'shs',
        canActivate: [SiteTypeMasterAuthGuard],
        data: {
          guards: [
            SiteTypeGuard.SH_RA,
            SiteTypeGuard.SH_HP,
            SiteTypeGuard.CAS
          ]
        },
        // canActivate: [ShRaAuthGuard],
        children: [
          {
            path: 'newApp',
            component: NewAppComponent,
            canActivate: [SiteTypeMasterAuthGuard],
            data: {
              guards: [
                SiteTypeGuard.SH_RA,
                SiteTypeGuard.SH_HP,
                MixedAuthGuard.CAS_SYS_ADMIN
              ]
            },
            children: [
              { path: '', component: ConsentSearchComponent },
              { path: ':assessmentId', component: ConsentSearchComponent },
              { path: 'consent-search/:applicationID', component: ConsentSearchComponent },
              { path: 'demographics/:applicationID', component: DemographicsComponent },
              { path: 'housing-homeless/:applicationID', component: HousingHomelessComponent },
              {
                path: 'clinical-assessment/:applicationID',
                component: ClinicalAssessmentComponent
              },
              { path: 'adls/:applicationID', component: AdlsComponent },
              {
                path: 'medications-providers-and-hospitalization/:applicationID',
                component: MedicationsProvidersHospitalizationComponent
              },
              {
                path: 'trauma-child-welfare/:applicationID',
                component: TraumaChildWelfareComponent
              },
              {
                path: 'symptoms-substance-use/:applicationID',
                component: SymptomsSubstanceUseComponent
              },
              {
                path: 'housing-preferences/:applicationID',
                component: HousingPreferencesComponent
              },
              {
                path: 'psychiatric-psychosocial-mhr/:applicationID',
                component: PsychiatricPsychosocialMhrComponent
              },
              { path: 'documents/:applicationID', component: DocumentsComponent },
              { path: 'summary-transmit/:applicationID', component: SummaryTransmitComponent },
              { path: 'is-complete-status-check/:applicationID', component: IsCompleteStatusCheckComponent }
            ]
          },
          {
            path: 'pendingApp',
            component: PendingAppComponent,
            children: [
              { path: '', component: PendingAppsComponent },
              { path: 'consent-search/:applicationID', component: ConsentSearchComponent },
              { path: 'demographics/:applicationID', component: DemographicsComponent },
              { path: 'housing-homeless/:applicationID', component: HousingHomelessComponent },
              {
                path: 'clinical-assessment/:applicationID',
                component: ClinicalAssessmentComponent
              },
              { path: 'adls/:applicationID', component: AdlsComponent },
              {
                path: 'medications-providers-and-hospitalization/:applicationID',
                component: MedicationsProvidersHospitalizationComponent
              },
              {
                path: 'trauma-child-welfare/:applicationID',
                component: TraumaChildWelfareComponent
              },
              {
                path: 'symptoms-substance-use/:applicationID',
                component: SymptomsSubstanceUseComponent
              },
              {
                path: 'housing-preferences/:applicationID',
                component: HousingPreferencesComponent
              },
              {
                path: 'psychiatric-psychosocial-mhr/:applicationID',
                component: PsychiatricPsychosocialMhrComponent
              },
              { path: 'documents/:applicationID', component: DocumentsComponent },
              { path: 'summary-transmit/:applicationID', component: SummaryTransmitComponent },
              { path: 'is-complete-status-check/:applicationID', component: IsCompleteStatusCheckComponent }
            ]
          },
          { path: 'transmittedApps', component: TransmittedAppsComponent },
          { path: 'transmittedApps/:applicationID', component: TransmittedAppsComponent }
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(shsRoutes)],
  exports: [RouterModule],
  // providers: [NotificationResolver]
})
export class ShsRoutingModule { }
