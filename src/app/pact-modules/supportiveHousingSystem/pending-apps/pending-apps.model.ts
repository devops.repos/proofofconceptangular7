export interface PendingAppsList {
    clientName : string;
    clientNumber : number;
    surveyNumber: number;
    applicationType: string;
    applicationNumber: number;
    dateEntered: Date;
    dateExpires: Date;
    enteredBy: string;
    agency: string;
    site: string;
    isAppl: string;
    isMhr: string;
    isPsychEval: string;
    isPsychSocial: string;
    isPackageComplete: string;
    firstName: string;
    lastName: string;
    dob: string;
    cin: string;
    ssn: string;
    pactClientId: number;
    isYoungAdultTurningAdult?: boolean;
    optionUserId: number;
    userId : number;
  }

  export interface AgencyData {
    agencyID:number;
    agencyNo:string;
    name:string;
    agencyType:string;
    agencyAddress:string;
    city:string;
    state:string;
    zip:string;
    userId?:number;
    userName: string;
  }

  export interface pendingApplicationListParameters {
    optionUserID: number;
    agencyID: number;
}