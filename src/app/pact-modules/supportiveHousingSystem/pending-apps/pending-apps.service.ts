import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { PendingAppsComponent } from './pending-apps.component';
import { PendingAppsList, pendingApplicationListParameters } from './pending-apps.model';
import { environment } from '../../../../environments/environment';
import { Subject, Observable, BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class PendingAppsService {

  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    }),
    response : "json",
  };
    
  constructor(private httpClient: HttpClient) { }

  public pendingAppParams: pendingApplicationListParameters;
  getPendingApplicationListUrl = environment.pactApiUrl + 'PendingApplications/GetPendingApplicationList';
  deletePendingApplicationListItemUrl = environment.pactApiUrl + 'PendingApplications/DeletePendingApplicationListItem';

   getPendingApplicationList(optionUserID : number, selectedAgency : number) : Observable<any> {
    this.pendingAppParams = { optionUserID: optionUserID, agencyID: selectedAgency};
    return this.httpClient.post(this.getPendingApplicationListUrl, JSON.stringify(this.pendingAppParams), this.httpOptions);
  }

  deletePendingApplicationListItem(pendingApplicationList : PendingAppsList) : Observable<any>
  {
    return this.httpClient.post(this.deletePendingApplicationListItemUrl, JSON.stringify(pendingApplicationList), this.httpOptions);
  }

}
