import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { PendingAppsList } from './pending-apps.model';
import { UserService } from '../../../services/helper-services/user.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { MatDialog } from '@angular/material';
import { CommonService } from '../../../services/helper-services/common.service';
import { ClientDocumentsDialogComponent } from 'src/app/shared/client-documents-dialog/client-documents-dialog.component';
import { SummaryTransmitService } from '../summary-transmit/summary-transmit.service';
// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work

export interface iClientDocumentsData {
  firstName: string;
  lastName: string;
  dob: string;
  cin: string;
  ssn: string;
  pactClientId: number;
  pactClientSearchAuditId: number;
  optionUserId: number;
}

@Component({
  selector: "pending-apps-action",
  template: `
    <mat-icon
      class="pendingMenu-icon"
      color="warn"
      [matMenuTriggerFor]="pendingAppsAction"
    >
      more_vert
    </mat-icon>
    <mat-menu #pendingAppsAction="matMenu">
      <button mat-menu-item (click)="navigateToApplication()" [disabled]="isCASAgency == true" >Application</button>
      <button mat-menu-item (click)="showClientApplicationDocuments()"> Client Documents</button>
      <button mat-menu-item routerLink="/shs/pendingApp/documents/{{selectedApplicationID}}" [disabled]="isCASAgency == true">Attach Documents</button>
      <button mat-menu-item (click)="navigateToSummaryTransmit(0)" routerLink="/shs/pendingApp/summary-transmit/{{selectedApplicationID}}" [disabled]="isCASAgency == true">Summary</button>
      <button mat-menu-item (click)="navigateToSummaryTransmit(1)" routerLink="/shs/pendingApp/summary-transmit/{{selectedApplicationID}}" [disabled]="isCASAgency == true">Transmit</button>
      <button mat-menu-item (click)="deleteSelectedPendingApplication()" [disabled]="isCASAgency == true">Delete</button>
    </mat-menu>
  `,
  styles: [
    `
      .pendingMenu-icon {
        cursor: pointer;
      }
      .btn {
        line-height: 0.5;
        width: 100%;
      }
    `
  ]
})

export class PendingAppsActionComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  private applicationSelected: PendingAppsList;
  selectedApplicationID: any;
  userData: AuthData;
  clientDocumentsData: iClientDocumentsData = { firstName: null, lastName: null, dob: null, cin: null, ssn: null, pactClientId: 0, pactClientSearchAuditId: 0, optionUserId: null };
  isCASAgency;

  constructor(
    public dialog: MatDialog
    , private userService: UserService
    , private summaryTransmitService: SummaryTransmitService
    , private commonService: CommonService
  ) { }

  agInit(params: any): void {

    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    this.params = params;
    this.selectedApplicationID = this.params.data.applicationNumber;
    this.cell = { row: params.node.data, col: params.colDef.headerName };
    this.isCASAgency = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
  }

  deleteSelectedPendingApplication() {

    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    this.applicationSelected = {
      clientName: this.params.data.clientName,
      clientNumber: this.params.data.clientNumber,
      surveyNumber: this.params.data.surveyNumber,
      applicationType: this.params.data.applicationType,
      applicationNumber: this.params.data.applicationNumber,
      dateEntered: this.params.data.dateEntered,
      dateExpires: this.params.data.dateExpires,
      enteredBy: this.params.data.enteredBy,
      agency: this.params.data.agency,
      site: this.params.data.site,
      isAppl: this.params.data.isAppl,
      isMhr: this.params.data.isMhr,
      isPsychEval: this.params.data.isPsychEval,
      isPsychSocial: this.params.data.isPsychSocial,
      isPackageComplete: this.params.data.isPackageComplete,
      userId: this.params.data.userId,
      firstName: this.params.data.firstName,
      lastName: this.params.data.lastName,
      dob: this.params.data.dob,
      cin: this.params.data.cin,
      ssn: this.params.data.ssn,
      pactClientId: this.params.data.pactClientId == null ? 0 : this.params.data.pactClientId,
      optionUserId: this.userData.optionUserId
    }

    // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
    this.params.context.componentParent.deleteSelectedApplication(this.applicationSelected);

  }
  showClientApplicationDocuments() {

    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    //Set Client Documents Data for Client Documents
    this.clientDocumentsData.firstName = this.params.data.firstName;
    this.clientDocumentsData.lastName = this.params.data.lastName;
    this.clientDocumentsData.dob = this.params.data.dob ? (new Date(this.params.data.dob)).toLocaleDateString() : '';
    this.clientDocumentsData.cin = this.params.data.cin;
    this.clientDocumentsData.ssn = this.params.data.ssn;
    this.clientDocumentsData.pactClientId = this.params.data.pactClientId == null ? 0 : this.params.data.pactClientId;
    this.clientDocumentsData.optionUserId = this.userData.optionUserId;
    this.openClientDocumentDialog();
  }

  //Navigate to application
  navigateToApplication() {
    console.log('navigate to application - ', this.params.data.isYoungAdultTurningAdult );
    if(this.params.data.isYoungAdultTurningAdult){
      console.log('Its true');
      this.params.context.componentParent.showYoungAdultTurningAdultMessage();
    }
    this.params.context.componentParent.navigateToApplication(this.selectedApplicationID);
  }

  //Navigate To Transmit
  navigateToSummaryTransmit(tab: number) {
    this.summaryTransmitService.setSelectedTab(tab);
  }

  //Open Client Documents Dialog
  openClientDocumentDialog(): void {
    this.dialog.open(ClientDocumentsDialogComponent, {
      width: '1200px',
      maxHeight: '550px',
      disableClose: true,
      autoFocus: false,
      data: this.clientDocumentsData
    });
  }

  refresh(): boolean {
    return false;
  }
}
