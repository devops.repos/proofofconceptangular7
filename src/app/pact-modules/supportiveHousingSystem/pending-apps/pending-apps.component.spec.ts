import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingAppsComponent } from './pending-apps.component';

describe('PendingAppsComponent', () => {
  let component: PendingAppsComponent;
  let fixture: ComponentFixture<PendingAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
