import { Component, OnInit, ViewChild } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';

//Ag-Grid References
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';

//Model References
import { PendingAppsList, AgencyData } from './pending-apps.model';
import { AssessmentApplication } from '../consent-search/consent-search.model';

//Service References
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/helper-services/user.service';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { NavService } from 'src/app/core/sidenav-list/nav.service';
import { AuthData } from 'src/app/models/auth-data.model';
import { PendingAppsService } from './pending-apps.service';
import { ConfirmDialogService } from 'src/app/shared/confirm-dialog/confirm-dialog.service';
import { ConsentService } from '../consent-search/consent-search.service';
import { SiteAdminService } from '../../vacancyControlSystem/agency-site-maintenance/site-admin.service';

//Component References
import { PendingAppsActionComponent } from './pending-apps-action.component';
import { CommonService } from '../../../services/helper-services/common.service';
import { ClientApplicationService } from 'src/app/services/helper-services/client-application.service';


@Component({
  selector: 'app-pending-apps',
  templateUrl: './pending-apps.component.html',
  styleUrls: ['./pending-apps.component.scss']
})
export class PendingAppsComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  userData: AuthData;

  //Model Initialization
  pendingAppsList: PendingAppsList = {
    clientName: null,
    clientNumber: null,
    surveyNumber: null,
    applicationType: null,
    applicationNumber: null,
    dateEntered: null,
    dateExpires: null,
    enteredBy: null,
    agency: null,
    site: null,
    isAppl: null,
    isMhr: null,
    isPsychEval: null,
    isPsychSocial: null,
    isPackageComplete: null,
    firstName: null,
    lastName: null,
    dob: null,
    cin: null,
    ssn: null,
    pactClientId: null,
    optionUserId: null,
    userId: 0,
  }

  //ag-grid variables
  gridApi: any;
  gridColumnApi: any;
  pagination: any;
  rowSelection: any;
  defaultColDef: any;
  autoGroupColumnDef: any;
  isRowSelectable: any;
  frameworkComponents: any;
  public gridOptions: GridOptions;
  context: any;
  widthSideBar: number;
  widthSideBarSub: Subscription;
  rowData: PendingAppsList[];
  pendingAppsCount: number;
  agencyName: string;
  assessmentApplication: AssessmentApplication = { assessmentId: null, systemId: null, systemValue: null, systemFlag: null, userId: null, agcyNo: null, siteNo: null, pendingSRNO: null, savestatus: null };
  agencyList: AgencyData[];
  agencyInfo: AgencyData;
  agencyID;
  isCASAgency;
  searchStringCtrl = new FormControl();
  agencyGroup: FormGroup;
  filteredAgencyData: Observable<any[]>;
  
  
  //ag-grid Columns
  columnDefs = [
    {
      headerName: 'Survey Number',
      field: 'surveyNumber',
      cellRenderer: (params: { value: string; data: { capsReportId: string }; }) => {
        var link = document.createElement('a');
        link.href = '#';
        link.innerText = params.value;
        link.addEventListener('click', (e) => {
          e.preventDefault();
          if (params.data.capsReportId) {
            this.commonService.displaySurveySummaryReport(params.data.capsReportId, this.userData.optionUserId);
          }
        });
        return link;
      },
      filter: 'agTextColumnFilter',
      width: 130
    },
    {
      headerName: 'Application Number',
      field: 'applicationNumber',
      cellRenderer: (params: { value: string; data: { applicationNumber: number}; }) => {
        var link = document.createElement('a');
        link.href = '#';
        link.innerText = params.value;
        link.addEventListener('click', (e) => {
          e.preventDefault();
          this.commonService.setIsOverlay(true);
          this.commonService.displayApplicationSummaryReport(params.data.applicationNumber.toString(), this.userData.optionUserId);
        });
        return link;
      },
      width: 155,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Client Name (L,N)',
      field: 'clientName',
      width: 160,
      filter: 'agTextColumnFilter',
    },
    {
      headerName: 'HRA Client ID',
      field: 'clientNumber',
      width: 130,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Date Entered',
      field: 'dateEntered',
      width: 130,
      filter: 'agTextColumnFilter',
      cellRenderer: (data) => {
        return data.value ? (new Date(data.value)).toLocaleDateString() : '';
      }
    },
    {
      headerName: 'Date Expires',
      field: 'dateExpires',
      width: 130,
      filter: 'agTextColumnFilter',
      cellStyle: function (params) {
        return { color: 'red' };
      },
      cellRenderer: (data) => {
        return data.value ? (new Date(data.value)).toLocaleDateString() : '';
      }
    },
    {
      headerName: 'Entered By (L,N)',
      field: 'enteredBy',
      width: 130,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Agency',
      field: 'agency',
      width: 230,
      filter: 'agTextColumnFilter',
      tooltipField: 'agency'
    },
    {
      headerName: 'Site',
      field: 'site',
      width: 230,
      filter: 'agTextColumnFilter',
      tooltipField: 'site'
    },
    {
      headerName: 'Application Type',
      field: 'applicationType',
      width: 150,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Appl filled',
      field: 'isAppl',
      width: 100,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Psych Eval',
      field: 'isPsychEval',
      width: 100,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Psych Social',
      field: 'isPsychSocial',
      width: 150,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'MHR',
      field: 'isMhr',
      width: 100,
      filter: 'agTextColumnFilter'
    },
    {
      headerName: 'Package Complete',
      field: 'isPackageComplete',
      width: 150,
      filter: 'agTextColumnFilter'
    },

    {
      headerName: 'Actions',
      field: 'action',
      width: 70,
      filter: false,
      sortable: false,
      resizable: false,
      pinned: 'left',
      suppressMenu: true,
      suppressSizeToFit: true,
      cellRenderer: 'actionRenderer',
    }
  ];



  constructor(private pendingAppsService: PendingAppsService
    , private http: HttpClient
    , private userService: UserService
    , private siteAdminservice: SiteAdminService
    , private route: ActivatedRoute
    , private toastr: ToastrService
    , private confirmDialogService: ConfirmDialogService
    , private navService: NavService
    , private sidenavStatusService: SidenavStatusService
    , private commonService: CommonService
    , private consentService: ConsentService
    , private router: Router
    , private clientApplicationService: ClientApplicationService
    , private formBuilder: FormBuilder

  ) {

    this.agencyGroup = this.formBuilder.group({
      searchStringCtrl: ['']
    });

    this.gridOptions = {
      //rowHeight: 35,
      // columnDefs: this.columnDefs,
      sideBar: {
        toolPanels: [
          {
            id: 'columns',
            labelDefault: 'Columns',
            labelKey: 'columns',
            iconKey: 'columns',
            toolPanel: 'agColumnsToolPanel',
            toolPanelParams: {
              suppressValues: true,
              suppressPivots: true,
              suppressPivotMode: true,
              suppressRowGroups: false
            }
          },
          {
            id: 'filters',
            labelDefault: 'Filters',
            labelKey: 'filters',
            iconKey: 'filter',
            toolPanel: 'agFiltersToolPanel',
          }
        ],
        defaultToolPanel: ''
      }
    } as GridOptions;

    this.defaultColDef = {
      sortable: true,
      resizable: true,
      filter: true
    };
    this.rowSelection = 'single';
    this.pagination = true;
    this.context = { componentParent: this };
    this.frameworkComponents = {
      actionRenderer: PendingAppsActionComponent
    };

  }



  ngOnInit() {
    /** Setting the sidenav Status for the application with the given ID from URL router parameter
     * This is the case for page refresh or reload
     */

    //this.sidenavStatusService._sidenavStatusReload(this.route);


    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    this.agencyName = this.userData.agencyName + "/" + this.userData.agencyNo;

    /** Getting the width of the sidenav to reflect the changes to nav toggle */
    this.widthSideBarSub = this.navService.getWidthSideBar().subscribe(res => {
      this.widthSideBar = res;
    });
  }

  onGridReady(params) {
    params.api.setDomLayout('autoHeight');
    this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;

    // var allColumnIds = [];
    // this.gridColumnApi.getAllColumns().forEach(function (column) {

    //   if (column.colId !== 'action') {
    //     if (column.colId !== 'eligibility') {
    //       allColumnIds.push(column.colId);
    //     }
    //   }
    // });
    // this.gridColumnApi.autoSizeColumns(allColumnIds);
    //this.sidenavStatusService._sidenavStatusReload(this.route);
    this.isCASAgency = false;
    this.agencyInfo = {} as AgencyData;


    this.userService.getUserData().subscribe(res => {
      this.userData = res;
      if (this.userData) {
        if (this.userData.siteCategoryType.length > 0) {
          this.isCASAgency = this.commonService._doesValueExistInJson(this.userData.siteCategoryType, 8);
        }
        if (this.userData.agencyId > 0) {
          this.agencyID = this.userData.agencyId;
        }

        // If the user is from CAS Agency, Load the ag grid based on the drop down selected value
        // If the user is not from CAS Agency, we will load the Grid with all the applications the User has access i.e applications created by him & his subordinates
        if (this.isCASAgency == true) {
          this.agencyID = 0
        }
        this.siteAdminservice.getAgencyList(null,null)
          .subscribe(
            res => {
              this.agencyList = res.body as AgencyData[];
              if (this.agencyList.length > 0) {
                this.filteredAgencyData = this.searchStringCtrl.valueChanges.pipe(
                  startWith(''),
                  map(value => this._filter(value))
                );
              }
            },
            error => console.error('Error!', error)
          );
      }





      this.pendingAppsService.getPendingApplicationList(this.userData.optionUserId, this.agencyID)
        .subscribe(
          res => {
            this.rowData = res as PendingAppsList[];
            this.pendingAppsCount = this.rowData.length;
          },
          error => console.error('Error!', error)
        )

    });
  }

  private _filter(value: string): AgencyData[] {
    const filterValue = value.toLowerCase();
    return this.agencyList.filter(agency => agency.name.toLowerCase().includes(filterValue) || agency.agencyNo.includes(filterValue));
  }

  alertDialogBox(pMessage: string) {
    const title = 'Alert';
    const primaryMessage = pMessage;
    const secondaryMessage = '';
    const confirmButtonName = 'OK';
    const dismissButtonName = '';

    this.confirmDialogService.confirmDialog(title, primaryMessage, secondaryMessage, confirmButtonName, dismissButtonName);
  }

  deleteSelectedApplication(cell: PendingAppsList) {
    cell.userId = this.userData.optionUserId;
    const title = 'Confirm Delete';
    const primaryMessage = 'The Pending Application will be deleted permanently.';
    const secondaryMessage =
      'Are you sure you want to delete the selected application?';
    const confirmButtonName = 'Yes';
    const dismissButtonName = 'No';

    this.confirmDialogService
      .confirmDialog(
        title,
        primaryMessage,
        secondaryMessage,
        confirmButtonName,
        dismissButtonName
      )
      .then(
        positiveResponse => {
          this.pendingAppsService
            .deletePendingApplicationListItem(cell)
            .subscribe(
              res => {
                this.getPendinApplicationList();
                if (res) {
                  if (cell.surveyNumber && cell.applicationNumber) {
                    this.createAssessmentApplication(cell.surveyNumber, cell.applicationNumber, cell.site);
                  }
                  this.toastr.success('Pending Application data has been deleted.');
                } else {
                  this.toastr.error(
                    'There was an error in deleting the pending application data.'
                  );
                }
              },
              error => {
                this.toastr.error(
                  'There was an error in deleting the document.',
                  'Document delete Failed!'
                );
              }
            );
        },
        negativeResponse => console.log()
      );

  }

  getPendinApplicationList = () => {
    this.pendingAppsService.getPendingApplicationList(this.userData.optionUserId, 0)
      .subscribe(
        res => {
          this.rowData = res as PendingAppsList[];
          this.pendingAppsCount = this.rowData.length;
          this.agencyName = this.userData.agencyName;

        },
        error => console.error('Error!', error)
      );
  }

  onPageSizeChanged(selectedPageSize) {
    this.gridOptions.api.paginationSetPageSize(Number(selectedPageSize));
  }

  refreshAgGrid() {
    this.gridOptions.api.setFilterModel(null);
  }

  updateAgGrid(value: AgencyData) {

    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    this.pendingAppsService.getPendingApplicationList(this.userData.optionUserId, value.agencyID)
      .subscribe(
        res => {
          this.rowData = res as PendingAppsList[];
          this.pendingAppsCount = this.rowData.length;
        },
        error => console.error('Error!', error)
      );
  }

  onBtExport() {
    /** appending the current datetime with the filename */
    const date = new Date().toLocaleDateString().replace(/\//g, '-');
    const times = new Date().toLocaleTimeString().replace(/\s/g, '-');
    const time = times.replace(/:/g, '-');

    const params = {
      fileName: 'pendingApplicationList-' + date + '-' + time,
      // sheetName: document.querySelector("#sheetName").value,
    };
    this.gridApi.exportDataAsExcel(params);
  }

  showYoungAdultTurningAdultMessage = () => {
    console.log('in the warning pop-up');
      const title = 'Important Notice !';
      const primaryMessage = 
      'Applicant turned 26 after the application was created. The application type population options available/chosen on ‘Application Type’ tab at the time of creation may not be applicable right now.';
      const secondaryMessage =
      'Application with an invalid population option chosen on ‘Application Type’ tab will not be allowed to Transmit during final submission. Please create a new application if an invalid population option was chosen on  ‘Application Type’ tab.';
      const confirmButtonName = 'OK';
      const dismissButtonName = '';

      this.confirmDialogService.confirmDialog(
        title,
        primaryMessage,
        secondaryMessage,
        confirmButtonName,
        dismissButtonName
      );
  }


  //Create Assessment Application
  createAssessmentApplication(assessmentID: number, pactApplicationId: number, siteNo: string) {
    this.assessmentApplication.assessmentId = assessmentID.toString();
    this.assessmentApplication.systemId = 131;
    this.assessmentApplication.systemValue = pactApplicationId.toString();
    this.assessmentApplication.systemFlag = "D";
    this.assessmentApplication.userId = this.userData.lanId;
    this.assessmentApplication.agcyNo = this.userData.agencyNo;
    this.assessmentApplication.siteNo = siteNo;
    this.assessmentApplication.pendingSRNO = pactApplicationId.toString();
    this.consentService.createAssessmentApplication(this.assessmentApplication).subscribe(res => {
      if (!res.isUpdated) {
        throw new Error("Unable to create assessment application.");
      }
    });
  }

  //Navigate to demographics page
  navigateToApplication(selectedAppId: number) {
    this.clientApplicationService.setClientApplicationDataWithApplicationId(selectedAppId);
    this.sidenavStatusService.setApplicationIDForSidenavStatus(selectedAppId);
    this.consentService.setIsTransmittedApplication(false);
    this.router.navigate(['/shs/pendingApp/demographics', selectedAppId]);
  }
}
