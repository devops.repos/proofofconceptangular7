export class PACTPsychiatricPsychoMHR {
    pactPsychiatricPsychoMHRID? : number;
    pactApplicationID? : number;
    hasPsychiatricReport? : boolean;
    hasPsychiatricAttachmentORDataEnter? : number;
    hasPsychosocialReport? : boolean;
    hasPsychosocialAttachmentORDataEnter? : number;
    isMHRYes? : boolean;
    isPsychiatricEvaluationTabComplete? : boolean;
    isPsychoSocialEvaluationTabComplete? : boolean;
    isMHRTabComplete? : boolean;
    userID? : number;
  }

  export class TabStatus {
    parentTabIndex : number;
    childTabIndex : number;
  }

  export enum QuestionColor {
    redColor = '#FF0000',
    blackColor = '#000000',
  }
