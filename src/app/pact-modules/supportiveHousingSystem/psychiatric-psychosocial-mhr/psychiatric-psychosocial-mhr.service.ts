import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

//Model References
//import { PACTMentalHealthReport, PACTMHRDiagnosis } from './mhr/mhr.model';
import { PACTMentalHealthReport } from './mhr/mhr.model';
import { PACTPsychiatricEvaluation } from './psychiatric/psychiatric.model';
import { PACTPsychiatricPsychoMHR } from './psychiatric-psychosocial-mhr.model';
import { PACTPsychoSocialEvaluation } from './psychosocial/psychosocial.model';
//import { PACTClinicalMHRDiagnosisDetailsInput } from 'src/app/shared/diagnosis/diagnosis.model';


@Injectable({
  providedIn: 'root'
})
export class PsychiatricPsychosocialMHRService {
  //Save Urls
  savePACTMentalHealthReportUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/SavePACTMentalHealthReport';
  savePACTPsychiatricPsychoMHRUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/SavePACTPsychiatricPsychoMHR';
  //savePACTMHRDiagnosisUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/SavePACTMHRDiagnosis';
  //savePACTClinicalMHRDiagnosisDetailsUrl = environment.pactApiUrl + 'Diagnosis/SavePACTClinicalMHRDiagnosisDetails';
  savePACTPsychiatricEvaluationUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/SavePACTPsychiatricEvaluation';
  savePACTPsychoSocialEvaluationUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/SavePACTPsychoSocialEvaluation';
  
  //Get Urls
  getPACTMentalHealthReportUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/GetPACTMentalHealthReport';
  getPACTPsychiatricPsychoMHRUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/GetPACTPsychiatricPsychoMHR';
  //getPACTMHRDiagnosisUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/GetPACTMHRDiagnosis';
  //getGetPACTClinicalMHRDiagnosisDetailsUrl = environment.pactApiUrl + 'Diagnosis/GetPACTClinicalMHRDiagnosisDetails';
  getPACTPsychiatricEvaluationUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/GetPACTPsychiatricEvaluation';
  getPACTPsychoSocialEvaluationUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/GetPACTPsychoSocialEvaluation';
  
  //Delete Urls
  //deletePACTMHRDiagnosisUrl = environment.pactApiUrl + 'PsychiatricPsychosocialMHR/DeletePACTMHRDiagnosis';
  deletePACTClinicalMHRDiagnosisDetailsUrl = environment.pactApiUrl + 'Diagnosis/DeletePACTClinicalMHRDiagnosisDetails';
    
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),
    response : "json",
  };
  
  constructor(private httpClient: HttpClient) { }

  //Save Methods
  savePACTMentalHealthReport(pactMentalHealthReport : PACTMentalHealthReport) : Observable<any>
  {
    return this.httpClient.post(this.savePACTMentalHealthReportUrl, JSON.stringify(pactMentalHealthReport), this.httpOptions);
  }

  savePACTPsychiatricPsychoMHR(pactPsychiatricPsychoMHR : PACTPsychiatricPsychoMHR) : Observable<any>
  {
    return this.httpClient.post(this.savePACTPsychiatricPsychoMHRUrl, JSON.stringify(pactPsychiatricPsychoMHR), this.httpOptions);
  }

  // savePACTClinicalMHRDiagnosisDetails(pactClinicalMHRDiagnosisDetailsInput : PACTClinicalMHRDiagnosisDetailsInput) : Observable<any>
  // {
  //   return this.httpClient.post(this.savePACTClinicalMHRDiagnosisDetailsUrl, JSON.stringify(pactClinicalMHRDiagnosisDetailsInput), this.httpOptions);
  // }

  savePACTPsychiatricEvaluation(pactPsychiatricEvaluation : PACTPsychiatricEvaluation) : Observable<any>
  {
    return this.httpClient.post(this.savePACTPsychiatricEvaluationUrl, JSON.stringify(pactPsychiatricEvaluation), this.httpOptions);
  }

  savePACTPsychoSocialEvaluation(pactPsychoSocialEvaluation : PACTPsychoSocialEvaluation) : Observable<any>
  {
    return this.httpClient.post(this.savePACTPsychoSocialEvaluationUrl, JSON.stringify(pactPsychoSocialEvaluation), this.httpOptions);
  }

  //Get Methods
  getPACTMentalHealthReport(applicationID : number) : Observable<any>
  {
     return this.httpClient.post(this.getPACTMentalHealthReportUrl, JSON.stringify(applicationID), this.httpOptions);
  }

  getPACTPsychiatricPsychoMHR(applicationID : number) : Observable<any>
  {
     return this.httpClient.post(this.getPACTPsychiatricPsychoMHRUrl, JSON.stringify(applicationID), this.httpOptions);
  }

  // getPACTMHRDiagnosis<T>(pactMentalHealthReportID : number) : Observable<T>
  // {
  //    return this.httpClient.post<T>(this.getPACTMHRDiagnosisUrl, JSON.stringify(pactMentalHealthReportID), this.httpOptions);
  // }
  // getGetPACTClinicalMHRDiagnosisDetails<T>(pactApplicationID : number) : Observable<T>
  // {
  //    return this.httpClient.post<T>(this.getGetPACTClinicalMHRDiagnosisDetailsUrl, JSON.stringify(pactApplicationID), this.httpOptions);
  // }

  getPACTPsychiatricEvaluation(pactPsychiatricPsychoMHRID : number) : Observable<any>
  {
     return this.httpClient.post(this.getPACTPsychiatricEvaluationUrl, JSON.stringify(pactPsychiatricPsychoMHRID), this.httpOptions);
  }

  getPACTPsychoSocialEvaluation(pactPsychiatricPsychoMHRID : number) : Observable<any>
  {
     return this.httpClient.post(this.getPACTPsychoSocialEvaluationUrl, JSON.stringify(pactPsychiatricPsychoMHRID), this.httpOptions);
  }

  //Delete Methods
  // deletePACTClinicalMHRDiagnosisDetails(pactClinicalMHRDiagnosisDetailsInput : PACTClinicalMHRDiagnosisDetailsInput) : Observable<any>
  // {
  //   return this.httpClient.post(this.deletePACTClinicalMHRDiagnosisDetailsUrl, JSON.stringify(pactClinicalMHRDiagnosisDetailsInput), this.httpOptions);
  // }

}

