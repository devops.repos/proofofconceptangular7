import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PsychiatricPsychosocialMhrComponent } from './psychiatric-psychosocial-mhr.component';

describe('PsychiatricPsychosocialMhrComponent', () => {
  let component: PsychiatricPsychosocialMhrComponent;
  let fixture: ComponentFixture<PsychiatricPsychosocialMhrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PsychiatricPsychosocialMhrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PsychiatricPsychosocialMhrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
