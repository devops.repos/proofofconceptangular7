export class PACTPsychoSocialEvaluation {
    pactPsychoSocialEvaluationID? : number;
    pactPsychiatricPsychoMHRID : number;
    verificationConsent : boolean;
    verifiedBy : number;
    verifiedByName : string;
    verifiedDate : string;
    qualifiedProfessionalName : string;
    summaryDate : string;
    comprehensiveEval : string;
    isPsychoSocialEvaluationTabComplete? : boolean;
    userID?: number;
  }
