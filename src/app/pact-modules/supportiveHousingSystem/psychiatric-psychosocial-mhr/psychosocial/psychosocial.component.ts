import { Component, OnInit, Input, EventEmitter, Output, HostListener, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
//import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
//import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

//Model References
import { PACTPsychoSocialEvaluation } from './psychosocial.model';
import { PACTPsychiatricPsychoMHR, TabStatus } from '../psychiatric-psychosocial-mhr.model';
import { AuthData } from 'src/app/models/auth-data.model';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';

//Service References
import { PsychiatricPsychosocialMHRService } from '../psychiatric-psychosocial-mhr.service';
import { ToastrService } from 'ngx-toastr';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { CommonService } from 'src/app/services/helper-services/common.service';

import { environment } from 'src/environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
//import { ReportViewerComponent } from '../reportViewer/reportViewer.component';

// export const MY_FORMATS = {
//   parse: {
//     dateInput: 'MM/DD/YYYY',
//   },
//   display: {
//     dateInput: 'MM/DD/YYYY',
//     monthYearLabel: 'MMM YYYY',
//     dateA11yLabel: 'LL',
//     monthYearA11yLabel: 'MMMM YYYY',
//   },
// };

@Component({
  selector: 'app-psychosocial',
  templateUrl: './psychosocial.component.html',
  styleUrls: ['./psychosocial.component.scss'],
  providers: [DatePipe,
    // { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    // { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class PsychosocialComponent implements OnInit, OnDestroy {

  @Input() userData;
  uData: AuthData;

  @Input() pactPsychiatricPsychoMHR;
  psychiatricPsychoMHR = new PACTPsychiatricPsychoMHR();

  @Input() tabStatus;
  tStatus = new TabStatus();
  @Output() tabNavigation = new EventEmitter<TabStatus>();
  @Output() psychosocialTabStatus = new EventEmitter<any>();
  isPsychoSocialEvaluationTabComplete?: boolean;
  isValid: boolean = false;
  isControlTouched: boolean = false;
  routeSub: any;
  isVerificationTouched: boolean = false;

  isPsychosocialAttachment: boolean;
  verifiedDate: string;
  verifiedBy: number;
  verifiedByName: string;

  minDate = new Date(new Date().getTime() - 180 * 24 * 60 * 60 * 1000);
  maxDate = new Date(Date.now());

  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  psychosocialGroup: FormGroup;
  pactPsychoSocialEvaluation = new PACTPsychoSocialEvaluation();

  reportUrl: SafeResourceUrl;
  reportServerUrl: string = environment.pactReportServerUrl;
  reportName: string = 'PsychosocialEvalReport';
  reportParameters: string = '&rs:Command=Render&rc:Toolbar=false&ReportParameterID=';
  pactReportParameters: PACTReportParameters[] = [];
  guid: string;
  reportParams: PACTReportUrlParams;

  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  //Validation
  validationControls : [string, string][] = [];

  constructor(private formBuilder: FormBuilder
    , private datePipe: DatePipe
    , private psychiatricPsychosocialMHRService: PsychiatricPsychosocialMHRService
    , private router: Router
    , private toastrService: ToastrService
    , private sidenavStatusService: SidenavStatusService
    , private activatedRoute: ActivatedRoute
    , private dialog: MatDialog
    , private sanitizer: DomSanitizer
    , private commonService: CommonService
  ) { }

  //Save Data on page refresh
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (this.psychosocialGroup.dirty) {
      this.savePsychoSocialEvaluation();

      if (this.psychiatricPsychoMHR.pactApplicationID) {
        this.sidenavStatusService.setApplicationIDForSidenavStatus(this.psychiatricPsychoMHR.pactApplicationID);
      }

    }
  };

  ngOnInit() {

    this.psychosocialGroup = this.formBuilder.group({
      verificationConsent: [''],
      qualifiedProfessionalName: [''],
      //summaryDate: ['', Validators.compose([Validators.required, this.dateValidator, Validators.pattern(/^[0-9\/]+$/)])],
      summaryDate: [''],
      comprehensiveEval: [''],
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.psychosocialGroup.touched && event.navigationTrigger != "popstate") {
          this.savePsychoSocialEvaluation();
        }
        if (event.navigationTrigger == "popstate" && this.psychosocialGroup.dirty) // Handles forward and backward browser clicks
        {
          this.savePsychoSocialEvaluation();
        }

      };
    });


    this.tStatus = this.tabStatus;
    //console.log(this.tStatus);
    this.uData = this.userData;
    this.psychiatricPsychoMHR = this.pactPsychiatricPsychoMHR;
    if (this.psychiatricPsychoMHR != null) {
      this.isPsychosocialAttachment = this.psychiatricPsychoMHR.hasPsychosocialAttachmentORDataEnter == 356 ? true : false;

      if (this.psychiatricPsychoMHR.pactPsychiatricPsychoMHRID > 0)
        this.loadData(this.psychiatricPsychoMHR.pactPsychiatricPsychoMHRID);

    }

  }

  loadData(pactPsychiatricPsychoMHRID: number) {

    this.psychiatricPsychosocialMHRService.getPACTPsychoSocialEvaluation(pactPsychiatricPsychoMHRID)
      .subscribe(
        res => {
          //console.log(res);
          this.pactPsychoSocialEvaluation = res;
          this.populateForm(this.pactPsychoSocialEvaluation);
        });

  }

  populateForm(formData: PACTPsychoSocialEvaluation) {
    //console.log("Populate ", formData);
    if (formData != null) {
      this.psychosocialGroup.controls['verificationConsent'].setValue(formData.verificationConsent);
      this.psychosocialGroup.controls['qualifiedProfessionalName'].setValue(formData.qualifiedProfessionalName);
      this.psychosocialGroup.controls['summaryDate'].setValue(formData.summaryDate);
      this.psychosocialGroup.controls['comprehensiveEval'].setValue(formData.comprehensiveEval);

      this.verifiedDate = formData.verifiedDate != null ? this.datePipe.transform(formData.verifiedDate, "MM/dd/yyyy") : null;
      this.verifiedByName = formData.verifiedByName != null ? formData.verifiedByName : null;
      this.verifiedBy = formData.verifiedBy != null ? formData.verifiedBy : null;

      if (formData.isPsychoSocialEvaluationTabComplete != null)
        this.isPsychoSocialEvaluationTabComplete = formData.isPsychoSocialEvaluationTabComplete;
      else
        this.isPsychoSocialEvaluationTabComplete = null;
    }
    else {
      this.pactPsychoSocialEvaluation = new PACTPsychoSocialEvaluation();
      this.pactPsychoSocialEvaluation.pactPsychoSocialEvaluationID = null;

      this.psychosocialGroup.controls['verificationConsent'].setValue(false);
      this.psychosocialGroup.controls['qualifiedProfessionalName'].setValue(null);
      this.psychosocialGroup.controls['summaryDate'].setValue(null);
      this.psychosocialGroup.controls['comprehensiveEval'].setValue(null);

      this.verifiedDate = null;
      this.verifiedByName = null;
      this.verifiedBy = null;
      this.isPsychoSocialEvaluationTabComplete = null;
    }

    this.psychosocialTabStatus.emit(this.isPsychoSocialEvaluationTabComplete);
  }

  //Next button click
  nextTab() {
    //console.log(this.tStatus);
    //this.router.navigate(['/shs/newApp/documents', this.psychiatricPsychoMHR.pactApplicationID]);
    //Call Save
    this.savePsychoSocialEvaluation();

    if (this.psychiatricPsychoMHR.isMHRYes === true) {
      this.tStatus.parentTabIndex = 2;
      this.tabNavigation.emit(this.tStatus);
    }
    else
      this.sidenavStatusService.routeToNextPage(this.router, this.activatedRoute);
  }

  //Previous button click
  previousTab() {
    //Call Save
    this.savePsychoSocialEvaluation();

    if (this.psychiatricPsychoMHR.hasPsychiatricReport === true)
      this.tStatus.parentTabIndex = 1;
    else
      this.tStatus.parentTabIndex = 0;

    this.tabNavigation.emit(this.tStatus);
  }

  //On Consent check change
  onIsConsentCheckChange(event: { checked: Boolean }) {
    this.isVerificationTouched = true;
    if (event.checked) {
      this.psychosocialGroup.controls['verificationConsent'].setValue(true);
      this.verifiedBy = this.uData.optionUserId;
      this.verifiedByName = this.uData.firstName + " " + this.uData.lastName;
      this.verifiedDate = this.datePipe.transform(new Date(), "MM/dd/yyyy");

    }
    else {
      this.psychosocialGroup.controls['verificationConsent'].setValue(false);
      this.verifiedBy = null;
      this.verifiedByName = null;
      this.verifiedDate = null;
    }
  }

  autoSaveComprehensiveEvaluation(value: string, mode: string): void {
    if (mode == "Type") {
      switch (value.length) {
        case 10000:
          this.savePsychoSocialEvaluation();
          break;
        case 20000:
          this.savePsychoSocialEvaluation();
          break;
      }
    }
    else if (mode == "Paste")
      this.savePsychoSocialEvaluation();
  }

  openReport() {
    //this.reportUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.reportServerUrl + this.reportName + this.reportParameters + this.pactPsychiatricPsychoMHR.pactApplicationID);
    //this.reportUrl = this.reportServerUrl + this.reportName + this.reportParameters + this.pactPsychiatricPsychoMHR.pactApplicationID;
    this.pactReportParameters = [
      { parameterName: 'applicationID', parameterValue: this.pactPsychiatricPsychoMHR.pactApplicationID, CreatedBy: this.userData.optionUserId },
      { parameterName: 'reportName', parameterValue: this.reportName, CreatedBy: this.userData.optionUserId },
      { parameterName: 'source', parameterValue: this.router.url, CreatedBy: this.userData.optionUserId },
    ];

    this.commonService.generateGUID(this.pactReportParameters)
      .subscribe(
        res => {
          if (res !== null) {
            this.guid = res;
            //console.log('GUID ', this.guid);

            this.reportParams = { reportParameterID: this.guid, reportName: this.reportName, "reportFormat": "PDF" };

            this.commonService.generateReport(this.reportParams)
              .subscribe(
                res => {
                  var data = new Blob([res.body], { type: 'application/pdf' });
                  //console.log(data);
                  if (data.size > 512) {
                    var fileURL = URL.createObjectURL(data);
                    const printContent = window.open(fileURL, "_blank");
                    if (printContent != null) {
                      printContent.focus();
                      printContent.print();
                    }
                  }


                },
                error => {
                  // if (!this.toastrService.currentlyActive)
                  //   this.toastrService.error("There is an error while generating report.");

                  throw new Error('There is an error while generating Psychosocial Report.');
                }
              );
          }

        },
        error => {
          // if (!this.toastrService.currentlyActive)
          //   this.toastrService.error("There is an error while generating report.");

          throw new Error('There is an error while generating Psychosocial Report.');
        }
      );

  }


  //Validate Date in last 180 days
  validateDate() {

    var summaryDate = this.datePipe.transform(this.psychosocialGroup.get('summaryDate').value, 'MM/dd/yyyy');

    if (!moment(summaryDate, 'MM/DD/YYYY', true).isValid()) {
      if (!this.toastrService.currentlyActive)
        this.toastrService.error("Invalid Date.");

      this.isValid = false;
    }
    else{

    //if (summaryDate) {
      let days = moment(Date.now()).diff(moment(summaryDate), 'day', true);
      //console.log(days);
      if (days > 181 || days < -0) {
        if (!this.toastrService.currentlyActive)
          this.toastrService.error("Date of Psychosocial Summary must be within 180 days of today's date.");
        
        this.psychosocialGroup.controls['summaryDate'].setValue(null);
        this.isValid = false;
      }
      else {
        this.psychosocialGroup.controls.summaryDate.clearValidators();
        this.psychosocialGroup.controls.summaryDate.updateValueAndValidity();
        this.isValid = true;
      }
    }
  }

  //2 Character Length Validation
  validateControlsLength(formGroup: FormGroup)
  {
    this.validationControls = [];
    var characterValidation : boolean = true;

    // var controlValue = this.psychosocialGroup.get('comprehensiveEval').value != null ? this.psychosocialGroup.get('comprehensiveEval').value.trim() : '';
    // if (controlValue.length < 2 && controlValue != '')
    // {
    //   this.toastrService.error("Comprehensive Psychosocial Evaluation should be at least 2 character long");
    //   this.psychosocialGroup.controls['comprehensiveEval'].setValue(null);
    //   //this.isValid = false;
    // }

    var controlValue = formGroup.controls.qualifiedProfessionalName.value != null ? formGroup.controls.qualifiedProfessionalName.value.trim() : '';
    if (controlValue.length < 2 && controlValue != '')
    {
      this.fillValidationControls('qualifiedProfessionalName', 'Name of Qualified Professional');
      characterValidation = false;
    }
    
    if(!this.isPsychosocialAttachment)
    {
      controlValue = formGroup.controls.comprehensiveEval.value != null ? formGroup.controls.comprehensiveEval.value.trim() : '';
      if (controlValue.length < 2 && controlValue != '')
      {
        this.fillValidationControls('comprehensiveEval', 'Comprehensive Psychiatric Evaluation');
        characterValidation = false;
      }
    }

    if(characterValidation === false)
      this.clearControls(this.validationControls);

    return characterValidation;
  }

  fillValidationControls(controlName : string, controlLable : string)
  {
    var validationControl : [string, string] = ['',''];
    validationControl[0] = controlName;
    validationControl[1] = controlLable;
    this.validationControls.push(validationControl);
  }

  clearControls(controls : [string, string][])
  {
    var validationMessage : string = '';

    controls.forEach(control => {
      this.psychosocialGroup.controls[control[0]].setValue(null);
      validationMessage = validationMessage + control[1] + ", ";
      });

      if(validationMessage.length > 0)
        validationMessage = validationMessage.slice(0, validationMessage.length - 2);

     this.callToastrService(validationMessage);
  }

  callToastrService(controlName : string)
  {
    var frontmsg : string = '';
    var backmsg : string = ' should be at least 2 character long';
    var message = frontmsg + controlName + backmsg;
    this.isValid = false;
    if (!this.toastrService.currentlyActive)
      this.toastrService.error(message);
  }

  savePsychoSocialEvaluation() {
    //Verify if controls is touched
    //this.isControlTouched = this.markFormGroupTouched(this.psychosocialGroup);
    // console.log('Psychosocial Control Touched ', this.isControlTouched);

    //if (this.isControlTouched){
      if (this.psychosocialGroup.get('summaryDate').value != null)
        this.validateDate();
      else
        this.isValid = true;

      //Validate ComprehensiveEval Lenght
      //if(!this.isPsychosocialAttachment)
      this.validateControlsLength(this.psychosocialGroup);

      // console.log('Psychosocial Valid ', this.isValid);
      if (this.isValid) {

        this.pactPsychoSocialEvaluation.pactPsychiatricPsychoMHRID = this.psychiatricPsychoMHR.pactPsychiatricPsychoMHRID;
        this.pactPsychoSocialEvaluation.verificationConsent = this.psychosocialGroup.get('verificationConsent').value != null ? this.psychosocialGroup.get('verificationConsent').value : null;
        this.pactPsychoSocialEvaluation.qualifiedProfessionalName = this.psychosocialGroup.get('qualifiedProfessionalName').value != null ? this.psychosocialGroup.get('qualifiedProfessionalName').value : null;
        this.pactPsychoSocialEvaluation.summaryDate = this.psychosocialGroup.get('summaryDate').value != null ? this.psychosocialGroup.get('summaryDate').value : null;
        this.pactPsychoSocialEvaluation.comprehensiveEval = this.psychosocialGroup.get('comprehensiveEval').value != null ? this.psychosocialGroup.get('comprehensiveEval').value : null;
        //this.pactPsychoSocialEvaluation.verifiedBy = this.uData.optionUserId;
        this.pactPsychoSocialEvaluation.verifiedBy = this.verifiedBy;
        this.pactPsychoSocialEvaluation.verifiedDate = this.verifiedDate;
        this.pactPsychoSocialEvaluation.userID = this.uData.optionUserId;

        //console.log("Save ", this.pactPsychoSocialEvaluation);
        this.psychiatricPsychosocialMHRService.savePACTPsychoSocialEvaluation(this.pactPsychoSocialEvaluation)
          .subscribe(data => {
            if (!this.toastrService.currentlyActive)
              this.toastrService.success("Data has been saved.");

            this.isVerificationTouched = false;
            //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);
            if (this.pactPsychiatricPsychoMHR.pactApplicationID) {
              this.sidenavStatusService.setApplicationIDForSidenavStatus(this.psychiatricPsychoMHR.pactApplicationID);
            }

            this.populateForm(data);
          },
            error => {
              // if (!this.toastrService.currentlyActive)
              //   this.toastrService.error("There is an error while saving data.");

              throw new Error('There is an error while saving Psychosocial Evaluation Data.');
            });
      }

    //}

  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    // const controlTouched = !!(formGroup && (formGroup.dirty || formGroup.touched));
    // return controlTouched;

    var controlTouched : any;
    //console.log(formGroup);

    if ((this.isVerificationTouched === true)
      || (formGroup.controls.qualifiedProfessionalName.touched)
      || (formGroup.controls.summaryDate.touched)
      || (formGroup.controls.comprehensiveEval.touched)
    )
      controlTouched = true;
    else
      controlTouched = false;

    //console.log('controlTouched ', controlTouched);
    return controlTouched;
  }

  onUploadFinish = (event: string) => {
    //console.log(event);
    if (event != null) {
      this.isPsychoSocialEvaluationTabComplete = true;
      this.psychosocialTabStatus.emit(this.isPsychoSocialEvaluationTabComplete);
    }
  }

  async ngOnDestroy() {
    this.savePsychoSocialEvaluation();

    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }

    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }

    if (this.routeSub)
    {
      await this.routeSub.unsubscribe();
    }

  }

}
