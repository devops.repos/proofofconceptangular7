import { Component, OnInit, Input, HostListener, OnDestroy } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';

//Model References
import { PACTPsychiatricPsychoMHR, TabStatus, QuestionColor } from './psychiatric-psychosocial-mhr.model';
import { AuthData } from 'src/app/models/auth-data.model';

//Enums
import { appStatusColor } from 'src/app/models/pact-enums.enum';

//Service References
import { PsychiatricPsychosocialMHRService } from './psychiatric-psychosocial-mhr.service';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { UserService } from '../../../services/helper-services/user.service';
import { ClientApplicationService } from './../../../services/helper-services/client-application.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-psychiatric-psychosocial-mhr',
  templateUrl: './psychiatric-psychosocial-mhr.component.html',
  styleUrls: ['./psychiatric-psychosocial-mhr.component.scss']
})
export class PsychiatricPsychosocialMhrComponent implements OnInit, OnDestroy {
  userData: AuthData;
  pactPsychiatricPsychoMHRGroup: FormGroup;
  selectedTab = 0;
  isParent = true;
  pactApplicationID: number;
  isValid: boolean;
  isControlTouched: boolean = false;
  routeSub: any;
  supportiveHousingTypes: string;

  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  pactPsychiatricPsychoMHR = new PACTPsychiatricPsychoMHR();
  tabStatus = new TabStatus();

  // Tab Status : Pending = 0 ; Complete = 1; Blank = 2;
  blankTabStatus = 2; pendingTabStatus = 0; completedTabStatus = 1;
  supportingDocumentsDataTabStatus: number = this.blankTabStatus;
  psychiatricEvaluationTabStatus: number = this.blankTabStatus;
  psychosocialEvaluationTabStatus: number = this.blankTabStatus;
  mhrTabStatus: number = this.blankTabStatus;

  currentTabName : string = "SupportingDocuments";
  previousTabName : string;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  redColor = QuestionColor.redColor;
  blackColor = QuestionColor.blackColor;
  isPsychiatricRequired: boolean = false;
  isPsychosocialRequired: boolean = false;
  supportiveHousingTypesArray: number[];

  constructor(private psychiatricPsychosocialMHRService: PsychiatricPsychosocialMHRService
    , private activatedRoute: ActivatedRoute
    , private router: Router
    , private sidenavStatusService: SidenavStatusService
    , private userService: UserService
    , private formBuilder: FormBuilder
    , private clientApplicationService: ClientApplicationService
    , private toastrService: ToastrService
  ) { }

  //Save Data on page refresh
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (this.pactPsychiatricPsychoMHRGroup.dirty) {
      this.savePsychiatricPsychosocialMHR();
    }
  };


  ngOnInit() {
    /** Setting the sidenav Status for the application with the given ID from URL router parameter
     * This is the case for page refresh or reload
     */
    // this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);

    /** To Load the Sidenav Completion Status for all the Application related Pages */
    this.activatedRouteSub = this.activatedRoute.paramMap.subscribe(params => {
      const selectedAppId = params.get('applicationID');
      if (selectedAppId == null) {
        return;
      } else {
        const appid = parseInt(selectedAppId);
        if (isNaN(appid)) {
          throw new Error('Invalid ApplicationID!!');
        } else if (appid > 0 && appid < 2147483647) {
          this.pactApplicationID = appid;
          this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);
        }
      }
    });

    this.clientApplicationService.getClientApplicationData().subscribe(res => {

      if (res && res.pactApplicationId > 0) {
        this.pactApplicationID = res.pactApplicationId;
        this.supportiveHousingTypes = res.supportiveHousingType;
        //console.log('62,65,66,68,69', this.supportiveHousingTypes);
        if (this.supportiveHousingTypes.indexOf('62') >= 0 || this.supportiveHousingTypes.indexOf('65') >= 0 || this.supportiveHousingTypes.indexOf('66') >= 0
          || this.supportiveHousingTypes.indexOf('69') >= 0)
          this.isPsychiatricRequired = true;

        //console.log(this.isPsychiatricRequired);
        this.loadData();
      }
    });

    this.userService.getUserData().subscribe(res => {
      this.userData = res;
    });

    this.pactPsychiatricPsychoMHRGroup = this.formBuilder.group({
      hasPsychiatricReportCtrl: [''],
      hasPsychiatricAttachmentORDataEnterCtrl: [''],
      hasPsychosocialReportCtrl: [''],
      hasPsychosocialAttachmentORDataEnterCtrl: [''],
      isMHRYesReportCtrl: [''],
      isPsychiatricEvaluationTabCompleteCtrl: [''],
      isPsychoSocialEvaluationTabCompleteCtrl: [''],
      isMHRTabCompleteCtrl: [''],
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.pactPsychiatricPsychoMHRGroup.touched && event.navigationTrigger != "popstate") {
          this.savePsychiatricPsychosocialMHR();
        }
        if (event.navigationTrigger == "popstate" && this.pactPsychiatricPsychoMHRGroup.dirty) // Handles forward and backward browser clicks
        {
          this.savePsychiatricPsychosocialMHR();
        }

      };
    });

  }

  loadData() {

    if (this.pactApplicationID > 0) {
      this.psychiatricPsychosocialMHRService.getPACTPsychiatricPsychoMHR(this.pactApplicationID)
        .subscribe(
          res => {
            //console.log(res);
            this.pactPsychiatricPsychoMHR = res;
            this.populateForm(this.pactPsychiatricPsychoMHR);
          });
    }
  }

  populateForm(formData: PACTPsychiatricPsychoMHR) {
    //console.log('Populate Supportive Housing ', formData);
    if (formData != null) {
      this.pactPsychiatricPsychoMHR = formData;

      if (formData.hasPsychiatricReport != null)
        this.pactPsychiatricPsychoMHRGroup.controls['hasPsychiatricReportCtrl'].setValue(formData.hasPsychiatricReport == true ? 'Y' : 'N');
      else
        this.pactPsychiatricPsychoMHRGroup.controls['hasPsychiatricReportCtrl'].setValue(null);

      if (formData.hasPsychiatricAttachmentORDataEnter != null)
        this.pactPsychiatricPsychoMHRGroup.controls['hasPsychiatricAttachmentORDataEnterCtrl'].setValue(formData.hasPsychiatricAttachmentORDataEnter == 356 ? 'A' : 'D');
      else
        this.pactPsychiatricPsychoMHRGroup.controls['hasPsychiatricAttachmentORDataEnterCtrl'].setValue(null);

      if (formData.hasPsychosocialReport != null)
        this.pactPsychiatricPsychoMHRGroup.controls['hasPsychosocialReportCtrl'].setValue(formData.hasPsychosocialReport == true ? 'Y' : 'N');
      else
        this.pactPsychiatricPsychoMHRGroup.controls['hasPsychosocialReportCtrl'].setValue(null);

      if (formData.hasPsychosocialAttachmentORDataEnter != null)
        this.pactPsychiatricPsychoMHRGroup.controls['hasPsychosocialAttachmentORDataEnterCtrl'].setValue(formData.hasPsychosocialAttachmentORDataEnter == 356 ? 'A' : 'D');
      else
        this.pactPsychiatricPsychoMHRGroup.controls['hasPsychosocialAttachmentORDataEnterCtrl'].setValue(null);

      if (formData.isMHRYes)
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(formData.isMHRYes);
      else
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(null);
    }
    else {
      this.pactPsychiatricPsychoMHR = new PACTPsychiatricPsychoMHR();
      this.pactPsychiatricPsychoMHR.pactApplicationID = this.pactApplicationID;
      this.pactPsychiatricPsychoMHR.pactPsychiatricPsychoMHRID = null;

      this.pactPsychiatricPsychoMHRGroup.controls['hasPsychiatricReportCtrl'].setValue(null);
      this.pactPsychiatricPsychoMHRGroup.controls['hasPsychiatricAttachmentORDataEnterCtrl'].setValue(null);
      this.pactPsychiatricPsychoMHRGroup.controls['hasPsychosocialReportCtrl'].setValue(null);
      this.pactPsychiatricPsychoMHRGroup.controls['hasPsychosocialAttachmentORDataEnterCtrl'].setValue(null);
      this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(null);
    }

    this.setSupportingDocumentsDataTabStatus(formData);

  }

  //Set the Tab Status
  setSupportingDocumentsDataTabStatus(formData: PACTPsychiatricPsychoMHR) {
    if (formData != null) {
      //Psychiatric Evaluation Tab
      if (formData.isPsychiatricEvaluationTabComplete != null)
        this.psychiatricEvaluationTabStatus = formData.isPsychiatricEvaluationTabComplete === true ? this.completedTabStatus : this.pendingTabStatus;
      else
        this.psychiatricEvaluationTabStatus = this.blankTabStatus;

      //PsychoSocial Evaluation Tab
      if (formData.isPsychoSocialEvaluationTabComplete != null)
        this.psychosocialEvaluationTabStatus = formData.isPsychoSocialEvaluationTabComplete === true ? this.completedTabStatus : this.pendingTabStatus;
      else
        this.psychosocialEvaluationTabStatus = this.blankTabStatus;

      //MHR Tab
      if (formData.isMHRTabComplete != null)
        this.mhrTabStatus = formData.isMHRTabComplete === true ? this.completedTabStatus : this.pendingTabStatus;
      else
        this.mhrTabStatus = this.blankTabStatus;

      // console.log('mhr tab status', this.mhrTabStatus);

      //Supporting Document Tab
      if (formData.isMHRYes === true) {
        this.supportingDocumentsDataTabStatus = this.completedTabStatus;
      }
      else {
        if (this.isPsychiatricRequired === true) {
          if (
            ((formData.hasPsychiatricReport === true && formData.hasPsychiatricAttachmentORDataEnter !== null)
              && (formData.hasPsychosocialReport === true && formData.hasPsychosocialAttachmentORDataEnter !== null))
            || ((formData.hasPsychiatricReport === false)
              && (formData.hasPsychosocialReport === true && formData.hasPsychosocialAttachmentORDataEnter !== null))
            || ((formData.hasPsychiatricReport === true && formData.hasPsychiatricAttachmentORDataEnter !== null)
              && (formData.hasPsychosocialReport === false))
          )
            this.supportingDocumentsDataTabStatus = this.completedTabStatus;
          else
            this.supportingDocumentsDataTabStatus = this.pendingTabStatus;
        }
        else {
          if (formData.hasPsychosocialReport !== null) {
            if (
              (formData.hasPsychosocialReport === true && formData.hasPsychosocialAttachmentORDataEnter !== null)
              || (formData.hasPsychosocialReport === false)
            )
              this.supportingDocumentsDataTabStatus = this.completedTabStatus;
            else
              this.supportingDocumentsDataTabStatus = this.pendingTabStatus;
          }
          else
            this.supportingDocumentsDataTabStatus = this.pendingTabStatus;
        }
      }
    }
    else {
      this.supportingDocumentsDataTabStatus = this.blankTabStatus;
      this.psychiatricEvaluationTabStatus = this.blankTabStatus;
      this.psychosocialEvaluationTabStatus = this.blankTabStatus;
      this.mhrTabStatus = this.blankTabStatus;
    }
  }

  psychiatricEvaluationSelection(value: string) {
    this.pactPsychiatricPsychoMHRGroup.controls['hasPsychiatricReportCtrl'].setValue(value);
    this.savePsychiatricPsychosocialMHR();

  }

  psychiatricEvaluationAttachOrDataEnterSelection(value: string) {
    this.pactPsychiatricPsychoMHRGroup.controls['hasPsychiatricAttachmentORDataEnterCtrl'].setValue(value);
    this.savePsychiatricPsychosocialMHR();
  }

  psychosocialEvaluationSelection(value: string) {
    this.pactPsychiatricPsychoMHRGroup.controls['hasPsychosocialReportCtrl'].setValue(value);
    this.savePsychiatricPsychosocialMHR();
  }

  psychosocialEvaluationAttachOrDataEnterSelection(value: string) {
    this.pactPsychiatricPsychoMHRGroup.controls['hasPsychosocialAttachmentORDataEnterCtrl'].setValue(value);
    this.savePsychiatricPsychosocialMHR();
  }

  //Tab Changes Event
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    //console.log(tabChangeEvent.tab.textLabel);
    //this.previousTabName = this.currentTabName;
    //console.log('Previous ', this.previousTabName);
    switch (this.previousTabName) {
      case "SupportingDocuments":
        this.savePsychiatricPsychosocialMHR();
        this.currentTabName = tabChangeEvent.tab.textLabel;
      break;
      case "PsychiatricEvaluation":
        this.currentTabName = tabChangeEvent.tab.textLabel;
      break;
      case "PsychosocialEvaluation":
        this.currentTabName = tabChangeEvent.tab.textLabel;
      break;
      case "MentalHealthReport":
        this.currentTabName = tabChangeEvent.tab.textLabel;
      break;
    }

    //console.log('Current ', this.currentTabName);

    //this.savePsychiatricPsychosocialMHR();
    this.tabStatus.parentTabIndex = tabChangeEvent.index;
    this.selectedTab = tabChangeEvent.index;

    if (this.selectedTab == 0)
      this.isParent = true;
    else
      this.isParent = false;
  }

  //Next button click
  nextTab() {
    if (this.selectedTab < 2) {
      this.savePsychiatricPsychosocialMHR();

      //console.log('Next', this.isValid);
      if (this.isValid) {
        this.selectedTab = this.selectedTab + 1;
        this.tabStatus.parentTabIndex = this.selectedTab;

        if (this.selectedTab == 0)
          this.isParent = true;
        else
          this.isParent = false;
      }
    }

  }

  //Previous button click
  previousTab() {
    if (this.selectedTab != 0) {
      this.savePsychiatricPsychosocialMHR();

      //console.log('Prev');
      if (this.isValid) {
        this.selectedTab = this.selectedTab - 1;
        this.tabStatus.parentTabIndex = this.selectedTab;
      }

    }

    if (this.selectedTab == 0) {
      this.isParent = true;
      this.validateForm();
      if (this.isValid) {
        this.sidenavStatusService.routeToPreviousPage(this.router, this.activatedRoute);
      }

    }
    else
      this.isParent = false;
  }

  tabNavigation(data: TabStatus): void {
    //console.log(data);
    //Navigation From MHR Tab
    if (data.childTabIndex == 0 && data.parentTabIndex == 3) {

      if (this.pactPsychiatricPsychoMHR !== null) {
        if (this.pactPsychiatricPsychoMHR.hasPsychiatricReport === true || this.pactPsychiatricPsychoMHR.hasPsychosocialReport === true)
          this.selectedTab = 1;
        else
          this.selectedTab = 0;
      }
      else
        this.selectedTab = 0;
    }

    //Navigation From Psychiatric and Psycosocial Tab
    if (data.parentTabIndex < 3) {
      this.selectedTab = data.parentTabIndex;
    }
  }

  psychiatricTabStatus(data: any): void {
    //console.log(data);
    if (data != null)
      this.psychiatricEvaluationTabStatus = data === true ? this.completedTabStatus : this.pendingTabStatus;
    else
      this.psychiatricEvaluationTabStatus = this.blankTabStatus;
  }

  psychosocialTabStatus(data: any): void {
    //console.log(data);
    if (data != null)
      this.psychosocialEvaluationTabStatus = data === true ? this.completedTabStatus : this.pendingTabStatus;
    else
      this.psychosocialEvaluationTabStatus = this.blankTabStatus;
  }

  mentalHealthReportTabStatus(data: any): void {
    //console.log('MHR Tab Status Change', data);
    if (data != null)
      this.mhrTabStatus = data === true ? this.completedTabStatus : this.pendingTabStatus;
    else
      this.mhrTabStatus = this.blankTabStatus;

  }

  validateForm() {
    this.isValid = true;
    var hasPsychiatricReportCtrl = this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricReportCtrl').value;
    var hasPsychiatricAttachmentORDataEnterCtrl = this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricAttachmentORDataEnterCtrl').value;
    var hasPsychosocialReportCtrl = this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialReportCtrl').value;
    var hasPsychosocialAttachmentORDataEnterCtrl = this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialAttachmentORDataEnterCtrl').value;

    if (this.isPsychiatricRequired === true) {

      if (hasPsychiatricReportCtrl == null && hasPsychosocialReportCtrl == null)
        this.callToastrService("Please select Psychiatric or Psychosocial Evaluation.");

      if (hasPsychiatricReportCtrl == null && hasPsychosocialReportCtrl != null)
      {
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(false);
        if (hasPsychosocialReportCtrl == 'Y')
        {
            if (hasPsychosocialAttachmentORDataEnterCtrl == null)
              this.callToastrService("Please select Attach or Data-enter option for Psychosocial Evaluation.");
        }
        else
          this.callToastrService("Please select Psychiatric Evaluation.");
      }

      if (hasPsychiatricReportCtrl != null && hasPsychosocialReportCtrl == null)
      {
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(false);
        if (hasPsychiatricReportCtrl == 'Y')
        {
            if (hasPsychiatricAttachmentORDataEnterCtrl == null)
              this.callToastrService("Please select Attach or Data-enter option for Psychiatric Evaluation.");
        }
        else
          this.callToastrService("Please select Psychosocial Evaluation.");
      }

      if (hasPsychiatricReportCtrl != null && hasPsychosocialReportCtrl != null)
      {
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(false);
        if (hasPsychiatricReportCtrl == 'Y')
        {
            if (hasPsychiatricAttachmentORDataEnterCtrl == null)
              this.callToastrService("Please select Attach or Data-enter option for Psychiatric Evaluation.");
        }

        if (hasPsychosocialReportCtrl == 'Y')
        {
          if (hasPsychosocialAttachmentORDataEnterCtrl == null)
            this.callToastrService("Please select Attach or Data-enter option for Psychosocial Evaluation.");
        }

        // if (hasPsychiatricReportCtrl == 'Y' && hasPsychosocialReportCtrl == 'Y')
        // {
        //   this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(false);
        // }
      }

    }
    else {

      if (hasPsychosocialReportCtrl == null)
        this.callToastrService("Please select Psychosocial Evaluation.");
      else
      {
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(false);
        if (hasPsychosocialReportCtrl == 'Y')
        {
          if (hasPsychosocialAttachmentORDataEnterCtrl == null)
            this.callToastrService("Please select Attach or Data-enter option for Psychosocial Evaluation.");
        }
        else
        {
          if (hasPsychiatricReportCtrl == 'Y')
          {
              if (hasPsychiatricAttachmentORDataEnterCtrl == null)
              {
                this.supportingDocumentsDataTabStatus = this.pendingTabStatus;
                this.callToastrService("Please select Attach or Data-enter option for Psychiatric Evaluation.");
              }
          }
          // else
          //   this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(true);

        }
      }
    }

    if(hasPsychiatricReportCtrl === 'N' && hasPsychosocialReportCtrl === 'N')
    {
      if(this.isPsychiatricRequired)
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(false);
      else
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(true);
    }
    else
      this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(false);
  }

  callToastrService(message : string)
  {
    this.isValid = false;
    if (!this.toastrService.currentlyActive)
      this.toastrService.error(message);
  }

  savePsychiatricPsychosocialMHR() {

    //this.isControlTouched = this.markFormGroupTouched(this.pactPsychiatricPsychoMHRGroup);
    //console.log('Supporting Doc Control Touched ', this.isControlTouched);

    this.validateForm();
    //console.log('Supporting Doc Valid ', this.isValid);

    if (this.isValid) {

      if (this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricReportCtrl').value != null)
        this.pactPsychiatricPsychoMHR.hasPsychiatricReport = this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricReportCtrl').value == 'Y' ? true : false;
      else
        this.pactPsychiatricPsychoMHR.hasPsychiatricReport = null;

      if (this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricAttachmentORDataEnterCtrl').value != null)
        this.pactPsychiatricPsychoMHR.hasPsychiatricAttachmentORDataEnter = this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricAttachmentORDataEnterCtrl').value == 'A' ? 356 : 357;
      else
        this.pactPsychiatricPsychoMHR.hasPsychiatricAttachmentORDataEnter = null;

      if (this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialReportCtrl').value != null)
        this.pactPsychiatricPsychoMHR.hasPsychosocialReport = this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialReportCtrl').value == 'Y' ? true : false;
      else
        this.pactPsychiatricPsychoMHR.hasPsychosocialReport = null;

      if (this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialAttachmentORDataEnterCtrl').value != null)
        this.pactPsychiatricPsychoMHR.hasPsychosocialAttachmentORDataEnter = this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialAttachmentORDataEnterCtrl').value == 'A' ? 356 : 357;
      else
        this.pactPsychiatricPsychoMHR.hasPsychosocialAttachmentORDataEnter = null;

      if ((this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricReportCtrl').value == "N" && this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialReportCtrl').value == "N")
        // || (this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricReportCtrl').value == "Y" && this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialReportCtrl').value == "N")
        // || (this.pactPsychiatricPsychoMHRGroup.get('hasPsychiatricReportCtrl').value == "N" && this.isPsychiatricRequired === true)
        // || (this.pactPsychiatricPsychoMHRGroup.get('hasPsychosocialReportCtrl').value == "N" && this.isPsychiatricRequired === false)
        ) {
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(true);
      }
      else
        this.pactPsychiatricPsychoMHRGroup.controls['isMHRYesReportCtrl'].setValue(false);

      this.pactPsychiatricPsychoMHR.isMHRYes = this.pactPsychiatricPsychoMHRGroup.get('isMHRYesReportCtrl').value;
      this.pactPsychiatricPsychoMHR.pactApplicationID = this.pactApplicationID;
      this.pactPsychiatricPsychoMHR.userID = this.userData.optionUserId;

      //console.log('Data Passed ', this.pactPsychiatricPsychoMHR);

      this.psychiatricPsychosocialMHRService.savePACTPsychiatricPsychoMHR(this.pactPsychiatricPsychoMHR)
        .subscribe(data => {
          //console.log('Success!', data);
          if (!this.toastrService.currentlyActive)
            this.toastrService.success("Data has been saved.");

          //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);
          if (this.pactApplicationID) {
            this.sidenavStatusService.setApplicationIDForSidenavStatus(this.pactApplicationID);
          }


          this.populateForm(data);
        },
          error => {
            // if (!this.toastrService.currentlyActive)
            //   this.toastrService.error("There is an error while saving data.");
            throw new Error('There is an error while saving Supporting Documents Data.');
          }
          );
    }

  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    //const controlTouched = !!(formGroup && (formGroup.dirty || formGroup.touched));
    var controlTouched;

    if ((formGroup.controls.hasPsychiatricReportCtrl.dirty || formGroup.controls.hasPsychiatricReportCtrl.touched)
      || (formGroup.controls.hasPsychiatricAttachmentORDataEnterCtrl.dirty || formGroup.controls.hasPsychiatricAttachmentORDataEnterCtrl.touched)
      || (formGroup.controls.hasPsychosocialReportCtrl.dirty || formGroup.controls.hasPsychosocialReportCtrl.touched)
      || (formGroup.controls.hasPsychosocialAttachmentORDataEnterCtrl.dirty || formGroup.controls.hasPsychosocialAttachmentORDataEnterCtrl.touched)
    )
      controlTouched = true;
    else
      controlTouched = false;

    return controlTouched;
  }

  async ngOnDestroy() {
    this.savePsychiatricPsychosocialMHR();

    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }

    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }

    if (this.routeSub)
    {
      await this.routeSub.unsubscribe();
    }

  }

}
