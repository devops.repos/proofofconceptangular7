import {
  Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output, ViewChild
  , HostListener, OnDestroy
} from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

//Model References
//import { PACTMentalHealthReport, PACTMHRDiagnosis } from '../mhr/mhr.model';
import { PACTMentalHealthReport } from '../mhr/mhr.model';
import { AuthData } from 'src/app/models/auth-data.model';
import { PACTPsychiatricPsychoMHR, TabStatus } from '../psychiatric-psychosocial-mhr.model';
import { PACTClinicalMHRDiagnosisDetails, PACTClinicalMHRDiagnosisDetailsInput } from 'src/app/shared/diagnosis/diagnosis.model';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';

//Service References
import { PsychiatricPsychosocialMHRService } from '../psychiatric-psychosocial-mhr.service';
import { CommonService } from 'src/app/services/helper-services/common.service';
import { ToastrService } from 'ngx-toastr';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';
import { DiagnosisService } from 'src/app/shared/diagnosis/diagnosis.service';

//Ag-Grid Reference
import { AgGridAngular } from 'ag-grid-angular';
import 'ag-grid-enterprise';

//Enums
import { appStatusColor } from 'src/app/models/pact-enums.enum';


@Component({
  selector: 'app-mhr',
  templateUrl: './mhr.component.html',
  styleUrls: ['./mhr.component.scss'],
  providers: [DatePipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MhrComponent implements OnInit, OnDestroy {
  //Input Variables
  @Input() userData;
  @Input() pactPsychiatricPsychoMHR;
  @Input() tabStatus;

  //Output Variables
  @Output() optionSelected = new EventEmitter();
  @Output() tabNavigation = new EventEmitter<TabStatus>();

  //View Child
  @ViewChild('agGrid') agGrid: AgGridAngular;


  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  // Tab Status (Pending = 0 ; Complete = 1; Blank = 2;)
  tStatus = new TabStatus();
  blankTabStatus = 2; pendingTabStatus = 0; completedTabStatus = 1;
  historyTabStatus: number = this.blankTabStatus;
  socialFamilyWorkTabStatus: number = this.blankTabStatus;
  mentalStatusTabStatus: number = this.blankTabStatus;
  doctorRecommendationTabStatus: number = this.blankTabStatus;
  verificationTabStatus: number = this.blankTabStatus;

  pendingColor = appStatusColor.pendingColor;
  completedColor = appStatusColor.completedColor;
  noColor = appStatusColor.noColor;

  @Output() mentalHealthReportTabStatus = new EventEmitter<any>();

  routeSub: any; // Subscription to route observer

  uData: AuthData;
  psychiatricPsychoMHR: PACTPsychiatricPsychoMHR;
  mhrGroup: FormGroup;
  pactMentalHealthReport = new PACTMentalHealthReport();
  isSaveVisible: boolean = true;


  //Verification Variables
  verifiedDate: string;
  verifiedBy: number;
  verifiedByName: string;
  clinicianTitleTypes: RefGroupDetails[];
  collaboratingClinicianTitleTypes: RefGroupDetails[];
  isCollaboratingFieldsVisible : boolean = false;

  minDate = new Date(new Date().getTime() - 180 * 24 * 60 * 60 * 1000);
  maxDate = new Date(Date.now());

  selectedTab = 0;
  isControlTouched: boolean = false;
  isValid: boolean = false;
  isDiagnosisTouched : boolean = false;

  //Doctor Recommendation Variables
  medicalDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 48, historyOf: true, riskOf: true, provisional: false };
  principalDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 49, historyOf: true, riskOf: true, provisional: true };
  otherDiagnosis: PACTClinicalMHRDiagnosisDetails = { diagnosisTypeID: 51, historyOf: false, riskOf: true, provisional: false };
  mdList: PACTClinicalMHRDiagnosisDetails[] = [];
  pdList: PACTClinicalMHRDiagnosisDetails[] = [];
  odList: PACTClinicalMHRDiagnosisDetails[] = [];
  diagnosisList: PACTClinicalMHRDiagnosisDetails[] = [];
  diagnosisSearchList: PACTClinicalMHRDiagnosisDetails[] = [];
  hasMedicalDiagnosis: boolean;
  hasPrincipalDiagnosis: boolean;
  hasOtherDiagnosis: boolean;
  pactClinicalMHRDiagnosisDetailsInput: PACTClinicalMHRDiagnosisDetailsInput;

  //Validation
  validationControls : [string, string][] = [];

  //MHR Variables
  isMHRHistoryTabComplete: boolean;
  isMHRSFWTabComplete: boolean;
  isMHRMentalStatusTabComplete: boolean;
  isMHRDoctorRecommendationTabComplete: boolean;
  isMHRVerificationTabComplete: boolean;
  isMHRTabComplete: boolean;

  reportUrl: string;
  sanitizedReportUrl: SafeResourceUrl = null;
  reportName: string = 'MentalHealthReport';
  pactReportParameters: PACTReportParameters[] = [];
  guid: string;
  reportParams: PACTReportUrlParams;
  isReportLoaded: boolean = false;

  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;
  
  constructor(private formBuilder: FormBuilder
    , private psychiatricPsychosocialMHRService: PsychiatricPsychosocialMHRService
    , private cdRef: ChangeDetectorRef
    , private commonService: CommonService
    //, private confirmDialogService: ConfirmDialogService
    , private router: Router
    , private activatedRoute: ActivatedRoute
    , private toastrService: ToastrService
    , private sidenavStatusService: SidenavStatusService
    , private datePipe: DatePipe
    , private sanitizer: DomSanitizer
    , private diagnosisService : DiagnosisService
  ) {

  }

  //Save Data on page refresh
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (this.mhrGroup.dirty) {
      this.saveMHR(1);

      if (this.psychiatricPsychoMHR.pactApplicationID) {
        this.sidenavStatusService.setApplicationIDForSidenavStatus(this.psychiatricPsychoMHR.pactApplicationID);
      }

    }
  };

  ngAfterViewInit() {
    this.cdRef.detectChanges();
    //console.log('Change Detection called');
  }

  ngOnInit() {

    //Form Group
    this.mhrGroup = this.formBuilder.group({
      //History Controls
      presentIllnessHistoryCtrl: [''],
      medicalHistoryCtrl: [''],
      psychiatricHistoryCtrl: [''],
      substanceUseHistoryCtrl: [''],

      //Social/Family/Work Controls
      familyHistoryCtrl: [''],
      socialRelationsCtrl: [''],
      educationEmploymentCtrl: [''],
      legalCriminalCtrl: [''],
      housingHomelessCtrl: [''],

      //Mental Status Controls
      appearanceCtrl: [''],
      motorActivityCtrl: [''],
      affectCtrl: [''],
      moodCtrl: [''],
      speechCtrl: [''],
      thoughtFormCtrl: [''],
      insightCtrl: [''],
      cognitiveTestingCtrl: [''],
      thoughtContentCtrl: [''],
      judgementCtrl: [''],
      impulseControlCtrl: [''],

      //Medical Diagnosis Controls
      hasMedicalDiagnosisCtrl: [''],
      hasPsychiatricDiagnosisCtrl: [''],
      hasOtherDiagnosisCtrl: [''],
      recommendationCtrl: [''],
      formulationCtrl: [''],

      //Verification Controls
      verificationConsentCtrl: [''],
      nysLicensedClinicianTitleTypeCtrl: [''],
      psychiatricEvalDateCtrl: ['', Validators.compose([Validators.required, this.dateValidator])],
      clinicianNameCtrl: [''],
      clinicianLicenseNoCtrl: [''],
      collaboratingClinicianTitleType: [''],
      collaboratingClinicianName: [''],
      collaboratingClinicianLicenseNo: [''],

    });

    //Routing Event
    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.mhrGroup.touched && event.navigationTrigger != "popstate") {
          this.saveMHR(1);
        }
        if (event.navigationTrigger == "popstate" && this.mhrGroup.dirty) // Handles forward and backward browser clicks
        {
          this.saveMHR(1);
        }

      };
    });


    //Set Tab Status, User data
    this.tStatus = this.tabStatus;
    this.uData = this.userData;
    this.psychiatricPsychoMHR = this.pactPsychiatricPsychoMHR;

    //Get Refgroup Details
    var refGroupList = "44";
    this.commonService.getRefGroupDetails(refGroupList)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.clinicianTitleTypes = data;
          this.collaboratingClinicianTitleTypes = data.filter(d => (d.refGroupDetailID === 432 || d.refGroupDetailID === 434 || d.refGroupDetailID === 435));
        },
        error => {
          //console.log(error);
          // if (!this.toastrService.currentlyActive)
          //   this.toastrService.error("There is an error while fetching Clinician Titles.");
          
          throw new Error('There is an error while fetching Clinician Titles Data.');  
        }
      );

    //Load Form Data
    //console.log(this.psychiatricPsychoMHR);
    if (this.psychiatricPsychoMHR != null) {
      if (this.pactPsychiatricPsychoMHR.pactApplicationID > 0)
        this.loadData();
    }

  }

  //Load Form Data
  loadData() {
    //console.log('Load Data Called');
    //Get MHR Details
    this.psychiatricPsychosocialMHRService.getPACTMentalHealthReport(this.psychiatricPsychoMHR.pactApplicationID)
      .subscribe(
        res => {
          this.pactMentalHealthReport = res;
          this.populateForm(this.pactMentalHealthReport);

          //Get Diagnosis Details
          if (this.psychiatricPsychoMHR.pactApplicationID > 0)
            this.getGetPACTClinicalMHRDiagnosisDetails(this.psychiatricPsychoMHR.pactApplicationID);
        });

    //Get Ref Diagnosis
    this.getRefDiagnosis();
  }

  //Populate Form Data
  populateForm(formData: PACTMentalHealthReport) {
    //console.log('Populate MHR ', formData);

    if (formData !== null && formData.pactMentalHealthReportID > 0) {
      this.pactMentalHealthReport = formData;
      this.mhrGroup.controls['presentIllnessHistoryCtrl'].setValue(formData.presentIllnessHistory);
      this.mhrGroup.controls['medicalHistoryCtrl'].setValue(formData.medicalHistory);
      this.mhrGroup.controls['psychiatricHistoryCtrl'].setValue(formData.psychiatricHistory);
      this.mhrGroup.controls['substanceUseHistoryCtrl'].setValue(formData.substanceUse);

      this.mhrGroup.controls['familyHistoryCtrl'].setValue(formData.familyHistory);
      this.mhrGroup.controls['socialRelationsCtrl'].setValue(formData.socialRelations);
      this.mhrGroup.controls['educationEmploymentCtrl'].setValue(formData.educationEmployment);
      this.mhrGroup.controls['legalCriminalCtrl'].setValue(formData.legalCriminal);
      this.mhrGroup.controls['housingHomelessCtrl'].setValue(formData.housingHomeless);

      this.mhrGroup.controls['appearanceCtrl'].setValue(formData.appearance);
      this.mhrGroup.controls['motorActivityCtrl'].setValue(formData.motorActivity);
      this.mhrGroup.controls['affectCtrl'].setValue(formData.affect);
      this.mhrGroup.controls['moodCtrl'].setValue(formData.mood);
      this.mhrGroup.controls['speechCtrl'].setValue(formData.speech);
      this.mhrGroup.controls['thoughtFormCtrl'].setValue(formData.thoughtForm);
      this.mhrGroup.controls['insightCtrl'].setValue(formData.insight);
      this.mhrGroup.controls['thoughtContentCtrl'].setValue(formData.thoughtContent);
      this.mhrGroup.controls['cognitiveTestingCtrl'].setValue(formData.cognitiveTesting);
      this.mhrGroup.controls['judgementCtrl'].setValue(formData.judgement);
      this.mhrGroup.controls['impulseControlCtrl'].setValue(formData.impulseControl);
      this.mhrGroup.controls['impulseControlCtrl'].setValue(formData.impulseControl);

      if (formData.hasMedicalDiagnosis != null)
        this.mhrGroup.controls['hasMedicalDiagnosisCtrl'].setValue(formData.hasMedicalDiagnosis);
      else
        this.mhrGroup.controls['hasMedicalDiagnosisCtrl'].setValue(null);

      this.hasMedicalDiagnosis = this.mhrGroup.get('hasMedicalDiagnosisCtrl').value;

      if (formData.hasPrincipalDiagnosis != null)
        this.mhrGroup.controls['hasPsychiatricDiagnosisCtrl'].setValue(formData.hasPrincipalDiagnosis);
      else
        this.mhrGroup.controls['hasPsychiatricDiagnosisCtrl'].setValue(null);

      this.hasPrincipalDiagnosis = this.mhrGroup.get('hasPsychiatricDiagnosisCtrl').value;

      if (formData.hasOtherDiagnosis != null)
        this.mhrGroup.controls['hasOtherDiagnosisCtrl'].setValue(formData.hasOtherDiagnosis);
      else
        this.mhrGroup.controls['hasOtherDiagnosisCtrl'].setValue(null);

      this.hasOtherDiagnosis = this.mhrGroup.get('hasOtherDiagnosisCtrl').value;

      this.mhrGroup.controls['recommendationCtrl'].setValue(formData.recommendation);
      this.mhrGroup.controls['formulationCtrl'].setValue(formData.formulation);


      this.mhrGroup.controls['verificationConsentCtrl'].setValue(formData.verificationConsent);
      this.mhrGroup.controls['nysLicensedClinicianTitleTypeCtrl'].setValue(formData.nysLicensedClinicianTitleType);
      this.mhrGroup.controls['psychiatricEvalDateCtrl'].setValue(formData.psychiatricEvalDate);
      this.mhrGroup.controls['clinicianNameCtrl'].setValue(formData.clinicianName);
      this.mhrGroup.controls['clinicianLicenseNoCtrl'].setValue(formData.clinicianLicenseNo);
      this.verifiedDate = formData.verifiedDate != null ? this.datePipe.transform(formData.verifiedDate, "MM/dd/yyyy") : null;
      this.verifiedByName = formData.verifiedByName != null ? formData.verifiedByName : null;
      this.verifiedBy = formData.verifiedBy != null ? formData.verifiedBy : null;

      this.mhrGroup.controls['collaboratingClinicianTitleType'].setValue(formData.collaboratingClinicianTitleType);
      this.mhrGroup.controls['collaboratingClinicianName'].setValue(formData.collaboratingClinicianName);
      this.mhrGroup.controls['collaboratingClinicianLicenseNo'].setValue(formData.collaboratingClinicianLicenseNo);

      if(formData.nysLicensedClinicianTitleType === 932)
        this.isCollaboratingFieldsVisible = true;
      else
        this.isCollaboratingFieldsVisible = false;
      
    }
    else {

      this.pactMentalHealthReport = new PACTMentalHealthReport();
      this.pactMentalHealthReport.pactMentalHealthReportID = null;
      this.pactMentalHealthReport.pactPsychiatricPsychoMHRID = this.psychiatricPsychoMHR.pactPsychiatricPsychoMHRID;

      this.mhrGroup.controls['presentIllnessHistoryCtrl'].setValue(null);
      this.mhrGroup.controls['medicalHistoryCtrl'].setValue(null);
      this.mhrGroup.controls['psychiatricHistoryCtrl'].setValue(null);
      this.mhrGroup.controls['substanceUseHistoryCtrl'].setValue(null);

      this.mhrGroup.controls['familyHistoryCtrl'].setValue(null);
      this.mhrGroup.controls['socialRelationsCtrl'].setValue(null);
      this.mhrGroup.controls['educationEmploymentCtrl'].setValue(null);
      this.mhrGroup.controls['legalCriminalCtrl'].setValue(null);
      this.mhrGroup.controls['housingHomelessCtrl'].setValue(null);

      this.mhrGroup.controls['appearanceCtrl'].setValue(null);
      this.mhrGroup.controls['motorActivityCtrl'].setValue(null);
      this.mhrGroup.controls['affectCtrl'].setValue(null);
      this.mhrGroup.controls['moodCtrl'].setValue(null);
      this.mhrGroup.controls['speechCtrl'].setValue(null);
      this.mhrGroup.controls['thoughtFormCtrl'].setValue(null);
      this.mhrGroup.controls['insightCtrl'].setValue(null);
      this.mhrGroup.controls['thoughtContentCtrl'].setValue(null);
      this.mhrGroup.controls['cognitiveTestingCtrl'].setValue(null);
      this.mhrGroup.controls['judgementCtrl'].setValue(null);
      this.mhrGroup.controls['impulseControlCtrl'].setValue(null);

      this.mhrGroup.controls['hasMedicalDiagnosisCtrl'].setValue(null);
      this.mhrGroup.controls['hasPsychiatricDiagnosisCtrl'].setValue(null);
      this.mhrGroup.controls['hasOtherDiagnosisCtrl'].setValue(null);
      this.mhrGroup.controls['recommendationCtrl'].setValue(null);
      this.mhrGroup.controls['formulationCtrl'].setValue(null);

      this.mhrGroup.controls['verificationConsentCtrl'].setValue(null);
      this.mhrGroup.controls['nysLicensedClinicianTitleTypeCtrl'].setValue(null);
      this.mhrGroup.controls['psychiatricEvalDateCtrl'].setValue(null);
      this.mhrGroup.controls['clinicianNameCtrl'].setValue(null);
      this.mhrGroup.controls['clinicianLicenseNoCtrl'].setValue(null);
      this.mhrGroup.controls['collaboratingClinicianTitleType'].setValue(null);
      this.mhrGroup.controls['collaboratingClinicianName'].setValue(null);
      this.mhrGroup.controls['collaboratingClinicianLicenseNo'].setValue(null);
      this.verifiedDate = null;
      this.verifiedByName = null;
      this.verifiedBy = null;
     
      this.isMHRHistoryTabComplete = null;
      this.isMHRSFWTabComplete = null;
      this.isMHRMentalStatusTabComplete = null;
      this.isMHRDoctorRecommendationTabComplete = null;
      this.isMHRVerificationTabComplete = null;

      //Create Entry in Table
      if (this.pactMentalHealthReport.pactPsychiatricPsychoMHRID != null)
        this.saveMHR(0);
    }

    this.setTabStatus(formData);
      
  }

  //Set Tab Status
  setTabStatus(formData: PACTMentalHealthReport) {
    //console.log(formData);
    if (formData != null) {
      this.isMHRHistoryTabComplete = formData.isMHRHistoryTabComplete != null ? formData.isMHRHistoryTabComplete : null;
      this.historyTabStatus = this.isMHRHistoryTabComplete != null ? this.isMHRHistoryTabComplete === true ? this.completedTabStatus : this.pendingTabStatus : this.blankTabStatus;

      this.isMHRSFWTabComplete = formData.isMHRSFWTabComplete != null ? formData.isMHRSFWTabComplete : null;
      this.socialFamilyWorkTabStatus = this.isMHRSFWTabComplete != null ? this.isMHRSFWTabComplete === true ? this.completedTabStatus : this.pendingTabStatus : this.blankTabStatus;

      this.isMHRMentalStatusTabComplete = formData.isMHRMentalStatusTabComplete != null ? formData.isMHRMentalStatusTabComplete : null;
      this.mentalStatusTabStatus = this.isMHRMentalStatusTabComplete != null ? this.isMHRMentalStatusTabComplete === true ? this.completedTabStatus : this.pendingTabStatus : this.blankTabStatus;

      this.isMHRDoctorRecommendationTabComplete = formData.isMHRDoctorRecommendationTabComplete != null ? formData.isMHRDoctorRecommendationTabComplete : null;
      this.doctorRecommendationTabStatus = this.isMHRDoctorRecommendationTabComplete != null ? this.isMHRDoctorRecommendationTabComplete === true ? this.completedTabStatus : this.pendingTabStatus : this.blankTabStatus;

      this.isMHRVerificationTabComplete = formData.isMHRVerificationTabComplete != null ? formData.isMHRVerificationTabComplete : null;
      this.verificationTabStatus = this.isMHRVerificationTabComplete != null ? this.isMHRVerificationTabComplete === true ? this.completedTabStatus : this.pendingTabStatus : this.blankTabStatus;
    }
    else {
      this.historyTabStatus = this.blankTabStatus;
      this.isMHRHistoryTabComplete = null;

      this.socialFamilyWorkTabStatus = this.blankTabStatus;
      this.isMHRSFWTabComplete = null;

      this.mentalStatusTabStatus = this.blankTabStatus;
      this.isMHRMentalStatusTabComplete = null;

      this.doctorRecommendationTabStatus = this.blankTabStatus;
      this.isMHRDoctorRecommendationTabComplete = null;

      this.verificationTabStatus = this.blankTabStatus;
      this.isMHRVerificationTabComplete = null;
    }

    if (this.isMHRHistoryTabComplete === true && this.isMHRSFWTabComplete === true && this.isMHRMentalStatusTabComplete === true
      && this.isMHRDoctorRecommendationTabComplete === true && this.isMHRVerificationTabComplete === true)
      this.isMHRTabComplete = true;
    else if (this.isMHRHistoryTabComplete === false || this.isMHRSFWTabComplete === false || this.isMHRMentalStatusTabComplete === false
      || this.isMHRDoctorRecommendationTabComplete === false || this.isMHRVerificationTabComplete === false)
      this.isMHRTabComplete = false;
    else if (this.isMHRHistoryTabComplete === null && this.isMHRSFWTabComplete === null && this.isMHRMentalStatusTabComplete === null
      && this.isMHRDoctorRecommendationTabComplete === null && this.isMHRVerificationTabComplete === null)
      this.isMHRTabComplete = null;

    this.mentalHealthReportTabStatus.emit(this.isMHRTabComplete);
  }

  //Get Ref Diagnosis
  getRefDiagnosis() {
    this.commonService.getRefDiagnosis()
      .subscribe(
        res => {
          const data = res as PACTClinicalMHRDiagnosisDetails[];
          this.diagnosisSearchList = data;
        },
        error => {
          // if (!this.toastrService.currentlyActive)
          //   this.toastrService.error("There is an error while fetching Diagnosis.");

          throw new Error('There is an error while fetching Diagnosis Data.');  
        }
      );
  }

  addDiagnosis(diagnosis: PACTClinicalMHRDiagnosisDetails): void {
    //console.log(diagnosis);
    this.pactClinicalMHRDiagnosisDetailsInput = {
      pactApplicationID: this.psychiatricPsychoMHR.pactApplicationID,
      pactClinicalMHRDiagnosisDetails: diagnosis,
      userID: this.uData.optionUserId
    };

    //console.log('Add Diagnosis', this.pactMHRDiagnosis);
    this.diagnosisService.savePACTClinicalMHRDiagnosisDetails(this.pactClinicalMHRDiagnosisDetailsInput)
      .subscribe(data => {
        this.getGetPACTClinicalMHRDiagnosisDetails(this.psychiatricPsychoMHR.pactApplicationID);
      },
      error => {
        //console.log(error);
        // if (!this.toastrService.currentlyActive)
        //   this.toastrService.error("There is an error while saving Diagnosis.");
        throw new Error('There is an error while saving Diagnosis Data.');  
      });

  }

  //Get Diagnosis Details
  getGetPACTClinicalMHRDiagnosisDetails(pactApplicationID: number) {
    //console.log('Get Diagnosis ', pactMentalHealthReportID);
    //this.psychiatricPsychosocialMHRService.getPACTMHRDiagnosis(pactMentalHealthReportID)
    this.diagnosisService.getGetPACTClinicalMHRDiagnosisDetails(pactApplicationID)
      .subscribe(
        res => {
          const data = res as PACTClinicalMHRDiagnosisDetails[];
          this.diagnosisList = data;
          //console.log('diagnosisList', data.length);
          if (data.length > 0) {
            this.refreshDiagnosisList();
          }
          else
          {
            this.refreshDiagnosisChanged();
          }
        },
        error => {
          //console.log(error);
          // if (!this.toastrService.currentlyActive)
          //   this.toastrService.error("There is an error while fetching Diagnosis.");
          throw new Error('There is an error while fetching Diagnosis Data.');  
        }
      );
  }

  refreshDiagnosisList() {
    //console.log("refresh DiagnosisList triggered");
    this.refreshDiagnosisChanged();

    //Update Diagnosis Flag Value
    this.saveMHR(0);
  }

  removeDiagnosis(diagnosis: PACTClinicalMHRDiagnosisDetails): void {
    //console.log("removeDiagnosisList triggered");
    const index = this.diagnosisList.filter(x => x.diagnosisID == diagnosis.diagnosisID).length;
    //console.log('Index', index);
    if (index > 0) {
      this.pactClinicalMHRDiagnosisDetailsInput = {
        // pactMentalHealthReportID: this.pactMentalHealthReport.pactMentalHealthReportID,
        // pactClinicalMHRDiagnosisDetails: diagnosis,
        // userid: this.uData.optionUserId
        pactApplicationID: this.psychiatricPsychoMHR.pactApplicationID,
        pactClinicalMHRDiagnosisDetails: diagnosis,
        userID: this.uData.optionUserId
      };
      //console.log(this.pactClinicalMHRDiagnosisDetailsInput);
      this.diagnosisService.deletePACTClinicalMHRDiagnosisDetails(this.pactClinicalMHRDiagnosisDetailsInput)
        .subscribe(res => {
          this.getGetPACTClinicalMHRDiagnosisDetails(this.pactPsychiatricPsychoMHR.pactApplicationID);
        },
          error => {
            //console.error('Error!', error);
            // if (!this.toastrService.currentlyActive)
            // this.toastrService.error("There is an error while deleting diagnosis.");
            throw new Error('There is an error while deleting Diagnosis Data.');  
          }
        );

    }

  }

  diagnosisChanged(itm: any): void {
    //console.log("diagnosisChanged triggered");
    //console.log('Item ', itm);
    if (itm['diagnosisID'] === 48) {
      this.hasMedicalDiagnosis = itm['value'];
      this.mhrGroup.controls['hasMedicalDiagnosisCtrl'].setValue(this.hasMedicalDiagnosis);
      this.diagnosisList.filter(x => x.diagnosisTypeID === 48).forEach(x => x.isActive = false);
    } else if (itm['diagnosisID'] === 49) {
      this.hasPrincipalDiagnosis = itm['value'];
      this.mhrGroup.controls['hasPsychiatricDiagnosisCtrl'].setValue(this.hasPrincipalDiagnosis);
      this.diagnosisList.filter(x => x.diagnosisTypeID === 49).forEach(x => x.isActive = false);
    } else if (itm['diagnosisID'] === 51) {
      this.hasOtherDiagnosis = itm['value'];
      this.mhrGroup.controls['hasOtherDiagnosisCtrl'].setValue(this.hasOtherDiagnosis);
      this.diagnosisList.filter(x => x.diagnosisTypeID === 51).forEach(x => x.isActive = false);
    }

    //console.log('diagnosisChanged - ',  this.mhrGroup.get('hasMedicalDiagnosisCtrl').value);
    this.isDiagnosisTouched = true;
    this.saveMHR(1);

    //Refresh Diagnosis Changes
    this.refreshDiagnosisChanged();
  }

  refreshDiagnosisChanged()
  {
    this.mdList = this.diagnosisList.filter(x => x.diagnosisTypeID === 48 && x.isActive === true);
    this.pdList = this.diagnosisList.filter(x => x.diagnosisTypeID === 49 && x.isActive === true);
    this.odList = this.diagnosisList.filter(x => x.diagnosisTypeID === 51 && x.isActive === true);
  }

  //On Consent check change
  onIsConsentCheckChange(event: { checked: Boolean }) {
    if (event.checked) {
      this.mhrGroup.controls['verificationConsentCtrl'].setValue(true);
      this.verifiedBy = this.uData.optionUserId;
      this.verifiedByName = this.uData.firstName + " " + this.uData.lastName;
      this.verifiedDate = this.datePipe.transform(new Date(), "MM/dd/yyyy");
    }
    else {
      this.mhrGroup.controls['verificationConsentCtrl'].setValue(false);
      this.verifiedBy = null;
      this.verifiedByName = null;
      this.verifiedDate = null;
    }
  }

  //Tab Changes Event
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    //Saving Data Between Tab Navigation
    this.saveMHR(1);
    this.selectedTab = tabChangeEvent.index;

    //Show/Hide Save Button
    this.isSaveButtonVisible(this.selectedTab);
  }

  //Next button click
  nextTab() {
    if (this.selectedTab < 5) {
      this.saveMHR(1);
      this.selectedTab = this.selectedTab + 1;

      //Show/Hide Save Button
      this.isSaveButtonVisible(this.selectedTab);
    }
    else if (this.selectedTab >= 5) {
      //this.router.navigate(['/shs/newApp/documents', this.psychiatricPsychoMHR.pactApplicationID]);
      this.sidenavStatusService.routeToNextPage(this.router, this.activatedRoute);
    }
  }

  //Previous button click
  previousTab() {
    if (this.selectedTab != 0) {
      this.saveMHR(1);
      this.selectedTab = this.selectedTab - 1;

      //Show/Hide Save Button
      this.isSaveButtonVisible(this.selectedTab);

    }
    else if (this.selectedTab <= 0) {
      this.tabStatus.childTabIndex = this.selectedTab;
      this.tabStatus.parentTabIndex = 3;
      this.tabNavigation.emit(this.tabStatus);
    }

  }

  //Hide Save Button for MHR Report Tab
  isSaveButtonVisible(tabNumber: number) {
    if (this.selectedTab == 4) {
      this.isSaveVisible = false;
      this.isReportLoaded = false;
      this.sanitizedReportUrl = null;

      this.pactReportParameters = [
        { parameterName: 'applicationID', parameterValue: this.pactPsychiatricPsychoMHR.pactApplicationID, CreatedBy: this.userData.optionUserId },
        { parameterName: 'reportName', parameterValue: this.reportName, CreatedBy: this.userData.optionUserId },
        { parameterName: 'source', parameterValue: this.router.url, CreatedBy: this.userData.optionUserId },
      ];

      this.commonService.generateGUID(this.pactReportParameters)
        .subscribe(
          res => {
            if (res !== null) {
              this.guid = res;
              //console.log('GUID ', this.guid);
              this.reportParams = { reportParameterID: this.guid, reportName: this.reportName, "reportFormat": "PDF" };

              this.commonService.generateReport(this.reportParams)
                .subscribe(
                  res => {
                    var data = new Blob([res.body], { type: 'application/pdf' });
                    //console.log('Report Data - ',data);
                    if (data.size > 512) {

                      this.reportUrl = URL.createObjectURL(data);
                      //console.log('Report Url - ',this.reportUrl);

                      this.sanitizedReportUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.reportUrl);
                      this.isReportLoaded = true;
                      this.cdRef.detectChanges();

                      //console.log('Loading Flag - ',this.isReportLoaded);

                    }
                  },
                  error => {
                    // if (!this.toastrService.currentlyActive)
                    //   this.toastrService.error("There is an error while generating report.");

                    throw new Error('There is an error while generating Mental Health Report.');  

                      //this.isLoading = false;
                  }
                );

            }

          },
          error => {
            // if (!this.toastrService.currentlyActive)
            //   this.toastrService.error("There is an error while generating report.");

            throw new Error('There is an error while generating Mental Health Report.'); 

          }
        );
    }
    else
    {
      this.isSaveVisible = true;
      this.isReportLoaded = false;
      this.sanitizedReportUrl = null;
    }
  }

  //Validate Date
  dateValidator(AC: AbstractControl) {
    if (AC && AC.value && !moment(AC.value, 'MM/DD/YYYY', true).isValid()) {
      return { 'dateValidator': true };
    }
    return null;
  }

  //Validate Date in last 180 days
  validateDate() {
    var psychiatricEvalDate = this.datePipe.transform(this.mhrGroup.get('psychiatricEvalDateCtrl').value, 'MM/dd/yyyy');
    //console.log(psychiatricEvalDate);
    //console.log(moment(psychiatricEvalDate, 'MM/DD/YYYY', true).isValid());
    //if (!moment(this.mhrGroup.get('psychiatricEvalDateCtrl').value, 'MM/DD/YYYY', true).isValid()) {
    if (!moment(psychiatricEvalDate, 'MM/DD/YYYY', true).isValid()) {
      if (!this.toastrService.currentlyActive)
        this.toastrService.error("Invalid Date.");

      this.isValid = false;
      //this.mhrGroup.controls['psychiatricEvalDateCtrl'].setValue("");
      //return;
    }
    else {
      //var psychiatricEvalDate = this.datePipe.transform(this.mhrGroup.get('psychiatricEvalDateCtrl').value, 'MM/dd/yyyy');
      if (psychiatricEvalDate) {
        let days = moment(Date.now()).diff(moment(psychiatricEvalDate), 'day', true);
        //console.log(days);
        if (days > 181 || days < -0) {
          if (!this.toastrService.currentlyActive)
            this.toastrService.error("Date of Psychiatric Evaluation must be within 180 days of today's date.");
          
          this.mhrGroup.controls['psychiatricEvalDateCtrl'].setValue(null);
          this.isValid = false;
        }
        else {
          this.mhrGroup.controls.psychiatricEvalDateCtrl.clearValidators();
          this.mhrGroup.controls.psychiatricEvalDateCtrl.updateValueAndValidity();
          this.isValid = true;
        }
      }
    }
  }

  //Open PDF document in a new window with print dialog
  printReport() {
    if (this.reportUrl != null)
    {
      //var fileURL = URL.createObjectURL(this.reportData);
      const printContent = window.open(this.reportUrl, "_blank");
      if (printContent != null) {
        printContent.focus();
        printContent.print();
      }
    }
    else{
      if (!this.toastrService.currentlyActive)
        this.toastrService.error("There is an error while generating report.");
    }
  }

  onClinicianTitleTypeChange()
  {
    if (this.mhrGroup.get('nysLicensedClinicianTitleTypeCtrl').value === 932)
      this.isCollaboratingFieldsVisible = true;
    else
    {
      this.isCollaboratingFieldsVisible = false; 

      this.mhrGroup.controls['collaboratingClinicianTitleType'].setValue(null);
      this.mhrGroup.controls['collaboratingClinicianName'].setValue(null);
      this.mhrGroup.controls['collaboratingClinicianLicenseNo'].setValue(null);
    }
  }

  //Save MHR various tab details
  //callType to avoid API call - 0 : first time for entry creation, 1 : after first time if page control is changed
  saveMHR(callType: number) {
    //Verify if controls is touched
    if (callType === 0) {
      this.isControlTouched = true;
    }
    else if (callType === 1) {
      this.isControlTouched = this.markFormGroupTouched(this.mhrGroup);
    }

    //console.log('MHR Control Touched - callType ', this.isControlTouched, callType);
    if(this.isControlTouched){

      this.validateControlsLength(this.mhrGroup);
      // if (this.validateControlsLength(this.mhrGroup))
      // {
        //console.log('Tab - ',this.selectedTab);
        //Validate Psychiatric Evaluation Date
        if (this.selectedTab === 5) {
          if (this.mhrGroup.get('psychiatricEvalDateCtrl').value != null)
            this.validateDate();
          else
            this.isValid = true;
        }
        else {
          this.isValid = true;
        }

        //console.log('MHR IsValid ', this.isValid);
        if (this.isValid) {
          //switch (this.selectedTab) {
          //  case 0:

          this.pactMentalHealthReport.presentIllnessHistory = this.mhrGroup.get('presentIllnessHistoryCtrl').value != null ? this.mhrGroup.get('presentIllnessHistoryCtrl').value : null;
          this.pactMentalHealthReport.medicalHistory = this.mhrGroup.get('medicalHistoryCtrl').value != null ? this.mhrGroup.get('medicalHistoryCtrl').value : null;
          this.pactMentalHealthReport.psychiatricHistory = this.mhrGroup.get('psychiatricHistoryCtrl').value != null ? this.mhrGroup.get('psychiatricHistoryCtrl').value : null;
          this.pactMentalHealthReport.substanceUse = this.mhrGroup.get('substanceUseHistoryCtrl').value != null ? this.mhrGroup.get('substanceUseHistoryCtrl').value : null;
          //    break;
          //  case 1:
          this.pactMentalHealthReport.familyHistory = this.mhrGroup.get('familyHistoryCtrl').value != null ? this.mhrGroup.get('familyHistoryCtrl').value : null;
          this.pactMentalHealthReport.socialRelations = this.mhrGroup.get('socialRelationsCtrl').value != null ? this.mhrGroup.get('socialRelationsCtrl').value : null;
          this.pactMentalHealthReport.educationEmployment = this.mhrGroup.get('educationEmploymentCtrl').value != null ? this.mhrGroup.get('educationEmploymentCtrl').value : null;
          this.pactMentalHealthReport.legalCriminal = this.mhrGroup.get('legalCriminalCtrl').value != null ? this.mhrGroup.get('legalCriminalCtrl').value : null;
          this.pactMentalHealthReport.housingHomeless = this.mhrGroup.get('housingHomelessCtrl').value != null ? this.mhrGroup.get('housingHomelessCtrl').value : null;
          //   break;
          // case 2:
          this.pactMentalHealthReport.appearance = this.mhrGroup.get('appearanceCtrl').value != null ? this.mhrGroup.get('appearanceCtrl').value : null;
          this.pactMentalHealthReport.motorActivity = this.mhrGroup.get('motorActivityCtrl').value != null ? this.mhrGroup.get('motorActivityCtrl').value : null;
          this.pactMentalHealthReport.affect = this.mhrGroup.get('affectCtrl').value != null ? this.mhrGroup.get('affectCtrl').value : null;
          this.pactMentalHealthReport.mood = this.mhrGroup.get('moodCtrl').value != null ? this.mhrGroup.get('moodCtrl').value : null;
          this.pactMentalHealthReport.speech = this.mhrGroup.get('speechCtrl').value != null ? this.mhrGroup.get('speechCtrl').value : null;
          this.pactMentalHealthReport.thoughtForm = this.mhrGroup.get('thoughtFormCtrl').value != null ? this.mhrGroup.get('thoughtFormCtrl').value : null;
          this.pactMentalHealthReport.insight = this.mhrGroup.get('insightCtrl').value != null ? this.mhrGroup.get('insightCtrl').value : null;
          this.pactMentalHealthReport.cognitiveTesting = this.mhrGroup.get('cognitiveTestingCtrl').value != null ? this.mhrGroup.get('cognitiveTestingCtrl').value : null;
          this.pactMentalHealthReport.thoughtContent = this.mhrGroup.get('thoughtContentCtrl').value != null ? this.mhrGroup.get('thoughtContentCtrl').value : null;
          this.pactMentalHealthReport.judgement = this.mhrGroup.get('judgementCtrl').value != null ? this.mhrGroup.get('judgementCtrl').value : null;
          this.pactMentalHealthReport.impulseControl = this.mhrGroup.get('impulseControlCtrl').value != null ? this.mhrGroup.get('impulseControlCtrl').value : null;
          //   break;
          // case 3:
          this.pactMentalHealthReport.hasMedicalDiagnosis = this.mhrGroup.get('hasMedicalDiagnosisCtrl').value != null ? this.mhrGroup.get('hasMedicalDiagnosisCtrl').value : null;
          this.pactMentalHealthReport.hasPrincipalDiagnosis = this.mhrGroup.get('hasPsychiatricDiagnosisCtrl').value != null ? this.mhrGroup.get('hasPsychiatricDiagnosisCtrl').value : null;
          this.pactMentalHealthReport.hasOtherDiagnosis = this.mhrGroup.get('hasOtherDiagnosisCtrl').value != null ? this.mhrGroup.get('hasOtherDiagnosisCtrl').value : null;
          this.pactMentalHealthReport.recommendation = this.mhrGroup.get('recommendationCtrl').value != null ? this.mhrGroup.get('recommendationCtrl').value : null;
          this.pactMentalHealthReport.formulation = this.mhrGroup.get('formulationCtrl').value != null ? this.mhrGroup.get('formulationCtrl').value : null;
          // break;
          // }

          this.pactMentalHealthReport.verificationConsent = this.mhrGroup.get('verificationConsentCtrl').value != null ? this.mhrGroup.get('verificationConsentCtrl').value : null;
          this.pactMentalHealthReport.nysLicensedClinicianTitleType = this.mhrGroup.get('nysLicensedClinicianTitleTypeCtrl').value != null ? this.mhrGroup.get('nysLicensedClinicianTitleTypeCtrl').value : null;
          this.pactMentalHealthReport.psychiatricEvalDate = this.mhrGroup.get('psychiatricEvalDateCtrl').value != null ? this.mhrGroup.get('psychiatricEvalDateCtrl').value : null;
          this.pactMentalHealthReport.clinicianName = this.mhrGroup.get('clinicianNameCtrl').value != null ? this.mhrGroup.get('clinicianNameCtrl').value : null;
          this.pactMentalHealthReport.clinicianLicenseNo = this.mhrGroup.get('clinicianLicenseNoCtrl').value != null ? this.mhrGroup.get('clinicianLicenseNoCtrl').value : null;
          this.pactMentalHealthReport.verifiedBy = this.verifiedBy;
          this.pactMentalHealthReport.verifiedDate = this.verifiedDate;

          this.pactMentalHealthReport.collaboratingClinicianTitleType = this.mhrGroup.get('collaboratingClinicianTitleType').value;
          this.pactMentalHealthReport.collaboratingClinicianName = this.mhrGroup.get('collaboratingClinicianName').value;
          this.pactMentalHealthReport.collaboratingClinicianLicenseNo = this.mhrGroup.get('collaboratingClinicianLicenseNo').value;
        
          this.pactMentalHealthReport.isMHRHistoryTabComplete = this.isMHRHistoryTabComplete !== null ? this.isMHRHistoryTabComplete : null;
          this.pactMentalHealthReport.isMHRSFWTabComplete = this.isMHRSFWTabComplete !== null ? this.isMHRSFWTabComplete : null;
          this.pactMentalHealthReport.isMHRMentalStatusTabComplete = this.isMHRMentalStatusTabComplete !== null ? this.isMHRMentalStatusTabComplete : null;
          this.pactMentalHealthReport.isMHRDoctorRecommendationTabComplete = this.isMHRDoctorRecommendationTabComplete !== null ? this.isMHRDoctorRecommendationTabComplete : null;
          this.pactMentalHealthReport.isMHRVerificationTabComplete = this.isMHRVerificationTabComplete !== null ? this.isMHRVerificationTabComplete : null;

          this.pactMentalHealthReport.userID = this.uData.optionUserId;
          this.pactMentalHealthReport.pactPsychiatricPsychoMHRID = this.psychiatricPsychoMHR.pactPsychiatricPsychoMHRID;
          this.pactMentalHealthReport.pactApplicationID = this.psychiatricPsychoMHR.pactApplicationID;

          //console.log('Save MHR', this.pactMentalHealthReport);

          this.psychiatricPsychosocialMHRService.savePACTMentalHealthReport(this.pactMentalHealthReport)
            .subscribe(data => {
              this.pactMentalHealthReport = data;
              if (!this.toastrService.currentlyActive)
                this.toastrService.success("Data has been saved.");

              //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);
              if (this.psychiatricPsychoMHR.pactApplicationID) {
                this.sidenavStatusService.setApplicationIDForSidenavStatus(this.psychiatricPsychoMHR.pactApplicationID);
              }


              this.populateForm(this.pactMentalHealthReport);
            },
              error => {
                // if (!this.toastrService.currentlyActive)
                //   this.toastrService.error("There is an error while saving data.");
                throw new Error('There is an error while saving Mental Health Report Data.'); 
              });

        }

      // }

    }

  }

  validateControlsLength(formGroup: FormGroup)
  {
    this.validationControls = [];
    var characterValidation : boolean = true;
    switch(this.selectedTab)
    {
        //History
        case 0:
          var controlValue = formGroup.controls.presentIllnessHistoryCtrl.value != null ? formGroup.controls.presentIllnessHistoryCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('presentIllnessHistoryCtrl', 'Chief Complaint and History of Present Illness');
            characterValidation = false;
          }

          controlValue = formGroup.controls.medicalHistoryCtrl.value != null ? formGroup.controls.medicalHistoryCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('medicalHistoryCtrl', 'Medical History');
            characterValidation = false;
          }

          controlValue = formGroup.controls.psychiatricHistoryCtrl.value != null ? formGroup.controls.psychiatricHistoryCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('psychiatricHistoryCtrl', 'Psychiatric History');
            characterValidation = false;
          }

          controlValue = formGroup.controls.substanceUseHistoryCtrl.value != null ? formGroup.controls.substanceUseHistoryCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('substanceUseHistoryCtrl', 'Substance Use');
            characterValidation = false;
          }

          if(characterValidation === false)
            this.clearControls(this.validationControls);

        break;
        //Social/Family/Works
        case 1:
          var controlValue = formGroup.controls.familyHistoryCtrl.value != null ? formGroup.controls.familyHistoryCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('familyHistoryCtrl', 'Family History');
            characterValidation = false;
          }

          controlValue = formGroup.controls.socialRelationsCtrl.value != null ? formGroup.controls.socialRelationsCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('socialRelationsCtrl', 'Social Relations');
            characterValidation = false;
          }

          controlValue = formGroup.controls.educationEmploymentCtrl.value != null ? formGroup.controls.educationEmploymentCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('educationEmploymentCtrl', 'Education/Employment');
            characterValidation = false;
          }

          controlValue = formGroup.controls.legalCriminalCtrl.value != null ? formGroup.controls.legalCriminalCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('legalCriminalCtrl', 'Legal/Criminal');
            characterValidation = false;
          }

          controlValue = formGroup.controls.housingHomelessCtrl.value != null ? formGroup.controls.housingHomelessCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('housingHomelessCtrl', 'Housing Homeless');
            characterValidation = false;
          }

          if(characterValidation === false)
            this.clearControls(this.validationControls);

        break;
        //Mental Status
        case 2:
          var controlValue = formGroup.controls.appearanceCtrl.value != null ? formGroup.controls.appearanceCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('appearanceCtrl', 'Appearance');
            characterValidation = false;
          }

          controlValue = formGroup.controls.motorActivityCtrl.value != null ? formGroup.controls.motorActivityCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('motorActivityCtrl', 'Motor Activity');
            characterValidation = false;
          }

          controlValue = formGroup.controls.affectCtrl.value != null ? formGroup.controls.affectCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('affectCtrl', 'Affect');
            characterValidation = false;
          }

          controlValue = formGroup.controls.moodCtrl.value != null ? formGroup.controls.moodCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('moodCtrl', 'Mood');
            characterValidation = false;
          }

          controlValue = formGroup.controls.speechCtrl.value != null ? formGroup.controls.speechCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('speechCtrl', 'Speech');
            characterValidation = false;
          }

          controlValue = formGroup.controls.thoughtFormCtrl.value != null ? formGroup.controls.thoughtFormCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('thoughtFormCtrl', 'Thought Form');
            characterValidation = false;
          }

          controlValue = formGroup.controls.insightCtrl.value != null ? formGroup.controls.insightCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('insightCtrl', 'Insight');
            characterValidation = false;
          }

          controlValue = formGroup.controls.cognitiveTestingCtrl.value != null ? formGroup.controls.cognitiveTestingCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('cognitiveTestingCtrl', 'Cognitive Testing');
            characterValidation = false;
          }

          controlValue = formGroup.controls.thoughtContentCtrl.value != null ? formGroup.controls.thoughtContentCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('thoughtContentCtrl', 'Thought Content');
            characterValidation = false;
          }

          controlValue = formGroup.controls.judgementCtrl.value != null ? formGroup.controls.judgementCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('judgementCtrl', 'Judgement');
          }

          controlValue = formGroup.controls.impulseControlCtrl.value != null ? formGroup.controls.impulseControlCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('impulseControlCtrl', 'Impulse Control');
            characterValidation = false;
          }

          if(characterValidation === false)
            this.clearControls(this.validationControls);

        break;
        //Doctor Recommendation
        case 3:
          var controlValue = formGroup.controls.recommendationCtrl.value != null ? formGroup.controls.recommendationCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('recommendationCtrl', 'Recommendation');
            characterValidation = false;
          }

          controlValue = formGroup.controls.formulationCtrl.value != null ? formGroup.controls.formulationCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('formulationCtrl', 'Formulation');
            characterValidation = false;
          }

          if(characterValidation === false)
            this.clearControls(this.validationControls);

        break;
        //Verification
        case 5:
          var controlValue = formGroup.controls.clinicianNameCtrl.value != null ? formGroup.controls.clinicianNameCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('clinicianNameCtrl', 'Name of Licensed Clinician');
            characterValidation = false;
          }

          controlValue = formGroup.controls.clinicianLicenseNoCtrl.value != null ? formGroup.controls.clinicianLicenseNoCtrl.value.trim() : '';
          if (controlValue.length < 2 && controlValue != '')
          {
            this.fillValidationControls('clinicianLicenseNoCtrl', 'Clinician License No');
            characterValidation = false;
          }

          var nysLicensedClinicianTitleTypeCtrl = this.mhrGroup.get('nysLicensedClinicianTitleTypeCtrl').value;
          if (nysLicensedClinicianTitleTypeCtrl === 932)
          {
            controlValue = formGroup.controls.collaboratingClinicianName.value != null ? formGroup.controls.collaboratingClinicianName.value.trim() : '';
            if (controlValue.length < 2 && controlValue != '')
            {
              this.fillValidationControls('collaboratingClinicianName', 'Collaborating Clinician Name');
              characterValidation = false;
            }

            controlValue = formGroup.controls.collaboratingClinicianLicenseNo.value != null ? formGroup.controls.collaboratingClinicianLicenseNo.value.trim() : '';
            if (controlValue.length < 2 && controlValue != '')
            {
              this.fillValidationControls('collaboratingClinicianLicenseNo', 'Collaborating Clinician License No');
              characterValidation = false;
            }
          }

          if(characterValidation === false)
            this.clearControls(this.validationControls);

        break;  

    }

     return characterValidation;
  }

  fillValidationControls(controlName : string, controlLable : string)
  {
    var validationControl : [string, string] = ['',''];
    validationControl[0] = controlName;
    validationControl[1] = controlLable;
    this.validationControls.push(validationControl);
  }

  clearControls(controls : [string, string][])
  {
    var validationMessage : string = '';

    controls.forEach(control => {
      //this.mhrGroup.controls[control[0]].reset(null);
      this.mhrGroup.controls[control[0]].setValue(null);
      //this.mhrGroup.controls[control[0]].clearValidators();
      //this.mhrGroup.controls[control[0]].updateValueAndValidity();
      validationMessage = validationMessage + control[1] + ", ";
      });

      if(validationMessage.length > 0)
        validationMessage = validationMessage.slice(0, validationMessage.length - 2);

     this.callToastrService(validationMessage);
  }

  callToastrService(controlName : string)
  {
    var frontmsg : string = '';
    var backmsg : string = ' should be at least 2 character long';
    var message = frontmsg + controlName + backmsg;
    this.isValid = false;
    if (!this.toastrService.currentlyActive)
      this.toastrService.error(message);
  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    // const controlTouched = !!(formGroup && (formGroup.dirty || formGroup.touched));
    // return controlTouched;

    var controlTouched : any;

    if (this.selectedTab === 3)
    {
      if ((this.isDiagnosisTouched === true)
        || (formGroup.controls.recommendationCtrl.dirty || formGroup.controls.recommendationCtrl.touched)
        || (formGroup.controls.formulationCtrl.dirty || formGroup.controls.formulationCtrl.touched)
      )
        controlTouched = true;
      else
        controlTouched = false;
    }
    else{
      controlTouched = !!(formGroup && (formGroup.dirty || formGroup.touched));
      this.isDiagnosisTouched = false;
    }


    return controlTouched;

  }

  //Get Selected Clinician ToolTip
  getToolTipData() {
    var selected = this.mhrGroup.get('nysLicensedClinicianTitleTypeCtrl').value != null ? this.mhrGroup.get('nysLicensedClinicianTitleTypeCtrl').value : null;
    if (selected) {
      if (this.clinicianTitleTypes && this.clinicianTitleTypes.length > 0) {
        const selectedTitleType = this.clinicianTitleTypes.find(({ refGroupDetailID }) => refGroupDetailID === selected);
        if (selectedTitleType) {
          return selectedTitleType.longDescription;
        }
      }
    }
  }

   //Get Selected Collaborating Clinician ToolTip
   getCollaboratingToolTipData() {
    var selected = this.mhrGroup.get('collaboratingClinicianTitleType').value != null ? this.mhrGroup.get('collaboratingClinicianTitleType').value : null;
    if (selected) {
      if (this.collaboratingClinicianTitleTypes && this.collaboratingClinicianTitleTypes.length > 0) {
        const selectedTitleType = this.collaboratingClinicianTitleTypes.find(({ refGroupDetailID }) => refGroupDetailID === selected);
        if (selectedTitleType) {
          return selectedTitleType.longDescription;
        }
      }
    }
  }

  async ngOnDestroy() {
    this.saveMHR(1);

    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }

    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }

    if (this.routeSub)
    {
      await this.routeSub.unsubscribe();
    }
  }



}
