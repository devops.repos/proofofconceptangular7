import { PACTClinicalMHRDiagnosisDetails } from 'src/app/shared/diagnosis/diagnosis.model';

export class PACTMentalHealthReport {
    pactMentalHealthReportID?: number;
    pactApplicationID: number;
    pactPsychiatricPsychoMHRID?: number;
    presentIllnessHistory: string;
    medicalHistory: string;
    psychiatricHistory: string;
    substanceUse: string;
    familyHistory : string;
    socialRelations : string;
    educationEmployment : string;
    legalCriminal : string;
    housingHomeless : string;
    appearance: string;
    motorActivity: string;
    affect: string;
    mood: string;
    speech: string;
    thoughtForm : string;
    insight : string;
    cognitiveTesting : string;
    thoughtContent : string;
    judgement : string;
    impulseControl : string;
    hasMedicalDiagnosis : boolean;
    hasPrincipalDiagnosis : boolean;
    hasOtherDiagnosis : boolean;
    recommendation : string;
    formulation : string;
    //isMHRTabComplete? : boolean;
    verificationConsent : boolean;
    verifiedBy : number;
    verifiedByName : string;
    verifiedDate : string;
    nysLicensedClinicianTitleType : number;
    psychiatricEvalDate : string;
    clinicianName : string;
    clinicianLicenseNo : string;
    collaboratingClinicianTitleType : number;
    collaboratingClinicianName : string;
    collaboratingClinicianLicenseNo : string;
    isMHRHistoryTabComplete ? : boolean;
    isMHRSFWTabComplete ? : boolean;
    isMHRMentalStatusTabComplete ? : boolean;   
    isMHRDoctorRecommendationTabComplete ? : boolean;
    isMHRVerificationTabComplete ? : boolean;
    userID ? : number;
    
  }


  // export interface PACTMHRDiagnosis {
  //   pactMHRDiagnosisID?: number;
  //   pactMentalHealthReportID: number;
  //   refDiagnosisID: number;
  //   refDiagnosisDescription : string;
  //   diagnosisTypeID : number;
  //   riskOf: boolean;
  //   historyOf: boolean;
  //   provisional: boolean;
  //   userid?: number;
  // }

//   export interface PACTMHRDiagnosis {
//       pactMentalHealthReportID: number;
//       pactClinicalMHRDiagnosisDetails : PACTClinicalMHRDiagnosisDetails;
//       userid?: number;
//   }
