export class PACTPsychiatricEvaluation {
    pactPsychiatricEvaluationID? : number;
    pactPsychiatricPsychoMHRID : number;
    verificationConsent : boolean;
    verifiedBy : number;
    verifiedByName : string;
    verifiedDate : string;
    nysLicensedClinicianTitleType : number;
    evaluationDate : string;
    clinicianName : string;
    licenseNo : string;
    collaboratingClinicianTitleType : number;
    collaboratingClinicianName : string;
    collaboratingClinicianLicenseNo : string;
    comprehensiveEval : string;
    isPsychiatricEvaluationTabComplete? : boolean;
    userID?: number;
  }