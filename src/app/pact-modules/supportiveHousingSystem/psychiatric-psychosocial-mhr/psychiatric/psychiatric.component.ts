import { Component, OnInit, Input, EventEmitter, Output, HostListener, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

//Model References
import { PACTPsychiatricEvaluation } from './psychiatric.model';
import { PACTPsychiatricPsychoMHR, TabStatus } from '../psychiatric-psychosocial-mhr.model';
import { AuthData } from 'src/app/models/auth-data.model';
import { RefGroupDetails } from 'src/app/models/refGroupDetailsDropDown.model';
import { PACTReportParameters, PACTReportUrlParams } from 'src/app/models/pactReportParameters.model';

//Service References
import { CommonService } from 'src/app/services/helper-services/common.service';
import { PsychiatricPsychosocialMHRService } from '../psychiatric-psychosocial-mhr.service';
import { ToastrService } from 'ngx-toastr';
import { SidenavStatusService } from 'src/app/core/sidenav-list/sidenav-status.service';

import { environment } from 'src/environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-psychiatric',
  templateUrl: './psychiatric.component.html',
  styleUrls: ['./psychiatric.component.scss'],
  providers: [DatePipe]
})
export class PsychiatricComponent implements OnInit, OnDestroy {
  @Input() userData;
  uData: AuthData;

  @Input() pactPsychiatricPsychoMHR;
  psychiatricPsychoMHR = new PACTPsychiatricPsychoMHR();

  @Input() tabStatus;
  tStatus = new TabStatus();
  @Output() tabNavigation = new EventEmitter<TabStatus>();
  @Output() psychiatricTabStatus = new EventEmitter<any>();
  isPsychiatricEvaluationTabComplete: boolean;
  isValid: boolean = false;
  isControlTouched: boolean = false;
  isVerificationTouched: boolean = false;

  isPsychiatricAttachment: boolean;
  verifiedDate: string;
  verifiedBy: number;
  verifiedByName: string;
  clinicianTitleTypes: RefGroupDetails[];
  collaboratingClinicianTitleTypes: RefGroupDetails[];
  isCollaboratingFieldsVisible : boolean = false;
  routeSub: any;

  public mask = {
    guide: true,
    showMask: false,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  minDate = new Date(new Date().getTime() - 180 * 24 * 60 * 60 * 1000);
  maxDate = new Date(Date.now());


  psychiatricGroup: FormGroup;
  pactPsychiatricEvaluation = new PACTPsychiatricEvaluation();

  reportUrl: SafeResourceUrl;
  reportServerUrl: string = environment.pactReportServerUrl;
  reportName: string = 'PsychiarticEvalReport';
  reportParameters: string = '&rs:Command=Render&rc:Toolbar=false&ReportParameterID=';
  pactReportParameters: PACTReportParameters[] = [];
  guid: string;
  reportParams: PACTReportUrlParams;

  activatedRouteSub: Subscription;
  sidenavCompleteStatusSub: Subscription;

  //Validation
  validationControls : [string, string][] = [];

  constructor(private formBuilder: FormBuilder
    , private datePipe: DatePipe
    , private commonService: CommonService
    , private psychiatricPsychosocialMHRService: PsychiatricPsychosocialMHRService
    , private router: Router
    , private toastrService: ToastrService
    , private sidenavStatusService: SidenavStatusService
    , private activatedRoute: ActivatedRoute
    , private dialog: MatDialog
    , private sanitizer: DomSanitizer
  ) { }

  //Save Data on page refresh
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (this.psychiatricGroup.dirty) {
      this.savePsychiatricEvaluation();

      if (this.psychiatricPsychoMHR.pactApplicationID) {
        this.sidenavStatusService.setApplicationIDForSidenavStatus(this.psychiatricPsychoMHR.pactApplicationID);
      }

    }
  };

  ngOnInit() {

    this.psychiatricGroup = this.formBuilder.group({
      verificationConsent: [''],
      nysLicensedClinicianTitleType: [''],
      evaluationDate: ['', Validators.compose([Validators.required])],
      clinicianName: [''],
      licenseNo: [''],
      collaboratingClinicianTitleType: [''],
      collaboratingClinicianName: [''],
      collaboratingClinicianLicenseNo: [''],
      comprehensiveEval: [''],
    });

    this.routeSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (this.psychiatricGroup.touched && event.navigationTrigger != "popstate") {
          this.savePsychiatricEvaluation();
        }
        if (event.navigationTrigger == "popstate" && this.psychiatricGroup.dirty) // Handles forward and backward browser clicks
        {
          this.savePsychiatricEvaluation();
        }

      };
    });

    this.tStatus = this.tabStatus;

    this.uData = this.userData;
    this.psychiatricPsychoMHR = this.pactPsychiatricPsychoMHR;
    if (this.psychiatricPsychoMHR != null) {
      this.isPsychiatricAttachment = this.psychiatricPsychoMHR.hasPsychiatricAttachmentORDataEnter == 356 ? true : false;

      if (this.psychiatricPsychoMHR.pactPsychiatricPsychoMHRID > 0)
        this.loadData(this.psychiatricPsychoMHR.pactPsychiatricPsychoMHRID);
    }

    //Get Refgroup Details
    var refGroupList = "44";
    this.commonService.getRefGroupDetails(refGroupList)
      .subscribe(
        res => {
          const data = res.body as RefGroupDetails[];
          this.clinicianTitleTypes = data;
          this.collaboratingClinicianTitleTypes = data.filter(d => (d.refGroupDetailID === 432 || d.refGroupDetailID === 434 || d.refGroupDetailID === 435));
        },
        error => {
          //console.log(error);
          // if (!this.toastrService.currentlyActive)
          //   this.toastrService.error("There is an error while fetching Clinician Titles.");

          throw new Error('There is an error while fetching Clinician Title Data.');
        }
      );
  }

  loadData(pactPsychiatricPsychoMHRID: number) {

    this.psychiatricPsychosocialMHRService.getPACTPsychiatricEvaluation(pactPsychiatricPsychoMHRID)
      .subscribe(
        res => {
          //console.log(res);
          this.pactPsychiatricEvaluation = res;
          this.populateForm(this.pactPsychiatricEvaluation);
        });

  }

  populateForm(formData: PACTPsychiatricEvaluation) {
    //console.log('Psychiatric FormData - ', formData);
    if (formData != null) {
      this.psychiatricGroup.controls['verificationConsent'].setValue(formData.verificationConsent);
      this.psychiatricGroup.controls['nysLicensedClinicianTitleType'].setValue(formData.nysLicensedClinicianTitleType);
      this.psychiatricGroup.controls['evaluationDate'].setValue(formData.evaluationDate);
      this.psychiatricGroup.controls['clinicianName'].setValue(formData.clinicianName);
      this.psychiatricGroup.controls['licenseNo'].setValue(formData.licenseNo);
      this.psychiatricGroup.controls['comprehensiveEval'].setValue(formData.comprehensiveEval);
      this.psychiatricGroup.controls['collaboratingClinicianTitleType'].setValue(formData.collaboratingClinicianTitleType);
      this.psychiatricGroup.controls['collaboratingClinicianName'].setValue(formData.collaboratingClinicianName);
      this.psychiatricGroup.controls['collaboratingClinicianLicenseNo'].setValue(formData.collaboratingClinicianLicenseNo);

      if(formData.nysLicensedClinicianTitleType === 932)
        this.isCollaboratingFieldsVisible = true;
      else
        this.isCollaboratingFieldsVisible = false;

      this.verifiedDate = formData.verifiedDate != null ? this.datePipe.transform(formData.verifiedDate, "MM/dd/yyyy") : null;
      this.verifiedByName = formData.verifiedByName != null ? formData.verifiedByName : null;
      this.verifiedBy = formData.verifiedBy != null ? formData.verifiedBy : null;

      if (formData.isPsychiatricEvaluationTabComplete != null)
        this.isPsychiatricEvaluationTabComplete = formData.isPsychiatricEvaluationTabComplete;
      else
        this.isPsychiatricEvaluationTabComplete = null;
    }
    else {
      this.pactPsychiatricEvaluation = new PACTPsychiatricEvaluation();
      this.pactPsychiatricEvaluation.pactPsychiatricEvaluationID = null;

      this.psychiatricGroup.controls['verificationConsent'].setValue(false);
      this.psychiatricGroup.controls['nysLicensedClinicianTitleType'].setValue(null);
      this.psychiatricGroup.controls['evaluationDate'].setValue(null);
      this.psychiatricGroup.controls['clinicianName'].setValue(null);
      this.psychiatricGroup.controls['licenseNo'].setValue(null);
      this.psychiatricGroup.controls['comprehensiveEval'].setValue(null);
      this.psychiatricGroup.controls['collaboratingClinicianTitleType'].setValue(null);
      this.psychiatricGroup.controls['collaboratingClinicianName'].setValue(null);
      this.psychiatricGroup.controls['collaboratingClinicianLicenseNo'].setValue(null);

      this.verifiedDate = null;
      this.verifiedByName = null;
      this.verifiedBy = null;
      this.isPsychiatricEvaluationTabComplete = null;
    }

    this.psychiatricTabStatus.emit(this.isPsychiatricEvaluationTabComplete);
  }



  //Next button click
  nextTab() {
    //console.log(this.tStatus);
    //console.log(this.psychiatricPsychoMHR);

    //Call save
    this.savePsychiatricEvaluation();

    if (this.psychiatricPsychoMHR.hasPsychosocialReport === true || this.psychiatricPsychoMHR.isMHRYes === true) {
      this.tStatus.parentTabIndex = 2;
      this.tabNavigation.emit(this.tStatus);
    }
    else {
      //this.router.navigate(['/shs/newApp/documents', this.psychiatricPsychoMHR.pactApplicationID]);
      this.sidenavStatusService.routeToNextPage(this.router, this.activatedRoute);
    }
  }

  //Previous button click
  previousTab() {
    //Call save
    this.savePsychiatricEvaluation();

    this.tStatus.parentTabIndex = 0;
    this.tabNavigation.emit(this.tStatus);
  }

  //On Consent check change
  onIsConsentCheckChange(event: { checked: Boolean }) {
    this.isVerificationTouched = true;
    if (event.checked) {
      this.psychiatricGroup.controls['verificationConsent'].setValue(true);
      this.verifiedBy = this.uData.optionUserId;
      this.verifiedByName = this.uData.firstName + " " + this.uData.lastName;
      this.verifiedDate = this.datePipe.transform(new Date(), "MM/dd/yyyy");
    }
    else {
      this.psychiatricGroup.controls['verificationConsent'].setValue(false);
      this.verifiedBy = null;
      this.verifiedByName = null;
      this.verifiedDate = null;
    }

  }

  autoSaveComprehensiveEvaluation(value: string, mode: string): void {
    if (mode == "Type") {
      switch (value.length) {
        case 10000:
          this.savePsychiatricEvaluation();
          break;
        case 20000:
          this.savePsychiatricEvaluation();
          break;
      }
    }
    else if (mode == "Paste")
      this.savePsychiatricEvaluation();
  }

  openReport() {
    this.pactReportParameters = [
      { parameterName: 'applicationID', parameterValue: this.pactPsychiatricPsychoMHR.pactApplicationID, CreatedBy: this.userData.optionUserId },
      { parameterName: 'reportName', parameterValue: this.reportName, CreatedBy: this.userData.optionUserId },
      { parameterName: 'source', parameterValue: this.router.url, CreatedBy: this.userData.optionUserId },
    ];

    this.commonService.generateGUID(this.pactReportParameters)
      .subscribe(
        res => {
          if (res !== null) {
            this.guid = res;
            //this.reportUrl = this.reportServerUrl + this.reportName + this.reportParameters + this.guid;
            //console.log('GUID ', this.guid);
            this.reportParams = { reportParameterID: this.guid, reportName: this.reportName, "reportFormat": "PDF" };

            this.commonService.generateReport(this.reportParams)
              .subscribe(
                res => {
                  var data = new Blob([res.body], { type: 'application/pdf' });
                  //console.log(data);
                  if (data.size > 512) {
                    var fileURL = URL.createObjectURL(data);
                    const printContent = window.open(fileURL, "_blank");
                    if (printContent != null) {
                      printContent.focus();
                      printContent.print();
                    }
                  }


                },
                error => {
                  // if (!this.toastrService.currentlyActive)
                  //   this.toastrService.error("There is an error while generating report.");

                  throw new Error('There is an error while generating Psychiatric Report.');
                }
              );

          }

        },
        error => {
          // if (!this.toastrService.currentlyActive)
          //   this.toastrService.error("There is an error while generating report.");

          throw new Error('There is an error while generating Psychiatric Report.');
        }
      );

  }

  //Validate Date in last 180 days
  validateDate() {
    var evaluationDate = this.datePipe.transform(this.psychiatricGroup.get('evaluationDate').value, 'MM/dd/yyyy');

    if (!moment(evaluationDate, 'MM/DD/YYYY', true).isValid()) {
      if (!this.toastrService.currentlyActive)
        this.toastrService.error("Invalid Date.");

      this.isValid = false;
    }
    else{

    // }

    // if (evaluationDate) {
      let days = moment(Date.now()).diff(moment(evaluationDate), 'day', true);
      //console.log(days);
      if (days > 181 || days < -0) {
        if (!this.toastrService.currentlyActive)
          this.toastrService.error("Date of Psychiatric Evaluation must be with 180 days of today’s date.");
        
        this.psychiatricGroup.controls['evaluationDate'].setValue(null);
        this.isValid = false;
      }
      else {
        //this.psychiatricGroup.controls['evaluationDate'].setErrors({'firstError': null});
        this.psychiatricGroup.controls.evaluationDate.clearValidators();
        this.psychiatricGroup.controls.evaluationDate.updateValueAndValidity();
       
        this.isValid = true;
      }
    }
  }

  //2 Character Length Validation
  validateControlsLength(formGroup: FormGroup)
  {
    this.validationControls = [];
    var characterValidation : boolean = true;

    var controlValue = formGroup.controls.clinicianName.value != null ? formGroup.controls.clinicianName.value.trim() : '';
    if (controlValue.length < 2 && controlValue != '')
    {
      this.fillValidationControls('clinicianName', 'Name of Licensed Clinician');
      characterValidation = false;
    }

    controlValue = formGroup.controls.licenseNo.value != null ? formGroup.controls.licenseNo.value.trim() : '';
    if (controlValue.length < 2 && controlValue != '')
    {
      this.fillValidationControls('licenseNo', 'Clinician License No');
      characterValidation = false;
    }

    var nysLicensedClinicianTitleType = this.psychiatricGroup.get('nysLicensedClinicianTitleType').value;
    if (nysLicensedClinicianTitleType === 932)
    {
      controlValue = formGroup.controls.collaboratingClinicianName.value != null ? formGroup.controls.collaboratingClinicianName.value.trim() : '';
      if (controlValue.length < 2 && controlValue != '')
      {
        this.fillValidationControls('collaboratingClinicianName', 'Collaborating Clinician Name');
        characterValidation = false;
      }

      controlValue = formGroup.controls.collaboratingClinicianLicenseNo.value != null ? formGroup.controls.collaboratingClinicianLicenseNo.value.trim() : '';
      if (controlValue.length < 2 && controlValue != '')
      {
        this.fillValidationControls('collaboratingClinicianLicenseNo', 'Collaborating Clinician License No');
        characterValidation = false;
      }
    }
    
    if(!this.isPsychiatricAttachment)
    {
      //controlValue = this.psychiatricGroup.get('comprehensiveEval').value != null ? this.psychiatricGroup.get('comprehensiveEval').value.trim() : '';
      controlValue = formGroup.controls.comprehensiveEval.value != null ? formGroup.controls.comprehensiveEval.value.trim() : '';
      if (controlValue.length < 2 && controlValue != '')
      {
        // this.toastrService.error("Comprehensive Psychiatric Evaluation should be at least 2 character long");
        // this.psychiatricGroup.controls['comprehensiveEval'].setValue(null);
        this.fillValidationControls('comprehensiveEval', 'Comprehensive Psychiatric Evaluation');
        characterValidation = false;
      }
   }

    if(characterValidation === false)
      this.clearControls(this.validationControls);

    return characterValidation;
  }

  fillValidationControls(controlName : string, controlLable : string)
  {
    var validationControl : [string, string] = ['',''];
    validationControl[0] = controlName;
    validationControl[1] = controlLable;
    this.validationControls.push(validationControl);
  }

  clearControls(controls : [string, string][])
  {
    var validationMessage : string = '';

    controls.forEach(control => {
      this.psychiatricGroup.controls[control[0]].setValue(null);
      validationMessage = validationMessage + control[1] + ", ";
      });

      if(validationMessage.length > 0)
        validationMessage = validationMessage.slice(0, validationMessage.length - 2);

     this.callToastrService(validationMessage);
  }

  callToastrService(controlName : string)
  {
    var frontmsg : string = '';
    var backmsg : string = ' should be at least 2 character long';
    var message = frontmsg + controlName + backmsg;
    this.isValid = false;
    if (!this.toastrService.currentlyActive)
      this.toastrService.error(message);
  }

  savePsychiatricEvaluation() {
    //Verify if controls is touched
    //this.isControlTouched = this.markFormGroupTouched(this.psychiatricGroup);
    //console.log('Psychiatric Control Touched ', this.isControlTouched);

    //if(this.isControlTouched){
      if (this.psychiatricGroup.get('evaluationDate').value != null)
        this.validateDate();
      else
        this.isValid = true;

      //Validate ComprehensiveEval Lenght
      //if(!this.isPsychiatricAttachment)
      this.validateControlsLength(this.psychiatricGroup);

      //console.log('Psychiatric Valid ', this.isValid);
      if (this.isValid) {

        this.pactPsychiatricEvaluation.pactPsychiatricPsychoMHRID = this.psychiatricPsychoMHR.pactPsychiatricPsychoMHRID;
        this.pactPsychiatricEvaluation.verificationConsent = this.psychiatricGroup.get('verificationConsent').value != null ? this.psychiatricGroup.get('verificationConsent').value : null;
        this.pactPsychiatricEvaluation.nysLicensedClinicianTitleType = this.psychiatricGroup.get('nysLicensedClinicianTitleType').value != null ? this.psychiatricGroup.get('nysLicensedClinicianTitleType').value : null;
        this.pactPsychiatricEvaluation.evaluationDate = this.psychiatricGroup.get('evaluationDate').value != null ? this.psychiatricGroup.get('evaluationDate').value : null;
        this.pactPsychiatricEvaluation.clinicianName = this.psychiatricGroup.get('clinicianName').value != null ? this.psychiatricGroup.get('clinicianName').value : null;
        this.pactPsychiatricEvaluation.licenseNo = this.psychiatricGroup.get('licenseNo').value != null ? this.psychiatricGroup.get('licenseNo').value : null;
        this.pactPsychiatricEvaluation.comprehensiveEval = this.psychiatricGroup.get('comprehensiveEval').value != null ? this.psychiatricGroup.get('comprehensiveEval').value : null;
        //this.pactPsychiatricEvaluation.verifiedBy = this.uData.optionUserId;
        this.pactPsychiatricEvaluation.verifiedBy = this.verifiedBy;
        this.pactPsychiatricEvaluation.verifiedDate = this.verifiedDate;
        this.pactPsychiatricEvaluation.userID = this.uData.optionUserId;

        // if(this.psychiatricGroup.get('nysLicensedClinicianTitleType').value === 932)
        // {
          this.pactPsychiatricEvaluation.collaboratingClinicianTitleType = this.psychiatricGroup.get('collaboratingClinicianTitleType').value;
          this.pactPsychiatricEvaluation.collaboratingClinicianName = this.psychiatricGroup.get('collaboratingClinicianName').value;
          this.pactPsychiatricEvaluation.collaboratingClinicianLicenseNo = this.psychiatricGroup.get('collaboratingClinicianLicenseNo').value;
        // }
        // else{
        //   this.pactPsychiatricEvaluation.collaboratingClinicianTitleType = null;
        //   this.pactPsychiatricEvaluation.collaboratingClinicianName = null;
        //   this.pactPsychiatricEvaluation.collaboratingClinicianLicenseNo = null;
        // }

        //console.log('PactPsychiatricEvaluation - ', this.pactPsychiatricEvaluation);

        this.psychiatricPsychosocialMHRService.savePACTPsychiatricEvaluation(this.pactPsychiatricEvaluation)
          .subscribe(data => {
            if (!this.toastrService.currentlyActive)
              this.toastrService.success("Data has been saved.");

            this.isVerificationTouched = false;
            //this.sidenavStatusService._sidenavStatusReload(this.activatedRoute);
            if (this.pactPsychiatricPsychoMHR.pactApplicationID) {
              this.sidenavStatusService.setApplicationIDForSidenavStatus(this.psychiatricPsychoMHR.pactApplicationID);
            }


            this.populateForm(data);
          },
            error => {
              // if (!this.toastrService.currentlyActive)
              //   this.toastrService.error("There is an error while saving data.");

              throw new Error('There is an error while saving Psychiatric Evaluation Data.');
            });
      }

    //}



  }

  onClinicianTitleTypeChange()
  {
    if (this.psychiatricGroup.get('nysLicensedClinicianTitleType').value === 932)
      this.isCollaboratingFieldsVisible = true;
    else
    {
      this.isCollaboratingFieldsVisible = false; 

      this.psychiatricGroup.controls['collaboratingClinicianTitleType'].setValue(null);
      this.psychiatricGroup.controls['collaboratingClinicianName'].setValue(null);
      this.psychiatricGroup.controls['collaboratingClinicianLicenseNo'].setValue(null);

      this.psychiatricGroup.controls.collaboratingClinicianName.clearValidators();
      this.psychiatricGroup.controls.collaboratingClinicianName.updateValueAndValidity();
      this.psychiatricGroup.controls.collaboratingClinicianLicenseNo.clearValidators();
      this.psychiatricGroup.controls.collaboratingClinicianLicenseNo.updateValueAndValidity();
    }
  }

  private markFormGroupTouched = (formGroup: FormGroup) => {
    // const controlTouched = !!(formGroup && (formGroup.dirty || formGroup.touched));
    // return controlTouched;

    var controlTouched : any;
    //console.log(formGroup);

    if ((this.isVerificationTouched === true)
      || (formGroup.controls.nysLicensedClinicianTitleType.dirty || formGroup.controls.nysLicensedClinicianTitleType.touched)
      || (formGroup.controls.evaluationDate.dirty || formGroup.controls.evaluationDate.touched)
      || (formGroup.controls.clinicianName.dirty || formGroup.controls.clinicianName.touched)
      || (formGroup.controls.licenseNo.dirty || formGroup.controls.licenseNo.touched)
      || (formGroup.controls.comprehensiveEval.dirty || formGroup.controls.comprehensiveEval.touched)
    )
      controlTouched = true;
    else
      controlTouched = false;

    //console.log('controlTouched ', controlTouched);
    return controlTouched;
  }

  onUploadFinish = (event: string) => {
    //console.log(event);
    if (event != null) {
      this.isPsychiatricEvaluationTabComplete = true;
      this.psychiatricTabStatus.emit(this.isPsychiatricEvaluationTabComplete);
    }
  }

  //Get Selected Clinician ToolTip
  getToolTipData() {
    var selected = this.psychiatricGroup.get('nysLicensedClinicianTitleType').value != null ? this.psychiatricGroup.get('nysLicensedClinicianTitleType').value : null;
    if (selected) {
      if (this.clinicianTitleTypes && this.clinicianTitleTypes.length > 0) {
        const selectedTitleType = this.clinicianTitleTypes.find(({ refGroupDetailID }) => refGroupDetailID === selected);
        if (selectedTitleType) {
          return selectedTitleType.longDescription;
        }
      }
    }
  }

  //Get Selected Collaborating Clinician ToolTip
  getCollaboratingToolTipData() {
    var selected = this.psychiatricGroup.get('collaboratingClinicianTitleType').value != null ? this.psychiatricGroup.get('collaboratingClinicianTitleType').value : null;
    if (selected) {
      if (this.collaboratingClinicianTitleTypes && this.collaboratingClinicianTitleTypes.length > 0) {
        const selectedTitleType = this.collaboratingClinicianTitleTypes.find(({ refGroupDetailID }) => refGroupDetailID === selected);
        if (selectedTitleType) {
          return selectedTitleType.longDescription;
        }
      }
    }
  }

  async ngOnDestroy() {
    this.savePsychiatricEvaluation();

    if (this.activatedRouteSub) {
      this.activatedRouteSub.unsubscribe();
    }

    if (this.sidenavCompleteStatusSub) {
      this.sidenavCompleteStatusSub.unsubscribe();
    }

    if (this.routeSub)
    {
      await this.routeSub.unsubscribe();
    }
  }

}
