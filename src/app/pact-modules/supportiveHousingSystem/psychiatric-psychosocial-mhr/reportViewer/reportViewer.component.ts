import { Component, Inject, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
    selector: 'app-reportViewer',
    templateUrl: './reportViewer.component.html',
})

export class ReportViewerComponent {
    reportUrl : string;
    sanitizedReportUrl : SafeResourceUrl;
    
    //Constructor
    constructor(
        @Inject(MAT_DIALOG_DATA) private data: any
        , private dialogRef: MatDialogRef<ReportViewerComponent>
        , private sanitizer: DomSanitizer
        , private elementRef: ElementRef

        ) {
        if (data) {
            this.reportUrl = data;
            //this.sanitizedReportUrl = this.sanitizer.bypassSecurityTrustResourceUrl(data);
        }
    }

    ngOnInit() {

        const iframe = this.elementRef.nativeElement.querySelector('iframe');
        this.sanitizedReportUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.reportUrl);
        iframe.src = this.sanitizedReportUrl;
    }

    //Open PDF document in a new window with print dialog
    printReport() {
        const printContent = window.open(this.reportUrl, "_blank");
        if (printContent != null) {
            printContent.focus();
            printContent.print();
        }
        return false;
    }

    //Close the dialog on close button
    closeDialog() {
        this.dialogRef.close(true);
    }

    
}