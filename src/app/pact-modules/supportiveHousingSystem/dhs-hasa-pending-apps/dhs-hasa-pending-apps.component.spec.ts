import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DhsHasaPendingAppsComponent } from './dhs-hasa-pending-apps.component';

describe('DhsHasaPendingAppsComponent', () => {
  let component: DhsHasaPendingAppsComponent;
  let fixture: ComponentFixture<DhsHasaPendingAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DhsHasaPendingAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DhsHasaPendingAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
