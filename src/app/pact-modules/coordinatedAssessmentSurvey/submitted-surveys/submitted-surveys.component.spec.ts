import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittedSurveysComponent } from './submitted-surveys.component';

describe('SubmittedSurveysComponent', () => {
  let component: SubmittedSurveysComponent;
  let fixture: ComponentFixture<SubmittedSurveysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmittedSurveysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedSurveysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
