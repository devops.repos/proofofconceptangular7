import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
// import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ErrorsModule } from 'src/app/shared/errors-handling/errors.module';

import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';

import { CapsRoutingModule } from './caps-routing.module';
import { NewSurveyComponent } from './new-survey/new-survey.component';
import { PendingSurveysComponent } from './pending-surveys/pending-surveys.component';
import { SubmittedSurveysComponent } from './submitted-surveys/submitted-surveys.component';

@NgModule({
  declarations: [
    NewSurveyComponent,
    PendingSurveysComponent,
    SubmittedSurveysComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    CapsRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    ErrorsModule,
    AgGridModule.withComponents([]),
  ]
})
export class CapsModule { }
