import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PactLandingPageComponent } from 'src/app/core/pact-landing-page/pact-landing-page.component';
import { NewSurveyComponent } from './new-survey/new-survey.component';
import { PendingSurveysComponent } from './pending-surveys/pending-surveys.component';
import { SubmittedSurveysComponent } from './submitted-surveys/submitted-surveys.component';
// import { NotificationResolver } from 'src/app/services/resolvers/notification.resolver';

const capsRoutes: Routes = [
  {
    path: '',
    component: PactLandingPageComponent,
    // resolve: { notifications: NotificationResolver},
    children: [
      {
        path: 'caps', children: [
          { path: 'new-survey', component: NewSurveyComponent },
          { path: 'pending-surveys', component: PendingSurveysComponent },
          { path: 'submitted-surveys', component: SubmittedSurveysComponent }
        ]
      },
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(capsRoutes)],
  exports: [RouterModule],
  // providers: [NotificationResolver]
})
export class CapsRoutingModule { }
