export const environment = {
  production: true,
  pactApiUrl: 'https://webd16options01.windows.nyc.hra.nycnet/PACTAPI/api/',
  pactReportServerUrl: 'http://d2e1cldb16/ReportServer?/CAPS/Development/',
  capsURL: 'https://webdevd.hra.nycnet/CAPSDEV/Home',
  juniperLogoutUrl: 'https://a069-ra.nyc.gov/dana-na/auth/logout.cgi',
  juniperHomeUrl: 'https://a069-ra.nyc.gov/dana/home/starter.cgi?startpageonly=1',
  idleTimeOut: 1500, //1500 = 25 minutes
  idleDialogWaitTime: 300 //300 = 5 mintues
};
