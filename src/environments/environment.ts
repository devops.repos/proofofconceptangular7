// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  pactApiUrl: 'https://webd16options01.windows.nyc.hra.nycnet/PACTAPI/api/',
  // pactApiUrl: 'http://localhost:37959/api/',
  pactReportServerUrl: 'http://d2e1cldb16/ReportServer?/CAPS/Development/',
  capsURL: 'https://webdevd.hra.nycnet/CAPSDEV/Home',
  juniperLogoutUrl: 'https://a069-ra.nyc.gov/dana-na/auth/logout.cgi',
  juniperHomeUrl: 'https://a069-ra.nyc.gov/dana/home/starter.cgi?startpageonly=1',
  idleTimeOut: 1500, //1500 = 25 minutes , 0 = deactive idle check
  idleDialogWaitTime: 300 //300 = 5 mintues
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
